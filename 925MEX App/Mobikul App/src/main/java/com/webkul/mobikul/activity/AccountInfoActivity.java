package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityAccountInfoBinding;
import com.webkul.mobikul.handler.AccountInfoHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.customer.accountInfo.AccountInfoResponseData;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AccountInfoActivity extends BaseActivity {

    public ActivityAccountInfoBinding mBinding;
    private AccountInfoResponseData mActivityInfoResponseData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_account_info);
        setActionbarTitle(getResources().getString(R.string.account_info_activity_title));
        mBinding.setIsLoading(true);

        ApiConnection.getAccountInfo(AppSharedPref.getStoreId(this)
                , AppSharedPref.getCustomerId(this))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<AccountInfoResponseData>(this) {
            @Override
            public void onNext(AccountInfoResponseData accountInfoResponseData) {
                super.onNext(accountInfoResponseData);
                mBinding.setIsLoading(false);
                onResponseRecieved(accountInfoResponseData);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mBinding.setIsLoading(false);
            }
        });
    }

    private void onResponseRecieved(AccountInfoResponseData accountInfoResponseData) {
        mActivityInfoResponseData = accountInfoResponseData;

        String[] dateOfBirth = mActivityInfoResponseData.getDOBValue().split(" ");
        mActivityInfoResponseData.setDOBValue(dateOfBirth[0]);
        mBinding.setAccountInfoData(mActivityInfoResponseData);
        mBinding.setAccountInfoHandler(new AccountInfoHandler(AccountInfoActivity.this));

        mActivityInfoResponseData.initPrefixSelectedItemPosition();
        mActivityInfoResponseData.initSuffixSelectedItemPosition();

        setUpPrefix();
        setUpSuffix();
        setUpGender();
    }

    public ActivityAccountInfoBinding getBinding() {
        return mBinding;
    }

    private void setUpGender() {
        if (mActivityInfoResponseData.getIsGenderVisible()) {
            List<String> genderSpinnerData = new ArrayList<>();
            genderSpinnerData.add("");
            genderSpinnerData.add(getResources().getString(R.string.male));
            genderSpinnerData.add(getResources().getString(R.string.female));

            mBinding.genderSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                    , genderSpinnerData
            ));
            mBinding.genderSpinner.setSelection(mActivityInfoResponseData.getGenderValue());
            mBinding.genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mActivityInfoResponseData.setGenderValue(parent.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void setUpSuffix() {
        if (mActivityInfoResponseData.getIsSuffixVisible() && mActivityInfoResponseData.isSuffixHasOptions() && mActivityInfoResponseData.getSuffixOptions().size() > 0) {
            List<String> suffixSpinnerData = new ArrayList<>();
            if (!mActivityInfoResponseData.getIsSuffixRequired())
                suffixSpinnerData.add("");
            for (int suffixIterator = 0; suffixIterator < mActivityInfoResponseData.getSuffixOptions().size(); suffixIterator++) {
                suffixSpinnerData.add(mActivityInfoResponseData.getSuffixOptions().get(suffixIterator));
            }

            mBinding.suffixSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                    , suffixSpinnerData
            ));
            mBinding.suffixSpinner.setSelection(mActivityInfoResponseData.getSuffixSelectedItemPosition());
            mBinding.suffixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mActivityInfoResponseData.setSuffixValue(parent.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void setUpPrefix() {
        if (mActivityInfoResponseData.getIsPrefixVisible() && mActivityInfoResponseData.isPrefixHasOptions() && mActivityInfoResponseData.getPrefixOptions().size() > 0) {
            List<String> prefixSpinnerData = new ArrayList<>();
            if (!mActivityInfoResponseData.getIsPrefixRequired())
                prefixSpinnerData.add("");
            for (int prefixIterator = 0; prefixIterator < mActivityInfoResponseData.getPrefixOptions().size(); prefixIterator++) {
                prefixSpinnerData.add(mActivityInfoResponseData.getPrefixOptions().get(prefixIterator));
            }

            mBinding.prefixSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                    , prefixSpinnerData
            ));
            mBinding.prefixSpinner.setSelection(mActivityInfoResponseData.getPrefixSelectedItemPosition());
            mBinding.prefixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mActivityInfoResponseData.setPrefixValue(parent.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (mBinding.fingerPrintLoginContainer.getVisibility() == View.VISIBLE) {
            mBinding.fingerPrintLoginContainer.setVisibility(View.GONE);
            mBinding.getAccountInfoHandler().cancelFingerPrint();
        } else {
            super.onBackPressed();
        }
    }
}