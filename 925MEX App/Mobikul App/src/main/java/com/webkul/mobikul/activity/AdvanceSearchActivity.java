package com.webkul.mobikul.activity;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityAdvanceSearchBinding;
import com.webkul.mobikul.handler.AdvanceSearchActivityHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.search.AdvanceSearchFormData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AdvanceSearchActivity extends BaseActivity {

    public ActivityAdvanceSearchBinding mBinding;
    public AdvanceSearchFormData mAdvanceSearchFormData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_advance_search);
        mBinding.setIsLoading(true);
        setActionbarTitle(getResources().getString(R.string.activity_advance_search_title));
        ApiConnection.getAdvanceSearchFormData(AppSharedPref.getStoreId(this), AppSharedPref.getCurrencyCode(this))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<AdvanceSearchFormData>(this) {
            @Override
            public void onNext(AdvanceSearchFormData advanceSearchFormData) {
                super.onNext(advanceSearchFormData);
                mAdvanceSearchFormData = advanceSearchFormData;
                createAdvanceSearchForm();
                mBinding.setHandler(new AdvanceSearchActivityHandler(AdvanceSearchActivity.this));
                mBinding.setIsLoading(false);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mBinding.setIsLoading(false);
            }
        });

        mBinding.scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, final int scrollY, int oldScrollX, int oldScrollY) {
                if ((scrollY - oldScrollY) < 0 || (scrollY > (mBinding.scrollView.getChildAt(0).getHeight() - mBinding.scrollView.getHeight() - 100))) {
                    mBinding.searchBtn.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
                } else {
                    mBinding.searchBtn.animate().alpha(0f).translationY(mBinding.searchBtn.getHeight()).setInterpolator(new AccelerateInterpolator(1.4f));
                }
            }
        });
    }

    private void createAdvanceSearchForm() {
        if (mAdvanceSearchFormData.getFieldList().size() > 0) {
            mBinding.setHasFields(true);
            for (int i = 0; i < mAdvanceSearchFormData.getFieldList().size(); i++) {

                TextView textView = new TextView(this);
                textView.setText(mAdvanceSearchFormData.getFieldList().get(i).getLabel());
                textView.setTextSize(16);
                mBinding.advanceSearchFieldContainer.addView(textView);

                switch ((mAdvanceSearchFormData.getFieldList().get(i).getInputType())) {
                    case "yesno":
//                    ArrayList<String> SpinnerOptions = new ArrayList<String>();
//                    SpinnerOptions.add("All");
//                    for (int j = 0; j < mobikulCatalogGetadvancedsearchFieldsDataArr.getJSONObject(i).getJSONArray("options").length(); j++) {
//                        SpinnerOptions.add(mobikulCatalogGetadvancedsearchFieldsDataArr.getJSONObject(i).getJSONArray("options").getJSONObject(j).getString("label"));
//                    }
//                    Spinner spinner = new Spinner(AdvancedSearch.this);
//                    spinner.setTag("inputType/" + i);
//                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(AdvancedSearch.this,
//                            android.R.layout.simple_spinner_dropdown_item, SpinnerOptions);
//                    spinner.setAdapter(spinnerArrayAdapter);
//                    rootView.addView(spinner);
                        break;
                    case "select":
                        if(!mAdvanceSearchFormData.getFieldList().get(i).getLabel().equals("Color")) {
                            for (int j = 0; j < mAdvanceSearchFormData.getFieldList().get(i).getOptions().size(); j++) {
                                CheckBox chk = new CheckBox(this);
                                chk.setTag("inputType/" + i + "/check/" + j);
                                chk.setText(mAdvanceSearchFormData.getFieldList().get(i).getOptions().get(j).getLabel());
                                chk.setTextSize(14);
                                mBinding.advanceSearchFieldContainer.addView(chk);
                            }
                        }else{
                            textView.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case "string":
                        EditText editText = new EditText(this);
                        editText.setSingleLine();
                        editText.setTag("inputType/" + i);
                        editText.setTextSize(14);
                        mBinding.advanceSearchFieldContainer.addView(editText);
                        break;
                    case "price":
                        LinearLayout priceLinearLayout = new LinearLayout(this);
                        priceLinearLayout.setTag("inputType/" + i);
                        priceLinearLayout.setOrientation(LinearLayout.HORIZONTAL);

                        LinearLayout.LayoutParams lLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        priceLinearLayout.setLayoutParams(lLParams);

                        EditText editText1 = new EditText(this);
                        editText1.setTag("priceFrom");
                        editText1.setHint(getResources().getString(R.string.price_from));
                        editText1.setTextSize(14);
                        editText1.setSingleLine();
                        editText1.setInputType(InputType.TYPE_CLASS_NUMBER);

                        priceLinearLayout.addView(editText1);
                        priceLinearLayout.addView(getDashView());

                        EditText editText2 = new EditText(this);
                        editText2.setTag("priceTo");
                        editText2.setHint(getResources().getString(R.string.price_to));
                        editText2.setTextSize(14);
                        editText2.setInputType(InputType.TYPE_CLASS_NUMBER);
                        editText2.setSingleLine();
                        priceLinearLayout.addView(editText2);

                        TextView currencyTv = new TextView(this);
                        currencyTv.setText(AppSharedPref.getCurrencyCode(this));
                        currencyTv.setTextSize(14);
                        currencyTv.setPadding(10, 0, 0, 0);
                        priceLinearLayout.addView(currencyTv);

                        mBinding.advanceSearchFieldContainer.addView(priceLinearLayout);

                        break;
                    case "date":
                        final Calendar myCalendar = Calendar.getInstance();
                        final LinearLayout dateLinearLayout = new LinearLayout(this);
                        dateLinearLayout.setTag("inputType/" + i);
                        dateLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                        ActionBar.LayoutParams dateLayoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                        dateLinearLayout.setLayoutParams(dateLayoutParams);

                        final EditText editText3 = new EditText(this);
                        editText3.setSingleLine();
                        editText3.setTag("dateFrom");
                        editText3.setHint(getResources().getString(R.string.date_from));
                        editText3.setTextSize(14);
                        editText3.setInputType(InputType.TYPE_NULL);
                        editText3.setFocusable(false);
                        dateLinearLayout.addView(editText3);

                        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                myCalendar.set(Calendar.YEAR, year);
                                myCalendar.set(Calendar.MONTH, monthOfYear);
                                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                String myFormat = "MM/dd/yy";
                                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                editText3.setText(sdf.format(myCalendar.getTime()));
                            }
                        };
                        editText3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new DatePickerDialog(AdvanceSearchActivity.this, date1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                            }
                        });

                        dateLinearLayout.addView(getDashView());
                        final EditText editText4 = new EditText(this);
                        editText4.setSingleLine();
                        editText4.setTag("dateTo");
                        editText4.setHint(getResources().getString(R.string.date_to));
                        editText4.setTextSize(14);
                        editText4.setInputType(InputType.TYPE_NULL);
                        editText4.setFocusable(false);
                        dateLinearLayout.addView(editText4);


                        View fillGape = new View(this);
                        fillGape.setLayoutParams(new LinearLayout.LayoutParams(0, ActionBar.LayoutParams.MATCH_PARENT, 1.0f));
                        fillGape.setBackgroundColor(Color.WHITE);
                        (dateLinearLayout).addView(fillGape);

                        Button resetDateBtn = new Button(this);
                        resetDateBtn.setTag(i);
                        resetDateBtn.setText(getResources().getString(R.string.reset_date));
                        resetDateBtn.setTextSize(14);

                        resetDateBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), android.R.color.white));
                        resetDateBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.button_background_color));
                        resetDateBtn.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                ((EditText) dateLinearLayout.findViewWithTag("dateFrom")).setText("");
                                ((EditText) dateLinearLayout.findViewWithTag("dateTo")).setText("");
                            }
                        });
                        dateLinearLayout.addView(resetDateBtn);
                        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                myCalendar.set(Calendar.YEAR, year);
                                myCalendar.set(Calendar.MONTH, monthOfYear);
                                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                String myFormat = "MM/dd/yy";
                                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                editText4.setText(sdf.format(myCalendar.getTime()));
                            }
                        };

                        editText4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new DatePickerDialog(AdvanceSearchActivity.this, date2, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                            }
                        });
                        mBinding.advanceSearchFieldContainer.addView(dateLinearLayout);
                        break;
                    case "button":

                        break;
                }
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.spacing_generic));
                View spacing = new View(this);
                spacing.setLayoutParams(params);
                mBinding.advanceSearchFieldContainer.addView(spacing);
            }
        } else {
            mBinding.setHasFields(false);
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.button_height));
        View view = new View(this);
        view.setLayoutParams(params);
        mBinding.advanceSearchFieldContainer.addView(view);
    }

    private TextView getDashView() {
        TextView CustomTextViewDash = new TextView(this);
        CustomTextViewDash.setText("   -   ");
        CustomTextViewDash.setTextSize(14);
        return CustomTextViewDash;
    }
}