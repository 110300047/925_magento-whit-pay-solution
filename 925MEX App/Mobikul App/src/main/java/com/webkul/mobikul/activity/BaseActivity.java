package com.webkul.mobikul.activity;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.RetrofitClient;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.DataBaseHelper;
import com.webkul.mobikul.helper.LocaleUtils;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.helper.NetworkStateReceiver;
import com.webkul.mobikul.helper.SyncCartDbWithServer;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.helper.Utils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.disposables.CompositeDisposable;

import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CART_COUNT;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */

public abstract class BaseActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    public SweetAlertDialog mSweetAlertDialog;
    public StyleableToast mStylableToast;
    public MenuItem mItemCart;
    public Gson gson;
    public DataBaseHelper mOfflineDataBaseHandler;
    //public Context mContext=this;

    public CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public NetworkStateReceiver mNetworkStateReceiver;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showBackButton();
        gson = new Gson();
        mOfflineDataBaseHandler = new DataBaseHelper(this);

        mNetworkStateReceiver = new NetworkStateReceiver();
        mNetworkStateReceiver.addListener(this);
        registerReceiver(mNetworkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    protected void showBackButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected void showHomeButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    public void setActionbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchMenuItem = menu.findItem(R.id.menu_item_search);
        initSearchMenuItem(searchMenuItem);
        mItemCart = menu.findItem(R.id.menu_item_cart);
        LayerDrawable icon = (LayerDrawable) mItemCart.getIcon();
        Utils.setBadgeCount(this, icon, AppSharedPref.getCartCount(this, 0));
        mItemCart.setVisible(AppSharedPref.isLoggedIn(getApplicationContext()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemId = item.getItemId();
        if (menuItemId == android.R.id.home) {
            onBackPressed();
        } else if (menuItemId == R.id.menu_item_cart) {
            startActivity(new Intent(this, CartActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocaleUtils.setStoreLanguage(this, AppSharedPref.getStoreCode(this));
        if (mItemCart != null) {
            LayerDrawable icon = (LayerDrawable) mItemCart.getIcon();
            Utils.setBadgeCount(this, icon, AppSharedPref.getCartCount(this, 0));
        }
    }

    protected void initSearchMenuItem(MenuItem searchMenuItem) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextColor(Color.GRAY);
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(new ComponentName(getApplicationContext(), ((MobikulApplication) getApplication()).getCatalogProductPageClass()));
        searchView.setSearchableInfo(searchableInfo);
    }

    public void updateCartBadge(int cartCount) {
        AppSharedPref.getSharedPreferenceEditor(this, CUSTOMER_PREF).putInt(KEY_CART_COUNT, cartCount).apply();
        if (mItemCart != null) {
            LayerDrawable icon = (LayerDrawable) mItemCart.getIcon();
            Utils.setBadgeCount(this, icon, AppSharedPref.getCartCount(this, 0));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.clear();
        RetrofitClient.getDispatcher().cancelAll();
        ToastHelper.dismiss(this);
        mNetworkStateReceiver.removeListener(this);
        unregisterReceiver(mNetworkStateReceiver);
    }

    @Override
    public void networkAvailable() {
        if (mOfflineDataBaseHandler.getCartTableRowCount() > 0) {
            showAlertDialogWithClickListener(this, SweetAlertDialog.NORMAL_TYPE, getString(R.string.offline_bag), getString(R.string.sync_cart_with_server_body_message)
                    , getString(R.string.sync_now), getString(R.string.clear), new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            showToast(BaseActivity.this, getString(R.string.syncing_in_bg), Toast.LENGTH_LONG, 0);
                            SyncCartDbWithServer syncCartDbWithServer = new SyncCartDbWithServer(BaseActivity.this);
                            syncCartDbWithServer.execute();
                        }
                    }, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            mOfflineDataBaseHandler.clearCartTableData();
                        }
                    });
        }
    }

    @Override
    public void networkUnavailable() {

    }
}