package com.webkul.mobikul.activity;

import android.annotation.TargetApi;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.ActivityCmspageDetailsBinding;
import com.webkul.mobikul.model.CMSPageDataResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_IDENTIFIER;

public class CMSPageDetailsActivity extends BaseActivity {

    private ActivityCmspageDetailsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_cmspage_details);
        startInitialization();
    }

    private void startInitialization() {
        showBackButton();
        showHomeButton();
        setActionbarTitle("");

        ApiConnection.getCMSPageData(
                getIntent().getExtras().getInt(BUNDLE_KEY_IDENTIFIER))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CMSPageDataResponse>(this) {
            @Override
            public void onNext(CMSPageDataResponse cMSPageDataResponse) {
                super.onNext(cMSPageDataResponse);
                setActionbarTitle(cMSPageDataResponse.getTitle());
                mBinding.setData(cMSPageDataResponse);
                WebSettings webSettings = mBinding.cmsPageInfoWebView.getSettings();
                webSettings.setDefaultFontSize(14);
                mBinding.cmsPageInfoWebView.setFocusableInTouchMode(false);
                mBinding.cmsPageInfoWebView.setFocusable(false);
                mBinding.cmsPageInfoWebView.setClickable(false);
                mBinding.cmsPageInfoWebView.setWebViewClient(new WebViewClient() {
                    @SuppressWarnings("deprecation")
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        return true;
                    }

                    @TargetApi(Build.VERSION_CODES.N)
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                        return true;
                    }

                    @Override
                    public void onLoadResource(WebView view, String url) {
                        Log.d(ApplicationConstant.TAG, "onLoadResource: " + url);
                        super.onLoadResource(view, url);
                    }
                });
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }
}