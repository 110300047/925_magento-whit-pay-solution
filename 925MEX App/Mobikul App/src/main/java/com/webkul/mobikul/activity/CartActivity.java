package com.webkul.mobikul.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.CartItemsRecyclerAdapter;
import com.webkul.mobikul.adapter.CrossSellProductsRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityCartBinding;
import com.webkul.mobikul.fragment.EmptyFragment;
import com.webkul.mobikul.handler.CartActivityHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.cart.CartDetailsResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;

import static android.view.View.GONE;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_UP_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_IS_VIRTUAL;

//@Shortcut(id = "cart", icon = R.drawable.ic_vector_menu_cart_accent, shortLabelRes = R.string.title_activity_cart, rank = 3, backStack = {HomeActivity.class})
public class CartActivity extends BaseActivity {

    public ActivityCartBinding mBinding;

    public CartDetailsResponse mCartDetailsResponseData;
    public CartItemsRecyclerAdapter mCartItemsRecyclerAdapter;
    public CrossSellProductsRvAdapter mCrossSellProductsRvAdapter;
    public RewardedVideoAd mRewardVideoAd;
    public boolean hasPlayedVideoReward = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_cart);
        showBackButton();
        setActionbarTitle(getString(R.string.title_activity_cart));

        handleIntent();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        mBinding.scrollView.setVisibility(GONE);
        handleIntent();
    }

    private void handleIntent() {
        mBinding.cartProgressBar.setVisibility(View.VISIBLE);

        ApiConnection.getCartDetails(AppSharedPref.getStoreId(CartActivity.this)
                , Utils.getScreenWidth()
                , AppSharedPref.getQuoteId(CartActivity.this)
                , AppSharedPref.getCustomerId(CartActivity.this)
                , AppSharedPref.getCurrencyCode(CartActivity.this))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CartDetailsResponse>(CartActivity.this) {
            @Override
            public void onNext(CartDetailsResponse cartDetailsResponse) {
                super.onNext(cartDetailsResponse);
                onResponseRecieved(cartDetailsResponse);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    private void onResponseRecieved(CartDetailsResponse cartDetailsResponse) {
        mCartDetailsResponseData = cartDetailsResponse;

        updateCartBadge(mCartDetailsResponseData.getCartCount());

        if (mCartDetailsResponseData.getSubtotal().getUnformatedValue() < mCartDetailsResponseData.getMinimumAmount()) {
            mCartDetailsResponseData.setCanProcceedToCheckout(false);
            mCartDetailsResponseData.setError(getString(R.string.min_order_amt_error) + " " + mCartDetailsResponseData.getMinimumFormattedAmount());
        }

        mBinding.setData(mCartDetailsResponseData);
        mBinding.setHandler(new CartActivityHandler(CartActivity.this));
//        mBinding.executePendingBindings();

        if (cartDetailsResponse.getItems().size() == 0) {
            mBinding.cartProgressBar.setVisibility(GONE);
            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_cart, getString(R.string.empty_bag)
                    , getString(R.string.add_item_to_your_bag_now)), EmptyFragment.class.getSimpleName());
            ft.commit();
        } else {
            mBinding.productsRv.setLayoutManager(new LinearLayoutManager(CartActivity.this));
            mCartItemsRecyclerAdapter = new CartItemsRecyclerAdapter(CartActivity.this, mCartDetailsResponseData.getItems(), mCartDetailsResponseData.getShowThreshold());
            mBinding.productsRv.setAdapter(mCartItemsRecyclerAdapter);
            mBinding.productsRv.setNestedScrollingEnabled(false);
            mBinding.productsRv.setItemAnimator(new FadeInLeftAnimator());
            mBinding.productsRv.getItemAnimator().setRemoveDuration(500);

            /*Loading Cross Sell Products Data*/
            mCrossSellProductsRvAdapter = new CrossSellProductsRvAdapter(CartActivity.this, mCartDetailsResponseData.getCrossSellList());
            mBinding.crossSellProductsRv.setAdapter(mCrossSellProductsRvAdapter);

            mBinding.scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, final int scrollY, int oldScrollX, int oldScrollY) {

                    if ((scrollY - oldScrollY) < 0 || (scrollY > (mBinding.scrollView.getChildAt(0).getHeight() - mBinding.scrollView.getHeight() - 100))) {
                        if (mCartDetailsResponseData.isCanProcceedToCheckout())
                            mBinding.proceedToCheckoutBtn.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
                    } else {
                        mBinding.proceedToCheckoutBtn.animate().alpha(0f).translationY(mBinding.proceedToCheckoutBtn.getHeight()).setInterpolator(new AccelerateInterpolator(1.4f));
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public ActivityCartBinding getBinding() {
        return mBinding;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SIGN_IN_PAGE || requestCode == SIGN_UP_PAGE) {
                Intent i = new Intent(CartActivity.this, CheckoutActivity.class);
                i.putExtra(BUNDLE_KEY_IS_VIRTUAL, mCartDetailsResponseData.getIsVirtual());
                startActivity(i);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCartDetailsResponseData != null && !hasPlayedVideoReward) {
            mBinding.scrollView.setVisibility(GONE);
            mBinding.proceedToCheckoutBtn.setVisibility(GONE);
            handleIntent();
        } else {
            hasPlayedVideoReward = false;
        }
    }
}