package com.webkul.mobikul.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.google.gson.Gson;
import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.CatalogProductListRvAdapter;
import com.webkul.mobikul.adapter.CriteriaDataAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityProductCatalogBinding;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.EmptyFragment;
import com.webkul.mobikul.handler.CatalogProductActivityHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.catalog.CatalogProductData;
import com.webkul.mobikul.model.catalog.ProductShortData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.connection.ApiInterface.MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_PAGE_COUNT;
import static com.webkul.mobikul.constants.ApplicationConstant.REQUEST_TYPE_ADVANCE_SEARCH_COLLECTION;
import static com.webkul.mobikul.constants.ApplicationConstant.REQUEST_TYPE_CATEGORY_COLLECTION;
import static com.webkul.mobikul.constants.ApplicationConstant.REQUEST_TYPE_CUSTOM_COLLECTION;
import static com.webkul.mobikul.constants.ApplicationConstant.REQUEST_TYPE_HOME_PAGE_COLLECTION;
import static com.webkul.mobikul.constants.ApplicationConstant.REQUEST_TYPE_SEARCH_PRODUCT_COLLECTION;
import static com.webkul.mobikul.constants.ApplicationConstant.VIEW_TYPE_GRID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ADVANCE_SEARCH_QUERY_JSON;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CALLED_FROM_HOME_BANNER;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_COLLECTION_TYPE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_FROM_NOTIFICATION;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_IS_FROM_URL;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_NOTIFICATION_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SEARCH_TERM_QUERY;
import static com.webkul.mobikul.customviews.MaterialSearchView.RC_CODE;
import static com.webkul.mobikul.helper.AlertDialogHelper.dismiss;
import static com.webkul.mobikul.helper.AnimationHelper.ANIMATION_DURATION_SHORT;
import static com.webkul.mobikul.helper.AnimationHelper.ANIMATION_DURATION_SHORTEST;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;
import static com.webkul.mobikul.helper.ProductHelper.getProductShortData;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

public class CatalogProductActivity extends BaseActivity implements Callback<CatalogProductData> {

    public HashMap<String, String> sSellerAttributesIdOptionCodeMap;
    public CatalogProductListRvAdapter mCatalogProductListRvAdapter;
    public CatalogProductData mCatalogProductData;
    public ArrayList<ProductShortData> mProductShortDatas;
    public ActivityProductCatalogBinding mBinding;
    private CriteriaDataAdapter mCriteriaDataAdapter;
    public boolean isFirstCall = true;
    private boolean mOpenHome = false;
    private String mNotificationId;
    private String mCollectionType;
    private String mQuery;
    private int mDyListener = 0;
    public int mPageNumber = DEFAULT_PAGE_COUNT;
    private JSONObject mAdvanceSearchQueryObject;
    public JSONArray mFilterInputJson = new JSONArray();
    public JSONArray mSortingInputJson = new JSONArray();
    public int mCategoryId;
    private ActionBar mSupportActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_catalog);
        showBackButton();

        mCatalogProductData = new CatalogProductData();
        mCatalogProductData.setTotalCount(-1);
        mBinding.setData(mCatalogProductData);
        mBinding.setHandler(new CatalogProductActivityHandler(this));
        mBinding.executePendingBindings();

//        if (isTablet(this))
//            mBinding.productCatalogRv.setLayoutManager(new GridLayoutManager(this, 3));
//        else
        if (AppSharedPref.getViewType(this) == VIEW_TYPE_GRID) {
            mBinding.productCatalogRv.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            mBinding.productCatalogRv.setLayoutManager(new LinearLayoutManager(this));
            mBinding.switcherIv.setImageResource(R.drawable.ic_vector_grid);
        }

        mCatalogProductListRvAdapter = new CatalogProductListRvAdapter(this, null);
        mBinding.productCatalogRv.setAdapter(mCatalogProductListRvAdapter);
        mBinding.criteriaDataRv.setLayoutManager(new LinearLayoutManager(this));
        mCriteriaDataAdapter = new CriteriaDataAdapter(this, null);
        mBinding.criteriaDataRv.setAdapter(mCriteriaDataAdapter);

        gson = new Gson();

        mSupportActionBar = getSupportActionBar();

        sSellerAttributesIdOptionCodeMap = new HashMap<>();
        handleIntent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        menu.findItem(R.id.menu_item_customer).setVisible(false);

        mItemCart = menu.findItem(R.id.menu_item_cart);
        LayerDrawable icon = (LayerDrawable) mItemCart.getIcon();
        Utils.setBadgeCount(this, icon, AppSharedPref.getCartCount(this, 0));
        mItemCart.setVisible(AppSharedPref.isLoggedIn(getApplicationContext()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int i = item.getItemId();
        if (i == R.id.menu_item_home_search) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(EmptyFragment.class.getSimpleName());
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
            mBinding.searchView.openSearch();
        }
        return true;
    }

    public void onSearchOpened() {
        if (mSupportActionBar != null) {
            mSupportActionBar.hide();
        }
        mBinding.searchView.setVisibility(View.VISIBLE);
    }

    public void onSearchClosed() {
        Utils.hideKeyboard(this);
        long delayMillis;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            delayMillis = ANIMATION_DURATION_SHORT;
        } else {
            delayMillis = ANIMATION_DURATION_SHORTEST;
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBinding.searchView.setVisibility(View.GONE);
                if (mSupportActionBar != null) {
                    mSupportActionBar.show();
                }
            }
        }, delayMillis);
    }

    /**
     * Search Widget fire ACTION_SEARCH intent twice when user search a word
     * http://stackoverflow.com/a/19392225
     */

    @Override
    protected void onNewIntent(Intent intent) {
        /*Init Data */
        setIntent(intent);
        handleIntent();
    }

    protected void handleIntent() {
        mPageNumber = DEFAULT_PAGE_COUNT;
        isFirstCall = true;

        mCatalogProductData.setLazyLoading(true);
        if (Intent.ACTION_SEARCH.equals(getIntent().getAction()) || getIntent().hasExtra(BUNDLE_KEY_SEARCH_TERM_QUERY)) {
            mCatalogProductData.setRequestType(REQUEST_TYPE_SEARCH_PRODUCT_COLLECTION);
            if (getIntent().hasExtra(BUNDLE_KEY_SEARCH_TERM_QUERY)) {
                mQuery = getIntent().getExtras().getString(BUNDLE_KEY_SEARCH_TERM_QUERY);
            } else {
                mQuery = getIntent().getStringExtra(SearchManager.QUERY);
            }
            setTitle(mQuery);
            FirebaseAnalyticsImpl.logSearchEvent(this, mQuery);
        } else if (getIntent().hasExtra(KEY_CATEGORY_ID)) {
            mCatalogProductData.setRequestType(REQUEST_TYPE_CATEGORY_COLLECTION);

            if (getIntent().hasExtra(BUNDLE_KEY_CALLED_FROM_HOME_BANNER)) {
                mCatalogProductData.setShowBanner(false);
            } else {
                mCatalogProductData.setShowBanner(true);
            }
            setTitle(getIntent().getExtras().getString(KEY_CATEGORY_NAME));
        } else if (getIntent().hasExtra(BUNDLE_KEY_NOTIFICATION_ID)) {
            mNotificationId = getIntent().getStringExtra(BUNDLE_KEY_NOTIFICATION_ID);
            mCatalogProductData.setRequestType(REQUEST_TYPE_CUSTOM_COLLECTION);
            setTitle(getResources().getString(R.string.title_activity_custom_collection));
        } else if (getIntent().hasExtra(BUNDLE_KEY_COLLECTION_TYPE)) {
            mCollectionType = getIntent().getExtras().getString(BUNDLE_KEY_COLLECTION_TYPE);
            mCatalogProductData.setRequestType(REQUEST_TYPE_HOME_PAGE_COLLECTION);
        } else if (getIntent().hasExtra(BUNDLE_KEY_ADVANCE_SEARCH_QUERY_JSON)) {
            mCatalogProductData.setRequestType(REQUEST_TYPE_ADVANCE_SEARCH_COLLECTION);
            setTitle(getResources().getString(R.string.title_activity_Advance_search));
            try {
                mAdvanceSearchQueryObject = new JSONObject(getIntent().getExtras().getString(BUNDLE_KEY_ADVANCE_SEARCH_QUERY_JSON));
            } catch (JSONException e) {
                e.printStackTrace();
                mAdvanceSearchQueryObject = new JSONObject();
            }
        }

        /*For Deeplinking And Notification*/
        if (getIntent().hasExtra(BUNDLE_KEY_IS_FROM_URL) || getIntent().hasExtra(BUNDLE_KEY_FROM_NOTIFICATION)) {
            mOpenHome = true;
        }
        callApi();
    }

    protected void callApi() {
        switch (mCatalogProductData.getRequestType()) {
            case REQUEST_TYPE_CATEGORY_COLLECTION:
                ApiConnection.getCatalogProductData(getIntent().getExtras().getInt(KEY_CATEGORY_ID)
                        , AppSharedPref.getStoreId(CatalogProductActivity.this)
                        , Utils.getScreenWidth()
                        , mPageNumber
                        , AppSharedPref.getCustomerId(this)
                        , mSortingInputJson
                        , mFilterInputJson
                        , getResources().getDisplayMetrics().density
                        , AppSharedPref.getQuoteId(CatalogProductActivity.this)
                        , AppSharedPref.getCurrencyCode(CatalogProductActivity.this))

                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductData>(this) {
                    @Override
                    public void onNext(CatalogProductData catalogProductData) {
                        super.onNext(catalogProductData);
                        onResponseRecieved(catalogProductData);
                    }

                    @Override
                    public void onError(Throwable t) {
                        final Cursor databaseCursor = mOfflineDataBaseHandler.selectFromOfflineDB(MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST, String.valueOf(getIntent().getExtras().getInt(KEY_CATEGORY_ID)));
                        if (databaseCursor != null && databaseCursor.getCount() != 0) {
                            databaseCursor.moveToFirst();
                            onResponseRecieved(gson.fromJson(databaseCursor.getString(0), CatalogProductData.class));
                        } else {
                            super.onError(t);
                        }
                        onRequestFailure();
                    }
                });
                break;

            case REQUEST_TYPE_SEARCH_PRODUCT_COLLECTION:
                ApiConnection.getCatalogSearchResult(
                        AppSharedPref.getCustomerId(CatalogProductActivity.this)
                        , mQuery
                        , AppSharedPref.getStoreId(CatalogProductActivity.this)
                        , Utils.getScreenWidth()
                        , mPageNumber
                        , mSortingInputJson
                        , mFilterInputJson
                        , AppSharedPref.getCurrencyCode(CatalogProductActivity.this))

                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductData>(this) {
                    @Override
                    public void onNext(CatalogProductData catalogProductData) {
                        super.onNext(catalogProductData);
                        onResponseRecieved(catalogProductData);
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        onRequestFailure();
                    }
                });
                break;

            case REQUEST_TYPE_ADVANCE_SEARCH_COLLECTION:
                ApiConnection.getAdvanceSearchResult(AppSharedPref.getStoreId(CatalogProductActivity.this)
                        , mAdvanceSearchQueryObject
                        , mSortingInputJson
                        , mFilterInputJson
                        , Utils.getScreenWidth()
                        , mPageNumber
                        , AppSharedPref.getCurrencyCode(CatalogProductActivity.this))

                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductData>(this) {
                    @Override
                    public void onNext(CatalogProductData catalogProductData) {
                        super.onNext(catalogProductData);
                        onResponseRecieved(catalogProductData);
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        onRequestFailure();
                    }
                });
                break;

            case REQUEST_TYPE_CUSTOM_COLLECTION:
                ApiConnection.getCustomCollectionData(mNotificationId
                        , AppSharedPref.getStoreId(CatalogProductActivity.this)
                        , Utils.getScreenWidth()
                        , mPageNumber
                        , AppSharedPref.getCustomerId(this)
                        , mSortingInputJson
                        , mFilterInputJson
                        , getResources().getDisplayMetrics().density
                        , AppSharedPref.getQuoteId(CatalogProductActivity.this))

                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductData>(this) {
                    @Override
                    public void onNext(CatalogProductData catalogProductData) {
                        super.onNext(catalogProductData);
                        onResponseRecieved(catalogProductData);
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        onRequestFailure();
                    }
                });
                break;

            case REQUEST_TYPE_HOME_PAGE_COLLECTION:
                if (mCollectionType != null) {
                    switch (mCollectionType) {
                        case "featuredProducts":
                            setTitle(getResources().getString(R.string.title_activity_featured_products));
                            ApiConnection.getFeaturedProductData(AppSharedPref.getStoreId(CatalogProductActivity.this)
                                    , Utils.getScreenWidth()
                                    , mPageNumber
                                    , AppSharedPref.getCustomerId(CatalogProductActivity.this)
                                    , AppSharedPref.getQuoteId(CatalogProductActivity.this)
                                    , mSortingInputJson
                                    , mFilterInputJson
                                    , AppSharedPref.getCurrencyCode(CatalogProductActivity.this))

                                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductData>(this) {
                                @Override
                                public void onNext(CatalogProductData catalogProductData) {
                                    super.onNext(catalogProductData);
                                    onResponseRecieved(catalogProductData);
                                }

                                @Override
                                public void onError(Throwable t) {
                                    super.onError(t);
                                    onRequestFailure();
                                }
                            });
                            break;
                        case "newProducts":
                            setTitle(getResources().getString(R.string.title_activity_new_products));
                            ApiConnection.getNewProductData(AppSharedPref.getStoreId(CatalogProductActivity.this)
                                    , Utils.getScreenWidth()
                                    , mPageNumber
                                    , AppSharedPref.getCustomerId(CatalogProductActivity.this)
                                    , AppSharedPref.getQuoteId(CatalogProductActivity.this)
                                    , mSortingInputJson
                                    , mFilterInputJson
                                    , AppSharedPref.getCurrencyCode(CatalogProductActivity.this))

                                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductData>(this) {
                                @Override
                                public void onNext(CatalogProductData catalogProductData) {
                                    super.onNext(catalogProductData);
                                    onResponseRecieved(catalogProductData);
                                }

                                @Override
                                public void onError(Throwable t) {
                                    super.onError(t);
                                    onRequestFailure();
                                }
                            });
                            break;
                        case "hotDeals":
                            setTitle(getResources().getString(R.string.title_activity_hot_deals));
                            ApiConnection.getHotDealsProductData(AppSharedPref.getStoreId(CatalogProductActivity.this)
                                    , Utils.getScreenWidth()
                                    , mPageNumber
                                    , AppSharedPref.getCustomerId(CatalogProductActivity.this)
                                    , AppSharedPref.getQuoteId(CatalogProductActivity.this)
                                    , mSortingInputJson
                                    , mFilterInputJson
                                    , AppSharedPref.getCurrencyCode(CatalogProductActivity.this))

                                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductData>(this) {
                                @Override
                                public void onNext(CatalogProductData catalogProductData) {
                                    super.onNext(catalogProductData);
                                    onResponseRecieved(catalogProductData);
                                }

                                @Override
                                public void onError(Throwable t) {
                                    super.onError(t);
                                    onRequestFailure();
                                }
                            });
                            break;
                    }
                }
                break;
        }
    }

    private void onRequestFailure() {
        mCatalogProductData.setLazyLoading(false);
    }

    public void onResponseRecieved(CatalogProductData catalogProductData) {
        if (isFirstCall && getIntent().hasExtra(KEY_CATEGORY_ID)) {
            String responseJSON = gson.toJson(catalogProductData);
            if (NetworkHelper.isNetworkAvailable(CatalogProductActivity.this)) {
                mOfflineDataBaseHandler.updateIntoOfflineDB(MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST, responseJSON, String.valueOf(getIntent().getExtras().getInt(KEY_CATEGORY_ID)));
            }
        }

        mPageNumber++;
        mCatalogProductData.setLazyLoading(false);
        dismiss(this);

        if (isFirstCall) {
            /*---->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UPDATING REQUEST TYPE AS THE OBJECT HAS BEEN CHANGED HERE <<<<<<<<<<<<<<<<<<<<<<--------------------*/
            catalogProductData.setRequestType(mCatalogProductData.getRequestType());
            catalogProductData.setShowBanner(mCatalogProductData.isShowBanner());
            mCatalogProductData = catalogProductData;
            mBinding.setData(mCatalogProductData);
            mBinding.executePendingBindings();
            if (mCatalogProductData.getCartCount() == 0)
                updateCartBadge(AppSharedPref.getCartCount(this, 0));
            else
                updateCartBadge(mCatalogProductData.getCartCount());

            if (mCatalogProductData.getBannerImage() != null && !mCatalogProductData.getBannerImage().isEmpty()) {
                mBinding.bannerImage.setVisibility(View.VISIBLE);
            }

            if (mFilterInputJson.length() == 0 && (mCatalogProductData.getCatalogProductCollection() == null || mCatalogProductData.getCatalogProductCollection().size() == 0)) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_product_catalog, getString(R.string.empty_product_catalog)
                        , getString(R.string.try_different_category_or_search_keyword_maybe))
                        , EmptyFragment.class.getSimpleName()).commit();
            } else {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(EmptyFragment.class.getSimpleName());
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                }
                mProductShortDatas = new ArrayList<>();
                mProductShortDatas.addAll(getProductShortData(catalogProductData.getCatalogProductCollection()));
                initProductCatalogRv();
            }
        } else {
            mCatalogProductData.getCatalogProductCollection().addAll(catalogProductData.getCatalogProductCollection());
            mCatalogProductListRvAdapter.notifyDataSetChanged();
            mProductShortDatas.addAll(getProductShortData(catalogProductData.getCatalogProductCollection()));
        }

        if (catalogProductData.getCriteriaData() != null && catalogProductData.getCriteriaData().size() > 0) {
            mCriteriaDataAdapter = new CriteriaDataAdapter(CatalogProductActivity.this, catalogProductData.getCriteriaData());
            mBinding.criteriaDataRv.setAdapter(mCriteriaDataAdapter);
        }
    }

    private void initProductCatalogRv() {
        if (mCatalogProductData.getCatalogProductCollection() != null) {

//            if (mCatalogProductData.getCatalogProductCollection().size() > 1){
//                DividerItemDecoration dividerItemDecorationVertical = new DividerItemDecoration(this, LinearLayout.VERTICAL);
//                mBinding.productCatalogRv.addItemDecoration(dividerItemDecorationVertical);
//            }
//            if (mCatalogProductData.getCatalogProductCollection().size() > 2){
//                DividerItemDecoration dividerItemDecorationHorizontal = new DividerItemDecoration(this, LinearLayout.HORIZONTAL);
//                mBinding.productCatalogRv.addItemDecoration(dividerItemDecorationHorizontal);
//            }


            mCatalogProductListRvAdapter = new CatalogProductListRvAdapter(this, mCatalogProductData.getCatalogProductCollection());
            mBinding.productCatalogRv.setAdapter(mCatalogProductListRvAdapter);

            if (mCatalogProductData.getCatalogProductCollection().size() <= 4) {
                mBinding.rvSpace.setVisibility(View.VISIBLE);
            } else {
                mBinding.rvSpace.setVisibility(View.GONE);
            }

            mBinding.productCatalogRv.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    mDyListener = dy;
                }

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    int lastCompletelyVisibleItemPosition;
                    if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                        lastCompletelyVisibleItemPosition = ((GridLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    } else {
                        lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    }

                    if (mDyListener > 0 && mCatalogProductData.getCatalogProductCollection().size() > 4) {
                        mBinding.footerContainer.animate().alpha(0f).translationY(mBinding.footerContainer.getHeight()).setInterpolator(new AccelerateInterpolator(1.4f));
                    } else {
                        mBinding.footerContainer.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
                    }

                    showPositionToast(lastCompletelyVisibleItemPosition);

                    if (NetworkHelper.isNetworkAvailable(CatalogProductActivity.this)) {
                        if (!mCatalogProductData.isLazyLoading() && mCatalogProductData.getCatalogProductCollection().size() != mCatalogProductData.getTotalCount()
                                && lastCompletelyVisibleItemPosition == (mCatalogProductData.getCatalogProductCollection().size() - 1) && lastCompletelyVisibleItemPosition < mCatalogProductData.getTotalCount()) {
                            callApi();
                            mCatalogProductData.setLazyLoading(true);
                            isFirstCall = false;
                        }
                    }
                }
            });
        }
    }

    private void showPositionToast(int lastCompletelyVisibleItemPosition) {
        String toastString = lastCompletelyVisibleItemPosition + 1 + " " + getString(R.string.of_toast_for_no_of_item) + " " + mCatalogProductData.getTotalCount();
        showToast(this, toastString, Toast.LENGTH_LONG, 0);
    }

    public ActivityProductCatalogBinding getBinding() {
        return mBinding;
    }

    public void onSortByItemSelected(JSONArray sortingInputJson) {
        mSortingInputJson = sortingInputJson;
        handleIntent();
    }

    public void onFilterByItemSelected(JSONArray filterInputJson) {
        mFilterInputJson = filterInputJson;
        handleIntent();
    }

    public JSONArray getSortingInputJson() {
        return mSortingInputJson;
    }

    public JSONArray getFilterInputJson() {
        return mFilterInputJson;
    }

    public int getFilterCategoryId() {
        return mCategoryId;
    }

    public void onFilterByCategoryItemSelected(int categoryId) {
        mCategoryId = categoryId;
        handleIntent();
    }

    @Override
    public void onBackPressed() {
        if (mBinding.searchView.isOpen()) {
            mBinding.searchView.closeSearch();
            Utils.hideKeyboard(this);
            if (mFilterInputJson.length() == 0 && (mCatalogProductData.getCatalogProductCollection() == null || mCatalogProductData.getCatalogProductCollection().size() == 0)) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_product_catalog
                        , getString(R.string.empty_product_catalog)
                        , getString(R.string.try_different_category_or_search_keyword_maybe))
                        , EmptyFragment.class.getSimpleName()).commit();
            }
            return;
        }

        /*For Deeplinking And Notification*/
        if (mOpenHome) {
            Intent intent = new Intent(this, ((MobikulApplication) getApplication()).getHomePageClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else
            super.onBackPressed();
    }

    @Override
    public void onResponse(Call<CatalogProductData> call, Response<CatalogProductData> response) {
        if (!NetworkHelper.isValidResponse(this, response, true)) {
            return;
        }
        onResponseRecieved(response.body());
    }

    @Override
    public void onFailure(Call<CatalogProductData> call, Throwable t) {
        mCatalogProductData.setLazyLoading(false);
        NetworkHelper.onFailure(t, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_CODE && resultCode == RESULT_OK) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            mBinding.searchView.setQuery(result.get(0), false);
        }
    }
}