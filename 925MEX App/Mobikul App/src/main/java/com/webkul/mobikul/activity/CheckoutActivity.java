package com.webkul.mobikul.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ActivityCheckoutBinding;
import com.webkul.mobikul.fragment.BillingShippingFragment;
import com.webkul.mobikul.handler.CheckoutActivityHandler;
import com.webkul.mobikul.model.checkout.CheckoutActivityData;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_APP_SIGN_IN;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_CHECK_LOCATION_SETTINGS;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_LOCATION_FETCH_ADDRESS_PERMISSION;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_IS_VIRTUAL;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;

public class CheckoutActivity extends BaseActivity implements FragmentManager.OnBackStackChangedListener {

    public ActivityCheckoutBinding mBinding;
    public boolean mIsVirtual = false;
    private CheckoutActivityData mCheckoutActivityData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_checkout);
        mCheckoutActivityData = new CheckoutActivityData();
        mBinding.setData(mCheckoutActivityData);
        showBackButton();
        setActionbarTitle(getString(R.string.title_activity_checkout));

        if (getIntent().hasExtra(BUNDLE_KEY_IS_VIRTUAL))
            mIsVirtual = getIntent().getBooleanExtra(BUNDLE_KEY_IS_VIRTUAL, false);

        mBinding.setIsVirtual(mIsVirtual);
        mBinding.setHandler(new CheckoutActivityHandler(getSupportFragmentManager(), mIsVirtual));

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, BillingShippingFragment.newInstance(), BillingShippingFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(BillingShippingFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onBackStackChanged() {
        switch (getSupportFragmentManager().getBackStackEntryCount()) {
            case 1:
                mCheckoutActivityData.setShippingMethodClickable(false);
                mCheckoutActivityData.setPaymentMethodClickable(false);
                mCheckoutActivityData.setOrderReviewClickable(false);
                break;

            case 2:
                if (mIsVirtual) {
                    mCheckoutActivityData.setShippingMethodClickable(false);
                    mCheckoutActivityData.setPaymentMethodClickable(true);
                    mCheckoutActivityData.setOrderReviewClickable(false);
                } else {
                    mCheckoutActivityData.setShippingMethodClickable(true);
                    mCheckoutActivityData.setPaymentMethodClickable(false);
                    mCheckoutActivityData.setOrderReviewClickable(false);
                }
                break;

            case 3:
                if (mIsVirtual) {
                    mCheckoutActivityData.setShippingMethodClickable(false);
                    mCheckoutActivityData.setPaymentMethodClickable(true);
                    mCheckoutActivityData.setOrderReviewClickable(true);
                } else {
                    mCheckoutActivityData.setShippingMethodClickable(true);
                    mCheckoutActivityData.setPaymentMethodClickable(true);
                    mCheckoutActivityData.setOrderReviewClickable(false);
                }
                break;
            case 4:
                mCheckoutActivityData.setShippingMethodClickable(true);
                mCheckoutActivityData.setPaymentMethodClickable(true);
                mCheckoutActivityData.setOrderReviewClickable(true);
                break;
        }
    }

    public ActivityCheckoutBinding getBinding() {
        return mBinding;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        BillingShippingFragment billingShippingFragment = (BillingShippingFragment) getSupportFragmentManager().findFragmentByTag(BillingShippingFragment.class.getSimpleName());
        if (requestCode == RC_LOCATION_FETCH_ADDRESS_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                billingShippingFragment.fetchCurrentAddress(billingShippingFragment.mUpdateShippingAddressFromGps);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        BillingShippingFragment billingShippingFragment = (BillingShippingFragment) getSupportFragmentManager().findFragmentByTag(BillingShippingFragment.class.getSimpleName());
        if (requestCode == RC_CHECK_LOCATION_SETTINGS) {
            if (resultCode == RESULT_OK) {
                showDefaultAlertDialog(billingShippingFragment.getContext());
                if (ActivityCompat.checkSelfPermission(billingShippingFragment.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(billingShippingFragment.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                billingShippingFragment.mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, billingShippingFragment);
            }
        } else if (requestCode == RC_APP_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack();
                fragmentManager.addOnBackStackChangedListener(this);
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.container, BillingShippingFragment.newInstance(), BillingShippingFragment.class.getSimpleName());
                fragmentTransaction.addToBackStack(BillingShippingFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
                fragmentTransaction.commit();
            }
        }
    }
}