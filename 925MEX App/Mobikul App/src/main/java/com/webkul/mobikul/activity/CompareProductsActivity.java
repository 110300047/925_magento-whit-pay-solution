package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.CompareProductsAttributesListRvAdapter;
import com.webkul.mobikul.adapter.CompareProductsListRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityCompareProductsBinding;
import com.webkul.mobikul.databinding.ItemCompareProductsAttributeBinding;
import com.webkul.mobikul.fragment.EmptyFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.compare.CompareListData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */
public class CompareProductsActivity extends BaseActivity {

    public CompareListData mCompareListData;
    public CompareProductsListRvAdapter mCompareProductsListRvAdapter;
    public ActivityCompareProductsBinding mBinding;
    private int mTouchedRvTag = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_compare_products);

        showBackButton();
        showHomeButton();
        setActionbarTitle(getResources().getString(R.string.title_activity_compare_products));
        startInitialization();
    }

    public void startInitialization() {
        ApiConnection.getCompareList(AppSharedPref.getStoreId(this)
                , AppSharedPref.getCustomerId(this)
                , Utils.getScreenWidth()
                , AppSharedPref.getCurrencyCode(this))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CompareListData>(this) {
            @Override
            public void onNext(CompareListData compareListData) {
                super.onNext(compareListData);
                onResponseRecieved(compareListData);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    private void onResponseRecieved(CompareListData compareListData) {
        mCompareListData = compareListData;

        if (mCompareListData.getProductList() != null && mCompareListData.getProductList().size() > 0) {
            createCompareProductView();
        } else {
            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_product_compare, getString(R.string.empty_compare_list)
                    , getString(R.string.add_item_to_your_compare_list_now)), EmptyFragment.class.getSimpleName());
            ft.commit();
        }
        mBinding.compareProductsProgressBar.setVisibility(View.GONE);
    }

    private void createCompareProductView() {
        mBinding.attributesContainer.removeAllViews();
        mCompareProductsListRvAdapter = new CompareProductsListRvAdapter(this, mCompareListData.getProductList());
        mBinding.productsRv.setAdapter(mCompareProductsListRvAdapter);
        if (mTouchedRvTag == -1) {
            mBinding.productsRv.setTag(0);
            mBinding.productsRv.addOnItemTouchListener(mRvOnTouchListener);
            mBinding.productsRv.addOnScrollListener(mRvOnScrollListener);
        }

        for (int noOfAttributes = 0; noOfAttributes < mCompareListData.getAttributeValueList().size(); noOfAttributes++) {
            ItemCompareProductsAttributeBinding itemCompareProductAttribute = ItemCompareProductsAttributeBinding.inflate(getLayoutInflater(), mBinding.attributesContainer, true);

            itemCompareProductAttribute.attributeName.setText(mCompareListData.getAttributeValueList().get(noOfAttributes).getAttributeName());

            CompareProductsAttributesListRvAdapter compareAttributesListRvAdapter = new CompareProductsAttributesListRvAdapter(this, mCompareListData.getAttributeValueList().get(noOfAttributes).getValue());
            itemCompareProductAttribute.attributeRv.setAdapter(compareAttributesListRvAdapter);
            itemCompareProductAttribute.attributeRv.setTag(noOfAttributes + 1);
            itemCompareProductAttribute.attributeRv.addOnItemTouchListener(mRvOnTouchListener);
            itemCompareProductAttribute.attributeRv.addOnScrollListener(mRvOnScrollListener);
        }
    }

    private RecyclerView.OnScrollListener mRvOnScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int currentRvTag = (int) recyclerView.getTag();
            if (currentRvTag == mTouchedRvTag) {
                for (int noOfAttributes = 0; noOfAttributes < mCompareListData.getAttributeValueList().size() + 1; noOfAttributes++) {
                    if (noOfAttributes != currentRvTag) {
                        RecyclerView tempRecyclerView = mBinding.container.findViewWithTag(noOfAttributes);
                        tempRecyclerView.scrollBy(dx, dy);
                    }
                }
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
    };

    private RecyclerView.OnItemTouchListener mRvOnTouchListener = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mTouchedRvTag = (int) rv.getTag();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    };
}