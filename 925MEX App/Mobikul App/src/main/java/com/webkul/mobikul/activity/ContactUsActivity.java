package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.ActivityContactUsBinding;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.BaseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */
public class ContactUsActivity extends BaseActivity {

    private ActivityContactUsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us);

        startInitialization();
    }

    private void startInitialization() {
        showBackButton();
        showHomeButton();
        setActionbarTitle(getResources().getString(R.string.title_activity_contact_us));

        if (AppSharedPref.isLoggedIn(this)) {
            mBinding.name.setText(AppSharedPref.getCustomerName(this));
            mBinding.email.setText(AppSharedPref.getCustomerEmail(this));
            mBinding.msg.requestFocus();
        }
    }

    public void onClickSubmitBtn(View view) {
        if (isFormValidated()) {
            showDefaultAlertDialog(this);

            ApiConnection.postContactUs(AppSharedPref.getStoreId(this)
                    , mBinding.name.getText().toString()
                    , mBinding.email.getText().toString()
                    , mBinding.phone.getText().toString()
                    , mBinding.msg.getText().toString())

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(this) {
                @Override
                public void onNext(BaseModel responseData) {
                    super.onNext(responseData);
                    showToast(ContactUsActivity.this, responseData.getMessage(), Toast.LENGTH_LONG, 0);
                    if (responseData.getSuccess())
                        finish();
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        }
    }

    private boolean isFormValidated() {
        boolean formValidated = true;
        if (mBinding.msg.getText().toString().trim().isEmpty()) {
            formValidated = false;
            mBinding.msg.requestFocus();
            mBinding.msgError.setText(getString(R.string.please_fill) + " " + getString(R.string.message));
        } else {
            mBinding.msgError.setText("");
        }
        if (!mBinding.phone.getText().toString().trim().isEmpty() && mBinding.phone.getText().toString().trim().length() != ApplicationConstant.NUMBER_OF_MOBILE_NUMBER_DIGIT) {
            formValidated = false;
            mBinding.phone.requestFocus();
            mBinding.phone.setError(getString(R.string.enter_a_valid_phone_number));
        } else {
            mBinding.phone.setError(null);
        }
        if (mBinding.email.getText().toString().trim().isEmpty() || !EMAIL_PATTERN.matcher(mBinding.email.getText().toString().trim()).matches()) {
            formValidated = false;
            mBinding.email.requestFocus();
            if (!EMAIL_PATTERN.matcher(mBinding.email.getText().toString().trim()).matches()) {
                mBinding.email.setError(getString(R.string.enter_valid_email));
            } else {
                mBinding.email.setError(getString(R.string.please_fill) + " " + getString(R.string.email_address));
            }
        } else {
            mBinding.email.setError(null);
        }
        if (mBinding.name.getText().toString().trim().isEmpty()) {
            formValidated = false;
            mBinding.name.requestFocus();
            mBinding.name.setError(getString(R.string.please_fill) + " " + getString(R.string.name_required));
        } else {
            mBinding.name.setError(null);
        }
        return formValidated;
    }
}