package com.webkul.mobikul.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.DashboardPagerAdapter;
import com.webkul.mobikul.connection.ApiInterface;
import com.webkul.mobikul.connection.RetrofitClient;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.ActivityCustomerDashboardBinding;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ImageHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.accountInfo.UploadPicResponseData;

import java.io.ByteArrayOutputStream;
import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.helper.AlertDialogHelper.dismiss;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_BANNER_IMAGE;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_PROFILE_IMAGE;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

public class CustomerDashboardActivity extends BaseActivity {

    public static final String UPLOAD_PROFILE_PIC_CONTROLLER = "mobikulhttp/index/uploadprofilepic/customerId/";
    public static final String UPLOAD_BANNER_IMAGE_CONTROLLER = "mobikulhttp/index/uploadbannerpic/customerId/";
    private static final int RC_SAVE_PROFILE_PICTURE = 1;
    private static final int REQUEST_CAMERA = 2;
    private ActivityCustomerDashboardBinding mBinding;
    private boolean mUpdateProfileImage;
    private Uri mFileUri;
    private ViewPager.OnPageChangeListener dashboardViewPagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    mBinding.tabs.getTabAt(0).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_address_book_wrapper_dark));
                    mBinding.tabs.getTabAt(1).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_order_wrapper));
                    mBinding.tabs.getTabAt(2).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_vector_review_wrapper));
                    break;
                case 1:
                    mBinding.tabs.getTabAt(0).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_address_book_wrapper));
                    mBinding.tabs.getTabAt(1).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_order_wrapper_dark));
                    mBinding.tabs.getTabAt(2).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_vector_review_wrapper));
                    break;
                case 2:
                    mBinding.tabs.getTabAt(0).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_address_book_wrapper));
                    mBinding.tabs.getTabAt(1).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_order_wrapper));
                    mBinding.tabs.getTabAt(2).setIcon(ContextCompat.getDrawable(CustomerDashboardActivity.this, R.drawable.ic_vector_review_wrapper_dark));
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_customer_dashboard);
        setSupportActionBar(mBinding.toolbar);
        showBackButton();

        updateProfilePic();
        updateBannerImage();

        mBinding.customerEmail.setText(AppSharedPref.getCustomerEmail(this));

        mBinding.viewPager.setAdapter(new DashboardPagerAdapter(getSupportFragmentManager(), this));
        mBinding.viewPager.setOffscreenPageLimit(2);
        mBinding.viewPager.addOnPageChangeListener(dashboardViewPagerOnPageChangeListener);
        mBinding.tabs.setupWithViewPager(mBinding.viewPager);
        setupTabIcons();

        mBinding.appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    mBinding.collapsingToolbar.setTitle(getResources().getString(R.string.customer_dashboard_activity_title));
                    isShow = true;
                } else if (isShow) {
                    mBinding.collapsingToolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }

    public void updateProfilePic() {
        try {
            ImageHelper.load(mBinding.profileImage,
                    AppSharedPref.getCustomerProfileImage(this, "").isEmpty() ? null : AppSharedPref.getCustomerProfileImage(this, null),
                    getResources().getDrawable(R.drawable.customer_profile_placeholder));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateBannerImage() {
        try {
            if (!AppSharedPref.getCustomerBannerImage(this, "").isEmpty()) {
                ImageHelper.load(mBinding.bannerImage,
                        AppSharedPref.getCustomerBannerImage(this, null),
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void setupTabIcons() {
        try {
            mBinding.tabs.getTabAt(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_address_book_wrapper_dark));
            mBinding.tabs.getTabAt(1).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_order_wrapper));
            mBinding.tabs.getTabAt(2).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_vector_review_wrapper));
        } catch (NullPointerException e) {
            Log.e(ApplicationConstant.TAG, "SETTING UP ICON FOR DASHBOARD TABS ");
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void onClickEditProfilePic(View view) {
        mUpdateProfileImage = true;
        selectImage();
    }

    public void onClickEditBannerImage(View view) {
        mUpdateProfileImage = false;
        selectImage();
    }

    public void selectImage() {
        final CharSequence[] items = {getString(R.string.choose_from_library), getString(R.string.camera_pick), getString(R.string.no)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (mUpdateProfileImage) {
            builder.setTitle(getString(R.string.add_profile_image));
        } else {
            builder.setTitle(getString(R.string.add_banner_image));
        }
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.choose_from_library))) {
                    if (ContextCompat.checkSelfPermission(CustomerDashboardActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_from_library)), RC_SAVE_PROFILE_PICTURE);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                            requestPermissions(permissions, RC_SAVE_PROFILE_PICTURE);
                        }
                    }
                } else if (items[item].equals(getString(R.string.camera_pick))) {
                    if (ContextCompat.checkSelfPermission(CustomerDashboardActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(CustomerDashboardActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
                        startActivityForResult(intent, REQUEST_CAMERA);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                            requestPermissions(permissions, REQUEST_CAMERA);
                        }
                    }
                } else if (items[item].equals(getString(R.string.no))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Uri getImageUri() {
        // Store image in dcim
        File imagesFolder = new File(Environment.getExternalStorageDirectory() + "/DCIM");
        if (imagesFolder.isDirectory() || imagesFolder.mkdir()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mFileUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".my.package.name.provider", new File(imagesFolder, "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            } else {
                mFileUri = Uri.fromFile(new File(imagesFolder, "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            }
        }
        return mFileUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RC_SAVE_PROFILE_PICTURE || requestCode == REQUEST_CAMERA) {
                CropImage.activity(data == null ? mFileUri : data.getData())
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(mUpdateProfileImage ? 1 : 3, mUpdateProfileImage ? 1 : 2)
                        .start(this);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                uploadPic(CropImage.getActivityResult(data).getUri());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
            }
        } else if (requestCode == RC_SAVE_PROFILE_PICTURE) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_from_library)), RC_SAVE_PROFILE_PICTURE);
        }
    }

    public void uploadPic(Uri data) {
        try {
            showDefaultAlertDialog(this);

            Bitmap bitmap = BitmapFactory.decodeFile(data.getPath());

            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), bos.toByteArray());
            RequestBody width = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Utils.getScreenWidth()));
            RequestBody mFactor = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getResources().getDisplayMetrics().density));

            if (mUpdateProfileImage) {
                Call<UploadPicResponseData> call = RetrofitClient.getClient().create(ApiInterface.class).uploadCustomerImagePic(
                        ApplicationConstant.BASE_URL + UPLOAD_PROFILE_PIC_CONTROLLER + AppSharedPref.getCustomerId(this)

                        , fileBody
                        , width
                        , mFactor);
                call.enqueue(new Callback<UploadPicResponseData>() {
                    @Override
                    public void onResponse(Call<UploadPicResponseData> call, Response<UploadPicResponseData> response) {
                        try {
                            dismiss(CustomerDashboardActivity.this);
                            showToast(CustomerDashboardActivity.this, getString(R.string.refreshing_profile_image), Toast.LENGTH_SHORT, 0);
                            SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(CustomerDashboardActivity.this, CUSTOMER_PREF);
                            customerDataSharedPref.putString(KEY_CUSTOMER_PROFILE_IMAGE, response.body().getUrl());
                            customerDataSharedPref.apply();
                            updateProfilePic();
                        } catch (Exception e) {
                            showToast(CustomerDashboardActivity.this, getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
                        }
                        mFileUri = null;
                    }

                    @Override
                    public void onFailure(Call<UploadPicResponseData> call, Throwable t) {
                        dismiss(CustomerDashboardActivity.this);
                        mFileUri = null;
                        showToast(CustomerDashboardActivity.this, getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
                    }
                });
            } else {
                Call<UploadPicResponseData> call = RetrofitClient.getClient().create(ApiInterface.class).uploadCustomerImagePic(
                        ApplicationConstant.BASE_URL + UPLOAD_BANNER_IMAGE_CONTROLLER + AppSharedPref.getCustomerId(this)

                        , fileBody
                        , width
                        , mFactor);
                call.enqueue(new Callback<UploadPicResponseData>() {
                    @Override
                    public void onResponse(Call<UploadPicResponseData> call, Response<UploadPicResponseData> response) {
                        try {
                            dismiss(CustomerDashboardActivity.this);
                            showToast(CustomerDashboardActivity.this, getString(R.string.refreshing_banner_image), Toast.LENGTH_SHORT, 0);
                            SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(CustomerDashboardActivity.this, CUSTOMER_PREF);
                            customerDataSharedPref.putString(KEY_CUSTOMER_BANNER_IMAGE, response.body().getUrl());
                            customerDataSharedPref.apply();
                            updateBannerImage();
                        } catch (Exception e) {
                            showToast(CustomerDashboardActivity.this, getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
                        }
                        mFileUri = null;
                    }

                    @Override
                    public void onFailure(Call<UploadPicResponseData> call, Throwable t) {
                        dismiss(CustomerDashboardActivity.this);
                        mFileUri = null;
                        showToast(CustomerDashboardActivity.this, getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
                    }
                });
            }
        } catch (Exception e) {
            dismiss(this);
            showToast(this, getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
            e.printStackTrace();
        }
    }
}