package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.webkul.mobikul.R;
import com.webkul.mobikul.customviews.TouchImageView;
import com.webkul.mobikul.databinding.ActivityFullScreenImageScrollBinding;
import com.webkul.mobikul.databinding.ItemProductSmallImageViewBinding;
import com.webkul.mobikul.helper.ImageHelper;

import java.util.ArrayList;
import java.util.List;

import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;

public class FullScreenImageScrollActivity extends BaseActivity {


    public static final String KEY_SMALL_IMAGE_GALLERY_DATA = "smallImageGalleryData";
    public static final String KEY_LARGE_IMAGE_GALLERY_DATA = "largeImageGalleryData";
    public static final String KEY_CURRENT_ITEM_POSITION = "position";
    private ActivityFullScreenImageScrollBinding mBinding;
    private ArrayList<String> largeImageList;

    /**
     * Step 1: Download and set up v4 support library: http://developer.android.com/tools/support-library/setup.html
     * Step 2: Create ExtendedViewPager wrapper which calls TouchImageView.canScrollHorizontallyFroyo
     * Step 3: ExtendedViewPager is a custom view and must be referred to by its full package name in XML
     * Step 4: Write TouchImageAdapter, located below
     * Step 5. The ViewPager in the XML should be ExtendedViewPager
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_full_screen_image_scroll);
        setTitle(getIntent().getStringExtra(KEY_PRODUCT_NAME));
        showBackButton();
        List<String> smallImageList = getIntent().getStringArrayListExtra(FullScreenImageScrollActivity.KEY_SMALL_IMAGE_GALLERY_DATA);
        largeImageList = getIntent().getStringArrayListExtra(FullScreenImageScrollActivity.KEY_LARGE_IMAGE_GALLERY_DATA);
        int currentPosition = getIntent().getIntExtra(FullScreenImageScrollActivity.KEY_CURRENT_ITEM_POSITION, 0);

        LayoutInflater inflater = LayoutInflater.from(this);

        for (int smallImageListIndex = 0; smallImageListIndex < smallImageList.size(); smallImageListIndex++) {
            String eachSmallProductImage = smallImageList.get(smallImageListIndex);
            ItemProductSmallImageViewBinding itemProductSmallImageViewBinding = DataBindingUtil.bind(inflater.inflate(R.layout.item_product_small_image_view, mBinding.smallImageContainer, false));
            itemProductSmallImageViewBinding.setData(eachSmallProductImage);
            itemProductSmallImageViewBinding.getRoot().setTag(smallImageListIndex);
            itemProductSmallImageViewBinding.executePendingBindings();
            mBinding.smallImageContainer.addView(itemProductSmallImageViewBinding.getRoot());
        }
        mBinding.fullScreenProductViewPager.setAdapter(new TouchImageAdapter());
        mBinding.fullScreenProductViewPager.setCurrentItem(currentPosition);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeProductsLargeImage(View v) {
        mBinding.fullScreenProductViewPager.setCurrentItem((int) v.getTag());
    }

    private class TouchImageAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return largeImageList.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            TouchImageView touchImageView = new TouchImageView(container.getContext(), mBinding.smallImageBtnHsv);
            try {
                ImageHelper.load(touchImageView, largeImageList.get(position), null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            container.addView(touchImageView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return touchImageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

}