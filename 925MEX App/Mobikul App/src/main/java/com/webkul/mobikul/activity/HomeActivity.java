package com.webkul.mobikul.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.FeaturedCategoriesRvAdapter;
import com.webkul.mobikul.adapter.FeaturedProductsRvAdapter;
import com.webkul.mobikul.adapter.HomeAdapterCategory;
import com.webkul.mobikul.adapter.HomeBannerAdapter;
import com.webkul.mobikul.adapter.HotDealsRvAdapter;
import com.webkul.mobikul.adapter.NavDrawerCategoryStartRvAdapter;
import com.webkul.mobikul.adapter.NavDrawerEndRvAdapter;
import com.webkul.mobikul.adapter.NavDrawerStartExtrasRvAdapter;
import com.webkul.mobikul.adapter.NavDrawerStartSearchRvAdapter;
import com.webkul.mobikul.adapter.NavDrawerStartStoreRvAdapter;
import com.webkul.mobikul.adapter.NewProductsRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.ActivityHomeBinding;
import com.webkul.mobikul.fragment.LoggedinLayoutFragment;
import com.webkul.mobikul.fragment.LoggedoutLayoutFragment;
import com.webkul.mobikul.handler.HomePageHandler;
import com.webkul.mobikul.handler.NavDrawerStartExtrasItemHandler;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.helper.ZoomOutPageTransformer;
import com.webkul.mobikul.model.catalog.HomePageResponseData;
import com.webkul.mobikul.model.catalog.NavDrawerCustomerItem;
import com.webkul.mobikul.model.catalog.NavDrawerStartExtrasItem;
import com.webkul.mobikul.model.catalog.SubCategory;
import com.webkul.mobikul.model.customer.NavDrawerEndData;
import com.webkul.mobikul.model.search.NavDrawerStartSearchMenuData;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.content.Intent.ACTION_SEARCH;
import static com.webkul.mobikul.connection.ApiInterface.MOBIKUL_CATALOG_HOME_PAGE_DATA;
import static com.webkul.mobikul.constants.ApplicationConstant.CAN_DISPLAY_CURRENCY;
import static com.webkul.mobikul.constants.ApplicationConstant.CAN_DISPLAY_STORES;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_BACK_PRESSED_TIME_TO_CLOSE;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_TIME_TO_SWITCH_BANNER_IN_MILIS;
import static com.webkul.mobikul.constants.ApplicationConstant.IS_MARKETPLACE_APP;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_APP_SIGN_IN;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ACTION;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_HOME_PAGE_RESPONSE_DATA;
import static com.webkul.mobikul.customviews.MaterialSearchView.RC_CODE;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AnimationHelper.ANIMATION_DURATION_SHORT;
import static com.webkul.mobikul.helper.AnimationHelper.ANIMATION_DURATION_SHORTEST;
import static com.webkul.mobikul.helper.ToastHelper.showToast;
import static com.webkul.mobikul.model.search.NavDrawerStartSearchMenuData.ADVANCE_SEARCH;
import static com.webkul.mobikul.model.search.NavDrawerStartSearchMenuData.SEARCH_TERMS;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */

public class HomeActivity extends BaseActivity {

    public ActivityHomeBinding mBinding;
    public HomePageResponseData mHomePageResponseDataObject;
    Timer mBannerSwitcherTimer;
    private ActionBarDrawerToggle mDrawerToggle;
    private long mBackPressedTime;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        mHomePageResponseDataObject = getIntent().getParcelableExtra(BUNDLE_KEY_HOME_PAGE_RESPONSE_DATA);

        mDrawerToggle = setupDrawerToggle(mBinding.toolbar);
        mBinding.drawerLayout.addDrawerListener(mDrawerToggle);

        mBinding.swipeRefresh.setDistanceToTriggerSync(400);
        mBinding.swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ApiConnection.getHomePageData(AppSharedPref.getStoreId(HomeActivity.this)
                                , AppSharedPref.getQuoteId(HomeActivity.this)
                                , AppSharedPref.getCustomerId(HomeActivity.this)
                                , Utils.getScreenWidth()
                                , ApplicationConstant.DEFAULT_WEBSITE_ID
                                , getResources().getDisplayMetrics().density
                                , 0
                                , ""
                                , AppSharedPref.getCurrencyCode(HomeActivity.this))

                                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<HomePageResponseData>(HomeActivity.this) {
                            @Override
                            public void onNext(HomePageResponseData homePageResponseData) {
                                super.onNext(homePageResponseData);
                                mHomePageResponseDataObject = homePageResponseData;
                                startInitialization();
                            }

                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                                mBinding.swipeRefresh.setRefreshing(false);
                            }
                        });
                        mBinding.swipeRefresh.setRefreshing(true);
                    }
                }
        );
        startInitialization();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(BUNDLE_KEY_ACTION) && intent.getStringExtra(BUNDLE_KEY_ACTION).equals(ACTION_SEARCH)) {
            mBinding.searchView.post(new Runnable() {
                @Override
                public void run() {
                    //create your anim here
                    mBinding.searchView.openSearch();
                }
            });
        }
    }

    public void startInitialization() {
        //Comprueba si se devolvioalgo el servidor
        if (mHomePageResponseDataObject == null) {
            if (getIntent().hasExtra(BUNDLE_KEY_ACTION) && getIntent().getStringExtra(BUNDLE_KEY_ACTION).equals(ACTION_SEARCH)) {
                mBinding.searchView.post(new Runnable() {
                    @Override
                    public void run() {
                        //create your anim here
                        mBinding.searchView.openSearch();
                    }
                });
            }

            showDefaultAlertDialog(this);

            ApiConnection.getHomePageData(AppSharedPref.getStoreId(this)
                    , AppSharedPref.getQuoteId(this)
                    , AppSharedPref.getCustomerId(this)
                    , Utils.getScreenWidth()
                    , ApplicationConstant.DEFAULT_WEBSITE_ID
                    , getResources().getDisplayMetrics().density
                    , 0
                    , ""
                    , AppSharedPref.getCurrencyCode(this))

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<HomePageResponseData>(this) {
                @Override
                public void onNext(HomePageResponseData homePageResponseData) {
                    super.onNext(homePageResponseData);
                    mHomePageResponseDataObject = homePageResponseData;
                    startInitialization();
                }

                @Override
                public void onError(Throwable t) {
                    final Cursor databaseCursor = mOfflineDataBaseHandler.selectFromOfflineDB(MOBIKUL_CATALOG_HOME_PAGE_DATA, null);
                    if (databaseCursor != null && databaseCursor.getCount() != 0) {
                        databaseCursor.moveToFirst();
                        mHomePageResponseDataObject = gson.fromJson(databaseCursor.getString(0), HomePageResponseData.class);
                        startInitialization();
                        AlertDialogHelper.dismiss(HomeActivity.this);
                    } else {
                        super.onError(t);
                    }
                }
            });
        } else {
            setSupportActionBar(mBinding.toolbar);
            showBackButton();
            showHomeButton();
            setActionbarTitle(getResources().getString(R.string.home_activity_title));

            AppSharedPref.setMobileLoginEnabled(HomeActivity.this, mHomePageResponseDataObject.isMobileLoginEnable());

            if (AppSharedPref.getCurrencyCode(this).isEmpty()) {
                AppSharedPref.setCurrencyCode(this, mHomePageResponseDataObject.getDefaultCurrency());
            }

            mBinding.setData(mHomePageResponseDataObject);
            mBinding.setHandler(new HomePageHandler());

            updateCartBadge(mHomePageResponseDataObject.getCartCount());

            if (this != null && !mBinding.swipeRefresh.isRefreshing()) {
                if (AppSharedPref.isLoggedIn(this)) {
                    LoggedinLayoutFragment myAccount = new LoggedinLayoutFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.login_layout, myAccount, LoggedinLayoutFragment.class.getSimpleName()).commitAllowingStateLoss();
                    loadNavigationViewEndRecyclerViewAdapter();
                } else {
                    Fragment login_fr = getSupportFragmentManager().findFragmentByTag(LoggedoutLayoutFragment.class.getSimpleName());
                    if (login_fr == null || !login_fr.isAdded()) {
                        LoggedoutLayoutFragment account = new LoggedoutLayoutFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.login_layout, account, LoggedoutLayoutFragment.class.getSimpleName()).commitAllowingStateLoss();
                    }
                }
            }


        /*Init Category NavView RV and Adapter*/ //Aqui empieza el menu despegable de lado izquierdo
            if (mHomePageResponseDataObject.getDefaultCategory() != null && mHomePageResponseDataObject.getDefaultCategory().getCategoriesData() != null) {
                prepareCategoriesData();
                //Log.d(ApplicationConstant.TAG,"categoriesdata: "+mHomePageResponseDataObject.getDefaultCategory().getCategoriesData()+" images: "+mHomePageResponseDataObject.getCategoryImages());
                mBinding.navDrawerStartCategoryRecyclerView.setAdapter(new NavDrawerCategoryStartRvAdapter(this, mHomePageResponseDataObject.getDefaultCategory().getCategoriesData(), mHomePageResponseDataObject.getCategoryImages()));
                mBinding.navDrawerStartCategoryRecyclerView.setNestedScrollingEnabled(false);
            }

        /*Init Search NavView RV and Adapter*/
            List<NavDrawerStartSearchMenuData> navDrawerSeachItems = new ArrayList<>();
            navDrawerSeachItems.add(new NavDrawerStartSearchMenuData(getString(R.string.search_terms), R.drawable.ic_vector_nav_search_terms, SEARCH_TERMS));
            navDrawerSeachItems.add(new NavDrawerStartSearchMenuData(getString(R.string.advance_search), R.drawable.ic_vector_nav_advance_serach, ADVANCE_SEARCH));
            mBinding.navDrawerStartSearchRecyclerView.setAdapter(new NavDrawerStartSearchRvAdapter(this, navDrawerSeachItems));
            mBinding.navDrawerStartSearchRecyclerView.setNestedScrollingEnabled(false);

        /*Init Currency NavView RV and Adapter*/
            if (CAN_DISPLAY_CURRENCY) {
                mBinding.currencyItem.setData(new NavDrawerStartExtrasItem(this, AppSharedPref.getCurrencyCode(this), NavDrawerStartExtrasItem.TYPE_CURRENCY, R.drawable.ic_vector_nav_currency));
                mBinding.currencyItem.setHandler(new NavDrawerStartExtrasItemHandler(this));
                mBinding.currencyItem.horizontalLine.setVisibility(View.GONE);
                if (mHomePageResponseDataObject.getAuthKey() != null) {
                    mBinding.navDrawerStartCurrencyRecyclerView.setAdapter(new NavDrawerStartCurrencyRvAdapter(this, mHomePageResponseDataObject.getAllowedCurrencies()));
                    mBinding.navDrawerStartCurrencyRecyclerView.setNestedScrollingEnabled(false);
                }
            }

        /*Init Store NavView RV and Adapter*/
            if (CAN_DISPLAY_STORES) {
                if (mHomePageResponseDataObject.getStoreData() != null && mHomePageResponseDataObject.getStoreData().get(0).getStores().size() > 1) {
                    mBinding.navDrawerStartStoreRecyclerView.setAdapter(new NavDrawerStartStoreRvAdapter(this, mHomePageResponseDataObject.getStoreData()));
                    mBinding.navDrawerStartStoreRecyclerView.setNestedScrollingEnabled(false);
                }
            }

        /*Init Extras NavView RV*/
            List<NavDrawerStartExtrasItem> navDrawerExtrasItems = new ArrayList<>();
            /*if (IS_MARKETPLACE_APP) {
                navDrawerExtrasItems.add(new NavDrawerStartExtrasItem(this, getString(R.string.marketplace), NavDrawerStartExtrasItem.TYPE_MARKETPLACE, R.drawable.ic_vector_nav_market));
            }*/
            navDrawerExtrasItems.add(new NavDrawerStartExtrasItem(this, getString(R.string.compare_products), NavDrawerStartExtrasItem.TYPE_COMPARE_PRODUCT, R.drawable.ic_vector_nav_compare_product));
            navDrawerExtrasItems.add(new NavDrawerStartExtrasItem(this, getString(R.string.contact_us), NavDrawerStartExtrasItem.TYPE_CONTACT_US, R.drawable.ic_vector_nav_email));
            /*if (!AppSharedPref.isLoggedIn(this)) {
                navDrawerExtrasItems.add(new NavDrawerStartExtrasItem(this, getString(R.string.orders_and_returns), NavDrawerStartExtrasItem.TYPE_ORDERS_AND_RETURNS, R.drawable.ic_vector_nav_orders_and_returns));
            }*/

            mBinding.navDrawerStartExtrasRecyclerView.setAdapter(new NavDrawerStartExtrasRvAdapter(this, navDrawerExtrasItems));
            mBinding.navDrawerStartExtrasRecyclerView.setNestedScrollingEnabled(false);

        /*Init CMS NavView RV and Adapter*/
            if (mHomePageResponseDataObject.getCmsData() != null && mHomePageResponseDataObject.getCmsData().size() > 0) {
                mBinding.navDrawerStartCmsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                mBinding.navDrawerStartCmsRecyclerView.setAdapter(new NavDrawerStartCompanyRvAdapter(this, mHomePageResponseDataObject.getCmsData()));
                mBinding.navDrawerStartCmsRecyclerView.setNestedScrollingEnabled(false);
            }

        /*Init Featured Category RV and Adapter
            if (mHomePageResponseDataObject.getFeaturedCategories() != null && mHomePageResponseDataObject.getFeaturedCategories().size() > 0) {
                mBinding.featuredCategoriesRv.setAdapter(new FeaturedCategoriesRvAdapter(this, mHomePageResponseDataObject.getFeaturedCategories()));
            }*/

        /*Loading Banner Data
            if (mHomePageResponseDataObject.getBannerImages() != null && mHomePageResponseDataObject.getBannerImages().size() > 0) {
                mBinding.bannerViewPager.setAdapter(new HomeBannerAdapter(this, mHomePageResponseDataObject.getBannerImages()));
                mBinding.bannerViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
                mBinding.bannerViewPager.setOffscreenPageLimit(2);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                    mBinding.bannerViewPager.setPageMargin(getResources().getDisplayMetrics().widthPixels / -8);
                mBinding.bannerDotsTabLayout.setupWithViewPager(mBinding.bannerViewPager, true);
                bannerPageSwitcher(mHomePageResponseDataObject.getBannerImages().size());
            }*/

        /*Init Featured Products RV and Adapter
            if (mHomePageResponseDataObject.getFeaturedProductDatas() != null && mHomePageResponseDataObject.getFeaturedProductDatas().size() > 0) {
                mBinding.featuredProductsRv.setAdapter(new FeaturedProductsRvAdapter(this, mHomePageResponseDataObject.getFeaturedProductDatas()));
            }*/

        /*Init New Products RV and Adapter
            if (mHomePageResponseDataObject.getNewProducts() != null && mHomePageResponseDataObject.getNewProducts().size() > 0) {
                mBinding.newProductsRv.setAdapter(new NewProductsRvAdapter(this, mHomePageResponseDataObject.getNewProducts()));
            }*/

        /*Init Hot Deals RV and Adapter
            if (mHomePageResponseDataObject.getHotDeals() != null && mHomePageResponseDataObject.getHotDeals().size() > 0) {
                mBinding.hotDealsRv.setAdapter(new HotDealsRvAdapter(this, mHomePageResponseDataObject.getHotDeals()));
                mBinding.hotDealsRv.setNestedScrollingEnabled(false);
            }*/

        /*Init main view category*/ //Aqui estan categorial principales del menu
            if (mHomePageResponseDataObject.getDefaultCategory() != null && mHomePageResponseDataObject.getDefaultCategory().getCategoriesData() != null) {
                Log.d(ApplicationConstant.TAG,"categoriesdata: "+mHomePageResponseDataObject.getDefaultCategory().getCategoriesData()+" images: "+mHomePageResponseDataObject.getCategoryImages());
                mBinding.recyclerCategory.setAdapter(new HomeAdapterCategory(this,mHomePageResponseDataObject.getDefaultCategory().getCategoriesData()));
                mBinding.navDrawerStartCategoryRecyclerView.setNestedScrollingEnabled(false);
            }

            mBinding.swipeRefresh.setRefreshing(false);
            mBinding.executePendingBindings();
        }
    }

    private void prepareCategoriesData() {
        for (int noOfCategoriesData = 0; noOfCategoriesData < mHomePageResponseDataObject.getDefaultCategory().getCategoriesData().size(); noOfCategoriesData++) {
            if (mHomePageResponseDataObject.getDefaultCategory().getCategoriesData().get(noOfCategoriesData).getChildren().size() > 0) {
                SubCategory subCategory = new SubCategory();
                subCategory.setCategoryId(mHomePageResponseDataObject.getDefaultCategory().getCategoriesData().get(noOfCategoriesData).getCategoryId());
                subCategory.setName(getString(R.string.all) + " " + mHomePageResponseDataObject.getDefaultCategory().getCategoriesData().get(noOfCategoriesData).getName());
                subCategory.setIsActive(String.valueOf(mHomePageResponseDataObject.getDefaultCategory().getCategoriesData().get(noOfCategoriesData).getIsActive()));
                subCategory.setPosition(String.valueOf(mHomePageResponseDataObject.getDefaultCategory().getCategoriesData().get(noOfCategoriesData).getPosition()));
                subCategory.setLevel(String.valueOf(mHomePageResponseDataObject.getDefaultCategory().getCategoriesData().get(noOfCategoriesData).getLevel()));
                subCategory.setChildren(new ArrayList<SubCategory>() {
                });
                mHomePageResponseDataObject.getDefaultCategory().getCategoriesData().get(noOfCategoriesData).getChildren().add(0, subCategory);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        mItemCart = menu.findItem(R.id.menu_item_cart);
        LayerDrawable icon = (LayerDrawable) mItemCart.getIcon();
        Utils.setBadgeCount(this, icon, AppSharedPref.getCartCount(this, 0));
        mItemCart.setVisible(AppSharedPref.isLoggedIn(getApplicationContext()));
        return true;
    }

    public void loadNavigationViewEndRecyclerViewAdapter() {
        /*Init Customer NavView RV END and Adapter*/
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mBinding.navDrawerEndRecyclerView.setLayoutManager(layoutManager);

        List<NavDrawerCustomerItem> navDrawerCustmerItems = new ArrayList<>();
        navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_customer_label), NavDrawerCustomerItem.TYPE_HEADER));
        navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.dashboard), NavDrawerCustomerItem.TYPE_DASHBOARD, R.drawable.ic_vector_dashboard_wrapper));
        navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.my_downloadable_products), NavDrawerCustomerItem.TYPE_MY_DOWNLOADABLE_PRODUCTS, R.drawable.ic_vector_downloadable_products_wrapper));
        navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.my_wishlist), NavDrawerCustomerItem.TYPE_WISHLIST, R.drawable.ic_vector_my_wishlist_wrapper));
        navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.account_info), NavDrawerCustomerItem.TYPE_ACCOUNT_INFO, R.drawable.ic_vector_account_infor_wrapper));
        navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.notifications), NavDrawerCustomerItem.TYPE_NOTIFICATION, R.drawable.ic_vector_notification_wrapper));
        navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.logout), NavDrawerCustomerItem.TYPE_SIGN_IN, R.drawable.ic_vector_sign_out_wrapper));
        mBinding.navDrawerEndRecyclerView.setAdapter(new NavDrawerEndRvAdapter(this, navDrawerCustmerItems));
        mBinding.navDrawerEndRecyclerView.setNestedScrollingEnabled(false);
    }

    public ActionBarDrawerToggle setupDrawerToggle(Toolbar toolbar) {
        return new ActionBarDrawerToggle(this, mBinding.drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else if (item.getItemId() == R.id.menu_item_customer) {
            mBinding.drawerLayout.openDrawer(GravityCompat.END);
        } else if (item.getItemId() == R.id.menu_item_home_search) {
            mBinding.searchView.openSearch();
        }
        return super.onOptionsItemSelected(item);
    }

    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE! Make sure to override the method with only a single `Bundle` argument
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /*public void bannerPageSwitcher(int bannerSize) {
        if (mBannerSwitcherTimer != null) {
            mBannerSwitcherTimer.cancel();
            mBannerSwitcherTimer.purge();
        }
        mBannerSwitcherTimer = new Timer();
        mBannerSwitcherTimer.scheduleAtFixedRate(new BannerSwitchTimerTask(bannerSize), 0, DEFAULT_TIME_TO_SWITCH_BANNER_IN_MILIS);
    }*/

    @Override
    public void onBackPressed() {
        if (mBinding.searchView.isOpen()) {
            mBinding.searchView.closeSearch();
            Utils.hideKeyboard(this);
        } else if (mBinding.drawerLayout != null && mBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else if (mBinding.drawerLayout != null && mBinding.drawerLayout.isDrawerOpen(GravityCompat.END)) {
            mBinding.drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            long time = System.currentTimeMillis();
            if (time - mBackPressedTime > DEFAULT_BACK_PRESSED_TIME_TO_CLOSE) {
                mBackPressedTime = time;
                showToast(this, getResources().getString(R.string.press_back_to_exit), Toast.LENGTH_SHORT, 0);
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppSharedPref.isLoggedIn(this)) {
            NavDrawerEndData mNavDrawerEndData = new NavDrawerEndData();
            mNavDrawerEndData.setCustomerName(AppSharedPref.getCustomerName(this));
            mNavDrawerEndData.setCustomerEmail(AppSharedPref.getCustomerEmail(this));
            LoggedinLayoutFragment loggedinLayoutFragment = (LoggedinLayoutFragment) getSupportFragmentManager().findFragmentByTag(LoggedinLayoutFragment.class.getSimpleName());
            LoggedoutLayoutFragment loggedoutLayoutFragment = (LoggedoutLayoutFragment) getSupportFragmentManager().findFragmentByTag(LoggedoutLayoutFragment.class.getSimpleName());
            if (loggedinLayoutFragment != null) {
                loggedinLayoutFragment.mFragmentLoginLayout.setData(mNavDrawerEndData);
                loggedinLayoutFragment.updateProfilePic();
                loggedinLayoutFragment.updateBannerImage();
            } else if (loggedoutLayoutFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(loggedoutLayoutFragment).commit();
                loggedinLayoutFragment = new LoggedinLayoutFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.login_layout, loggedinLayoutFragment, LoggedinLayoutFragment.class.getSimpleName()).commit();
            }
            loadNavigationViewEndRecyclerViewAdapter();
        }
    }

    public void onSearchOpened() {
        mBinding.swipeRefresh.setEnabled(false);
        mBinding.toolbar.setVisibility(View.GONE);
        mBinding.searchView.setVisibility(View.VISIBLE);
    }

    public void onSearchClosed() {
        long delayMillis;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            delayMillis = ANIMATION_DURATION_SHORT;
        } else {
            delayMillis = ANIMATION_DURATION_SHORTEST;
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBinding.toolbar.setVisibility(View.VISIBLE);
                mBinding.searchView.setVisibility(View.GONE);
                mBinding.swipeRefresh.setEnabled(true);
            }
        }, delayMillis);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_CODE) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                mBinding.searchView.setQuery(result.get(0), false);
            } else if (requestCode == RC_APP_SIGN_IN) {
                Intent intent = new Intent(this, ((MobikulApplication) getApplication()).getHomePageClass());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    }

    /*private class BannerSwitchTimerTask extends TimerTask {
        private final int mBannerSize;

        boolean firstTime = true;

        BannerSwitchTimerTask(int bannerSize) {
            mBannerSize = bannerSize;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (mBinding.bannerViewPager.getCurrentItem() == mBannerSize - 1) {
                        mBinding.bannerViewPager.setCurrentItem(0);
                    } else {
                        if (firstTime) {
                            mBinding.bannerViewPager.setCurrentItem(mBinding.bannerViewPager.getCurrentItem());
                            firstTime = false;
                        } else {
                            mBinding.bannerViewPager.setCurrentItem(mBinding.bannerViewPager.getCurrentItem() + 1);
                        }
                    }
                }
            });
        }
    }*/


}