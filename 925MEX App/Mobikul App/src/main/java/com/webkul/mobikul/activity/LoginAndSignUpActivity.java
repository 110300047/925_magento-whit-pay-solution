package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.webkul.mobikul.R;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.ActivitySignInOrSignUpBinding;
import com.webkul.mobikul.fragment.SignInFragment;
import com.webkul.mobikul.fragment.SignUpFragment;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.signin.LinkedinUserData;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormDataParcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;

public class LoginAndSignUpActivity extends BaseActivity {

    int mIsSocial = 0;
    String mLoginType = "";
    CreateAccountFormDataParcelable mSocialLoginData;
    GoogleSignInAccount mGoogleUserData;
    JSONObject mFacebookUserData;
    LinkedinUserData mLinkedInUserData;
    private int mSelectedPage = SIGN_IN_PAGE;
    private ActivitySignInOrSignUpBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in_or_sign_up);

        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                if (getIntent().hasExtra(BUNDLE_KEY_SELECT_PAGE)) {
                    mSelectedPage = extras.getInt(BUNDLE_KEY_SELECT_PAGE);
                }
                if (getIntent().hasExtra("isSocial")) {
                    mIsSocial = extras.getInt("isSocial");
                    if (getIntent().hasExtra("googleUserData")) {
                        mGoogleUserData = getIntent().getParcelableExtra("googleUserData");
                        mLoginType = "google";
                        createLoginData(mLoginType);
                    }
                    if (getIntent().hasExtra("linkedInUserData")) {
                        mLinkedInUserData = getIntent().getParcelableExtra("linkedInUserData");
                        mLoginType = "linkedIn";
                        createLoginData(mLoginType);
                    }
                    if (getIntent().hasExtra("facebookUserData")) {
                        mFacebookUserData = new JSONObject(getIntent().getStringExtra("facebookUserData"));
                        mLoginType = "facebook";
                        createLoginData(mLoginType);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
    }

    private void createLoginData(String mLoginType) {
        mSocialLoginData = new CreateAccountFormDataParcelable();
        switch (mLoginType) {
            case "facebook":
                try {
                    Log.d(ApplicationConstant.TAG, "createLoginData: facebook data cretaed at logina nd signup" + mFacebookUserData.getString("first_name"));
                    mSocialLoginData.setLoginFrom("facebook");
                    mSocialLoginData.setFirstName(mFacebookUserData.getString("first_name"));
                    mSocialLoginData.setLastName(mFacebookUserData.getString("last_name"));
                    mSocialLoginData.setEmailAddr(mFacebookUserData.getString("email"));
                    mSocialLoginData.setGender(mFacebookUserData.getString("gender"));
                    JSONObject Pic = mFacebookUserData.getJSONObject("picture");
                    JSONObject data = Pic.getJSONObject("data");
                    mSocialLoginData.setPictureURL(data.getString("url"));
                    mSocialLoginData.setPassword(Utils.generateRandomPassword());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case "google":
                mSocialLoginData.setLoginFrom("google");
                mSocialLoginData.setFirstName(mGoogleUserData.getDisplayName());
                mSocialLoginData.setLastName(mGoogleUserData.getFamilyName());
                mSocialLoginData.setEmailAddr(mGoogleUserData.getEmail());
                mSocialLoginData.setPictureURL(String.valueOf(mGoogleUserData.getPhotoUrl()));
                mSocialLoginData.setPassword(Utils.generateRandomPassword());
                break;
            case "linkedIn":
                mSocialLoginData.setLoginFrom("linkedIn");
                mSocialLoginData.setFirstName(mLinkedInUserData.getFirstName());
                mSocialLoginData.setLastName(mLinkedInUserData.getLastName());
                mSocialLoginData.setEmailAddr(mLinkedInUserData.getEmailAddress());
                mSocialLoginData.setPictureURL(mLinkedInUserData.getPictureUrl());
                mSocialLoginData.setPassword(Utils.generateRandomPassword());
                break;
            default:
                break;
        }
    }

    private void init() {
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN); // hide the keyboard on activity start
        setActionbarTitle(getResources().getString(R.string.login_or_sign_up_activity_title));
        setupViewPager();
    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SignInFragment(), getString(R.string.signin_action_title));
        adapter.addFragment(new SignUpFragment().newInstance(mIsSocial, mSocialLoginData), getString(R.string.signup_action_title));
        mBinding.viewpager.setAdapter(adapter);
        mBinding.viewpager.setCurrentItem(mSelectedPage);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }
}