package com.webkul.mobikul.activity;

import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.MyDownloadsRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityMyDownloadableProductsBinding;
import com.webkul.mobikul.fragment.EmptyFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.DownloadHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.model.customer.downloadable.DownloadableProductListData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.RC_WRITE_TO_EXTERNAL_STORAGE;
import static com.webkul.mobikul.handler.MyDownloadableListItemHandler.mFileName;
import static com.webkul.mobikul.handler.MyDownloadableListItemHandler.mMimeType;
import static com.webkul.mobikul.handler.MyDownloadableListItemHandler.mUrl;

public class MyDownloadableProductsActivity extends BaseActivity {

    private ActivityMyDownloadableProductsBinding mBinding;
    private int mPageNumber = 1;
    private boolean isFirstCall = true;
    private DownloadableProductListData mMyDownloadableProductResponseData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_downloadable_products);
        showBackButton();
        setActionbarTitle(getString(R.string.title_activity_my_downloadable_product));

        startInitialization();
    }

    private void startInitialization() {
        mBinding.setLazyLoading(true);

        ApiConnection.getMyDownloadsList(
                AppSharedPref.getCustomerId(this)
                , mPageNumber)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<DownloadableProductListData>(this) {
            @Override
            public void onNext(DownloadableProductListData downloadableProductListData) {
                super.onNext(downloadableProductListData);
                try {
                    if (isFirstCall) {
                        if (downloadableProductListData.getDownloadsList() == null || downloadableProductListData.getDownloadsList().size() == 0) {
                            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                            ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_downloadable_products, getString(R.string.empty_downloadable_product)
                                    , getString(R.string.start_shopping_and_download_your_products_from_here))
                                    , EmptyFragment.class.getSimpleName());
                            ft.commit();
                        } else {
                            mMyDownloadableProductResponseData = downloadableProductListData;
                            mBinding.myDownloadableRv.setLayoutManager(new LinearLayoutManager(MyDownloadableProductsActivity.this));
                            mBinding.myDownloadableRv.setAdapter(new MyDownloadsRvAdapter(MyDownloadableProductsActivity.this, mMyDownloadableProductResponseData.getDownloadsList()));
                            mBinding.myDownloadableRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                @Override
                                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                    super.onScrollStateChanged(recyclerView, newState);

                                    int lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

                                    if (NetworkHelper.isNetworkAvailable(MyDownloadableProductsActivity.this)) {
                                        if (!mBinding.getLazyLoading() && mMyDownloadableProductResponseData.getDownloadsList().size() != mMyDownloadableProductResponseData.getTotalCount()
                                                && lastCompletelyVisibleItemPosition == (mMyDownloadableProductResponseData.getDownloadsList().size() - 1) && lastCompletelyVisibleItemPosition < mMyDownloadableProductResponseData.getTotalCount()) {
                                            mPageNumber++;
                                            isFirstCall = false;
                                            startInitialization();
                                        }
                                    }
                                }
                            });
                            mBinding.executePendingBindings();
                        }
                    } else {
                        mMyDownloadableProductResponseData.getDownloadsList().addAll(downloadableProductListData.getDownloadsList());
                        mBinding.myDownloadableRv.getAdapter().notifyDataSetChanged();
                    }
                    mBinding.setLazyLoading(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mBinding.setLazyLoading(false);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RC_WRITE_TO_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                DownloadHelper.downloadFile(MyDownloadableProductsActivity.this, mUrl, mFileName, mMimeType);
            }
        }
    }
}
