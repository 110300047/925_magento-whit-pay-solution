package com.webkul.mobikul.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemCmsPageLinkBinding;
import com.webkul.mobikul.handler.NavDrawerStartCompanyRvHandler;
import com.webkul.mobikul.model.catalog.CMSPageData;

import java.util.List;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */
public class NavDrawerStartCompanyRvAdapter extends RecyclerView.Adapter<NavDrawerStartCompanyRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<CMSPageData> mCMSPageList;

    public NavDrawerStartCompanyRvAdapter(Context context, List<CMSPageData> cmsPageList) {
        mContext = context;
        mCMSPageList = cmsPageList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new ViewHolder(inflater.inflate(R.layout.item_cms_page_link, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final CMSPageData companyItem = mCMSPageList.get(position);
        holder.mBinding.setData(companyItem);
        holder.mBinding.setHandler(new NavDrawerStartCompanyRvHandler(mContext));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCMSPageList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemCmsPageLinkBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}