package com.webkul.mobikul.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemCurrencyBinding;
import com.webkul.mobikul.handler.NavDrawerStartCurrencyHandler;
import com.webkul.mobikul.helper.AppSharedPref;

import java.util.List;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */
public class NavDrawerStartCurrencyRvAdapter extends RecyclerView.Adapter<NavDrawerStartCurrencyRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<String> mAllowedCurrenciesList;

    public NavDrawerStartCurrencyRvAdapter(Context context, List<String> allowedCurrenciesList) {
        mContext = context;
        mAllowedCurrenciesList = allowedCurrenciesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new ViewHolder(inflater.inflate(R.layout.item_currency, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String currencyItem = mAllowedCurrenciesList.get(position);
        if (currencyItem.equals(AppSharedPref.getCurrencyCode(((BaseActivity) mContext).getApplication()))) {
            holder.mBinding.setIsSelected(true);
        } else {
            holder.mBinding.setIsSelected(false);
        }
        holder.mBinding.setData(currencyItem);
        holder.mBinding.setHandler(new NavDrawerStartCurrencyHandler());
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mAllowedCurrenciesList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemCurrencyBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}