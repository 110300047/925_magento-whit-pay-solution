package com.webkul.mobikul.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.NewAddressStreetAddressRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityNewAddressBinding;
import com.webkul.mobikul.handler.NewAddressActivityHandler;
import com.webkul.mobikul.helper.AddressHelper;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.model.customer.address.AddressFormResponseData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.RC_CHECK_LOCATION_SETTINGS;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_LOCATION_FETCH_ADDRESS_PERMISSION;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ACTIVITY_TITLE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ADDRESS_ID;
import static com.webkul.mobikul.helper.AlertDialogHelper.dismiss;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Pvt. Ltd
 */
public class NewAddressActivity extends BaseActivity implements LocationListener {

    private int mAddressId = 0;
    private ActivityNewAddressBinding mActivityNewAddress;
    private AddressFormResponseData mAddressFormResponseData;
    private LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityNewAddress = DataBindingUtil.setContentView(this, R.layout.activity_new_address);
        showBackButton();
        setTitle(getIntent().getStringExtra(BUNDLE_KEY_ACTIVITY_TITLE));

        mAddressId = getIntent().getIntExtra(BUNDLE_KEY_ADDRESS_ID, 0);

        showDefaultAlertDialog(this);

        ApiConnection.getAddressFormData(
                AppSharedPref.getCustomerId(NewAddressActivity.this)
                , AppSharedPref.getStoreId(NewAddressActivity.this)
                , mAddressId)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<AddressFormResponseData>(this) {
            @Override
            public void onNext(AddressFormResponseData addressFormResponseData) {
                super.onNext(addressFormResponseData);
                mAddressFormResponseData = addressFormResponseData;

                mAddressFormResponseData.initPrefixSelectedItemPosition();
                mAddressFormResponseData.initSuffixSelectedItemPosition();

                setUpPrefix();
                setUpSuffix();
                setUpCountrySpinner();

                if (mAddressId == 0) {
                    mAddressFormResponseData.getAddressData().setPrefix(addressFormResponseData.getPrefixValue());
                    mAddressFormResponseData.getAddressData().setFirstname(addressFormResponseData.getFirstName());
                    mAddressFormResponseData.getAddressData().setLastname(addressFormResponseData.getLastName());
                    mAddressFormResponseData.getAddressData().setSuffix(addressFormResponseData.getSuffixValue());
                }

                mActivityNewAddress.setData(mAddressFormResponseData);
                mActivityNewAddress.setHandler(new NewAddressActivityHandler(NewAddressActivity.this));
                mActivityNewAddress.executePendingBindings();

                loadStreetAddressRv();
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });

        mActivityNewAddress.container.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, final int scrollY, int oldScrollX, int oldScrollY) {
                if ((scrollY - oldScrollY) < 0 || (scrollY > (mActivityNewAddress.container.getChildAt(0).getHeight() - mActivityNewAddress.container.getHeight() - 100))) {
                    mActivityNewAddress.saveAddress.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
                } else {
                    mActivityNewAddress.saveAddress.animate().alpha(0f).translationY(mActivityNewAddress.saveAddress.getHeight()).setInterpolator(new AccelerateInterpolator(1.4f));
                }
            }
        });
    }

    public int getAddressId() {
        return mAddressId;
    }

    public ActivityNewAddressBinding getBinding() {
        return mActivityNewAddress;
    }

    private void setUpSuffix() {
        if (mAddressFormResponseData.getIsSuffixVisible() && mAddressFormResponseData.isSuffixHasOptions() && mAddressFormResponseData.getSuffixOptions().size() > 0) {
            List<String> suffixSpinnerData = new ArrayList<>();
            if (!mAddressFormResponseData.getIsSuffixRequired())
                suffixSpinnerData.add("");
            for (int suffixIterator = 0; suffixIterator < mAddressFormResponseData.getSuffixOptions().size(); suffixIterator++) {
                suffixSpinnerData.add(mAddressFormResponseData.getSuffixOptions().get(suffixIterator));
            }

            mActivityNewAddress.suffixSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                    , suffixSpinnerData
            ));
            mActivityNewAddress.suffixSpinner.setSelection(mAddressFormResponseData.getSuffixSelectedItemPosition());
            mActivityNewAddress.suffixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mAddressFormResponseData.setSuffixValue(parent.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void setUpPrefix() {
        if (mAddressFormResponseData.getIsPrefixVisible() && mAddressFormResponseData.isPrefixHasOptions() && mAddressFormResponseData.getPrefixOptions().size() > 0) {
            List<String> prefixSpinnerData = new ArrayList<>();
            if (!mAddressFormResponseData.getIsPrefixRequired())
                prefixSpinnerData.add("");
            for (int prefixIterator = 0; prefixIterator < mAddressFormResponseData.getPrefixOptions().size(); prefixIterator++) {
                prefixSpinnerData.add(mAddressFormResponseData.getPrefixOptions().get(prefixIterator));
            }

            mActivityNewAddress.prefixSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                    , prefixSpinnerData
            ));
            mActivityNewAddress.prefixSpinner.setSelection(mAddressFormResponseData.getPrefixSelectedItemPosition());
            mActivityNewAddress.prefixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mAddressFormResponseData.setPrefixValue(parent.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void loadStreetAddressRv() {
        mActivityNewAddress.streetAddressRv.setLayoutManager(new LinearLayoutManager(NewAddressActivity.this));
        mActivityNewAddress.streetAddressRv.setAdapter(new NewAddressStreetAddressRvAdapter(NewAddressActivity.this
                , mAddressFormResponseData.getAddressData().getStreet()
                , mAddressFormResponseData.getStreetLineCount()));
    }

    private void setUpCountrySpinner() {
        // creating a country name list and country name list
        mAddressFormResponseData.initCountryNameList();

        mActivityNewAddress.countrySpinner.setAdapter(new ArrayAdapter<>(NewAddressActivity.this, android.R.layout.simple_spinner_dropdown_item
                , mAddressFormResponseData.getCountryNameList()));
        mActivityNewAddress.countrySpinner.setSelection(mAddressFormResponseData.getSelectedCountryPositionFromAddressData());
        mActivityNewAddress.countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mAddressFormResponseData.setSelectedCountryPosition(position);
                if (mAddressFormResponseData.getCountryData().get(position).getStates() != null) {
                    mAddressFormResponseData.setHasStateData(true);
                    mActivityNewAddress.stateSpinner.setAdapter(new ArrayAdapter<>(NewAddressActivity.this, android.R.layout.simple_spinner_dropdown_item, mAddressFormResponseData.getStateNameList(position)));
                    mActivityNewAddress.stateSpinner.setSelection(mAddressFormResponseData.getSelectedStatePositionFromAddressData());
                    mActivityNewAddress.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mAddressFormResponseData.setSelectedStatePosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    mAddressFormResponseData.setHasStateData(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void fetchCurrentAddress() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showToast(this, getString(R.string.access_needed_to_fill_address), Toast.LENGTH_LONG, 0);
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_LOCATION_FETCH_ADDRESS_PERMISSION);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_LOCATION_FETCH_ADDRESS_PERMISSION);
            }
            return;
        }
        checkIsGpsEnabled();
    }

    private void checkIsGpsEnabled() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        mSweetAlertDialog = new SweetAlertDialog(NewAddressActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                        mSweetAlertDialog.setTitleText(NewAddressActivity.this.getString(R.string.please_wait));
                        mSweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorAccent));
                        mSweetAlertDialog.setCancelable(true);
                        mSweetAlertDialog.show();
                        if (ActivityCompat.checkSelfPermission(NewAddressActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(NewAddressActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, NewAddressActivity.this);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(NewAddressActivity.this, RC_CHECK_LOCATION_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        SnackbarHelper.getSnackbar(NewAddressActivity.this, getString(R.string.something_went_wrong)).show();
                        break;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mLocationManager != null && !mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            dismiss(this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        try {
            Log.d("location", latitude + "-------" + longitude);
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addresses != null) {
                List<String> street = new ArrayList<>();
                for (int addressLineIdx = 0; addressLineIdx < mAddressFormResponseData.getStreetLineCount(); addressLineIdx++) {
                    street.add(addresses.get(0).getAddressLine(addressLineIdx));
                }
                mAddressFormResponseData.getAddressData().setStreet(street);
                loadStreetAddressRv();
                mAddressFormResponseData.getAddressData().setRegion(addresses.get(0).getAdminArea());
                mAddressFormResponseData.getAddressData().setCity(addresses.get(0).getLocality());
                mAddressFormResponseData.getAddressData().setPostcode(addresses.get(0).getPostalCode());
                int newCountryPostion = AddressHelper.getCountryPositionFromCountryCode(mAddressFormResponseData.getCountryData()
                        , addresses.get(0).getCountryCode());
                if (newCountryPostion != 0) {
                    mActivityNewAddress.countrySpinner.setSelection(newCountryPostion);
                }
                mActivityNewAddress.setData(mAddressFormResponseData);
            } else {
                showToast(this, getString(R.string.unable_to_get_exact_address), Toast.LENGTH_SHORT, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showToast(this, getString(R.string.access_needed_to_fill_address), Toast.LENGTH_LONG, 0);
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_LOCATION_FETCH_ADDRESS_PERMISSION);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_LOCATION_FETCH_ADDRESS_PERMISSION);
            }
            return;
        }
        dismiss(this);
        mLocationManager.removeUpdates(this);
        mActivityNewAddress.getLocationBtn.setEnabled(false);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RC_LOCATION_FETCH_ADDRESS_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchCurrentAddress();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_CHECK_LOCATION_SETTINGS) {
                showDefaultAlertDialog(NewAddressActivity.this);
                if (ActivityCompat.checkSelfPermission(NewAddressActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(NewAddressActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, NewAddressActivity.this);
            }
        }
    }
}