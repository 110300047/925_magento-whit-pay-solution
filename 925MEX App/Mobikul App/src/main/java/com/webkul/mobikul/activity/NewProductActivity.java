package com.webkul.mobikul.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ActivityNewProductBinding;
import com.webkul.mobikul.fragment.ProductFragment;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.catalog.ProductShortData;

import java.util.ArrayList;
import java.util.List;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_EDIT_PRODUCT;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_FROM_NOTIFICATION;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_IS_FROM_URL;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ITEM_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_IMAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_NAME;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECTED_PRODUCT_NUMBER;

public class NewProductActivity extends BaseActivity {

    public ActivityNewProductBinding mBinding;
    public ArrayList<ProductShortData> mProductDatas;
    private boolean mOpenHome = false;
    private int mSelectedProduct = 0;
    private String mProductName = "";
    private String mProductImage = "";
    private int mProductId = 0;
    private int mItemId = 0;
    private boolean mIsEditProduct = false;
    private ViewPager.OnPageChangeListener productViewPagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            setActionbarTitle(mProductDatas.get(position).getName());
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_product);
        if (getIntent().hasExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST)) {
            mProductDatas = getIntent().getParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST);
            mSelectedProduct = getIntent().getIntExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, 0);
            mProductName = mProductDatas.get(mSelectedProduct).getName();
        } else {
            mProductId = getIntent().getIntExtra(BUNDLE_KEY_PRODUCT_ID, 0);
            mProductName = getIntent().getStringExtra(BUNDLE_KEY_PRODUCT_NAME);
            mProductImage = getIntent().getStringExtra(BUNDLE_KEY_PRODUCT_IMAGE);
            if (getIntent().hasExtra(BUNDLE_KEY_EDIT_PRODUCT)) {
                mIsEditProduct = getIntent().getBooleanExtra(BUNDLE_KEY_EDIT_PRODUCT, false);
                mItemId = getIntent().getIntExtra(BUNDLE_KEY_ITEM_ID, 0);
            }
        }

        gson = new Gson();

        setActionbarTitle(mProductName);

        /*For Deeplinking And Notification*/
        if (getIntent().hasExtra(BUNDLE_KEY_IS_FROM_URL) || getIntent().hasExtra(BUNDLE_KEY_FROM_NOTIFICATION))
            mOpenHome = true;

        setupViewPager();
    }

    protected void setupViewPager() {
        try {
            ProductViewPagerFragement productViewPagerFragement = new ProductViewPagerFragement(getSupportFragmentManager());
            if (mProductDatas != null) {
                for (int positionOfProduct = 0; positionOfProduct < mProductDatas.size(); positionOfProduct++) {
                    productViewPagerFragement.addFragment(ProductFragment.newInstance(mProductDatas.get(positionOfProduct).getEntityId(), mIsEditProduct, mItemId, mProductDatas.get(positionOfProduct).getName(), mProductDatas.get(positionOfProduct).getThumbNail()));
                }
            } else {
                productViewPagerFragement.addFragment(ProductFragment.newInstance(mProductId, mIsEditProduct, mItemId, mProductName, mProductImage));
            }
            mBinding.mainProductViewPager.setAdapter(productViewPagerFragement);
            mBinding.mainProductViewPager.setCurrentItem(mSelectedProduct);
            mBinding.mainProductViewPager.setOffscreenPageLimit(2);
            mBinding.mainProductViewPager.addOnPageChangeListener(productViewPagerOnPageChangeListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        /*For Deeplinking*/
        if (mOpenHome) {
            Intent intent = new Intent(this, ((MobikulApplication) getApplication()).getHomePageClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else
            super.onBackPressed();
    }

    protected class ProductViewPagerFragement extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        ProductViewPagerFragement(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }
}