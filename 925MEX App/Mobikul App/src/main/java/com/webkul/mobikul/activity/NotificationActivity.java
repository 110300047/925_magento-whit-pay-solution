package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.NotificationListRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityNotificationBinding;
import com.webkul.mobikul.fragment.EmptyFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.notification.NotificationResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */

//@Shortcut(id = "notification", icon = R.drawable.ic_vector_notification_item_bell, shortLabelRes = R.string.title_activity_notification, rank = 1, backStack = {HomeActivity.class})
public class NotificationActivity extends BaseActivity {

    private ActivityNotificationBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        mBinding.setIsLoading(true);
        startInitialization();
    }

    private void startInitialization() {
        showBackButton();
        showHomeButton();
        setActionbarTitle(getResources().getString(R.string.title_activity_notification));

        ApiConnection.getNotificationsList(AppSharedPref.getStoreId(this)
                , Utils.getScreenWidth()
                , getResources().getDisplayMetrics().density)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<NotificationResponseData>(this) {
            @Override
            public void onNext(NotificationResponseData notificationResponseData) {
                super.onNext(notificationResponseData);
                if (notificationResponseData.getSuccess()) {
                    mBinding.setIsLoading(false);
                    if (notificationResponseData.getNotificationList().size() > 0) {
                        mBinding.notificationListRv.setLayoutManager(new LinearLayoutManager(NotificationActivity.this));
                        NotificationListRvAdapter notificationListRvAdapter = new NotificationListRvAdapter(NotificationActivity.this, notificationResponseData.getNotificationList());
                        mBinding.notificationListRv.setAdapter(notificationListRvAdapter);
                        mBinding.notificationListRv.setNestedScrollingEnabled(false);
                    } else {
                        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_notification, getString(R.string.empty_notification)
                                , getString(R.string.visit_later_to_check_your_notification))
                                , EmptyFragment.class.getSimpleName());
                        ft.commit();
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mBinding.setIsLoading(false);
            }
        });
    }
}