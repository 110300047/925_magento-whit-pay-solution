package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.OrderDetailProductInfoRecyclerViewAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityOrderDetailBinding;
import com.webkul.mobikul.handler.OrderDetailActivityHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.customer.order.OrderDetailResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;

public class OrderDetailActivity extends BaseActivity {

    private ActivityOrderDetailBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail);

        showBackButton();
        setActionbarTitle(getResources().getString(R.string.my_order_detail_activity_title));

        String mIncrementId = getIntent().getStringExtra(BUNDLE_KEY_INCREMENT_ID);
        if (mIncrementId.equals("")) {
            setTitle(String.format(getString(R.string.title_activity_order_noX), mIncrementId));
        }
        ApiConnection.getOrderDetailsData(mIncrementId
                , AppSharedPref.getStoreId(OrderDetailActivity.this)
                , AppSharedPref.getCurrencyCode(this))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<OrderDetailResponseData>(this) {
            @Override
            public void onNext(OrderDetailResponseData orderDetailResponseData) {
                super.onNext(orderDetailResponseData);
                onResponseRecieved(orderDetailResponseData);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    private void onResponseRecieved(OrderDetailResponseData orderDetailResponseData) {
        mBinding.orderDetailsProgressBar.setVisibility(View.GONE);
        mBinding.setData(orderDetailResponseData);
        mBinding.setHandler(new OrderDetailActivityHandler(OrderDetailActivity.this));
        mBinding.productInfoRv.setAdapter(new OrderDetailProductInfoRecyclerViewAdapter(OrderDetailActivity.this, orderDetailResponseData.getOrderData().getItems()));
        for (int noOfTotalItems = 0; noOfTotalItems < orderDetailResponseData.getOrderData().getTotals().size(); noOfTotalItems++) {
            switch (orderDetailResponseData.getOrderData().getTotals().get(noOfTotalItems).getCode()) {
                case "subtotal":
                    mBinding.setSubtotalData(orderDetailResponseData.getOrderData().getTotals().get(noOfTotalItems));
                    break;
                case "shipping":
                    mBinding.setShippingData(orderDetailResponseData.getOrderData().getTotals().get(noOfTotalItems));
                    break;
                case "discount":
                    mBinding.setDiscountData(orderDetailResponseData.getOrderData().getTotals().get(noOfTotalItems));
                    break;
                case "grand_total":
                    mBinding.setGrandTotalData(orderDetailResponseData.getOrderData().getTotals().get(noOfTotalItems));
                    break;
                case "tax":
                    mBinding.setTaxData(orderDetailResponseData.getOrderData().getTotals().get(noOfTotalItems));
                    break;
                case "base_grandtotal":
                    mBinding.setBaseGrandTotalData(orderDetailResponseData.getOrderData().getTotals().get(noOfTotalItems));
                    break;
            }
        }
        mBinding.executePendingBindings();
    }
}