package com.webkul.mobikul.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ActivityOrderPlacedBinding;
import com.webkul.mobikul.handler.OrderPlaceActivityHandler;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.checkout.SaveOrderResponse;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SAVE_ORDER_RESPONSE;

public class OrderPlacedActivity extends BaseActivity {

    public ActivityOrderPlacedBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_placed);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        showBackButton();
        setActionbarTitle(getString(R.string.title_activity_order_placed));

        SaveOrderResponse saveOrderResponseData = getIntent().getParcelableExtra(BUNDLE_KEY_SAVE_ORDER_RESPONSE);
        mBinding.setData(saveOrderResponseData);
        mBinding.setHandler(new OrderPlaceActivityHandler());
        mBinding.executePendingBindings();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ((MobikulApplication) getApplication()).getHomePageClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}