package com.webkul.mobikul.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityOrdersAndReturnsBinding;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.BaseModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */
public class OrdersAndReturnsActivity extends BaseActivity {

    private ActivityOrdersAndReturnsBinding mBinding;

    private String mOrderType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_orders_and_returns);

        startInitialization();
    }

    private void startInitialization() {
        showBackButton();
        showHomeButton();
        setActionbarTitle(getResources().getString(R.string.title_activity_orders_and_returns));

        setupOrderBySpinner();
    }

    private void setupOrderBySpinner() {
        List<String> orderBySpinnerData = new ArrayList<>();
        orderBySpinnerData.add(getString(R.string.email));
        orderBySpinnerData.add(getString(R.string.zip_code));

        mBinding.orderBySpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                , orderBySpinnerData
        ));
        mBinding.orderBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mOrderType = "email";
                    mBinding.emailTv.setVisibility(View.VISIBLE);
                    mBinding.email.setVisibility(View.VISIBLE);
                    mBinding.zipTv.setVisibility(View.GONE);
                    mBinding.zip.setVisibility(View.GONE);
                } else {
                    mOrderType = "zip";
                    mBinding.emailTv.setVisibility(View.GONE);
                    mBinding.email.setVisibility(View.GONE);
                    mBinding.zipTv.setVisibility(View.VISIBLE);
                    mBinding.zip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onClickSubmitBtn(View view) {
        if (isFormValidated()) {
            showDefaultAlertDialog(this);

            ApiConnection.getGuestOrderDetails(AppSharedPref.getStoreId(this)
                    , mOrderType
                    , mBinding.orderId.getText().toString()
                    , mBinding.lastName.getText().toString()
                    , mOrderType.equals("email") ? mBinding.email.getText().toString() : ""
                    , mOrderType.equals("zip") ? mBinding.zip.getText().toString() : "")

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(this) {
                @Override
                public void onNext(BaseModel responseData) {
                    super.onNext(responseData);
                    if (responseData.getSuccess()) {
                        Intent intent = new Intent(OrdersAndReturnsActivity.this, OrderDetailActivity.class);
                        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, mBinding.orderId.getText().toString());
                        startActivity(intent);
                    } else {
                        showToast(OrdersAndReturnsActivity.this, responseData.getMessage(), Toast.LENGTH_LONG, 0);
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        }
    }

    private boolean isFormValidated() {
        boolean formValidated = true;
        if (mOrderType.equals("zip")) {
            if (mBinding.zip.getText().toString().trim().isEmpty()) {
                formValidated = false;
                mBinding.zip.requestFocus();
                mBinding.zip.setError(getString(R.string.please_fill) + " " + getString(R.string.billing_zip_code_required));
            } else {
                mBinding.zip.setError(null);
            }
        } else {
            if (mBinding.email.getText().toString().trim().isEmpty() || !EMAIL_PATTERN.matcher(mBinding.email.getText().toString().trim()).matches()) {
                formValidated = false;
                mBinding.email.requestFocus();
                mBinding.email.setError(getString(R.string.enter_valid_email));
            } else {
                mBinding.email.setError(null);
            }
        }

        if (mBinding.lastName.getText().toString().trim().isEmpty()) {
            formValidated = false;
            mBinding.lastName.requestFocus();
            mBinding.lastName.setError(getString(R.string.please_fill) + " " + getString(R.string.billing_last_name_required));
        } else {
            mBinding.lastName.setError(null);
        }
        if (mBinding.orderId.getText().toString().trim().isEmpty()) {
            formValidated = false;
            mBinding.orderId.requestFocus();
            mBinding.orderId.setError(getString(R.string.please_fill) + " " + getString(R.string.order_id_required));
        } else {
            mBinding.orderId.setError(null);
        }
        return formValidated;
    }
}