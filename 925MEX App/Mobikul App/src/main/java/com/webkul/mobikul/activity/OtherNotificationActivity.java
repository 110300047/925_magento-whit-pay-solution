package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.webkit.WebSettings;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityOtherNotificationBinding;
import com.webkul.mobikul.model.notification.OtherNotificationResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_NOTIFICATION_ID;

public class OtherNotificationActivity extends BaseActivity {

    private ActivityOtherNotificationBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_notification);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_other_notification);

        startInitialization();
    }

    private void startInitialization() {
        showBackButton();
        showHomeButton();
        setActionbarTitle(getResources().getString(R.string.title_activity_other_notification));

        String notificationId = getIntent().getStringExtra(BUNDLE_KEY_NOTIFICATION_ID);

        WebSettings webSettings = mBinding.detailsWebView.getSettings();
        webSettings.setDefaultFontSize(14);

        ApiConnection.getOtherNotificationData(
                notificationId)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<OtherNotificationResponseData>(this) {
            @Override
            public void onNext(OtherNotificationResponseData otherNotificationResponseData) {
                super.onNext(otherNotificationResponseData);
                if (otherNotificationResponseData.isSuccess()) {
                    mBinding.setData(otherNotificationResponseData);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }
}