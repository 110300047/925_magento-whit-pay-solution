package com.webkul.mobikul.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.fragment.OrderReviewFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.checkout.OrderReviewRequestData;

import org.json.JSONException;

import java.math.BigDecimal;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import mx.openpay.android.PayPal;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CAN_REORDER;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.helper.ToastHelper.showToast;
import static java.security.AccessController.getContext;

public class PayPalActivity extends FragmentActivity {

    private Button btnPay;
    private TextView amount;

    public static final int PAYPAL_REQUEST_CODE = 7171;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PayPal.PAYPAL_CLIENT_ID);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paypal_main);

        //Servicio de paypal
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config);
        startService(intent);

        btnPay = (Button)findViewById(R.id.btnPay);
        amount = (TextView) findViewById(R.id.Amount);

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processPayment();
            }
        });
    }



    @Override
    protected void onDestroy(){
        stopService(new Intent(this,PayPalService.class));
        super.onDestroy();
    }

    private void processPayment(){

        PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(String.valueOf(10))
                ,"USD"
                ,"Puerto Morelos"
                ,PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payPalPayment);
        startActivityForResult(intent,PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data){
        if (requestCode == PAYPAL_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                /*PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirmation !=  null){
                    ApiConnection.getOrderReviewData(
                            AppSharedPref.getStoreId(this)
                            , AppSharedPref.getQuoteId(this)
                            , AppSharedPref.getCustomerId(this)
                            ,"PayPal"
                            , AppSharedPref.getCurrencyCode(this))
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<OrderReviewRequestData>(this) {
                        @Override
                        public void onNext(OrderReviewRequestData orderReviewRequestData) {
                            super.onNext(orderReviewRequestData);
                            if (orderReviewRequestData.isSuccess()) {
                                FragmentManager fragmentManager = new CheckoutActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.add(R.id.container, OrderReviewFragment.newInstance("PayPal", orderReviewRequestData), OrderReviewFragment.class.getSimpleName());
                                fragmentTransaction.addToBackStack(OrderReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
                                fragmentTransaction.commit();
                            } /*else {
                                showToast(getContext(), orderReviewRequestData.getMessage(), Toast.LENGTH_LONG, 0);
                                if (orderReviewRequestData.getCartCount() == 0) {
                                    CheckoutActivity.finish();
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            Log.d("mensaje", String.valueOf(t));
                        }
                    });
                }*/
            }
        }
        else if(resultCode == Activity.RESULT_CANCELED){
            Toast.makeText(this, "Cancel",Toast.LENGTH_LONG).show();
        }else if(resultCode ==  PaymentActivity.RESULT_EXTRAS_INVALID){
            Toast.makeText(this, "invalid",Toast.LENGTH_LONG).show();
        }
    }
}
