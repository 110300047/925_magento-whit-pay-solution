package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.ReviewDetailRatingDataRecyclerViewAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityReviewBinding;
import com.webkul.mobikul.handler.CustomerReviewActivityHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.review.ReviewDetailsResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_REVIEW_ID;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED;

public class ReviewActivity extends BaseActivity {

    private ActivityReviewBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_review);
        setActionbarTitle(getResources().getString(R.string.my_review_activity_title));
        callApi();
    }

    private void callApi() {
        ApiConnection.getReviewDetails(AppSharedPref.getStoreId(this)
                , Utils.getScreenWidth()
                , getIntent().getIntExtra(BUNDLE_KEY_REVIEW_ID, 0))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ReviewDetailsResponseData>(this) {
            @Override
            public void onNext(ReviewDetailsResponseData reviewDetailsResponseData) {
                super.onNext(reviewDetailsResponseData);
                mBinding.setData(reviewDetailsResponseData);
                mBinding.setHandler(new CustomerReviewActivityHandler());
                mBinding.executePendingBindings();
                mBinding.ratingDataRv.setLayoutManager(new LinearLayoutManager(ReviewActivity.this));
                mBinding.ratingDataRv.setAdapter(new ReviewDetailRatingDataRecyclerViewAdapter(ReviewActivity.this, reviewDetailsResponseData.getRatingData()));
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }
}