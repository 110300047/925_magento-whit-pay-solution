package com.webkul.mobikul.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.SearchTermsRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivitySearchTermsBinding;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.search.SearchTermsResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */
public class SearchTermsActivity extends BaseActivity {

    private ActivitySearchTermsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_terms);
        mBinding.setIsListEmpty(false);
        mBinding.setIsLoading(true);
        startInitialization();
    }

    private void startInitialization() {
        showBackButton();
        showHomeButton();
        setActionbarTitle(getResources().getString(R.string.title_activity_search_terms));

        ApiConnection.getSearchTermsList(
                AppSharedPref.getStoreId(this))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<SearchTermsResponseData>(this) {
            @Override
            public void onNext(SearchTermsResponseData searchTermsResponseData) {
                super.onNext(searchTermsResponseData);
                mBinding.setIsLoading(false);
                if (searchTermsResponseData.isSuccess() && searchTermsResponseData.getTermList().size() != 0) {
                    mBinding.searchTermsRv.setLayoutManager(new LinearLayoutManager(SearchTermsActivity.this));
                    mBinding.searchTermsRv.setAdapter(new SearchTermsRvAdapter(SearchTermsActivity.this, searchTermsResponseData.getTermList()));
                    mBinding.searchTermsRv.setNestedScrollingEnabled(false);
                } else {
                    mBinding.setIsListEmpty(true);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mBinding.setIsLoading(false);
            }
        });
    }
}