package com.webkul.mobikul.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.webkul.mobikul.BuildConfig;
import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.ActivitySplashScreenBinding;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.DataBaseHelper;
import com.webkul.mobikul.helper.LocaleUtils;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.SyncCartDbWithServer;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.helper.VersionChecker;
import com.webkul.mobikul.model.catalog.HomePageResponseData;
import com.webkul.mobikul.model.product.PriceFormatData;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.connection.ApiInterface.MOBIKUL_CATALOG_HOME_PAGE_DATA;
import static com.webkul.mobikul.constants.ApplicationConstant.APP_PLAYSTORE_URL;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_BACK_PRESSED_TIME_TO_CLOSE;
import static com.webkul.mobikul.constants.ApplicationConstant.HOST_NAME;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CAN_REORDER;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_FROM_NOTIFICATION;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_HOME_PAGE_RESPONSE_DATA;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_IS_FROM_URL;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_NOTIFICATION_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_NOTIFICATION_TYPE;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRICE_PATTERN;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRICE_PRECISION;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;
import static com.webkul.mobikul.helper.AppSharedPref.PRICE_FORMAT_PREF;
import static com.webkul.mobikul.helper.ToastHelper.showToast;
import static java.lang.Integer.parseInt;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */

public class SplashScreenActivity extends BaseActivity {

    protected int RC_UPDATE_APP_FROM_PLAYSTORE = 1;

    private ActivitySplashScreenBinding mBinding;
    private String mLatestVersionName;
    private HomePageResponseData homePageResponseDataObject;
    private int mIsFromUrl = 0;
    private String mDeepLinkingUrl = "";
    private long mBackPressedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalyticsImpl.logAppOpenEvent(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        onNewIntent(getIntent());
    }

    protected void onNewIntent(Intent intent) {
        String action = intent.getAction();
        String data = intent.getDataString();

        if (data == null && intent.hasExtra("message")) {
            Intent newIntent = null;

            String notificationId = "0";
            String notificationType = intent.getStringExtra(BUNDLE_KEY_NOTIFICATION_TYPE);

            switch (notificationType) {
                case "order":
                    newIntent = new Intent(this, OrderDetailActivity.class);
                    newIntent.putExtra(BUNDLE_KEY_INCREMENT_ID, intent.getStringExtra("incrementId"));
                    newIntent.putExtra(BUNDLE_KEY_CAN_REORDER, intent.getStringExtra("canReorder"));
                    break;
                case "sellerOrder":
                    newIntent = new Intent(this, ((MobikulApplication) getApplicationContext()).getSellerOrderDetailsActivity());
                    newIntent.putExtra(BUNDLE_KEY_INCREMENT_ID, intent.getStringExtra("incrementId"));
                    break;
                case "productApproval":
                    newIntent = new Intent(this, NewProductActivity.class);
                    newIntent.putExtra(KEY_PRODUCT_ID, parseInt(intent.getStringExtra("productId")));
                    newIntent.putExtra(KEY_PRODUCT_NAME, intent.getStringExtra("productName"));
                    break;
                case "sellerApproval":
                    if (AppSharedPref.isLoggedIn(this) && AppSharedPref.getCustomerId(this) == parseInt(intent.getStringExtra("sellerId"))) {
                        intent = new Intent(this, ((MobikulApplication) getApplicationContext()).getSellerDashBoardActivity());
                    }
                    break;
            }

            if (intent.hasExtra("store_id")) {
                notificationId = intent.getStringExtra("id");
                String storeIdString = intent.getStringExtra("store_id");
                String[] storeIdsArray = storeIdString.split(",");
                if (Arrays.asList(storeIdsArray).contains("0") || Arrays.asList(storeIdsArray).contains(String.valueOf(AppSharedPref.getStoreId(this)))) {
                    switch (notificationType) {
                        case "product":
                            newIntent = new Intent(this, NewProductActivity.class);
                            newIntent.putExtra(KEY_PRODUCT_ID, parseInt(intent.getStringExtra("productId")));
                            newIntent.putExtra(KEY_PRODUCT_NAME, intent.getStringExtra("productName"));
                            break;
                        case "category":
                            newIntent = new Intent(this, ((MobikulApplication) getApplication()).getCatalogProductPageClass());
                            newIntent.putExtra(KEY_CATEGORY_ID, parseInt(intent.getStringExtra("categoryId")));
                            newIntent.putExtra(KEY_CATEGORY_NAME, intent.getStringExtra("categoryName"));
                            break;
                        case "others":
                            newIntent = new Intent(this, OtherNotificationActivity.class);
                            break;
                        case "custom":
                            newIntent = new Intent(this, ((MobikulApplication) getApplication()).getCatalogProductPageClass());
                            break;
                        default:
                            return;
                    }
                    DataBaseHelper db = new DataBaseHelper(this);
                    db.addNotificationID(String.valueOf(notificationId));
                    db.close();
                    Utils.setNotificationBadge(this);
                }
            }
            if (newIntent != null) {
                newIntent.putExtra(BUNDLE_KEY_NOTIFICATION_TYPE, notificationType);
                newIntent.putExtra(BUNDLE_KEY_FROM_NOTIFICATION, true);
                newIntent.putExtra(BUNDLE_KEY_NOTIFICATION_ID, notificationId);

                startActivity(newIntent);
                this.finish();
                return;
            }
        }

        if (Intent.ACTION_VIEW.equals(action) && data != null && !data.equals(HOST_NAME)) {
            /*Deep Link Testing*/
            /*adb shell am start -a android.intent.action.VIEW -d "http://192.168.10.114/Mo1/home-decor/bed-bath.html"*/
            /*adb shell am start -a android.intent.action.VIEW -d "http://192.168.10.114/Mo1/modern-murray-ceramic-vase-490.html"*/
            mDeepLinkingUrl = data;
            mIsFromUrl = 1;
        }

        VersionChecker versionChecker = new VersionChecker();
        try {
            mLatestVersionName = versionChecker.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        if (NetworkHelper.isNetworkAvailable(SplashScreenActivity.this) && mOfflineDataBaseHandler.getCartTableRowCount() > 0) {
            showAlertDialogWithClickListener(this, SweetAlertDialog.NORMAL_TYPE, getString(R.string.offline_bag), getString(R.string.sync_cart_with_server_body_message)
                    , getResources().getString(R.string.sync_now), getResources().getString(R.string.clear), new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            showToast(SplashScreenActivity.this, getString(R.string.syncing_in_bg), Toast.LENGTH_LONG, 0);
                            SyncCartDbWithServer syncCartDbWithServer = new SyncCartDbWithServer(SplashScreenActivity.this);
                            syncCartDbWithServer.execute();
                            getHomePageData();
                        }
                    }, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            mOfflineDataBaseHandler.clearCartTableData();
                            getHomePageData();
                        }
                    });
        } else {
            getHomePageData();
        }
    }

    private void getHomePageData() {
        mBinding.setIsLoading(true);
        if (NetworkHelper.isNetworkAvailable(this)) {
            callApi();
        } else {
            final Cursor databaseCursor = mOfflineDataBaseHandler.selectFromOfflineDB(MOBIKUL_CATALOG_HOME_PAGE_DATA, null);
            if (databaseCursor != null && databaseCursor.getCount() != 0) {
                databaseCursor.moveToFirst();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        homePageResponseDataObject = gson.fromJson(databaseCursor.getString(0), HomePageResponseData.class);
                        startHomeActivity();
                    }
                }, 2000);
            } else {
                callApi();
            }
        }
    }

    private void callApi() {
        ApiConnection.getHomePageData(AppSharedPref.getStoreId(this)
                , AppSharedPref.getQuoteId(this)
                , AppSharedPref.getCustomerId(this)
                , Utils.getScreenWidth()
                , ApplicationConstant.DEFAULT_WEBSITE_ID
                , getResources().getDisplayMetrics().density
                , mIsFromUrl
                , mDeepLinkingUrl
                , AppSharedPref.getCurrencyCode(this))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<HomePageResponseData>(this) {
            @Override
            public void onNext(final HomePageResponseData homePageResponseData) {
                super.onNext(homePageResponseData);
                try {
                    if (homePageResponseData.getSuccess()) {
                        onResponse(homePageResponseData);
                    } else if (NetworkHelper.isNetworkAvailable(SplashScreenActivity.this)) {
                        showAlertDialogWithClickListener(SplashScreenActivity.this, SweetAlertDialog.WARNING_TYPE, getString(R.string.error), homePageResponseData.getMessage(), getResources().getString(R.string.yes), "", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                try {
                                    switch (homePageResponseData.getOtherError()) {
                                        case "customerNotExist":
                                            SharedPreferences.Editor customerSharedPrefEditor = AppSharedPref.getSharedPreferenceEditor(SplashScreenActivity.this, CUSTOMER_PREF);
                                            customerSharedPrefEditor.clear();
                                            customerSharedPrefEditor.apply();
                                            onResponse(homePageResponseData);
                                            break;
                                        default:
                                            finish();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    finish();
                                }
                            }
                        }, null);
                    } else {
                        onFailure();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    onFailure();
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                onFailure();
            }
        });
    }

    private void onResponse(HomePageResponseData homePageResponseData) {
        homePageResponseDataObject = homePageResponseData;

        String responseJSON = gson.toJson(homePageResponseDataObject);
        if (NetworkHelper.isNetworkAvailable(this)) {
            mOfflineDataBaseHandler.updateIntoOfflineDB(MOBIKUL_CATALOG_HOME_PAGE_DATA, responseJSON, null);
        }

        if (AppSharedPref.getCurrencyCode(this).isEmpty()) {
            AppSharedPref.setCurrencyCode(this, homePageResponseDataObject.getDefaultCurrency());
        }

        if (homePageResponseDataObject.getProductId() != 0) {
            Log.d(ApplicationConstant.TAG, "onResponse: Intent from product URL");
            Intent intent = new Intent(SplashScreenActivity.this, NewProductActivity.class);
            intent.putExtra(KEY_PRODUCT_ID, homePageResponseDataObject.getProductId());
            intent.putExtra(KEY_PRODUCT_NAME, homePageResponseDataObject.getProductName());
            intent.putExtra(BUNDLE_KEY_IS_FROM_URL, "");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            SplashScreenActivity.this.finish();
            return;
        } else if (homePageResponseDataObject.getCategoryId() != 0) {
            Log.d(ApplicationConstant.TAG, "onResponse: Intent from category URL");
            Intent intent = new Intent(SplashScreenActivity.this, ((MobikulApplication) getApplication()).getCatalogProductPageClass());
            intent.putExtra(KEY_CATEGORY_ID, homePageResponseDataObject.getCategoryId());
            intent.putExtra(KEY_CATEGORY_NAME, homePageResponseDataObject.getCategoryName());
            intent.putExtra(BUNDLE_KEY_IS_FROM_URL, "");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            SplashScreenActivity.this.finish();
            return;
        }

        if (homePageResponseDataObject.getStoreId() > 0) {
            AppSharedPref.setStoreId(this, homePageResponseDataObject.getStoreId());
            for (int noOfStore = 0; noOfStore < homePageResponseData.getStoreData().get(0).getStores().size(); noOfStore++) {
                if (homePageResponseData.getStoreData().get(0).getStores().get(noOfStore).getId() == homePageResponseDataObject.getStoreId()) {
                    AppSharedPref.setStoreCode(this, homePageResponseData.getStoreData().get(0).getStores().get(noOfStore).getCode());
                    LocaleUtils.setStoreLanguage(this, AppSharedPref.getStoreCode(this));
                }
            }
        }

        updatePriceFormatPref(homePageResponseDataObject.getPriceFormat());

        AppSharedPref.setMobileLoginEnabled(SplashScreenActivity.this, homePageResponseDataObject.isMobileLoginEnable());

        if (mLatestVersionName != null && Double.parseDouble(BuildConfig.VERSION_NAME) < Double.parseDouble(mLatestVersionName)) {

            mBinding.setIsLoading(false);
            showAlertDialogWithClickListener(this, SweetAlertDialog.WARNING_TYPE, getString(R.string.update_alert_title), getResources().getString(R.string.new_version_available), getResources().getString(R.string.yes), "", new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(APP_PLAYSTORE_URL)), RC_UPDATE_APP_FROM_PLAYSTORE);
                }
            }, null);
        } else {
            startHomeActivity();
        }
    }

    private void updatePriceFormatPref(PriceFormatData priceFormat) {
        SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(this, PRICE_FORMAT_PREF);
        customerDataSharedPref.putString(KEY_PRICE_PATTERN, priceFormat.getPattern());
        customerDataSharedPref.putInt(KEY_PRICE_PRECISION, priceFormat.getPrecision());
        customerDataSharedPref.apply();
    }

    private void onFailure() {
        mBinding.setIsLoading(false);
        if (NetworkHelper.isNetworkAvailable(this)) {
            showAlertDialogWithClickListener(this, SweetAlertDialog.ERROR_TYPE, getString(R.string.oops), getString(R.string.server_down_error)
                    , getString(R.string.try_again), getString(R.string.no), new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            getHomePageData();
                        }
                    }, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            finish();
                        }
                    });
        } else {
            showAlertDialogWithClickListener(this, SweetAlertDialog.ERROR_TYPE, getString(R.string.oops), getString(R.string.error_no_network)
                    , getString(R.string.try_again), getString(R.string.no), new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            getHomePageData();
                        }
                    }, new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            finish();
                        }
                    });
        }
    }

    public void startHomeActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, ((MobikulApplication) getApplication()).getHomePageClass());
        intent.putExtra(BUNDLE_KEY_HOME_PAGE_RESPONSE_DATA, homePageResponseDataObject);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        SplashScreenActivity.this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_UPDATE_APP_FROM_PLAYSTORE) {
            startHomeActivity();
        }
    }

    @Override
    public void onBackPressed() {
        long time = System.currentTimeMillis();
        if (time - mBackPressedTime > DEFAULT_BACK_PRESSED_TIME_TO_CLOSE) {
            mBackPressedTime = time;
            showToast(this, getResources().getString(R.string.press_back_to_exit), Toast.LENGTH_SHORT, 0);
        } else {
            finish();
        }
    }
}