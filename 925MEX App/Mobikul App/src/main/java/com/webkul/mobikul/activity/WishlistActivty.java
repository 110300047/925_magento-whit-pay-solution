package com.webkul.mobikul.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.WishlistRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ActivityWishlistBinding;
import com.webkul.mobikul.handler.WishListActivityHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.wishlist.WishListResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_PAGE_COUNT;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_APP_SIGN_IN;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

//@Shortcut(id = "wishlist", icon = R.drawable.ic_vector_in_wishlist, shortLabelRes = R.string.title_activity_my_wishlist, rank = 2, backStack = {HomeActivity.class})
public class WishlistActivty extends BaseActivity {

    private int mPageNumber = DEFAULT_PAGE_COUNT;
    public ActivityWishlistBinding mBinding;
    public WishListResponseData mWishlistData;
    private boolean isFirstCall = true;

    public ActivityWishlistBinding getBinding() {
        return mBinding;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_wishlist);
        showBackButton();
        setActionbarTitle(getString(R.string.title_activity_my_wishlist));
        handleIntent();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        mPageNumber = DEFAULT_PAGE_COUNT;
        isFirstCall = true;
        handleIntent();
    }

    public void handleIntent() {
        if (AppSharedPref.isLoggedIn(this)) {
            mBinding.setLazyLoading(true);

            ApiConnection.getWishListData(
                    AppSharedPref.getStoreId(WishlistActivty.this)
                    , AppSharedPref.getCustomerId(WishlistActivty.this)
                    , Utils.getScreenWidth()
                    , mPageNumber
            ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<WishListResponseData>(this) {
                @Override
                public void onNext(WishListResponseData wishListResponseData) {
                    super.onNext(wishListResponseData);
                    mBinding.setLazyLoading(false);

                    if (isFirstCall) {
                        mWishlistData = wishListResponseData;
                        mBinding.setData(mWishlistData);
                        mBinding.setHandler(new WishListActivityHandler());
                        mBinding.wishlistRv.setLayoutManager(new LinearLayoutManager(WishlistActivty.this));
                        mBinding.wishlistRv.setAdapter(new WishlistRvAdapter(WishlistActivty.this, mWishlistData.getWishlistItemData()));
                        mBinding.wishlistRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);

                                int lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

                                if (NetworkHelper.isNetworkAvailable(WishlistActivty.this)) {
                                    if (!mBinding.getLazyLoading() && mWishlistData.getWishlistItemData().size() != mWishlistData.getTotalCount()
                                            && lastCompletelyVisibleItemPosition == (mWishlistData.getWishlistItemData().size() - 1) && lastCompletelyVisibleItemPosition < mWishlistData.getTotalCount()) {
                                        mPageNumber++;
                                        isFirstCall = false;
                                        mBinding.setLazyLoading(true);
                                        handleIntent();
                                    }
                                }
                            }
                        });

                        mBinding.wishlistRv.setItemAnimator(new FadeInLeftAnimator());
                        mBinding.wishlistRv.getItemAnimator().setRemoveDuration(500);

                        mBinding.executePendingBindings();
                    } else {
                        mWishlistData.setTotalCount(wishListResponseData.getTotalCount());
                        mWishlistData.getWishlistItemData().addAll(wishListResponseData.getWishlistItemData());
                        mBinding.wishlistRv.getAdapter().notifyDataSetChanged();
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mBinding.setLazyLoading(false);
                }
            });
        } else {
            showToast(this, getString(R.string.login_first_to_check_wishlist), Toast.LENGTH_SHORT, 0);
            Intent intent = new Intent(this, LoginAndSignUpActivity.class);
            intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_IN_PAGE);
            startActivityForResult(intent, RC_APP_SIGN_IN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_APP_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                handleIntent();
            } else {
                finish();
            }
        }
    }
}