package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.databinding.ItemCartBinding;
import com.webkul.mobikul.handler.CartItemsRecyclerHandler;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.cart.CartOptionItem;
import com.webkul.mobikul.model.cart.Item;

import java.util.List;


/**
 * Created by vedesh.kumar on 27/12/16. @Webkul Software Pvt. Ltd
 */

public class CartItemsRecyclerAdapter extends RecyclerView.Adapter<CartItemsRecyclerAdapter.ViewHolder> {
    private final Context mContext;
    private final List<Item> mItems;
    private Boolean mShowThreshold;

    public CartItemsRecyclerAdapter(Context context, List<Item> items, Boolean showThreshold) {
        mContext = context;
        mItems = items;
        mShowThreshold = showThreshold;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_cart, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Item item = mItems.get(position);
        holder.mBinding.incrementQtyIv.setTag(position);
        holder.mBinding.decrementQtyIv.setTag(position);
        holder.mBinding.setData(item);
        holder.mBinding.setPosition(position);
        holder.mBinding.setShowThreshold(mShowThreshold);
        holder.mBinding.setHandler(new CartItemsRecyclerHandler(mContext, this, holder.mBinding));
        holder.mBinding.itemMessageTv.setText(item.getMessages().getText());
        if (!((CartActivity) mContext).mCartDetailsResponseData.isContainsDownloadableProducts() && item.getTypeId().equals("downloadable")) {
            ((CartActivity) mContext).mCartDetailsResponseData.setContainsDownloadableProducts(true);
        }
        if (item.getMessages().getType() != null && item.getMessages().getType().equals("error")) {
            holder.mBinding.itemMessageTv.setTextColor(mContext.getResources().getColor(android.R.color.holo_red_light));
            ((CartActivity) mContext).mCartDetailsResponseData.setCanProcceedToCheckout(false);
            ((CartActivity) mContext).mBinding.setData(((CartActivity) mContext).mCartDetailsResponseData);
            ((CartActivity) mContext).mBinding.executePendingBindings();
        } else {
            holder.mBinding.itemMessageTv.setTextColor(mContext.getResources().getColor(android.R.color.holo_orange_light));
        }
        if (item.getOptionItems() != null && item.getOptionItems().size() > 0) {
            holder.mBinding.optionTableLayout.removeAllViews();
            for (int optionIterator = 0; optionIterator < item.getOptionItems().size(); optionIterator++) {

                CartOptionItem optionItem = item.getOptionItems().get(optionIterator);

                TableRow tableRow = new TableRow(mContext);
                tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                tableRow.setPadding(1, 1, 3, 1);
                tableRow.setBackgroundResource(R.drawable.shape_rectangular_white_bg_gray_border_1dp);
                tableRow.setGravity(Gravity.CENTER_VERTICAL);

                TextView labelTv = new TextView(mContext);
                labelTv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));
                labelTv.setMaxWidth((int) (Utils.getScreenWidth() / 2.5));
                labelTv.setPadding(10, 5, 10, 5);
                labelTv.setTextSize(10);
                labelTv.setBackgroundResource(R.color.grey_200);
                labelTv.setText(optionItem.getLabel());
                tableRow.addView(labelTv);

                TextView val = new TextView(mContext);
                val.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
                val.setPadding(10, 5, 10, 5);
                val.setTextSize(10);
                String value = optionItem.getValue().get(0);
                for (int noOfValues = 1; noOfValues < optionItem.getValue().size(); noOfValues++) {
                    value = value + "\n" + optionItem.getValue().get(noOfValues);
                }
                val.setText(value);
                tableRow.addView(val);
                holder.mBinding.optionTableLayout.addView(tableRow);

            }
        }
        holder.mBinding.executePendingBindings();
    }

    public boolean isCartItemQtyChanged() {
        for (Item eachCartItem : mItems) {
            if (eachCartItem.isCartItemQtyChanged())
                return true;
        }
        return false;
    }

    public Item getCartItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void remove(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemCartBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }

}
