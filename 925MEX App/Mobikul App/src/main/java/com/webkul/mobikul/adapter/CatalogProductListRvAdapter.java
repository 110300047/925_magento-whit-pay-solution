package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.databinding.ItemCatalogProductGridBinding;
import com.webkul.mobikul.databinding.ItemCatalogProductListBinding;
import com.webkul.mobikul.handler.CatalogProductListRvHandler;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.List;

import static com.webkul.mobikul.constants.ApplicationConstant.VIEW_TYPE_GRID;
import static com.webkul.mobikul.constants.ApplicationConstant.VIEW_TYPE_LIST;

/**
 * Created by vedesh.kumar on 21/12/16. @Webkul Software Pvt. Ltd
 */
public class CatalogProductListRvAdapter extends RecyclerView.Adapter<CatalogProductListRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<ProductData> mProductDatas;

    public CatalogProductListRvAdapter(Context context, List<ProductData> productDatas) {
        mContext = context;
        mProductDatas = productDatas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (viewType == VIEW_TYPE_LIST) {
            return new ViewHolder(inflater.inflate(R.layout.item_catalog_product_list, parent, false));
        } else {
            return new ViewHolder(inflater.inflate(R.layout.item_catalog_product_grid, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(CatalogProductListRvAdapter.ViewHolder holder, int position) {
        final ProductData productData = mProductDatas.get(position);
        productData.setProductPosition(position);
        if (getItemViewType(position) == VIEW_TYPE_LIST) {
            ((ItemCatalogProductListBinding) holder.mBinding).setCollection(productData);
            ((ItemCatalogProductListBinding) holder.mBinding).setHandler(new CatalogProductListRvHandler(mContext));
            if (productData.isInWishlist())
                ((ItemCatalogProductListBinding) holder.mBinding).addToWishlistIv.setProgress(1);
            else
                ((ItemCatalogProductListBinding) holder.mBinding).addToWishlistIv.setProgress(0);
        } else {
            ((ItemCatalogProductGridBinding) holder.mBinding).setCollection(productData);
            ((ItemCatalogProductGridBinding) holder.mBinding).setHandler(new CatalogProductListRvHandler(mContext));
            if (productData.isInWishlist())
                ((ItemCatalogProductGridBinding) holder.mBinding).addToWishlistIv.setProgress(1);
            else
                ((ItemCatalogProductGridBinding) holder.mBinding).addToWishlistIv.setProgress(0);
        }
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemViewType(int position) {
        /*As this class is used by seller proflie page and catalog product page*/
        if (mContext instanceof CatalogProductActivity) {
            if (((CatalogProductActivity) mContext).getBinding().productCatalogRv.getLayoutManager() instanceof GridLayoutManager) {
                return VIEW_TYPE_GRID;
            } else {
                return VIEW_TYPE_LIST;
            }
        }
        return VIEW_TYPE_GRID;
    }

    @Override
    public int getItemCount() {
        if (mProductDatas == null) {
            return 0;
        }
        return mProductDatas.size();

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);

        }
    }
}
