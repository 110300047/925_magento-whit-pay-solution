package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemCompareProductsAttributeValueBinding;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class CompareProductsAttributesListRvAdapter extends RecyclerView.Adapter<CompareProductsAttributesListRvAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<String> mCompareProductsAttributeValuesDatas;

    public CompareProductsAttributesListRvAdapter(Context context, ArrayList<String> compareProductsAttributeValuesDatas) {
        mContext = context;
        mCompareProductsAttributeValuesDatas = compareProductsAttributeValuesDatas;
    }

    @Override
    public CompareProductsAttributesListRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_compare_products_attribute_value, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CompareProductsAttributesListRvAdapter.ViewHolder holder, int position) {
        final String productListData = mCompareProductsAttributeValuesDatas.get(position);
        holder.mBinding.setData(productListData.replaceAll("<style([\\s\\S]+?)</style>", ""));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCompareProductsAttributeValuesDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemCompareProductsAttributeValueBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}