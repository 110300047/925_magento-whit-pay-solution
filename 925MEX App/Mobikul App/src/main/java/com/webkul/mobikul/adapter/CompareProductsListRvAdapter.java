package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemCompareProductGridViewBinding;
import com.webkul.mobikul.handler.CompareProductListRvHandler;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class CompareProductsListRvAdapter extends RecyclerView.Adapter<CompareProductsListRvAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<ProductData> mCompareProductsDatas;

    public CompareProductsListRvAdapter(Context context, ArrayList<ProductData> compareProductsDatas) {
        mContext = context;
        mCompareProductsDatas = compareProductsDatas;
    }

    @Override
    public CompareProductsListRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_compare_product_grid_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CompareProductsListRvAdapter.ViewHolder holder, int position) {
        final ProductData productListData = mCompareProductsDatas.get(position);
        productListData.setProductPosition(position);
        holder.mBinding.setData(productListData);
        holder.mBinding.setHandler(new CompareProductListRvHandler(mContext, mCompareProductsDatas));
        if (productListData.isInWishlist())
            holder.mBinding.addToWishlistIv.setProgress(1);
        else
            holder.mBinding.addToWishlistIv.setProgress(0);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCompareProductsDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemCompareProductGridViewBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}