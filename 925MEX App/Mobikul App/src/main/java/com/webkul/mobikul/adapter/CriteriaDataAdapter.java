package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemCriteriaDataBinding;

import java.util.List;

/**
 * Created by vedesh.kumar on 9/6/17.
 */

public class CriteriaDataAdapter extends RecyclerView.Adapter<CriteriaDataAdapter.ViewHolder> {

    private Context mContext;
    private List<String> criteriaDataList;

    public CriteriaDataAdapter(Context mContext, List<String> criteriaDataList) {
        this.mContext = mContext;
        this.criteriaDataList = criteriaDataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View criteriaDataView = inflater.inflate(R.layout.item_criteria_data, parent, false);
        return new ViewHolder(criteriaDataView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setData(criteriaDataList.get(position));
    }

    @Override
    public int getItemCount() {
        if (criteriaDataList == null) {
            return 0;
        } else {
            return criteriaDataList.size();
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemCriteriaDataBinding mBinding;

        public ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
