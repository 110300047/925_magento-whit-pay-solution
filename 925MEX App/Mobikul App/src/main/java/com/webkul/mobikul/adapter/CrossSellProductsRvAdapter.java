package com.webkul.mobikul.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.databinding.ItemCrossSellProductGridViewBinding;
import com.webkul.mobikul.handler.CrossSellProductRvHandler;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class CrossSellProductsRvAdapter extends RecyclerView.Adapter<CrossSellProductsRvAdapter.ViewHolder> {

    private final CartActivity mContext;
    private final ArrayList<ProductData> mCrossSellProductsDatas;

    public CrossSellProductsRvAdapter(CartActivity context, ArrayList<ProductData> crossSellProductsDatas) {
        mContext = context;
        mCrossSellProductsDatas = crossSellProductsDatas;
    }

    @Override
    public CrossSellProductsRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_cross_sell_product_grid_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CrossSellProductsRvAdapter.ViewHolder holder, int position) {
        final ProductData crossSellProductsDatas = mCrossSellProductsDatas.get(position);
        crossSellProductsDatas.setProductPosition(position);
        holder.mBinding.setData(crossSellProductsDatas);
        holder.mBinding.setHandler(new CrossSellProductRvHandler(mContext, mCrossSellProductsDatas));
        if (crossSellProductsDatas.isInWishlist())
            holder.mBinding.addToWishlistIv.setProgress(1);
        else
            holder.mBinding.addToWishlistIv.setProgress(0);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCrossSellProductsDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemCrossSellProductGridViewBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}