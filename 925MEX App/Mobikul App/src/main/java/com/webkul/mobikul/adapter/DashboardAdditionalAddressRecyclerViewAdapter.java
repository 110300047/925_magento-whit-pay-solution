package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemDashboardAddressBinding;
import com.webkul.mobikul.handler.DashboardAdditionalAddressRecyclerViewHandler;
import com.webkul.mobikul.model.customer.address.BillingShippingAddress;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class DashboardAdditionalAddressRecyclerViewAdapter extends RecyclerView.Adapter<DashboardAdditionalAddressRecyclerViewAdapter.ViewHolder> {
    private final Context mContext;
    private final List<BillingShippingAddress> mAdditionalAddress;
    private final DashboardAdditionalAddressRecyclerViewHandler.OnAdditionalAddressDeletedListener mOnAdditionalAddressDeletedListener;

    public DashboardAdditionalAddressRecyclerViewAdapter(Context context, List<BillingShippingAddress> additionalAddress, DashboardAdditionalAddressRecyclerViewHandler.OnAdditionalAddressDeletedListener onAdditionalAddressDeletedListener) {
        mContext = context;
        mAdditionalAddress = additionalAddress;
        mOnAdditionalAddressDeletedListener = onAdditionalAddressDeletedListener;
    }

    @Override
    public DashboardAdditionalAddressRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_dashboard_address, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(DashboardAdditionalAddressRecyclerViewAdapter.ViewHolder holder, int position) {
        final BillingShippingAddress additionalAddress = mAdditionalAddress.get(position);
        holder.mBinding.setData(additionalAddress);
        holder.mBinding.setPosition(position);
        holder.mBinding.setHandler(new DashboardAdditionalAddressRecyclerViewHandler(mContext, mOnAdditionalAddressDeletedListener));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mAdditionalAddress.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemDashboardAddressBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
