package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemOrderBinding;
import com.webkul.mobikul.handler.CustomerOrderItemRecyclerHandler;
import com.webkul.mobikul.model.customer.order.OrderList;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class DashboardOrderRecyclerViewAdapter extends RecyclerView.Adapter<DashboardOrderRecyclerViewAdapter.ViewHolder> {
    private final Context mContext;
    private final List<OrderList> mOrderLists;

    public DashboardOrderRecyclerViewAdapter(Context context, List<OrderList> orderList) {
        mContext = context;
        mOrderLists = orderList;
    }

    @Override
    public DashboardOrderRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_order, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(DashboardOrderRecyclerViewAdapter.ViewHolder holder, int position) {
        final OrderList eachOrderData = mOrderLists.get(position);
        holder.mBinding.setData(eachOrderData);
        holder.mBinding.setHandler(new CustomerOrderItemRecyclerHandler());
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mOrderLists.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemOrderBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
