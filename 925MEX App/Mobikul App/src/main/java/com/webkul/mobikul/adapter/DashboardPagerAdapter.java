package com.webkul.mobikul.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.webkul.mobikul.fragment.DashboardAddressFragment;
import com.webkul.mobikul.fragment.DashboardOrderFragment;
import com.webkul.mobikul.fragment.DashboardReviewFragment;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class DashboardPagerAdapter extends FragmentPagerAdapter {

    @SuppressWarnings("FieldCanBeLocal")
    private final int PAGE_COUNT = 3;
    private final String[] tabTitles;

    public DashboardPagerAdapter(FragmentManager supportFragmentManager, Context context) {

        super(supportFragmentManager);
        tabTitles = new String[]{
                context.getString(com.webkul.mobikul.R.string.dashboard_tab_my_address)
                , context.getString(com.webkul.mobikul.R.string.dashboard_tab_my_orders)
                , context.getString(com.webkul.mobikul.R.string.dashboard_tab_title_my_reviews)
        };
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return DashboardAddressFragment.newInstance();

            case 1:
                return DashboardOrderFragment.newInstance(false);

            case 2:
                return DashboardReviewFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
