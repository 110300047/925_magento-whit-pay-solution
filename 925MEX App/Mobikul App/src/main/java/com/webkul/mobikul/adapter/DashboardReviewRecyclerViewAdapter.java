package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemReviewBinding;
import com.webkul.mobikul.handler.CustomerReviewItemRvHandler;
import com.webkul.mobikul.model.customer.review.ReviewListData;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class DashboardReviewRecyclerViewAdapter extends RecyclerView.Adapter<DashboardReviewRecyclerViewAdapter.ViewHolder> {
    private final Context mContext;
    private final List<ReviewListData> mReviewListDatas;

    public DashboardReviewRecyclerViewAdapter(Context context, List<ReviewListData> reviewListDatas) {
        mContext = context;
        mReviewListDatas = reviewListDatas;
    }

    @Override
    public DashboardReviewRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_review, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(DashboardReviewRecyclerViewAdapter.ViewHolder holder, int position) {
        final ReviewListData reviewListData = mReviewListDatas.get(position);
        holder.mBinding.setData(reviewListData);
        holder.mBinding.setHandler(new CustomerReviewItemRvHandler());
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mReviewListDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemReviewBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}