package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemFeaturedCategoryBinding;
import com.webkul.mobikul.handler.FeaturedCategoryHandler;
import com.webkul.mobikul.model.catalog.FeaturedCategory;

import java.util.List;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class FeaturedCategoriesRvAdapter extends RecyclerView.Adapter<FeaturedCategoriesRvAdapter.ViewHolder> {

    private final Context mContext;
    private final List<FeaturedCategory> mFeaturedCategoryDatas;

    public FeaturedCategoriesRvAdapter(Context context, List<FeaturedCategory> featuredCategotyDatas) {
        mContext = context;
        mFeaturedCategoryDatas = featuredCategotyDatas;
    }

    @Override
    public FeaturedCategoriesRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_featured_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FeaturedCategoriesRvAdapter.ViewHolder holder, int position) {
        final FeaturedCategory featuredCategotyData = mFeaturedCategoryDatas.get(position);
        holder.mBinding.setData(featuredCategotyData);
        holder.mBinding.setHandler(new FeaturedCategoryHandler());
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mFeaturedCategoryDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemFeaturedCategoryBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}