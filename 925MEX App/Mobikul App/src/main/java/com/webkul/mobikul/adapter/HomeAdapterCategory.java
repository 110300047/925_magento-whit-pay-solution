package com.webkul.mobikul.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.ItemDrawerEndBinding;
import com.webkul.mobikul.databinding.ItemDrawerStartCategoryBinding;
import com.webkul.mobikul.databinding.ItemHomeCategoryBinding;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.catalog.CategoriesData;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;

public class HomeAdapterCategory extends RecyclerView.Adapter<HomeAdapterCategory.HomeCategoryHolder> {
    private List<CategoriesData> mCategoriesLists;
    private Context mContext;

    public static class HomeCategoryHolder extends  RecyclerView.ViewHolder{
        private ItemHomeCategoryBinding mBinding;
        public CircleImageView mCircleImageView;
        public HomeCategoryHolder(View itemView){
           super(itemView);
           mBinding= DataBindingUtil.bind(itemView);
           mCircleImageView=(CircleImageView) itemView.findViewById(R.id.image_category);
        }
    }


    public HomeAdapterCategory(Context context, List<CategoriesData> categoriesData){
        this.mCategoriesLists=categoriesData;
        this.mContext=context;
    }

    @Override
    public HomeCategoryHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        //Aqui se crea la vista
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_home_category, viewGroup, false);
        return new HomeCategoryHolder(view);
    }

    //Aqui se modifican los iconos del menu principal (home)
    @Override
    public void onBindViewHolder(HomeCategoryHolder homeCategoryHolder, int i) {
        final CategoriesData categoriesData = mCategoriesLists.get(i);
        Log.d(ApplicationConstant.TAG,"este es: "+categoriesData.getName());
        switch (categoriesData.getName()){
            case "Accommodation":
                homeCategoryHolder.mCircleImageView.setImageResource(R.drawable.accommodation002);
                //homeCategoryHolder.mCircleImageView.setImageResource(R.drawable.ic_accommodation);
            break;
            case "Transportation":
                homeCategoryHolder.mCircleImageView.setImageResource(R.drawable.transportation002);
            break;
            case "Culinary":
                homeCategoryHolder.mCircleImageView.setImageResource(R.drawable.culinary002);
                break;
            case "Leisure":
                homeCategoryHolder.mCircleImageView.setImageResource(R.drawable.leisure002);
                break;
            case "Services":
                homeCategoryHolder.mCircleImageView.setImageResource(R.drawable.services002);
                break;
            case "Stores":
                homeCategoryHolder.mCircleImageView.setImageResource(R.drawable.stores002);
                break;
        }
        homeCategoryHolder.mBinding.setCategoriesData(categoriesData);
        homeCategoryHolder.mBinding.categoryContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ((MobikulApplication) mContext.getApplicationContext()).getCatalogProductPageClass());
                intent.putExtra(KEY_CATEGORY_ID, categoriesData.getCategoryId());
                intent.putExtra(KEY_CATEGORY_NAME, categoriesData.getName());
                mContext.startActivity(intent); }
            });
    }

    @Override
    public int getItemCount() {
        return mCategoriesLists.size();
    }
}
