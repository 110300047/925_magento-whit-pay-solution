package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemViewPagerHomeBannerBinding;
import com.webkul.mobikul.handler.HomeBannerHandler;
import com.webkul.mobikul.model.catalog.BannerImage;

import java.util.List;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */
public class HomeBannerAdapter extends PagerAdapter {

    private final Context mContext;
    private List<BannerImage> mBannerDatas;

    public HomeBannerAdapter(Context context, List<BannerImage> bannerDatas) {
        mContext = context;
        mBannerDatas = bannerDatas;
    }

    @Override
    public int getCount() {
        return mBannerDatas.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ItemViewPagerHomeBannerBinding itemViewPagerBannerBinding = DataBindingUtil.bind(inflater.inflate(R.layout.item_view_pager_home_banner, container, false));
        itemViewPagerBannerBinding.setData(mBannerDatas.get(position));
        itemViewPagerBannerBinding.setHandler(new HomeBannerHandler());
        itemViewPagerBannerBinding.executePendingBindings();
        container.addView(itemViewPagerBannerBinding.getRoot());
        return (itemViewPagerBannerBinding.getRoot());
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
