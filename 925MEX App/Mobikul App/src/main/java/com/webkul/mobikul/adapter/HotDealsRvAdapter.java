package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemHotDealsBinding;
import com.webkul.mobikul.handler.HomePageProductHandler;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/1/17. @Webkul Software Pvt. Ltd
 */

public class HotDealsRvAdapter extends RecyclerView.Adapter<HotDealsRvAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<ProductData> mHotDealsDatas;

    public HotDealsRvAdapter(Context context, ArrayList<ProductData> featuredProductsDatas) {
        mContext = context;
        mHotDealsDatas = featuredProductsDatas;
    }

    @Override
    public HotDealsRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_hot_deals, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HotDealsRvAdapter.ViewHolder holder, int position) {
        final ProductData hotDealsData = mHotDealsDatas.get(position);
        hotDealsData.setProductPosition(position);
        holder.mBinding.setData(hotDealsData);
        holder.mBinding.setHandler(new HomePageProductHandler(mContext, mContext.getString(R.string.category_hot_deals_identifier), mHotDealsDatas));
        if (hotDealsData.isInWishlist())
            holder.mBinding.wishlistAnimationView.setProgress(1);
        else
            holder.mBinding.wishlistAnimationView.setProgress(0);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        if (mHotDealsDatas.size() <= 4)
            return mHotDealsDatas.size();
        else
            return 4;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemHotDealsBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
