package com.webkul.mobikul.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemInviteeBinding;
import com.webkul.mobikul.fragment.ProductEmailFragment;
import com.webkul.mobikul.handler.InviteeItemHandler;
import com.webkul.mobikul.model.product.InviteeItem;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class InviteeRvAdapter extends RecyclerView.Adapter<InviteeRvAdapter.ViewHolder> {

    private final ProductEmailFragment mContext;
    private final ArrayList<InviteeItem> mItemData;

    public InviteeRvAdapter(ProductEmailFragment context, ArrayList<InviteeItem> itemData) {
        mContext = context;
        mItemData = itemData;
    }

    @Override
    public InviteeRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext.getContext());
        View view = inflater.inflate(R.layout.item_invitee, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final InviteeRvAdapter.ViewHolder holder, int position) {
        final InviteeItem inviteeData = mItemData.get(position);
        inviteeData.setPosition(position);
        holder.mBinding.setData(inviteeData);
        holder.mBinding.setHandler(new InviteeItemHandler(mContext));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mItemData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemInviteeBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}