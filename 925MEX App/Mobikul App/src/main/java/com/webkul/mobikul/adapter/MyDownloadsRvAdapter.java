package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemMyDownloadsBinding;
import com.webkul.mobikul.handler.MyDownloadableListItemHandler;
import com.webkul.mobikul.model.customer.downloadable.DownloadsList;

import java.util.List;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class MyDownloadsRvAdapter extends RecyclerView.Adapter<MyDownloadsRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<DownloadsList> mDownloadsListDatas;


    public MyDownloadsRvAdapter(Context context, List<DownloadsList> downloadsListDatas) {
        mContext = context;
        mDownloadsListDatas = downloadsListDatas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_my_downloads, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final DownloadsList downloadlistItemData = mDownloadsListDatas.get(position);
        holder.mBinding.setData(downloadlistItemData);
        holder.mBinding.setHandler(new MyDownloadableListItemHandler(mContext, this));
        holder.mBinding.executePendingBindings();
    }

    public DownloadsList getWishlistItem(int position) {
        return mDownloadsListDatas.get(position);
    }

    public List<DownloadsList> getWishlistDataList() {
        return mDownloadsListDatas;
    }

    @Override
    public int getItemCount() {
        return mDownloadsListDatas.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemMyDownloadsBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
