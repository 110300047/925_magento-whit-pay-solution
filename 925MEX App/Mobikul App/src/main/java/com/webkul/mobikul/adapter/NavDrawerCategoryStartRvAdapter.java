package com.webkul.mobikul.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.databinding.ItemDrawerStartCategoryBinding;
import com.webkul.mobikul.databinding.ItemDrawerStartSubcategoryBinding;
import com.webkul.mobikul.handler.NavDrawerStartCategoryHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.catalog.CategoriesData;
import com.webkul.mobikul.model.catalog.CategoriesImageData;
import com.webkul.mobikul.model.catalog.SubCategory;

import java.util.List;

import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;

/**
 * Created by vedesh.kumar on 13/10/16. @Webkul Software Pvt. Ltd
 */
public class NavDrawerCategoryStartRvAdapter extends ExpandableRecyclerAdapter<CategoriesData, SubCategory, NavDrawerCategoryStartRvAdapter.CategoryParentViewHolder, NavDrawerCategoryStartRvAdapter.CategoryChildViewHolder> {
    private static Context mContext;
    private List<CategoriesData> mCategoriesData;
    private List<CategoriesImageData> mCategoriesImages;

    public NavDrawerCategoryStartRvAdapter(Context context, List<CategoriesData> categoriesData, List<CategoriesImageData> categoriesImages) {
        super(categoriesData);
        mContext = context;
        mCategoriesData = categoriesData;
        mCategoriesImages = categoriesImages;
    }

    @NonNull
    @Override
    public CategoryParentViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View parentView = inflater.inflate(R.layout.item_drawer_start_category, parentViewGroup, false);
        return new NavDrawerCategoryStartRvAdapter.CategoryParentViewHolder(parentView);
    }

    @NonNull
    @Override
    public CategoryChildViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View childView = inflater.inflate(R.layout.item_drawer_start_subcategory, childViewGroup, false);
        return new NavDrawerCategoryStartRvAdapter.CategoryChildViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(@NonNull CategoryParentViewHolder parentViewHolder, int parentPosition, @NonNull CategoriesData parent) {
        final CategoriesData categoriesData = mCategoriesData.get(parentViewHolder.getAdapterPosition());
        parentViewHolder.mBinding.setCategoriesData(categoriesData);
        if (categoriesData.getChildren().size() == 0)
            parentViewHolder.mBinding.categoryContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ((MobikulApplication) mContext.getApplicationContext()).getCatalogProductPageClass());
                    intent.putExtra(KEY_CATEGORY_ID, categoriesData.getCategoryId());
                    intent.putExtra(KEY_CATEGORY_NAME, categoriesData.getName());
                    mContext.startActivity(intent);
                    ((HomeActivity) v.getContext()).mBinding.drawerLayout.closeDrawers();
                }
            });
        parentViewHolder.mBinding.setCategoryIcon(getCategoryImageFromId(categoriesData.getCategoryId()));
        parentViewHolder.mBinding.executePendingBindings();
    }

    @Override
    public void onBindChildViewHolder(@NonNull CategoryChildViewHolder childViewHolder, int parentPosition, int childPosition, @NonNull SubCategory subCategoryData) {
        childViewHolder.mBinding.setSubCategory(subCategoryData);
        childViewHolder.mBinding.setHandler(new NavDrawerStartCategoryHandler(mContext, childPosition));
        childViewHolder.mBinding.executePendingBindings();
    }

    private String getCategoryImageFromId(int categoryId) {
        for (CategoriesImageData categoryImageData : mCategoriesImages) {
            if (categoryImageData.getId() == categoryId) {
                return categoryImageData.getThumbnail();
            }
        }
        return null;
    }

    static class CategoryParentViewHolder extends ParentViewHolder {
        private ItemDrawerStartCategoryBinding mBinding;

        private CategoryParentViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

        @Override
        @UiThread
        public void onClick(View v) {
            super.onClick(v);
            if (isExpanded()) {
                if (AppSharedPref.getStoreCode(mContext).equals("ar"))
                    mBinding.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_vector_down_arrow_wrapper, 0, 0, 0);
                else {
                    mBinding.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_down_arrow_wrapper, 0);
                }
            } else {
                if (AppSharedPref.getStoreCode(mContext).equals("ar"))
                    mBinding.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_vector_side_arrow_arabic_wrapper, 0, 0, 0);
                else {
                    mBinding.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_side_arrow_wrapper, 0);
                }
            }
        }
    }

    static class CategoryChildViewHolder extends ChildViewHolder {
        private final ItemDrawerStartSubcategoryBinding mBinding;

        private CategoryChildViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}