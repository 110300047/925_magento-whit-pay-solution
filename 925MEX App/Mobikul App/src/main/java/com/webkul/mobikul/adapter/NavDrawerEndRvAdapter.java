package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemDrawerEndBinding;
import com.webkul.mobikul.databinding.ItemDrawerEndHeaderBinding;
import com.webkul.mobikul.handler.NavDrawerEndRvHandler;
import com.webkul.mobikul.model.catalog.NavDrawerCustomerItem;

import java.util.List;

import static com.webkul.mobikul.model.catalog.NavDrawerCustomerItem.TYPE_HEADER;

/**
 * Created by vedesh.kumar on 13/10/16. @Webkul Software Pvt. Ltd
 */
public class NavDrawerEndRvAdapter extends RecyclerView.Adapter<NavDrawerEndRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<NavDrawerCustomerItem> mNavDrawerCustmerItems;

    public NavDrawerEndRvAdapter(Context context, List<NavDrawerCustomerItem> navDrawerCustmerItems) {
        mContext = context;
        mNavDrawerCustmerItems = navDrawerCustmerItems;
    }

    public List<NavDrawerCustomerItem> getNavDrawerCustomerItems() {
        return mNavDrawerCustmerItems;
    }

    @Override
    public int getItemViewType(int position) {
        return mNavDrawerCustmerItems.get(position).getItemType();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (viewType == TYPE_HEADER) {
            return new ViewHolder(inflater.inflate(R.layout.item_drawer_end_header, parent, false));
        } else {
            return new ViewHolder(inflater.inflate(R.layout.item_drawer_end, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NavDrawerCustomerItem navDrawerCustmerItem = mNavDrawerCustmerItems.get(position);
        if (navDrawerCustmerItem.getItemType() == TYPE_HEADER) {
            ((ItemDrawerEndHeaderBinding) holder.mBinding).setData(navDrawerCustmerItem);
        } else {
            ((ItemDrawerEndBinding) holder.mBinding).setData(navDrawerCustmerItem);
            ((ItemDrawerEndBinding) holder.mBinding).setHandler(new NavDrawerEndRvHandler(mContext));
        }
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mNavDrawerCustmerItems.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
