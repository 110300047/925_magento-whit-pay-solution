package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.NavDrawerExtrasItemBinding;
import com.webkul.mobikul.handler.NavDrawerStartExtrasItemHandler;
import com.webkul.mobikul.model.catalog.NavDrawerStartExtrasItem;

import java.util.List;

/**
 * Created by vedesh.kumar on 13/10/16. @Webkul Software Pvt. Ltd
 */

public class NavDrawerStartExtrasRvAdapter extends RecyclerView.Adapter<NavDrawerStartExtrasRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<NavDrawerStartExtrasItem> mNavDrawerExtrasMenuItem;

    public NavDrawerStartExtrasRvAdapter(Context context, List<NavDrawerStartExtrasItem> navDrawerExtrasMenuItem) {
        mContext = context;
        mNavDrawerExtrasMenuItem = navDrawerExtrasMenuItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new ViewHolder(inflater.inflate(R.layout.nav_drawer_extras_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NavDrawerStartExtrasItem navDrawerExtrasMenuItem = mNavDrawerExtrasMenuItem.get(position);
        holder.mBinding.setData(navDrawerExtrasMenuItem);
        holder.mBinding.setHandler(new NavDrawerStartExtrasItemHandler(mContext));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mNavDrawerExtrasMenuItem.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private NavDrawerExtrasItemBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}