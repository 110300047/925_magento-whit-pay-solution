package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemDrawerStartSearchBinding;
import com.webkul.mobikul.handler.NavDrawerStartSearchHandler;
import com.webkul.mobikul.model.search.NavDrawerStartSearchMenuData;

import java.util.List;

/**
 * Created by vedesh.kumar on 13/10/16. @Webkul Software Pvt. Ltd
 */
public class NavDrawerStartSearchRvAdapter extends RecyclerView.Adapter<NavDrawerStartSearchRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<NavDrawerStartSearchMenuData> mNavDrawerSearchMenuItem;

    public NavDrawerStartSearchRvAdapter(Context context, List<NavDrawerStartSearchMenuData> navDrawerSearchMenuItem) {
        mContext = context;
        mNavDrawerSearchMenuItem = navDrawerSearchMenuItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new ViewHolder(inflater.inflate(R.layout.item_drawer_start_search, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NavDrawerStartSearchMenuData navDrawerSearchMenuItem = mNavDrawerSearchMenuItem.get(position);
        holder.mBinding.setData(navDrawerSearchMenuItem);
        holder.mBinding.setHandler(new NavDrawerStartSearchHandler(mContext));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mNavDrawerSearchMenuItem.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemDrawerStartSearchBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
