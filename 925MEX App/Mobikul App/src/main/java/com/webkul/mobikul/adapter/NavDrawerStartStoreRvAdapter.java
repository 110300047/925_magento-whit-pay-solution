package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.databinding.ItemDrawerStartStoreBinding;
import com.webkul.mobikul.databinding.ItemDrawerStartStoreViewBinding;
import com.webkul.mobikul.handler.NavDrawerStartStoreViewHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.catalog.Store;
import com.webkul.mobikul.model.catalog.StoreData;

import java.util.List;

/**
 * Created by vedesh.kumar on 13/10/16. @Webkul Software Pvt. Ltd
 */

public class NavDrawerStartStoreRvAdapter extends ExpandableRecyclerAdapter<StoreData, Store, NavDrawerStartStoreRvAdapter.StoreViewHolder, NavDrawerStartStoreRvAdapter
        .StoreViewViewHolder> {
    private static Context mContext;
//    private List<MagentoStoredata> mStoreData;


    public NavDrawerStartStoreRvAdapter(Context context, @NonNull List<StoreData> storeData) {
        super(storeData);
        mContext = context;
    }

    @NonNull
    @Override
    public StoreViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View parentView = inflater.inflate(R.layout.item_drawer_start_store, parentViewGroup, false);
        return new StoreViewHolder(parentView);
    }

    @NonNull
    @Override
    public StoreViewViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View childView = inflater.inflate(R.layout.item_drawer_start_store_view, childViewGroup, false);
        return new StoreViewViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(@NonNull StoreViewHolder parentViewHolder, int parentPosition, @NonNull StoreData storeData) {
        parentViewHolder.mBinding.setStoreData(storeData);
        parentViewHolder.mBinding.executePendingBindings();
    }

    @Override
    public void onBindChildViewHolder(@NonNull StoreViewViewHolder childViewHolder, int parentPosition, int childPosition, @NonNull Store storeInfo) {
        if (storeInfo.getId() == AppSharedPref.getStoreId(((BaseActivity) mContext).getApplication())) {
            childViewHolder.mBinding.setIsSelected(true);
        } else {
            childViewHolder.mBinding.setIsSelected(false);
        }

        childViewHolder.mBinding.setData(storeInfo);
        childViewHolder.mBinding.setHandler(new NavDrawerStartStoreViewHandler());
        childViewHolder.mBinding.executePendingBindings();
    }

    static class StoreViewHolder extends ParentViewHolder {
        private final ItemDrawerStartStoreBinding mBinding;

        private StoreViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

        @Override
        @UiThread
        public void onClick(View v) {
            super.onClick(v);
            if (isExpanded()) {
                if (AppSharedPref.getStoreCode(mContext).equals("ar"))
                    mBinding.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_vector_down_arrow_wrapper, 0, 0, 0);
                else {
                    mBinding.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_down_arrow_wrapper, 0);
                }
            } else {
                if (AppSharedPref.getStoreCode(mContext).equals("ar"))
                    mBinding.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_vector_side_arrow_arabic_wrapper, 0, 0, 0);
                else {
                    mBinding.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_side_arrow_wrapper, 0);
                }
            }
        }

    }

    static class StoreViewViewHolder extends ChildViewHolder {
        private final ItemDrawerStartStoreViewBinding mBinding;

        private StoreViewViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
