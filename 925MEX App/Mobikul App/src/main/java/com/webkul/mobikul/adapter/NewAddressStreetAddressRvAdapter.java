package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.NewAddressActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.ItemCheckoutNewAddressStreetBinding;
import com.webkul.mobikul.databinding.ItemNewAddressStreetBinding;
import com.webkul.mobikul.model.customer.address.StreetAddressData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 5/1/17. @Webkul Software Pvt. Ltd
 */
public class NewAddressStreetAddressRvAdapter extends RecyclerView.Adapter<NewAddressStreetAddressRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<StreetAddressData> mStreetAddressDataList;
    private final int mStreetFieldCount;

    public NewAddressStreetAddressRvAdapter(Context context, List<String> streetAddressList, int streetFieldCount) {
        List<StreetAddressData> streetAddressDataList = new ArrayList<>();
        for (int streetIndex = 0; streetIndex < streetFieldCount; streetIndex++) {
            try {
                streetAddressDataList.add(new StreetAddressData(streetAddressList.get(streetIndex)));
            } catch (IndexOutOfBoundsException e) {
                streetAddressDataList.add(new StreetAddressData(""));
            }
        }
        mContext = context;
        mStreetAddressDataList = streetAddressDataList;
        mStreetFieldCount = streetFieldCount;
    }

    public List<StreetAddressData> getStreetAddressList() {
        return mStreetAddressDataList;
    }

    public String getItem(int position) {
        return mStreetAddressDataList.get(position).getStreetValue();
    }

    /**
     * on the basis of first street address Tiet field
     *
     * @return boolean
     */
    public boolean isStreetAddressValidated() {
        if (mStreetAddressDataList.get(0).getStreetValue() == null || mStreetAddressDataList.get(0).getStreetValue().isEmpty()) {
            mStreetAddressDataList.get(0).setStreetValidated(false);
        } else {
            mStreetAddressDataList.get(0).setStreetValidated(true);
        }
        this.notifyDataSetChanged();
        return mStreetAddressDataList.get(0).isStreetValidated();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView;
        if (mContext instanceof NewAddressActivity)
            contactView = inflater.inflate(R.layout.item_new_address_street, parent, false);
        else
            contactView = inflater.inflate(R.layout.item_checkout_new_address_street, parent, false);
        return new NewAddressStreetAddressRvAdapter.ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final StreetAddressData streetAddressData = mStreetAddressDataList.get(position);
        if (mContext instanceof NewAddressActivity) {
            ((ItemNewAddressStreetBinding) holder.mBinding).setData(streetAddressData);
            ((ItemNewAddressStreetBinding) holder.mBinding).setPosition(position);
        } else {
            if (position == 0) {
                Log.d(ApplicationConstant.TAG, "onBindViewHolder: " + streetAddressData.isStreetValidated());
            }
            ((ItemCheckoutNewAddressStreetBinding) holder.mBinding).setData(streetAddressData);
            ((ItemCheckoutNewAddressStreetBinding) holder.mBinding).setPosition(position);
        }
    }

    @Override
    public int getItemCount() {
        return mStreetFieldCount;
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
