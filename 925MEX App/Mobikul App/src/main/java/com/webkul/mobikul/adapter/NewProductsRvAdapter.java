package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemProductGridViewBinding;
import com.webkul.mobikul.handler.HomePageProductHandler;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class NewProductsRvAdapter extends RecyclerView.Adapter<NewProductsRvAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<ProductData> mNewProductsDatas;

    public NewProductsRvAdapter(Context context, ArrayList<ProductData> newProductsDatas) {
        mContext = context;
        mNewProductsDatas = newProductsDatas;
    }

    @Override
    public NewProductsRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_product_grid_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NewProductsRvAdapter.ViewHolder holder, int position) {
        if (position == 0) {
            holder.mBinding.mainLayout.setVisibility(View.GONE);
        } else if (position == (getItemCount() - 1)) {
            holder.mBinding.mainLayout.setVisibility(View.VISIBLE);
            holder.mBinding.productContainer.setVisibility(View.GONE);
            holder.mBinding.addToCompareIv.setVisibility(View.GONE);
            holder.mBinding.viewAllBtn.setVisibility(View.VISIBLE);
            holder.mBinding.wishlistAnimationView.setVisibility(View.GONE);
        } else {
            holder.mBinding.mainLayout.setVisibility(View.VISIBLE);
            holder.mBinding.viewAllBtn.setVisibility(View.GONE);
            holder.mBinding.productContainer.setVisibility(View.VISIBLE);
            holder.mBinding.addToCompareIv.setVisibility(View.VISIBLE);
            holder.mBinding.wishlistAnimationView.setVisibility(View.VISIBLE);
            final ProductData newProductData = mNewProductsDatas.get(position - 1);
            newProductData.setProductPosition(position - 1);
            holder.mBinding.setData(newProductData);
            if (newProductData.isInWishlist())
                holder.mBinding.wishlistAnimationView.setProgress(1);
            else
                holder.mBinding.wishlistAnimationView.setProgress(0);
            holder.mBinding.executePendingBindings();
        }
        holder.mBinding.setHandler(new HomePageProductHandler(mContext, mContext.getString(R.string.category_new_product_identifier), mNewProductsDatas));
    }

    @Override
    public int getItemCount() {
        return mNewProductsDatas.size() + 2;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemProductGridViewBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}