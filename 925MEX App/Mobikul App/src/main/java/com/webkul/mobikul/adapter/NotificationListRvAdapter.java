package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemNotificationListBinding;
import com.webkul.mobikul.handler.NotificationActivityHandler;
import com.webkul.mobikul.helper.DataBaseHelper;
import com.webkul.mobikul.model.notification.NotificationList;

import java.util.List;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */
public class NotificationListRvAdapter extends RecyclerView.Adapter<NotificationListRvAdapter.ViewHolder> {

    private final Context mContext;
    private final List<NotificationList> mNotificationList;
    private DataBaseHelper mDataBaseHelper;

    public NotificationListRvAdapter(Context context, List<NotificationList> notificationList) {
        mContext = context;
        mNotificationList = notificationList;
        mDataBaseHelper = new DataBaseHelper(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new ViewHolder(inflater.inflate(R.layout.item_notification_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final NotificationList notificationListItem = mNotificationList.get(position);
        holder.mBinding.setData(notificationListItem);
        holder.mBinding.setHandler(new NotificationActivityHandler(mContext));
        List<String> notificationIdList = mDataBaseHelper.getAllNotification();

        if (notificationIdList.contains(notificationListItem.getId()))
            holder.mBinding.setIsNewNotification(true);
        else
            holder.mBinding.setIsNewNotification(false);

        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemNotificationListBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}