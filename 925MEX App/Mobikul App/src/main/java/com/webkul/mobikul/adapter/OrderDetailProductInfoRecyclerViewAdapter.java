package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemOrderProductInfoBinding;
import com.webkul.mobikul.model.customer.order.OrderItem;

import java.util.List;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class OrderDetailProductInfoRecyclerViewAdapter extends RecyclerView.Adapter<OrderDetailProductInfoRecyclerViewAdapter.ViewHolder> {
    private final Context mContext;
    private final List<OrderItem> mItems;

    public OrderDetailProductInfoRecyclerViewAdapter(Context context, List<OrderItem> items) {
        mContext = context;
        mItems = items;
    }

    @Override
    public OrderDetailProductInfoRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_order_product_info, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(OrderDetailProductInfoRecyclerViewAdapter.ViewHolder holder, int position) {
        final OrderItem orderItem = mItems.get(position);
        holder.mBinding.setData(orderItem);

        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemOrderProductInfoBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
