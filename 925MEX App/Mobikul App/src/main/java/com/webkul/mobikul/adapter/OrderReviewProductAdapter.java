package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemOrderReviewProductBinding;
import com.webkul.mobikul.model.checkout.OrderReviewItemProduct;

import java.util.List;


/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class OrderReviewProductAdapter extends RecyclerView.Adapter<OrderReviewProductAdapter.ViewHolder> {

    private final Context mContext;
    private final List<OrderReviewItemProduct> mOrderReviewItemProducts;

    public OrderReviewProductAdapter(Context context, List<OrderReviewItemProduct> orderReviewItemProducts) {
        mContext = context;
        mOrderReviewItemProducts = orderReviewItemProducts;
    }

    @Override
    public OrderReviewProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_order_review_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final OrderReviewProductAdapter.ViewHolder holder, int position) {
        final OrderReviewItemProduct orderReviewItemProduct = mOrderReviewItemProducts.get(position);
        holder.mBinding.setData(orderReviewItemProduct);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mOrderReviewItemProducts.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemOrderReviewProductBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
