package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemProductAdditionalInfoBinding;
import com.webkul.mobikul.model.product.AdditionalInformationData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 3/1/17. @Webkul Software Pvt. Ltd
 */

public class ProductAdditionInfoRvAdapter extends RecyclerView.Adapter<ProductAdditionInfoRvAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<AdditionalInformationData> mAdditionInfoData;

    public ProductAdditionInfoRvAdapter(Context context, ArrayList<AdditionalInformationData> additionInfoData) {
        mContext = context;
        mAdditionInfoData = additionInfoData;
    }

    @Override
    public ProductAdditionInfoRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_product_additional_info, parent, false);
        return new ProductAdditionInfoRvAdapter.ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(ProductAdditionInfoRvAdapter.ViewHolder holder, int position) {
        AdditionalInformationData eachAdditionInfoData = mAdditionInfoData.get(position);
        holder.mBinding.setData(eachAdditionInfoData);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mAdditionInfoData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemProductAdditionalInfoBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
