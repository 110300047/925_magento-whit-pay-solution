package com.webkul.mobikul.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemAttributeSwatchBinding;
import com.webkul.mobikul.fragment.ProductFragment;
import com.webkul.mobikul.handler.CatalogAttributesSwatchHandler;
import com.webkul.mobikul.model.catalog.SwatchData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class ProductAttributesSwatchRvAdapter extends RecyclerView.Adapter<ProductAttributesSwatchRvAdapter.ViewHolder> {

    private ProductFragment mContext;
    private int mRecyclerViewTag;
    private boolean mUpdateProductPreviewImage;
    private ArrayList<SwatchData> mCatalogAttributeSwatchValuesDatas;

    public ProductAttributesSwatchRvAdapter(ProductFragment context, int recyclerViewTag, boolean updateProductPreviewImage, ArrayList<SwatchData> catalogAttributeSwatchValuesDatas) {
        mContext = context;
        mRecyclerViewTag = recyclerViewTag;
        mUpdateProductPreviewImage = updateProductPreviewImage;
        mCatalogAttributeSwatchValuesDatas = catalogAttributeSwatchValuesDatas;
    }

    @Override
    public ProductAttributesSwatchRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext.getContext());
        View view = inflater.inflate(R.layout.item_attribute_swatch, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductAttributesSwatchRvAdapter.ViewHolder holder, int position) {
        final SwatchData productSwatchData = mCatalogAttributeSwatchValuesDatas.get(position);
        productSwatchData.setPosition(position);
        holder.mBinding.setData(productSwatchData);
        holder.mBinding.setHandler(new CatalogAttributesSwatchHandler(null, mContext, mRecyclerViewTag, mUpdateProductPreviewImage, mCatalogAttributeSwatchValuesDatas));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCatalogAttributeSwatchValuesDatas.size();
    }

    public ArrayList<SwatchData> getSwatchValuesData() {
        return mCatalogAttributeSwatchValuesDatas;
    }

    public void setSwatchValuesData(ArrayList<SwatchData> catalogAttributeSwatchValuesDatas) {
        mCatalogAttributeSwatchValuesDatas = catalogAttributeSwatchValuesDatas;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemAttributeSwatchBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}