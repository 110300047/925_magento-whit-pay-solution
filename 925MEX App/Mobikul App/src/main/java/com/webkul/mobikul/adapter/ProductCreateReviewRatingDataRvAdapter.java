package com.webkul.mobikul.adapter;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemProductCreateReviewRatingBinding;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.model.product.RatingFormData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by vedesh.kumar on 3/1/17. @Webkul Software Pvt. Ltd
 */
public class ProductCreateReviewRatingDataRvAdapter extends RecyclerView.Adapter<ProductCreateReviewRatingDataRvAdapter.ViewHolder> {
    @SuppressWarnings("unused")
    private final Context mContext;
    private final List<RatingFormData> mRatingFormData;

    public ProductCreateReviewRatingDataRvAdapter(Context context, List<RatingFormData> ratingFormData) {
        mContext = context;
        mRatingFormData = ratingFormData;
    }

    public JSONObject getRatingData() {
        JSONObject ratingJsonObject = new JSONObject();
        for (int ratingId = 0; ratingId < mRatingFormData.size(); ratingId++) {
            try {
                ratingJsonObject.put(String.valueOf(mRatingFormData.get(ratingId).getId()), mRatingFormData.get(ratingId).getSelectedRatingValue());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ratingJsonObject;
    }

    public boolean isRatingFilled() {
        for (int ratingIdx = 0; ratingIdx < mRatingFormData.size(); ratingIdx++) {
            if (mRatingFormData.get(ratingIdx).getRatingValue() == 0.0) {
                SnackbarHelper.getSnackbar((Activity) mContext, mContext.getString(R.string.alert_please_select_one_of_each_rating)).show();
                return false;
            }
        }
        return true;
    }

    @Override
    public ProductCreateReviewRatingDataRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_product_create_review_rating, parent, false);
        return new ProductCreateReviewRatingDataRvAdapter.ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(ProductCreateReviewRatingDataRvAdapter.ViewHolder holder, int position) {
        holder.mBinding.setData(mRatingFormData.get(position));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mRatingFormData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemProductCreateReviewRatingBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}