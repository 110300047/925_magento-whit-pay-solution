package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemProductReviewBinding;
import com.webkul.mobikul.model.product.ProductReviewData;

import java.util.List;


/**
 * Created by vedesh.kumar on 15/12/16. @Webkul Software Pvt. Ltd
 */

public class ProductReviewsRvAdapter extends RecyclerView.Adapter<ProductReviewsRvAdapter.ViewHolder> {


    private final Context mContext;
    private final List<ProductReviewData> mReviewListData;

    public ProductReviewsRvAdapter(Context context, List<ProductReviewData> reviewListData) {
        mContext = context;
        mReviewListData = reviewListData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_product_review, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ProductReviewData productReviewData = mReviewListData.get(position);
        holder.mBinding.setData(productReviewData);
        holder.mBinding.executePendingBindings();

        holder.mBinding.ratingDataRv.setLayoutManager(new LinearLayoutManager(mContext));
        holder.mBinding.ratingDataRv.setAdapter(new ProductReviewRatingDataRecyclerViewAdapter(mContext, productReviewData.getRatings()));
    }


    @Override
    public int getItemCount() {
        return mReviewListData.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemProductReviewBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }

}
