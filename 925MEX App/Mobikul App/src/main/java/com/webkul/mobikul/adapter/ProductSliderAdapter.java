package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemViewPagerProductBannerBinding;
import com.webkul.mobikul.handler.ProductSliderHandler;
import com.webkul.mobikul.model.product.ImageGalleryData;

import java.util.ArrayList;


/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */
public class ProductSliderAdapter extends PagerAdapter {
    private final Context mContext;
    private final ArrayList<ImageGalleryData> mImageGalleryData;
    private String mProductName;

    public ProductSliderAdapter(Context context, String productName, ArrayList<ImageGalleryData> imageGalleryData) {
        mContext = context;
        mProductName = productName;
        mImageGalleryData = imageGalleryData;
    }

    @Override
    public int getCount() {
        return mImageGalleryData.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ItemViewPagerProductBannerBinding itemViewPagerProductBannerBinding = DataBindingUtil.bind(inflater.inflate(R.layout.item_view_pager_product_banner, container, false));
        itemViewPagerProductBannerBinding.setPosition(position);
        itemViewPagerProductBannerBinding.setData(mImageGalleryData.get(position));
        itemViewPagerProductBannerBinding.setProductName(mProductName);
        itemViewPagerProductBannerBinding.setHandler(new ProductSliderHandler(mImageGalleryData));
        itemViewPagerProductBannerBinding.executePendingBindings();
        container.addView(itemViewPagerProductBannerBinding.getRoot());
        return (itemViewPagerProductBannerBinding.getRoot());
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}

