package com.webkul.mobikul.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemRelatedProductGridViewBinding;
import com.webkul.mobikul.fragment.ProductFragment;
import com.webkul.mobikul.handler.RelatedProductRvHandler;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class RelatedProductsRvAdapter extends RecyclerView.Adapter<RelatedProductsRvAdapter.ViewHolder> {

    private final ProductFragment mContext;
    private final ArrayList<ProductData> mRelatedProductsDatas;

    public RelatedProductsRvAdapter(ProductFragment context, ArrayList<ProductData> relatedProductsDatas) {
        mContext = context;
        mRelatedProductsDatas = relatedProductsDatas;
    }

    @Override
    public RelatedProductsRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext.getContext());
        View view = inflater.inflate(R.layout.item_related_product_grid_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RelatedProductsRvAdapter.ViewHolder holder, int position) {
        final ProductData relatedProductData = mRelatedProductsDatas.get(position);
        relatedProductData.setProductPosition(position);
        holder.mBinding.setData(relatedProductData);
        holder.mBinding.setHandler(new RelatedProductRvHandler(mContext, mRelatedProductsDatas));
        if (relatedProductData.isInWishlist())
            holder.mBinding.addToWishlistIv.setProgress(1);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mRelatedProductsDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemRelatedProductGridViewBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}