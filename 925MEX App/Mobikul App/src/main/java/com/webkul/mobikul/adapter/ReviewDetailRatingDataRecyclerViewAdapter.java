package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemReviewRatingBinding;
import com.webkul.mobikul.model.product.ProductRatingData;

import java.util.List;


/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class ReviewDetailRatingDataRecyclerViewAdapter extends RecyclerView.Adapter<ReviewDetailRatingDataRecyclerViewAdapter.ViewHolder> {
    private final Context mContext;
    private final List<ProductRatingData> mProductRatingDatas;

    public ReviewDetailRatingDataRecyclerViewAdapter(Context context, List<ProductRatingData> productRatingDatas) {
        mContext = context;
        mProductRatingDatas = productRatingDatas;
    }

    @Override
    public ReviewDetailRatingDataRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_review_rating, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(ReviewDetailRatingDataRecyclerViewAdapter.ViewHolder holder, int position) {
        final ProductRatingData productRatingData = mProductRatingDatas.get(position);
        holder.mBinding.setData(productRatingData);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mProductRatingDatas.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemReviewRatingBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
