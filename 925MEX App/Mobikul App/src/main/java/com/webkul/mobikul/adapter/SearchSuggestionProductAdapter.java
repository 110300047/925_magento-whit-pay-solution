package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemSearchSuggestionProductBinding;
import com.webkul.mobikul.handler.SearchSuggestionProductHandler;
import com.webkul.mobikul.model.extra.SuggestionProductData;

import java.util.List;


/**
 * Created by vedesh.kumar on 4/5/17.
 */

public class SearchSuggestionProductAdapter extends RecyclerView.Adapter<SearchSuggestionProductAdapter.ViewHolder> {


    private final Context mContext;
    private List<SuggestionProductData> mProducts;

    public SearchSuggestionProductAdapter(Context context, List<SuggestionProductData> products) {
        mContext = context;
        mProducts = products;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_search_suggestion_product, parent, false);
        return new ViewHolder(contactView);
    }


    public void updateData(List<SuggestionProductData> products) {
        this.mProducts = products;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setData(mProducts.get(position));
        holder.mBinding.setHandler(new SearchSuggestionProductHandler(mContext, mProducts.get(position)));
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemSearchSuggestionProductBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
