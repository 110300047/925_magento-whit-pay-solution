package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemSearchSuggestionTagsBinding;
import com.webkul.mobikul.handler.SearchSuggestionTagHandler;
import com.webkul.mobikul.model.extra.SuggestionTagData;

import java.util.List;


/**
 * Created by vedesh.kumar on 4/5/17.
 */

public class SearchSuggestionTagAdapter extends RecyclerView.Adapter<SearchSuggestionTagAdapter.ViewHolder> {


    private final Context mContext;
    private List<SuggestionTagData> mTags;

    public SearchSuggestionTagAdapter(Context context, List<SuggestionTagData> tags) {
        mContext = context;
        mTags = tags;
    }


    public void updateData(List<SuggestionTagData> tags) {
        this.mTags = tags;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_search_suggestion_tags, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setData(mTags.get(position));
        holder.mBinding.setHandler(new SearchSuggestionTagHandler(mContext, mTags.get(position)));
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemSearchSuggestionTagsBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
