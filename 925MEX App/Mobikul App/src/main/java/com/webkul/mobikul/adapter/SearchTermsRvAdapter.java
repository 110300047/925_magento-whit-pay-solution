package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemSearchTermBinding;
import com.webkul.mobikul.handler.SearchTermsActivityHandler;
import com.webkul.mobikul.model.search.TermList;

import java.util.List;

import static com.webkul.mobikul.helper.Utils.setAnimation;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Pvt. Ltd
 */
public class SearchTermsRvAdapter extends RecyclerView.Adapter<SearchTermsRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<TermList> mSearchTermList;
    private boolean animation = true;

    public SearchTermsRvAdapter(Context context, List<TermList> searchTermList) {
        mContext = context;
        mSearchTermList = searchTermList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new ViewHolder(inflater.inflate(R.layout.item_search_term, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TermList searchTermItem = mSearchTermList.get(position);
        holder.mBinding.setData(searchTermItem);
        holder.mBinding.setHandler(new SearchTermsActivityHandler(mContext));
        holder.mBinding.executePendingBindings();

        if (animation) {
            animation = false;
            setAnimation(holder.itemView, position, android.R.anim.slide_in_left, mContext);
        } else {
            animation = true;
            setAnimation(holder.itemView, position, R.anim.slide_in_right, mContext);
        }
    }

    @Override
    public int getItemCount() {
        return mSearchTermList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemSearchTermBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
