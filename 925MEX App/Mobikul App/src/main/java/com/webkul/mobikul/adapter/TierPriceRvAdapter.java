package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemTierPriceBinding;

import java.util.List;

/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */

public class TierPriceRvAdapter extends RecyclerView.Adapter<TierPriceRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<String> mTierPrices;

    public TierPriceRvAdapter(Context context, List<String> tierPrices) {
        mContext = context;
        mTierPrices = tierPrices;
    }

    @Override
    public TierPriceRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_tier_price, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(TierPriceRvAdapter.ViewHolder holder, int position) {
        final String tierPrice = mTierPrices.get(position);
        holder.mBinding.setData(tierPrice);
        holder.mBinding.setPosition(position);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mTierPrices.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemTierPriceBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
