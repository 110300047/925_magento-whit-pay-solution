package com.webkul.mobikul.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemUpsellProductGridViewBinding;
import com.webkul.mobikul.fragment.ProductFragment;
import com.webkul.mobikul.handler.UpsellProductRvHandler;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class UpsellProductsRvAdapter extends RecyclerView.Adapter<UpsellProductsRvAdapter.ViewHolder> {

    private final ProductFragment mContext;
    private final ArrayList<ProductData> mUpsellProductsDatas;

    public UpsellProductsRvAdapter(ProductFragment context, ArrayList<ProductData> upsellProductsDatas) {
        mContext = context;
        mUpsellProductsDatas = upsellProductsDatas;
    }

    @Override
    public UpsellProductsRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext.getContext());
        View view = inflater.inflate(R.layout.item_upsell_product_grid_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UpsellProductsRvAdapter.ViewHolder holder, int position) {
        final ProductData upsellProductsDatas = mUpsellProductsDatas.get(position);
        upsellProductsDatas.setProductPosition(position);
        holder.mBinding.setData(upsellProductsDatas);
        holder.mBinding.setHandler(new UpsellProductRvHandler(mContext, mUpsellProductsDatas));
        if (upsellProductsDatas.isInWishlist())
            holder.mBinding.addToWishlistIv.setProgress(1);
        else
            holder.mBinding.addToWishlistIv.setProgress(0);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mUpsellProductsDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemUpsellProductGridViewBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}