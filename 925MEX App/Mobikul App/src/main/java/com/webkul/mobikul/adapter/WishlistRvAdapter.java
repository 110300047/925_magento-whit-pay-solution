package com.webkul.mobikul.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemWishlistBinding;
import com.webkul.mobikul.handler.WishlistItemHandler;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.cart.CartOptionItem;
import com.webkul.mobikul.model.customer.wishlist.WishlistItemData;

import java.util.List;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class WishlistRvAdapter extends RecyclerView.Adapter<WishlistRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<WishlistItemData> mWishlistItemDatas;


    public WishlistRvAdapter(Context context, List<WishlistItemData> wishlistItemDatas) {
        mContext = context;
        mWishlistItemDatas = wishlistItemDatas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_wishlist, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final WishlistItemData wishlistItemData = mWishlistItemDatas.get(position);
        wishlistItemData.setPosition(position);
        holder.mBinding.setData(wishlistItemData);
        holder.mBinding.setHandler(new WishlistItemHandler(mContext, this));
        holder.mBinding.updateBtn.setTag(position);
        holder.mBinding.incrementQtyIv.setTag(position);
        holder.mBinding.decrementQtyIv.setTag(position);
        if (wishlistItemData.getItemOption() != null && wishlistItemData.getItemOption().size() > 0) {
            holder.mBinding.optionTableLayout.removeAllViews();
            for (int optionIterator = 0; optionIterator < wishlistItemData.getItemOption().size(); optionIterator++) {

                CartOptionItem optionItem = wishlistItemData.getItemOption().get(optionIterator);

                TableRow tableRow = new TableRow(mContext);
                tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                tableRow.setPadding(1, 1, 3, 1);
                tableRow.setBackgroundResource(R.drawable.shape_rectangular_white_bg_gray_border_1dp);
                tableRow.setGravity(Gravity.CENTER_VERTICAL);

                TextView labelTv = new TextView(mContext);
                labelTv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
                labelTv.setMaxWidth((int) (Utils.getScreenWidth() / 2.5));
                labelTv.setPadding(10, 5, 10, 5);
                labelTv.setTextSize(10);
                labelTv.setBackgroundResource(R.color.grey_200);
                labelTv.setText(optionItem.getLabel());
                tableRow.addView(labelTv);

                TextView val = new TextView(mContext);
                val.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
                val.setPadding(10, 5, 10, 5);
                val.setTextSize(10);
                String value = optionItem.getValue().get(0);
                for (int noOfValues = 1; noOfValues < optionItem.getValue().size(); noOfValues++) {
                    value = value + "\n" + optionItem.getValue().get(noOfValues);
                }
                val.setText(value);
                tableRow.addView(val);
                holder.mBinding.optionTableLayout.addView(tableRow);
            }
        }
        holder.mBinding.executePendingBindings();
    }

    public WishlistItemData getWishlistItem(int position) {
        return mWishlistItemDatas.get(position);
    }

    public List<WishlistItemData> getWishlistDataList() {
        return mWishlistItemDatas;
    }

    public void remove(int position) {
        mWishlistItemDatas.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return mWishlistItemDatas.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemWishlistBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
