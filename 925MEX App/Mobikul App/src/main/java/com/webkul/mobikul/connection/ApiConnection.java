package com.webkul.mobikul.connection;

import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.CMSPageDataResponse;
import com.webkul.mobikul.model.cart.CartDetailsResponse;
import com.webkul.mobikul.model.catalog.CatalogProductData;
import com.webkul.mobikul.model.catalog.CatalogSingleProductData;
import com.webkul.mobikul.model.catalog.HomePageResponseData;
import com.webkul.mobikul.model.checkout.BillingShippingInfoData;
import com.webkul.mobikul.model.checkout.OpenPayResponseData;
import com.webkul.mobikul.model.checkout.OrderReviewRequestData;
import com.webkul.mobikul.model.checkout.SaveOrderResponse;
import com.webkul.mobikul.model.checkout.ShippingMethodInfoData;
import com.webkul.mobikul.model.compare.CompareListData;
import com.webkul.mobikul.model.customer.accountInfo.AccountInfoResponseData;
import com.webkul.mobikul.model.customer.accountInfo.SaveAccountInfoResponseData;
import com.webkul.mobikul.model.customer.address.AddressBookResponseData;
import com.webkul.mobikul.model.customer.address.AddressFormResponseData;
import com.webkul.mobikul.model.customer.address.CheckCustomerByEmailResponseData;
import com.webkul.mobikul.model.customer.downloadable.DownloadProductResponse;
import com.webkul.mobikul.model.customer.downloadable.DownloadableProductListData;
import com.webkul.mobikul.model.customer.order.OrderDetailResponseData;
import com.webkul.mobikul.model.customer.order.OrderListResponseData;
import com.webkul.mobikul.model.customer.order.ReorderResponseData;
import com.webkul.mobikul.model.customer.review.ReviewDetailsResponseData;
import com.webkul.mobikul.model.customer.review.ReviewListResponseData;
import com.webkul.mobikul.model.customer.signin.SignInForgotPasswordResponse;
import com.webkul.mobikul.model.customer.signin.SignInResponseData;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormData;
import com.webkul.mobikul.model.customer.signup.SignUpResponseData;
import com.webkul.mobikul.model.customer.wishlist.AddToCartFromWishListResponse;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductAddToWishlistResponse;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductRemoveFromWishlistResponse;
import com.webkul.mobikul.model.customer.wishlist.WishListResponseData;
import com.webkul.mobikul.model.extra.SearchSuggestionResponse;
import com.webkul.mobikul.model.firebase.RefreshToken;
import com.webkul.mobikul.model.notification.NotificationResponseData;
import com.webkul.mobikul.model.notification.OtherNotificationResponseData;
import com.webkul.mobikul.model.product.ProductAddToCartResponse;
import com.webkul.mobikul.model.search.AdvanceSearchFormData;
import com.webkul.mobikul.model.search.SearchTermsResponseData;

import org.json.JSONArray;
import org.json.JSONObject;

import io.reactivex.Observable;

import static com.webkul.mobikul.connection.RetrofitClient.getClient;
import static com.webkul.mobikul.constants.ApplicationConstant.LOG_PARAMS;
import static com.webkul.mobikul.constants.ApplicationConstant.LOG_RESPONSE;

/**
 * Created by vedesh.kumar on 16/3/17. @Webkul Software Pvt. Ltd
 */

public class ApiConnection {

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        CATALOG API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    //ApiConnection para paypal
    public static Observable<OrderReviewRequestData> getOrderReviewDataPaypal(int storeId, int quoteId, int customerId, String device_session_id, String paypal_token, String paymentMethod, String currency) {
        return getClient().create(ApiInterface.class).getOrderReviewDataPaypal(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , device_session_id
                , paypal_token
                , paymentMethod
                , currency
        );
    }

    public static Observable<HomePageResponseData> getHomePageData(int storeId, int quoteId, int customerId, int width, int websiteId, float density, int isFromUrl, String url, String currency) {
        return getClient().create(ApiInterface.class).getHomePageData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , width
                , websiteId
                , density
                , isFromUrl
                , url
                , currency
        );
    }

    public static Observable<CatalogProductAddToWishlistResponse> addToWishlist(int storeId, int productId, int customerId, JSONObject params) {
        return getClient().create(ApiInterface.class).addToWishlist(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , productId
                , customerId
                , params
        );
    }

    public static Observable<CatalogProductData> getCatalogProductData(int categoryId, int storeId, int width, int pageNumber, int customerId, JSONArray sortData, JSONArray filterData, Float mFactor, int quoteId, String currency) {
        return getClient().create(ApiInterface.class).getCatalogProductData(
                LOG_PARAMS
                , LOG_RESPONSE

                , categoryId
                , storeId
                , width
                , pageNumber
                , customerId
                , sortData
                , filterData
                , mFactor
                , quoteId
                , currency
        );
    }

    public static Observable<CatalogProductData> getCatalogSearchResult(int customerId, String searchQuery, int storeId, int width, int pageNumber, JSONArray sortData, JSONArray filterData, String currency) {
        return getClient().create(ApiInterface.class).getCatalogSearchResult(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , searchQuery
                , storeId
                , width
                , pageNumber
                , sortData
                , filterData
                , currency
                , 1
        );
    }

    public static Observable<AdvanceSearchFormData> getAdvanceSearchFormData(int storeId, String currency) {
        return getClient().create(ApiInterface.class).getAdvanceSearchFormData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , currency
                , 1
        );
    }

    public static Observable<CatalogProductData> getAdvanceSearchResult(int storeId, JSONObject queryString, JSONArray sortData, JSONArray filterData, int width, int pageNumber, String currency) {
        return getClient().create(ApiInterface.class).getAdvanceSearchResult(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , queryString
                , sortData
                , filterData
                , width
                , pageNumber
                , currency
                , 1
        );
    }

    public static Observable<CatalogSingleProductData> getCatalogSingleProductData(int storeId, int productId, int width, int quoteId, int customerId, String currency) {
        return getClient().create(ApiInterface.class).getCatalogSingleProductData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , productId
                , width
                , quoteId
                , customerId
                , currency
        );
    }

    public static Observable<CatalogProductData> getFeaturedProductData(int storeId, int width, int pageNumber, int customerId, int quoteId, JSONArray sortData, JSONArray filterData, String currency) {
        return getClient().create(ApiInterface.class).getFeaturedProductData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , width
                , pageNumber
                , customerId
                , quoteId
                , sortData
                , filterData
                , currency
                , 1
        );
    }

    public static Observable<CatalogProductData> getNewProductData(int storeId, int width, int pageNumber, int customerId, int quoteId, JSONArray sortData, JSONArray filterData, String currency) {
        return getClient().create(ApiInterface.class).getNewProductData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , width
                , pageNumber
                , customerId
                , quoteId
                , sortData
                , filterData
                , currency
                , 1
        );
    }

    public static Observable<CatalogProductData> getHotDealsProductData(int storeId, int width, int pageNumber, int customerId, int quoteId, JSONArray sortData, JSONArray filterData, String currency) {
        return getClient().create(ApiInterface.class).getHotDealsProductData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , width
                , pageNumber
                , customerId
                , quoteId
                , sortData
                , filterData
                , currency
                , 1
        );
    }

    public static Observable<BaseModel> productShare(int storeId, int productId, String customerName, String customerEmail, String message, JSONArray recipientName, JSONArray recipientEmail) {
        return getClient().create(ApiInterface.class).productShare(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , productId
                , customerName
                , customerEmail
                , message
                , recipientName
                , recipientEmail
        );
    }

    public static Observable<BaseModel> addToCompare(int storeId, int customerId, int productId) {
        return getClient().create(ApiInterface.class).addToCompare(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , productId
        );
    }

    public static Observable<CompareListData> getCompareList(int storeId, int customerId, int width, String currency) {
        return getClient().create(ApiInterface.class).getCompareList(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , width
                , currency
        );
    }

    public static Observable<BaseModel> removeFromCompareList(int storeId, int customerId, int productId) {
        return getClient().create(ApiInterface.class).removeFromCompareList(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , productId
        );
    }



    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        CUSTOMER API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static Observable<SignInResponseData> getSignInData(String username, String password, int width, int quoteId, int storeId, int websiteId, float density, String token, String mobile, String os) {
        return getClient().create(ApiInterface.class).getSignInData(
                LOG_PARAMS
                , LOG_RESPONSE

                , username
                , password
                , width
                , quoteId
                , storeId
                , websiteId
                , density
                , token
                , mobile
                , os
        );
    }

    public static Observable<CreateAccountFormData> getCreateAccountFormData(int storeId) {
        return getClient().create(ApiInterface.class).getCreateAccountFormData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
        );
    }

    public static Observable<SignUpResponseData> getSignUpData(String token, int storeId, int websiteId, int quoteId, String prefix, String firstName, String middleName, String lastName, String suffix, String dob, String taxvat, int gender, String email, String password, String pictureURL, int isSocial, String mobile, String storeUrl, boolean signupAsSeller, String os) {
        return getClient().create(ApiInterface.class).getSignUpData(
                LOG_PARAMS
                , LOG_RESPONSE

                , token
                , storeId
                , websiteId
                , quoteId
                , prefix
                , firstName
                , middleName
                , lastName
                , suffix
                , dob
                , taxvat
                , gender
                , email
                , password
                , pictureURL
                , isSocial
                , mobile
                , storeUrl
                , signupAsSeller ? 1 : 0
                , os
        );
    }

    public static Observable<SignInForgotPasswordResponse> forgotSignInPassword(int storeId, String emailAddress, int websiteId) {
        return getClient().create(ApiInterface.class).forgotSignInPassword(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , emailAddress
                , websiteId
        );
    }

    public static Observable<AccountInfoResponseData> getAccountInfo(int storeId, int customerId) {
        return getClient().create(ApiInterface.class).getAccountInfo(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
        );
    }

    public static Observable<SaveAccountInfoResponseData> saveAccountInfo(int customerId, String prefixValue, String firstName, String middleName, String lastName, String emailAddress, String dobValue, String taxvat, String suffixValue, int gender, int doChangeEmail, int doChangePassword, String currentPassword, String newPassword, String confirmPassword, String mobile) {
        return getClient().create(ApiInterface.class).saveAccountInfo(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , prefixValue
                , firstName
                , middleName
                , lastName
                , emailAddress
                , dobValue
                , taxvat
                , suffixValue
                , gender
                , doChangeEmail
                , doChangePassword
                , currentPassword
                , newPassword
                , confirmPassword
                , mobile
        );
    }

    public static Observable<AddressBookResponseData> getAddressBookData(int customerId, int storeId) {
        return getClient().create(ApiInterface.class).getAddressBookData(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , storeId
        );
    }

    public static Observable<OrderListResponseData> getOrderListData(int customerId, int storeId, int pageNumber) {
        return getClient().create(ApiInterface.class).getOrderListData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , pageNumber
        );
    }

    public static Observable<ReviewListResponseData> getReviewListData(int customerId, int pageNumber, int storeId, int width) {
        return getClient().create(ApiInterface.class).getReviewListData(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , pageNumber
                , storeId
                , width
        );
    }

    public static Observable<OrderDetailResponseData> getOrderDetailsData(String incrementId, int storeId, String currency) {
        return getClient().create(ApiInterface.class).getOrderDetailsData(
                LOG_PARAMS
                , LOG_RESPONSE

                , incrementId
                , storeId
                , currency
        );
    }

    public static Observable<CatalogProductRemoveFromWishlistResponse> removeItemFromWishlist(int customerId, int storeId, int itemId) {
        return getClient().create(ApiInterface.class).removeItemFromWishlist(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , storeId
                , itemId
        );
    }

    public static Observable<ReorderResponseData> reorder(int customerId, String incrementId, int storeId) {
        return getClient().create(ApiInterface.class).reorder(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , incrementId
                , storeId
        );
    }

    public static Observable<ReviewDetailsResponseData> getReviewDetails(int storeId, int width, int reviewId) {
        return getClient().create(ApiInterface.class).getReviewDetails(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , width
                , reviewId
        );
    }

    public static Observable<AddressFormResponseData> getAddressFormData(int customerId, int storeId, int addressId) {
        return getClient().create(ApiInterface.class).getAddressFormData(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , storeId
                , addressId
        );
    }

    public static Observable<BaseModel> saveAddress(int storeId, int customerId, int addressId, JSONObject addressData) {
        return getClient().create(ApiInterface.class).saveAddress(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , addressId
                , addressData
        );
    }

    public static Observable<WishListResponseData> getWishListData(int storeId, int customerId, int width, int pageNumber) {
        return getClient().create(ApiInterface.class).getWishListData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , width
                , pageNumber
        );
    }

    public static Observable<BaseModel> updateWishlist(int customerId, JSONArray itemData) {
        return getClient().create(ApiInterface.class).updateWishlist(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , itemData
        );
    }

    public static Observable<AddToCartFromWishListResponse> addToCartFromWishlist(int customerId, int storeId, int productId, int itemId, int qty) {
        return getClient().create(ApiInterface.class).addToCartFromWishlist(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , storeId
                , productId
                , itemId
                , qty
        );
    }

    public static Observable<BaseModel> deleteAddress(int addressId) {
        return getClient().create(ApiInterface.class).deleteAddress(
                LOG_PARAMS
                , LOG_RESPONSE

                , addressId
        );
    }

    public static Observable<DownloadableProductListData> getMyDownloadsList(int customerId, int pageNumber) {
        return getClient().create(ApiInterface.class).getMyDownloadsList(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , pageNumber
        );
    }

    public static Observable<DownloadProductResponse> downloadProduct(int customerId, String hash) {
        return getClient().create(ApiInterface.class).downloadProduct(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , hash
        );
    }

    public static Observable<BaseModel> saveReview(int customerId, int storeId, int id, String title, String detail, String nickname, JSONObject ratingObj) {
        return getClient().create(ApiInterface.class).saveReview(
                LOG_PARAMS
                , LOG_RESPONSE

                , customerId
                , storeId
                , id
                , title
                , detail
                , nickname
                , ratingObj
        );
    }




    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        CHECKOUT API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static Observable<ProductAddToCartResponse> addToCart(int storeId, int productId, int qty, JSONObject params, JSONArray relatedProducts, int quoteId, int customerId) {
        return getClient().create(ApiInterface.class).addToCart(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , productId
                , qty
                , params
                , relatedProducts
                , quoteId
                , customerId
        );
    }

    public static Observable<ProductAddToCartResponse> updateproduct(int storeId, int productId, int qty, JSONObject params, JSONArray relatedProducts, int quoteId, int customerId, int itemId) {
        return getClient().create(ApiInterface.class).updateproduct(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , productId
                , qty
                , params
                , relatedProducts
                , quoteId
                , customerId
                , itemId
        );
    }

    public static Observable<CartDetailsResponse> getCartDetails(int storeId, int width, int quoteId, int customerId, String currency) {
        return getClient().create(ApiInterface.class).getCartDetails(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , width
                , quoteId
                , customerId
                , currency
        );
    }

    public static Observable<CartDetailsResponse> moveToWishlist(int storeId, int customerId, int itemId) {
        return getClient().create(ApiInterface.class).moveToWishlist(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , itemId
        );
    }

    public static Observable<CartDetailsResponse> removeCartItem(int storeId, int quoteId, int customerId, int itemId) {
        return getClient().create(ApiInterface.class).removeCartItem(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , itemId
        );
    }

    public static Observable<BaseModel> emptyCart(int storeId, int quoteId, int customerId) {
        return getClient().create(ApiInterface.class).emptyCart(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
        );
    }

    public static Observable<BaseModel> updateCart(int storeId, int quoteId, int customerId, JSONArray itemIds, JSONArray itemQtys) {
        return getClient().create(ApiInterface.class).updateCart(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , itemIds
                , itemQtys
        );
    }

    public static Observable<BaseModel> applyOrCancelCoupon(int storeId, int quoteId, int customerId, String couponCode, int removeCoupon) {
        return getClient().create(ApiInterface.class).applyOrCancelCoupon(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , couponCode
                , removeCoupon
        );
    }

    public static Observable<BillingShippingInfoData> getBillingShippingInfoData(int storeId, int quoteId, int customerId) {
        return getClient().create(ApiInterface.class).getBillingShippingInfoData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
        );
    }

    public static Observable<ShippingMethodInfoData> getShippingMethodInfoData(int storeId, int quoteId, int customerId, String checkoutMethod, JSONObject billingData, JSONObject shippingData, String currency) {
        return getClient().create(ApiInterface.class).getShippingMethodInfoData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , checkoutMethod
                , billingData
                , shippingData
                , currency
        );
    }

    /*public static Observable<OrderReviewRequestData> getOrderReviewData(int storeId, int quoteId, int customerId, String paymentMethod, String currency) {
        return getClient().create(ApiInterface.class).getOrderReviewData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , paymentMethod
                , currency
        );
    }*/

    public static Observable<OrderReviewRequestData> getOrderReviewData(int storeId, int quoteId, int customerId, String shippingMethod, String cc_cid, String cc_exp_month, String cc_exp_year, String cc_number, String cc_type, String device_session_id, String openpay_token, String paymentMethod, String currency) {
        return getClient().create(ApiInterface.class).getOrderReviewData(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , shippingMethod
                , cc_cid
                , cc_exp_month
                , cc_exp_year
                , cc_number
                , cc_type
                , device_session_id
                , openpay_token
                , paymentMethod
                , currency
        );
    }

    public static Observable<SaveOrderResponse> saveOrder(int storeId, int quoteId, int customerId, String token) {
        return getClient().create(ApiInterface.class).saveOrder(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , quoteId
                , customerId
                , token
        );
    }

    public static Observable<BaseModel> accountCreateEmail(int storeId, String orderId) {
        return getClient().create(ApiInterface.class).accountCreateEmail(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , orderId
        );
    }

    public static Observable<CheckCustomerByEmailResponseData> checkCustomerByEmail(int storeId, String email) {
        return getClient().create(ApiInterface.class).checkCustomerByEmail(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , email
        );
    }



    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        EXTRAS API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static Observable<RefreshToken> uploadTokenData(String token, int customerId, String os) {
        return getClient().create(ApiInterface.class).uploadTokenData(
                LOG_PARAMS
                , LOG_RESPONSE

                , token
                , customerId
                , os
        );
    }

    public static Observable<SearchTermsResponseData> getSearchTermsList(int storeId) {
        return getClient().create(ApiInterface.class).getSearchTermsList(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
        );
    }

    public static Observable<NotificationResponseData> getNotificationsList(int storeId, int width, float mFactor) {
        return getClient().create(ApiInterface.class).getNotificationsList(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , width
                , mFactor
        );
    }

    public static Observable<OtherNotificationResponseData> getOtherNotificationData(String notificationId) {
        return getClient().create(ApiInterface.class).getOtherNotificationData(
                LOG_PARAMS
                , LOG_RESPONSE

                , notificationId
        );
    }

    public static Observable<CMSPageDataResponse> getCMSPageData(int id) {
        return getClient().create(ApiInterface.class).getCMSPageData(
                LOG_PARAMS
                , LOG_RESPONSE

                , id
        );
    }

    public static Observable<CatalogProductData> getCustomCollectionData(String notificationId, int storeId, int width, int pageNumber, int customerId, JSONArray sortData, JSONArray filterData, float mFactor, int quoteId) {
        return getClient().create(ApiInterface.class).getCustomCollectionData(
                LOG_PARAMS
                , LOG_RESPONSE

                , notificationId
                , storeId
                , width
                , pageNumber
                , customerId
                , sortData
                , filterData
                , mFactor
                , quoteId
        );
    }

    public static Observable<BaseModel> logout(String token, int customerId) {
        return getClient().create(ApiInterface.class).logout(
                LOG_PARAMS
                , LOG_RESPONSE

                , token
                , customerId
        );
    }

    public static Observable<SearchSuggestionResponse> getSearchSuggestions(int storeId, String searchQuery) {
        return getClient().create(ApiInterface.class).getSearchSuggestions(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , searchQuery
        );
    }



    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        OTHER API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/


    public static Observable<BaseModel> postContactUs(int storeId, String name, String email, String telephone, String comment) {
        return getClient().create(ApiInterface.class).postContactUs(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , name
                , email
                , telephone
                , comment
        );
    }

    public static Observable<BaseModel> addPriceAlert(int storeId, int customerId, int productId) {
        return getClient().create(ApiInterface.class).addPriceAlert(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , productId
        );
    }

    public static Observable<BaseModel> addStockAlert(int storeId, int customerId, int productId) {
        return getClient().create(ApiInterface.class).addStockAlert(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , customerId
                , productId
        );
    }

    public static Observable<BaseModel> getGuestOrderDetails(int storeId, String type, String incrementId, String lastName, String email, String zipCode) {
        return getClient().create(ApiInterface.class).getGuestOrderDetails(
                LOG_PARAMS
                , LOG_RESPONSE

                , storeId
                , type
                , incrementId
                , lastName
                , email
                , zipCode
        );
    }
}