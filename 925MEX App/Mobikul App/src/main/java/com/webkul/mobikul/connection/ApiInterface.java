package com.webkul.mobikul.connection;

import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.CMSPageDataResponse;
import com.webkul.mobikul.model.MapResponse;
import com.webkul.mobikul.model.cart.CartDetailsResponse;
import com.webkul.mobikul.model.catalog.CatalogProductData;
import com.webkul.mobikul.model.catalog.CatalogSingleProductData;
import com.webkul.mobikul.model.catalog.HomePageResponseData;
import com.webkul.mobikul.model.checkout.BillingShippingInfoData;
import com.webkul.mobikul.model.checkout.OrderReviewRequestData;
import com.webkul.mobikul.model.checkout.SaveOrderResponse;
import com.webkul.mobikul.model.checkout.ShippingMethodInfoData;
import com.webkul.mobikul.model.compare.CompareListData;
import com.webkul.mobikul.model.customer.accountInfo.AccountInfoResponseData;
import com.webkul.mobikul.model.customer.accountInfo.SaveAccountInfoResponseData;
import com.webkul.mobikul.model.customer.accountInfo.UploadPicResponseData;
import com.webkul.mobikul.model.customer.address.AddressBookResponseData;
import com.webkul.mobikul.model.customer.address.AddressFormResponseData;
import com.webkul.mobikul.model.customer.address.CheckCustomerByEmailResponseData;
import com.webkul.mobikul.model.customer.downloadable.DownloadProductResponse;
import com.webkul.mobikul.model.customer.downloadable.DownloadableProductListData;
import com.webkul.mobikul.model.customer.order.OrderDetailResponseData;
import com.webkul.mobikul.model.customer.order.OrderListResponseData;
import com.webkul.mobikul.model.customer.order.ReorderResponseData;
import com.webkul.mobikul.model.customer.review.ReviewDetailsResponseData;
import com.webkul.mobikul.model.customer.review.ReviewListResponseData;
import com.webkul.mobikul.model.customer.signin.LinkedinAccessToken;
import com.webkul.mobikul.model.customer.signin.LinkedinUserData;
import com.webkul.mobikul.model.customer.signin.SignInForgotPasswordResponse;
import com.webkul.mobikul.model.customer.signin.SignInResponseData;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormData;
import com.webkul.mobikul.model.customer.signup.SignUpResponseData;
import com.webkul.mobikul.model.customer.wishlist.AddToCartFromWishListResponse;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductAddToWishlistResponse;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductRemoveFromWishlistResponse;
import com.webkul.mobikul.model.customer.wishlist.WishListResponseData;
import com.webkul.mobikul.model.extra.SearchSuggestionResponse;
import com.webkul.mobikul.model.firebase.RefreshToken;
import com.webkul.mobikul.model.notification.NotificationResponseData;
import com.webkul.mobikul.model.notification.OtherNotificationResponseData;
import com.webkul.mobikul.model.product.ProductAddToCartResponse;
import com.webkul.mobikul.model.search.AdvanceSearchFormData;
import com.webkul.mobikul.model.search.SearchTermsResponseData;

import org.json.JSONArray;
import org.json.JSONObject;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */

public interface ApiInterface {

    /*Catalog*/
    String MOBIKUL_CATALOG_HOME_PAGE_DATA = "mobikulhttp/catalog/homePageData";
    String MOBIKUL_CATALOG_ADD_TO_WISHLIST = "mobikulhttp/catalog/addtoWishlist";
    String MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST = "mobikulhttp/catalog/categoryProductList";
    String MOBIKUL_CATALOG_CATALOG_SEARCH_RESULT = "mobikulhttp/catalog/searchResult";
    String MOBIKUL_CATALOG_ADVANCED_SEARCH_FORM_DATA = "mobikulhttp/catalog/advancedSearchFormData";
    String MOBIKUL_CATALOG_ADVANCED_SEARCH_RESULT = "mobikulhttp/catalog/advancedSearchResult";
    String MOBIKUL_CATALOG_FEATURED_PRODUCT_LIST = "mobikulhttp/catalog/featuredProductList";
    String MOBIKUL_CATALOG_NEW_PRODUCT_LIST = "mobikulhttp/catalog/newProductList";
    String MOBIKUL_CATALOG_HOT_DEAL_LIST = "mobikulhttp/catalog/hotDealList";
    String MOBIKUL_CATALOG_PRODUCT_PAGE_DATA = "mobikulhttp/catalog/productPageData";
    String MOBIKUL_CATALOG_PRODUCT_SHARE = "mobikulhttp/catalog/productshare";
    String MOBIKUL_CATALOG_ADD_TO_COMPARE = "mobikulhttp/catalog/addtocompare";
    String MOBIKUL_CATALOG_COMPARE_LIST = "mobikulhttp/catalog/comparelist";
    String MOBIKUL_CATALOG_REMOVE_FROM_COMPARE_LIST = "mobikulhttp/catalog/removefromcompare";


    /*Checkout*/
    String MOBIKUL_CHECKOUT_ADD_TO_CART = "mobikulhttp/checkout/addtoCart";
    String MOBIKUL_CHECKOUT_UPDATE_ITEM_OPTION = "mobikulhttp/checkout/updateitemoptions";
    String MOBIKUL_CHECKOUT_CART_DETAILS = "mobikulhttp/checkout/cartDetails";
    String MOBIKUL_CHECKOUT_WISHLIST_FROM_CART = "mobikulhttp/checkout/wishlistfromCart";
    String MOBIKUL_CHECKOUT_APPLY_COUPON = "mobikulhttp/checkout/applyCoupon";
    String MOBIKUL_CHECKOUT_EMPTY_CART = "mobikulhttp/checkout/emptyCart";
    String MOBIKUL_CHECKOUT_UPDATE_CART = "mobikulhttp/checkout/updateCart";
    String MOBIKUL_CHECKOUT_REMOVE_CART_ITEM = "mobikulhttp/checkout/removeCartItem";

    String MOBIKUL_CHECKOUT_BILLING_SHIPPING_INFO = "mobikulhttp/checkout/billingShippingInfo";
    String MOBIKUL_CHECKOUT_SHIPPING_PAYMENT_METHOD_INFO = "mobikulhttp/checkout/shippingPaymentMethodInfo";
    String MOBIKUL_CHECKOUT_ORDER_REVIEW_INFO = "mobikulhttp/checkout/orderreviewInfo";
    String MOBIKUL_CHECKOUT_SAVE_ORDER = "mobikulhttp/checkout/saveOrder";
    String MOBIKUL_CHECKOUT_ACCOUNT_CREATE_EMAIL = "mobikulhttp/checkout/accountcreate";
    String MOBIKUL_CHECKOUT_ORDER_REVIEW_INFO_PAYPAL = "mobikulhttp/checkout/orderreviewInfoPaypal";


    String MOBIKUL_CHECKOUT_OPEN_PAY = "mobikulhttp/checkout/openpay";

    /*Customer*/
    String MOBIKUL_CUSTOMER_LOGIN = "mobikulhttp/customer/logIn";
    String MOBIKUL_CUSTOMER_FORGOT_PASSWORD = "mobikulhttp/customer/forgotpassword";
    String MOBIKUL_CUSTOMER_CREATE_ACCOUNT_FORM_DATA = "mobikulhttp/customer/createAccountFormData";
    String MOBIKUL_CUSTOMER_CREATE_ACCOUNT = "mobikulhttp/customer/createAccount";
    String MOBIKUL_CUSTOMER_GET_ADDRESSBOOK_DATA = "mobikulhttp/customer/addressBookData";
    String MOBIKUL_CUSTOMER_ORDER_LIST = "mobikulhttp/customer/orderList";
    String MOBIKUL_CUSTOMER_ORDER_DETAIL = "mobikulhttp/customer/orderDetails";
    String MOBIKUL_CUSTOMER_REVIEW_LIST = "mobikulhttp/customer/reviewList";
    String MOBIKUL_CUSTOMER_REVIEW_DETAIL = "mobikulhttp/customer/reviewDetails";
    String MOBIKUL_CUSTOMER_DOWNLOAD_PRODUCT = "mobikulhttp/customer/downloadProduct";

    String MOBIKUL_CUSTOMER_GET_ACCOUNT_INFO_DATA = "mobikulhttp/customer/accountinfoData";
    String MOBIKUL_CUSTOMER_SAVE_ACCOUNT_INFO = "mobikulhttp/customer/saveAccountInfo";
    String MOBIKUL_CUSTOMER_MY_DOWNLOADS_LIST = "mobikulhttp/customer/myDownloadsList";
    String MOBIKUL_CUSTOMER_DELETE_ADDRESS = "mobikulhttp/customer/deleteAddress";
    String MOBIKUL_CUSTOMER_WISHLIST = "mobikulhttp/customer/wishList";
    String MOBIKUL_CUSTOMER_REMOVE_FROM_WISHLIST = "mobikulhttp/customer/removefromWishlist";
    String MOBIKUL_CUSTOMER_UPDATE_WISHLIST = "mobikulhttp/customer/updateWishlist";
    String MOBIKUL_CUSTOMER_REORDER = "mobikulhttp/customer/reOrder";
    String MOBIKUL_CUSTOMER_ADDRESS_FORM_DATA = "mobikulhttp/customer/addressformData";
    String MOBIKUL_CUSTOMER_SAVE_ADDRESS = "mobikulhttp/customer/saveAddress";
    String MOBIKUL_CUSTOMER_SAVE_REVIEW = "mobikulhttp/customer/saveReview";
    String MOBIKUL_CUSTOMER_WISHLIST_TO_CART = "mobikulhttp/customer/wishlisttoCart";
    String MOBIKUL_CUSTOMER_CHECK_CUSTOEMR_BY_EMAIL = "mobikulhttp/customer/checkCustomerByEmail";

    /*Extras*/
    String MOBIKUL_EXTRAS_SAVE_ANDROID_TOKEN = "mobikulhttp/extra/registerDevice";
    String MOBIKUL_EXTRAS_SEARCH_TERMS_LIST = "mobikulhttp/extra/searchTermList";
    String MOBIKUL_EXTRAS_NOTIFICATION_LIST = "mobikulhttp/extra/notificationList";
    String MOBIKUL_EXTRAS_OTHER_NOTIFICATION_DATA = "mobikulhttp/extra/otherNotificationData";
    String MOBIKUL_EXTRAS_CUSTOM_COLLECTION = "mobikulhttp/extra/customcollection";
    String MOBIKUL_EXTRAS_CMS_PAGES_DATA = "mobikulhttp/extra/cmsData";
    String MOBIKUL_EXTRAS_LOGOUT = "mobikulhttp/extra/logOut";
    String MOBIKUL_EXTRAS_SEARCH_SUGGESTION = "mobikulhttp/extra/searchSuggestion";


    /*Other*/
    String MOBIKUL_CONTACT_POST = "mobikulhttp/contact/post";
    String MOBIKUL_PRODUCT_ALERT_PRICE = "mobikulhttp/productalert/price";
    String MOBIKUL_PRODUCT_ALERT_STOCK = "mobikulhttp/productalert/stock";
    String MOBIKUL_SALES_GUEST_VIEW = "mobikulhttp/sales/guestview";

     /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        CATALOG API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    //Paypal
    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_ORDER_REVIEW_INFO_PAYPAL)
    Observable<OrderReviewRequestData> getOrderReviewDataPaypal(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("device_session_id") String device_session_id
            , @Field("openpay_token") String paypal_token
            , @Field("method") String paymentMethod
            , @Field("currency") String currency
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_HOME_PAGE_DATA)
    Observable<HomePageResponseData> getHomePageData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("width") int width
            , @Field("websiteId") int websiteId
            , @Field("mFactor") float density
            , @Field("isFromUrl") int isFromUrl
            , @Field("url") String url
            , @Field("currency") String currency
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_ADD_TO_WISHLIST)
    Observable<CatalogProductAddToWishlistResponse> addToWishlist(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("productId") int productId
            , @Field("customerId") int customerId
            , @Field("params") JSONObject params
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST)
    Observable<CatalogProductData> getCatalogProductData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("categoryId") int categoryId
            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
            , @Field("customerId") int customerId
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
            , @Field("mFactor") Float density
            , @Field("quoteId") int quoteId
            , @Field("currency") String currency
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_CATALOG_SEARCH_RESULT)
    Observable<CatalogProductData> getCatalogSearchResult(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("searchQuery") String searchQuery
            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
            , @Field("currency") String currency
            , @Field("customCollection") int customCollection
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_ADVANCED_SEARCH_FORM_DATA)
    Observable<AdvanceSearchFormData> getAdvanceSearchFormData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("currency") String currency
            , @Field("customCollection") int customCollection
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_ADVANCED_SEARCH_RESULT)
    Observable<CatalogProductData> getAdvanceSearchResult(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("queryString") JSONObject queryString
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
            , @Field("currency") String currency
            , @Field("customCollection") int customCollection
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_PRODUCT_PAGE_DATA)
    Observable<CatalogSingleProductData> getCatalogSingleProductData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("productId") int productId
            , @Field("width") int width
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("currency") String currency
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_FEATURED_PRODUCT_LIST)
    Observable<CatalogProductData> getFeaturedProductData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
            , @Field("customerId") int customerId
            , @Field("quoteId") int quoteId
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
            , @Field("currency") String currency
            , @Field("customCollection") int customCollection
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_NEW_PRODUCT_LIST)
    Observable<CatalogProductData> getNewProductData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
            , @Field("customerId") int customerId
            , @Field("quoteId") int quoteId
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
            , @Field("currency") String currency
            , @Field("customCollection") int customCollection
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_HOT_DEAL_LIST)
    Observable<CatalogProductData> getHotDealsProductData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
            , @Field("customerId") int customerId
            , @Field("quoteId") int quoteId
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
            , @Field("currency") String currency
            , @Field("customCollection") int customCollection
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_PRODUCT_SHARE)
    Observable<BaseModel> productShare(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("productId") int productId
            , @Field("customerName") String customerName
            , @Field("customerEmail") String customerEmail
            , @Field("message") String message
            , @Field("recipientName") JSONArray recipientName
            , @Field("recipientEmail") JSONArray recipientEmail
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_ADD_TO_COMPARE)
    Observable<BaseModel> addToCompare(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("productId") int productId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_COMPARE_LIST)
    Observable<CompareListData> getCompareList(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("width") int width
            , @Field("currency") String currency
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CATALOG_REMOVE_FROM_COMPARE_LIST)
    Observable<BaseModel> removeFromCompareList(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("productId") int productId
    );


    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        CUSTOMER API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/


    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_LOGIN)
    Observable<SignInResponseData> getSignInData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("username") String username
            , @Field("password") String password
            , @Field("width") int width
            , @Field("quoteId") int quoteId
            , @Field("storeId") int storeId
            , @Field("websiteId") int websiteId
            , @Field("mFactor") Float density
            , @Field("token") String token
            , @Field("mobile") String mobile
            , @Field("os") String os
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_CREATE_ACCOUNT_FORM_DATA)
    Observable<CreateAccountFormData> getCreateAccountFormData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_CREATE_ACCOUNT)
    Observable<SignUpResponseData> getSignUpData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("token") String token
            , @Field("storeId") int storeId
            , @Field("websiteId") int websiteId
            , @Field("quoteId") int quoteId
            , @Field("prefix") String prefix
            , @Field("firstName") String firstName
            , @Field("middleName") String middleName
            , @Field("lastName") String lastName
            , @Field("suffix") String suffix
            , @Field("dob") String dob
            , @Field("taxvat") String taxvat
            , @Field("gender") int gender
            , @Field("email") String emailAddr
            , @Field("password") String password
            , @Field("pictureURL") String pictureURL
            , @Field("isSocial") int isSocial
            , @Field("mobile") String mobile
            , @Field("shopUrl") String shopURL
            , @Field("becomeSeller") int becomeSeller
            , @Field("os") String os
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_FORGOT_PASSWORD)
    Observable<SignInForgotPasswordResponse> forgotSignInPassword(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("email") String emailAddress
            , @Field("websiteId") int websiteId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_GET_ACCOUNT_INFO_DATA)
    Observable<AccountInfoResponseData> getAccountInfo(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_SAVE_ACCOUNT_INFO)
    Observable<SaveAccountInfoResponseData> saveAccountInfo(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("prefix") String prefixValue
            , @Field("firstName") String firstName
            , @Field("middleName") String middleName
            , @Field("lastName") String lastName
            , @Field("email") String emailAddress
            , @Field("dob") String dobValue
            , @Field("taxvat") String taxvat
            , @Field("suffix") String suffixValue
            , @Field("gender") int gender
            , @Field("doChangeEmail") int doChangeEmail
            , @Field("doChangePassword") int doChangePassword
            , @Field("currentPassword") String currentPassword
            , @Field("newPassword") String newPassword
            , @Field("confirmPassword") String confirmPassword
            , @Field("mobile") String mobile
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_GET_ADDRESSBOOK_DATA)
    Observable<AddressBookResponseData> getAddressBookData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_ORDER_LIST)
    Observable<OrderListResponseData> getOrderListData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("pageNumber") int pageNumber
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_REVIEW_LIST)
    Observable<ReviewListResponseData> getReviewListData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("pageNumber") int pageNumber
            , @Field("storeId") int storeId
            , @Field("width") int width
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_ORDER_DETAIL)
    Observable<OrderDetailResponseData> getOrderDetailsData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("incrementId") String incrementId
            , @Field("storeId") int storeId
            , @Field("currency") String currency
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_REMOVE_FROM_WISHLIST)
    Observable<CatalogProductRemoveFromWishlistResponse> removeItemFromWishlist(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("storeId") int storeId
            , @Field("itemId") int itemId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_REORDER)
    Observable<ReorderResponseData> reorder(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("incrementId") String incrementId
            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_REVIEW_DETAIL)
    Observable<ReviewDetailsResponseData> getReviewDetails(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("reviewId") int reviewId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_ADDRESS_FORM_DATA)
    Observable<AddressFormResponseData> getAddressFormData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("storeId") int storeId
            , @Field("addressId") int addressId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_SAVE_ADDRESS)
    Observable<BaseModel> saveAddress(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("addressId") int addressId
            , @Field("addressData") JSONObject addressData
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_WISHLIST)
    Observable<WishListResponseData> getWishListData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_UPDATE_WISHLIST)
    Observable<BaseModel> updateWishlist(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("itemData") JSONArray itemData
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_WISHLIST_TO_CART)
    Observable<AddToCartFromWishListResponse> addToCartFromWishlist(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("storeId") int storeId
            , @Field("productId") int productId
            , @Field("itemId") int itemId
            , @Field("qty") int qty
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_DELETE_ADDRESS)
    Observable<BaseModel> deleteAddress(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("addressId") int addressId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_MY_DOWNLOADS_LIST)
    Observable<DownloadableProductListData> getMyDownloadsList(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("pageNumber") int pageNumber
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_DOWNLOAD_PRODUCT)
    Observable<DownloadProductResponse> downloadProduct(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("hash") String hash
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_SAVE_REVIEW)
    Observable<BaseModel> saveReview(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("customerId") int customerId
            , @Field("storeId") int storeId
            , @Field("productId") int id
            , @Field("title") String title
            , @Field("detail") String detail
            , @Field("nickname") String nickname
            , @Field("ratings") JSONObject ratingObj
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_CHECK_CUSTOEMR_BY_EMAIL)
    Observable<CheckCustomerByEmailResponseData> checkCustomerByEmail(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("email") String email
    );

    @Multipart
    @POST
    Call<UploadPicResponseData> uploadCustomerImagePic(
            @Url String url

            , @Part("file\"; filename=\"customerImage.png\" ") RequestBody file
            , @Part("width") RequestBody width
            , @Part("mFactor") RequestBody density);

    @Multipart
    @POST
    Call<UploadPicResponseData> uploadImage(
            @Url String url

            , @Part MultipartBody.Part file
            , @Part("width") RequestBody width
            , @Part("mFactor") RequestBody density);

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        CHECKOUT API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_ADD_TO_CART)
    Observable<ProductAddToCartResponse> addToCart(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("productId") int productId
            , @Field("qty") int qty
            , @Field("params") JSONObject params
            , @Field("relatedProducts") JSONArray relatedProducts
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_UPDATE_ITEM_OPTION)
    Observable<ProductAddToCartResponse> updateproduct(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("productId") int productId
            , @Field("qty") int qty
            , @Field("params") JSONObject params
            , @Field("relatedProducts") JSONArray relatedProducts
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("itemId") int itemId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_CART_DETAILS)
    Observable<CartDetailsResponse> getCartDetails(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("currency") String currency
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_WISHLIST_FROM_CART)
    Observable<CartDetailsResponse> moveToWishlist(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("itemId") int itemId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_REMOVE_CART_ITEM)
    Observable<CartDetailsResponse> removeCartItem(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("itemId") int itemId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_EMPTY_CART)
    Observable<BaseModel> emptyCart(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_UPDATE_CART)
    Observable<BaseModel> updateCart(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("itemIds") JSONArray itemIds
            , @Field("itemQtys") JSONArray itemQtys
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_APPLY_COUPON)
    Observable<BaseModel> applyOrCancelCoupon(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("couponCode") String couponCode
            , @Field("removeCoupon") int removeCoupon
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_BILLING_SHIPPING_INFO)
    Observable<BillingShippingInfoData> getBillingShippingInfoData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_SHIPPING_PAYMENT_METHOD_INFO)
    Observable<ShippingMethodInfoData> getShippingMethodInfoData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("checkoutMethod") String checkoutMethod
            , @Field("billingData") JSONObject billingData
            , @Field("shippingData") JSONObject shippingData
            , @Field("currency") String currency
    );

    /*@FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_ORDER_REVIEW_INFO)
    Observable<OrderReviewRequestData> getOrderReviewData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("method") String paymentMethod
            , @Field("currency") String currency
    );*/

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_ORDER_REVIEW_INFO)
    Observable<OrderReviewRequestData> getOrderReviewData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("shippingMethod") String shippingMethod
            , @Field("cc_cid") String cc_cid
            , @Field("cc_exp_month") String cc_exp_month
            , @Field("cc_exp_year") String cc_exp_year
            , @Field("cc_number") String cc_number
            , @Field("cc_type") String cc_type
            , @Field("device_session_id") String device_session_id
            , @Field("openpay_token") String openpay_token
            , @Field("method") String paymentMethod
            , @Field("currency") String currency
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_SAVE_ORDER)
    Observable<SaveOrderResponse> saveOrder(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("quoteId") int quoteId
            , @Field("customerId") int customerId
            , @Field("token") String token
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHECKOUT_ACCOUNT_CREATE_EMAIL)
    Observable<BaseModel> accountCreateEmail(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("orderId") String orderId
    );

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        EXTRAS API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST(MOBIKUL_EXTRAS_SAVE_ANDROID_TOKEN)
    Observable<RefreshToken> uploadTokenData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("token") String token
            , @Field("customerId") int customerId
            , @Field("os") String os
    );

    @FormUrlEncoded
    @POST(MOBIKUL_EXTRAS_SEARCH_TERMS_LIST)
    Observable<SearchTermsResponseData> getSearchTermsList(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_EXTRAS_NOTIFICATION_LIST)
    Observable<NotificationResponseData> getNotificationsList(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("mFactor") float mFactor
    );

    @FormUrlEncoded
    @POST(MOBIKUL_EXTRAS_OTHER_NOTIFICATION_DATA)
    Observable<OtherNotificationResponseData> getOtherNotificationData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("notificationId") String notificationId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_EXTRAS_CMS_PAGES_DATA)
    Observable<CMSPageDataResponse> getCMSPageData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("id") int id
    );

    @FormUrlEncoded
    @POST(MOBIKUL_EXTRAS_CUSTOM_COLLECTION)
    Observable<CatalogProductData> getCustomCollectionData(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("notificationId") String notificationId
            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
            , @Field("customerId") int customerId
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
            , @Field("mFactor") float density
            , @Field("quoteId") int quoteId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_EXTRAS_LOGOUT)
    Observable<BaseModel> logout(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("token") String token
            , @Field("customerId") int customerId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_EXTRAS_SEARCH_SUGGESTION)
    Observable<SearchSuggestionResponse> getSearchSuggestions(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("searchQuery") String searchQuery
    );





    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        OTHER API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST(MOBIKUL_CONTACT_POST)
    Observable<BaseModel> postContactUs(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("name") String name
            , @Field("email") String email
            , @Field("telephone") String telephone
            , @Field("comment") String comment
    );

    @FormUrlEncoded
    @POST(MOBIKUL_PRODUCT_ALERT_PRICE)
    Observable<BaseModel> addPriceAlert(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("productId") int productId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_PRODUCT_ALERT_STOCK)
    Observable<BaseModel> addStockAlert(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("productId") int productId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SALES_GUEST_VIEW)
    Observable<BaseModel> getGuestOrderDetails(
            @Field("logParams") int logParams
            , @Field("logResponse") int logResponse

            , @Field("storeId") int storeId
            , @Field("type") String type
            , @Field("incrementId") String incrementId
            , @Field("lastName") String lastName
            , @Field("email") String email
            , @Field("zipCode") String zipCode
    );




    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------
        SOCIAL LOGIN API's
     ------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @POST
    Call<LinkedinAccessToken> getLinkedinAccessTokenReq(@Url String url);

    @GET
    Call<LinkedinUserData> getLinkedinUserData(@Url String url);

    @GET
    Call<MapResponse> getGoogleMapResponse(@Url String url);

}