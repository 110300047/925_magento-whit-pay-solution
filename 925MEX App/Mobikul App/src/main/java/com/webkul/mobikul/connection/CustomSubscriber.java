package com.webkul.mobikul.connection;

import android.content.Context;
import android.support.annotation.NonNull;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.model.BaseModel;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED;

/**
 * Created by vedesh.kumar on 6/6/17. @Webkul Software Private limited
 */

public class CustomSubscriber<T> implements Observer<T>, Subscriber<T> {

    @SuppressWarnings("unused")
    private static final String TAG = "CustomSubscriber";

    private Context mContext;

    public CustomSubscriber(Context context) {
        mContext = context;
    }

    @Override
    public void onSubscribe(@NonNull Disposable disposable) {
        try {
            ((BaseActivity) mContext).mCompositeDisposable.add(disposable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSubscribe(Subscription subscription) {

    }

    @Override
    public void onNext(T t) {
        AlertDialogHelper.dismiss(mContext);
        if (((BaseModel) t).getResponseCode() == NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED) {
            ApplicationSingleton.getInstance().setAuthKey(((BaseModel) t).getAuthKey());
        }
    }

    @Override
    public void onError(Throwable t) {
        try {
            AlertDialogHelper.dismiss(mContext);
            NetworkHelper.onFailure(t, (BaseActivity) mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onComplete() {

    }
}