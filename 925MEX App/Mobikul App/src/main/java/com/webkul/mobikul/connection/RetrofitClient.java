package com.webkul.mobikul.connection;

import android.util.Log;

import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.ApplicationSingleton;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.webkul.mobikul.constants.ApplicationConstant.BASE_URL;
import static com.webkul.mobikul.constants.ApplicationConstant.GOOGLE_MAP_BASE_URL;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */

public class RetrofitClient {

    private static final int CONNECT_TIMEOUT_MULTIPLIER = 1;
    private static final int DEFAULT_CONNECT_TIMEOUT_IN_SEC = 30;
    private static final int DEFAULT_WRITE_TIMEOUT_IN_SEC = 30;
    private static final int DEFAULT_READ_TIMEOUT_IN_SEC = 30;

    private static final int NO_OF_LOG_CHAR = 1000;

    @SuppressWarnings("FieldCanBeLocal")
    private static Retrofit sRetrofitClient = null;
    private static Dispatcher sDispatcher = null;

    private static Retrofit sGoogleMapRetrofitClient = null;

    public static Retrofit getClient() {
        if (sRetrofitClient == null) {
            sRetrofitClient = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getOkHttpClientBuilder().build())
                    .build();
        }
        return sRetrofitClient;
    }

    private static OkHttpClient.Builder getOkHttpClientBuilder() {
//        int cacheSize = 10 * 1024 * 1024; // 10 MB
//        Cache cache = new Cache(sBaseContext.getCacheDir(), cacheSize);

        /*OkHttp client builder*/
        OkHttpClient.Builder oktHttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_MULTIPLIER * DEFAULT_CONNECT_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
                .writeTimeout(CONNECT_TIMEOUT_MULTIPLIER * DEFAULT_WRITE_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
                .readTimeout(CONNECT_TIMEOUT_MULTIPLIER * DEFAULT_READ_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
//                .cache(cache)
                .cookieJar(new JavaNetCookieJar(getCookieManager())); /* Using okhttp3 cookie instead of java net cookie*/
        oktHttpClientBuilder.dispatcher(getDispatcher());

        oktHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder()
                        .addHeader("Content-Type", "text/html")
                        .addHeader("authKey", ApplicationSingleton.getInstance().getAuthKey())
                        .addHeader("apiKey", ApplicationConstant.API_USER_NAME)
                        .addHeader("apiPassword", ApplicationConstant.API_PASSWORD);
                return chain.proceed(builder.build());
            }
        });

        oktHttpClientBuilder.addInterceptor(getHttpLoggingInterceptor());
        oktHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Response response = chain.proceed(request);
                Log.d(ApplicationConstant.TAG, "intercept: responseCode " + response.code());
                return response;
            }
        });
        return oktHttpClientBuilder;
    }

    private static CookieManager getCookieManager() {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        return cookieManager;
    }

    private static Interceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                if (message.length() > NO_OF_LOG_CHAR) {
                    for (int noOflogs = 0; noOflogs <= (message.length() / NO_OF_LOG_CHAR); noOflogs++) {
                        if (((noOflogs * NO_OF_LOG_CHAR) + NO_OF_LOG_CHAR) < message.length()) {
                            Log.d(ApplicationConstant.TAG, message.substring((noOflogs * NO_OF_LOG_CHAR), ((noOflogs * NO_OF_LOG_CHAR) + NO_OF_LOG_CHAR)));
                        } else {
                            Log.d(ApplicationConstant.TAG, message.substring((noOflogs * NO_OF_LOG_CHAR), message.length()));
                        }
                    }
                } else {
                    Log.d(ApplicationConstant.TAG, message);
                }
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }

    public static Dispatcher getDispatcher() {
        if (sDispatcher == null) {
            return new Dispatcher();
        }
        return sDispatcher;
    }

    public static Retrofit getCLientForMap() {
        if (sGoogleMapRetrofitClient == null) {
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder oktHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(DEFAULT_CONNECT_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
                    .writeTimeout(DEFAULT_WRITE_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
                    .readTimeout(DEFAULT_READ_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
                    .cookieJar(new JavaNetCookieJar(cookieManager));

            oktHttpClient.addInterceptor(logging);

            sGoogleMapRetrofitClient = new Retrofit.Builder()
                    .baseUrl(GOOGLE_MAP_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(oktHttpClient.build())
                    .build();
        }
        return sGoogleMapRetrofitClient;
    }

}