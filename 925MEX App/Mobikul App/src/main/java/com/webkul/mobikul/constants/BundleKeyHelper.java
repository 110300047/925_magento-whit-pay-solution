package com.webkul.mobikul.constants;

/**
 * Created by vedesh.kumar on 5/9/17. @Webkul Software Private limited
 */

public interface BundleKeyHelper {

    String BUNDLE_KEY_HOME_PAGE_RESPONSE_DATA = "HomePageResponseData";
    String BUNDLE_KEY_SAVE_ORDER_RESPONSE = "saveOrderResponse";
    String BUNDLE_KEY_INCREMENT_ID = "incrementId";
    String BUNDLE_KEY_ITEM_ID = "itemId";
    String BUNDLE_KEY_ORDER_ID = "orderId";
    String BUNDLE_KEY_CAN_REORDER = "canReorder";
    String BUNDLE_KEY_NEW_SELLER_ORDER = "newSellerOrder";
    String BUNDLE_KEY_NEW_CUSTOMER_ORDER = "newCustomerOrder";
    String BUNDLE_KEY_NOTIFICATION_TYPE = "notificationType";
    String BUNDLE_KEY_FROM_NOTIFICATION = "fromNotification";
    String BUNDLE_KEY_NOTIFICATION_ID = "notificationId";
    String BUNDLE_KEY_WIDGET_TYPE = "widgetType";
    String BUNDLE_KEY_REVIEW_ID = "reviewId";
    String BUNDLE_KEY_PRODUCT_ID = "productId";
    String BUNDLE_KEY_PRODUCT_NAME = "productName";
    String BUNDLE_KEY_PRODUCT_IMAGE = "productImage";
    String BUNDLE_KEY_EDIT_PRODUCT = "editProduct";
    String BUNDLE_KEY_PRODUCT_PAGE_DATA = "singleProductPageData";
    String BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST = "productPageDataList";
    String BUNDLE_KEY_SELECTED_PRODUCT_NUMBER = "selectedProductnumber";
    String BUNDLE_KEY_LOAD_SELLER_ORDERS = "loadSellerOrders";
    String BUNDLE_KEY_DESCRIPTION = "description";
    String BUNDLE_KEY_ACTION = "action";
    String BUNDLE_KEY_RATING_CODE_ARRAY = "ratingCode";
    String BUNDLE_KEY_RATING_VALUE_ARRAY = "ratingValue";
    String BUNDLE_KEY_ADDRESS_ID = "billingAddressId";
    String BUNDLE_KEY_ACTIVITY_TITLE = "activityTitle";
    String BUNDLE_KEY_HTML_CONTENT = "html_content";
    String BUNDLE_KEY_SELLER_ORDER_FILTER_DATA = "seller_order_filter_data";
    String BUNDLE_KEY_TRANSACTION_ID = "sellerTransactionId";
    String BUNDLE_KEY_DOB_VALUE = "dobValue";
    String BUNDLE_KEY_DOB_TYPE = "dobType";
    String BUNDLE_KEY_SELECT_PAGE = "selectPage";
    String BUNDLE_KEY_IS_PROCEED_TO_CHECKOUT = "isProceedToCheckout";
    String BUNDLE_KEY_SHIPPING_METHOD_INFO_DATA = "shippingMethodInfo";
    String BUNDLE_KEY_PAYMENT_METHOD_DATA = "shippingMethodInfo";
    String BUNDLE_KEY_SELECTED_SHIPPING_METHOD_CODE = "selectedShippingMethodCode";
    String BUNDLE_KEY_SELECTED_PAYMENT_METHOD_CODE = "selectedPaymentMethodCode";
    String BUNDLE_KEY_ORDER_REVIEW_REQUEST_DATA = "orderReviewRequestData";
    String BUNDLE_KEY_WIDGET_DATA = "widgetData";
    String BUNDLE_KEY_SELLER_STORE_DATA = "sellerStoreData";
    String BUNDLE_KEY_EMAIL = "email";
    String BUNDLE_KEY_SELLER_MOBILE_NUMBER = "sellerMobileNumber";
    String BUNDLE_KEY_SELLER_STORE_NAME = "sellerStoreName";
    String BUNDLE_KEY_SELLER_ID = "sellerId";
    String BUNDLE_KEY_SHOP_URL = "shopUrl";
    String BUNDLE_KEY_SELLER_TITLE = "sellerTitle";
    String BUNDLE_KEY_SELLER_ORDER_DATA = "sellerOrderData";
    String BUNDLE_KEY_SELLER_REVIEW_DATA = "sellerReviewData";
    String BUNDLE_KEY_CALLED_FROM_HOME_BANNER = "calledFromHomeBanner";
    String BUNDLE_KEY_EMPTY_FRAGMENT_DRAWABLE_ID = "emptyFragmentDrawableId";
    String BUNDLE_KEY_EMPTY_FRAGMENT_TITLE_ID = "emptyFragmentTitleId";
    String BUNDLE_KEY_EMPTY_FRAGMENT_SUBTITLE_ID = "emptyFragmentSubtitleId";
    String BUNDLE_KEY_EMPTY_FRAGMENT_HIDE_CONTINUE_SHOPPING_BTN = "hideContinueShoppingBtn";
    String BUNDLE_KEY_SORTING_DATA = "sortingData";
    String BUNDLE_KEY_FILTERING_DATA = "filteringData";
    String BUNDLE_KEY_FILTER_CATEGORY_DATA = "filterCategoryData";
    String BUNDLE_KEY_SORTING_INPUT_JSON = "sortingInputJson";
    String BUNDLE_KEY_FILTER_INPUT_JSON = "filteringInputJson";
    String BUNDLE_KEY_ADVANCE_SEARCH_QUERY_JSON = "advanceSearchQueryJSON";
    String BUNDLE_KEY_SEARCH_TERM_QUERY = "searchQueryJSON";
    String BUNDLE_KEY_IDENTIFIER = "identifier";
    String BUNDLE_KEY_COLLECTION_TYPE = "collectionType";
    String BUNDLE_KEY_IS_FROM_URL = "isFromUrl";
    String BUNDLE_KEY_IS_VIRTUAL = "isVirtual";
    String BUNDLE_KEY_RATING_FORM_DATA = "ratingFormData";
    String BUNDLE_KEY_REVIEW_LIST_DATA = "reviewListData";
    String BUNDLE_KEY_CATEGORY_NAME = "categoryName";
    String BUNDLE_KEY_RETURN_POLICY = "returnPolicy";
    String BUNDLE_KEY_SHIPPING_POLICY = "shippingPolicy";

    String BUNDLE_KEY_INVOICE_ID = "invoiceId";
    String BUNDLE_KEY_SHIPMENT_ID = "shipmentId";
    String BUNDLE_KEY_CREDIT_MEMO_ID = "creditMemoId";


    /*ORDER STATUS CODES*/
    String ORDER_STATUS_COMPLETE = "complete";
    String ORDER_STATUS_PENDING = "pending";
    String ORDER_STATUS_PROCESSING = "processing";
    String ORDER_STATUS_HOLD = "holded";
    String ORDER_STATUS_CANCELLED = "canceled";
    String ORDER_STATUS_NEW = "new";
    String ORDER_STATUS_CLOSED = "closed";
}
