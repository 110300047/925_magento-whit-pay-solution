package com.webkul.mobikul.constants;

import java.util.regex.Pattern;

/**
 * Created by vedesh.kumar on 20/10/16. @Webkul Software Pvt. Ltd
 */

/**
 * Class used for constants used in the application
 */
public interface MobikulConstant {

    Pattern EMAIL_PATTERN = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
}