package com.webkul.mobikul.customviews;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.support.v7.widget.DividerItemDecoration;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.adapter.SearchSuggestionProductAdapter;
import com.webkul.mobikul.adapter.SearchSuggestionTagAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.MaterialSearchViewBinding;
import com.webkul.mobikul.handler.MaterialSearchViewHandler;
import com.webkul.mobikul.helper.AnimationHelper;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.BindingAdapterUtils;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.extra.SearchSuggestionResponse;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by vedesh.kumar on 2/2/17. @Webkul Software Pvt. Ltd
 */
public class MaterialSearchView extends FrameLayout {

    public static final int RC_CODE = 42;
    @SuppressWarnings("unused")
    private static final String TAG = "MaterialSearchView";
    private static final int MAX_VOICE_RESULTS = 1;
    public MaterialSearchViewBinding mBinding;
    private Context mContext;
    private boolean mOpen;
    private CharSequence mCurrentQuery;
    private SearchSuggestionTagAdapter mSearchSuggestionTagAdapter;
    private SearchSuggestionProductAdapter mSearchSuggestionProductAdapter;

    public MaterialSearchView(Context context) {
        this(context, null);
    }

    public MaterialSearchView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaterialSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initializeView();
    }


    private void initializeView() {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.material_search_view, this, true);
        mBinding.setHandler(new MaterialSearchViewHandler(this));
        initSearchView();
    }

    private void initSearchView() {
        mBinding.etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                onSubmitQuery();
                return true;
            }
        });

        mBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                try {
                    if (!charSequence.toString().isEmpty() && charSequence.length() >= (mContext instanceof HomeActivity ? ((HomeActivity) mContext).mHomePageResponseDataObject.getMinQueryLength() : 1)) {
                        ApiConnection.getSearchSuggestions(AppSharedPref.getStoreId(mContext)
                                , charSequence.toString())
                                .debounce(500, TimeUnit.MILLISECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new CustomSubscriber<SearchSuggestionResponse>(mContext) {
                                    @Override
                                    public void onNext(SearchSuggestionResponse searchSuggestionResponse) {
                                        super.onNext(searchSuggestionResponse);
                                        mBinding.setData(searchSuggestionResponse);
                                        if (mBinding.suggestionTagsRv.getAdapter() == null) {
                                            mBinding.suggestionTagsRv.addItemDecoration(new DividerItemDecoration(mContext, LinearLayout.VERTICAL));
                                            mSearchSuggestionTagAdapter = new SearchSuggestionTagAdapter(mContext, searchSuggestionResponse.getSuggestProductArray().getTags());
                                            mBinding.suggestionTagsRv.setAdapter(mSearchSuggestionTagAdapter);
                                        } else {
                                            ((SearchSuggestionTagAdapter) mBinding.suggestionTagsRv.getAdapter()).updateData(searchSuggestionResponse.getSuggestProductArray().getTags());
                                        }

                                        if (mBinding.suggestionProductsRv.getAdapter() == null) {
                                            mSearchSuggestionProductAdapter = new SearchSuggestionProductAdapter(mContext, searchSuggestionResponse.getSuggestProductArray().getProducts());
                                            mBinding.suggestionProductsRv.setAdapter(mSearchSuggestionProductAdapter);
                                        } else {
                                            ((SearchSuggestionProductAdapter) mBinding.suggestionProductsRv.getAdapter()).updateData(searchSuggestionResponse.getSuggestProductArray().getProducts());
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                        /*No need to log the errors...*/
                                    }
                                });
                    }
                    MaterialSearchView.this.onTextChanged(charSequence);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mBinding.etSearch.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Utils.showKeyboard(mBinding.etSearch);
                    showSuggestions();
                }
            }
        });
    }

    private void onTextChanged(@SuppressWarnings("UnusedParameters") CharSequence newText) {
        mCurrentQuery = mBinding.etSearch.getText();
        if (TextUtils.isEmpty(mCurrentQuery)) {
            displayVoiceButton(true);
            displayClearButton(false);
        } else {
            displayVoiceButton(false);
            displayClearButton(true);
        }
    }

    private void onSubmitQuery() {
        CharSequence query = mBinding.etSearch.getText();
        if (query != null && TextUtils.getTrimmedLength(query) > 0) {
            Intent intent = new Intent(mContext, CatalogProductActivity.class);
            intent.setAction(Intent.ACTION_SEARCH);
            intent.putExtra(SearchManager.QUERY, query.toString());
            mContext.startActivity(intent);
            closeSearch();
//            mBinding.etSearch.setText("");
        }
    }

    public void setQuery(CharSequence query, boolean submit) {
        if (query != null) {
            BindingAdapterUtils.setLoadHtmlText(mBinding.etSearch, query.toString());
            mBinding.etSearch.setSelection(mBinding.etSearch.length());
            mCurrentQuery = query;
        }

        if (submit && !TextUtils.isEmpty(query)) {
            onSubmitQuery();
        }
    }

    public void closeSearch() {
        if (!mOpen) {
            return;
        }
//        mBinding.etSearch.setText("");
        dismissSuggestions();
        clearFocus();
        AnimatorListenerAdapter listenerAdapter = new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mBinding.searchLayout.setVisibility(View.GONE);
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AnimationHelper.circleHideView(mBinding.searchBar, listenerAdapter);
        } else {
            AnimationHelper.fadeOutView(mBinding.searchLayout);
        }
        mOpen = false;


        /*REMOVE THIS CONTEXT CHECKS AND BETTER USE SOME KIND OF INTERFACE............................................*/

        if (mContext instanceof HomeActivity) {
            ((HomeActivity) mContext).onSearchClosed();
        } else if (mContext instanceof CatalogProductActivity) {
            ((CatalogProductActivity) mContext).onSearchClosed();
        }
    }

    public void onVoiceClicked() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, mContext.getString(R.string.hint_prompt));
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MAX_VOICE_RESULTS); // Quantity of results we want to receive

        if (mContext instanceof HomeActivity) {
            ((HomeActivity) mContext).startActivityForResult(intent, RC_CODE);
        } else if (mContext instanceof CatalogProductActivity) {
            ((CatalogProductActivity) mContext).startActivityForResult(intent, RC_CODE);
        }
    }

    public void openSearch() {
        if (mOpen) {
            return;
        }
//        mBinding.etSearch.setText("");
        mBinding.etSearch.requestFocus();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBinding.searchLayout.setVisibility(View.VISIBLE);
            AnimationHelper.circleRevealView(mBinding.searchBar);
        } else {
            AnimationHelper.fadeInView(mBinding.searchLayout);
        }

        if (mContext instanceof HomeActivity) {
            ((HomeActivity) mContext).onSearchOpened();
        } else if (mContext instanceof CatalogProductActivity) {
            ((CatalogProductActivity) mContext).onSearchOpened();
        }
        mOpen = true;
    }

    public boolean isOpen() {
        return mOpen;
    }

    private void displayVoiceButton(boolean display) {
        if (display && Utils.isVoiceAvailable(mContext)) {
            mBinding.actionVoice.setVisibility(View.VISIBLE);
        } else {
            mBinding.actionVoice.setVisibility(View.GONE);
        }
    }

    private void displayClearButton(boolean display) {
        mBinding.actionClear.setVisibility(display ? View.VISIBLE : View.GONE);
    }

    private void showSuggestions() {
        mBinding.suggestionTagsRv.setVisibility(View.VISIBLE);
        mBinding.suggestionProductsRv.setVisibility(View.VISIBLE);
    }

    private void dismissSuggestions() {
        mBinding.suggestionTagsRv.setVisibility(View.GONE);
        mBinding.suggestionProductsRv.setVisibility(View.GONE);
    }

}
