package com.webkul.mobikul.dialog;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.DialogFragmentFilterByBinding;
import com.webkul.mobikul.databinding.ItemCatalogSelectedFilterBinding;
import com.webkul.mobikul.handler.FilterByFragmentHandler;
import com.webkul.mobikul.helper.CatalogHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.catalog.CategoryList;
import com.webkul.mobikul.model.catalog.LayeredData;
import com.webkul.mobikul.model.catalog.LayeredDataOption;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Map;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_FILTERING_DATA;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_FILTER_CATEGORY_DATA;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_FILTER_INPUT_JSON;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class FilterByFragment extends Fragment implements RadioGroup.OnCheckedChangeListener {

    public static ArrayList<LayeredData> sAllLayeredDatas = new ArrayList<>();
    public static ArrayList<CategoryList> sAllCategoryList = new ArrayList<>();
    public ArrayList<LayeredData> mLayeredDatas;
    public ArrayList<CategoryList> mCategoriesDatas;
    private DialogFragmentFilterByBinding mBinding;
    private JSONArray mFilterInputJson;

    public static FilterByFragment newInstance(ArrayList<LayeredData> filterData, JSONArray filterInputJson, ArrayList<CategoryList> categoryList) {
        FilterByFragment sortByFragment = new FilterByFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(BUNDLE_KEY_FILTERING_DATA, filterData);
        args.putString(BUNDLE_KEY_FILTER_INPUT_JSON, filterInputJson.toString());
        args.putParcelableArrayList(BUNDLE_KEY_FILTER_CATEGORY_DATA, categoryList);
        sortByFragment.setArguments(args);
        return sortByFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_filter_by, container, false);
        mBinding.setHandler(new FilterByFragmentHandler(this));
        mLayeredDatas = getArguments().getParcelableArrayList(BUNDLE_KEY_FILTERING_DATA);
        mCategoriesDatas = getArguments().getParcelableArrayList(BUNDLE_KEY_FILTER_CATEGORY_DATA);

        if (mCategoriesDatas == null) {
            try {
                for (int noOfLayeredData = 0; noOfLayeredData < mLayeredDatas.size(); noOfLayeredData++)
                    sAllLayeredDatas.add(mLayeredDatas.get(noOfLayeredData));

                try {
                    mFilterInputJson = new JSONArray(getArguments().getString(BUNDLE_KEY_FILTER_INPUT_JSON, ""));
                } catch (JSONException e) {
                    mFilterInputJson = new JSONArray();
                    e.printStackTrace();
                }

                if (mLayeredDatas == null) {
                    return mBinding.getRoot(); /*Return empty dialog....*/
                }

                if (((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.size() != 0) {
                    mBinding.selectedFiltersLayout.setVisibility(View.VISIBLE);
                    mBinding.clearAll.setVisibility(View.VISIBLE);

                    mBinding.clearAll.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            clearAllFilters();
                        }
                    });
                    loadSelectedFiltersData();
                }
                setUpFilterByCB();
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getContext(), getContext().getString(R.string.msg_no_filter_option_available), Toast.LENGTH_SHORT, 0);
                ((CatalogProductActivity) getContext()).getSupportFragmentManager().popBackStack();
            }
        } else {
            for (int noOfLayeredData = 0; noOfLayeredData < mCategoriesDatas.size(); noOfLayeredData++)
                sAllCategoryList.add(mCategoriesDatas.get(noOfLayeredData));
            if (((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.size() != 0) {
                mBinding.selectedFiltersLayout.setVisibility(View.VISIBLE);
                mBinding.clearAll.setVisibility(View.VISIBLE);

                mBinding.clearAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clearAllFilters();
                    }
                });
                loadSelectedFiltersData();
            }
            setUpFilterByCategoryCB();
        }
        return mBinding.getRoot();
    }

    public void setUpFilterByCB() {
        int labelTopPadding = (int) Utils.convertDpToPixel((int) getContext().getResources().getDimension(R.dimen.spacing_generic), getContext());
        for (int filterDataIdx = 0; filterDataIdx < mLayeredDatas.size(); filterDataIdx++) {
            LayeredData eachFilterData = mLayeredDatas.get(filterDataIdx);

            if (eachFilterData.getOptions() != null && eachFilterData.getOptions().size() > 0) {
                TextView eachFilterDataLabel = new TextView(getContext());
                eachFilterDataLabel.setText(eachFilterData.getLabel());
                eachFilterDataLabel.setTextSize(16);
                eachFilterDataLabel.setTextColor(getContext().getResources().getColor(R.color.text_color_Secondary));
                eachFilterDataLabel.setTypeface(eachFilterDataLabel.getTypeface(), Typeface.BOLD);
                if (filterDataIdx != 0) {
                    eachFilterDataLabel.setPadding(0, labelTopPadding, 0, 0);
                }
                mBinding.filterContainer.addView(eachFilterDataLabel);
                View view = new View(getContext());
                view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) Utils.convertDpToPixel(1.0f, getContext())));
                view.setBackgroundColor(getContext().getResources().getColor(R.color.borderColor));
                mBinding.filterContainer.addView(view);

                RadioGroup eachFilterRg = new RadioGroup(getContext());
                eachFilterRg.setTag(eachFilterData.getCode());
                for (LayeredDataOption eachFilterDataOptions : eachFilterData.getOptions()) {
                    RadioButton eachFilterDataOptionRb = new RadioButton(getContext());
                    eachFilterDataOptionRb.setTag(eachFilterDataOptions.getId());
                    eachFilterDataOptionRb.setText(eachFilterDataOptions.getLabel() + " (" + eachFilterDataOptions.getCount() + ")");
                    eachFilterDataOptionRb.setTextSize(14);
                    eachFilterDataOptionRb.setTextColor(getContext().getResources().getColor(R.color.text_color_primary));
                    eachFilterRg.addView(eachFilterDataOptionRb);
                }
                eachFilterRg.setOnCheckedChangeListener(this);
                mBinding.filterContainer.addView(eachFilterRg);
            }
        }
    }

    public void setUpFilterByCategoryCB() {
        if (mCategoriesDatas.size() > 0) {
            TextView eachFilterDataLabel = new TextView(getContext());
            eachFilterDataLabel.setText(R.string.seller_category);
            eachFilterDataLabel.setTextSize(16);
            eachFilterDataLabel.setTextColor(getContext().getResources().getColor(R.color.text_color_Secondary));
            eachFilterDataLabel.setTypeface(eachFilterDataLabel.getTypeface(), Typeface.BOLD);
            mBinding.filterContainer.addView(eachFilterDataLabel);

            View view = new View(getContext());
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) Utils.convertDpToPixel(1.0f, getContext())));
            view.setBackgroundColor(getContext().getResources().getColor(R.color.borderColor));
            mBinding.filterContainer.addView(view);

            RadioGroup eachFilterCategoryRg = new RadioGroup(getContext());
            eachFilterCategoryRg.setTag(getContext().getString(R.string.seller_category));
            for (int filterDataIdx = 0; filterDataIdx < mCategoriesDatas.size(); filterDataIdx++) {
                CategoryList eachFilterCategoryData = mCategoriesDatas.get(filterDataIdx);
                RadioButton eachFilterCategoeyDataOptionRb = new RadioButton(getContext());
                eachFilterCategoeyDataOptionRb.setTag(eachFilterCategoryData.getId());
                eachFilterCategoeyDataOptionRb.setText(eachFilterCategoryData.getName() + " (" + eachFilterCategoryData.getCount() + ")");
                eachFilterCategoeyDataOptionRb.setTextSize(14);
                eachFilterCategoeyDataOptionRb.setTextColor(getContext().getResources().getColor(R.color.text_color_primary));
                eachFilterCategoryRg.addView(eachFilterCategoeyDataOptionRb);
                eachFilterCategoryRg.setOnCheckedChangeListener(this);
            }
            mBinding.filterContainer.addView(eachFilterCategoryRg);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton selectedRadioBtn = group.findViewById(checkedId);
        if (mCategoriesDatas == null) {
            ((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.put(group.getTag().toString(), selectedRadioBtn.getTag().toString());
            updateFilterInputData();
        } else {
            ((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.put(group.getTag().toString(), selectedRadioBtn.getTag().toString());
            ((CatalogProductActivity) getContext()).onFilterByCategoryItemSelected(Integer.parseInt(selectedRadioBtn.getTag().toString()));
        }
        mBinding.doneBtn.performClick();
    }

    public void updateFilterInputData() {
        mFilterInputJson = new JSONArray();
        JSONArray selectedAttributeIdArr = new JSONArray();
        JSONArray selectedAttributeCodeArr = new JSONArray();

        for (Map.Entry pair : ((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.entrySet()) {
            Log.d(ApplicationConstant.TAG, "updateFilterInputData: " + pair.getKey() + " : " + pair.getValue());
            selectedAttributeIdArr.put(pair.getKey());
            selectedAttributeCodeArr.put(pair.getValue());
        }

        mFilterInputJson.put(selectedAttributeCodeArr);
        mFilterInputJson.put(selectedAttributeIdArr);
        ((CatalogProductActivity) getContext()).onFilterByItemSelected(mFilterInputJson);
    }

    private void loadSelectedFiltersData() {
        for (final Map.Entry pair : ((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.entrySet()) {
            ItemCatalogSelectedFilterBinding itemCatalogSelectedFilter = DataBindingUtil.inflate(LayoutInflater.from(getContext())
                    , R.layout.item_catalog_selected_filter, mBinding.selectedFiltersLayout, true);
            itemCatalogSelectedFilter.setSelectedOption(getSelectedFilterContent(pair));
            if (mCategoriesDatas == null) {
                itemCatalogSelectedFilter.clearFilterIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.entrySet().remove(pair);
                        updateFilterInputData();
                        mBinding.doneBtn.performClick();
                    }
                });
            } else {
                itemCatalogSelectedFilter.clearFilterIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.entrySet().remove(pair);
                        ((CatalogProductActivity) getContext()).onFilterByCategoryItemSelected(0);
                        mBinding.doneBtn.performClick();
                    }
                });
            }
        }
    }

    private String getSelectedFilterContent(Map.Entry pair) {
        if (mCategoriesDatas == null) {
            return CatalogHelper.getLayeredDataAttributeLabel(sAllLayeredDatas, pair) + " : " + CatalogHelper.getLayeredDataOptionLabel(sAllLayeredDatas, pair);
        } else {
            return getContext().getString(R.string.seller_category) + " : " + CatalogHelper.getLayeredDataCategoryAttributeLabel(sAllCategoryList, pair);
        }
    }

    private void clearAllFilters() {
        if (mCategoriesDatas == null) {
            ((CatalogProductActivity) getContext()).onFilterByItemSelected(new JSONArray());
        } else {
            ((CatalogProductActivity) getContext()).onFilterByCategoryItemSelected(0);
        }
        ((CatalogProductActivity) getContext()).sSellerAttributesIdOptionCodeMap.clear();
        mBinding.clearAll.setVisibility(View.GONE);
        mBinding.selectedFiltersLayout.setVisibility(View.GONE);
        mBinding.doneBtn.performClick();
    }
}