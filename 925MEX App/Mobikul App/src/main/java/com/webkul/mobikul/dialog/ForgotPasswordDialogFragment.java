package com.webkul.mobikul.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.DialogFragmentForgotPasswordBinding;
import com.webkul.mobikul.handler.ForgotPasswordDialogHandler;
import com.webkul.mobikul.model.customer.signin.SignInForgetPasswordData;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */

public class ForgotPasswordDialogFragment extends DialogFragment {

    private static final String KEY_EMAIL = "email";

    public static ForgotPasswordDialogFragment newInstance(String email) {
        ForgotPasswordDialogFragment forgotPasswordDialogFragment = new ForgotPasswordDialogFragment();
        Bundle args = new Bundle();
        args.putString(KEY_EMAIL, email);
        forgotPasswordDialogFragment.setArguments(args);
        return forgotPasswordDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DialogFragmentForgotPasswordBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_forgot_password, container, false);
        binding.setData(new SignInForgetPasswordData(getArguments().getString(KEY_EMAIL)));
        binding.setHandler(new ForgotPasswordDialogHandler(getContext(), getDialog()));
        return binding.getRoot();
    }
}