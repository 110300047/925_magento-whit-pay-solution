package com.webkul.mobikul.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.DialogFragmentSortByBinding;
import com.webkul.mobikul.model.catalog.SortingData;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SORTING_DATA;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SORTING_INPUT_JSON;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class SortByFragment extends DialogFragment implements RadioGroup.OnCheckedChangeListener {

    private final String SORTING_DATA_CODE_POSITION = "position";
    private final String SORTING_DATA_CODE_NAME = "name";
    private final String SORTING_DATA_CODE_PRICE = "price";

    int DIRECTION_HIGH_TO_LOW = 1;
    int DIRECTION_LOW_TO_HIGH = 2;

    private DialogFragmentSortByBinding mBinding;
    private ArrayList<SortingData> mSortingDatas;
    private JSONArray mSortingInputJson;

    public static SortByFragment newInstance(ArrayList<SortingData> sortingData, JSONArray sortingInputJson) {
        SortByFragment sortByFragment = new SortByFragment();
        sortByFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        Bundle args = new Bundle();
        args.putParcelableArrayList(BUNDLE_KEY_SORTING_DATA, sortingData);
        args.putString(BUNDLE_KEY_SORTING_INPUT_JSON, sortingInputJson.toString());
        sortByFragment.setArguments(args);
        return sortByFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_sort_by, container, false);
        mSortingDatas = getArguments().getParcelableArrayList(BUNDLE_KEY_SORTING_DATA);
        try {
            mSortingInputJson = new JSONArray(getArguments().getString(BUNDLE_KEY_SORTING_INPUT_JSON, ""));
        } catch (JSONException e) {
            mSortingInputJson = new JSONArray();
            e.printStackTrace();
        }
        if (mSortingDatas != null) {
            setUpSortByRg();
        }
        return mBinding.getRoot();
    }


    public void setUpSortByRg() {
        for (SortingData eachSortingData : mSortingDatas) {
            Log.d(ApplicationConstant.TAG, "SortByFragment setUpSortByRg: " + mSortingInputJson);

            RadioButton eachRb1 = new RadioButton(getContext());
            eachRb1.setText(String.format("%s %s", eachSortingData.getLabel(), getSuffix(eachSortingData.getCode(), DIRECTION_LOW_TO_HIGH)));
            eachRb1.setTextSize(14);
            eachRb1.setTextColor(getResources().getColor(R.color.text_color_primary));
            mBinding.sortNyRg.addView(eachRb1);

            RadioButton eachRb2 = new RadioButton(getContext());
            eachRb2.setText(String.format("%s %s", eachSortingData.getLabel(), getSuffix(eachSortingData.getCode(), DIRECTION_HIGH_TO_LOW)));
            eachRb2.setTextSize(14);
            eachRb2.setTextColor(getResources().getColor(R.color.text_color_primary));
            mBinding.sortNyRg.addView(eachRb2);


            /*SET SELECTION OF LAST SELECTED ITEM*/
            try {
                if (mSortingInputJson.getString(0).equals(eachSortingData.getCode())) {
                    if (mSortingInputJson.getInt(1) == 0) {
                        eachRb1.setChecked(true);
                    } else {
                        eachRb2.setChecked(true);
                    }
                }
            } catch (JSONException e) {
                Log.e(ApplicationConstant.TAG, " setUpSortByRg: " + e.getMessage());
            }
        }
        mBinding.sortNyRg.setOnCheckedChangeListener(this);
    }

    public String getSuffix(String code, int direction) {
        switch (code) {
            case SORTING_DATA_CODE_NAME:
                if (direction == DIRECTION_LOW_TO_HIGH) {
                    return getString(R.string.sort_by_suffix_a2z);
                } else {
                    return getString(R.string.sort_by_suffix_z2a);
                }

            case SORTING_DATA_CODE_POSITION:
            case SORTING_DATA_CODE_PRICE:
            default:
                if (direction == DIRECTION_LOW_TO_HIGH) {
                    return getString(R.string.sort_by_suffix_l2h);
                } else {
                    return getString(R.string.sort_by_suffix_h2l);
                }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int selectedRbPostion = group.indexOfChild(group.findViewById(checkedId));
        getDialog().dismiss();
        JSONArray sortingInputJson = new JSONArray();
        sortingInputJson.put(mSortingDatas.get(selectedRbPostion / 2).getCode());
        sortingInputJson.put(selectedRbPostion % 2);
        ((CatalogProductActivity) getActivity()).onSortByItemSelected(sortingInputJson);
    }
}