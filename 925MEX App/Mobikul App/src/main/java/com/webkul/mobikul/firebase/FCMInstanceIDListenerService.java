/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webkul.mobikul.firebase;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.firebase.RefreshToken;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FCMInstanceIDListenerService extends FirebaseInstanceIdService {
    // If you want to send messages to this application instance or
    // manage this apps subscriptions on the server side, send the
    // Instance ID token to your app server.

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is also called
     * when the InstanceID token is initially generated, so this is where
     * you retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                sendRegistrationToServer();
                subscribeTopics();
            }
        }, 10000);
    }

    /*not applicable right now*/
    private void sendRegistrationToServer() {
        Log.d(ApplicationConstant.TAG, "sendRegistrationToServer: Token send : " + FirebaseInstanceId.getInstance().getToken());

        ApiConnection.uploadTokenData(
                FirebaseInstanceId.getInstance().getToken()
                , AppSharedPref.getCustomerId(this)
                , "android")
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<RefreshToken>(this) {
            @Override
            public void onNext(RefreshToken refreshToken) {
                super.onNext(refreshToken);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    private void subscribeTopics() {
        FirebaseMessaging pubSub = FirebaseMessaging.getInstance();
        for (String topic : ApplicationConstant.DEFAULT_FCM_TOPICS) {
            pubSub.subscribeToTopic(topic);
        }
    }
    // [END refresh_token]
}