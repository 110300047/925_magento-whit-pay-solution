package com.webkul.mobikul.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.activity.OrderDetailActivity;
import com.webkul.mobikul.activity.OtherNotificationActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.DataBaseHelper;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.helper.Utils;

import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CAN_REORDER;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_FROM_NOTIFICATION;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_NOTIFICATION_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_NOTIFICATION_TYPE;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;
import static java.lang.Integer.parseInt;

public class FCMMessageReceiverService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(ApplicationConstant.TAG, "FCMMessageReceiverService onMessageReceived : " + remoteMessage);
        Log.d(ApplicationConstant.TAG, "FCMMessageReceiverService onMessageReceived: data " + remoteMessage.getData());

        try {
            Map<String, String> data = remoteMessage.getData();
            if (data.size() > 0) {
                String notificationTitle = data.get("title");
                String notificationMessage = data.get("message");
                String notificationType = data.get("notificationType");
                String notificationId = data.get("id");

                Intent intent = null;
                switch (notificationType) {
                    case "adminNotification":
//                    intent = new Intent(this, ChatActivity.class);
//                    intent.putExtra("user_name",data.get("name"));
//                    intent.putExtra("customer_id",data.get("id"));
                        break;
                    case "order":
                        intent = new Intent(this, OrderDetailActivity.class);
                        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, data.get("incrementId"));
                        intent.putExtra(BUNDLE_KEY_CAN_REORDER, data.get("canReorder"));
                        break;
                    case "sellerOrder":
                        intent = new Intent(this, ((MobikulApplication) getApplicationContext()).getSellerOrderDetailsActivity());
                        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, data.get("incrementId"));
                        break;
                    case "productApproval":
                        intent = new Intent(this, NewProductActivity.class);
                        intent.putExtra(KEY_PRODUCT_ID, parseInt(data.get("productId")));
                        intent.putExtra(KEY_PRODUCT_NAME, data.get("productName"));
                        break;
                    case "sellerApproval":
                        if (AppSharedPref.isLoggedIn(this) && AppSharedPref.getCustomerId(this) == parseInt(data.get("sellerId"))) {
                            intent = new Intent(this, ((MobikulApplication) getApplicationContext()).getSellerDashBoardActivity());
                        }
                        break;
                    default:
                        if (data.containsKey("store_id")) {
                            String storeIdString = data.get("store_id");
                            String[] storeIdsArray = storeIdString.split(",");
                            if (Arrays.asList(storeIdsArray).contains("0") || Arrays.asList(storeIdsArray).contains(String.valueOf(AppSharedPref.getStoreId(this)))) {
                                switch (notificationType) {
                                    case "product":
                                        intent = new Intent(this, NewProductActivity.class);
                                        intent.putExtra(KEY_PRODUCT_ID, parseInt(data.get("productId")));
                                        intent.putExtra(KEY_PRODUCT_NAME, data.get("productName"));
                                        break;
                                    case "category":
                                        intent = new Intent(this, ((MobikulApplication) getApplication()).getCatalogProductPageClass());
                                        intent.putExtra(KEY_CATEGORY_ID, parseInt(data.get("categoryId")));
                                        intent.putExtra(KEY_CATEGORY_NAME, data.get("categoryName"));
                                        break;
                                    case "others":
                                        intent = new Intent(this, OtherNotificationActivity.class);
                                        break;
                                    case "custom":
                                        intent = new Intent(this, ((MobikulApplication) getApplication()).getCatalogProductPageClass());
                                        break;
                                    default:
                                        return;
                                }
                                try {
                                    DataBaseHelper db = new DataBaseHelper(this);
                                    db.addNotificationID(String.valueOf(notificationId));
                                    db.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Utils.setNotificationBadge(this);
                            }
                        }
                }

                if (intent != null) {
                    intent.putExtra(BUNDLE_KEY_NOTIFICATION_TYPE, notificationType);
                    intent.putExtra(BUNDLE_KEY_FROM_NOTIFICATION, "");
                    intent.putExtra(BUNDLE_KEY_NOTIFICATION_ID, notificationId);
                    sendNotification(notificationTitle, notificationMessage, data.get("banner_url") == null || data.get("banner_url").isEmpty() ? "" : data.get("banner_url"), Integer.parseInt(notificationId), intent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param notificationContent FCM message body received.
     * @param notificationId
     * @param intent
     */
    private void sendNotification(String notificationTitle, String notificationContent, String bannerUrl, int notificationId, Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_app)
                .setContentTitle(notificationTitle)
                .setContentText(notificationContent)
                .setLargeIcon(icon)
                .setAutoCancel(true)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationCompat.BigPictureStyle notificationBigPictureStyle = new NotificationCompat.BigPictureStyle();
        try {
            if (!bannerUrl.isEmpty()) {
                Bitmap remote_picture = BitmapFactory.decodeStream((InputStream) new URL(bannerUrl).getContent());
                notificationBigPictureStyle.bigPicture(remote_picture);
                notificationBigPictureStyle.setSummaryText(notificationContent);
                notificationBuilder.setStyle(notificationBigPictureStyle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
    }
}