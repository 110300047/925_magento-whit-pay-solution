package com.webkul.mobikul.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.activity.LoginAndSignUpActivity;
import com.webkul.mobikul.adapter.NewAddressStreetAddressRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.FragmentBillingShippingBinding;
import com.webkul.mobikul.handler.BillingShippingFragmentHandler;
import com.webkul.mobikul.helper.AddressHelper;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.checkout.BillingShippingInfoData;
import com.webkul.mobikul.model.customer.address.CheckCustomerByEmailResponseData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.RC_APP_SIGN_IN;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_CHECK_LOCATION_SETTINGS;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_LOCATION_FETCH_ADDRESS_PERMISSION;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;
import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;
import static com.webkul.mobikul.fragment.DatePickerFragment.BILLING_ADDRESS_DOB;
import static com.webkul.mobikul.fragment.DatePickerFragment.SHIPPING_ADDRESS_DOB;
import static com.webkul.mobikul.helper.AlertDialogHelper.dismiss;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created with passion and love by vedesh.kumar on 11/1/17. @Webkul Software Pvt. Ltd
 */

public class BillingShippingFragment extends Fragment implements DatePickerFragment.OnDateSelected, LocationListener {

    public FragmentBillingShippingBinding mBinding;
    public BillingShippingInfoData mBillingShippingInfoData;
    public boolean mUpdateShippingAddressFromGps = false;
    public LocationManager mLocationManager;
    public boolean isEmailChecked = true;

    public static BillingShippingFragment newInstance() {
        return new BillingShippingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_billing_shipping, container, false);
        mBinding.setHandler(new BillingShippingFragmentHandler(getContext(), BillingShippingFragment.this, this));
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showDefaultAlertDialog(getContext());

        ApiConnection.getBillingShippingInfoData(
                AppSharedPref.getStoreId(getContext())
                , AppSharedPref.getQuoteId(getContext())
                , AppSharedPref.getCustomerId(getContext()))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BillingShippingInfoData>(getContext()) {
            @Override
            public void onNext(BillingShippingInfoData billingShippingInfoData) {
                super.onNext(billingShippingInfoData);
                mBillingShippingInfoData = billingShippingInfoData;
                mBillingShippingInfoData.setContext(getContext());
                mBillingShippingInfoData.initAddressList();

                ((CheckoutActivity) getContext()).mBinding.setIsVirtual(mBillingShippingInfoData.isIsVirtual());

                if (AppSharedPref.isLoggedIn(getContext())) {
                    String[] name = AppSharedPref.getCustomerName(getContext()).split(" ");
                    mBillingShippingInfoData.setFirstName(name[0]);
                    mBillingShippingInfoData.setLastName(name[1]);
                    mBillingShippingInfoData.setBillingEmail(AppSharedPref.getCustomerEmail(getContext()));
                }

                setUpBillingAddressSpinner();
                setUpShippingAddressSpinner();

                setUpBillingAddressPrefix();
                setUpBillingAddressSuffix();

                setUpShippingAddressPrefix();
                setUpShippingAddressSuffix();

                // creating a country name list and country name list
                mBillingShippingInfoData.initCountryNameList();
                setUpBillingCountrySpinner();
                setUpShippingCountrySpinner();

                mBinding.billingStreetAddressRv.setLayoutManager(new LinearLayoutManager(getContext()));
                mBinding.billingStreetAddressRv.setAdapter(new NewAddressStreetAddressRvAdapter(getContext()
                        , new ArrayList<String>()
                        , mBillingShippingInfoData.getStreetLineCount()));

                loadBillingStreetAddressRv(new ArrayList<String>());

                mBinding.shippingStreetAddressRv.setLayoutManager(new LinearLayoutManager(getContext()));
                mBinding.shippingStreetAddressRv.setAdapter(new NewAddressStreetAddressRvAdapter(getContext()
                        , new ArrayList<String>()
                        , mBillingShippingInfoData.getStreetLineCount()));

                if (!AppSharedPref.isLoggedIn(getContext())) {
                    isEmailChecked = false;
                    setupEmailChangeListener();
                }
                loadShippingStreetAddressRv(new ArrayList<String>());

                mBinding.setData(mBillingShippingInfoData);
                mBinding.executePendingBindings();
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });

        mBinding.scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, final int scrollY, int oldScrollX, int oldScrollY) {
                if ((scrollY - oldScrollY) < 0 || (scrollY > (mBinding.scrollView.getChildAt(0).getHeight() - mBinding.scrollView.getHeight() - 100))) {
                    mBinding.continueBtn.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
                } else {
                    mBinding.continueBtn.animate().alpha(0f).translationY(mBinding.continueBtn.getHeight()).setInterpolator(new AccelerateInterpolator(1.4f));
                }
            }
        });
    }

    private void setupEmailChangeListener() {
        mBinding.emailTiet.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!((TextInputEditText) v).getText().toString().trim().isEmpty() && EMAIL_PATTERN.matcher(((TextInputEditText) v).getText().toString().trim()).matches()) {
                        mBinding.progressBar.setVisibility(View.VISIBLE);
                        ApiConnection.checkCustomerByEmail(
                                AppSharedPref.getStoreId(getContext())
                                , ((TextInputEditText) v).getText().toString())
                                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CheckCustomerByEmailResponseData>(getContext()) {
                            @Override
                            public void onNext(CheckCustomerByEmailResponseData checkCustomerByEmail) {
                                super.onNext(checkCustomerByEmail);
                                mBinding.progressBar.setVisibility(View.GONE);
                                if (checkCustomerByEmail.isSuccess()) {
                                    isEmailChecked = true;
                                    if (checkCustomerByEmail.isCustomerExist()) {
                                        showLoginAlertDialog();
                                    }
                                }
                            }

                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                                mBinding.progressBar.setVisibility(View.GONE);
                                isEmailChecked = true;
                            }
                        });
                    }
                }
            }
        });
    }

    public void showLoginAlertDialog() {
        showAlertDialogWithClickListener(getContext(), SweetAlertDialog.NORMAL_TYPE, getContext().getResources().getString(R.string.login), getContext().getString(R.string.customer_exist), getContext().getResources().getString(R.string.login), getContext().getResources().getString(R.string.checkout_btn_continue_shipping_billing)
                , new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(getContext(), LoginAndSignUpActivity.class);
                        intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_IN_PAGE);
                        ((CheckoutActivity) getContext()).startActivityForResult(intent, RC_APP_SIGN_IN);
                        sweetAlertDialog.dismissWithAnimation();
                    }
                }, null);
    }

    private void setUpBillingCountrySpinner() {

        mBinding.billingCountrySpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                , mBillingShippingInfoData.getCountryNameList()));
//        mBinding.billingCountrySpinner.setSelection(mBillingShippingInfoData.getSelectedCountryPositionFromAddressData());
        mBinding.billingCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mBillingShippingInfoData.setSelectedBillingCountryPosition(position);
                if (mBillingShippingInfoData.getCountryData().get(position).getStates() != null) {
                    mBillingShippingInfoData.setHasBillingStateData(true);
                    mBinding.billingStateSpinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, mBillingShippingInfoData.getStateNameList(position)));
                    mBinding.billingStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mBillingShippingInfoData.setSelectedBillingStatePosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    mBillingShippingInfoData.setHasBillingStateData(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUpShippingCountrySpinner() {

        mBinding.shippingCountrySpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                , mBillingShippingInfoData.getCountryNameList()));
//        mBinding.billingCountrySpinner.setSelection(mBillingShippingInfoData.getSelectedCountryPositionFromAddressData());
        mBinding.shippingCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mBillingShippingInfoData.setSelectedShippingCountryPosition(position);
                if (mBillingShippingInfoData.getCountryData().get(position).getStates() != null) {
                    mBillingShippingInfoData.setHasShippingStateData(true);
                    mBinding.shippingStateSpinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item
                            , mBillingShippingInfoData.getStateNameList(position)));
                    mBinding.shippingStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mBillingShippingInfoData.setSelectedShippingStatePosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    mBillingShippingInfoData.setHasShippingStateData(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUpBillingAddressPrefix() {
        if (mBillingShippingInfoData.isIsPrefixVisible() && mBillingShippingInfoData.isPrefixHasOptions()
                && mBillingShippingInfoData.getPrefixOptions().size() > 0) {
            mBinding.billingPrefixSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                    , mBillingShippingInfoData.getPrefixOptions()
            ));
            mBinding.billingPrefixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mBillingShippingInfoData.setBillingPrefixValue(parent.getSelectedItem().toString());
                    mBillingShippingInfoData.setBillingPrefixPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void setUpBillingAddressSuffix() {
        if (mBillingShippingInfoData.isIsSuffixVisible() && mBillingShippingInfoData.isSuffixHasOptions()
                && mBillingShippingInfoData.getSuffixOptions().size() > 0) {
            mBinding.billingSuffixSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                    , mBillingShippingInfoData.getSuffixOptions()
            ));
            mBinding.billingSuffixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mBillingShippingInfoData.setBillingSuffixValue(parent.getSelectedItem().toString());
                    mBillingShippingInfoData.setBillingSuffixPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void setUpBillingAddressSpinner() {
        mBinding.billingAddressSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                , mBillingShippingInfoData.getAddressList()));
    }

    private void setUpShippingAddressSpinner() {
        mBinding.shippingAddressSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                , mBillingShippingInfoData.getAddressList()));
    }

    private void setUpShippingAddressPrefix() {
        if (mBillingShippingInfoData.isIsPrefixVisible() && mBillingShippingInfoData.isPrefixHasOptions()
                && mBillingShippingInfoData.getPrefixOptions().size() > 0) {
            mBinding.shippingPrefixSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                    , mBillingShippingInfoData.getPrefixOptions()
            ));
            mBinding.shippingPrefixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mBillingShippingInfoData.setShippingPrefixValue(parent.getSelectedItem().toString());
                    mBillingShippingInfoData.setShippingPrefixPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void setUpShippingAddressSuffix() {
        if (mBillingShippingInfoData.isIsSuffixVisible() && mBillingShippingInfoData.isSuffixHasOptions()
                && mBillingShippingInfoData.getSuffixOptions().size() > 0) {
            mBinding.shippingSuffixSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                    , mBillingShippingInfoData.getSuffixOptions()
            ));
            mBinding.shippingSuffixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mBillingShippingInfoData.setShippingSuffixValue(parent.getSelectedItem().toString());
                    mBillingShippingInfoData.setShippingSuffixPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public void onDateSet(int year, int month, int day, @SuppressWarnings("UnusedParameters") int type) {
        if (type == BILLING_ADDRESS_DOB) {
            mBinding.billingDobTiet.setText(Utils.formatDate(mBillingShippingInfoData.getDateFormat(), year, month, day));
        } else if (type == SHIPPING_ADDRESS_DOB) {
            mBinding.shippingDobTiet.setText(Utils.formatDate(mBillingShippingInfoData.getDateFormat(), year, month, day));
        }
    }

    public void fetchCurrentAddress(boolean updateShippingAddressFromGps) {
        mUpdateShippingAddressFromGps = updateShippingAddressFromGps;
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                showToast(getContext(), getString(R.string.access_needed_to_fill_address), Toast.LENGTH_LONG, 0);
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        RC_LOCATION_FETCH_ADDRESS_PERMISSION);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        RC_LOCATION_FETCH_ADDRESS_PERMISSION);
            }
            return;
        }
        checkIsGpsEnabled();
    }

    private void checkIsGpsEnabled() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getContext()).addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        ((BaseActivity) getContext()).mSweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
                        ((BaseActivity) getContext()).mSweetAlertDialog.setTitleText(getContext().getString(R.string.please_wait));
                        ((BaseActivity) getContext()).mSweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorAccent));
                        ((BaseActivity) getContext()).mSweetAlertDialog.setCancelable(true);
                        ((BaseActivity) getContext()).mSweetAlertDialog.show();
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, BillingShippingFragment.this);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(getActivity(), RC_CHECK_LOCATION_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        SnackbarHelper.getSnackbar(getActivity(), getContext().getString(R.string.something_went_wrong)).show();
                        break;
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mLocationManager != null && !mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            dismiss(getContext());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        try {
            Log.d("location", latitude + "-------" +
                    longitude);
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addresses != null) {
                displayAddressOutput(addresses.get(0));
            }
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showToast(getContext(), getString(R.string.access_needed_to_fill_address), Toast.LENGTH_LONG, 0);
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_LOCATION_FETCH_ADDRESS_PERMISSION);
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_LOCATION_FETCH_ADDRESS_PERMISSION);
                }
                return;
            }
            dismiss(getContext());
            mLocationManager.removeUpdates(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayAddressOutput(Address address) {
        if (mUpdateShippingAddressFromGps) {
            mBillingShippingInfoData.setShippingStateValue(address.getAdminArea());
            mBillingShippingInfoData.setShippingCity(address.getLocality());
            mBillingShippingInfoData.setShippingPostCode(address.getPostalCode());

            int newCountryPostion = AddressHelper.getCountryPositionFromCountryCode(mBillingShippingInfoData.getCountryData()
                    , address.getCountryCode());

            if (newCountryPostion != 0) {
                getBinding().shippingCountrySpinner.setSelection(newCountryPostion);
            }

            List<String> street = new ArrayList<>();
            for (int addressLineIdx = 0; addressLineIdx < 2; addressLineIdx++) {
                if (addressLineIdx < mBillingShippingInfoData.getStreetLineCount()) {
                    street.add(address.getAddressLine(addressLineIdx));
                }
            }
            loadShippingStreetAddressRv(street);
            mBinding.shippingGPSBtn.setEnabled(false);
        } else {
            mBillingShippingInfoData.setBillingStateValue(address.getAdminArea());
            mBillingShippingInfoData.setBillingCity(address.getLocality());
            mBillingShippingInfoData.setBillingPostCode(address.getPostalCode());

            int newCountryPostion = AddressHelper.getCountryPositionFromCountryCode(mBillingShippingInfoData.getCountryData()
                    , address.getCountryCode());

            if (newCountryPostion != 0) {
                getBinding().billingCountrySpinner.setSelection(newCountryPostion);
            }

            List<String> street = new ArrayList<>();
            for (int addressLineIdx = 0; addressLineIdx < 2; addressLineIdx++) {
                if (addressLineIdx < mBillingShippingInfoData.getStreetLineCount()) {
                    street.add(address.getAddressLine(addressLineIdx));
                }
            }
            loadBillingStreetAddressRv(street);
            mBinding.billingGPSBtn.setEnabled(false);
        }
        mBinding.setData(mBillingShippingInfoData);
    }

    public FragmentBillingShippingBinding getBinding() {
        return mBinding;
    }

    private void loadShippingStreetAddressRv(List<String> street) {
        mBinding.shippingStreetAddressRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.shippingStreetAddressRv.setAdapter(new NewAddressStreetAddressRvAdapter(getContext()
                , street
                , mBillingShippingInfoData.getStreetLineCount()));
    }

    private void loadBillingStreetAddressRv(List<String> street) {
        mBinding.billingStreetAddressRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.billingStreetAddressRv.setAdapter(new NewAddressStreetAddressRvAdapter(getContext()
                , street
                , mBillingShippingInfoData.getStreetLineCount()));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}