package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.DialogFragmentCardBinding;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.model.checkout.OrderReviewRequestData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import mx.openpay.android.Openpay;
import mx.openpay.android.OperationCallBack;
import mx.openpay.android.OperationResult;
import mx.openpay.android.exceptions.OpenpayServiceException;
import mx.openpay.android.exceptions.ServiceUnavailableException;
import mx.openpay.android.model.Card;
import mx.openpay.android.model.Token;
import mx.openpay.android.validation.CardValidator;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 21/3/17. @Webkul Software Pvt. Ltd
 */
public class CardFragment extends DialogFragment implements View.OnClickListener {

    private static PaymentMethodFragment mPaymentMethodFragmentContext;
    private DialogFragmentCardBinding mBinding;
    private String mDeviceSessionId;
    private String mOpenpayToken;
    private String mCardType;

    public static CardFragment newInstance(PaymentMethodFragment mContext) {
        mPaymentMethodFragmentContext = mContext;
        return new CardFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Viisualiza la vista para ingresar los datos de la tarjeta
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_card, container, false);
        try {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().getAttributes().windowAnimations = R.style.CustomDialogFragmentAnimation;
            setCancelable(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mBinding.payBtn.setOnClickListener(this);
        mBinding.cancelBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.pay_btn) {
            onClickPayBtn();
        } else if (v.getId() == R.id.cancel_btn) {
            dismiss();
        }
    }

    public void onClickPayBtn() {
        boolean isFormValidated = true;

        if (mBinding.cardHolder.getText().toString().trim().isEmpty()) {
            isFormValidated = false;
            mBinding.cardHolder.requestFocus();
            mBinding.cardHolder.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        }

        if (mBinding.cardNumber.getText().toString().trim().isEmpty()) {
            isFormValidated = false;
            mBinding.cardNumber.requestFocus();
            mBinding.cardNumber.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        } else if (!CardValidator.validateNumber(mBinding.cardNumber.getText().toString().trim())) {
            isFormValidated = false;
            mBinding.cardNumber.requestFocus();
            mBinding.cardNumber.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        } else {
            Log.d(ApplicationConstant.TAG, "onClickPayBtn: " + CardValidator.getType(mBinding.cardNumber.getText().toString().trim()));
            switch (CardValidator.getType(mBinding.cardNumber.getText().toString().trim())) {
                case AMEX:
                    mCardType = "AE";
                    break;
                case VISA:
                    mCardType = "VI";
                    break;
                case MASTERCARD:
                    mCardType = "MC";
                    break;
            }
        }

        if (mBinding.month.getText().toString().trim().isEmpty()) {
            isFormValidated = false;
            mBinding.month.requestFocus();
            mBinding.month.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        } else if (Integer.parseInt(mBinding.month.getText().toString().trim()) > 12 || Integer.parseInt(mBinding.month.getText().toString().trim()) < 0) {
            isFormValidated = false;
            mBinding.month.requestFocus();
            mBinding.month.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        } else if (!CardValidator.validateMonth(Integer.valueOf(mBinding.month.getText().toString().trim()))) {
            isFormValidated = false;
            mBinding.month.requestFocus();
            mBinding.month.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        }

        if (mBinding.year.getText().toString().trim().isEmpty()) {
            isFormValidated = false;
            mBinding.year.requestFocus();
            mBinding.year.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        } else if (mBinding.year.getText().toString().trim().length() < 2) {
            isFormValidated = false;
            mBinding.year.requestFocus();
            mBinding.year.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        }

        if (mBinding.cvv.getText().toString().trim().isEmpty()) {
            isFormValidated = false;
            mBinding.cvv.requestFocus();
            mBinding.cvv.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        } else if (!CardValidator.validateCVV(mBinding.cvv.getText().toString().trim(), mBinding.cardNumber.getText().toString().trim())) {
            isFormValidated = false;
            mBinding.cvv.requestFocus();
            mBinding.cvv.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake_error));
        }

        if (isFormValidated) {
            showDefaultAlertDialog(getContext());
            Openpay openpay = new Openpay(
                    mPaymentMethodFragmentContext.mShippingPaymentMethodInfoData.getMerchantId()
                    , mPaymentMethodFragmentContext.mShippingPaymentMethodInfoData.getPublicKey()
                    , mPaymentMethodFragmentContext.mShippingPaymentMethodInfoData.getIsSandbox() == 0);
            mDeviceSessionId = openpay.getDeviceCollectorDefaultImpl().setup(getActivity());

            Card card = new Card();
            card.holderName(mBinding.cardHolder.getText().toString().trim());
            card.cardNumber(mBinding.cardNumber.getText().toString().trim());
            card.expirationMonth(Integer.valueOf(mBinding.month.getText().toString().trim()));
            card.expirationYear(Integer.valueOf(mBinding.year.getText().toString().trim()));
            card.cvv2(mBinding.cvv.getText().toString().trim());

            openpay.createToken(card, new OperationCallBack<Token>() {

                @Override
                public void onSuccess(OperationResult arg0) {
                    Token openpayToken = (Token) arg0.getResult();
                    mOpenpayToken = openpayToken.getId();
                    callOrderReview();
                }

                @Override
                public void onError(OpenpayServiceException arg0) {
                    AlertDialogHelper.dismiss(getContext());
                    ToastHelper.showToast(getContext(), arg0.getDescription(), Toast.LENGTH_LONG, 0);
                }

                @Override
                public void onCommunicationError(ServiceUnavailableException arg0) {
                    AlertDialogHelper.dismiss(getContext());
                    ToastHelper.showToast(getContext(), arg0.getMessage(), Toast.LENGTH_LONG, 0);
                }
            });
        }
    }

    /*private void callOrderReview() {
        ApiConnection.getOrderReviewData(
                AppSharedPref.getStoreId(getContext())
                , AppSharedPref.getQuoteId(getContext())
                , AppSharedPref.getCustomerId(getContext())
                , mPaymentMethodFragmentContext.getSelectedPaymentMethodCode()
                , AppSharedPref.getCurrencyCode(getContext()))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<OrderReviewRequestData>(getContext()) {
            @Override
            public void onNext(OrderReviewRequestData orderReviewRequestData) {
                super.onNext(orderReviewRequestData);
                if (orderReviewRequestData.isSuccess()) {
                        FragmentManager fragmentManager = ((CheckoutActivity) getContext()).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.add(R.id.container, OrderReviewFragment.newInstance(mPaymentMethodFragmentContext.getSelectedPaymentMethodCode(), orderReviewRequestData), OrderReviewFragment.class.getSimpleName());
                    fragmentTransaction.addToBackStack(OrderReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
                    fragmentTransaction.commit();
                    getDialog().dismiss();
                } else {
                    showToast(getContext(), orderReviewRequestData.getMessage(), Toast.LENGTH_LONG, 0);
                    if (orderReviewRequestData.getCartCount() == 0) {
                        ((CheckoutActivity) getContext()).finish();
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }*/

    private void callOrderReview() {
        ApiConnection.getOrderReviewData(
                AppSharedPref.getStoreId(getContext())
                , AppSharedPref.getQuoteId(getContext())
                , AppSharedPref.getCustomerId(getContext())
                , mPaymentMethodFragmentContext.mBinding.getHandler().mShippingMethod
                , mBinding.cvv.getText().toString().trim()
                , mBinding.month.getText().toString().trim()
                , "20" + mBinding.year.getText().toString().trim()
                , mBinding.cardNumber.getText().toString().trim()
                , mCardType
                , mDeviceSessionId
                , mOpenpayToken
                , mPaymentMethodFragmentContext.getSelectedPaymentMethodCode()
                , AppSharedPref.getCurrencyCode(getContext()))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<OrderReviewRequestData>(getContext()) {
            @Override
            public void onNext(OrderReviewRequestData orderReviewRequestData) {
                super.onNext(orderReviewRequestData);
                if (orderReviewRequestData.isSuccess()) {
                    FragmentManager fragmentManager = ((CheckoutActivity) getContext()).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.add(R.id.container, OrderReviewFragment.newInstance(mPaymentMethodFragmentContext.getSelectedPaymentMethodCode(), orderReviewRequestData), OrderReviewFragment.class.getSimpleName());
                    fragmentTransaction.addToBackStack(OrderReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
                    fragmentTransaction.commit();
                    getDialog().dismiss();
                } else {
                    showToast(getContext(), orderReviewRequestData.getMessage(), Toast.LENGTH_LONG, 0);
                    if (orderReviewRequestData.getCartCount() == 0) {
                        ((CheckoutActivity) getContext()).finish();
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }
}
