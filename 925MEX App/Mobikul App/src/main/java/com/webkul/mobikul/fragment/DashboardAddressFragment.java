package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.DashboardAdditionalAddressRecyclerViewAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.FragmentDashboardAddressBinding;
import com.webkul.mobikul.handler.AddressBookFragmentHandler;
import com.webkul.mobikul.handler.DashboardAdditionalAddressRecyclerViewHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.model.customer.address.AddressBookResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class DashboardAddressFragment extends Fragment implements DashboardAdditionalAddressRecyclerViewHandler.OnAdditionalAddressDeletedListener {


    private FragmentDashboardAddressBinding mBinding;

    public static DashboardAddressFragment newInstance() {
        return new DashboardAddressFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard_address, container, false);
        mBinding.setHandler(new AddressBookFragmentHandler(getContext()));
        return mBinding.getRoot();
    }

    private void updateDashboardAddressFragment() {
        mBinding.setLazyLoading(true);
        ApiConnection.getAddressBookData(AppSharedPref.getCustomerId(getContext())
                , AppSharedPref.getStoreId(getContext()))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<AddressBookResponseData>(getContext()) {
            @Override
            public void onNext(AddressBookResponseData addressBookResponseData) {
                super.onNext(addressBookResponseData);
                onResponseRecieved(addressBookResponseData);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mBinding.setLazyLoading(false);
            }
        });
    }

    private void onResponseRecieved(AddressBookResponseData addressBookResponseData) {
        mBinding.setData(addressBookResponseData);
        mBinding.executePendingBindings();
        mBinding.additionalAddressRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.additionalAddressRv.setAdapter(new DashboardAdditionalAddressRecyclerViewAdapter(getContext(), addressBookResponseData.getAdditionalAddress(), DashboardAddressFragment.this));
        mBinding.additionalAddressRv.setNestedScrollingEnabled(false);
        mBinding.setLazyLoading(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateDashboardAddressFragment();
    }

    @Override
    public void onAdditionalAddressDeleted() {
        updateDashboardAddressFragment();
    }
}
