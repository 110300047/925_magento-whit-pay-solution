package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.DashboardOrderRecyclerViewAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.FragmentOrderAddressBinding;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.customer.order.OrderListResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_PAGE_COUNT;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_LOAD_SELLER_ORDERS;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class DashboardOrderFragment extends Fragment {

    private FragmentOrderAddressBinding mBinding;
    private int mPageNumber = DEFAULT_PAGE_COUNT;
    private boolean isFirstCall = true;
    private OrderListResponseData mOrderListResponseData;

    public static DashboardOrderFragment newInstance(boolean isLoadSellerOrder) {
        DashboardOrderFragment dashboardOrderFragment = new DashboardOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(BUNDLE_KEY_LOAD_SELLER_ORDERS, isLoadSellerOrder);
        dashboardOrderFragment.setArguments(bundle);
        return dashboardOrderFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_address, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callApi();
    }

    private void callApi() {
        mBinding.setLazyLoading(true);
        ApiConnection.getOrderListData(AppSharedPref.getCustomerId(getContext())
                , AppSharedPref.getStoreId(getContext())
                , mPageNumber++)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<OrderListResponseData>(getContext()) {
            @Override
            public void onNext(OrderListResponseData orderListResponseData) {
                super.onNext(orderListResponseData);
                onResponseRecieved(orderListResponseData);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    private void onResponseRecieved(OrderListResponseData orderListResponseData) {
        if (isFirstCall) {
            isFirstCall = false;
            mOrderListResponseData = orderListResponseData;
            mBinding.setData(mOrderListResponseData);
            mBinding.executePendingBindings();

            mBinding.orderRv.setLayoutManager(new LinearLayoutManager(getContext()));
            mBinding.orderRv.setAdapter(new DashboardOrderRecyclerViewAdapter(getContext(), mOrderListResponseData.getOrderList()));

            mBinding.orderRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    int lastCompletelyVisibleItemPosition;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    callLazyConnection(lastCompletelyVisibleItemPosition);
                }
            });
        } else {
            mOrderListResponseData.getOrderList().addAll(orderListResponseData.getOrderList());
            mBinding.orderRv.getAdapter().notifyDataSetChanged();
        }
        mBinding.setLazyLoading(false);
    }

    private void callLazyConnection(int lastCompletelyVisibleItemPosition) {
        if (!mBinding.getLazyLoading() && mOrderListResponseData.getOrderList().size() != mOrderListResponseData.getTotalCount()
                && lastCompletelyVisibleItemPosition == (mOrderListResponseData.getOrderList().size() - 1)) {
            callApi();
        }
    }
}