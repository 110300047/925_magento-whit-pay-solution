package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.DashboardReviewRecyclerViewAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.FragmentReviewAddressBinding;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.review.ReviewListResponseData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.helper.AlertDialogHelper.dismiss;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED;


/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class DashboardReviewFragment extends Fragment {

    private FragmentReviewAddressBinding mBinding;
    private int mPageNumber = 1;
    private ReviewListResponseData mReviewListResponseData;
    private boolean isFirstCall = true;

    public static DashboardReviewFragment newInstance() {
        return new DashboardReviewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_review_address, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callApi();
    }

    private void callApi() {
        mBinding.setLazyLoading(true);
        ApiConnection.getReviewListData(AppSharedPref.getCustomerId(getContext())
                , mPageNumber++
                , AppSharedPref.getStoreId(getContext())
                , Utils.getScreenWidth())

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ReviewListResponseData>(getContext()) {
            @Override
            public void onNext(ReviewListResponseData reviewListResponseData) {
                super.onNext(reviewListResponseData);
                onResponseRecieved(reviewListResponseData);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    private void onResponseRecieved(ReviewListResponseData reviewListResponseData) {
        if (isFirstCall) {
            mReviewListResponseData = reviewListResponseData;
            mBinding.setData(mReviewListResponseData);

            if (reviewListResponseData.getReviewList() != null || reviewListResponseData.getReviewList().size() > 0) {
                mBinding.reviewRv.setLayoutManager(new LinearLayoutManager(getContext()));
                mBinding.reviewRv.setAdapter(new DashboardReviewRecyclerViewAdapter(getContext(), reviewListResponseData.getReviewList()));
                mBinding.reviewRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        int lastCompletelyVisibleItemPosition;
                        lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                        callLazyConnection(lastCompletelyVisibleItemPosition);
                    }
                });
            }
        } else {
            mReviewListResponseData.getReviewList().addAll(mReviewListResponseData.getReviewList());
            mBinding.reviewRv.getAdapter().notifyDataSetChanged();
        }
        mBinding.setLazyLoading(false);
        dismiss(getContext());
    }

    private void callLazyConnection(int lastCompletelyVisibleItemPosition) {
        if (!mBinding.getLazyLoading() && mReviewListResponseData.getReviewList().size() != mReviewListResponseData.getTotalCount()
                && lastCompletelyVisibleItemPosition == (mReviewListResponseData.getReviewList().size() - 1)) {
            isFirstCall = false;
            callApi();
        }
    }
}
