package com.webkul.mobikul.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_DOB_TYPE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_DOB_VALUE;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    public static final String DEFAULT_DATE_FORMAT = "MM/dd/yy";

    /* Constant for date selected*/
    public static final int UNKNOWN_DOB = 0;
    public static final int BILLING_ADDRESS_DOB = 1;
    public static final int SHIPPING_ADDRESS_DOB = 2;
    public static final int FROM_DATE_DOB = 3;
    public static final int TO_DATE_DOB = 4;

    private static OnDateSelected onDateSelected;

    public static DatePickerFragment newInstance(OnDateSelected onDateSelected, String dobValue, int type) {
        DatePickerFragment.onDateSelected = onDateSelected;
        Bundle args = new Bundle();
        DatePickerFragment fragment = new DatePickerFragment();
        args.putString(BUNDLE_KEY_DOB_VALUE, dobValue);
        args.putInt(BUNDLE_KEY_DOB_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        try {
            //noinspection ConstantConditions
            String dobValue[] = getArguments().getString(BUNDLE_KEY_DOB_VALUE).split(" ");
            String splittedDob[] = dobValue[0].split("-");
            return new DatePickerDialog(getActivity(), this, Integer.parseInt(splittedDob[0]), Integer.parseInt(splittedDob[1])
                    , Integer.parseInt(splittedDob[2]));
        } catch (Exception e) {
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
//        	DATE PICKER RETURN MONTH BETWEEN [0-11]
//        VALUE OF MONTH IS INCREMENTED
//        String dobValue[] = getArguments().getString(BUNDLE_KEY_DOB_VALUE).split(" ");
//        https://code.google.com/p/android/issues/detail?id=231801&thanks=231801&ts=1483958066
        onDateSelected.onDateSet(year, month, day, getArguments().getInt(BUNDLE_KEY_DOB_TYPE));
//        onDateSelected.onDateSet(year, month + 1, day);
    }


    public interface OnDateSelected {
        void onDateSet(int year, int month, int day, int type);
    }
}