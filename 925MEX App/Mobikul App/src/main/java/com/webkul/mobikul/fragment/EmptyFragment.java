package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.FragmentEmptyBinding;
import com.webkul.mobikul.handler.EmptyFragmentHandler;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_EMPTY_FRAGMENT_DRAWABLE_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_EMPTY_FRAGMENT_HIDE_CONTINUE_SHOPPING_BTN;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_EMPTY_FRAGMENT_SUBTITLE_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_EMPTY_FRAGMENT_TITLE_ID;

/**
 * Created by vedesh.kumar on 24/1/17. @Webkul Software Pvt. Ltd
 */

public class EmptyFragment extends Fragment {
    private FragmentEmptyBinding mBinding;

    public static EmptyFragment newInstance(int ic_vector_empty_cart, String empty_bag, String add_item_to_your_bag_now) {
        EmptyFragment emptyFragment = new EmptyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_KEY_EMPTY_FRAGMENT_DRAWABLE_ID, ic_vector_empty_cart);
        bundle.putString(BUNDLE_KEY_EMPTY_FRAGMENT_TITLE_ID, empty_bag);
        bundle.putString(BUNDLE_KEY_EMPTY_FRAGMENT_SUBTITLE_ID, add_item_to_your_bag_now);
        emptyFragment.setArguments(bundle);
        return emptyFragment;
    }

    public static EmptyFragment newInstance(int ic_vector_empty_cart, String empty_bag, String add_item_to_your_bag_now, boolean hideContinueShoppingBtn) {
        EmptyFragment emptyFragment = new EmptyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_KEY_EMPTY_FRAGMENT_DRAWABLE_ID, ic_vector_empty_cart);
        bundle.putString(BUNDLE_KEY_EMPTY_FRAGMENT_TITLE_ID, empty_bag);
        bundle.putString(BUNDLE_KEY_EMPTY_FRAGMENT_SUBTITLE_ID, add_item_to_your_bag_now);
        bundle.putBoolean(BUNDLE_KEY_EMPTY_FRAGMENT_HIDE_CONTINUE_SHOPPING_BTN, hideContinueShoppingBtn);
        emptyFragment.setArguments(bundle);
        return emptyFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_empty, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mBinding.setHideContinueShoppingBtn(getArguments().getBoolean(BUNDLE_KEY_EMPTY_FRAGMENT_HIDE_CONTINUE_SHOPPING_BTN));
        mBinding.setEmptyImage(getArguments().getInt(BUNDLE_KEY_EMPTY_FRAGMENT_DRAWABLE_ID));
        mBinding.setTitle(getArguments().getString(BUNDLE_KEY_EMPTY_FRAGMENT_TITLE_ID));
        mBinding.setSubtitle(getArguments().getString(BUNDLE_KEY_EMPTY_FRAGMENT_SUBTITLE_ID));
        mBinding.setHandler(new EmptyFragmentHandler());
    }
}