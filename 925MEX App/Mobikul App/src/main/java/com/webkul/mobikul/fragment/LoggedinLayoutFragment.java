package com.webkul.mobikul.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiInterface;
import com.webkul.mobikul.connection.RetrofitClient;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.CustomNavEndHeaderLoggedinBinding;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ImageHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.NavDrawerEndData;
import com.webkul.mobikul.model.customer.accountInfo.UploadPicResponseData;

import java.io.ByteArrayOutputStream;
import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.webkul.mobikul.helper.AlertDialogHelper.dismiss;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_BANNER_IMAGE;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_PROFILE_IMAGE;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 17/1/17. @Webkul Software Private limited
 */

public class LoggedinLayoutFragment extends Fragment implements View.OnClickListener {

    public static final String UPLOAD_PROFILE_PIC_CONTROLLER = "mobikulhttp/index/uploadprofilepic/customerId/";
    public static final String UPLOAD_BANNER_IMAGE_CONTROLLER = "mobikulhttp/index/uploadbannerpic/customerId/";
    private static final int RC_SAVE_PROFILE_PICTURE = 1;
    private static final int REQUEST_CAMERA = 2;

    public boolean mUpdateProfileImage;
    public CustomNavEndHeaderLoggedinBinding mFragmentLoginLayout;
    private Uri mFileUri;
    private Callback<UploadPicResponseData> mUploadProfilePicCallback = new Callback<UploadPicResponseData>() {
        @Override
        public void onResponse(Call<UploadPicResponseData> call, Response<UploadPicResponseData> response) {
            dismiss(getContext());
            if (isAdded()) {
                try {
                    showToast(getContext(), getString(R.string.refreshing_profile_image), Toast.LENGTH_SHORT, 0);
                    SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(getContext(), CUSTOMER_PREF);
                    customerDataSharedPref.putString(KEY_CUSTOMER_PROFILE_IMAGE, response.body().getUrl());
                    customerDataSharedPref.apply();
                    updateProfilePic();
                } catch (Exception e) {
                    showToast(getContext(), getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
                }
                mFileUri = null;
            }
        }

        @Override
        public void onFailure(Call<UploadPicResponseData> call, Throwable t) {
            dismiss(getContext());
            mFileUri = null;
            if (isAdded())
                showToast(getContext(), getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
        }
    };
    private Callback<UploadPicResponseData> mUploadBannerCallback = new Callback<UploadPicResponseData>() {
        @Override
        public void onResponse(Call<UploadPicResponseData> call, Response<UploadPicResponseData> response) {
            dismiss(getContext());
            if (isAdded()) {
                try {
                    showToast(getContext(), getString(R.string.refreshing_banner_image), Toast.LENGTH_SHORT, 0);
                    SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(getContext(), CUSTOMER_PREF);
                    customerDataSharedPref.putString(KEY_CUSTOMER_BANNER_IMAGE, response.body().getUrl());
                    customerDataSharedPref.apply();
                    updateBannerImage();
                } catch (Exception e) {
                    showToast(getContext(), getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
                }
                mFileUri = null;
            }
        }

        @Override
        public void onFailure(Call<UploadPicResponseData> call, Throwable t) {
            dismiss(getContext());
            mFileUri = null;
            if (isAdded())
                showToast(getContext(), getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentLoginLayout = DataBindingUtil.inflate(inflater, R.layout.custom_nav_end_header_loggedin, container, false);
        return mFragmentLoginLayout.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavDrawerEndData mNavDrawerEndData = new NavDrawerEndData();
        mNavDrawerEndData.setCustomerName(AppSharedPref.getCustomerName(getContext()));
        mNavDrawerEndData.setCustomerEmail(AppSharedPref.getCustomerEmail(getContext()));

        updateProfilePic();
        updateBannerImage();

        mFragmentLoginLayout.editProfilePic.setOnClickListener(this);
        mFragmentLoginLayout.editBannerImage.setOnClickListener(this);

        mFragmentLoginLayout.setData(mNavDrawerEndData);
    }

    public void updateProfilePic() {
        try {
            ImageHelper.load(mFragmentLoginLayout.navHeaderProfileImage
                    , AppSharedPref.getCustomerProfileImage(getContext(), "").isEmpty() ? null : AppSharedPref.getCustomerProfileImage(getContext(), null)
                    , getResources().getDrawable(R.drawable.customer_profile_placeholder));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateBannerImage() {
        try {
            if (!AppSharedPref.getCustomerBannerImage(getContext(), "").isEmpty()) {
                ImageHelper.load(mFragmentLoginLayout.navHeaderBannerImage
                        , AppSharedPref.getCustomerBannerImage(getContext(), null)
                        , null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.edit_profile_pic) {
            mUpdateProfileImage = true;
            selectImage();
        } else if (v.getId() == R.id.edit_banner_image) {
            mUpdateProfileImage = false;
            selectImage();
        }
    }

    public void selectImage() {
        final CharSequence[] items = {getString(R.string.choose_from_library), getString(R.string.camera_pick), getString(R.string.no)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        if (mUpdateProfileImage) {
            builder.setTitle(getString(R.string.add_profile_image));
        } else {
            builder.setTitle(getString(R.string.add_banner_image));
        }
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.choose_from_library))) {
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_from_library)), RC_SAVE_PROFILE_PICTURE);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                            requestPermissions(permissions, RC_SAVE_PROFILE_PICTURE);
                        }
                    }
                } else if (items[item].equals(getString(R.string.camera_pick))) {
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
                        startActivityForResult(intent, REQUEST_CAMERA);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                            requestPermissions(permissions, REQUEST_CAMERA);
                        }
                    }
                } else if (items[item].equals(getString(R.string.no))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Uri getImageUri() {
        // Store image in dcim
        File imagesFolder = new File(Environment.getExternalStorageDirectory() + "/DCIM");
        if (imagesFolder.isDirectory() || imagesFolder.mkdir()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mFileUri = FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".my.package.name.provider", new File(imagesFolder, "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            } else {
                mFileUri = Uri.fromFile(new File(imagesFolder, "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            }
        }
        return mFileUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_SAVE_PROFILE_PICTURE || requestCode == REQUEST_CAMERA) {
                CropImage.activity(data == null ? mFileUri : data.getData())
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(mUpdateProfileImage ? 1 : 3, mUpdateProfileImage ? 1 : 2)
                        .start(getContext(), this);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                uploadPic(CropImage.getActivityResult(data).getUri());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == REQUEST_CAMERA) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
                startActivityForResult(intent, REQUEST_CAMERA);
            } else if (requestCode == RC_SAVE_PROFILE_PICTURE) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_from_library)), RC_SAVE_PROFILE_PICTURE);
            }
        }
    }

    public void uploadPic(Uri data) {
        try {
            showDefaultAlertDialog(getContext());

            Bitmap bitmap = BitmapFactory.decodeFile(data.getPath());

            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), bos.toByteArray());
            RequestBody width = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Utils.getScreenWidth()));
            RequestBody mFactor = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getResources().getDisplayMetrics().density));

            if (mUpdateProfileImage) {
                Call<UploadPicResponseData> call = RetrofitClient.getClient().create(ApiInterface.class).uploadCustomerImagePic(
                        ApplicationConstant.BASE_URL + UPLOAD_PROFILE_PIC_CONTROLLER + AppSharedPref.getCustomerId(getContext())

                        , fileBody
                        , width
                        , mFactor);
                call.enqueue(mUploadProfilePicCallback);
            } else {
                Call<UploadPicResponseData> call = RetrofitClient.getClient().create(ApiInterface.class).uploadCustomerImagePic(
                        ApplicationConstant.BASE_URL + UPLOAD_BANNER_IMAGE_CONTROLLER + AppSharedPref.getCustomerId(getContext())

                        , fileBody
                        , width
                        , mFactor);
                call.enqueue(mUploadBannerCallback);
            }
        } catch (Exception e) {
            dismiss(getContext());
            showToast(getContext(), getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
            e.printStackTrace();
        }
    }
}