package com.webkul.mobikul.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.CustomNavEndHeaderLoggedoutBinding;
import com.webkul.mobikul.handler.NavDrawerEndHandler;

/**
 * Created by vedesh.kumar on 17/1/17. @Webkul Software Private limited
 */

public class LoggedoutLayoutFragment extends Fragment {

    private CustomNavEndHeaderLoggedoutBinding mFragmentLogoutLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mFragmentLogoutLayout = DataBindingUtil.inflate(inflater, R.layout.custom_nav_end_header_loggedout, container, false);
        mFragmentLogoutLayout.setHandler(new NavDrawerEndHandler(LoggedoutLayoutFragment.this));
        return mFragmentLogoutLayout.getRoot();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        new NavDrawerEndHandler(LoggedoutLayoutFragment.this).onActivityResult(requestCode, resultCode, data);
    }
}