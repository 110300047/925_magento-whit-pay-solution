package com.webkul.mobikul.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.OrderReviewProductAdapter;
import com.webkul.mobikul.databinding.FragmentOrderReviewBinding;
import com.webkul.mobikul.handler.OrderReviewFragmentHandler;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.model.checkout.OrderReviewRequestData;

import static android.app.Activity.RESULT_OK;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_PAYMENT;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ORDER_REVIEW_REQUEST_DATA;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECTED_PAYMENT_METHOD_CODE;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class OrderReviewFragment extends Fragment {

    private FragmentOrderReviewBinding mBinding;
    public String mSelectedPaymentMethodCode;
    public OrderReviewRequestData mOrderReviewRequestData;

    public static OrderReviewFragment newInstance(String selectedPaymentMethodCode, OrderReviewRequestData orderReviewRequestData) {
        OrderReviewFragment OrderReviewFragment = new OrderReviewFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_SELECTED_PAYMENT_METHOD_CODE, selectedPaymentMethodCode);
        args.putParcelable(BUNDLE_KEY_ORDER_REVIEW_REQUEST_DATA, orderReviewRequestData);
        OrderReviewFragment.setArguments(args);
        return OrderReviewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_review, container, false);
            return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSelectedPaymentMethodCode = getArguments().getString(BUNDLE_KEY_SELECTED_PAYMENT_METHOD_CODE);
        mOrderReviewRequestData = getArguments().getParcelable(BUNDLE_KEY_ORDER_REVIEW_REQUEST_DATA);
        if (mOrderReviewRequestData != null) {
            mBinding.setData(mOrderReviewRequestData);
            mBinding.setHandler(new OrderReviewFragmentHandler(OrderReviewFragment.this));
            mBinding.executePendingBindings();

            mBinding.productRv.setLayoutManager(new LinearLayoutManager(getContext()));
            mBinding.productRv.setAdapter(new OrderReviewProductAdapter(getContext(), mOrderReviewRequestData.getOrderReviewData().getItems()));
            mBinding.productRv.setNestedScrollingEnabled(false);
        }

        mBinding.container.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, final int scrollY, int oldScrollX, int oldScrollY) {

                if ((scrollY - oldScrollY) < 0 || (scrollY > (mBinding.container.getChildAt(0).getHeight() - mBinding.container.getHeight() - 100))) {
                    mBinding.footerContainer.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
                } else {
                    mBinding.footerContainer.animate().alpha(0f).translationY(mBinding.footerContainer.getHeight()).setInterpolator(new AccelerateInterpolator(1.4f));
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_PAYMENT) {
                mBinding.getHandler().onPaymentResponse();
            }
        } else {
            ToastHelper.showToast(getContext(), getString(R.string.payment_canceled), Toast.LENGTH_LONG, 0);
            getActivity().finish();
        }
    }
}