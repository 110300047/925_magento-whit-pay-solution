package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.FragmentPaymentMethodBinding;
import com.webkul.mobikul.handler.PaymentMethodFragmentHandler;
import com.webkul.mobikul.model.checkout.PaymentMethod;
import com.webkul.mobikul.model.checkout.ShippingMethodInfoData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SHIPPING_METHOD_INFO_DATA;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class PaymentMethodFragment extends Fragment {

    public FragmentPaymentMethodBinding mBinding;
    public ShippingMethodInfoData mShippingPaymentMethodInfoData;
    private String mSelectedPaymentMethodCode = "";

    public static PaymentMethodFragment newInstance(ShippingMethodInfoData shippingMethodInfoData) {
        PaymentMethodFragment PaymentMethodFragment = new PaymentMethodFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_KEY_SHIPPING_METHOD_INFO_DATA, shippingMethodInfoData);
        PaymentMethodFragment.setArguments(args);
        return PaymentMethodFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Abre el layout de seleccionar metodo de pago
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment_method, container, false);
        mBinding.setIsPaymentMethodAvailable(true);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mShippingPaymentMethodInfoData = getArguments().getParcelable(BUNDLE_KEY_SHIPPING_METHOD_INFO_DATA);
        ArrayList<PaymentMethod> paymentMethods = mShippingPaymentMethodInfoData.getPaymentMethods();
        mBinding.setHandler(new PaymentMethodFragmentHandler(PaymentMethodFragment.this));
        if (paymentMethods != null) {
            if (paymentMethods.size() != 0)
                updatePaymentMethodRg(paymentMethods);
            else
                mBinding.setIsPaymentMethodAvailable(false);
        }

        mBinding.scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, final int scrollY, int oldScrollX, int oldScrollY) {
                if ((scrollY - oldScrollY) < 0 || (scrollY > (mBinding.scrollView.getChildAt(0).getHeight() - mBinding.scrollView.getHeight() - 100))) {
                    mBinding.continueBtn.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
                } else {
                    mBinding.continueBtn.animate().alpha(0f).translationY(mBinding.continueBtn.getHeight()).setInterpolator(new AccelerateInterpolator(1.4f));
                }
            }
        });
    }

    private void updatePaymentMethodRg(ArrayList<PaymentMethod> paymentMethods) {
        for (int paymentMethodPosition = 0; paymentMethodPosition < paymentMethods.size(); paymentMethodPosition++) {
            if (Arrays.asList(ApplicationConstant.AVAILABLE_PAYMENT_METHOD).contains(paymentMethods.get(paymentMethodPosition).getCode())) {
                RadioButton eachPaymentMethodRb = new RadioButton(getContext());
                eachPaymentMethodRb.setText("PayPal");
                //eachPaymentMethodRb.setText(paymentMethods.get(paymentMethodPosition).getTitle());
                eachPaymentMethodRb.setTextSize(14);
                eachPaymentMethodRb.setTextColor(getResources().getColor(R.color.text_color_primary));
                eachPaymentMethodRb.setTag(paymentMethods.get(paymentMethodPosition).getCode());
                if (paymentMethods.size() == 1) {
                    eachPaymentMethodRb.setChecked(true);
                    mSelectedPaymentMethodCode = paymentMethods.get(paymentMethodPosition).getCode();
                }
                mBinding.paymentMethodRg.addView(eachPaymentMethodRb);
            }
        }

        mBinding.paymentMethodRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                View selectedRb = group.findViewById(checkedId);
                mSelectedPaymentMethodCode = selectedRb.getTag().toString();
            }
        });
    }

    public String getSelectedPaymentMethodCode() {
        return mSelectedPaymentMethodCode;
    }
}