package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.ProductCreateReviewRatingDataRvAdapter;
import com.webkul.mobikul.databinding.FragmentProductCreateReviewBinding;
import com.webkul.mobikul.handler.ProductCreateReviewFragmentHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.product.CreateReviewData;
import com.webkul.mobikul.model.product.RatingFormData;

import java.util.ArrayList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_RATING_FORM_DATA;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_ID;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class ProductCreateReviewFragment extends Fragment {

    ArrayList<RatingFormData> mRatingFormData;
    private FragmentProductCreateReviewBinding mBinding;
    private int mProductId;

    public static Fragment newInstance(ArrayList<RatingFormData> ratingFormData, int id) {
        ProductCreateReviewFragment productCreateReviewFragment = new ProductCreateReviewFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(BUNDLE_KEY_RATING_FORM_DATA, ratingFormData);
        args.putInt(KEY_PRODUCT_ID, id);
        productCreateReviewFragment.setArguments(args);
        return productCreateReviewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_create_review, container, false);

        mProductId = getArguments().getInt(KEY_PRODUCT_ID, 0);
        mRatingFormData = getArguments().getParcelableArrayList(BUNDLE_KEY_RATING_FORM_DATA);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(getString(R.string.write_your_review));
        mBinding.ratingDataRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.ratingDataRv.setAdapter(new ProductCreateReviewRatingDataRvAdapter(getContext(), mRatingFormData));
        CreateReviewData createReviewData = new CreateReviewData();
        createReviewData.setNickName(AppSharedPref.getCustomerName(getContext()));
        mBinding.setData(createReviewData);
        mBinding.setProductId(mProductId);
        mBinding.setHandler(new ProductCreateReviewFragmentHandler());
    }

    public FragmentProductCreateReviewBinding getBinding() {
        return mBinding;
    }
}
