package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.InviteeRvAdapter;
import com.webkul.mobikul.databinding.FragmentProductEmailBinding;
import com.webkul.mobikul.handler.ProductEmailHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.product.InviteeItem;
import com.webkul.mobikul.model.product.ProductEmailData;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;

/**
 * Created by vedesh.kumar on 3/1/17. @Webkul Software Pvt. Ltd
 */

public class ProductEmailFragment extends Fragment {

    public FragmentProductEmailBinding mBinding;

    public int mProductId = 0;
    public ProductEmailData productEmailData;
    public InviteeRvAdapter mInviteeRvAdapter;

    public static ProductEmailFragment newInstance(int description) {
        ProductEmailFragment productDescriptionFragment = new ProductEmailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_KEY_PRODUCT_ID, description);
        productDescriptionFragment.setArguments(bundle);
        return productDescriptionFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_email, container, false);
        mProductId = getArguments().getInt(BUNDLE_KEY_PRODUCT_ID);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        productEmailData = new ProductEmailData();
        productEmailData.setName(AppSharedPref.getCustomerName(getContext()));
        productEmailData.setEmail(AppSharedPref.getCustomerEmail(getContext()));
        productEmailData.getInviteeItemsList().add(new InviteeItem());

        mInviteeRvAdapter = new InviteeRvAdapter(ProductEmailFragment.this, productEmailData.getInviteeItemsList());
        mBinding.inviteeRv.setAdapter(mInviteeRvAdapter);

        mBinding.setData(productEmailData);
        mBinding.setHandler(new ProductEmailHandler(ProductEmailFragment.this));
    }
}