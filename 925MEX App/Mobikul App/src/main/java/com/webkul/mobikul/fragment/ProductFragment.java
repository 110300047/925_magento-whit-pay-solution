package com.webkul.mobikul.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.adapter.ProductAdditionInfoRvAdapter;
import com.webkul.mobikul.adapter.ProductAttributesSwatchRvAdapter;
import com.webkul.mobikul.adapter.ProductReviewRatingDataRecyclerViewAdapter;
import com.webkul.mobikul.adapter.ProductSliderAdapter;
import com.webkul.mobikul.adapter.RelatedProductsRvAdapter;
import com.webkul.mobikul.adapter.TierPriceRvAdapter;
import com.webkul.mobikul.adapter.UpsellProductsRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.FragmentProductBinding;
import com.webkul.mobikul.handler.ProductActivityHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ImageHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.catalog.CatalogSingleProductData;
import com.webkul.mobikul.model.catalog.SwatchData;
import com.webkul.mobikul.model.product.ImageGalleryData;
import com.webkul.mobikul.model.product.ProductCustomOption;
import com.webkul.mobikul.model.product.ProductCustomOptionValues;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.connection.ApiInterface.MOBIKUL_CATALOG_PRODUCT_PAGE_DATA;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_EDIT_PRODUCT;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ITEM_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_IMAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_NAME;
import static com.webkul.mobikul.helper.DownloadHelper.downloadFile;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 17/1/17. @Webkul Software Private limited
 */

public class ProductFragment extends Fragment {

    /*Request codes*/
    private static final int REQUEST_GET_SINGLE_FILE = 0;
    private static final int RC_ACCESS_WRITE_STORAGE = 1001;
    public FragmentProductBinding mFragmentProductBinding;
    public CatalogSingleProductData mCatalogSingleProductData;
    public Gson gson;
    public RelatedProductsRvAdapter mRelatedProductsRvAdapter;
    public UpsellProductsRvAdapter mUpsellProductsRvAdapter;
    public int mItemId = 0;
    public boolean mIsEditProduct = false;
    private int mProductId;
    private Calendar auxDate1;
    private Calendar[] myCalendar = {null};
    private Calendar[] forSlot = {null};
    private Calendar intSlot;
    private Spinner spinnerSlot;
    private ProductCustomOption customOption;
    private String mPreventSchedulingBefore;
    private int mSeletedCustomOption = 0;
    private Uri mFileUri;
    private Uri uriData;
    private  ArrayAdapter<String> spinnerArrayAdapter;
    private List<List<String[]>> lista;

    private ProductSliderAdapter mProductSliderAdapter;
    private AdapterView.OnItemSelectedListener configurableAttributeOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View selectedItemView, int selectedItemPosition, long id) {
            try {
                int currentAttributePosition = (int) parent.getTag();
                if ((currentAttributePosition + 1) < mCatalogSingleProductData.getConfigurableData().getAttributes().size()) {
                    initializeConfigurableAttributeOption(currentAttributePosition + 1);
                }
                updatePrice();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    public static ProductFragment newInstance(int productId, boolean isEditProduct, int mItemId, String productName, String productImage) {
        ProductFragment productFragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putInt(BUNDLE_KEY_PRODUCT_ID, productId);
        args.putString(BUNDLE_KEY_PRODUCT_NAME, productName);
        args.putString(BUNDLE_KEY_PRODUCT_IMAGE, productImage);
        args.putBoolean(BUNDLE_KEY_EDIT_PRODUCT, isEditProduct);
        args.putInt(BUNDLE_KEY_ITEM_ID, mItemId);
        productFragment.setArguments(args);
        return productFragment;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFragmentProductBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product, container, false);
        return mFragmentProductBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mFragmentProductBinding.setIsLoading(true);
        gson = new Gson();

        mProductId = getArguments().getInt(BUNDLE_KEY_PRODUCT_ID, 0);
        mIsEditProduct = getArguments().getBoolean(BUNDLE_KEY_EDIT_PRODUCT, false);

        // Initializing Basic Data
        mFragmentProductBinding.setProductName(getArguments().getString(BUNDLE_KEY_PRODUCT_NAME, ""));
        ArrayList<ImageGalleryData> productImage = new ArrayList<>();
        ImageGalleryData imageGalleryData = new ImageGalleryData();
        imageGalleryData.setSmallImage(getArguments().getString(BUNDLE_KEY_PRODUCT_IMAGE, ""));
        imageGalleryData.setLargeImage(getArguments().getString(BUNDLE_KEY_PRODUCT_IMAGE, ""));
        productImage.add(imageGalleryData);
        mProductSliderAdapter = new ProductSliderAdapter(getContext(), getArguments().getString(BUNDLE_KEY_PRODUCT_NAME, ""), productImage);
        mFragmentProductBinding.productSliderViewPager.setAdapter(mProductSliderAdapter);
        mFragmentProductBinding.productSliderDotsTabLayout.setupWithViewPager(mFragmentProductBinding.productSliderViewPager, true);

        if (mIsEditProduct) {
            mItemId = getArguments().getInt(BUNDLE_KEY_ITEM_ID, 0);
            mFragmentProductBinding.addToCartBtnLayout.setVisibility(View.GONE);
            mFragmentProductBinding.staticAddToCartBtnLayout.setVisibility(View.GONE);
            mFragmentProductBinding.buyNowBtn.setText(getString(R.string.update_cart));
            mFragmentProductBinding.staticBuyNowBtn.setText(getString(R.string.update_cart));
        }
        if (!AppSharedPref.isLoggedIn(getContext())) {
            //mItemId = getArguments().getInt(BUNDLE_KEY_ITEM_ID, 0);
            mFragmentProductBinding.addToCartBtnLayout.setVisibility(View.GONE);
            mFragmentProductBinding.staticAddToCartBtnLayout.setVisibility(View.GONE);
            mFragmentProductBinding.buyNowBtn.setText(getString(R.string.login_for_book_buy));
            mFragmentProductBinding.staticBuyNowBtn.setText(getString(R.string.login_for_book_buy));
        }

        ApiConnection.getCatalogSingleProductData(AppSharedPref.getStoreId(getContext())
                , mProductId
                , Utils.getScreenWidth()
                , AppSharedPref.getQuoteId(getContext())
                , AppSharedPref.getCustomerId(getContext())
                , AppSharedPref.getCurrencyCode(getContext()))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogSingleProductData>(getContext()) {
            @Override
            public void onNext(CatalogSingleProductData catalogSingleProductData) {
                super.onNext(catalogSingleProductData);
                mCatalogSingleProductData = catalogSingleProductData;

                String responseJSON = gson.toJson(mCatalogSingleProductData);
                if (NetworkHelper.isNetworkAvailable(getContext())) {
                    if (getContext() != null)
                        ((NewProductActivity) getContext()).mOfflineDataBaseHandler.updateIntoOfflineDB(MOBIKUL_CATALOG_PRODUCT_PAGE_DATA, responseJSON, String.valueOf(mProductId));
                }
                onResponse();
            }

            @Override
            public void onError(Throwable t) {
                if (getContext() != null) {
                    Cursor databaseCursor = ((NewProductActivity) getContext()).mOfflineDataBaseHandler.selectFromOfflineDB(MOBIKUL_CATALOG_PRODUCT_PAGE_DATA, String.valueOf(mProductId));
                    if (databaseCursor != null && databaseCursor.getCount() != 0) {
                        databaseCursor.moveToFirst();
                        mCatalogSingleProductData = ((NewProductActivity) getContext()).gson.fromJson(databaseCursor.getString(0), CatalogSingleProductData.class);
                        onResponse();
                    } else {
                        super.onError(t);
                    }
                }
            }
        });
    }

    private void onResponse() {
        mFragmentProductBinding.setData(mCatalogSingleProductData);
        mFragmentProductBinding.setHandler(new ProductActivityHandler(ProductFragment.this));
        mFragmentProductBinding.executePendingBindings();

        /*Loading Banner Data*/
        if (mCatalogSingleProductData.getImageGallery().size() > 1) {
            mFragmentProductBinding.productTabsLayout.setVisibility(View.VISIBLE);
        }
        mProductSliderAdapter = new ProductSliderAdapter(getContext(), mCatalogSingleProductData.getName(), mCatalogSingleProductData.getImageGallery());
        mFragmentProductBinding.productSliderViewPager.setAdapter(mProductSliderAdapter);
        mFragmentProductBinding.productSliderDotsTabLayout.setupWithViewPager(mFragmentProductBinding.productSliderViewPager, true);

        /*Loading Seller Related Data*/
        if (mCatalogSingleProductData.getSellerRating() != null && mCatalogSingleProductData.getSellerRating().size() > 0) {
            mFragmentProductBinding.sellerRatingRv.setLayoutManager(new LinearLayoutManager(getContext()));
            mFragmentProductBinding.sellerRatingRv.setAdapter(new ProductReviewRatingDataRecyclerViewAdapter(getContext(), mCatalogSingleProductData.getSellerRating()));
            mFragmentProductBinding.sellerRatingRv.setNestedScrollingEnabled(false);
        }

        /*Loading Tier Price Data*/
        mFragmentProductBinding.tierPriceRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mFragmentProductBinding.tierPriceRv.setAdapter(new TierPriceRvAdapter(getContext(), mCatalogSingleProductData.getTierPrices()));

        /*Loading Product Data*/
        loadProductOptions();

        /*Loading wishlist animator*/
        if (mCatalogSingleProductData.getIsInWishlist())
            mFragmentProductBinding.addToWishlistIv.setProgress(1);

        /*Loading Addition Info Data*/
        mFragmentProductBinding.additionalInfoRv.setAdapter(new ProductAdditionInfoRvAdapter(getContext(), mCatalogSingleProductData.getAdditionalInformation()));

        /*Loading Related Products Data*/
        mRelatedProductsRvAdapter = new RelatedProductsRvAdapter(ProductFragment.this, mCatalogSingleProductData.getRelatedProductListData());
        mFragmentProductBinding.relatedProductsRv.setAdapter(mRelatedProductsRvAdapter);

        /*Loading Upsell Products Data*/
        mUpsellProductsRvAdapter = new UpsellProductsRvAdapter(ProductFragment.this, mCatalogSingleProductData.getUpsellProductListData());
        mFragmentProductBinding.upsellProductsRv.setAdapter(mUpsellProductsRvAdapter);

        mFragmentProductBinding.setIsLoading(false);

        mFragmentProductBinding.scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView view, int scrollX, final int scrollY, int oldScrollX, int oldScrollY) {
                Rect rect = new Rect();
                if (!mCatalogSingleProductData.getIsAvailable() || (mFragmentProductBinding.staticFooterContainer.getGlobalVisibleRect(rect) && mFragmentProductBinding.staticFooterContainer.getHeight() == rect.height() &&
                        mFragmentProductBinding.staticFooterContainer.getWidth() == rect.width()) || scrollY > mFragmentProductBinding.staticFooterContainer.getY()) {
                    mFragmentProductBinding.floatingFooterContainer.setVisibility(View.GONE);
                } else {
                    mFragmentProductBinding.floatingFooterContainer.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void loadProductOptions() {

        if (mCatalogSingleProductData.getCustomOptions().size() != 0 && mCatalogSingleProductData.getIsAvailable()) {
            loadCustomOption();
        }

        switch (mCatalogSingleProductData.getTypeId()) {
            case "bundle":
                loadbundledProductData();
                break;
            case "grouped":
                loadGroupedProductData();
                break;
            case "downloadable":
                loadDownloadableProductData();
                break;
            case "hotelbooking":
            case "configurable":
                loadConfigurableProductData();
                break;
        }
    }

    private void loadCustomOption() {
        try {
            //noOfCustomOpt checa cuantos customs options tiene el producto en cuestion, en este caso siempre hay 4, Booking from, booking till, adults y kids
            //esto no quiere decir que necesariamento todos los prodcutos tengan estas 4 customOptions
            for (int noOfCustomOpt = 0; noOfCustomOpt < mCatalogSingleProductData.getCustomOptions().size(); noOfCustomOpt++) {

                /*
                * A continuacion un ejemplo de como obtener los datos de los slots (Revisar JSON que se recibe)
                * a traves de un JSON, directamente se puede acceder a la duracion del slot y al tiemṕo preventivo,
                * es decir, el tiempo que con antelacion que se puede hacer una reserva.
                *
                * IMPORTANTE: Los valores de slots recibidos son de tipo String, no son de tipo int
                * */
                //Log.d(ApplicationConstant.TAG,"oli1, este es noOfCustom: "+noOfCustomOpt);
                //Log.d(ApplicationConstant.TAG,"name: "+mCatalogSingleProductData.getName());
                //Log.d(ApplicationConstant.TAG,"option: "+mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle());
                //Log.d(ApplicationConstant.TAG,"slot duration: "+mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getSlotDuration());
                customOption = mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt);
                //A traves de validaciones se pueden acceder a las listas que contien la fecha desde donde se puede hacer
                //las reservas, esto aplica para los 7 dias de la semana ( el tmaño de slotInfo =7 )
                /*if(customOption.getBookingSlotInfo().size()>0) {
                    List<List<String[]>> lista=customOption.getBookingSlotInfo();
                    for (int i=0; i<lista.size();i++) {
                        Log.d(ApplicationConstant.TAG,"Dia: "+i);
                        if (lista.get(i).size()<0){
                            Log.d(ApplicationConstant.TAG,"Slot?? vacio");
                        }else {
                            for (String[] horas:lista.get(i))
                            Log.d(ApplicationConstant.TAG,"Horas: " + Arrays.toString(horas));
                        }
                    }
                }*/
                //este ejemplo es de caracter ilustratuvo, para mostrar como acceder a toda la info
                //sientase libre de utilizar los datos que mas le convengan
                //Por ultimo, aqui termina ejemplo
                setCustomOptionName(noOfCustomOpt);
                //mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).setPreventSchedulingBefore();
                switch (customOption.getType()) {
                    case "field":
                        //Aqu i selecciona los 2 Booking ps busca Booking
                        String titleFieldCustomOption=mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle();
                        //TODO: Hacer una funciona para Booking Till pasando como parametro la fecha de Booking from y comparar
                        if (titleFieldCustomOption.contains("Booking From")
                                || titleFieldCustomOption.contains("Rent From")){
                            //Date
                            auxDate1 = loadCustomOptionDateFrom(noOfCustomOpt);
                            break;
                        }else if (titleFieldCustomOption.contains("Booking Till")
                                || titleFieldCustomOption.contains("Rent To")){
                            //Date
                            loadCustomOptionDateTo(noOfCustomOpt,auxDate1);
                            break;
                        }else if(titleFieldCustomOption.contains("Adults")
                                || titleFieldCustomOption.contains("Kids")
                                || titleFieldCustomOption.contains("Charged Per")){
                            //numeric
                            loadCustomOptionNumericType(noOfCustomOpt);
                            break;
                        }else if (titleFieldCustomOption.contains("Booking Date")){
                            //Date
                            intSlot = loadCustomOptionDateSlot(noOfCustomOpt);

                            break;
                        }else if(titleFieldCustomOption.contains("Booking Slot")){
                            //time or spinner
                            mPreventSchedulingBefore=customOption.getPreventSchedulingBefore();
                            lista=customOption.getBookingSlotInfo();
                            loadCustomOptionTimeSlot(noOfCustomOpt, intSlot);
                            break;
                        }
                    case "area":
                        loadCustomOptionFieldAreaType(noOfCustomOpt);
                        break;

                    case "file":
                        loadCustomOptionFileType(noOfCustomOpt);
                        break;

                    case "drop_down":
                        loadCustomOptionDropDownType(noOfCustomOpt);
                        break;

                    case "radio":
                        loadCustomOptionRadioType(noOfCustomOpt);
                        break;

                    case "checkbox":
                    case "multiple":
                        loadCustomOptionCheckBoxType(noOfCustomOpt);
                        break;

                    case "date":
                        loadCustomOptionDateType(noOfCustomOpt);
                        break;

                    case "time":
                        loadCustomOptionTimeType(noOfCustomOpt);
                        break;

                    case "date_time":
                        loadCustomOptionDateTimeType(noOfCustomOpt);
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadbundledProductData() {
        try {
            if (mCatalogSingleProductData.getIsAvailable()) {
                if (mCatalogSingleProductData.getBundleOptions().size() != 0) {
                    for (int noOfBundleOpt = 0; noOfBundleOpt < mCatalogSingleProductData.getBundleOptions().size(); noOfBundleOpt++) {

                        TextView eachBundleItemNameTV = new TextView(getContext());
                        eachBundleItemNameTV.setTextSize(16);
                        eachBundleItemNameTV.setPadding(0, 15, 0, 0);

                        eachBundleItemNameTV.setText(mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getTitle());
                        if (mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getRequired() == 1) {
                            Spannable required_sign = new SpannableString("*");
                            required_sign.setSpan(new ForegroundColorSpan(Color.RED), 0, required_sign.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            eachBundleItemNameTV.setText(String.valueOf(eachBundleItemNameTV.getText()));
                            eachBundleItemNameTV.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
                            eachBundleItemNameTV.append(required_sign);
                        }
                        mFragmentProductBinding.otherProductOptionsContainer.addView(eachBundleItemNameTV);


                        switch (mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getType()) {
                            case "select":
                                loadDropDownBundleOption(noOfBundleOpt);
                                break;
                            case "radio":
                                loadRadioBundleOption(noOfBundleOpt);
                                break;
                            case "checkbox":
                            case "multi":
                                loadCheckAndMultiBundleOption(noOfBundleOpt);
                                break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void loadGroupedProductData() {
        try {
            if (mCatalogSingleProductData.getIsAvailable()) {

                if (mCatalogSingleProductData.getGroupedData().size() != 0) {
                    mFragmentProductBinding.qtyLayout.setVisibility(View.GONE);
                    for (int noOfGroupedData = 0; noOfGroupedData < mCatalogSingleProductData.getGroupedData().size(); noOfGroupedData++) {
                        View eachGroupProductItem = getActivity().getLayoutInflater().inflate(R.layout.item_grouped_product, mFragmentProductBinding.otherProductOptionsContainer, false);
                        eachGroupProductItem.setTag(noOfGroupedData);


                        if (!mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).isIsAvailable()) {
                            eachGroupProductItem.findViewById(R.id.quantity_container).setVisibility(View.GONE);
                            EditText editText = eachGroupProductItem.findViewById(R.id.qty);
                            editText.setText(String.valueOf(0));
                            eachGroupProductItem.findViewById(R.id.outofstock).setVisibility(View.VISIBLE);
                        }

                        ((TextView) eachGroupProductItem.findViewById(R.id.nameOfGroupedData)).setText(mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).getName());

                        if (mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).getIsInRange()
                                && !mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).getSpecialPrice().equalsIgnoreCase("$0.00")) {

                            ((TextView) eachGroupProductItem.findViewById(R.id.priceOfGroupedProduct)).setText(mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).getSpecialPrice());
                            ((TextView) eachGroupProductItem.findViewById(R.id.specialpriceOfGroupedProduct)).setText(mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).getForamtedPrice());

                            ((TextView) eachGroupProductItem.findViewById(R.id.specialpriceOfGroupedProduct)).setPaintFlags(((TextView) eachGroupProductItem.findViewById(R.id.priceOfGroupedProduct)).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            eachGroupProductItem.findViewById(R.id.specialpriceOfGroupedProduct).setVisibility(View.VISIBLE);
                        } else {
                            ((TextView) eachGroupProductItem.findViewById(R.id.priceOfGroupedProduct)).setText(mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).getForamtedPrice());
                        }

                        ImageView incrementQty = eachGroupProductItem.findViewById(R.id.increment_qty_iv);
                        incrementQty.setTag(noOfGroupedData);
                        incrementQty.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                v.getTag();
                                EditText editText = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.qty);
                                int currentQty = Integer.parseInt(editText.getText().toString()) + 1;
                                editText.setText(String.valueOf(currentQty));
                            }
                        });
                        ImageView decrementQty = eachGroupProductItem.findViewById(R.id.decrement_qty_iv);
                        decrementQty.setTag(noOfGroupedData);
                        decrementQty.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                EditText editText = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.qty);
                                int currentQty = Integer.parseInt(editText.getText().toString()) - 1;
                                if (currentQty < 0)
                                    editText.setText("0");
                                else
                                    editText.setText(String.valueOf(currentQty));
                            }
                        });

                        ImageView eachGroupedProdcutImage = eachGroupProductItem.findViewById(R.id.grouped_product_image);
                        ImageHelper.load(eachGroupedProdcutImage, mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).getThumbNail(), null);
                        mFragmentProductBinding.otherProductOptionsContainer.addView(eachGroupProductItem);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadDownloadableProductData() {

        try {
            if (mCatalogSingleProductData.getIsAvailable()) {
                TextView linksTitle = new TextView(getContext());
                linksTitle.setText(mCatalogSingleProductData.getLinks().getTitle());
                linksTitle.setTextSize(16);

                if (mCatalogSingleProductData.getLinks().getLinksPurchasedSeparately() == 1) {
                    linksTitle.setText(String.valueOf(linksTitle.getText() + " *"));
                    linksTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
                }
                mFragmentProductBinding.otherProductOptionsContainer.addView(linksTitle);

                for (int noOflinkData = 0; noOflinkData < mCatalogSingleProductData.getLinks().getLinkData().size(); noOflinkData++) {

                    CheckBox eachLinkCheckBox = (CheckBox) getActivity().getLayoutInflater().inflate(R.layout.item_links_downloadable_product, mFragmentProductBinding.otherProductOptionsContainer, false);
                    eachLinkCheckBox.setText(mCatalogSingleProductData.getLinks().getLinkData().get(noOflinkData).getLinkTitle());
                    eachLinkCheckBox.setTextSize(14);
                    eachLinkCheckBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            updatePrice();
                        }
                    });

                    if (mCatalogSingleProductData.getLinks().getLinkData().get(noOflinkData).getPrice() != 0) {
                        eachLinkCheckBox.setText(String.valueOf(eachLinkCheckBox.getText() + "   +" + mCatalogSingleProductData.getLinks().getLinkData().get(noOflinkData).getFormatedPrice()));
                    }

                    if (mCatalogSingleProductData.getLinks().getLinksPurchasedSeparately() == 0) {
                        eachLinkCheckBox.setEnabled(false);
                        eachLinkCheckBox.setButtonDrawable(R.drawable.selector_checkbox);
                    }

                    mFragmentProductBinding.otherProductOptionsContainer.addView(eachLinkCheckBox);
                }
            }

            if (mCatalogSingleProductData.getSamples().isHasSample()) {
                TextView sampleTitle = new TextView(getContext());
                sampleTitle.setText(mCatalogSingleProductData.getSamples().getTitle());
                sampleTitle.setTextSize(18);
                mFragmentProductBinding.otherProductOptionsContainer.addView(sampleTitle);

                for (int noOflinkData = 0; noOflinkData < mCatalogSingleProductData.getSamples().getLinkSampleData().size(); noOflinkData++) {
                    TextView sampleLink = new TextView(getContext());
                    sampleLink.setPadding(15, 0, 0, 0);
                    sampleLink.setTextSize(16);
                    sampleLink.setText(mCatalogSingleProductData.getSamples().getLinkSampleData().get(noOflinkData).getSampleTitle());
                    sampleLink.setTextColor(ContextCompat.getColor(getContext(), R.color.link_text_color));
                    sampleLink.setTag(noOflinkData);
                    sampleLink.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (checkPermissions()) {
                                final int positionOfSampleLint = (Integer) v.getTag();
                                showToast(getContext(), getContext().getString(R.string.download_started), Toast.LENGTH_LONG, 0);
                                downloadFile(getContext(), mCatalogSingleProductData.getSamples().getLinkSampleData().get(positionOfSampleLint).getUrl(), mCatalogSingleProductData.getSamples().getLinkSampleData().get(positionOfSampleLint).getFileName()
                                        , mCatalogSingleProductData.getSamples().getLinkSampleData().get(positionOfSampleLint).getMimeType());
                            }
                        }
                    });
                    mFragmentProductBinding.otherProductOptionsContainer.addView(sampleLink);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadConfigurableProductData() {
        try {
            mFragmentProductBinding.otherProductOptionsContainer.setVisibility(View.VISIBLE);
            int dropDownTypeOptionCount = 0;
            for (int attributePosition = 0; attributePosition < mCatalogSingleProductData.getConfigurableData().getAttributes().size(); attributePosition++) {
                // Adding visual or text type swatch
                if (mCatalogSingleProductData.getConfigurableData().getAttributes().get(attributePosition).getSwatchType().equals("visual") || mCatalogSingleProductData.getConfigurableData().getAttributes().get(attributePosition).getSwatchType().equals("text")) {

                    View eachConfigurableAttributeLayout = getActivity().getLayoutInflater().inflate(R.layout.item_configurable_product_attribute_swatch_layout, mFragmentProductBinding.otherProductOptionsContainer, false);
                    eachConfigurableAttributeLayout.setTag("config" + attributePosition);

                    Spannable required_sign = new SpannableString("*");
                    required_sign.setSpan(new ForegroundColorSpan(Color.RED), 0, required_sign.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    ((TextView) eachConfigurableAttributeLayout.findViewById(R.id.configurable_attribute_label)).setText(String.valueOf(mCatalogSingleProductData.getConfigurableData().getAttributes()
                            .get(attributePosition).getLabel()));
                    ((TextView) eachConfigurableAttributeLayout.findViewById(R.id.configurable_attribute_label)).append(required_sign);

                    RecyclerView eachSwatchRecyclerView = eachConfigurableAttributeLayout.findViewById(R.id.configurable_item_rv);
                    eachSwatchRecyclerView.setTag("config" + attributePosition);
                    ArrayList<SwatchData> attributeSwatchData = new ArrayList<>();
                    try {
                        JSONObject mainSwatchDataObject = new JSONObject(mCatalogSingleProductData.getConfigurableData().getSwatchData());
                        JSONObject eachOptionData = mainSwatchDataObject.getJSONObject(mCatalogSingleProductData.getConfigurableData().getAttributes().get(attributePosition).getId());
                        Iterator<?> keys = eachOptionData.keys();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (eachOptionData.get(key) instanceof JSONObject) {
                                JSONObject eachSwatchData = new JSONObject(eachOptionData.get(key).toString());
                                SwatchData swatchData = new SwatchData();
                                swatchData.setId(key);
                                swatchData.setType(eachSwatchData.getString("type"));
                                swatchData.setValue(eachSwatchData.getString("value"));
                                attributeSwatchData.add(swatchData);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ProductAttributesSwatchRvAdapter productAttributeSwatchRvAdapter = new ProductAttributesSwatchRvAdapter(this, attributePosition, mCatalogSingleProductData.getConfigurableData().getAttributes().get(attributePosition).getUpdateProductPreviewImage(), attributeSwatchData);
                    eachSwatchRecyclerView.setAdapter(productAttributeSwatchRvAdapter);
                    mFragmentProductBinding.otherProductOptionsContainer.addView(eachConfigurableAttributeLayout);
                }
                // Adding Dropdown type option
                else {
                    View eachConfigurableAttributeLayout = getActivity().getLayoutInflater().inflate(R.layout.item_configurable_product_attribute_dropdown_layout, mFragmentProductBinding.otherProductOptionsContainer, false);
                    eachConfigurableAttributeLayout.setTag("config" + attributePosition);

                    Spannable required_sign = new SpannableString("*");
                    required_sign.setSpan(new ForegroundColorSpan(Color.RED), 0, required_sign.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    ((TextView) eachConfigurableAttributeLayout.findViewById(R.id.configurable_attribute_label)).setText(String.valueOf(mCatalogSingleProductData.getConfigurableData().getAttributes()
                            .get(attributePosition).getLabel()));
                    ((TextView) eachConfigurableAttributeLayout.findViewById(R.id.configurable_attribute_label)).append(required_sign);
                    Spinner eachConfigurableAttributeSpinner = eachConfigurableAttributeLayout.findViewById(R.id.spinner_configurable_item);
                    eachConfigurableAttributeSpinner.setTag(attributePosition);
                    mFragmentProductBinding.otherProductOptionsContainer.addView(eachConfigurableAttributeLayout);
                }
            }

            initializeConfigurableAttributeOption(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCustomOptionName(int positionOfCustomOption) {
        try {
            /*String*/
            Spannable newCustomOptionLabelSpannable = new SpannableString(mCatalogSingleProductData.getCustomOptions().get(positionOfCustomOption).getTitle());
            newCustomOptionLabelSpannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.dark_text_color)), 0, newCustomOptionLabelSpannable.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            TextView customOptionsName = new TextView(getContext());
            customOptionsName.setTextSize(16);
            customOptionsName.setText(newCustomOptionLabelSpannable);
            if (positionOfCustomOption == 0) {
                customOptionsName.setPadding(0, 10, 0, 10);
            } else {
                customOptionsName.setPadding(0, 30, 0, 10);
            }

            String newCustomOptionLabel = "";
            if (mCatalogSingleProductData.getCustomOptions().get(positionOfCustomOption).getUnformatedDefaultPrice() != 0d) {
                String formattedDefaultPrice = mCatalogSingleProductData.getCustomOptions().get(positionOfCustomOption).getFormatedDefaultPrice();
                if (!mCatalogSingleProductData.getCustomOptions().get(positionOfCustomOption).getPriceType().equals("fixed")) {
                    Double price = mCatalogSingleProductData.getFinalPrice();
                    Double unformatedDefaultPrice = mCatalogSingleProductData.getCustomOptions().get(positionOfCustomOption).getUnformatedDefaultPrice();
                    String pattern = mCatalogSingleProductData.getPriceFormat().getPattern();
                    int precision = mCatalogSingleProductData.getPriceFormat().getPrecision();
                    price = price * unformatedDefaultPrice / 100;
                    String precisionFormat = "%." + precision + "f";
                    String formatedPrice = String.format(precisionFormat, price);
                    formattedDefaultPrice = pattern.replaceAll("%s", formatedPrice);
                    newCustomOptionLabel += "  (+" + formattedDefaultPrice + ")";
                } else {
                    newCustomOptionLabel += "  (+" + formattedDefaultPrice + ")";
                }
            }
            Spannable customOptionNameSpannable = new SpannableString(newCustomOptionLabel);
            customOptionNameSpannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, customOptionNameSpannable.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            customOptionsName.append(customOptionNameSpannable);

            if (mCatalogSingleProductData.getCustomOptions().get(positionOfCustomOption).getIsRequire() == 1) {
                Spannable required_sign = new SpannableString("*");
                required_sign.setSpan(new ForegroundColorSpan(Color.RED), 0, required_sign.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                customOptionsName.append(required_sign);
            }

            mFragmentProductBinding.productCustomOptionsContainer.addView(customOptionsName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCustomOptionFieldAreaType(int noOfCustomOpt) {
        try {
            EditText editTextView = new EditText(getContext());
            editTextView.setSingleLine();
            editTextView.setTag(noOfCustomOpt);
            editTextView.setTextSize(14);
            editTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    updatePrice();
                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(editTextView);

            if (!mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getMaxCharacters().equals("0")) {
                TextView maxNoOfCharacters = new TextView(getContext());
                maxNoOfCharacters.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_light));
                maxNoOfCharacters.setTextSize(12);
                maxNoOfCharacters.setText(getString(R.string.maximum_number_of_characters_));
                maxNoOfCharacters.append(" " + mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getMaxCharacters());
                mFragmentProductBinding.productCustomOptionsContainer.addView(maxNoOfCharacters);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCustomOptionFileType(final int noOfCustomOpt) {
        try {
            View eachFileTypeCustomOptionView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_file_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            eachFileTypeCustomOptionView.setTag(noOfCustomOpt);
            Button browseButton = eachFileTypeCustomOptionView.findViewById(R.id.browseButton);
            browseButton.setTag(noOfCustomOpt);

            browseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSeletedCustomOption = noOfCustomOpt;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        if (ContextCompat.checkSelfPermission(getContext(),
                                android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                        } else {
                            showFileChooser();
                        }
                    } else {
                        showFileChooser();
                    }
                }
            });

            mFragmentProductBinding.productCustomOptionsContainer.addView(eachFileTypeCustomOptionView);

            String file_extension = " <b>" + mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getFileExtension() + "</b>";

            if (!mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getFileExtension().equals("null")) {
                TextView allowedFileExt = new TextView(getContext());
                allowedFileExt.setTextSize(14);
                allowedFileExt.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_light));
                allowedFileExt.setText(Html.fromHtml(getResources().getString(R.string.allowed_file_extensions_to_upload_) + file_extension));
                allowedFileExt.setPadding(0, 2, 0, 2);
                mFragmentProductBinding.productCustomOptionsContainer.addView(allowedFileExt);
            }

            if (!mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getImageSizeX().equals("0")) {
                TextView image_size_x = new TextView(getContext());
                image_size_x.setPadding(0, 2, 0, 2);
                image_size_x.setTextSize(14);
                image_size_x.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_light));
                String imageSizeX = "<b>" + mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getImageSizeX() + "</b>";
                image_size_x.setText(Html.fromHtml(getResources().getString(R.string.maximum_image_width) + imageSizeX));
                mFragmentProductBinding.productCustomOptionsContainer.addView(image_size_x);
            }

            if (!mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getImageSizeY().equals("0")) {
                TextView image_size_y = new TextView(getContext());
                image_size_y.setPadding(0, 2, 0, 2);
                image_size_y.setTextSize(14);
                image_size_y.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_light));
                String imageSizeY = "<b>" + mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getImageSizeY() + "</b>";
                image_size_y.setText(Html.fromHtml(getResources().getString(R.string.maximum_image_height) + imageSizeY));
                mFragmentProductBinding.productCustomOptionsContainer.addView(image_size_y);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        try {
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), REQUEST_GET_SINGLE_FILE);
        } catch (android.content.ActivityNotFoundException ex) {
            showToast(getContext(), getContext().getString(R.string.file_manager_error), Toast.LENGTH_SHORT, 0);
        }
    }

    private void loadCustomOptionDropDownType(int noOfCustomOpt) {
        try {
            ArrayList<String> spinnerOpions = new ArrayList<>();
            spinnerOpions.add(getResources().getString(R.string._please_select_));
            List<ProductCustomOptionValues> optionValues = mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues();

            for (int noOfOptions = 0; noOfOptions < optionValues.size(); noOfOptions++) {
                String optionString = optionValues.get(noOfOptions).getTitle();
                if (!String.valueOf(optionValues.get(noOfOptions).getDefaultPrice()).equals("0")) {
                    if (optionValues.get(noOfOptions).getDefaultPriceType().equals("fixed")) {
                        optionString += "  +" + optionValues.get(noOfOptions).getFormatedDefaultPrice();
                    } else {
                        Double price = mCatalogSingleProductData.getFinalPrice();
                        String pattern = mCatalogSingleProductData.getPriceFormat().getPattern();
                        int precision = mCatalogSingleProductData.getPriceFormat().getPrecision();
                        Double defPrice = optionValues.get(noOfOptions).getDefaultPrice();
                        price = price * defPrice / 100;
                        String precisionFormat = "%." + precision + "f";
                        String formatedPrice = String.format(precisionFormat, price);
                        String newPattern = pattern.replaceAll("%s", formatedPrice);
                        optionString += " +" + newPattern;
                    }
                }
                spinnerOpions.add(optionString);
            }

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, spinnerOpions);
            Spinner spinner = new Spinner(getContext());
            spinner.setTag(noOfCustomOpt);
            spinner.setBackground(getContext().getResources().getDrawable(R.drawable.spinner_layout));
            spinner.setAdapter(spinnerArrayAdapter);
            spinner.setSelection(0, false);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    updatePrice();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    updatePrice();
                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCustomOptionRadioType(int noOfCustomOpt) {
        try {

            List<ProductCustomOptionValues> optionValues = mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues();
            RadioGroup radioGroupCustomOption = new RadioGroup(getContext()); //create the RadioGroup
            radioGroupCustomOption.setTag(noOfCustomOpt);
            RadioButton eachOptionInRadioTypeCustomOption = new RadioButton(getContext());

            if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 0) {
                eachOptionInRadioTypeCustomOption.setText(getString(R.string.none));
                eachOptionInRadioTypeCustomOption.setTextSize(14);
                eachOptionInRadioTypeCustomOption.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_medium));
                radioGroupCustomOption.addView(eachOptionInRadioTypeCustomOption);
                eachOptionInRadioTypeCustomOption.setTag("");
                radioGroupCustomOption.check(eachOptionInRadioTypeCustomOption.getId());
            }

            for (int noOfOptions = 0; noOfOptions < optionValues.size(); noOfOptions++) {

                eachOptionInRadioTypeCustomOption = new RadioButton(getContext());
                eachOptionInRadioTypeCustomOption.setText(optionValues.get(noOfOptions).getTitle());
                eachOptionInRadioTypeCustomOption.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_medium));
                eachOptionInRadioTypeCustomOption.setTag(optionValues.get(noOfOptions).getOptionTypeId());

                Spannable optionString = new SpannableString("");
                if (optionValues.get(noOfOptions).getDefaultPrice() != 0) {
                    if (optionValues.get(noOfOptions).getDefaultPriceType().equals("fixed")) {
                        optionString = new SpannableString(" (+" + optionValues.get(noOfOptions).getFormatedDefaultPrice() + ")");
                    } else {
                        Double price = mCatalogSingleProductData.getFinalPrice();
                        String pattern = mCatalogSingleProductData.getPriceFormat().getPattern();
                        int precision = mCatalogSingleProductData.getPriceFormat().getPrecision();
                        Double defPrice = optionValues.get(noOfOptions).getDefaultPrice();
                        price = price * defPrice / 100;
                        String precisionFormat = "%." + precision + "f";
                        String formatedPrice = String.format(precisionFormat, price);
                        String newPattern = pattern.replaceAll("%s", formatedPrice);
                        optionString = new SpannableString(" (+" + newPattern + ")");
                    }
                }
                optionString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, optionString.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                eachOptionInRadioTypeCustomOption.append(optionString);
                radioGroupCustomOption.addView(eachOptionInRadioTypeCustomOption);
            }
            radioGroupCustomOption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    updatePrice();
                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(radioGroupCustomOption);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCustomOptionCheckBoxType(int noOfCustomOpt) {
        try {
            List<ProductCustomOptionValues> optionValues = mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues();
            LinearLayout checkBoxCustomOptionLayout = new LinearLayout(getContext());
            checkBoxCustomOptionLayout.setOrientation(LinearLayout.VERTICAL);
            checkBoxCustomOptionLayout.setTag(noOfCustomOpt);

            for (int noOfOptions = 0; noOfOptions < optionValues.size(); noOfOptions++) {
                CheckBox eachCheckBoxTypeOptionValue = new CheckBox(getContext());
                eachCheckBoxTypeOptionValue.setTag(optionValues.get(noOfOptions).getOptionTypeId());
                eachCheckBoxTypeOptionValue.setText(optionValues.get(noOfOptions).getTitle());
                eachCheckBoxTypeOptionValue.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_medium));

                Spannable optionString = new SpannableString("");
                if (optionValues.get(noOfOptions).getDefaultPrice() != 0) {
                    if (optionValues.get(noOfOptions).getDefaultPriceType().equals("fixed")) {
                        optionString = new SpannableString(" (+" + optionValues.get(noOfOptions).getFormatedDefaultPrice() + ")");
                    } else {
                        Double price = mCatalogSingleProductData.getFinalPrice();
                        String pattern = mCatalogSingleProductData.getPriceFormat().getPattern();
                        int precision = mCatalogSingleProductData.getPriceFormat().getPrecision();
                        Double defPrice = optionValues.get(noOfOptions).getDefaultPrice();
                        price = price * defPrice / 100;
                        String precisionFormat = "%." + precision + "f";
                        String formatedPrice = String.format(precisionFormat, price);
                        String newPattern = pattern.replaceAll("%s", formatedPrice);
                        optionString = new SpannableString(" (+" + newPattern + ")");
                    }
                }
                optionString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, optionString.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                eachCheckBoxTypeOptionValue.append(optionString);
                eachCheckBoxTypeOptionValue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updatePrice();
                    }
                });
                eachCheckBoxTypeOptionValue.setTextSize(14);
                checkBoxCustomOptionLayout.addView(eachCheckBoxTypeOptionValue);
            }
            mFragmentProductBinding.productCustomOptionsContainer.addView(checkBoxCustomOptionLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCustomOptionDateType(int noOfCustomOpt) {
        try {
            final Calendar myCalendar = Calendar.getInstance();
            View customOptionDateView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_date_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            customOptionDateView.setTag(noOfCustomOpt);

            final EditText dateEditText1 = customOptionDateView.findViewById(R.id.dateET);

            Button resetBtn = customOptionDateView.findViewById(R.id.resetBtn);
            resetBtn.setTag(noOfCustomOpt);
            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText resetDate = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.dateET);
                    resetDate.setText("");
                    updatePrice();
                }
            });

            final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "MM/dd/yy";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    dateEditText1.setText(sdf.format(myCalendar.getTime()));
                    updatePrice();
                }
            };
            dateEditText1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DatePickerDialog(getContext(), date1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(customOptionDateView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Funcion a trabajar
    private Calendar loadCustomOptionDateFrom(final int noOfCustomOpt) {
        try {
            //Log.d(ApplicationConstant.TAG, "este es el loadoption");
            myCalendar[0] = Calendar.getInstance();
            View customOptionDateView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_date_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            customOptionDateView.setTag(noOfCustomOpt);

            final EditText dateEditText1 = customOptionDateView.findViewById(R.id.dateET);

            Button resetBtn = customOptionDateView.findViewById(R.id.resetBtn);
            resetBtn.setTag(noOfCustomOpt);
            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d("Victor",v.toString());
                    EditText resetDateFirst = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.dateET);
                    resetDateFirst.setText("");
                    //Log.d("Victor",v.getTag().toString());

                    EditText resetDateSecond=mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt+1).findViewById(R.id.dateET);
                    resetDateSecond.setText("");
                    myCalendar[0] = null;
                    //loadCustomOptionDateType2(noOfCustomOpt, myCalendar[0]);
                    updatePrice();
                }
            });

            final Calendar finalMyCalendar = myCalendar[0];
            final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    finalMyCalendar.set(Calendar.YEAR, year);
                    finalMyCalendar.set(Calendar.MONTH, monthOfYear);
                    finalMyCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "MM/dd/yy";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    //myCalendar[0].setTime(finalMyCalendar.getTime());
                    dateEditText1.setText(sdf.format(finalMyCalendar.getTime()));
                    //loadCustomOptionDateType2(noOfCustomOpt, myCalendar[0]);
                    updatePrice();
                }
            };
            dateEditText1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog dp = new DatePickerDialog(getContext(), R.style.AlertDialogTheme, date1, finalMyCalendar.get(Calendar.YEAR), finalMyCalendar.get(Calendar.MONTH), finalMyCalendar.get(Calendar.DAY_OF_MONTH));
                    dp.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dp.show();
                    EditText resetDateSecond=mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt+1).findViewById(R.id.dateET);
                    resetDateSecond.setText("");
                }
            });

            mFragmentProductBinding.productCustomOptionsContainer.addView(customOptionDateView);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return myCalendar[0];
    }

    //Funcion a trabajar para leer fecha2
    private void loadCustomOptionDateTo(int noOfCustomOpt, final Calendar date2) {
        try {
            final Calendar myCalendar = Calendar.getInstance();
            View customOptionDateView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_date_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            customOptionDateView.setTag(noOfCustomOpt);

            final EditText dateEditText1 = customOptionDateView.findViewById(R.id.dateET);

            Button resetBtn = customOptionDateView.findViewById(R.id.resetBtn);
            resetBtn.setTag(noOfCustomOpt);
            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d("Victor",v.toString());
                    EditText resetDate = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.dateET);
                    resetDate.setText("");
                    updatePrice();
                }
            });

            final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "MM/dd/yy";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    dateEditText1.setText(sdf.format(myCalendar.getTime()));
                    updatePrice();
                }
            };
            dateEditText1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog dp  = new DatePickerDialog(getContext(),R.style.AlertDialogTheme, date1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                    dp.getDatePicker().setMinDate(date2.getTimeInMillis()+86400000);
                    dp.show();
                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(customOptionDateView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //funcion de tipo numerico
    private void loadCustomOptionNumericType(int noOfCustomOpt) {
        try {
            View customOptionTimeView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_integer_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            customOptionTimeView.setTag(noOfCustomOpt);
            final EditText intEditText1 = customOptionTimeView.findViewById(R.id.integer);

            intEditText1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager imm = (InputMethodManager) getContext().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(intEditText1, InputMethodManager.SHOW_IMPLICIT);
                    imm.hideSoftInputFromWindow(intEditText1.getWindowToken(), 0);
                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(customOptionTimeView);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    //TODO Get day for BookingSlot and create dynamic spinner
    private Calendar loadCustomOptionDateSlot(final int noOfCustomOpt) {
        forSlot[0] = Calendar.getInstance();
        final Calendar myCalendarNow = forSlot[0];
        try {
            View customOptionDateView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_date_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            customOptionDateView.setTag(noOfCustomOpt);

            final EditText dateEditText1 = customOptionDateView.findViewById(R.id.dateET);

            Button resetBtn = customOptionDateView.findViewById(R.id.resetBtn);
            resetBtn.setTag(noOfCustomOpt);
            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText resetDate = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.dateET);
                    resetDate.setText("");
                    forSlot[0] = null;
                    //loadCustomOptionTimeSlot(noOfCustomOpt, intSlot);
                    spinnerSlot.setSelection(0,false);
                    updatePrice();
                }
            });

            final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //Log.d(ApplicationConstant.TAG, "Este es Calendar antes: "+myCalendarNow.get(Calendar.YEAR)+"/"+myCalendarNow.get(Calendar.MONTH)+"/"+myCalendarNow.get(Calendar.DAY_OF_MONTH));
                    //Log.d(ApplicationConstant.TAG, "Estos son param: "+year+"/"+monthOfYear+"/"+dayOfMonth);

                    myCalendarNow.set(Calendar.YEAR, year);
                    myCalendarNow.set(Calendar.MONTH, monthOfYear);
                    myCalendarNow.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    Log.d(ApplicationConstant.TAG, "Este es calendar despues: "+myCalendarNow.get(Calendar.YEAR)+"/"+myCalendarNow.get(Calendar.MONTH)+"/"+myCalendarNow.get(Calendar.DAY_OF_MONTH));

                    String myFormat = "MM/dd/yy";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    dateEditText1.setText(sdf.format(myCalendarNow.getTime()));
                    try {
                        ArrayList<String> spinnerOpions = createListOfSlots(myCalendarNow);
                        if(!spinnerArrayAdapter.isEmpty()){
                            spinnerArrayAdapter.clear();
                            spinnerArrayAdapter.addAll(spinnerOpions);
                            spinnerArrayAdapter.notifyDataSetChanged();
                        }
                    }catch (Exception e){
                        Log.d("Victor",e.getMessage());
                    }
                    updatePrice();
                }
            };
            dateEditText1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog dp = new DatePickerDialog(getContext(), R.style.AlertDialogTheme, date1, myCalendarNow.get(Calendar.YEAR), myCalendarNow.get(Calendar.MONTH), myCalendarNow.get(Calendar.DAY_OF_MONTH));
                    dp.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dp.show();
                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(customOptionDateView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myCalendarNow;
    }

    private void loadCustomOptionTimeSlot(int noOfCustomOpt, final Calendar intSlot) {
        try {
            final ArrayList<String> spinnerOpions = createListOfSlots(intSlot);
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, spinnerOpions);
            spinnerSlot = new Spinner(getContext());
            spinnerSlot.setTag(noOfCustomOpt);
            spinnerSlot.setBackground(getContext().getResources().getDrawable(R.drawable.spinner_layout));
            spinnerSlot.setAdapter(spinnerArrayAdapter);
            spinnerSlot.setSelection(0, false);
            spinnerSlot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    updatePrice();
                    //Log.d(ApplicationConstant.TAG,"Seleccionaste: "+spinnerSlot.getSelectedItem());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    updatePrice();
                }
            });
            /*View customOptionDateView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_time_taxi, mFragmentProductBinding.productCustomOptionsContainer, false);
            customOptionDateView.setTag(noOfCustomOpt);

            Button resetBtn = customOptionDateView.findViewById(R.id.resetBtn);
            resetBtn.setTag(noOfCustomOpt);
            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*if(customOption.getBookingSlotInfo().size()>0) {
                        List<List<String[]>> lista=customOption.getBookingSlotInfo();
                        for (int i=0; i<lista.size();i++) {
                            //Log.d(ApplicationConstant.TAG,"Dia: "+i);
                            if (lista.get(i).size()<0){
                                //Log.d(ApplicationConstant.TAG,"Slot?? vacio");
                            }else {
                                for (String[] horas:lista.get(i)){
                                    if (intSlot.get(Calendar.DAY_OF_WEEK) == i){
                                        String aux = Arrays.toString(horas);
                                        aux = aux.replace("[","");
                                        aux = aux.replace("]","");
                                        String[] time = aux.split(", ");
                                        for (int j=0;j<time.length;j++){
                                            String curCalendar = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.ENGLISH).format(Calendar.getInstance().getTime());
                                            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.ENGLISH);
                                            int year = intSlot.get(Calendar.YEAR);
                                            int month = intSlot.get(Calendar.MONTH);
                                            Log.d("mensaje", String.valueOf(intSlot));
                                            Log.d("mensaje", String.valueOf(month));
                                            int dayMonth = intSlot.get(Calendar.DAY_OF_MONTH);
                                            String timeSlot = month + "/" + dayMonth + "/" + year+" " + time[j];
                                            try {

                                                Date date = sdf.parse(String.valueOf(curCalendar));
                                                Date timeDate = sdf.parse(String.valueOf(timeSlot));

                                                Log.d("mensaje", String.valueOf(date));
                                                Log.d("mensaje", String.valueOf(timeDate));

                                                long diff = (timeDate.getTime() - date.getTime())/(1000 * 60);
                                                Log.d("mensaje", String.valueOf(diff));

                                                if( diff > 20){
                                                    Log.d("mensaje", time[j]);
                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                Log.d("mensaje","error: "+e);
                                            }
                                        }
                                    /*for (int j=1; j < aux.length(); j++){

                                    }
                                    }

                                }
                                    //Log.d(ApplicationConstant.TAG,"Horas: " + Arrays.toString(horas));
                            }
                        }
                    }

                }
            });*/

            mFragmentProductBinding.productCustomOptionsContainer.addView(spinnerSlot);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> createListOfSlots (Calendar intSlot) throws Exception{
        //Log.d(ApplicationConstant.TAG,"entre createSlot");
        ArrayList<String> spinnerOpions = new ArrayList<>();
        spinnerOpions.add(getResources().getString(R.string._please_select_));
        //List<List<String[]>> lista=customOption.getBookingSlotInfo();
        //Log.d("Victor","Calendar recibido"+ new SimpleDateFormat("MM/dd/yy", Locale.US).format(intSlot.getTime()));
        //spinnerOpions.add();
        Calendar today= Calendar.getInstance();
        //Calendar slot=(Calendar) intSlot.clone();
        int todayYear=today.get(Calendar.YEAR);
        int todayMonth=today.get(Calendar.MONTH);
        int todayDay=today.get(Calendar.DAY_OF_MONTH);
        int slotYear=intSlot.get(Calendar.YEAR);
        int slotMonth=intSlot.get(Calendar.MONTH);
        int slotDay=intSlot.get(Calendar.DAY_OF_MONTH);
        if(lista.size()>0) {
            //lista=customOption.getBookingSlotInfo();
            for (int i=0;i<lista.size();i++){
                if (!lista.get(i).isEmpty() && (intSlot.get(Calendar.DAY_OF_WEEK)-1) == i) {
                    //Log.d(ApplicationConstant.TAG,"No de i: "+i);
                    List<String[]> slotPerDay=lista.get(i);
                    //Log.d(ApplicationConstant.TAG,"este es es 0: "+slotPerDay.get(0)[0]);
                    //Log.d(ApplicationConstant.TAG,"este es es slot: "+slotYear+"/"+slotMonth+"/"+slotDay);
                    if(slotPerDay.size()==1 && slotPerDay.get(0)[0].equals("Sin horas")){
                        spinnerOpions.add(getResources().getString(R.string.no_have_hours));
                    }
                    else if(todayYear==slotYear && todayMonth==slotMonth && todayDay==slotDay) {

                        //Log.d(ApplicationConstant.TAG,"entre if");
                        int todayHour=today.get(Calendar.HOUR);
                        int todayMinute= today.get(Calendar.MINUTE);
                        DateFormat dateFormat= new SimpleDateFormat("hh:mm a",Locale.US);
                        Date todayHourMinute=dateFormat.parse(""+todayHour+":"+todayMinute+" "+(today.get(Calendar.AM_PM)==0?"AM":"PM"));
                        //Log.d("Victor","today: "+todayHourMinute);
                        //Log.d("Victor","scheduling: "+mPreventSchedulingBefore);
                        int preventInMiliSeconds=Integer.parseInt(mPreventSchedulingBefore)*60*1000;
                        //Log.d("Victor","prevent: "+preventInMiliSeconds);
                        Date todayWithSchedulingPrevent=new Date(todayHourMinute.getTime()+preventInMiliSeconds);
                        //Log.d("Victor","today whit prevent: "+todayWithSchedulingPrevent);
                        for (String[] horas : slotPerDay) {
                            //Log.d(ApplicationConstant.TAG,"horas: "+Arrays.toString(horas));
                            for (int j = 0; j < horas.length; j++) {
                                //Log.d("Victor","slot: "+horas[j]);
                                Date slotHourMinute= dateFormat.parse(horas[j]);
                                if (todayWithSchedulingPrevent.before(slotHourMinute)) {
                                    spinnerOpions.add(horas[j]);
                                }
                            }
                        }
                        if (spinnerOpions.size()<=1){
                            spinnerOpions.add(getResources().getString(R.string.no_have_hours));
                        }
                    }else{
                        //Log.d(ApplicationConstant.TAG,"entre else");
                        for (String[] horas : slotPerDay) {
                            for (int j = 0; j < horas.length; j++) {
                                spinnerOpions.add(horas[j]);
                            }
                        }
                    }
                }
            }
        }
        return spinnerOpions;
    }

    private void loadCustomOptionTimeType(int noOfCustomOpt) {
        try {
            View customOptionTimeView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_time_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            customOptionTimeView.setTag(noOfCustomOpt);
            final EditText timeEditText1 = customOptionTimeView.findViewById(R.id.timeET);

            Button resetBtn = customOptionTimeView.findViewById(R.id.resetBtn);
            resetBtn.setTag(noOfCustomOpt);
            resetBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText resetDate = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.timeET);
                    resetDate.setText("");
                    updatePrice();
                }
            });

            timeEditText1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar currentTime = Calendar.getInstance();
                    int hour = currentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = currentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String aMpM = "AM";
                            if (selectedHour > 11) {
                                aMpM = "PM";
                            }
                            //Make the 24 hour time format to 12 hour time format
                            int currentHour;
                            if (selectedHour > 11) {
                                currentHour = selectedHour - 12;
                            } else {
                                currentHour = selectedHour;
                            }

                            timeEditText1.setText(String.format(Locale.US, "%02d", currentHour) + ":" + String.format(Locale.US, "%02d", selectedMinute) + " " + aMpM);

                            updatePrice();
                        }
                    }, hour, minute, false);//true: 24 hour time

                    mTimePicker.setTitle(getString(R.string.select_time));
                    mTimePicker.show();
                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(customOptionTimeView);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void loadCustomOptionDateTimeType(int noOfCustomOpt) {
        try {
            View cusotomOpptionDateLayout = getActivity().getLayoutInflater().inflate(R.layout.custom_option_date_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            cusotomOpptionDateLayout.setTag("date" + noOfCustomOpt);
            /*hiding one button so that there are only one button for this type of custom view*/
            cusotomOpptionDateLayout.findViewById(R.id.resetBtn).setVisibility(View.GONE);

            final Calendar myCalendar = Calendar.getInstance();

            final EditText dateEditText1 = cusotomOpptionDateLayout.findViewById(R.id.dateET);

            mFragmentProductBinding.productCustomOptionsContainer.addView(cusotomOpptionDateLayout);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 5, 0, 0);

            View customOptionTimeView = getActivity().getLayoutInflater().inflate(R.layout.custom_option_time_view, mFragmentProductBinding.productCustomOptionsContainer, false);
            customOptionTimeView.setTag("time" + noOfCustomOpt);
            customOptionTimeView.setLayoutParams(params);
            LinearLayout timeLayput = new LinearLayout(getContext());

            timeLayput.setOrientation(LinearLayout.HORIZONTAL);

            final EditText timeEditText1 = customOptionTimeView.findViewById(R.id.timeET);

            Button resetDateTimeBtn = customOptionTimeView.findViewById(R.id.resetBtn);
            resetDateTimeBtn.setTag(noOfCustomOpt);

            final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "MM/dd/yy";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                    dateEditText1.setText(sdf.format(myCalendar.getTime()));

                    if (timeEditText1.getText().toString().equals("")) {

                        Calendar mcurrentTime = Calendar.getInstance();
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        int minute = mcurrentTime.get(Calendar.MINUTE);
                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                String aMpM = "AM";
                                if (selectedHour > 11) {
                                    aMpM = "PM";
                                }
                                //Make the 24 hour time format to 12 hour time format
                                int currentHour;
                                if (selectedHour > 11) {
                                    currentHour = selectedHour - 12;
                                } else {
                                    currentHour = selectedHour;
                                }
                                timeEditText1.setText(String.format(Locale.US, "%02d", currentHour) + ":" + String.format(Locale.US, "%02d", selectedMinute) + " " + aMpM);
                                updatePrice();
                            }
                        }, hour, minute, false); //Yes 24 hour time
                        mTimePicker.setTitle(R.string.select_time);
                        mTimePicker.show();
                    }
                    updatePrice();
                }
            };
            dateEditText1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DatePickerDialog(getContext(), date1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            resetDateTimeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText resetDate = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag("date" + v.getTag().toString()).findViewById(R.id.dateET);
                    resetDate.setText("");
                    resetDate.setError(null);
                    EditText resetTime = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag("time" + v.getTag().toString()).findViewById(R.id.timeET);
                    resetTime.setText("");
                    resetTime.setError(null);
                    updatePrice();
                }
            });

            timeEditText1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String aMpM = "AM";
                            if (selectedHour > 11) {
                                aMpM = "PM";
                            }
                            //Make the 24 hour time format to 12 hour time format
                            int currentHour;
                            if (selectedHour > 11) {
                                currentHour = selectedHour - 12;
                            } else {
                                currentHour = selectedHour;
                            }
                            timeEditText1.setText(String.valueOf(currentHour + ":" + selectedMinute + " " + aMpM));

                            if (dateEditText1.getText().toString().equals("")) {
                                new DatePickerDialog(getContext(), date1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                            }
                            updatePrice();
                        }
                    }, hour, minute, false); //true: 24 hour time
                    mTimePicker.setTitle(R.string.select_time);
                    mTimePicker.show();

                }
            });
            mFragmentProductBinding.productCustomOptionsContainer.addView(timeLayput);
            mFragmentProductBinding.productCustomOptionsContainer.addView(customOptionTimeView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadDropDownBundleOption(int noOfBundleOpt) {
        try {
            View bundleOptionTypeDropDownLayout = getActivity().getLayoutInflater().inflate(R.layout.bundle_option_drop_down_layout, mFragmentProductBinding.otherProductOptionsContainer, false);
            bundleOptionTypeDropDownLayout.setTag("bundleoption" + noOfBundleOpt);

            final EditText qtyET = bundleOptionTypeDropDownLayout.findViewById(R.id.qty_et);
            qtyET.setTag(noOfBundleOpt);
            qtyET.addTextChangedListener(new ProductFragment.GenericTextWatcher(qtyET));

            ImageView incrementQty = bundleOptionTypeDropDownLayout.findViewById(R.id.increment_qty_iv);
            incrementQty.setTag(noOfBundleOpt);
            incrementQty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int currentQty = Integer.parseInt(qtyET.getText().toString()) + 1;
                    qtyET.setText(String.valueOf(currentQty));
                }
            });
            ImageView decrementQty = bundleOptionTypeDropDownLayout.findViewById(R.id.decrement_qty_iv);
            decrementQty.setTag(noOfBundleOpt);
            decrementQty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int currentQty = Integer.parseInt(qtyET.getText().toString()) - 1;
                    if (currentQty < 1)
                        qtyET.setText("1");
                    else
                        qtyET.setText(String.valueOf(currentQty));
                }
            });

            ArrayList<String> spinnerOptions = new ArrayList<>();
            ArrayList<String> spinnerOptionsKeys = new ArrayList<>();
            spinnerOptions.add(getResources().getString(R.string._choose_a_product_));
            spinnerOptionsKeys.add("");

            Spinner dropdownBundleOptionSpinner = bundleOptionTypeDropDownLayout.findViewById(R.id.dropdown_bundle_option_spinner);

            for (int noOfOptions = 0; noOfOptions < mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().size(); noOfOptions++) {
                String optionString = mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().get(noOfOptions).getTitle();
                spinnerOptions.add(optionString);
                spinnerOptionsKeys.add(mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().get(noOfOptions).getOptionId());
            }
            ProductFragment.SpinnerWithTagAdapter spinnerArrayAdapter = new ProductFragment.SpinnerWithTagAdapter(spinnerOptions, spinnerOptionsKeys);
            dropdownBundleOptionSpinner.setAdapter(spinnerArrayAdapter);
            dropdownBundleOptionSpinner.setTag(noOfBundleOpt);

            dropdownBundleOptionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> spinnerView, View selectedItemView, int position, long id) {
                    try {
                        int bundleOptionPosition = (int) spinnerView.getTag();
                        String bundleOptionValueKey = selectedItemView.getTag().toString();

                        if (position == 0 || bundleOptionValueKey.equals("")) {
                            qtyET.setEnabled(false);
                            qtyET.setText("0");
                        } else {
                            int defaultQty = mCatalogSingleProductData.getBundleOptions().get(bundleOptionPosition).getOptionValues().get(Integer.parseInt(bundleOptionValueKey)).getDefaultQty();
                            if (defaultQty != 0) {
                                qtyET.setText(String.valueOf(defaultQty));
                            } else {
                                qtyET.setText("1");
                            }
                            if (mCatalogSingleProductData.getBundleOptions().get(bundleOptionPosition).getOptionValues().get(Integer.parseInt(bundleOptionValueKey)).getIsQtyUserDefined() == 1) {
                                qtyET.setEnabled(true);
                            } else {
                                qtyET.setEnabled(false);
                            }
                        }
                        updatePrice();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    updatePrice();
                }
            });

            mFragmentProductBinding.otherProductOptionsContainer.addView(bundleOptionTypeDropDownLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadRadioBundleOption(int noOfBundleOpt) {
        try {

            View bundleOptionTypeRadioLayout = getActivity().getLayoutInflater().inflate(R.layout.bundle_option_radio_layout, mFragmentProductBinding.otherProductOptionsContainer, false);
            bundleOptionTypeRadioLayout.setTag("bundleoption" + noOfBundleOpt);
            final EditText qtyET = bundleOptionTypeRadioLayout.findViewById(R.id.qty_et);
            qtyET.setTag("bundleoption" + noOfBundleOpt);
            qtyET.addTextChangedListener(new ProductFragment.GenericTextWatcher(qtyET));

            RadioGroup bundleOptionRG = bundleOptionTypeRadioLayout.findViewById(R.id.bundle_option_rg);
            bundleOptionRG.setTag("bundleoption" + noOfBundleOpt);

            ImageView incrementQty = bundleOptionTypeRadioLayout.findViewById(R.id.increment_qty_iv);
            incrementQty.setTag("bundleoption" + noOfBundleOpt);
            incrementQty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.getTag();
                    EditText editText = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.qty_et);
                    int currentQty = Integer.parseInt(editText.getText().toString()) + 1;
                    editText.setText(String.valueOf(currentQty));
                }
            });
            ImageView decrementQty = bundleOptionTypeRadioLayout.findViewById(R.id.decrement_qty_iv);
            decrementQty.setTag("bundleoption" + noOfBundleOpt);
            decrementQty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText editText = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag(v.getTag()).findViewById(R.id.qty_et);
                    int currentQty = Integer.parseInt(editText.getText().toString()) - 1;
                    if (currentQty < 1)
                        editText.setText("1");
                    else
                        editText.setText(String.valueOf(currentQty));
                }
            });

            if (mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getRequired() == 0) {
                RadioButton noneRadioButton = new RadioButton(getContext());
                noneRadioButton.setText(getString(R.string.none));
                noneRadioButton.setTextSize(14);
                bundleOptionRG.addView(noneRadioButton);
            }

            for (int noOfOptions = 0; noOfOptions < mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().size(); noOfOptions++) {
                RadioButton eachBundleOptionValueRB = new RadioButton(getContext());
                String optionString = mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().get(noOfOptions).getTitle();
                eachBundleOptionValueRB.setText(optionString);
                eachBundleOptionValueRB.setTextSize(14);
                eachBundleOptionValueRB.setTag(noOfOptions);
                bundleOptionRG.addView(eachBundleOptionValueRB);
            }

            bundleOptionRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                    try {
                        if (checkedId != -1) {
                            int bundleOptPosition = (int) radioGroup.getTag();
                            View bundleOptionTypeRadioLayout = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag(bundleOptPosition);

                            RadioButton selectedRadioButton = radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
                            String bundleOptionValueKey = selectedRadioButton.getTag().toString();
                            EditText qtyET = bundleOptionTypeRadioLayout.findViewById(R.id.qty_et);
                            if (bundleOptionValueKey.isEmpty()) {
                                qtyET.setEnabled(false);
                                qtyET.setText("0");
                            } else {
                                int defaultQty = mCatalogSingleProductData.getBundleOptions().get(bundleOptPosition).getOptionValues().get(Integer.parseInt(bundleOptionValueKey)).getDefaultQty();
                                if (defaultQty != 0) {
                                    qtyET.setText(String.valueOf(defaultQty));
                                } else {
                                    qtyET.setText("1");
                                }
                                if (mCatalogSingleProductData.getBundleOptions().get(bundleOptPosition).getOptionValues().get(Integer.parseInt(bundleOptionValueKey)).getIsQtyUserDefined() == 1) {
                                    qtyET.setEnabled(true);
                                } else {
                                    qtyET.setEnabled(false);
                                }
                            }
                            updatePrice();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            mFragmentProductBinding.otherProductOptionsContainer.addView(bundleOptionTypeRadioLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadCheckAndMultiBundleOption(int noOfBundleOpt) {
        try {
            LinearLayout bundleOptionTypeCheckMultiLayout = new LinearLayout(getContext());
            bundleOptionTypeCheckMultiLayout.setOrientation(LinearLayout.VERTICAL);
            bundleOptionTypeCheckMultiLayout.setTag("bundleoption" + noOfBundleOpt);

            for (int noOfOptions = 0; noOfOptions < mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().size(); noOfOptions++) {
                CheckBox eachCheckTypeBundleOptionValue = new CheckBox(getContext());
                String optionString = mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().get(noOfOptions).getTitle();
                eachCheckTypeBundleOptionValue.setText(optionString);
                eachCheckTypeBundleOptionValue.setTextSize(14);
                eachCheckTypeBundleOptionValue.setTag(noOfOptions);

                eachCheckTypeBundleOptionValue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updatePrice();
                    }
                });
                bundleOptionTypeCheckMultiLayout.addView(eachCheckTypeBundleOptionValue);
            }
            mFragmentProductBinding.otherProductOptionsContainer.addView(bundleOptionTypeCheckMultiLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatePrice() {
        try {
            int productId = 0;
            Double price = mCatalogSingleProductData.getPrice();
            Double finalPrice = mCatalogSingleProductData.getFinalPrice();
            Double updatedFinalPrice = finalPrice;

//            For Configurable
            if (mCatalogSingleProductData.getConfigurableData().getAttributes() != null) {
                ArrayList<String> configOptionsId = new ArrayList<>();
                ArrayList<String> selectedConfigOptionsId = new ArrayList<>();

                for (int noOfConfigData = 0; noOfConfigData < mCatalogSingleProductData.getConfigurableData().getAttributes().size(); noOfConfigData++) {
                    configOptionsId.add(mCatalogSingleProductData.getConfigurableData().getAttributes().get(noOfConfigData).getId());
                    if (mCatalogSingleProductData.getConfigurableData().getAttributes().get(noOfConfigData).getSwatchType().equals("visual") || mCatalogSingleProductData.getConfigurableData().getAttributes().get(noOfConfigData).getSwatchType().equals("text")) {
                        RecyclerView eachSwatchRecyclerView = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + noOfConfigData).findViewById(R.id.configurable_item_rv);
                        ArrayList<SwatchData> recyclerViewOpions = ((ProductAttributesSwatchRvAdapter) eachSwatchRecyclerView.getAdapter()).getSwatchValuesData();

                        for (int noOfAttribute = 0; noOfAttribute < recyclerViewOpions.size(); noOfAttribute++) {
                            if (recyclerViewOpions.get(noOfAttribute).isSelected()) {
                                selectedConfigOptionsId.add(noOfConfigData, mCatalogSingleProductData.getConfigurableData().getAttributes().get(noOfConfigData)
                                        .getOptions().get(noOfAttribute).getId());
                            }
                        }
                    } else {
                        Spinner spinnerItem = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + noOfConfigData).findViewById(R.id.spinner_configurable_item);
                        int positionOfSpinnerItem = spinnerItem.getSelectedItemPosition();
                        if (positionOfSpinnerItem != 0) {
                            selectedConfigOptionsId.add(noOfConfigData, mCatalogSingleProductData.getConfigurableData().getAttributes().get(noOfConfigData)
                                    .getOptions().get(positionOfSpinnerItem - 1).getId());
                        }
                    }
                }

                if (configOptionsId.size() == mCatalogSingleProductData.getConfigurableData().getAttributes().size()) {
                    JSONArray indexArray = new JSONArray(mCatalogSingleProductData.getConfigurableData().getIndex());
                    boolean priceFound = false;
                    for (int noOfCombinationData = 0; noOfCombinationData < indexArray.length(); noOfCombinationData++) {
                        for (int noOfIndexItems = 0; noOfIndexItems < configOptionsId.size(); noOfIndexItems++) {
                            if (indexArray.getJSONObject(noOfCombinationData).getString(configOptionsId.get(noOfIndexItems)).equals(selectedConfigOptionsId.get(noOfIndexItems))) {
                                priceFound = true;
                            } else {
                                priceFound = false;
                                break;
                            }
                        }
                        if (priceFound) {
                            updatedFinalPrice = mCatalogSingleProductData.getConfigurableData().getOptionPrices().get(noOfCombinationData).getFinalPrice().getAmount();
                            productId = mCatalogSingleProductData.getConfigurableData().getOptionPrices().get(noOfCombinationData).getProduct();
                            break;
                        }
                    }
                }
                updateImages(productId);
            }

//            For customoptions
            if (mCatalogSingleProductData.getCustomOptions().size() != 0) {
                for (int noOfCustomOpt = 0; noOfCustomOpt < mCatalogSingleProductData.getCustomOptions().size(); noOfCustomOpt++) {
                    String titleFieldCustomOption=mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle();
                    switch (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getType()) {
                        case "field":
                        case "area":
                            if(titleFieldCustomOption.contains("Booking Slot")){
                                Spinner spinnerItem = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                                int positionOfSpinnerItem = spinnerItem.getSelectedItemPosition();
                                if (positionOfSpinnerItem != 0) {
                                    if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfSpinnerItem - 1).getPriceType().equals("fixed")) {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfSpinnerItem - 1).getDefaultPrice();
                                    } else {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfSpinnerItem - 1).getDefaultPrice() * finalPrice / 100;
                                    }
                                }
                            }else {
                                View customOptionFieldView = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                                if (!((EditText) customOptionFieldView).getText().toString().matches("")) {
                                    if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getPriceType().equals("fixed")) {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice();

                                    } else {
                                        updatedFinalPrice += (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice() * finalPrice / 100);
                                    }
                                }
                            }

                            break;

                        case "file":
                            if (!((TextView) mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt).findViewById(R.id.fileSelectedTV)).getText()
                                    .equals(getResources().getString(R.string.no_file_selected))) {
                                if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getPriceType().equals("fixed")) {
                                    updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice();
                                } else {
                                    updatedFinalPrice += (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice() * finalPrice / 100);
                                }
                            }
                            break;

                        case "drop_down":
                            Spinner spinnerItem = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                            int positionOfSpinnerItem = spinnerItem.getSelectedItemPosition();
                            if (positionOfSpinnerItem != 0) {
                                if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfSpinnerItem - 1).getPriceType().equals("fixed")) {
                                    updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfSpinnerItem - 1).getDefaultPrice();
                                } else {
                                    updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfSpinnerItem - 1).getDefaultPrice() * finalPrice / 100;
                                }
                            }
                            break;

                        case "radio":
                            RadioGroup radioGroup = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                            if (radioGroup.getCheckedRadioButtonId() != -1) {
                                RadioButton selectedRadioBtn = mFragmentProductBinding.getRoot().findViewById(radioGroup.getCheckedRadioButtonId());
                                int positionOfRadioItem = radioGroup.indexOfChild(selectedRadioBtn);

                                if (positionOfRadioItem == 0 && mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 0) {
                                    break;
                                }

                                if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 0) {
                                    if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfRadioItem).getPriceType().equals("fixed")) {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfRadioItem - 1).getDefaultPrice();
                                    } else {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfRadioItem - 1).getDefaultPrice() * finalPrice / 100;
                                    }
                                } else {
                                    if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfRadioItem).getPriceType().equals("fixed")) {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfRadioItem).getDefaultPrice();
                                    } else {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(positionOfRadioItem).getDefaultPrice() * finalPrice / 100;
                                    }
                                }
                            }
                            break;

                        case "checkbox":
                        case "multiple":
                            LinearLayout checkBoxContainerLL = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);

                            for (int i = 0; i < mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().size(); i++) {
                                CheckBox tempChkBox = (CheckBox) checkBoxContainerLL.getChildAt(i);

                                if (tempChkBox.isChecked()) {
                                    if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(i).getDefaultPriceType().equals("fixed")) {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(i).getDefaultPrice();
                                    } else {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues().get(i).getDefaultPrice() * finalPrice / 100;
                                    }
                                }
                            }
                            break;

                        case "date":
                            View dateET = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt).findViewById(R.id.dateET);
                            if (!((EditText) dateET).getText().toString().matches("")) {
                                if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getPriceType().equals("fixed")) {
                                    updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice();
                                } else {
                                    updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice() * finalPrice / 100;
                                }
                            }
                            break;

                        case "time":
                            View timeET = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt).findViewById(R.id.timeET);
                            if (!((EditText) timeET).getText().toString().isEmpty()) {
                                if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getPriceType().equals("fixed")) {
                                    updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice();
                                } else {
                                    updatedFinalPrice += (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice() * finalPrice / 100);
                                }
                            }
                            break;


                        case "date_time":
                            View v = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag("date" + noOfCustomOpt).findViewById(R.id.dateET);
                            if (!((EditText) v).getText().toString().matches("")) {
                                updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice();
                            }
                            if (((EditText) v).getText().toString().matches("")) {
                                EditText asscociatedET = mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag("time" + noOfCustomOpt).findViewById(R.id.timeET);
                                if (!asscociatedET.getText().toString().matches("")) {
                                    if (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getPriceType().equals("fixed")) {
                                        updatedFinalPrice += mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice();
                                    } else {
                                        updatedFinalPrice += (mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getDefaultPrice() * finalPrice / 100);
                                    }
                                }
                            }
                            break;
                    }
                }
            }

//            For Downloadable product links

            if (mCatalogSingleProductData.getLinks().getLinkData() != null) {
                for (int i = 0; i < mCatalogSingleProductData.getLinks().getLinkData().size(); i++) {
                    /* i+1 as it contain a extra text box for label*/
                    if (((CheckBox) mFragmentProductBinding.otherProductOptionsContainer.getChildAt(i + 1)).isChecked()) {
                        updatedFinalPrice += mCatalogSingleProductData.getLinks().getLinkData().get(i).getPrice();
                    }
                }
            }

            price += updatedFinalPrice - finalPrice;

            String pattern = AppSharedPref.getPattern(getContext());
            int precision = AppSharedPref.getPrecision(getContext());
            String precisionFormat = "%." + precision + "f";
            String formatedFinalPrice = String.format(Locale.US, precisionFormat, updatedFinalPrice);
            String formatedPrice = String.format(Locale.US, precisionFormat, price);
            String newFormattedFinalPrice = pattern.replaceAll("%s", formatedFinalPrice);
            String newFormattedPrice = pattern.replaceAll("%s", formatedPrice);
            if (mFragmentProductBinding.productSpecialPriceTv.getVisibility() == View.VISIBLE) {
                mFragmentProductBinding.productSpecialPriceTv.setText(newFormattedFinalPrice);
                mFragmentProductBinding.actualPrice.setText(newFormattedPrice);
            } else {
                mFragmentProductBinding.actualPrice.setText(newFormattedFinalPrice);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateImages(int productId) {
        ArrayList<ImageGalleryData> imageGalleryDataArray = new ArrayList<>();
        for (int noOfImages = 0; noOfImages < mCatalogSingleProductData.getImageGallery().size(); noOfImages++) {
            ImageGalleryData imageGalleryDataObject = new ImageGalleryData();
            imageGalleryDataObject.setSmallImage(mCatalogSingleProductData.getImageGallery().get(noOfImages).getSmallImage());
            imageGalleryDataObject.setLargeImage(mCatalogSingleProductData.getImageGallery().get(noOfImages).getLargeImage());
            imageGalleryDataArray.add(imageGalleryDataObject);
        }

        if (productId != 0) {
            try {
                JSONObject imagesJSON = new JSONObject(mCatalogSingleProductData.getConfigurableData().getImages());
                JSONArray imageGalleryArray = imagesJSON.getJSONArray(String.valueOf(productId));

                int insertedImagePosition = 0;

                if (imageGalleryArray != null) {
                    for (int noOfImages = 0; noOfImages < imageGalleryArray.length(); noOfImages++) {
                        ImageGalleryData imageGalleryDataObject = new ImageGalleryData();
                        imageGalleryDataObject.setSmallImage(imageGalleryArray.getJSONObject(noOfImages).getString("thumb"));
                        imageGalleryDataObject.setLargeImage(imageGalleryArray.getJSONObject(noOfImages).getString("full"));
                        imageGalleryDataArray.add(insertedImagePosition++, imageGalleryDataObject);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mProductSliderAdapter = new ProductSliderAdapter(getContext(), mCatalogSingleProductData.getName(), imageGalleryDataArray);
        mFragmentProductBinding.productSliderViewPager.setAdapter(mProductSliderAdapter);
        mFragmentProductBinding.productSliderDotsTabLayout.setupWithViewPager(mFragmentProductBinding.productSliderViewPager, true);
    }

    public boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showToast(getActivity(), getContext().getString(R.string.storage_acccess_permission), Toast.LENGTH_LONG, 0);
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        RC_ACCESS_WRITE_STORAGE);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        RC_ACCESS_WRITE_STORAGE);
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    public void initializeConfigurableAttributeOption(int position) {
        try {
            if (mCatalogSingleProductData.getConfigurableData().getAttributes().get(position).getSwatchType().equals("visual") || mCatalogSingleProductData.getConfigurableData().getAttributes().get(position).getSwatchType().equals("text")) {
                RecyclerView eachSwatchRecyclerView = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + position).findViewById(R.id.configurable_item_rv);
                ArrayList<SwatchData> recyclerViewOpions = ((ProductAttributesSwatchRvAdapter) eachSwatchRecyclerView.getAdapter()).getSwatchValuesData();

                for (int noOfAttribute = 0; noOfAttribute < recyclerViewOpions.size(); noOfAttribute++) {
                    if (canAddCurrentAttributeOption(position, noOfAttribute)) {
                        recyclerViewOpions.get(noOfAttribute).setEnabled(true);
                    } else {
                        recyclerViewOpions.get(noOfAttribute).setEnabled(false);
                        recyclerViewOpions.get(noOfAttribute).setSelected(false);
                    }
                }

                eachSwatchRecyclerView.getAdapter().notifyDataSetChanged();

            } else {

                Spinner currentConfigurableAttributeSpinner = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + position).findViewById(R.id.spinner_configurable_item);
                ArrayList<String> spinnerOpions = new ArrayList<>();
                ArrayList<String> spinnerTags = new ArrayList<>();
                spinnerOpions.add(mCatalogSingleProductData.getConfigurableData().getChooseText());
                spinnerTags.add("");

                for (int attributeOptionPosition = 0; attributeOptionPosition < mCatalogSingleProductData.getConfigurableData().getAttributes()
                        .get(position).getOptions().size(); attributeOptionPosition++) {
                    if (canAddCurrentAttributeOption(position, attributeOptionPosition)) {

                        spinnerOpions.add(mCatalogSingleProductData.getConfigurableData().getAttributes().get(position)
                                .getOptions().get(attributeOptionPosition).getLabel());
                        spinnerTags.add(String.valueOf(attributeOptionPosition));
                    }
                }

                ProductFragment.SpinnerWithTagAdapter spinnerWithTagAdapter = new ProductFragment.SpinnerWithTagAdapter(spinnerOpions, spinnerTags);
                currentConfigurableAttributeSpinner.setAdapter(spinnerWithTagAdapter);
                currentConfigurableAttributeSpinner.setOnItemSelectedListener(configurableAttributeOnItemSelectedListener);
            }

            initializeAllFollowingConfigurableOptionLayouts(position + 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeAllFollowingConfigurableOptionLayouts(int position) {
        try {
            for (int attributeOptionPosition = position; attributeOptionPosition < mCatalogSingleProductData.getConfigurableData().getAttributes().size(); attributeOptionPosition++) {

                if (mCatalogSingleProductData.getConfigurableData().getAttributes().get(position).getSwatchType().equals("visual") || mCatalogSingleProductData.getConfigurableData().getAttributes().get(position).getSwatchType().equals("text")) {

                } else {
                    Spinner eachConfigurableAttributeSpinner = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag(attributeOptionPosition).findViewById(R.id.spinner_configurable_item);
                    ArrayList<String> spinnerOpions = new ArrayList<>();
                    spinnerOpions.add(mCatalogSingleProductData.getConfigurableData().getChooseText());
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, spinnerOpions);
                    eachConfigurableAttributeSpinner.setAdapter(spinnerArrayAdapter);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean canAddCurrentAttributeOption(int currentAttributePostionToUpdate, int attributeOptionPosition) {
        try {
            /*add all attribute option value for the position 0*/
            if (currentAttributePostionToUpdate == 0) {
                return mCatalogSingleProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getOptions().get(attributeOptionPosition).getProducts().size() != 0;
            }

            List<String> currentProductList = mCatalogSingleProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getOptions().get(attributeOptionPosition).getProducts();

            for (int noOfProductAssociatedWithCurrentAttributeOption = 0; noOfProductAssociatedWithCurrentAttributeOption < currentProductList.size(); noOfProductAssociatedWithCurrentAttributeOption++) {

                int noOfMatches = 0;

                for (int noOfAttributeBeforeTheCurentAtteibute = 0; noOfAttributeBeforeTheCurentAtteibute < currentAttributePostionToUpdate; noOfAttributeBeforeTheCurentAtteibute++) {
                    if (mCatalogSingleProductData.getConfigurableData().getAttributes().get(noOfAttributeBeforeTheCurentAtteibute).getSwatchType().equals("visual") || mCatalogSingleProductData.getConfigurableData().getAttributes().get(noOfAttributeBeforeTheCurentAtteibute).getSwatchType().equals("text")) {
                        RecyclerView eachSwatchRecyclerView = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + noOfAttributeBeforeTheCurentAtteibute).findViewById(R.id.configurable_item_rv);
                        ArrayList<SwatchData> eachRecyclerViewData = ((ProductAttributesSwatchRvAdapter) eachSwatchRecyclerView.getAdapter()).getSwatchValuesData();
                        String actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute = "";
                        for (int noOfData = 0; noOfData < eachRecyclerViewData.size(); noOfData++) {
                            if (eachRecyclerViewData.get(noOfData).isSelected()) {
                                actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute = String.valueOf(noOfData);
                            }
                        }
                        if (actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute.isEmpty()) {
                            return mCatalogSingleProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getSwatchType().equals("visual") || mCatalogSingleProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getSwatchType().equals("text");
                        }
                        List<String> eachProductArrayOfAttributeBeforeTheCurrentAttribute = mCatalogSingleProductData.getConfigurableData().getAttributes()
                                .get(noOfAttributeBeforeTheCurentAtteibute).getOptions().get(Integer.parseInt(actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute)).getProducts();
                        if (eachProductArrayOfAttributeBeforeTheCurrentAttribute != null) {
                            for (int i = 0; i < eachProductArrayOfAttributeBeforeTheCurrentAttribute.size(); i++) {
                                if (eachProductArrayOfAttributeBeforeTheCurrentAttribute.get(i).equals(currentProductList.get(noOfProductAssociatedWithCurrentAttributeOption))) {
                                    noOfMatches++;
                                }
                            }
                        }
                    } else {
                        Spinner eachConfigurableProductAttributeSpinnerBeforeCurrentAttributr = mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + noOfAttributeBeforeTheCurentAtteibute).findViewById(R.id.spinner_configurable_item);
                        String actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute = (String) eachConfigurableProductAttributeSpinnerBeforeCurrentAttributr.getSelectedItem();
                        if (actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute.isEmpty()) {
                            return mCatalogSingleProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getSwatchType().equals("visual") || mCatalogSingleProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getSwatchType().equals("text");
                        }
                        List<String> eachProductArrayOfAttributeBeforeTheCurrentAttribute = mCatalogSingleProductData.getConfigurableData().getAttributes()
                                .get(noOfAttributeBeforeTheCurentAtteibute).getOptions().get(Integer.parseInt(actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute)).getProducts();
                        if (eachProductArrayOfAttributeBeforeTheCurrentAttribute != null) {
                            for (int i = 0; i < eachProductArrayOfAttributeBeforeTheCurrentAttribute.size(); i++) {
                                if (eachProductArrayOfAttributeBeforeTheCurrentAttribute.get(i).equals(currentProductList.get(noOfProductAssociatedWithCurrentAttributeOption))) {
                                    noOfMatches++;
                                }
                            }
                        }
                    }
                }
                if (noOfMatches == currentAttributePostionToUpdate) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_GET_SINGLE_FILE) {
                uriData = data.getData();
                final String path = getPath(getContext(), uriData);
                try {
                    if (path != null) {
                        mFileUri = Uri.fromFile(new File(path));
                    }
                    ((TextView) mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(mSeletedCustomOption).findViewById(R.id.fileSelectedTV)).setText(mFileUri.getLastPathSegment());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                updatePrice();
            }
        }
    }

    public String getPath(Context context, Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    class GenericTextWatcher implements TextWatcher {

        private GenericTextWatcher(View view) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            updatePrice();
        }
    }

    public class SpinnerWithTagAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;
        private ArrayList<String> mSpinnerOptions;
        private ArrayList<String> spinnerOptionsKeys;

        SpinnerWithTagAdapter(ArrayList<String> spinnerOptions, ArrayList<String> spinnerOptionsKeys) {
            this.mLayoutInflater = getActivity().getLayoutInflater();
            this.mSpinnerOptions = spinnerOptions;
            this.spinnerOptionsKeys = spinnerOptionsKeys;
        }

        @Override
        public int getCount() {
            return mSpinnerOptions.size();
        }

        @Override
        public Object getItem(int position) {
            return spinnerOptionsKeys.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CheckedTextView spinnerItem = (CheckedTextView) mLayoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, null);
            spinnerItem.setPadding(5, 5, 5, 5);
            spinnerItem.setMinHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));
            spinnerItem.setText(mSpinnerOptions.get(position));
            spinnerItem.setTag(position - 1);
            return spinnerItem;
        }
    }
}