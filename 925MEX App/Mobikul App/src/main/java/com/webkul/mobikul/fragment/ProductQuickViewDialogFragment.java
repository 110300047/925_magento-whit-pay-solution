package com.webkul.mobikul.fragment;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.adapter.CatalogAttributesSwatchRvAdapter;
import com.webkul.mobikul.adapter.ProductAttributesSwatchRvAdapter;
import com.webkul.mobikul.databinding.DialogFragmentProductQuickViewBinding;
import com.webkul.mobikul.handler.ProductQuickViewDialogFragmentHandler;
import com.webkul.mobikul.model.catalog.ProductData;
import com.webkul.mobikul.model.catalog.SwatchData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_COLLECTION_TYPE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_PAGE_DATA;

/**
 * Created by vedesh.kumar on 21/3/17. @Webkul Software Pvt. Ltd
 */

public class ProductQuickViewDialogFragment extends DialogFragment {

    public DialogFragmentProductQuickViewBinding mBinding;
    public String mCollectionType;
    public ProductData mProductData;

    public ArrayList<String> configOptionsId;
    public ArrayList<String> selectedConfigOptionsId;

    public static ProductQuickViewDialogFragment newInstance(String collectionType, ProductData productData) {
        ProductQuickViewDialogFragment productQuickViewDialogFragment = new ProductQuickViewDialogFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_COLLECTION_TYPE, collectionType);
        args.putParcelable(BUNDLE_KEY_PRODUCT_PAGE_DATA, productData);
        productQuickViewDialogFragment.setArguments(args);
        return productQuickViewDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_product_quick_view, container, false);
        try {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().getAttributes().windowAnimations = R.style.CustomDialogFragmentAnimation;
        } catch (Exception e) {
            e.printStackTrace();
        }
        mCollectionType = getArguments().getString(BUNDLE_KEY_COLLECTION_TYPE);
        mProductData = getArguments().getParcelable(BUNDLE_KEY_PRODUCT_PAGE_DATA);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        configOptionsId = new ArrayList<>();
        selectedConfigOptionsId = new ArrayList<>();

        mProductData.setQty("1");
        mBinding.setData(mProductData);
        mBinding.setHandler(new ProductQuickViewDialogFragmentHandler(ProductQuickViewDialogFragment.this));
        if (mProductData.isInWishlist())
            mBinding.addToWishlistIv.setProgress(1);
        if (mProductData.getTypeId().equals("configurable")) {
            boolean canShowSwatch;
            if (getContext() instanceof HomeActivity) {
                canShowSwatch = ((HomeActivity) getContext()).mHomePageResponseDataObject.isShowSwatchOnCollection();
            } else if (getContext() instanceof CatalogProductActivity) {
                canShowSwatch = ((CatalogProductActivity) getContext()).mCatalogProductData.isShowSwatchOnCollection();
            } else {
                canShowSwatch = false;
            }
            if (canShowSwatch) {
                for (int noOfAttribute = 0; noOfAttribute < mProductData.getConfigurableData().getAttributes().size(); noOfAttribute++) {
                    if (mProductData.getConfigurableData().getAttributes().get(noOfAttribute).getSwatchType().equals("visual") || mProductData.getConfigurableData().getAttributes().get(noOfAttribute).getSwatchType().equals("text")) {
                        RecyclerView eachSwatchRecyclerView = new RecyclerView(getContext());
                        eachSwatchRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                        eachSwatchRecyclerView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        eachSwatchRecyclerView.setTag(noOfAttribute);
                        ArrayList<SwatchData> attributeSwatchData = new ArrayList<>();
                        try {
                            JSONObject mainSwatchDataObject = new JSONObject(mProductData.getConfigurableData().getSwatchData());
                            JSONObject eachOptionData = mainSwatchDataObject.getJSONObject(mProductData.getConfigurableData().getAttributes().get(noOfAttribute).getId());
                            Iterator<?> keys = eachOptionData.keys();
                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                if (eachOptionData.get(key) instanceof JSONObject) {
                                    JSONObject eachSwatchData = new JSONObject(eachOptionData.get(key).toString());
                                    SwatchData swatchData = new SwatchData();
                                    swatchData.setId(key);
                                    swatchData.setType(eachSwatchData.getString("type"));
                                    swatchData.setValue(eachSwatchData.getString("value"));
                                    attributeSwatchData.add(swatchData);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        CatalogAttributesSwatchRvAdapter catalogAttributeSwatchRvAdapter = new CatalogAttributesSwatchRvAdapter(ProductQuickViewDialogFragment.this, noOfAttribute, mProductData.getConfigurableData().getAttributes().get(noOfAttribute).getUpdateProductPreviewImage(), attributeSwatchData);
                        eachSwatchRecyclerView.setAdapter(catalogAttributeSwatchRvAdapter);
                        mBinding.swatchContainer.addView(eachSwatchRecyclerView);
                    }
                }
                initializeConfigurableAttributeOption(0);
            }
        }
    }

    private void initializeConfigurableAttributeOption(int position) {
        try {
            RecyclerView eachSwatchRecyclerView = mBinding.swatchContainer.findViewWithTag(position);
            ArrayList<SwatchData> recyclerViewOpions = ((CatalogAttributesSwatchRvAdapter) eachSwatchRecyclerView.getAdapter()).getSwatchValuesData();

            for (int noOfAttribute = 0; noOfAttribute < recyclerViewOpions.size(); noOfAttribute++) {
                if (canAddCurrentAttributeOption(position, noOfAttribute)) {
                    recyclerViewOpions.get(noOfAttribute).setEnabled(true);
                } else {
                    recyclerViewOpions.get(noOfAttribute).setEnabled(false);
                    recyclerViewOpions.get(noOfAttribute).setSelected(false);
                }
            }
            eachSwatchRecyclerView.getAdapter().notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean canAddCurrentAttributeOption(int currentAttributePostionToUpdate, int attributeOptionPosition) {
        try {
            if (currentAttributePostionToUpdate == 0) {
                return mProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getOptions().get(attributeOptionPosition).getProducts().size() != 0;
            }

            List<String> currentProductList = mProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getOptions().get(attributeOptionPosition).getProducts();

            for (int noOfProductAssociatedWithCurrentAttributeOption = 0; noOfProductAssociatedWithCurrentAttributeOption < currentProductList.size(); noOfProductAssociatedWithCurrentAttributeOption++) {

                int noOfMatches = 0;

                for (int noOfAttributeBeforeTheCurentAtteibute = 0; noOfAttributeBeforeTheCurentAtteibute < currentAttributePostionToUpdate; noOfAttributeBeforeTheCurentAtteibute++) {
                    if (mProductData.getConfigurableData().getAttributes().get(noOfAttributeBeforeTheCurentAtteibute).getSwatchType().equals("visual") || mProductData.getConfigurableData().getAttributes().get(noOfAttributeBeforeTheCurentAtteibute).getSwatchType().equals("text")) {
                        RecyclerView eachSwatchRecyclerView = mBinding.swatchContainer.findViewWithTag(noOfAttributeBeforeTheCurentAtteibute).findViewById(R.id.configurable_item_rv);
                        ArrayList<SwatchData> eachRecyclerViewData = ((ProductAttributesSwatchRvAdapter) eachSwatchRecyclerView.getAdapter()).getSwatchValuesData();
                        String actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute = "";
                        for (int noOfData = 0; noOfData < eachRecyclerViewData.size(); noOfData++) {
                            if (eachRecyclerViewData.get(noOfData).isSelected()) {
                                actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute = String.valueOf(noOfData);
                            }
                        }
                        if (actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute.isEmpty()) {
                            return mProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getSwatchType().equals("visual") || mProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getSwatchType().equals("text");
                        }
                        List<String> eachProductArrayOfAttributeBeforeTheCurrentAttribute = mProductData.getConfigurableData().getAttributes()
                                .get(noOfAttributeBeforeTheCurentAtteibute).getOptions().get(Integer.parseInt(actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute)).getProducts();
                        if (eachProductArrayOfAttributeBeforeTheCurrentAttribute != null) {
                            for (int i = 0; i < eachProductArrayOfAttributeBeforeTheCurrentAttribute.size(); i++) {
                                if (eachProductArrayOfAttributeBeforeTheCurrentAttribute.get(i).equals(currentProductList.get(noOfProductAssociatedWithCurrentAttributeOption))) {
                                    noOfMatches++;
                                }
                            }
                        }
                    } else {
                        Spinner eachConfigurableProductAttributeSpinnerBeforeCurrentAttributr = mBinding.swatchContainer.findViewWithTag(noOfAttributeBeforeTheCurentAtteibute).findViewById(R.id.spinner_configurable_item);
                        String actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute = (String) eachConfigurableProductAttributeSpinnerBeforeCurrentAttributr.getSelectedItem();
                        if (actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute.isEmpty()) {
                            return mProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getSwatchType().equals("visual") || mProductData.getConfigurableData().getAttributes().get(currentAttributePostionToUpdate).getSwatchType().equals("text");
                        }
                        List<String> eachProductArrayOfAttributeBeforeTheCurrentAttribute = mProductData.getConfigurableData().getAttributes()
                                .get(noOfAttributeBeforeTheCurentAtteibute).getOptions().get(Integer.parseInt(actualPositionOfEachConfigurableProductAttributeSpinnerBeforeCurrentAttribute)).getProducts();
                        if (eachProductArrayOfAttributeBeforeTheCurrentAttribute != null) {
                            for (int i = 0; i < eachProductArrayOfAttributeBeforeTheCurrentAttribute.size(); i++) {
                                if (eachProductArrayOfAttributeBeforeTheCurrentAttribute.get(i).equals(currentProductList.get(noOfProductAssociatedWithCurrentAttributeOption))) {
                                    noOfMatches++;
                                }
                            }
                        }
                    }
                }
                if (noOfMatches == currentAttributePostionToUpdate) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }
}