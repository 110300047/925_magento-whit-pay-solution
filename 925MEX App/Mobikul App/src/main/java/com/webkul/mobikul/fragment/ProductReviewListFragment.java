package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.ProductReviewsRvAdapter;
import com.webkul.mobikul.databinding.FragmentProductReviewsBinding;
import com.webkul.mobikul.model.product.ProductReviewData;

import java.util.ArrayList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_REVIEW_LIST_DATA;

/**
 * Created by vedesh.kumar on 3/1/17. @Webkul Software Pvt. Ltd
 */

public class ProductReviewListFragment extends Fragment {

    private FragmentProductReviewsBinding mBinding;
    private ArrayList<ProductReviewData> mReviewList;

    public static ProductReviewListFragment newInstance(ArrayList<ProductReviewData> reviewList) {
        ProductReviewListFragment productReviewListFragment = new ProductReviewListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(BUNDLE_KEY_REVIEW_LIST_DATA, reviewList);
        productReviewListFragment.setArguments(args);
        return productReviewListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_reviews, container, false);

        mReviewList = getArguments().getParcelableArrayList(BUNDLE_KEY_REVIEW_LIST_DATA);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(R.string.product_reviews);
        mBinding.reviewRv.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.reviewRv.setAdapter(new ProductReviewsRvAdapter(getContext(), mReviewList));
    }
}
