package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.FragmentShippingMethodBinding;
import com.webkul.mobikul.databinding.ItemCheckoutShippingMethodBinding;
import com.webkul.mobikul.handler.ShippingMethodFragmentHandler;
import com.webkul.mobikul.model.checkout.ShippingMethod;
import com.webkul.mobikul.model.checkout.ShippingMethodInfoData;

import java.util.ArrayList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SHIPPING_METHOD_INFO_DATA;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class ShippingMethodFragment extends Fragment {

    private FragmentShippingMethodBinding mBinding;
    private ArrayList<View> shippingMethodsList;
    private ShippingMethodInfoData mShippingMethodInfoData;

    View.OnClickListener shippingMethodClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (View view : shippingMethodsList) {
                if (view.getTag() == v.getTag()) {
                    ((RadioButton) view.findViewById(R.id.shipping_method_rb)).setChecked(true);
                    mShippingMethodInfoData.setSelectedShippingMethodCode(v.getTag().toString());
                } else {
                    ((RadioButton) view.findViewById(R.id.shipping_method_rb)).setChecked(false);
                }
            }
        }
    };

    public static ShippingMethodFragment newInstance(ShippingMethodInfoData shippingMethodInfoData) {
        ShippingMethodFragment shippingMethodFragment = new ShippingMethodFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_KEY_SHIPPING_METHOD_INFO_DATA, shippingMethodInfoData);
        shippingMethodFragment.setArguments(args);
        return shippingMethodFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shipping_method, container, false);
        mBinding.setHandler(new ShippingMethodFragmentHandler());
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mShippingMethodInfoData = getArguments().getParcelable(BUNDLE_KEY_SHIPPING_METHOD_INFO_DATA);

        if (mShippingMethodInfoData != null) {
            mBinding.setData(mShippingMethodInfoData);
            shippingMethodsList = new ArrayList<>();
            updateShippingMethodRg();
        }

        mBinding.scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, final int scrollY, int oldScrollX, int oldScrollY) {
                if ((scrollY - oldScrollY) < 0 || (scrollY > (mBinding.scrollView.getChildAt(0).getHeight() - mBinding.scrollView.getHeight() - 100))) {
                    mBinding.continueBtn.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
                } else {
                    mBinding.continueBtn.animate().alpha(0f).translationY(mBinding.continueBtn.getHeight()).setInterpolator(new AccelerateInterpolator(1.4f));
                }
            }
        });
    }

    private void updateShippingMethodRg() {
        for (int shippingMethodPosition = 0; shippingMethodPosition < mShippingMethodInfoData.getShippingMethods().size(); shippingMethodPosition++) {
            ShippingMethod eachShippingMethod = mShippingMethodInfoData.getShippingMethods().get(shippingMethodPosition);
            TextView titleTv = new TextView(getContext());
            titleTv.setTextColor(getResources().getColor(R.color.grey_400));
            titleTv.setText(eachShippingMethod.getTitle());
            titleTv.setTextSize(14);

            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            llp.setMargins(15, 40, 0, 0);
            titleTv.setLayoutParams(llp);

            if (shippingMethodPosition > 0) {
                titleTv.setPadding(titleTv.getPaddingLeft(), titleTv.getPaddingTop() + 10, titleTv.getPaddingRight(), titleTv.getPaddingBottom());
            }

            mBinding.shippingMethodRg.addView(titleTv);
            for (int methodOfDifferentShippingMethodIdx = 0; methodOfDifferentShippingMethodIdx < eachShippingMethod.getMethod().size(); methodOfDifferentShippingMethodIdx++) {

                ItemCheckoutShippingMethodBinding methodBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.item_checkout_shipping_method, mBinding.shippingMethodRg, false);
                methodBinding.setData(eachShippingMethod.getMethod().get(methodOfDifferentShippingMethodIdx));
                methodBinding.getRoot().setTag(eachShippingMethod.getMethod().get(methodOfDifferentShippingMethodIdx).getCode());
                methodBinding.shippingMethodRb.setTag(eachShippingMethod.getMethod().get(methodOfDifferentShippingMethodIdx).getCode());
                methodBinding.getRoot().setOnClickListener(shippingMethodClickListener);
                methodBinding.shippingMethodRb.setOnClickListener(shippingMethodClickListener);

                if (mShippingMethodInfoData.getShippingMethods().size() == 1 && eachShippingMethod.getMethod().size() == 1) {
                    if (eachShippingMethod.getMethod().get(0).getError() == null || eachShippingMethod.getMethod().get(0).getError().isEmpty()) {
                        methodBinding.shippingMethodRb.setChecked(true);
                        mShippingMethodInfoData.setSelectedShippingMethodCode(eachShippingMethod.getMethod().get(methodOfDifferentShippingMethodIdx).getCode());
                    }
                }

                shippingMethodsList.add(methodBinding.getRoot());
                mBinding.shippingMethodRg.addView(methodBinding.getRoot());

                if (eachShippingMethod.getMethod().get(methodOfDifferentShippingMethodIdx).getError() != null &&
                        !eachShippingMethod.getMethod().get(methodOfDifferentShippingMethodIdx).getError().isEmpty()) {
                    methodBinding.mainLayout.setEnabled(false);
                    methodBinding.shippingMethodRb.setEnabled(false);
                    TextView errorTv = new TextView(getContext());
                    errorTv.setText(eachShippingMethod.getMethod().get(methodOfDifferentShippingMethodIdx).getError());
                    errorTv.setTextSize(12);
                    errorTv.setTextColor(getResources().getColor(R.color.red_600));
                    mBinding.shippingMethodRg.addView(errorTv);
                }
            }
        }
    }

    public ShippingMethodInfoData getShippingMethodInfoData() {
        return mShippingMethodInfoData;
    }
}