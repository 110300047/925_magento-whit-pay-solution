package com.webkul.mobikul.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.FragmentSignInBinding;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.customer.signin.SignInData;

import static com.webkul.mobikul.constants.ApplicationConstant.DEMO_PASSWORD;
import static com.webkul.mobikul.constants.ApplicationConstant.DEMO_USERNAME;


public class SignInFragment extends Fragment {

    public FragmentSignInBinding mBinding;
    public SignInData mSignInData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSignInData = new SignInData(getContext());
        mSignInData.setUsername(DEMO_USERNAME);
        mSignInData.setPassword(DEMO_PASSWORD);

        mBinding.setData(mSignInData);
        mBinding.setHandler(((MobikulApplication) getActivity().getApplication()).getSignInHandlerClass(getContext(), SignInFragment.this));
    }
}