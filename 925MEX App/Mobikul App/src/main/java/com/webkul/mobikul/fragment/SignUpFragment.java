package com.webkul.mobikul.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.FragmentSignUpBinding;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormData;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormDataParcelable;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SignUpFragment extends Fragment {

    public FragmentSignUpBinding mBinding;
    private CreateAccountFormDataParcelable mTemp = null;
    private CreateAccountFormData mCreateFormresponseData = null;
    private int mIsSocial = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false);
        return mBinding.getRoot();
    }

    public SignUpFragment newInstance(int isSocial, CreateAccountFormDataParcelable socialLoginData) {
        SignUpFragment signUpFragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putInt("isSocial", isSocial);
        args.putParcelable("socialLoginData", socialLoginData);
        signUpFragment.setArguments(args);
        return signUpFragment;
    }

    public FragmentSignUpBinding getBinding() {
        return mBinding;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mIsSocial = getArguments().getInt("isSocial", 0);
        mTemp = getArguments().getParcelable("socialLoginData");

        if (mIsSocial == 0) {
            mBinding.scrollView.setVisibility(View.VISIBLE);
        }

        ApiConnection.getCreateAccountFormData(AppSharedPref.getStoreId(getContext()))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CreateAccountFormData>(getContext()) {
            @Override
            public void onNext(CreateAccountFormData createAccountFormData) {
                super.onNext(createAccountFormData);
                onFormDataResponseRecieved(createAccountFormData);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mBinding.categoryProgress.setVisibility(View.GONE);
            }
        });
    }

    private void onFormDataResponseRecieved(CreateAccountFormData createAccountFormData) {
        mCreateFormresponseData = createAccountFormData;

        if (mIsSocial == 1)
            CreateSignupDataForSocialLogin();

        mBinding.setSignUpPageData(mCreateFormresponseData);
        mBinding.setSignUpHandler(((MobikulApplication) getActivity().getApplication()).getSignUpHandler(getContext(), mIsSocial, SignUpFragment.this, mCreateFormresponseData));

        setUpPrefix();
        setUpSuffix();
        setUpGender();
        mBinding.categoryProgress.setVisibility(View.GONE);
    }

    private void CreateSignupDataForSocialLogin() {
        switch (mTemp.getLoginFrom()) {
            case "facebook":
                mCreateFormresponseData.setFirstName(mTemp.getFirstName());
                mCreateFormresponseData.setLastName(mTemp.getLastName());
                mCreateFormresponseData.setEmailAddr(mTemp.getEmailAddr());
                mCreateFormresponseData.setGender(mTemp.getGender());
                mCreateFormresponseData.setPictureURL(mTemp.getPictureURL());
                mCreateFormresponseData.setPassword(mTemp.getPassword());
                break;
            case "google":
                mCreateFormresponseData.setFirstName(mTemp.getFirstName());
                mCreateFormresponseData.setLastName(mTemp.getLastName());
                mCreateFormresponseData.setEmailAddr(mTemp.getEmailAddr());
                mCreateFormresponseData.setPictureURL(mTemp.getPictureURL());
                mCreateFormresponseData.setPassword(mTemp.getPassword());
                break;
            case "linkedIn":
                mCreateFormresponseData.setFirstName(mTemp.getFirstName());
                mCreateFormresponseData.setLastName(mTemp.getLastName());
                mCreateFormresponseData.setEmailAddr(mTemp.getEmailAddr());
                mCreateFormresponseData.setPictureURL(mTemp.getPictureURL());
                mCreateFormresponseData.setPassword(mTemp.getPassword());
                break;
            default:
                break;
        }
    }

    private void setUpGender() {
        if (mCreateFormresponseData.isIsGenderVisible()) {
            List<String> genderSpinnerData = new ArrayList<>();
            genderSpinnerData.add("");
            genderSpinnerData.add(getResources().getString(R.string.male));
            genderSpinnerData.add(getResources().getString(R.string.female));

            mBinding.genderSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                    , genderSpinnerData
            ));
            mBinding.genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mCreateFormresponseData.setGender(parent.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void setUpSuffix() {
        if (mCreateFormresponseData.isIsSuffixVisible() && mCreateFormresponseData.isSuffixHasOptions() && mCreateFormresponseData.getSuffixOptions().size() > 0) {
            List<String> suffixSpinnerData = new ArrayList<>();
            suffixSpinnerData.add("");
            for (int suffixIterator = 0; suffixIterator < mCreateFormresponseData.getSuffixOptions().size(); suffixIterator++) {
                suffixSpinnerData.add(mCreateFormresponseData.getSuffixOptions().get(suffixIterator));
            }


            mBinding.suffixSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                    , suffixSpinnerData
            ));
            mBinding.suffixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mCreateFormresponseData.setSuffix(parent.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void setUpPrefix() {
        if (mCreateFormresponseData.isIsPrefixVisible() && mCreateFormresponseData.isPrefixHasOptions() && mCreateFormresponseData.getPrefixOptions().size() > 0) {
            List<String> prefixSpinnerData = new ArrayList<>();
            prefixSpinnerData.add("");
            for (int prefixIterator = 0; prefixIterator < mCreateFormresponseData.getPrefixOptions().size(); prefixIterator++) {
                prefixSpinnerData.add(mCreateFormresponseData.getPrefixOptions().get(prefixIterator));
            }

            mBinding.prefixSpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item
                    , prefixSpinnerData
            ));
            mBinding.prefixSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mCreateFormresponseData.setPrefix(parent.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }
}