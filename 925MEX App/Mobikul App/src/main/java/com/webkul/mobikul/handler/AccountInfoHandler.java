package com.webkul.mobikul.handler;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.AccountInfoActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.accountInfo.AccountInfoResponseData;
import com.webkul.mobikul.model.customer.accountInfo.SaveAccountInfoResponseData;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_EMAIL;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_NAME;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 21/1/17. @Webkul Software Private limited
 */

public class AccountInfoHandler {

    private static final String KEY_NAME = "key";

    private AccountInfoActivity mContext;
    private AccountInfoResponseData mAccountInfoResponseData;

    private Cipher mCipher;
    private KeyStore mKeyStore;
    private FingerprintHandler mHelper;
    private CancellationSignal mCancellationSignal;
    private boolean isCurrentPasswordVisible = false;
    private boolean isNewPasswordVisible = false;
    private boolean isConfirmNewPasswordVisible = false;

    public AccountInfoHandler(Context context) {
        mContext = (AccountInfoActivity) context;
    }

    public void onClickOpenCalender(View view, final String dobFormat) {

        final Calendar dobCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date_of_birth = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                dobCalendar.set(Calendar.YEAR, year);
                dobCalendar.set(Calendar.MONTH, month);
                dobCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dob = new SimpleDateFormat(dobFormat, Locale.US);

                mContext.mBinding.dobEt.setText(dob.format(dobCalendar.getTime()));
            }
        };

        new DatePickerDialog(mContext, date_of_birth, dobCalendar.get(Calendar.YEAR), dobCalendar.get(Calendar.MONTH), dobCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void onClickSaveBtn(View view, AccountInfoResponseData accountInfoResponseData) {
        if (!mContext.mBinding.getIsLoading()) {
            mAccountInfoResponseData = accountInfoResponseData;

            if (mAccountInfoResponseData.isFormValidated(mContext)) {
                initFingerPrintSecurity();
                Utils.hideKeyboard(mContext);
            }
        }
    }

    private void initFingerPrintSecurity() {
        String touch_username = AppSharedPref.getCustomerFingerUserName(mContext);
        String touch_password = AppSharedPref.getCustomerFingerPassword(mContext);

        KeyguardManager keyguardManager = (KeyguardManager) mContext.getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) mContext.getSystemService(FINGERPRINT_SERVICE);
            if ((!keyguardManager.isKeyguardSecure()) || (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) ||
                    (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (!fingerprintManager.hasEnrolledFingerprints())) || (touch_username.equals("") && touch_password.equals("")) ||
                    (!touch_username.equals(AppSharedPref.getCustomerEmail(mContext)))) {
                callApi();
            } else {
                mContext.mBinding.fingerPrintLoginContainer.setVisibility(View.VISIBLE);
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                inflater.inflate(R.layout.fingerprint_login_layout, mContext.mBinding.fingerPrintLoginContainer);

                generateKey();
                if (cipherInit()) {
                    FingerprintManager.CryptoObject cryptoObject;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        cryptoObject = new FingerprintManager.CryptoObject(mCipher);
                        mHelper = new FingerprintHandler();
                        mHelper.startAuth(fingerprintManager, cryptoObject);
                    }
                }
            }
        } else {
            callApi();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKey() {
        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            mKeyStore.load(null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator.init(new
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
            }
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean cipherInit() {
        try {
            mCipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(KEY_NAME, null);
            mCipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (InvalidKeyException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    public void cancelFingerPrint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && mCancellationSignal != null) {
            mCancellationSignal.cancel();
        }
    }

    private void callApi() {
        mContext.mBinding.setIsLoading(true);

        ApiConnection.saveAccountInfo(
                AppSharedPref.getCustomerId(mContext)
                , mAccountInfoResponseData.getPrefixValue()
                , mAccountInfoResponseData.getFirstName()
                , mAccountInfoResponseData.getMiddleName()
                , mAccountInfoResponseData.getLastName()
                , mAccountInfoResponseData.getEmail()
                , mAccountInfoResponseData.getDOBValue().replace("/", "-")
                , mAccountInfoResponseData.getTaxValue()
                , mAccountInfoResponseData.getSuffixValue()
                , mAccountInfoResponseData.getGenderValue()
                , mAccountInfoResponseData.isChangesEmailChecked() ? 1 : 0
                , mAccountInfoResponseData.isChangesPasswordChecked() ? 1 : 0
                , mAccountInfoResponseData.getCurrentPassword()
                , mAccountInfoResponseData.getNewPassword()
                , mAccountInfoResponseData.getConfirmNewPassword()
                , mAccountInfoResponseData.getMobile())

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<SaveAccountInfoResponseData>(mContext) {
            @Override
            public void onNext(SaveAccountInfoResponseData saveAccountInfoResponseData) {
                super.onNext(saveAccountInfoResponseData);
                mContext.mBinding.setIsLoading(false);
                onResponseRecieved(saveAccountInfoResponseData);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mContext.mBinding.setIsLoading(false);
            }
        });
    }

    private void onResponseRecieved(SaveAccountInfoResponseData saveAccountInfoResponseData) {
        if (saveAccountInfoResponseData.isSuccess()) {
            showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE, mContext.getString(R.string.message_account_info_updated), saveAccountInfoResponseData.getMessage());
            updateCustomerSharedPref(saveAccountInfoResponseData.getCustomerName(), mAccountInfoResponseData.getEmail());
        } else {
            showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.something_went_wrong), saveAccountInfoResponseData.getMessage());
        }
    }

    protected void updateCustomerSharedPref(String customerName, String email) {
        try {
            SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(mContext, CUSTOMER_PREF);
            customerDataSharedPref.putString(KEY_CUSTOMER_NAME, customerName);
            customerDataSharedPref.putString(KEY_CUSTOMER_EMAIL, email);
            customerDataSharedPref.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

        void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
            mCancellationSignal = new CancellationSignal();
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.authenticate(cryptoObject, mCancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errMsgId,
                                          CharSequence errString) {
            showToast(mContext, "Authentication error\n" + errString, Toast.LENGTH_LONG, 0);
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            showToast(mContext, "Authentication help\n" + helpString, Toast.LENGTH_LONG, 0);
        }

        @Override
        public void onAuthenticationFailed() {
            showToast(mContext, "Authentication failed.", Toast.LENGTH_SHORT, 0);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            String touch_username = AppSharedPref.getCustomerFingerUserName(mContext);
            String touch_password = AppSharedPref.getCustomerFingerPassword(mContext);
            if ((!touch_username.equals("")) && (!touch_password.equals(""))) {
                mContext.mBinding.fingerPrintLoginContainer.setVisibility(View.GONE);
                callApi();
                showToast(mContext, mContext.getString(R.string.auth_succeeded), Toast.LENGTH_SHORT, 0);
            }
        }
    }

    public void onClickShowHideCurrentPassword(@SuppressWarnings("UnusedParameters") View view) {
        if (!isCurrentPasswordVisible) {
            mContext.mBinding.currentPasswordEt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mContext.mBinding.currentPasswordEt.setSelection(mContext.mBinding.currentPasswordEt.length());
            isCurrentPasswordVisible = true;
            view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_strikethrough_diagonal));
        } else {
            mContext.mBinding.currentPasswordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mContext.mBinding.currentPasswordEt.setSelection(mContext.mBinding.currentPasswordEt.length());
            isCurrentPasswordVisible = false;
            view.setBackground(null);
        }
    }

    public void onClickShowHideNewPassword(@SuppressWarnings("UnusedParameters") View view) {
        if (!isNewPasswordVisible) {
            mContext.mBinding.newPasswordEt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mContext.mBinding.newPasswordEt.setSelection(mContext.mBinding.newPasswordEt.length());
            isNewPasswordVisible = true;
            view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_strikethrough_diagonal));
        } else {
            mContext.mBinding.newPasswordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mContext.mBinding.newPasswordEt.setSelection(mContext.mBinding.newPasswordEt.length());
            isNewPasswordVisible = false;
            view.setBackground(null);
        }
    }

    public void onClickShowHideConfirmNewPassword(@SuppressWarnings("UnusedParameters") View view) {
        if (!isConfirmNewPasswordVisible) {
            mContext.mBinding.confirmpasswordEt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mContext.mBinding.confirmpasswordEt.setSelection(mContext.mBinding.confirmpasswordEt.length());
            isConfirmNewPasswordVisible = true;
            view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_strikethrough_diagonal));
        } else {
            mContext.mBinding.confirmpasswordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mContext.mBinding.confirmpasswordEt.setSelection(mContext.mBinding.confirmpasswordEt.length());
            isConfirmNewPasswordVisible = false;
            view.setBackground(null);
        }
    }
}