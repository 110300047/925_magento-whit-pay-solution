package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.NewAddressActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ACTIVITY_TITLE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ADDRESS_ID;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class AddressBookFragmentHandler {

    private final Context mContext;

    public AddressBookFragmentHandler(Context context) {
        mContext = context;
    }

    public void onClickEditBillingAddress(View view, int billingId) {
        Intent intent = new Intent(mContext, NewAddressActivity.class);
        intent.putExtra(BUNDLE_KEY_ADDRESS_ID, billingId);
        intent.putExtra(BUNDLE_KEY_ACTIVITY_TITLE, mContext.getString(R.string.title_activity_billing_address));
        mContext.startActivity(intent);
    }

    public void onClickAddNewAddress(View view) {
        Intent intent = new Intent(mContext, NewAddressActivity.class);
        intent.putExtra(BUNDLE_KEY_ACTIVITY_TITLE, mContext.getString(R.string.title_activity_new_address));
        mContext.startActivity(intent);
    }

    public void onClickEditShippingAddress(View view, int shippingId) {
        Intent intent = new Intent(mContext, NewAddressActivity.class);
        intent.putExtra(BUNDLE_KEY_ADDRESS_ID, shippingId);
        intent.putExtra(BUNDLE_KEY_ACTIVITY_TITLE, mContext.getString(R.string.title_activity_shipping_address));
        mContext.startActivity(intent);
    }
}
