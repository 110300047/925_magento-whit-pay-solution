package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.webkul.mobikul.activity.AdvanceSearchActivity;
import com.webkul.mobikul.helper.MobikulApplication;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ADVANCE_SEARCH_QUERY_JSON;

/**
 * Created by vedesh.kumar on 2/2/17. @Webkul Software Private limited
 */

public class AdvanceSearchActivityHandler {

    private final AdvanceSearchActivity mContext;
    private JSONObject mAdvanceSearchInputJSONObject;

    public AdvanceSearchActivityHandler(AdvanceSearchActivity advanceSearchActivity) {
        mContext = advanceSearchActivity;
    }

    public void onClickSearchButton(View view) {
        boolean isAnyOptionSelected = false;

        mAdvanceSearchInputJSONObject = new JSONObject();
        for (int noOfInputType = 0; noOfInputType < mContext.mAdvanceSearchFormData.getFieldList().size(); noOfInputType++) {
            try {
                switch (mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getInputType()) {
                    case "string":
                        EditText eT = (EditText) mContext.mBinding.advanceSearchFieldContainer.findViewWithTag("inputType/" + noOfInputType);

                        if (!eT.getText().toString().trim().equals("")) {
                            isAnyOptionSelected = true;
                        }
                        JSONObject temp = new JSONObject();
                        temp.put("code", mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getAttributeCode());
                        temp.put("value", eT.getText().toString());
                        temp.put("inputType", "string");
                        mAdvanceSearchInputJSONObject.put("" + noOfInputType, temp);
                        break;

                    case "yesno":

//                                                        Spinner spinner = (Spinner) rootView.findViewWithTag("inputType/" + noOfInputType);
//                                                        String tag1[] = spinner.getTag().toString().split("/");
//                                                        JSONObject temp1 = new JSONObject();
//                                                        temp1.put("code", mobikulCatalogGetadvancedsearchFieldsDataArr.getJSONObject(Integer.parseInt(tag[1])).getString("attributeCode"));
//                                                        temp1.put("inputType", "yesno");
//
//                                                        if (spinner.getSelectedItemPosition() == 0) {
//                                                        } else {
//                                                            v.setTag(false);
//                                                        }
//
//                                                        if (labelToValueMap.containsKey(spinner.getSelectedItem().toString()))
//                                                            temp1.put("value", labelToValueMap.get(spinner.getSelectedItem().toString()));
//                                                        else
//                                                            temp1.put("value", "");
//                                                        responseObject.put("" + noOfInputType, temp1);

                        break;


                    case "price":
                        LinearLayout priceLinearLayout = (LinearLayout) mContext.mBinding.advanceSearchFieldContainer.findViewWithTag("inputType/" + noOfInputType);
                        EditText eT1 = (EditText) priceLinearLayout.findViewWithTag("priceFrom");
                        EditText eT2 = (EditText) priceLinearLayout.findViewWithTag("priceTo");

                        if (!(eT1.getText().toString().trim().equals("") || eT2.getText().toString().trim().equals(""))) {
                            isAnyOptionSelected = true;
                        }
                        JSONObject temp1 = new JSONObject();
                        JSONObject temp2 = new JSONObject();
                        temp1.put("code", mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getAttributeCode());
                        temp1.put("inputType", "price");
                        temp2.put("from", eT1.getText().toString().trim());
                        temp2.put("to", eT2.getText().toString().trim());
                        temp1.put("value", temp2);
                        mAdvanceSearchInputJSONObject.put("" + noOfInputType, temp1);
                        break;


                    case "date":
                        LinearLayout dateLinearLayout = (LinearLayout) mContext.mBinding.advanceSearchFieldContainer.findViewWithTag("inputType/" + noOfInputType);
                        EditText eT3 = (EditText) dateLinearLayout.findViewWithTag("dateFrom");
                        EditText eT4 = (EditText) dateLinearLayout.findViewWithTag("dateTo");

                        if (!(eT3.getText().toString().trim().equals("") || eT4.getText().toString().trim().equals(""))) {
                            isAnyOptionSelected = true;
                        }
                        JSONObject temp3 = new JSONObject();
                        JSONObject temp4 = new JSONObject();
                        temp3.put("code", mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getAttributeCode());
                        temp3.put("inputType", "date");
                        temp4.put("from", eT3.getText().toString().trim());
                        temp4.put("to", eT4.getText().toString().trim());
                        temp3.put("value", temp4);
                        mAdvanceSearchInputJSONObject.put("" + noOfInputType, temp3);
                        break;

                    case "select":
                        if(!mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getLabel().equals("Color")) {
                            JSONObject temp5 = new JSONObject();
                            JSONObject temp6 = new JSONObject();
                            for (int noOfOption = 0; noOfOption < mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getOptions().size(); noOfOption++) {
                                CheckBox chk = (CheckBox) mContext.mBinding.advanceSearchFieldContainer.findViewWithTag("inputType/" + noOfInputType + "/check/" + noOfOption);
                                if (chk.isChecked()) {
                                    temp6.put(mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getOptions().get(noOfOption).getValue(), "true");
                                    isAnyOptionSelected = true;
                                } else {
                                    temp6.put(mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getOptions().get(noOfOption).getValue(), "false");
                                }
                            }
                            temp5.put("code", mContext.mAdvanceSearchFormData.getFieldList().get(noOfInputType).getAttributeCode());
                            temp5.put("inputType", "select");
                            temp5.put("value", temp6);
                            mAdvanceSearchInputJSONObject.put("" + noOfInputType, temp5);
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (isAnyOptionSelected) {
            handleResponseObject();
        } else {
            new SweetAlertDialog(mContext)
                    .setTitleText("Nothing Selected")
                    .setContentText("Please select at least one search option !!!")
                    .show();
        }
    }

    private void handleResponseObject() {
        Intent advancedSearchResultsIntent = new Intent(mContext, ((MobikulApplication) mContext.getApplication()).getCatalogProductPageClass());
        advancedSearchResultsIntent.putExtra(BUNDLE_KEY_ADVANCE_SEARCH_QUERY_JSON, mAdvanceSearchInputJSONObject.toString());
        mContext.startActivity(advancedSearchResultsIntent);
    }
}