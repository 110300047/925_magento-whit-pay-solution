package com.webkul.mobikul.handler;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.fragment.BillingShippingFragment;
import com.webkul.mobikul.fragment.DatePickerFragment;
import com.webkul.mobikul.fragment.PaymentMethodFragment;
import com.webkul.mobikul.fragment.ShippingMethodFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.checkout.BillingShippingInfoData;
import com.webkul.mobikul.model.checkout.ShippingMethodInfoData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.fragment.DatePickerFragment.BILLING_ADDRESS_DOB;
import static com.webkul.mobikul.fragment.DatePickerFragment.SHIPPING_ADDRESS_DOB;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created with passion and love by vedesh.kumar on 11/1/17. @Webkul Software Pvt. Ltd
 */

public class BillingShippingFragmentHandler {

    private final DatePickerFragment.OnDateSelected mOnDateSelected;
    private final BillingShippingFragment mBillingShippingFragment;
    private Context mContext;
    private boolean mIsVirtual = false;

    public BillingShippingFragmentHandler(Context context, BillingShippingFragment billingShippingFragment, DatePickerFragment.OnDateSelected onDateSelected) {
        mContext = context;
        mBillingShippingFragment = billingShippingFragment;
        mOnDateSelected = onDateSelected;
    }

    public void onClickContinue(@SuppressWarnings("UnusedParameters") View v, BillingShippingInfoData data) {
        data.setDisplayError(true);
        if (data.isFormValidated() && mBillingShippingFragment.isEmailChecked) {
            mIsVirtual = data.isIsVirtual();

            showDefaultAlertDialog(mContext);

            ApiConnection.getShippingMethodInfoData(
                    AppSharedPref.getStoreId(mContext)
                    , AppSharedPref.getQuoteId(mContext)
                    , AppSharedPref.getCustomerId(mContext)
                    , data.getCheckoutMethodName(mContext)
                    , data.getBillingJsonData()
                    , data.getShippingJsonData()
                    , AppSharedPref.getCurrencyCode(mContext))
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ShippingMethodInfoData>(mContext) {
                @Override
                public void onNext(ShippingMethodInfoData shippingMethodInfoData) {
                    super.onNext(shippingMethodInfoData);
                    if (shippingMethodInfoData.isSuccess()) {
                        FragmentManager fragmentManager = ((CheckoutActivity) mContext).getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        if (mIsVirtual) {
                            fragmentTransaction.add(R.id.container, PaymentMethodFragment.newInstance(shippingMethodInfoData), PaymentMethodFragment.class.getSimpleName());
                            fragmentTransaction.addToBackStack(PaymentMethodFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
                        } else {
                            fragmentTransaction.add(R.id.container, ShippingMethodFragment.newInstance(shippingMethodInfoData), ShippingMethodFragment.class.getSimpleName());
                            fragmentTransaction.addToBackStack(ShippingMethodFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
                        }
                        fragmentTransaction.commit();
                    } else {
                        showToast(mContext, shippingMethodInfoData.getMessage(), Toast.LENGTH_LONG, 0);
                        if (shippingMethodInfoData.getCartCount() == 0) {
                            ((CheckoutActivity) mContext).finish();
                        }
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        } else {
            if (mBillingShippingFragment.isEmailChecked) {
                Utils.hideKeyboard((CheckoutActivity) mContext);
                SnackbarHelper.getSnackbar((CheckoutActivity) mContext, mContext.getString(R.string.msg_fill_req_field)).show();
            } else {
                ToastHelper.showToast(mContext, mContext.getString(R.string.please_wait), Toast.LENGTH_SHORT, 0);
            }
        }
    }

    public void onClickPickBillingDobBtn(@SuppressWarnings("UnusedParameters") View view, String dobValue) {
        DialogFragment newFragment = DatePickerFragment.newInstance(mOnDateSelected, dobValue, BILLING_ADDRESS_DOB);
        newFragment.show(((CheckoutActivity) mContext).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void onClickPickShippingDobBtn(@SuppressWarnings("UnusedParameters") View view, String dobValue) {
        DialogFragment newFragment = DatePickerFragment.newInstance(mOnDateSelected, dobValue, SHIPPING_ADDRESS_DOB);
        newFragment.show(((CheckoutActivity) mContext).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void onClickUpdateAddressFromGps(boolean updateShippingAddressFromGps) {
        mBillingShippingFragment.fetchCurrentAddress(updateShippingAddressFromGps);
    }
}