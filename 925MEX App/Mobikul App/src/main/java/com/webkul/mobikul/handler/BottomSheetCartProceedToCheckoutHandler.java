package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;

import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.activity.LoginAndSignUpActivity;

import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_UP_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;

/**
 * Created by vedesh.kumar on 10/1/17. @Webkul Software Pvt. Ltd
 */

public class BottomSheetCartProceedToCheckoutHandler {

    private final Context mContext;
    private BottomSheetDialog mProceedToCheckoutBottomSheet;

    public BottomSheetCartProceedToCheckoutHandler(Context context, BottomSheetDialog proceedToCheckoutBottomSheet) {
        mContext = context;
        mProceedToCheckoutBottomSheet = proceedToCheckoutBottomSheet;
    }

    public void onClickLoginBtn(View view) {
        mProceedToCheckoutBottomSheet.dismiss();
        Intent intent = new Intent(view.getContext(), LoginAndSignUpActivity.class);
        intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_IN_PAGE);
        ((CartActivity) mContext).startActivityForResult(intent, SIGN_IN_PAGE);
    }

    public void onClickCheckoutAsGuestBtn(View view) {
        mProceedToCheckoutBottomSheet.dismiss();
        Intent intent = new Intent(view.getContext(), CheckoutActivity.class);
        view.getContext().startActivity(intent);
    }

    public void onClickRegisterAndCheckoutBtn(View view) {
        mProceedToCheckoutBottomSheet.dismiss();
        Intent intent = new Intent(view.getContext(), LoginAndSignUpActivity.class);
        intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_UP_PAGE);
        ((CartActivity) mContext).startActivityForResult(intent, SIGN_UP_PAGE);
    }
}