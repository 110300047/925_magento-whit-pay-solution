package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.adapter.CartItemsRecyclerAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.BottomSheetCartProceedToCheckoutBinding;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.EmptyFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.BaseModel;

import org.json.JSONArray;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_IS_VIRTUAL;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 29/12/16. @Webkul Software Pvt. Ltd
 */

public class CartActivityHandler {

    private final Context mContext;
    private JSONArray mItemIdList;
    private JSONArray mQtyList;
    private int mIsRemoveCoupon;
    private String mCouponCode;

    public CartActivityHandler(Context context) {
        mContext = context;
    }

    public void onClickUpdateCart(View view, JSONArray itemIdList, JSONArray qtyList) {
        mItemIdList = itemIdList;
        mQtyList = qtyList;

        showToast(mContext, mContext.getString(R.string.update_cart), Toast.LENGTH_LONG, R.drawable.ic_vector_refresh_white);

        ApiConnection.updateCart(
                AppSharedPref.getStoreId(view.getContext())
                , AppSharedPref.getQuoteId(view.getContext())
                , AppSharedPref.getCustomerId(view.getContext())
                , mItemIdList
                , mQtyList)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
            @Override
            public void onNext(BaseModel updateCartResponse) {
                super.onNext(updateCartResponse);
                if (updateCartResponse.isSuccess()) {
                    showToast(mContext, mContext.getString(R.string.message_bag_updated_suceessfully), Toast.LENGTH_LONG, 0);

                    ((CartActivity) mContext).updateCartBadge(updateCartResponse.getCartCount());
                    Intent intent = new Intent(mContext, CartActivity.class);
                    mContext.startActivity(intent);
                } else {
                    showToast(mContext, mContext.getString(R.string.something_went_wrong), Toast.LENGTH_LONG, 0);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    public void onClickEmptyCart(View view) {
        showAlertDialogWithClickListener(mContext, SweetAlertDialog.WARNING_TYPE, mContext.getString(R.string.warning_are_you_sure), mContext.getString(R.string.question_want_to_empty_cart), mContext.getString(R.string.yes), mContext.getString(R.string.no),
                new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        showDefaultAlertDialog(mContext);

                        ApiConnection.emptyCart(
                                AppSharedPref.getStoreId(mContext)
                                , AppSharedPref.getQuoteId(mContext)
                                , AppSharedPref.getCustomerId(mContext))
                                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
                            @Override
                            public void onNext(BaseModel emptyCartResponse) {
                                super.onNext(emptyCartResponse);
                                if (emptyCartResponse.isSuccess()) {
                                    ((CartActivity) mContext).mBinding.scrollView.setVisibility(GONE);
                                    ((CartActivity) mContext).updateCartBadge(0);
                                    final FragmentTransaction ft = ((CartActivity) mContext).getSupportFragmentManager().beginTransaction();
                                    ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_cart, mContext.getString(R.string.empty_bag)
                                            , mContext.getString(R.string.add_item_to_your_bag_now)), EmptyFragment.class.getSimpleName());
                                    ft.commit();
                                } else {
                                    showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.title_activity_cart), mContext.getString(R.string.something_went_wrong));
                                }
                            }

                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                            }
                        });
                    }
                }, null);
    }

    public void onClickContinueShopping(@SuppressWarnings("UnusedParameters") View view) {
        Intent intent = new Intent(mContext, ((MobikulApplication) view.getContext().getApplicationContext()).getHomePageClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
    }

    public void onClickApplyOrCancelCoupon(View view, String couponCode) {
        if (view.getId() == R.id.cancel_coupon_tv) {
            mIsRemoveCoupon = 1;
        } else {
            mIsRemoveCoupon = 0;
        }

        if (couponCode != null && !couponCode.trim().isEmpty()) {
            mCouponCode = couponCode;
            showDefaultAlertDialog(mContext);

            ApiConnection.applyOrCancelCoupon(
                    AppSharedPref.getStoreId(view.getContext())
                    , AppSharedPref.getQuoteId(view.getContext())
                    , AppSharedPref.getCustomerId(view.getContext())
                    , mCouponCode
                    , mIsRemoveCoupon)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
                @Override
                public void onNext(BaseModel applyOrCancelCouponResponse) {
                    super.onNext(applyOrCancelCouponResponse);
                    showToast(mContext, applyOrCancelCouponResponse.getMessage(), Toast.LENGTH_LONG, 0);
                    if (applyOrCancelCouponResponse.isSuccess()) {
                        ((CartActivity) mContext).updateCartBadge(applyOrCancelCouponResponse.getCartCount());
                        Intent intent = new Intent(mContext, CartActivity.class);
                        mContext.startActivity(intent);
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        }
    }

    public void onClickProceedToCheckout() {
        if (((CartItemsRecyclerAdapter) ((CartActivity) mContext).getBinding().productsRv.getAdapter()).isCartItemQtyChanged()) {
            showToast(mContext, mContext.getString(R.string.error_update_cart_to_proceed), Toast.LENGTH_SHORT, 0);
        } else if (AppSharedPref.isLoggedIn(mContext)) {
            FirebaseAnalyticsImpl.logBeginCheckoutEvent(mContext);

            Intent intent = new Intent(mContext, CheckoutActivity.class);
            intent.putExtra(BUNDLE_KEY_IS_VIRTUAL, ((CartActivity) mContext).mCartDetailsResponseData.getIsVirtual());
            mContext.startActivity(intent);
        } else {
            BottomSheetDialog proceedToCheckoutBottomSheet = new BottomSheetDialog(mContext);
            BottomSheetCartProceedToCheckoutBinding bottomSheetCartProceedToCheckoutBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext)
                    , R.layout.bottom_sheet_cart_proceed_to_checkout, null, false);

            if (((CartActivity) mContext).mCartDetailsResponseData.getAllowedGuestCheckout()) {
                if (((CartActivity) mContext).mCartDetailsResponseData.isContainsDownloadableProducts()) {
                    if (((CartActivity) mContext).mCartDetailsResponseData.getCanGuestCheckoutDownloadable()) {
                        bottomSheetCartProceedToCheckoutBinding.setShowGuestCheckout(true);
                    } else {
                        bottomSheetCartProceedToCheckoutBinding.setShowGuestCheckout(false);
                    }
                } else {
                    bottomSheetCartProceedToCheckoutBinding.setShowGuestCheckout(true);
                }
            } else {
                bottomSheetCartProceedToCheckoutBinding.setShowGuestCheckout(false);
            }

            bottomSheetCartProceedToCheckoutBinding.setHandler(new BottomSheetCartProceedToCheckoutHandler(mContext, proceedToCheckoutBottomSheet));
            proceedToCheckoutBottomSheet.setContentView(bottomSheetCartProceedToCheckoutBinding.getRoot());
            proceedToCheckoutBottomSheet.setCancelable(true);
            proceedToCheckoutBottomSheet.show();
        }
    }

    public void onClickVideoRewardBtn() {
    }
}