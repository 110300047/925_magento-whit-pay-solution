package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.adapter.CartItemsRecyclerAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.databinding.ItemCartBinding;
import com.webkul.mobikul.fragment.EmptyFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.cart.CartDetailsResponse;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_EDIT_PRODUCT;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ITEM_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_IMAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_NAME;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class CartItemsRecyclerHandler {

    private final Context mContext;
    private final CartItemsRecyclerAdapter mCartItemsRecyclerAdapter;
    private final ItemCartBinding itemBinding;
    private int mItemId;
    private String mProductName;
    private int mPosition = 0;

    public CartItemsRecyclerHandler(Context context, CartItemsRecyclerAdapter cartItemsRecyclerAdapter, ItemCartBinding itemCartBinding) {
        mContext = context;
        mCartItemsRecyclerAdapter = cartItemsRecyclerAdapter;
        itemBinding = itemCartBinding;
    }

    public void onClickMoveToWishlist(View view, int itemId, String productName, int position) {
        if (AppSharedPref.isLoggedIn(mContext)) {
            mItemId = itemId;
            mProductName = productName;
            mPosition = position;

            showDefaultAlertDialog(mContext);

            ApiConnection.moveToWishlist(
                    AppSharedPref.getStoreId(view.getContext())
                    , AppSharedPref.getCustomerId(view.getContext())
                    , itemId)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CartDetailsResponse>(mContext) {
                @Override
                public void onNext(CartDetailsResponse moveToWishlistResponse) {
                    super.onNext(moveToWishlistResponse);
                    if (moveToWishlistResponse.isSuccess()) {
                        if (((CartActivity) mContext).mCartDetailsResponseData.isCanProcceedToCheckout()) {
                            showToast(mContext, mProductName + " " + mContext.getString(R.string.has_been_moved_to_your_wishlist), Toast.LENGTH_SHORT, 0);

                            ((CartActivity) mContext).mCartItemsRecyclerAdapter.remove(mPosition);
                            ((CartActivity) mContext).mCartDetailsResponseData.setTax(moveToWishlistResponse.getTax());
                            ((CartActivity) mContext).mCartDetailsResponseData.setSubtotal(moveToWishlistResponse.getSubtotal());
                            ((CartActivity) mContext).mCartDetailsResponseData.setDiscount(moveToWishlistResponse.getDiscount());
                            ((CartActivity) mContext).mCartDetailsResponseData.setShipping(moveToWishlistResponse.getShipping());
                            ((CartActivity) mContext).mCartDetailsResponseData.setGrandTotal(moveToWishlistResponse.getGrandTotal());
                            ((CartActivity) mContext).mBinding.setData(((CartActivity) mContext).mCartDetailsResponseData);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ((CartActivity) mContext).mCartItemsRecyclerAdapter.notifyDataSetChanged();
                                }
                            }, 1000);

                            if (moveToWishlistResponse.getCartCount() == 0) {
                                ((CartActivity) mContext).mBinding.scrollView.setVisibility(GONE);
                                final FragmentTransaction ft = ((CartActivity) mContext).getSupportFragmentManager().beginTransaction();
                                ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_cart, mContext.getString(R.string.empty_bag)
                                        , mContext.getString(R.string.add_item_to_your_bag_now)), EmptyFragment.class.getSimpleName());
                                ft.commit();
                            }
                        } else {
                            mContext.startActivity(new Intent(mContext, CartActivity.class));
                        }
                    } else {
                        showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mProductName, moveToWishlistResponse.getMessage());
                    }
                    ((CartActivity) mContext).updateCartBadge(moveToWishlistResponse.getCartCount());
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        } else {
            showToast(mContext, mContext.getString(R.string.login_first), Toast.LENGTH_SHORT, 0);
        }
    }

    public void onClickIncrementQty(View view) {
        try {
            int currentPostion = (int) view.getTag();
            mCartItemsRecyclerAdapter.getCartItem(currentPostion).setQtyByArrowBtn(String.valueOf(Integer.parseInt((itemBinding.quantityEt).getText().toString()) + 1));
            mCartItemsRecyclerAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClickDecrementQty(View view) {
        try {
            int currentPostion = (int) view.getTag();
            mCartItemsRecyclerAdapter.getCartItem(currentPostion).setQtyByArrowBtn(String.valueOf(Integer.parseInt((itemBinding.quantityEt).getText().toString()) - 1));
            mCartItemsRecyclerAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClickDeleteItem(int itemId, String productName, int position) {
        mItemId = itemId;
        mProductName = productName;
        mPosition = position;

        showAlertDialogWithClickListener(mContext, SweetAlertDialog.WARNING_TYPE, mContext.getString(R.string.warning_are_you_sure), mContext.getString(R.string.question_want_to_delete_this_product), mContext.getString(R.string.message_yes_delete_it), mContext.getString(R.string.no),
                new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        showDefaultAlertDialog(mContext);

                        ApiConnection.removeCartItem(
                                AppSharedPref.getStoreId(mContext)
                                , AppSharedPref.getQuoteId(mContext)
                                , AppSharedPref.getCustomerId(mContext)
                                , mItemId)
                                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CartDetailsResponse>(mContext) {
                            @Override
                            public void onNext(CartDetailsResponse removeCartItemResponse) {
                                super.onNext(removeCartItemResponse);
                                ((BaseActivity) mContext).updateCartBadge(removeCartItemResponse.getCartCount());
                                if (removeCartItemResponse.isSuccess()) {
                                    if (((CartActivity) mContext).mCartDetailsResponseData.isCanProcceedToCheckout()) {
                                        showToast(mContext, mProductName + " " + mContext.getString(R.string.has_been_removed_from_cart), Toast.LENGTH_SHORT, 0);

                                        ((CartActivity) mContext).mCartItemsRecyclerAdapter.remove(mPosition);
                                        ((CartActivity) mContext).mCartDetailsResponseData.setTax(removeCartItemResponse.getTax());
                                        ((CartActivity) mContext).mCartDetailsResponseData.setSubtotal(removeCartItemResponse.getSubtotal());
                                        ((CartActivity) mContext).mCartDetailsResponseData.setDiscount(removeCartItemResponse.getDiscount());
                                        ((CartActivity) mContext).mCartDetailsResponseData.setShipping(removeCartItemResponse.getShipping());
                                        ((CartActivity) mContext).mCartDetailsResponseData.setGrandTotal(removeCartItemResponse.getGrandTotal());
                                        ((CartActivity) mContext).mBinding.setData(((CartActivity) mContext).mCartDetailsResponseData);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                ((CartActivity) mContext).mCartItemsRecyclerAdapter.notifyDataSetChanged();
                                            }
                                        }, 1000);

                                        if (removeCartItemResponse.getCartCount() == 0) {
                                            ((CartActivity) mContext).mBinding.scrollView.setVisibility(GONE);
                                            final FragmentTransaction ft = ((CartActivity) mContext).getSupportFragmentManager().beginTransaction();
                                            ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_vector_empty_cart, mContext.getString(R.string.empty_bag)
                                                    , mContext.getString(R.string.add_item_to_your_bag_now)), EmptyFragment.class.getSimpleName());
                                            ft.commit();
                                        }
                                    } else {
                                        mContext.startActivity(new Intent(mContext, CartActivity.class));
                                    }
                                } else {
                                    showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mProductName, removeCartItemResponse.getMessage());
                                }
                            }

                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                            }
                        });
                    }
                }, null);
    }

    public void onClickCartItem(View view, int productId, String productName, String image) {
        Intent intent = new Intent(mContext, NewProductActivity.class);
        intent.putExtra(BUNDLE_KEY_PRODUCT_ID, productId);
        intent.putExtra(BUNDLE_KEY_PRODUCT_NAME, productName);
        intent.putExtra(BUNDLE_KEY_PRODUCT_IMAGE, image);
        mContext.startActivity(intent);
    }

    public void onClickEditItem(int productId, String productName, int itemId, String image) {
        Intent intent = new Intent(mContext, NewProductActivity.class);
        intent.putExtra(BUNDLE_KEY_PRODUCT_ID, productId);
        intent.putExtra(BUNDLE_KEY_PRODUCT_NAME, productName);
        intent.putExtra(BUNDLE_KEY_PRODUCT_IMAGE, image);
        intent.putExtra(BUNDLE_KEY_ITEM_ID, itemId);
        intent.putExtra(BUNDLE_KEY_EDIT_PRODUCT, true);
        mContext.startActivity(intent);
    }
}