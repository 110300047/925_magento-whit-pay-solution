package com.webkul.mobikul.handler;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.adapter.CatalogAttributesSwatchRvAdapter;
import com.webkul.mobikul.fragment.ProductFragment;
import com.webkul.mobikul.fragment.ProductQuickViewDialogFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ImageHelper;
import com.webkul.mobikul.model.catalog.SwatchData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by vedesh.kumar on 25/7/17. @Webkul Software Private limited
 */

public class CatalogAttributesSwatchHandler {
    private final ProductQuickViewDialogFragment mProductQuickViewDialogFragmentContext;
    private final int mRecyclerViewTag;
    private final ArrayList<SwatchData> mCatalogAttributeSwatchValuesDatas;
    private ProductFragment mProductFragmentContext;
    private boolean mUpdateProductPreviewImage;
    private int mProductId = 0;
    private Double mProductPrice;

    public CatalogAttributesSwatchHandler(ProductQuickViewDialogFragment productQuickViewDialogFragmentContext, ProductFragment productFragmentContext, int recyclerViewTag, boolean updateProductPreviewImage, ArrayList<SwatchData> catalogAttributeSwatchValuesDatas) {
        mProductQuickViewDialogFragmentContext = productQuickViewDialogFragmentContext;
        mProductFragmentContext = productFragmentContext;
        mRecyclerViewTag = recyclerViewTag;
        mUpdateProductPreviewImage = updateProductPreviewImage;
        mCatalogAttributeSwatchValuesDatas = catalogAttributeSwatchValuesDatas;
    }

    public void onAttributeSelected(View view, int position) {
        RecyclerView recyclerView;
        setSelectedAttribute(position);
        if (mProductQuickViewDialogFragmentContext != null) {
            recyclerView = mProductQuickViewDialogFragmentContext.mBinding.swatchContainer.findViewWithTag(mRecyclerViewTag);
            recyclerView.getAdapter().notifyDataSetChanged();

            setProductIdAndPrice();
            if (mUpdateProductPreviewImage) {
                updateImageAndPrice();
            }
        } else {
            recyclerView = mProductFragmentContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + mRecyclerViewTag).findViewById(R.id.configurable_item_rv);
            recyclerView.getAdapter().notifyDataSetChanged();

            if ((mRecyclerViewTag + 1) < mProductFragmentContext.mCatalogSingleProductData.getConfigurableData().getAttributes().size()) {
                mProductFragmentContext.initializeConfigurableAttributeOption(mRecyclerViewTag + 1);
            }
            if (mUpdateProductPreviewImage) {
                mProductFragmentContext.updatePrice();
            }
        }
    }

    private void setSelectedAttribute(int position) {
        for (int noOfSwatchvalue = 0; noOfSwatchvalue < mCatalogAttributeSwatchValuesDatas.size(); noOfSwatchvalue++) {
            if (noOfSwatchvalue == position) {
                mCatalogAttributeSwatchValuesDatas.get(noOfSwatchvalue).setSelected(true);
            } else {
                mCatalogAttributeSwatchValuesDatas.get(noOfSwatchvalue).setSelected(false);
            }
        }
    }

    private void setProductIdAndPrice() {
        mProductQuickViewDialogFragmentContext.configOptionsId = new ArrayList<>();
        mProductQuickViewDialogFragmentContext.selectedConfigOptionsId = new ArrayList<>();
        ArrayList<String> selectedConfigOptionsId = new ArrayList<>();
        for (int noOfAttributes = 0; noOfAttributes < mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getAttributes().size(); noOfAttributes++) {
            mProductQuickViewDialogFragmentContext.configOptionsId.add(mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getAttributes().get(noOfAttributes).getId());
            if (mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getAttributes().get(noOfAttributes).getSwatchType().equals("visual") ||
                    mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getAttributes().get(noOfAttributes).getSwatchType().equals("text")) {
                RecyclerView recyclerView = mProductQuickViewDialogFragmentContext.mBinding.swatchContainer.findViewWithTag(noOfAttributes);
                ArrayList<SwatchData> swatchDatas = ((CatalogAttributesSwatchRvAdapter) recyclerView.getAdapter()).getSwatchValuesData();
                String id = swatchDatas.get(0).getId();
                for (int noOfItems = 0; noOfItems < swatchDatas.size(); noOfItems++) {
                    if (swatchDatas.get(noOfItems).isSelected()) {
                        id = swatchDatas.get(noOfItems).getId();
                        mProductQuickViewDialogFragmentContext.selectedConfigOptionsId.add(id);
                        break;
                    }
                }
                selectedConfigOptionsId.add(id);
            } else {
                selectedConfigOptionsId.add(mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getAttributes().get(noOfAttributes).getOptions().get(0).getId());
            }
        }

        try {
            JSONArray indexArray = new JSONArray(mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getIndex());
            boolean priceFound = false;
            for (int noOfCombinationData = 0; noOfCombinationData < indexArray.length(); noOfCombinationData++) {
                for (int noOfIndexItems = 0; noOfIndexItems < mProductQuickViewDialogFragmentContext.configOptionsId.size(); noOfIndexItems++) {
                    if (indexArray.getJSONObject(noOfCombinationData).getString(mProductQuickViewDialogFragmentContext.configOptionsId.get(noOfIndexItems)).equals(selectedConfigOptionsId.get(noOfIndexItems))) {
                        priceFound = true;
                    } else {
                        priceFound = false;
                        break;
                    }
                }
                if (priceFound) {
                    mProductId = mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getOptionPrices().get(noOfCombinationData).getProduct();
                    mProductPrice = mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getOptionPrices().get(noOfCombinationData).getFinalPrice().getAmount();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateImageAndPrice() {
        if (mProductId != 0) {
            try {
                JSONObject imagesJSON = new JSONObject(mProductQuickViewDialogFragmentContext.mProductData.getConfigurableData().getImages());
                JSONArray imageGalleryArray = imagesJSON.getJSONArray(String.valueOf(mProductId));

                if (imageGalleryArray != null) {
                    ImageHelper.load(mProductQuickViewDialogFragmentContext.mBinding.productImageIv, imageGalleryArray.getJSONObject(0).getString("img"), null);
                }

                String pattern = AppSharedPref.getPattern(mProductQuickViewDialogFragmentContext.getContext());
                int precision = AppSharedPref.getPrecision(mProductQuickViewDialogFragmentContext.getContext());
                String precisionFormat = "%." + precision + "f";
                String formatedPrice = String.format(Locale.US, precisionFormat, mProductPrice);
                String newFormattedPrice = pattern.replaceAll("%s", formatedPrice);

//                if (mProductQuickViewDialogFragmentContext.mProductData.hasSpecialPrice()) {
//                    mProductQuickViewDialogFragmentContext.mBinding.productSpecialPriceTv.setText(newFormattedPrice);
//                } else {
//                    mProductQuickViewDialogFragmentContext.mBinding.productPriceTv.setText(newFormattedPrice);
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}