package com.webkul.mobikul.handler;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.dialog.FilterByFragment;
import com.webkul.mobikul.dialog.SortByFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.catalog.LayeredData;
import com.webkul.mobikul.model.catalog.SortingData;

import java.util.ArrayList;
import java.util.List;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.ApplicationConstant.VIEW_TYPE_GRID;
import static com.webkul.mobikul.constants.ApplicationConstant.VIEW_TYPE_LIST;
import static com.webkul.mobikul.helper.ToastHelper.showToast;


/**
 * Created by vedesh.kumar on 29/12/16. @Webkul Software Pvt. Ltd
 */

public class CatalogProductActivityHandler {
    private Context mContext;

    public CatalogProductActivityHandler(Context context) {
        mContext = context;
    }

    public void onClickViewSwitcher(View view) {
        if (((CatalogProductActivity) mContext).getBinding().productCatalogRv.getAdapter().getItemViewType(0) == VIEW_TYPE_LIST) {

//            if (Utils.isTablet(mContext))
//                ((CatalogProductActivity) mContext).getBinding().productCatalogRv.setLayoutManager(new GridLayoutManager(mContext, 3));
//            else
            ((CatalogProductActivity) mContext).getBinding().productCatalogRv.setLayoutManager(new GridLayoutManager(mContext, 2));

            ((ImageView) view).setImageResource(R.drawable.ic_vector_list);
            AppSharedPref.setViewType(mContext, VIEW_TYPE_GRID);
        } else {
            ((CatalogProductActivity) mContext).getBinding().productCatalogRv.setLayoutManager(new LinearLayoutManager(mContext));
            ((ImageView) view).setImageResource(R.drawable.ic_vector_grid);
            AppSharedPref.setViewType(mContext, VIEW_TYPE_LIST);
        }
    }

    public void onClickSortBy(List<SortingData> sortingData) {
        if (sortingData == null || sortingData.size() == 0) {
            showToast(mContext, mContext.getString(R.string.msg_no_sort_data_available), Toast.LENGTH_SHORT, 0);
            return;
        }
        FragmentManager supportFragmentManager = ((BaseActivity) mContext).getSupportFragmentManager();
        SortByFragment sortByFragment = SortByFragment.newInstance((ArrayList<SortingData>) sortingData
                , ((CatalogProductActivity) mContext).getSortingInputJson());
        sortByFragment.show(supportFragmentManager, SortByFragment.class.getSimpleName());
    }

    public void onClickFilterBy(List<LayeredData> layeredData) {
        if (((CatalogProductActivity) mContext).getFilterInputJson().length() == 0 &&
                (layeredData == null || layeredData.size() == 0) &&
                ((CatalogProductActivity) mContext).mCategoryId == 0 &&
                (((CatalogProductActivity) mContext).mCatalogProductData.getCategoryList() == null || ((CatalogProductActivity) mContext).mCatalogProductData.getCategoryList().size() == 0)) {
            showToast(mContext, mContext.getString(R.string.msg_no_filter_option_available), Toast.LENGTH_SHORT, 0);
        } else {
            FragmentManager supportFragmentManager = ((CatalogProductActivity) mContext).getSupportFragmentManager();
            FilterByFragment filterByFragment = FilterByFragment.newInstance(
                    (ArrayList<LayeredData>) layeredData
                    , ((CatalogProductActivity) mContext).getFilterInputJson()
                    , ((CatalogProductActivity) mContext).mCatalogProductData.getCategoryList());
            FragmentTransaction transaction = supportFragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            transaction.add(R.id.main_container, filterByFragment, FilterByFragment.class.getSimpleName());
            transaction.addToBackStack(FilterByFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
            transaction.commit();
        }
    }
}