package com.webkul.mobikul.handler;

import android.support.v4.app.FragmentManager;
import android.view.View;

import com.webkul.mobikul.model.checkout.CheckoutActivityData;


/**
 * Created with passion and love by vedesh.kumar on 11/1/17. @Webkul Software Pvt. Ltd
 */

public class CheckoutActivityHandler {

    private final FragmentManager mFragmentManager;
    private boolean mIsVirtual = false;

    public CheckoutActivityHandler(FragmentManager fragmentManager, boolean isVirtual) {
        mIsVirtual = isVirtual;
        mFragmentManager = fragmentManager;
    }

    public void onClickBillingShippingInfoIv(@SuppressWarnings("UnusedParameters") View view, CheckoutActivityData data) {
        while (mFragmentManager.getBackStackEntryCount() != 1) {
            mFragmentManager.popBackStackImmediate();
        }
    }

    public void onClickShippingMethodIv(@SuppressWarnings("UnusedParameters") View view, CheckoutActivityData data) {
        while (mFragmentManager.getBackStackEntryCount() != 2) {
            mFragmentManager.popBackStackImmediate();
        }
    }

    public void onClickPaymentMethodIv(@SuppressWarnings("UnusedParameters") View view, CheckoutActivityData data) {
        if (mIsVirtual)
            while (mFragmentManager.getBackStackEntryCount() != 2) {
                mFragmentManager.popBackStackImmediate();
            }
        else
            while (mFragmentManager.getBackStackEntryCount() != 3) {
                mFragmentManager.popBackStackImmediate();
            }
    }
}
