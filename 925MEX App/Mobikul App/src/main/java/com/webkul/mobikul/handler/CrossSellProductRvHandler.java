package com.webkul.mobikul.handler;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.ProductQuickViewDialogFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.ProductData;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductAddToWishlistResponse;
import com.webkul.mobikul.model.product.ProductAddToCartResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECTED_PRODUCT_NUMBER;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.ProductHelper.getProductShortData;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 6/1/17. @Webkul Software Private limited
 */

public class CrossSellProductRvHandler {

    private CartActivity mContext;
    private ArrayList<ProductData> mProductsDatas;
    private int mProductId;
    private int mQty = 1;
    private String mProductName;
    private int mProductPosition;
    private boolean mIsProcessing;

    public CrossSellProductRvHandler(CartActivity context, ArrayList<ProductData> productsDatas) {
        mContext = context;
        mProductsDatas = productsDatas;
    }

    public void onClickOpenProduct(View view, int selectedProduct) {
        Intent intent = new Intent(view.getContext(), NewProductActivity.class);
        intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, getProductShortData(mProductsDatas));
        intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
        view.getContext().startActivity(intent);
    }

    public void onClickAddToWishlist(final View view, int productId, String productName, int productPosition, final int itemId) {
        if (!mIsProcessing) {
            if (AppSharedPref.isLoggedIn(mContext)) {
                mProductId = productId;
                mProductName = productName;
                mProductPosition = productPosition;

                if (mContext.mCartDetailsResponseData.getCrossSellList().get(mProductPosition).isInWishlist()) {
                    showAlertDialogWithClickListener(mContext, SweetAlertDialog.WARNING_TYPE, mContext.getString(R.string.warning_are_you_sure), mContext.getString(R.string.question_want_to_remove_this_product_from_whislist), mContext.getString(R.string.message_yes_remove_it),
                            mContext.getString(R.string.no), new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismiss();
                                    mIsProcessing = true;
                                    ((LottieAnimationView) view).reverseAnimation();
                                    ApiConnection.removeItemFromWishlist(AppSharedPref.getCustomerId(mContext)
                                            , AppSharedPref.getStoreId(mContext)
                                            , itemId)

                                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
                                        @Override
                                        public void onNext(BaseModel removeItemFromWishlistResponse) {
                                            super.onNext(removeItemFromWishlistResponse);
                                            mIsProcessing = false;
                                            if (removeItemFromWishlistResponse.isSuccess()) {
                                                mContext.mCartDetailsResponseData.getCrossSellList().get(mProductPosition).setInWishlist(false);
                                                mContext.mCartDetailsResponseData.getCrossSellList().get(mProductPosition).setWishlistItemId(0);
                                            } else {
                                                ((LottieAnimationView) view).cancelAnimation();
                                                ((LottieAnimationView) view).setProgress(1);
                                                showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.something_went_wrong), removeItemFromWishlistResponse.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable t) {
                                            super.onError(t);
                                            mIsProcessing = false;
                                            ((LottieAnimationView) view).cancelAnimation();
                                            ((LottieAnimationView) view).setProgress(1);
                                        }
                                    });
                                }
                            }, null);
                } else {
                    mIsProcessing = true;
                    ((LottieAnimationView) view).playAnimation();

                    FirebaseAnalyticsImpl.logAddToWishlistEvent(mContext, mProductId, mProductName);

                    ApiConnection.addToWishlist(AppSharedPref.getStoreId(mContext)
                            , mProductId
                            , AppSharedPref.getCustomerId(mContext)
                            , new JSONObject())

                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductAddToWishlistResponse>(mContext) {
                        @Override
                        public void onNext(CatalogProductAddToWishlistResponse addToWishlistResponse) {
                            super.onNext(addToWishlistResponse);
                            mIsProcessing = false;
                            if (addToWishlistResponse.isSuccess()) {
                                mContext.mCartDetailsResponseData.getCrossSellList().get(mProductPosition).setInWishlist(true);
                                mContext.mCartDetailsResponseData.getCrossSellList().get(mProductPosition).setWishlistItemId(addToWishlistResponse.getItemId());
                            } else {
                                ((LottieAnimationView) view).cancelAnimation();
                                ((LottieAnimationView) view).setProgress(0);
                                showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mProductName, addToWishlistResponse.getMessage());
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            mIsProcessing = false;
                            ((LottieAnimationView) view).cancelAnimation();
                            ((LottieAnimationView) view).setProgress(0);
                        }
                    });

                }
            } else {
                showToast(mContext, mContext.getString(R.string.login_first), Toast.LENGTH_SHORT, 0);
            }
        }
    }

    public void onClickAddToCompare(View view, int productId, String productName, int productPosition) {
        showDefaultAlertDialog(mContext);

        mProductId = productId;
        mProductName = productName;

        ApiConnection.addToCompare(AppSharedPref.getStoreId(mContext)
                , AppSharedPref.getCustomerId(view.getContext())
                , mProductId)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
            @Override
            public void onNext(BaseModel addToCompareResponse) {
                super.onNext(addToCompareResponse);
                showToast(mContext, addToCompareResponse.getMessage(), Toast.LENGTH_SHORT, 0);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    public void onClickItem(View view, int productId, String productName, boolean haveOptions, int selectedProduct) {
        if (haveOptions) {
            Intent intent = new Intent(view.getContext(), NewProductActivity.class);
            intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, mProductsDatas);
            intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
            view.getContext().startActivity(intent);
        } else {
            mProductName = productName;
            mProductId = productId;

            FirebaseAnalyticsImpl.logAddToCartEvent(mContext, mProductId, mProductName);

            showDefaultAlertDialog(mContext);

            ApiConnection.addToCart(AppSharedPref.getStoreId(mContext)
                    , mProductId
                    , mQty
                    , new JSONObject()
                    , new JSONArray()
                    , AppSharedPref.getQuoteId(view.getContext())
                    , AppSharedPref.getCustomerId(view.getContext()))

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ProductAddToCartResponse>(mContext) {
                @Override
                public void onNext(ProductAddToCartResponse productAddToCartResponse) {
                    super.onNext(productAddToCartResponse);
                    onResponseAddToCart(productAddToCartResponse);
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        }
    }

    private void onResponseAddToCart(ProductAddToCartResponse productAddToCartResponse) {
        if (productAddToCartResponse.getSuccess()) {
            showToast(mContext, productAddToCartResponse.getMessage(), Toast.LENGTH_LONG, 0);

            if (productAddToCartResponse.getQuoteId() != 0) {
                AppSharedPref.setQuoteId(mContext, productAddToCartResponse.getQuoteId());
            }
            mContext.updateCartBadge(productAddToCartResponse.getCartCount());
        } else {
            showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mProductName, productAddToCartResponse.getMessage());
        }
    }

    public void onClickConfigureProduct(View view, ProductData productData) {
        FragmentManager supportFragmentManager = mContext.getSupportFragmentManager();
        ProductQuickViewDialogFragment productQuickViewDialogFragment = ProductQuickViewDialogFragment.newInstance("crossSell", productData);
        productQuickViewDialogFragment.show(supportFragmentManager, ProductQuickViewDialogFragment.class.getSimpleName());
    }
}