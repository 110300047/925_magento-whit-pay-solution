package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.OrderDetailActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class CustomerOrderItemRecyclerHandler {

    public void onClickOrderDetail(View view, String incrementId, boolean canReorder) {
        Intent intent = new Intent(view.getContext(), OrderDetailActivity.class);
        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, incrementId);
//        intent.putExtra(BUNDLE_KEY_CAN_REORDER, canReorder);
        view.getContext().startActivity(intent);
    }
}