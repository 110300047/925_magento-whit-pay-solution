package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.NewProductActivity;

import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;


/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class CustomerReviewActivityHandler {

    public void onClickProductImage(View view, int productId, String productName) {
        Intent intent = new Intent(view.getContext(), NewProductActivity.class);
        intent.putExtra(KEY_PRODUCT_ID, productId);
        intent.putExtra(KEY_PRODUCT_NAME, productName);
        view.getContext().startActivity(intent);
    }
}