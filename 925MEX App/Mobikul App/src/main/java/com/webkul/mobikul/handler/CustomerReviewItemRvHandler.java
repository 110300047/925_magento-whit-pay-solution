package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.ReviewActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_REVIEW_ID;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class CustomerReviewItemRvHandler {

    public void onClickReviewItem(View view, int reviewId) {
        Intent intent = new Intent(view.getContext(), ReviewActivity.class);
        intent.putExtra(BUNDLE_KEY_REVIEW_ID, reviewId);
        view.getContext().startActivity(intent);
    }
}
