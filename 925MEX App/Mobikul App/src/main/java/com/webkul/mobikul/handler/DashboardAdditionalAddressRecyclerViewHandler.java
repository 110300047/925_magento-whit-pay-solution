package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.NewAddressActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.model.BaseModel;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ACTIVITY_TITLE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ADDRESS_ID;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;

/**
 * Created by vedesh.kumar on 5/1/17. @Webkul Software Pvt. Ltd
 */

public class DashboardAdditionalAddressRecyclerViewHandler {

    private final Context mContext;
    private final OnAdditionalAddressDeletedListener mOnAdditionalAddressDeletedListener;
    private int mAddressId;

    public DashboardAdditionalAddressRecyclerViewHandler(Context context, OnAdditionalAddressDeletedListener onAdditionalAddressDeletedListener) {
        mContext = context;
        mOnAdditionalAddressDeletedListener = onAdditionalAddressDeletedListener;
    }

    public void onClickEditAdditionalAddress(View view, int addressId) {
        Intent intent = new Intent(mContext, NewAddressActivity.class);
        intent.putExtra(BUNDLE_KEY_ADDRESS_ID, addressId);
        intent.putExtra(BUNDLE_KEY_ACTIVITY_TITLE, mContext.getString(R.string.title_activity_additional_address));
        mContext.startActivity(intent);
    }

    public void onClickDeleteAdditionalAddress(View view, int addressId) {
        mAddressId = addressId;
        showAlertDialogWithClickListener(mContext, SweetAlertDialog.WARNING_TYPE, mContext.getString(R.string.warning_are_you_sure), mContext.getString(R.string.question_want_to_delete_this_address), mContext.getString(R.string.message_yes_delete_it), mContext.getString(R.string.no),
                new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance
                        sDialog.dismiss();

                        ApiConnection.deleteAddress(
                                mAddressId)

                                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
                            @Override
                            public void onNext(BaseModel deleteAddress) {
                                super.onNext(deleteAddress);
                                if (deleteAddress.isSuccess()) {
                                    showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE, mContext.getString(R.string.message_deleted), deleteAddress.getMessage());
                                    mOnAdditionalAddressDeletedListener.onAdditionalAddressDeleted();
                                } else {
                                    showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.something_went_wrong), deleteAddress.getMessage());
                                }
                            }

                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                            }
                        });
                        showDefaultAlertDialog(mContext);
                    }
                }, null);
    }

    public interface OnAdditionalAddressDeletedListener {
        void onAdditionalAddressDeleted();
    }
}
