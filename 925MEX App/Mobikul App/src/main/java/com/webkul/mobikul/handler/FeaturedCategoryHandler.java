package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.helper.MobikulApplication;

import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;

/**
 * Created by vedesh.kumar on 6/1/17. @Webkul Software Private limited
 */

public class FeaturedCategoryHandler {

    public void onClickFeaturedCategory(View view, int categoryId, String categoryName) {
        Intent intent = new Intent(view.getContext(), ((MobikulApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass());
        intent.putExtra(KEY_CATEGORY_ID, categoryId);
        intent.putExtra(KEY_CATEGORY_NAME, categoryName);
        view.getContext().startActivity(intent);
    }
}
