package com.webkul.mobikul.handler;

import android.view.View;

import com.webkul.mobikul.dialog.FilterByFragment;

import org.json.JSONObject;

/**
 * Created by vedesh.kumar on 1/2/17. @Webkul Software Private limited
 */

public class FilterByFragmentHandler {

    private final FilterByFragment mContext;
    JSONObject mFilterJSONObject;

    public FilterByFragmentHandler(FilterByFragment filterByFragment) {
        mContext = filterByFragment;
    }

    public void onClickApplyFilter(View view) {
//        mContext.getDialog().dismiss();
        mContext.getActivity().onBackPressed();
    }
}