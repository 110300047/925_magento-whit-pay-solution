package com.webkul.mobikul.handler;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.customer.signin.SignInForgotPasswordResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */

public class ForgotPasswordDialogHandler {
    private final Context mContext;
    private final Dialog mDialog;

    public ForgotPasswordDialogHandler(Context context, Dialog dialog) {
        mContext = context;
        mDialog = dialog;
    }

    public void onClickForgotPasswordPositiveBtn(View view, String email) {
        if (!EMAIL_PATTERN.matcher(email).matches()) {
            showToast(mContext, mContext.getResources().getString(R.string.enter_valid_email), Toast.LENGTH_SHORT, 0);
            return;
        }

        showDefaultAlertDialog(mContext);

        ApiConnection.forgotSignInPassword(AppSharedPref.getStoreId(mContext)
                , email
                , ApplicationConstant.DEFAULT_WEBSITE_ID)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<SignInForgotPasswordResponse>(mContext) {
            @Override
            public void onNext(SignInForgotPasswordResponse signInForgotPasswordResponse) {
                super.onNext(signInForgotPasswordResponse);
                onResponseRecieved(signInForgotPasswordResponse);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mDialog.dismiss();
            }
        });
    }

    private void onResponseRecieved(SignInForgotPasswordResponse signInForgotPasswordResponse) {
        if (signInForgotPasswordResponse.isSuccess()) {
            showToast(mContext, mContext.getString(R.string.link_to_resend_login_password_send), Toast.LENGTH_LONG, 0);
            mDialog.dismiss();
        } else {
            showToast(mContext, signInForgotPasswordResponse.getMessage(), Toast.LENGTH_SHORT, 0);
        }
    }

    public void onClickForgotPasswordNegativeBtn() {
        mDialog.dismiss();
    }
}