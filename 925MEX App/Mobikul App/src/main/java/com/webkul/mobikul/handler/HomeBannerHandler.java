package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.helper.MobikulApplication;

import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;

/**
 * Created by vedesh.kumar on 27/12/16. @Webkul Software Pvt. Ltd
 */

public class HomeBannerHandler {
    public void onClickBanner(View view, String bannerType, int productId, String productName, int categoryId, String categoryName) {
        Intent intent = null;
        if (bannerType.equals("product")) {
            intent = new Intent(view.getContext(), NewProductActivity.class);
            intent.putExtra(KEY_PRODUCT_ID, productId);
            intent.putExtra(KEY_PRODUCT_NAME, productName);
        }

        if (bannerType.equals("category")) {
            intent = new Intent(view.getContext(), ((MobikulApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass());
            intent.putExtra(KEY_CATEGORY_ID, categoryId);
            intent.putExtra(KEY_CATEGORY_NAME, categoryName);
        }
        if (productId != 0 || categoryId != 0) {
            view.getContext().startActivity(intent);
        }
    }
}