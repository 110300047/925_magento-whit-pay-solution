package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.helper.MobikulApplication;

import java.util.Timer;
import java.util.TimerTask;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_COLLECTION_TYPE;

/**
 * Created by vedesh.kumar on 10/1/17. @Webkul Software Private limited
 */

public class HomePageHandler {

    public void onClickHomePageCollection(View view, String collectionType) {
        Intent intent = new Intent(view.getContext(), ((MobikulApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass());
        intent.putExtra(BUNDLE_KEY_COLLECTION_TYPE, collectionType);
        view.getContext().startActivity(intent);
    }

    public void onClickMoreButton(final View view) {
        int visibility = ((HomeActivity) view.getContext()).mBinding.navDrawerStartCmsRecyclerView.getVisibility();

        if (visibility == 0)
            ((HomeActivity) view.getContext()).mBinding.navDrawerStartCmsRecyclerView.setVisibility(View.GONE);
        else {
            ((HomeActivity) view.getContext()).mBinding.navDrawerStartCmsRecyclerView.setVisibility(View.VISIBLE);

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    ((HomeActivity) view.getContext()).mBinding.navStartScrollView.smoothScrollBy(0, ((HomeActivity) view.getContext()).mBinding.navDrawerStartCmsRecyclerView.getBottom());
                }
            }, 100);

            // Not using from Utils class because does not work for the first click
//            Utils.customScrollTo(((HomeActivity) view.getContext()).mBinding.navStartScrollView, 0, ((HomeActivity) view.getContext()).mBinding.navDrawerStartCmsRecyclerView.getBottom());
        }
    }
}