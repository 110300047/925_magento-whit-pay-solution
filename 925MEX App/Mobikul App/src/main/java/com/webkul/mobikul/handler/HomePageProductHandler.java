package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.activity.LoginAndSignUpActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.ProductQuickViewDialogFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.ProductData;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductAddToWishlistResponse;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductRemoveFromWishlistResponse;
import com.webkul.mobikul.model.product.ProductAddToCartResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.RC_APP_SIGN_IN;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_COLLECTION_TYPE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECTED_PRODUCT_NUMBER;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.ProductHelper.getProductShortData;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 6/1/17. @Webkul Software Private limited
 */

public class HomePageProductHandler {

    private Context mContext;
    private String mCollectionType;
    private ArrayList<ProductData> mProductsCollection;
    private boolean isSelected = false;

    private String mProductName;
    private int mProductId;
    private JSONObject mProductParamsJSON;
    private int mProductPosition;
    private int mQty = 1;

    private boolean mIsProcessing;

    public HomePageProductHandler(Context context, String collectionType, ArrayList<ProductData> productsDatas) {
        mContext = context;
        mCollectionType = collectionType;
        mProductsCollection = productsDatas;
    }

    public void onClickItem(View view, int productId, String productName, boolean haveOptions, int selectedProduct) {
        if (haveOptions) {
            Intent intent = new Intent(view.getContext(), NewProductActivity.class);
            intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, getProductShortData(mProductsCollection));
            intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
            view.getContext().startActivity(intent);
        } else {
            mProductName = productName;
            mProductId = productId;
            mProductParamsJSON = new JSONObject();

            FirebaseAnalyticsImpl.logAddToCartEvent(mContext, mProductId, mProductName);

            showDefaultAlertDialog(mContext);

            ApiConnection.addToCart(AppSharedPref.getStoreId(mContext)
                    , mProductId
                    , mQty
                    , mProductParamsJSON
                    , new JSONArray()
                    , AppSharedPref.getQuoteId(view.getContext())
                    , AppSharedPref.getCustomerId(view.getContext()))

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ProductAddToCartResponse>(mContext) {
                @Override
                public void onNext(ProductAddToCartResponse productAddToCartResponse) {
                    super.onNext(productAddToCartResponse);
                    onResponseAddToCart(productAddToCartResponse);
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        }
    }

    private void onResponseAddToCart(ProductAddToCartResponse productAddToCartResponse) {
        if (productAddToCartResponse.getSuccess()) {
            showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE, mContext.getString(R.string.added_to_Card), productAddToCartResponse.getMessage());

            if (productAddToCartResponse.getQuoteId() != 0) {
                AppSharedPref.setQuoteId(mContext, productAddToCartResponse.getQuoteId());
            }
            ((BaseActivity) mContext).updateCartBadge(productAddToCartResponse.getCartCount());
        } else {
            showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mProductName, productAddToCartResponse.getMessage());
        }
    }

    public void onClickAddToWishlist(final View view, int productId, String productName, int productPosition, final int itemId) {
        Log.d(ApplicationConstant.TAG, "onClickAddToWishlist: " + itemId);
        if (!mIsProcessing) {
            if (AppSharedPref.isLoggedIn(mContext)) {
                mProductId = productId;
                mProductName = productName;
                mProductPosition = productPosition;

                if (mProductsCollection.get(mProductPosition).isInWishlist()) {
                    showAlertDialogWithClickListener(mContext, SweetAlertDialog.WARNING_TYPE, mContext.getString(R.string.warning_are_you_sure), mContext.getString(R.string.question_want_to_remove_this_product_from_whislist), mContext.getString(R.string.message_yes_remove_it), mContext.getString(R.string.no)
                            , new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismiss();
                                    mIsProcessing = true;
                                    ((LottieAnimationView) view).reverseAnimation();
                                    mProductsCollection.get(mProductPosition).setInWishlist(false);
                                    ApiConnection.removeItemFromWishlist(AppSharedPref.getCustomerId(mContext)
                                            , AppSharedPref.getStoreId(mContext)
                                            , itemId)

                                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductRemoveFromWishlistResponse>(mContext) {
                                        @Override
                                        public void onNext(CatalogProductRemoveFromWishlistResponse removeItemFromWishlistResponse) {
                                            super.onNext(removeItemFromWishlistResponse);
                                            mIsProcessing = false;
                                            if (removeItemFromWishlistResponse.isSuccess() || removeItemFromWishlistResponse.getAlreadyDeleted()) {
                                                mProductsCollection.get(mProductPosition).setWishlistItemId(0);
                                            } else {
                                                mProductsCollection.get(mProductPosition).setInWishlist(true);
                                                ((LottieAnimationView) view).cancelAnimation();
                                                ((LottieAnimationView) view).setProgress(1);
                                                showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.something_went_wrong), removeItemFromWishlistResponse.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable t) {
                                            super.onError(t);
                                            mIsProcessing = false;
                                            mProductsCollection.get(mProductPosition).setInWishlist(true);
                                            ((LottieAnimationView) view).cancelAnimation();
                                            ((LottieAnimationView) view).setProgress(1);
                                        }
                                    });
                                }
                            }, null);
                } else {
                    mIsProcessing = true;
                    ((LottieAnimationView) view).playAnimation();

                    FirebaseAnalyticsImpl.logAddToWishlistEvent(mContext, mProductId, mProductName);
                    mProductsCollection.get(mProductPosition).setInWishlist(true);

                    ApiConnection.addToWishlist(AppSharedPref.getStoreId(mContext)
                            , mProductId
                            , AppSharedPref.getCustomerId(mContext)
                            , new JSONObject())

                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductAddToWishlistResponse>(mContext) {
                        @Override
                        public void onNext(CatalogProductAddToWishlistResponse addToWishlistResponse) {
                            super.onNext(addToWishlistResponse);
                            mIsProcessing = false;
                            if (addToWishlistResponse.isSuccess()) {
                                mProductsCollection.get(mProductPosition).setWishlistItemId(addToWishlistResponse.getItemId());
                            } else {
                                mProductsCollection.get(mProductPosition).setInWishlist(false);
                                ((LottieAnimationView) view).cancelAnimation();
                                ((LottieAnimationView) view).setProgress(0);
                                showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mProductName, addToWishlistResponse.getMessage());
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            mIsProcessing = false;
                            mProductsCollection.get(mProductPosition).setInWishlist(false);
                            ((LottieAnimationView) view).cancelAnimation();
                            ((LottieAnimationView) view).setProgress(0);
                        }
                    });
                }
            } else {
                if (mContext instanceof HomeActivity) {
                    showLoginAlertDialog(mContext.getString(R.string.login_first));
                } else {
                    ToastHelper.showToast(mContext, mContext.getString(R.string.login_first), Toast.LENGTH_LONG, 0);
                }
            }
        }
    }

    private void showLoginAlertDialog(String message) {
        showAlertDialogWithClickListener(mContext, SweetAlertDialog.NORMAL_TYPE, mContext.getResources().getString(R.string.login), message, mContext.getResources().getString(R.string.login), mContext.getResources().getString(R.string.dialog_cancel)
                , new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(mContext, LoginAndSignUpActivity.class);
                        intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_IN_PAGE);
                        ((HomeActivity) mContext).startActivityForResult(intent, RC_APP_SIGN_IN);
                        sweetAlertDialog.dismissWithAnimation();
                    }
                }, null);
    }

    public void onClickAddToCompare(View view, int productId, String productName, int productPosition) {
        showDefaultAlertDialog(mContext);

        mProductId = productId;
        mProductName = productName;

        ApiConnection.addToCompare(AppSharedPref.getStoreId(mContext)
                , AppSharedPref.getCustomerId(view.getContext())
                , mProductId)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
            @Override
            public void onNext(BaseModel addToCompareResponse) {
                super.onNext(addToCompareResponse);
                onResponseAddToCompare(addToCompareResponse);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    private void onResponseAddToCompare(BaseModel addToCompareResponse) {
        showToast(mContext, addToCompareResponse.getMessage(), Toast.LENGTH_SHORT, 0);
    }

    public void onClickConfigureProduct(View view, ProductData productData) {
        if (!isSelected) {
            isSelected = true;
            FragmentManager supportFragmentManager = ((BaseActivity) mContext).getSupportFragmentManager();
            ProductQuickViewDialogFragment productQuickViewDialogFragment = ProductQuickViewDialogFragment.newInstance(mCollectionType, productData);
            productQuickViewDialogFragment.show(supportFragmentManager, ProductQuickViewDialogFragment.class.getSimpleName());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isSelected = false;
                }
            }, 1000);
        }
    }

    public void onClickViewAll(View view) {
        Intent intent = new Intent(view.getContext(), ((MobikulApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass());
        intent.putExtra(BUNDLE_KEY_COLLECTION_TYPE, mCollectionType);
        view.getContext().startActivity(intent);
    }
}