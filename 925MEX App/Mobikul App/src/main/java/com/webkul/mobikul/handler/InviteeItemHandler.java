package com.webkul.mobikul.handler;

import android.view.View;

import com.webkul.mobikul.fragment.ProductEmailFragment;

/**
 * Created by vedesh.kumar on 12/7/17. @Webkul Software Private limited
 */

public class InviteeItemHandler {

    private ProductEmailFragment mContext;

    public InviteeItemHandler(ProductEmailFragment mContext) {

        this.mContext = mContext;
    }

    public void onClickDeleteItemBtn(@SuppressWarnings("UnusedParameters") View view, int position) {
        mContext.productEmailData.getInviteeItemsList().remove(position);
        mContext.mInviteeRvAdapter.notifyDataSetChanged();
    }
}