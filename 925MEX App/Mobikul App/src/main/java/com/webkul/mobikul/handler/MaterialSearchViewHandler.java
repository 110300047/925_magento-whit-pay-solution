package com.webkul.mobikul.handler;

import android.app.Activity;

import com.webkul.mobikul.customviews.MaterialSearchView;
import com.webkul.mobikul.helper.Utils;


/**
 * Created by vedesh.kumar on 28/2/17. @Webkul Software Pvt. Ltd
 */

public class MaterialSearchViewHandler {
    @SuppressWarnings("unused")
    private static final String TAG = "MaterialSearchViewHandl";

    private MaterialSearchView mMaterialSearchView;

    public MaterialSearchViewHandler(MaterialSearchView materialSearchView) {
        mMaterialSearchView = materialSearchView;
    }

    public void backPressed() {
        mMaterialSearchView.closeSearch();
        Utils.hideKeyboard((Activity) mMaterialSearchView.getContext());
    }

    public void onVoiceClicked() {
        mMaterialSearchView.onVoiceClicked();
    }

    public void clearSearch() {
        mMaterialSearchView.mBinding.etSearch.setText("");
    }
}
