package com.webkul.mobikul.handler;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.OrderDetailActivity;
import com.webkul.mobikul.adapter.MyDownloadsRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.DownloadHelper;
import com.webkul.mobikul.model.customer.downloadable.DownloadProductResponse;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.RC_WRITE_TO_EXTERNAL_STORAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CAN_REORDER;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;

/**
 * Created by vedesh.kumar on 27/1/17. @Webkul Software Private limited
 */

public class MyDownloadableListItemHandler {

    public static String mUrl;
    public static String mFileName;
    public static String mMimeType;
    private final Context mContext;

    public MyDownloadableListItemHandler(Context context, MyDownloadsRvAdapter myDownloadListRvAdapter) {
        mContext = context;
    }

    public void onClickDonwloadableOrder(View view, String incrementId, Boolean canReorder) {
        Intent intent = new Intent(view.getContext(), OrderDetailActivity.class);
        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, incrementId);
        intent.putExtra(BUNDLE_KEY_CAN_REORDER, canReorder);
        view.getContext().startActivity(intent);
    }

    public void onClickDownloadbtn(View view, String hash) {
        showDefaultAlertDialog(mContext);

        ApiConnection.downloadProduct(
                AppSharedPref.getCustomerId(mContext)
                , hash)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<DownloadProductResponse>(mContext) {
            @Override
            public void onNext(DownloadProductResponse downloadProductResponse) {
                super.onNext(downloadProductResponse);
                if (downloadProductResponse.isSuccess()) {
                    mUrl = downloadProductResponse.getUrl();
                    mFileName = downloadProductResponse.getFileName();
                    mMimeType = downloadProductResponse.getMimeType();

                    if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                            ((Activity) mContext).requestPermissions(permissions, RC_WRITE_TO_EXTERNAL_STORAGE);
                        }
                    } else {
                        DownloadHelper.downloadFile(mContext, mUrl, mFileName, mMimeType);
                    }
                } else {
                    showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.something_went_wrong), downloadProductResponse.getMessage());
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }
}