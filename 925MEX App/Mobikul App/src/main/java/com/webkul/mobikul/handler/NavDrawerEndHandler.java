package com.webkul.mobikul.handler;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.view.View;

import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.activity.LoginAndSignUpActivity;
import com.webkul.mobikul.fragment.LoggedoutLayoutFragment;
import com.webkul.mobikul.helper.MobikulApplication;

import static android.app.Activity.RESULT_OK;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_APP_SIGN_IN;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_APP_SIGN_UP;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_UP_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;

/**
 * Created by vedesh.kumar on 6/1/17. @Webkul Software Pvt. Ltd
 */

public class NavDrawerEndHandler {

    private LoggedoutLayoutFragment mLoggedoutLayoutFragment;

    public NavDrawerEndHandler(LoggedoutLayoutFragment loggedoutLayoutFragment) {
        mLoggedoutLayoutFragment = loggedoutLayoutFragment;
    }

    public void onClickSignIn(View view) {
        ((HomeActivity) mLoggedoutLayoutFragment.getContext()).mBinding.drawerLayout.closeDrawer(GravityCompat.END);

        Intent intent = new Intent(view.getContext(), LoginAndSignUpActivity.class);
        intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_IN_PAGE);
        mLoggedoutLayoutFragment.startActivityForResult(intent, RC_APP_SIGN_IN);
    }

    public void onClickSignUp(View view) {
        ((HomeActivity) mLoggedoutLayoutFragment.getContext()).mBinding.drawerLayout.closeDrawer(GravityCompat.END);

        Intent intent = new Intent(view.getContext(), LoginAndSignUpActivity.class);
        intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_UP_PAGE);
        mLoggedoutLayoutFragment.startActivityForResult(intent, RC_APP_SIGN_UP);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_APP_SIGN_IN || requestCode == RC_APP_SIGN_UP) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(mLoggedoutLayoutFragment.getContext(), ((MobikulApplication) mLoggedoutLayoutFragment.getContext().getApplicationContext()).getHomePageClass());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mLoggedoutLayoutFragment.getContext().startActivity(intent);
            }
        }
    }
}