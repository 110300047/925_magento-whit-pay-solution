package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.AccountInfoActivity;
import com.webkul.mobikul.activity.CustomerDashboardActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.activity.MyDownloadableProductsActivity;
import com.webkul.mobikul.activity.NotificationActivity;
import com.webkul.mobikul.activity.WishlistActivty;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.NavDrawerCustomerItem;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.ToastHelper.showToast;
import static com.webkul.mobikul.model.catalog.NavDrawerCustomerItem.TYPE_SELLER_STATUS;
import static com.webkul.mobikul.model.catalog.NavDrawerCustomerItem.TYPE_SIGN_IN;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class NavDrawerEndRvHandler {

    private Context mContext;

    public NavDrawerEndRvHandler(Context context) {
        mContext = context;
    }

    public void onClickItem(int itemType) {
        if (itemType == TYPE_SIGN_IN) {
            showAlertDialogWithClickListener(mContext, SweetAlertDialog.WARNING_TYPE, mContext.getString(R.string.warning_are_you_sure), mContext.getString(R.string.logout_warning)
                    , mContext.getResources().getString(android.R.string.ok), mContext.getResources().getString(R.string.no), new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            if (AppSharedPref.isLoggedIn(mContext)) {
                                ApiConnection.logout(
                                        FirebaseInstanceId.getInstance().getToken()
                                        , AppSharedPref.getCustomerId(mContext))
                                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
                                    @Override
                                    public void onNext(BaseModel saveReviewResponse) {
                                        super.onNext(saveReviewResponse);
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        super.onError(t);
                                    }
                                });
                                SharedPreferences.Editor customerSharedPrefEditor = AppSharedPref.getSharedPreferenceEditor(mContext, CUSTOMER_PREF);
                                customerSharedPrefEditor.clear();
                                customerSharedPrefEditor.apply();
                                showToast(mContext, mContext.getString(R.string.logout_message), Toast.LENGTH_SHORT, 0);
                                Intent intent = new Intent(mContext, ((MobikulApplication) mContext.getApplicationContext()).getHomePageClass());
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                mContext.startActivity(intent);
                            }
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    }, null);
        } else if (itemType == NavDrawerCustomerItem.TYPE_ACCOUNT_INFO) {
            mContext.startActivity(new Intent(mContext, AccountInfoActivity.class));
        } else if (itemType == NavDrawerCustomerItem.TYPE_DASHBOARD) {
            mContext.startActivity(new Intent(mContext, CustomerDashboardActivity.class));
        } else if (itemType == NavDrawerCustomerItem.TYPE_WISHLIST) {
            mContext.startActivity(new Intent(mContext, WishlistActivty.class));
        } else if (itemType == NavDrawerCustomerItem.TYPE_MY_DOWNLOADABLE_PRODUCTS) {
            mContext.startActivity(new Intent(mContext, MyDownloadableProductsActivity.class));
        } else if (itemType == NavDrawerCustomerItem.TYPE_NOTIFICATION) {
            mContext.startActivity(new Intent(mContext, NotificationActivity.class));
        } else if (itemType == NavDrawerCustomerItem.TYPE_SELLER_DASHBOARD) {
            ((MobikulApplication) mContext.getApplicationContext()).callSellerDashBoardActivity(mContext);
        } else if (itemType == NavDrawerCustomerItem.TYPE_SELLER_PROFILE) {
            ((MobikulApplication) mContext.getApplicationContext()).callSellerProfileActivity(mContext);
        } else if (itemType == NavDrawerCustomerItem.TYPE_SELLER_ORDER) {
            ((MobikulApplication) mContext.getApplicationContext()).callSellerOrdersActivity(mContext);
        } else if (itemType == NavDrawerCustomerItem.TYPE_SELLER_NEW_PRODUCT) {
            ((MobikulApplication) mContext.getApplicationContext()).callSellerNewProductActivity(mContext);
        } else if (itemType == NavDrawerCustomerItem.TYPE_SELLER_PRODUCTS_LIST) {
            ((MobikulApplication) mContext.getApplicationContext()).callSellerProductsListActivity(mContext);
        } else if (itemType == NavDrawerCustomerItem.TYPE_SELLER_TRANSACTIONS_LIST) {
            ((MobikulApplication) mContext.getApplicationContext()).callSellerTransactionsListActivity(mContext);
        } else if (itemType == NavDrawerCustomerItem.TYPE_MANAGE_PRINT_PDF_HEADER) {
            ((MobikulApplication) mContext.getApplicationContext()).callManagePrintPdfHeaderActivity(mContext);
        } else if (itemType == NavDrawerCustomerItem.TYPE_ASK_QUESTION_TO_ADMIN) {
            ((MobikulApplication) mContext.getApplicationContext()).loadAskQuestionToAdminDialogFragment(mContext);
        } else if (itemType == NavDrawerCustomerItem.TYPE_CHAT) {
            ((MobikulApplication) mContext.getApplicationContext()).callChatRelatedActivity(mContext);
        } else if (itemType == TYPE_SELLER_STATUS) {
            ((MobikulApplication) mContext.getApplicationContext()).callSellerStatusActivity(mContext);
        }
        ((HomeActivity) mContext).mBinding.drawerLayout.closeDrawers();
    }
}