package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.helper.MobikulApplication;

import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;

/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */

public class NavDrawerStartCategoryHandler {

    private final Context mContext;
    private int childPosition;

    public NavDrawerStartCategoryHandler(Context mContext, int childPosition) {
        this.mContext = mContext;
        this.childPosition = childPosition;
    }

    public void onClickCategoryItem(View view, int categoryId, String categoryName) {
        Intent intent = new Intent(view.getContext(), ((MobikulApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass());
        intent.putExtra(KEY_CATEGORY_ID, categoryId);

        if (childPosition == 0)
            categoryName = categoryName.replace(mContext.getString(R.string.all) + " ", "");

        intent.putExtra(KEY_CATEGORY_NAME, categoryName);
        view.getContext().startActivity(intent);
        ((HomeActivity) view.getContext()).mBinding.drawerLayout.closeDrawers();
    }
}