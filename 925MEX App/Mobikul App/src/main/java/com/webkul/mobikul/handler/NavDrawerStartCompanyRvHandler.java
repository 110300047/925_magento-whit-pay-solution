package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.CMSPageDetailsActivity;
import com.webkul.mobikul.activity.HomeActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_IDENTIFIER;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Private limited
 */

public class NavDrawerStartCompanyRvHandler {

    private final Context mContext;

    public NavDrawerStartCompanyRvHandler(Context context) {
        mContext = context;
    }

    public void onClickCompanyItem(View v, int identifier) {
        Intent intent = new Intent(v.getContext(), CMSPageDetailsActivity.class);
        intent.putExtra(BUNDLE_KEY_IDENTIFIER, identifier);
        v.getContext().startActivity(intent);
        ((HomeActivity) v.getContext()).mBinding.drawerLayout.closeDrawers();
    }
}