package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.activity.SplashScreenActivity;
import com.webkul.mobikul.helper.AppSharedPref;

/**
 * Created by vedesh.kumar on 14/1/17. @Webkul Software Pvt. Ltd
 */

public class NavDrawerStartCurrencyHandler {

    public void onClickCurrencyItem(View view, String currencyCode) {
        if (!AppSharedPref.getCurrencyCode(view.getContext()).equals(currencyCode)) {
            AppSharedPref.setCurrencyCode(view.getContext(), currencyCode);
            ((HomeActivity) view.getContext()).mBinding.drawerLayout.closeDrawers();
            Intent intent = new Intent(view.getContext(), SplashScreenActivity.class);
            view.getContext().startActivity(intent);
        }
    }
}