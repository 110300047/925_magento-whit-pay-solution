package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CompareProductsActivity;
import com.webkul.mobikul.activity.ContactUsActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.activity.OrdersAndReturnsActivity;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.catalog.NavDrawerStartExtrasItem;

import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class NavDrawerStartExtrasItemHandler {

    private Context mContext;

    public NavDrawerStartExtrasItemHandler(Context context) {
        mContext = context;
    }

    public void onClickItem(int itemType) {
        if (itemType == NavDrawerStartExtrasItem.TYPE_COMPARE_PRODUCT) {
            mContext.startActivity(new Intent(mContext, CompareProductsActivity.class));
        } else if (itemType == NavDrawerStartExtrasItem.TYPE_MARKETPLACE) {
            mContext.startActivity(new Intent(mContext, ((MobikulApplication) mContext.getApplicationContext()).getMarketplaceLandingPageClass()));
        } else if (itemType == NavDrawerStartExtrasItem.TYPE_CONTACT_US) {
            mContext.startActivity(new Intent(mContext, ContactUsActivity.class));
        } else if (itemType == NavDrawerStartExtrasItem.TYPE_ORDERS_AND_RETURNS) {
            if (AppSharedPref.isLoggedIn(mContext)) {
                showToast(mContext, mContext.getString(R.string.order_and_retures_error_msg), Toast.LENGTH_LONG, 0);
            } else {
                mContext.startActivity(new Intent(mContext, OrdersAndReturnsActivity.class));
            }
        } else if (itemType == NavDrawerStartExtrasItem.TYPE_CURRENCY) {
            if (((HomeActivity) mContext).mBinding.navDrawerStartCurrencyRecyclerView.getVisibility() == View.VISIBLE) {
                ((HomeActivity) mContext).mBinding.navDrawerStartCurrencyRecyclerView.setVisibility(View.GONE);
                if (AppSharedPref.getStoreCode(mContext).equals("ar")) {
                    ((HomeActivity) mContext).mBinding.currencyItem.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_vector_side_arrow_arabic_wrapper, 0, 0, 0);
                } else {
                    ((HomeActivity) mContext).mBinding.currencyItem.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_side_arrow_wrapper, 0);
                }
            } else {
                ((HomeActivity) mContext).mBinding.navDrawerStartCurrencyRecyclerView.setVisibility(View.VISIBLE);
                if (AppSharedPref.getStoreCode(mContext).equals("ar")) {
                    ((HomeActivity) mContext).mBinding.currencyItem.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_vector_down_arrow_wrapper, 0, 0, 0);
                } else {
                    ((HomeActivity) mContext).mBinding.currencyItem.navDrawerParentTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_down_arrow_wrapper, 0);
                }
            }
            return;
        }
        ((HomeActivity) mContext).mBinding.drawerLayout.closeDrawers();
    }
}