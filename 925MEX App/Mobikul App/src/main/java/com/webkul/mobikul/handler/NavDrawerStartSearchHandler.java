package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.AdvanceSearchActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.activity.SearchTermsActivity;

import static com.webkul.mobikul.model.search.NavDrawerStartSearchMenuData.ADVANCE_SEARCH;
import static com.webkul.mobikul.model.search.NavDrawerStartSearchMenuData.SEARCH_TERMS;


/**
 * Created by vedesh.kumar on 31/1/17. @Webkul Software Private limited
 */

public class NavDrawerStartSearchHandler {

    private final Context mContext;

    public NavDrawerStartSearchHandler(Context context) {
        mContext = context;
    }

    public void onClickSearchItem(View v, int itemType) {
        if (itemType == SEARCH_TERMS) {
            mContext.startActivity(new Intent(mContext, SearchTermsActivity.class));
        } else if (itemType == ADVANCE_SEARCH) {
            mContext.startActivity(new Intent(mContext, AdvanceSearchActivity.class));
        }
        ((HomeActivity) v.getContext()).mBinding.drawerLayout.closeDrawers();
    }
}
