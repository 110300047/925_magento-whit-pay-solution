package com.webkul.mobikul.handler;

import android.util.Log;
import android.view.View;

import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.LocaleUtils;

//import org.apache.commons.io.FileUtils;

/**
 * Created by vedesh.kumar on 14/1/17. @Webkul Software Pvt. Ltd
 */

public class NavDrawerStartStoreViewHandler {
    public void onClickStoreView(View view, String languageCode, int storeId) {
        Log.d(ApplicationConstant.TAG, "NavDrawerStartStoreViewHandler onClickStoreView storeId: " + storeId);
        if (AppSharedPref.getStoreId(view.getContext()) != storeId) {
            AppSharedPref.setStoreCode(view.getContext(), languageCode);
            AppSharedPref.setStoreId(view.getContext(), storeId);
            LocaleUtils.changeLanguange(view.getContext());
            ((HomeActivity) view.getContext()).mBinding.drawerLayout.closeDrawers();
        }
    }
}