package com.webkul.mobikul.handler;

import android.content.Context;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.NewAddressActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.customer.address.AddressFormResponseData;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Pvt. Ltd
 */
public class NewAddressActivityHandler {
    private final Context mContext;

    public NewAddressActivityHandler(Context context) {
        mContext = context;
    }

    public void onClickGetLocation() {
        ((NewAddressActivity) mContext).fetchCurrentAddress();
    }

    public void onClickSaveAddress(@SuppressWarnings("UnusedParameters") View view, AddressFormResponseData addressFormResponseData) {
        if (addressFormResponseData.isFormValidated((NewAddressActivity) mContext)) {
            JSONObject mNewAddreesJsonData = addressFormResponseData.getNewAddressData(mContext);
            if (mNewAddreesJsonData != null) {
                showDefaultAlertDialog(mContext);
                ApiConnection.saveAddress(
                        AppSharedPref.getStoreId(mContext)
                        , AppSharedPref.getCustomerId(mContext)
                        , ((NewAddressActivity) mContext).getAddressId()
                        , mNewAddreesJsonData)

                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
                    @Override
                    public void onNext(BaseModel saveAddressResponseData) {
                        super.onNext(saveAddressResponseData);
                        if (saveAddressResponseData.isSuccess()) {
                            showAlertDialogWithClickListener(mContext, SweetAlertDialog.SUCCESS_TYPE, mContext.getString(R.string.message_address_saved), saveAddressResponseData.getMessage(), "", "", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    ((NewAddressActivity) mContext).finish();
                                }
                            }, null);
                        } else {
                            showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.error), saveAddressResponseData.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                    }
                });
            } else {
                Utils.hideKeyboard((NewAddressActivity) mContext);
                SnackbarHelper.getSnackbar((NewAddressActivity) mContext, mContext.getString(R.string.msg_fill_req_field)).show();
            }
        }
    }
}