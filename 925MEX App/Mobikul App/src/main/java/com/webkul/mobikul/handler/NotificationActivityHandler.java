package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.activity.OtherNotificationActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.DataBaseHelper;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.notification.NotificationList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_NOTIFICATION_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Private limited
 */

public class NotificationActivityHandler {

    private final Context mContext;

    public NotificationActivityHandler(Context context) {
        mContext = context;
    }

    public void onClickNotification(View view, NotificationList listData) {

        DataBaseHelper db = new DataBaseHelper(mContext);
        db.deleteNotification(String.valueOf(listData.getId()));
        db.close();
        Utils.setNotificationBadge(mContext);

        Intent intent;
        switch (listData.getNotificationType()) {
            case ("product"):
                intent = new Intent(mContext, NewProductActivity.class);
                intent.putExtra(KEY_PRODUCT_ID, listData.getProductId());
                intent.putExtra(KEY_PRODUCT_NAME, listData.getProductName());
                mContext.startActivity(intent);
                break;
            case ("category"):
                intent = new Intent(mContext, ((MobikulApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass());
                intent.putExtra(KEY_CATEGORY_ID, listData.getCategoryId());
                intent.putExtra(KEY_CATEGORY_NAME, listData.getCategoryName());
                Log.d(ApplicationConstant.TAG, "onClickNotification: " + listData.getCategoryId());
                Log.d(ApplicationConstant.TAG, "onClickNotification: " + listData.getCategoryName());
                mContext.startActivity(intent);
                break;
            case ("others"):
                intent = new Intent(mContext, OtherNotificationActivity.class);
                intent.putExtra(BUNDLE_KEY_NOTIFICATION_ID, listData.getId());
                mContext.startActivity(intent);
                break;
            case ("custom"):
                intent = new Intent(mContext, ((MobikulApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass());
                intent.putExtra(BUNDLE_KEY_NOTIFICATION_ID, listData.getId());
                mContext.startActivity(intent);
                break;
            default:
                break;
        }
    }
}