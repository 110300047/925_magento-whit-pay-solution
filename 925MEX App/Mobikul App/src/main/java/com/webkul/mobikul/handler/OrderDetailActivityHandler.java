package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.customer.order.ReorderResponseData;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class OrderDetailActivityHandler {

    private final Context mContext;
    private String mIncrementId;

    public OrderDetailActivityHandler(Context context) {
        mContext = context;
    }

    public void onClickOrderAgainBtn(View view, String incrementId) {
        mIncrementId = incrementId;
        showDefaultAlertDialog(mContext);

        ApiConnection.reorder(AppSharedPref.getCustomerId(view.getContext())
                , mIncrementId
                , AppSharedPref.getStoreId(mContext))

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ReorderResponseData>(mContext) {
            @Override
            public void onNext(ReorderResponseData reorderResponseData) {
                super.onNext(reorderResponseData);
                if (reorderResponseData.isSuccess()) {
                    showAlertDialogWithClickListener(mContext, SweetAlertDialog.NORMAL_TYPE, String.format(mContext.getString(R.string.title_activity_order_noX), mIncrementId), reorderResponseData.getMessage(), "", "", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            Intent intent = new Intent(mContext, CartActivity.class);
                            mContext.startActivity(intent);
                            ((BaseActivity) mContext).finish();
                        }
                    }, null);
                } else {
                    showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, String.format(mContext.getString(R.string.title_activity_order_noX), mIncrementId), reorderResponseData.getMessage());
                }

                ((BaseActivity) mContext).updateCartBadge(reorderResponseData.getCartCount());
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }
}
