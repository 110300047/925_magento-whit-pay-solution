package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.activity.OrderDetailActivity;
import com.webkul.mobikul.activity.OrderPlacedActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.BaseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CAN_REORDER;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created with passion and love by vedesh.kumar on 10/7/17. @Webkul Software Pvt. Ltd
 */

public class OrderPlaceActivityHandler {

    public void onClickStartShopping(View view) {
        Intent intent = new Intent(view.getContext(), ((MobikulApplication) view.getContext().getApplicationContext()).getHomePageClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        view.getContext().startActivity(intent);
    }

    public void onClickViewOrder(View view, String incrementId, boolean canReorder) {
        Intent intent = new Intent(view.getContext(), OrderDetailActivity.class);
        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, incrementId);
        intent.putExtra(BUNDLE_KEY_CAN_REORDER, canReorder);
        view.getContext().startActivity(intent);
    }

    public void onClickCreateAnAccount(final View view, String orderId) {
        view.setEnabled(false);

        ApiConnection.accountCreateEmail(AppSharedPref.getStoreId(view.getContext())
                , orderId)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(view.getContext()) {
            @Override
            public void onNext(BaseModel createAccountResponse) {
                super.onNext(createAccountResponse);
                if (createAccountResponse.getSuccess()) {
                    ((OrderPlacedActivity) view.getContext()).mBinding.msg.setText(createAccountResponse.getMessage());
                    ((OrderPlacedActivity) view.getContext()).mBinding.email.setVisibility(View.GONE);
                    ((OrderPlacedActivity) view.getContext()).mBinding.createAccountBtn.setVisibility(View.GONE);
                } else {
                    view.setEnabled(true);
                    showToast(view.getContext(), createAccountResponse.getMessage(), Toast.LENGTH_LONG, 0);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                view.setEnabled(true);
            }
        });
    }
}