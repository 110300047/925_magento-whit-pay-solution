package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.activity.OrderPlacedActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.fragment.OrderReviewFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.checkout.SaveOrderResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SAVE_ORDER_RESPONSE;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CART_COUNT;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class OrderReviewFragmentHandler {

    private final OrderReviewFragment mContext;
    private SaveOrderResponse mSaveOrderResponse;

    public OrderReviewFragmentHandler(OrderReviewFragment context) {
        mContext = context;
    }

    public void onClickContinueBtn(@SuppressWarnings("UnusedParameters") View view) {

        showDefaultAlertDialog(mContext.getContext());

        ApiConnection.saveOrder(
                AppSharedPref.getStoreId(mContext.getContext())
                , AppSharedPref.getQuoteId(mContext.getContext())
                , AppSharedPref.getCustomerId(mContext.getContext())
                , AppSharedPref.isLoggedIn(mContext.getContext()) ? "" : FirebaseInstanceId.getInstance().getToken())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<SaveOrderResponse>(mContext.getContext()) {
            @Override
            public void onNext(SaveOrderResponse saveOrderResponse) {
                super.onNext(saveOrderResponse);
                if (saveOrderResponse.isSuccess()) {
                    mSaveOrderResponse = saveOrderResponse;
                    AppSharedPref.setQuoteId(mContext.getContext(), ApplicationConstant.DEFAULT_QUOTE_ID);
                    AppSharedPref.getSharedPreferenceEditor(mContext.getContext(), CUSTOMER_PREF).putInt(KEY_CART_COUNT, 0).apply();
                    switch (mContext.mSelectedPaymentMethodCode) {
                        default:
                            onPaymentResponse();
                            break;
                    }
                } else {
                    showToast(mContext.getContext(), saveOrderResponse.getMessage(), Toast.LENGTH_LONG, 0);
                    if (saveOrderResponse.getCartCount() == 0) {
                        ((CheckoutActivity) mContext.getContext()).finish();
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    public void onPaymentResponse() {
        Intent intent = new Intent(mContext.getContext(), OrderPlacedActivity.class);
        intent.putExtra(BUNDLE_KEY_SAVE_ORDER_RESPONSE, mSaveOrderResponse);
        mContext.startActivity(intent);
    }
}