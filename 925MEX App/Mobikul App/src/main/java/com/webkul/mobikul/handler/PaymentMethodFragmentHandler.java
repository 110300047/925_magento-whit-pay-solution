package com.webkul.mobikul.handler;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.activity.PayPalActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.fragment.CardFragment;
import com.webkul.mobikul.fragment.OrderReviewFragment;
import com.webkul.mobikul.fragment.PaymentMethodFragment;
import com.webkul.mobikul.fragment.ShippingMethodFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.model.checkout.OrderReviewRequestData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.ApplicationConstant.PAYMENT_CODE_OPENPAY_CARDS;
import static com.webkul.mobikul.constants.ApplicationConstant.PAYMENT_CODE_PAYPAL;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class PaymentMethodFragmentHandler {

    public String mShippingMethod = "";
    private PaymentMethodFragment mContext;

    public PaymentMethodFragmentHandler(PaymentMethodFragment context) {
        mContext = context;
    }

    public void onClickContinueBtn(@SuppressWarnings("UnusedParameters") View view) {
        PaymentMethodFragment paymentMethodFragment = (PaymentMethodFragment) ((CheckoutActivity) mContext.getContext()).getSupportFragmentManager().findFragmentByTag(PaymentMethodFragment.class
                .getSimpleName());

        ShippingMethodFragment shippingMethodFragment = (ShippingMethodFragment) ((CheckoutActivity) mContext.getContext()).getSupportFragmentManager().findFragmentByTag
                (ShippingMethodFragment.class.getSimpleName());

        if (shippingMethodFragment != null)
            mShippingMethod = shippingMethodFragment.getShippingMethodInfoData().getSelectedShippingMethodCode();

        String paymentMethod = paymentMethodFragment.getSelectedPaymentMethodCode();
        if (paymentMethod.isEmpty()) {
            SnackbarHelper.getSnackbar((CheckoutActivity) mContext.getContext(), mContext.getString(R.string.message_no_payment_method_chosen)).show();
        } else if (paymentMethod.equals(PAYMENT_CODE_OPENPAY_CARDS)) {//Si es paypal abrimos activity por crear

            /*Intent intent = new Intent(mContext.getContext(), PayPalActivity.class);
            mContext.startActivity(intent);*/

            FragmentManager childFragmentManager = mContext.getChildFragmentManager();
            CardFragment cardFragment = CardFragment.newInstance(mContext);
            cardFragment.show(childFragmentManager, CardFragment.class.getSimpleName());
        } else {
            showDefaultAlertDialog(mContext.getContext());

            ApiConnection.getOrderReviewData(
                    AppSharedPref.getStoreId(mContext.getContext())
                    , AppSharedPref.getQuoteId(mContext.getContext())
                    , AppSharedPref.getCustomerId(mContext.getContext())
                    , mShippingMethod
                    , ""
                    , ""
                    , ""
                    , ""
                    , ""
                    , ""
                    , ""
                    , paymentMethod
                    , AppSharedPref.getCurrencyCode(mContext.getContext()))
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<OrderReviewRequestData>(mContext.getContext()) {
                @Override
                public void onNext(OrderReviewRequestData orderReviewRequestData) {
                    super.onNext(orderReviewRequestData);
                    if (orderReviewRequestData.isSuccess()) {
                        FragmentManager fragmentManager = ((CheckoutActivity) mContext.getContext()).getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.add(R.id.container, OrderReviewFragment.newInstance(mContext.getSelectedPaymentMethodCode(), orderReviewRequestData), OrderReviewFragment.class.getSimpleName());
                        fragmentTransaction.addToBackStack(OrderReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
                        fragmentTransaction.commit();
                    } else {
                        showToast(mContext.getContext(), orderReviewRequestData.getMessage(), Toast.LENGTH_LONG, 0);
                        if (orderReviewRequestData.getCartCount() == 0) {
                            ((CheckoutActivity) mContext.getContext()).finish();
                        }
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        }
    }
}