package com.webkul.mobikul.handler;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.activity.LoginAndSignUpActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.adapter.ProductAttributesSwatchRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.ProductCreateReviewFragment;
import com.webkul.mobikul.fragment.ProductEmailFragment;
import com.webkul.mobikul.fragment.ProductFragment;
import com.webkul.mobikul.fragment.ProductReviewListFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ProductHelper;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.CatalogSingleProductData;
import com.webkul.mobikul.model.catalog.SwatchData;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductAddToWishlistResponse;
import com.webkul.mobikul.model.product.ProductAddToCartResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_APP_SIGN_IN;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_TITLE;
import static com.webkul.mobikul.helper.AlertDialogHelper.dismiss;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.ToastHelper.showToast;


/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */

public class ProductActivityHandler {
    private final ProductFragment mContext;

    private JSONObject mProductParamsJSON;
    private JSONArray mRelatedProductsJSONArray;

    private Handler repeatUpdateHandler = new Handler();
    private int REFRESH_RATE = 200;
    private boolean isIncrement;
    private int productQty;
    private boolean mIsProcessing;
    private Runnable autoChangeQty = new Runnable() {
        @Override
        public void run() {
            if (isIncrement) {
                mContext.mCatalogSingleProductData.setQty(String.valueOf(productQty++));
            } else {
                mContext.mCatalogSingleProductData.setQty(String.valueOf(productQty--));
            }
            repeatUpdateHandler.postDelayed(this, REFRESH_RATE);
            if (REFRESH_RATE > 80)
                REFRESH_RATE -= 5;
        }
    };

    public ProductActivityHandler(ProductFragment context) {
        mContext = context;
    }

    public void onClickWriteReview(@SuppressWarnings("UnusedParameters") View view, boolean guestCanReview) {
        if (guestCanReview || AppSharedPref.isLoggedIn(mContext.getContext())) {
            FragmentManager fragmentManager = ((NewProductActivity) mContext.getContext()).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(android.R.id.content, ProductCreateReviewFragment.newInstance(mContext.mCatalogSingleProductData.getRatingFormData(), mContext.mCatalogSingleProductData.getId()), ProductCreateReviewFragment.class.getSimpleName());
            fragmentTransaction.addToBackStack(NewProductActivity.class.getSimpleName() + BACKSTACK_SUFFIX);
            fragmentTransaction.commit();
        } else {
            showLoginAlertDialog(mContext.getString(R.string.please_login_to_write_reviews));
        }
    }

    public void onClickProductReview(@SuppressWarnings("UnusedParameters") View view) {
        FragmentManager fragmentManager = ((NewProductActivity) mContext.getContext()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.add(android.R.id.content, ProductReviewListFragment.newInstance(mContext.mCatalogSingleProductData.getReviewList()));
        fragmentTransaction.addToBackStack(ProductReviewListFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        fragmentTransaction.commit();
    }

    public void onClickIncrementQty(@SuppressWarnings("UnusedParameters") View view, CatalogSingleProductData data) {
        data.setQty(String.valueOf(Integer.parseInt(data.getQty()) + 1));
    }

    public void onClickDecrementQty(@SuppressWarnings("UnusedParameters") View view, CatalogSingleProductData data) {
        data.setQty(String.valueOf(Integer.parseInt(data.getQty()) - 1));
    }

    public boolean onLongClickDecrementQty(@SuppressWarnings("UnusedParameters") View view, String qty) {
        productQty = Integer.parseInt(qty);
        isIncrement = false;
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)) {
                    repeatUpdateHandler.removeCallbacks(autoChangeQty);
                }
                return false;
            }
        });
        repeatUpdateHandler.postDelayed(autoChangeQty, 0);
        return true;
    }

    public boolean onLongClickIncrementQty(@SuppressWarnings("UnusedParameters") View view, String qty) {
        productQty = Integer.parseInt(qty);
        isIncrement = true;
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)) {
                    repeatUpdateHandler.removeCallbacks(autoChangeQty);
                }
                return false;
            }
        });
        repeatUpdateHandler.postDelayed(autoChangeQty, 0);
        return true;
    }

    public void onClickShareBtn(View view, String productUrl, int productId, String productName) {
        ProductHelper.shareProduct(mContext.getContext()
                , productUrl
                , productId
                , productName);
    }

    public void onClickEmailBtn(View view, int productId) {
        if (AppSharedPref.isLoggedIn(mContext.getContext())) {
            FragmentManager fragmentManager = ((NewProductActivity) mContext.getContext()).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.add(android.R.id.content, ProductEmailFragment.newInstance(productId));
            fragmentTransaction.addToBackStack(NewProductActivity.class.getSimpleName() + BACKSTACK_SUFFIX);
            fragmentTransaction.commit();
        } else {
            showLoginAlertDialog(mContext.getString(R.string.please_login_to_email));
        }
    }

    public void onClickAddToCompareBtn(View view, int productId, String productName) {
        showDefaultAlertDialog(mContext.getContext());

        ApiConnection.addToCompare(AppSharedPref.getStoreId(mContext.getContext())
                , AppSharedPref.getCustomerId(view.getContext())
                , mContext.mCatalogSingleProductData.getId())

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext.getContext()) {
            @Override
            public void onNext(BaseModel addToCompareResponse) {
                super.onNext(addToCompareResponse);
                onResponseAddToCompare(addToCompareResponse);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }


    private void onResponseAddToCompare(BaseModel addToCompareResponse) {
        showToast(mContext.getContext(), addToCompareResponse.getMessage(), Toast.LENGTH_SHORT, 0);
    }

    public void onClickAddToWishlistBtn(final View wishlistBtnView, int productId, String productName, final int itemId) {
        if (!mIsProcessing) {
            if (AppSharedPref.isLoggedIn(mContext.getContext())) {
                if (mContext.mCatalogSingleProductData.getIsInWishlist()) {
                    View layout = mContext.getActivity().getLayoutInflater().inflate(R.layout.popup_menu, null);
                    layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                    final PopupWindow mWishlistDropdown = new PopupWindow(layout, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, true);
                    mWishlistDropdown.setOutsideTouchable(true);
                    mWishlistDropdown.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    final TextView addAnother = layout.findViewById(R.id.add_another);
                    addAnother.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mWishlistDropdown.dismiss();
                            addToWishlist(wishlistBtnView);
                        }
                    });

                    final TextView removeFromWishlist = layout.findViewById(R.id.remove_from_wishlist);
                    removeFromWishlist.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mWishlistDropdown.dismiss();
                            showAlertDialogWithClickListener(mContext.getContext(), SweetAlertDialog.WARNING_TYPE, mContext.getContext().getString(R.string.warning_are_you_sure), mContext.getContext().getString(R.string.question_want_to_remove_this_product_from_whislist),
                                    mContext.getContext().getString(R.string.message_yes_remove_it), mContext.getContext().getString(R.string.no), new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismiss();
                                            mIsProcessing = true;
                                            ((LottieAnimationView) wishlistBtnView).reverseAnimation();
                                            ApiConnection.removeItemFromWishlist(AppSharedPref.getCustomerId(mContext.getContext())
                                                    , AppSharedPref.getStoreId(mContext.getContext())
                                                    , itemId)

                                                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext.getContext()) {
                                                @Override
                                                public void onNext(BaseModel removeItemFromWishlistResponse) {
                                                    super.onNext(removeItemFromWishlistResponse);
                                                    mIsProcessing = false;
                                                    if (removeItemFromWishlistResponse.isSuccess()) {
                                                        mContext.mCatalogSingleProductData.setIsInWishlist(false);
                                                        mContext.mCatalogSingleProductData.setWishlistItemId(0);
                                                    } else {
                                                        ((LottieAnimationView) wishlistBtnView).cancelAnimation();
                                                        ((LottieAnimationView) wishlistBtnView).setProgress(1);
                                                        showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.getContext().getString(R.string.something_went_wrong), removeItemFromWishlistResponse.getMessage());
                                                    }
                                                }

                                                @Override
                                                public void onError(Throwable t) {
                                                    super.onError(t);
                                                    mIsProcessing = false;
                                                    ((LottieAnimationView) wishlistBtnView).cancelAnimation();
                                                    ((LottieAnimationView) wishlistBtnView).setProgress(1);
                                                }
                                            });
                                        }
                                    }, null);
                        }
                    });
                    mWishlistDropdown.showAsDropDown(wishlistBtnView, 5, 5);
                } else {
                    addToWishlist(wishlistBtnView);
                }
            } else {
                showLoginAlertDialog(mContext.getString(R.string.login_first));
            }
        }
    }

    private void addToWishlist(final View view) {
        if (collectAllOptionData(false)) {
            mIsProcessing = true;
            ((LottieAnimationView) view).playAnimation();

            FirebaseAnalyticsImpl.logAddToWishlistEvent(mContext.getContext(), mContext.mCatalogSingleProductData.getId(), mContext.mCatalogSingleProductData.getName());

            ApiConnection.addToWishlist(AppSharedPref.getStoreId(mContext.getContext())
                    , mContext.mCatalogSingleProductData.getId()
                    , AppSharedPref.getCustomerId(mContext.getContext())
                    , mProductParamsJSON)

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductAddToWishlistResponse>(mContext.getContext()) {
                @Override
                public void onNext(CatalogProductAddToWishlistResponse addToWishlistResponse) {
                    super.onNext(addToWishlistResponse);
                    mIsProcessing = false;
                    if (addToWishlistResponse.isSuccess()) {
                        mContext.mCatalogSingleProductData.setIsInWishlist(true);
                        mContext.mCatalogSingleProductData.setWishlistItemId(addToWishlistResponse.getItemId());
                    } else {
                        ((LottieAnimationView) view).cancelAnimation();
                        ((LottieAnimationView) view).setProgress(0);
                        showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), addToWishlistResponse.getMessage());
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                    mIsProcessing = false;
                    ((LottieAnimationView) view).cancelAnimation();
                    ((LottieAnimationView) view).setProgress(0);
                }
            });
        }
    }

    public void onClickNotifyPriceDrop(int productId) {
        if (AppSharedPref.isLoggedIn(mContext.getContext())) {
            showDefaultAlertDialog(mContext.getContext());

            ApiConnection.addPriceAlert(AppSharedPref.getStoreId(mContext.getContext())
                    , AppSharedPref.getCustomerId(mContext.getContext())
                    , productId)

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext.getContext()) {
                @Override
                public void onNext(BaseModel responseData) {
                    super.onNext(responseData);
                    showToast(mContext.getContext(), responseData.getMessage(), Toast.LENGTH_LONG, 0);
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        } else {
            showLoginAlertDialog(mContext.getString(R.string.login_to_subscribe));
        }
    }

    public void onClickNotifyProductInStock(int productId) {
        if (AppSharedPref.isLoggedIn(mContext.getContext())) {
            showDefaultAlertDialog(mContext.getContext());

            ApiConnection.addStockAlert(AppSharedPref.getStoreId(mContext.getContext())
                    , AppSharedPref.getCustomerId(mContext.getContext())
                    , productId)

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext.getContext()) {
                @Override
                public void onNext(BaseModel responseData) {
                    super.onNext(responseData);
                    showToast(mContext.getContext(), responseData.getMessage(), Toast.LENGTH_LONG, 0);
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        } else {
            showLoginAlertDialog(mContext.getString(R.string.login_to_subscribe));
        }
    }

    public void onClickAddToCartBtn(View view, final boolean isAddedToCart) {
        if(AppSharedPref.isLoggedIn(mContext.getContext())) {
            if (collectAllOptionData(true)) {
                FirebaseAnalyticsImpl.logAddToCartEvent(mContext.getContext(), mContext.mCatalogSingleProductData.getId(), mContext.mCatalogSingleProductData.getName());

                showDefaultAlertDialog(mContext.getContext());

                if (mContext.mIsEditProduct) {
                    ApiConnection.updateproduct(AppSharedPref.getStoreId(mContext.getContext())
                            , mContext.mCatalogSingleProductData.getId()
                            , Integer.parseInt(mContext.mCatalogSingleProductData.getQty())
                            , mProductParamsJSON
                            , mRelatedProductsJSONArray
                            , AppSharedPref.getQuoteId(view.getContext())
                            , AppSharedPref.getCustomerId(view.getContext())
                            , mContext.mItemId)

                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ProductAddToCartResponse>(mContext.getContext()) {
                        @Override
                        public void onNext(ProductAddToCartResponse productAddToCartResponse) {
                            super.onNext(productAddToCartResponse);
                            onResponseAddToCart(productAddToCartResponse, isAddedToCart);
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                        }
                    });
                } else {
                    if (NetworkHelper.isNetworkAvailable(mContext.getContext())) {
                        ApiConnection.addToCart(AppSharedPref.getStoreId(mContext.getContext())
                                , mContext.mCatalogSingleProductData.getId()
                                , Integer.parseInt(mContext.mCatalogSingleProductData.getQty())
                                , mProductParamsJSON
                                , mRelatedProductsJSONArray
                                , AppSharedPref.getQuoteId(view.getContext())
                                , AppSharedPref.getCustomerId(view.getContext()))

                                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ProductAddToCartResponse>(mContext.getContext()) {
                            @Override
                            public void onNext(ProductAddToCartResponse productAddToCartResponse) {
                                super.onNext(productAddToCartResponse);
                                onResponseAddToCart(productAddToCartResponse, isAddedToCart);
                            }

                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                            }
                        });
                    } else {
                        try {
                            dismiss(mContext.getContext());
                            showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.SUCCESS_TYPE, mContext.getString(R.string.added_to_offline_bag), mContext.getString(R.string.offline_mode_add_to_cart_message));

                            ((BaseActivity) mContext.getContext()).mOfflineDataBaseHandler.addToCartOffline(
                                    AppSharedPref.getStoreId(mContext.getContext())
                                    , mContext.mCatalogSingleProductData.getId()
                                    , Integer.parseInt(mContext.mCatalogSingleProductData.getQty())
                                    , mProductParamsJSON.toString()
                                    , mRelatedProductsJSONArray.toString()
                                    , AppSharedPref.getQuoteId(view.getContext())
                                    , AppSharedPref.getCustomerId(view.getContext())
                                    , mContext.mCatalogSingleProductData.getName()
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                            showToast(mContext.getContext(), mContext.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT, 0);
                        }
                    }
                }
            }
        }else {
            showLoginAlertDialog(mContext.getString(R.string.login_for_book_buy_alert));
        }
    }

    private void onResponseAddToCart(ProductAddToCartResponse productAddToCartResponse, boolean isAddedToCart) {
        if (productAddToCartResponse.isSuccess()) {
            if (!isAddedToCart) {
                if (mContext.mIsEditProduct) {
                    mContext.startActivity(new Intent(mContext.getContext(), CartActivity.class));
                } else if (AppSharedPref.isLoggedIn(mContext.getContext()) || mContext.mCatalogSingleProductData.isAllowedGuestCheckout()) {
                    mContext.startActivity(new Intent(mContext.getContext(), CheckoutActivity.class));
                } else {
                    showLoginAlertDialog(mContext.getString(R.string.please_login_to_checkout));
                }
            } else {
                showToast(mContext.getContext(), productAddToCartResponse.getMessage(), Toast.LENGTH_LONG, 0);
            }

            if (!mContext.mIsEditProduct && productAddToCartResponse.getQuoteId() != 0) {
                AppSharedPref.setQuoteId(mContext.getContext(), productAddToCartResponse.getQuoteId());
            }
            ((NewProductActivity) mContext.getContext()).updateCartBadge(productAddToCartResponse.getCartCount());

        } else {
            showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.added_to_cart), productAddToCartResponse.getMessage());
        }
    }

    private boolean collectAllOptionData(boolean isChecksEnabled) {
        mProductParamsJSON = new JSONObject();
        boolean isAllRequiredOptnFilled = true;

        switch (mContext.mCatalogSingleProductData.getTypeId()) {
            case "grouped":
                try {
                    JSONObject groupOptionReqJSON = new JSONObject();
                    boolean isQtyOfAnyProductSet = false;
                    for (int noOfGroupedData = 0; noOfGroupedData < mContext.mCatalogSingleProductData.getGroupedData().size(); noOfGroupedData++) {

                        View eachGroupProductItem = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag(noOfGroupedData);
                        EditText qtyEt = eachGroupProductItem.findViewById(R.id.qty);
                        if ((Integer.parseInt(qtyEt.getText().toString())) > 0) {
                            isQtyOfAnyProductSet = true;
                            groupOptionReqJSON.put(mContext.mCatalogSingleProductData.getGroupedData().get(noOfGroupedData).getId(), qtyEt.getText().toString());
                        }
                    }

                    if (isChecksEnabled && !isQtyOfAnyProductSet) {
                        showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.bundle_product_specify_qty));
                        isAllRequiredOptnFilled = false;
                    }
                    mProductParamsJSON.put("super_group", groupOptionReqJSON);
                } catch (Exception e) {
                    isAllRequiredOptnFilled = false;
                    showToast(mContext.getContext(), mContext.getString(R.string.something_went_wrong), Toast.LENGTH_LONG, 0);
                    e.printStackTrace();
                }
                break;
            case "bundle":
                try {
                    if (!mContext.mCatalogSingleProductData.getBundleOptions().toString().equals("[]") && mContext.mCatalogSingleProductData.getIsAvailable()) {

                        JSONObject bundleOptionsItemObj = new JSONObject();
                        JSONObject bundleOptionsQtyObj = new JSONObject();


                        for (int noOfBundleOpt = 0; noOfBundleOpt < mContext.mCatalogSingleProductData.getBundleOptions().size(); noOfBundleOpt++) {
                            if (!isAllRequiredOptnFilled) {
                                break;
                            }
                            switch (mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getType()) {
                                case "select":
                                    Spinner dropdownBundleOptionSpinner = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("bundleoption" + noOfBundleOpt).findViewById(R.id.dropdown_bundle_option_spinner);
                                    EditText qtyDropdownET = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("bundleoption" + noOfBundleOpt).findViewById(R.id.qty_et);
                                    int selectedItemPosition = dropdownBundleOptionSpinner.getSelectedItemPosition();
                                    Log.d(ApplicationConstant.TAG, "onClickAddToCartBtn: " + selectedItemPosition);
                                    if (!isChecksEnabled || selectedItemPosition != 0) {
                                        bundleOptionsItemObj.put(mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionId(),
                                                mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues()
                                                        .get(Integer.parseInt(dropdownBundleOptionSpinner.getSelectedView().getTag().toString())).getOptionValueId());
                                        bundleOptionsQtyObj.put(mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionId(),
                                                qtyDropdownET.getText().toString());
                                    } else {
                                        showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                                + mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getTitle()
                                                + " " + mContext.getString(R.string._is_not_complete_));
                                        isAllRequiredOptnFilled = false;
                                    }
                                    break;
                                case "radio":
                                    RadioGroup bundleOptionRg = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("bundleoption" + noOfBundleOpt).findViewById(R.id.bundle_option_rg);
                                    EditText qtyRadioET = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("bundleoption" + noOfBundleOpt).findViewById(R.id.qty_et);
                                    if (mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getRequired() == 1) {
                                        if (!isChecksEnabled || bundleOptionRg.getCheckedRadioButtonId() != -1) {
                                            RadioButton selectedRadioBtn = mContext.mFragmentProductBinding.getRoot().findViewById(bundleOptionRg
                                                    .getCheckedRadioButtonId());
                                            bundleOptionsItemObj.put(mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionId(),
                                                    mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues()
                                                            .get(Integer.parseInt(selectedRadioBtn.getTag().toString())).getOptionValueId());
                                            bundleOptionsQtyObj.put(mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionId(),
                                                    qtyRadioET.getText().toString());
                                        } else {
                                            showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                                    + mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getTitle()
                                                    + " " + mContext.getString(R.string._is_not_complete_));
                                            isAllRequiredOptnFilled = false;
                                        }

                                    } else {
                                        if (bundleOptionRg.getCheckedRadioButtonId() != -1) {
                                            RadioButton selectedRadioBtn = mContext.mFragmentProductBinding.getRoot().findViewById(bundleOptionRg.getCheckedRadioButtonId());
                                            if (!selectedRadioBtn.getTag().toString().isEmpty()) {
                                                bundleOptionsItemObj.put(mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionId(),
                                                        mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().get(Integer.parseInt(selectedRadioBtn.getTag().toString())).getOptionValueId());
                                                bundleOptionsQtyObj.put(mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionId(),
                                                        qtyRadioET.getText().toString());
                                            }
                                        }
                                    }
                                    break;
                                case "checkbox":
                                case "multi":

                                    int noOfSelectedCheckbox = 0;
                                    JSONArray selectedOptionvaluesArr = new JSONArray();
                                    for (int noOfOptions = 0; noOfOptions < mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues().size(); noOfOptions++) {
                                        CheckBox eachCheckTypeBundleOptionValue = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("bundleoption" + noOfBundleOpt).findViewWithTag(noOfOptions);
                                        if (eachCheckTypeBundleOptionValue.isChecked()) {
                                            selectedOptionvaluesArr.put(mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionValues()
                                                    .get(Integer.parseInt(eachCheckTypeBundleOptionValue.getTag().toString())).getOptionValueId());
                                            noOfSelectedCheckbox++;
                                        }
                                    }

                                    bundleOptionsItemObj.put(mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getOptionId()
                                            , selectedOptionvaluesArr);

                                    if (isChecksEnabled && noOfSelectedCheckbox == 0) {
                                        showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                                + mContext.mCatalogSingleProductData.getBundleOptions().get(noOfBundleOpt).getTitle()
                                                + " " + mContext.getString(R.string._is_not_complete_));
                                        isAllRequiredOptnFilled = false;
                                    }
                                    break;
                            }
                        }
                        mProductParamsJSON.put("bundle_option", bundleOptionsItemObj);
                        mProductParamsJSON.put("bundle_option_qty", bundleOptionsQtyObj);
                    }
                } catch (Exception e) {
                    isAllRequiredOptnFilled = false;
                    showToast(mContext.getContext(), mContext.getString(R.string.something_went_wrong), Toast.LENGTH_LONG, 0);
                    e.printStackTrace();
                }
                break;
            case "downloadable":
                try {
                    int noOfLinksChecked = 0;
                    JSONObject linksPurchasedSeparatelyIds = new JSONObject();

                    if (mContext.mCatalogSingleProductData.getLinks().getLinksPurchasedSeparately() == 1) {
                        for (int i = 0; i < mContext.mCatalogSingleProductData.getLinks().getLinkData().size(); i++) {
                            /* i+1 as it contain a extra text box for label*/
                            if (((CheckBox) mContext.mFragmentProductBinding.otherProductOptionsContainer.getChildAt(i + 1)).isChecked()) {
                                linksPurchasedSeparatelyIds.put(String.valueOf(noOfLinksChecked++), mContext.mCatalogSingleProductData.getLinks().getLinkData().get(i).getId());
                            }
                        }
                    }

                    if (isChecksEnabled && noOfLinksChecked == 0 && mContext.mCatalogSingleProductData.getLinks().getLinksPurchasedSeparately() == 1
                            && mContext.mCatalogSingleProductData.getLinks().getLinkData().size() != 0) {
                        showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                + mContext.mCatalogSingleProductData.getLinks().getTitle()
                                + " " + mContext.getString(R.string._is_not_complete_));
                        isAllRequiredOptnFilled = false;
                    }
                    if (linksPurchasedSeparatelyIds.length() > 0)
                        mProductParamsJSON.put("links", linksPurchasedSeparatelyIds);
                } catch (Exception e) {
                    isAllRequiredOptnFilled = false;
                    showToast(mContext.getContext(), mContext.getString(R.string.something_went_wrong), Toast.LENGTH_LONG, 0);
                    e.printStackTrace();
                }
                break;
            case "hotelbooking":
            case "configurable":
                try {
                    JSONObject configurableOptnObj = new JSONObject();

                    for (int attributePosition = 0; attributePosition < mContext.mCatalogSingleProductData.getConfigurableData().getAttributes().size(); attributePosition++) {
                        if (mContext.mCatalogSingleProductData.getConfigurableData().getAttributes().get(attributePosition).getSwatchType().equals("visual") || mContext.mCatalogSingleProductData.getConfigurableData().getAttributes().get(attributePosition).getSwatchType().equals("text")) {
                            RecyclerView eachSwatchRecyclerView = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + attributePosition).findViewById(R.id.configurable_item_rv);
                            ArrayList<SwatchData> recyclerViewOpions = ((ProductAttributesSwatchRvAdapter) eachSwatchRecyclerView.getAdapter()).getSwatchValuesData();

                            String selectedItemId = "";
                            for (SwatchData swatch : recyclerViewOpions) {
                                if (swatch.isSelected()) {
                                    selectedItemId = swatch.getId();
                                    break;
                                }
                            }
                            if (isChecksEnabled && selectedItemId.isEmpty()) {
                                TextView errorTextView = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + attributePosition).findViewById(R.id.configurable_attribute_error);
                                errorTextView.setVisibility(View.VISIBLE);
                                errorTextView.startAnimation(AnimationUtils.loadAnimation(mContext.getActivity(), R.anim.shake_error));
                                mContext.mFragmentProductBinding.scrollView.scrollTo(0, mContext.mFragmentProductBinding.otherProductOptionsContainer.getTop() - 20);
                                isAllRequiredOptnFilled = false;
                            } else {
                                mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + attributePosition).findViewById(R.id.configurable_attribute_error).setVisibility(View.GONE);
                                configurableOptnObj.put(mContext.mCatalogSingleProductData.getConfigurableData().getAttributes().get(attributePosition).getId(), selectedItemId);
                            }
                        } else {
                            Spinner eachConfigurableAttributeSpinner = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + attributePosition).findViewById(R.id.spinner_configurable_item);
                            int selectedItemOfEacchConfigurableProductAttributeSpinner = eachConfigurableAttributeSpinner.getSelectedItemPosition();
                            String actualPositionOfEachConfigurableProductAttributeSpinner = (String) eachConfigurableAttributeSpinner.getItemAtPosition(selectedItemOfEacchConfigurableProductAttributeSpinner);

                            eachConfigurableAttributeSpinner.setFocusable(true);
                            eachConfigurableAttributeSpinner.setFocusableInTouchMode(true);
                            if (isChecksEnabled && actualPositionOfEachConfigurableProductAttributeSpinner.isEmpty()) {
                                TextView errorTextView = mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + attributePosition).findViewById(R.id.configurable_attribute_error);
                                errorTextView.setVisibility(View.VISIBLE);
                                errorTextView.startAnimation(AnimationUtils.loadAnimation(mContext.getActivity(), R.anim.shake_error));
                                mContext.mFragmentProductBinding.scrollView.scrollTo(0, mContext.mFragmentProductBinding.descriptionHeading.getBottom());
                                isAllRequiredOptnFilled = false;
                            } else {
                                mContext.mFragmentProductBinding.otherProductOptionsContainer.findViewWithTag("config" + attributePosition).findViewById(R.id.configurable_attribute_error).setVisibility(View.GONE);
                            }

                            String attibuteOptionValueId = mContext.mCatalogSingleProductData.getConfigurableData().getAttributes()
                                    .get(attributePosition).getOptions().get(Integer.parseInt(actualPositionOfEachConfigurableProductAttributeSpinner)).getId();

                            configurableOptnObj.put(mContext.mCatalogSingleProductData.getConfigurableData().getAttributes().get(attributePosition).getId(),
                                    attibuteOptionValueId);
                        }
                    }
                    mProductParamsJSON.put("super_attribute", configurableOptnObj);
                } catch (Exception e) {
                    isAllRequiredOptnFilled = false;
                    showToast(mContext.getContext(), mContext.getString(R.string.something_went_wrong), Toast.LENGTH_LONG, 0);
                    e.printStackTrace();
                }
                break;
        }

        try {
            if (isAllRequiredOptnFilled && mContext.mCatalogSingleProductData.getCustomOptions().size() != 0) {
                JSONObject customOptnObj = new JSONObject();
                for (int noOfCustomOpt = 0; noOfCustomOpt < mContext.mCatalogSingleProductData.getCustomOptions().size(); noOfCustomOpt++) {
                    switch (mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getType()) {
                        case "field":
                        case "area":
                            String title=mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle();
                            if (title.equals("Booking From")||
                                    title.equals("Booking Till")||
                                    title.equals("Rent To") ||
                                    title.equals("Rent From")||
                                    title.equals("Booking Date")){
                                LinearLayout customOptionDateView = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                                Log.d(ApplicationConstant.TAG, "onClickAddToCartBtn:1 " + customOptionDateView);

                                EditText dateOrTimeET;
                                dateOrTimeET = customOptionDateView.findViewById(R.id.dateET);
                                if (!dateOrTimeET.getText().toString().isEmpty()) {
                                    String date[] = dateOrTimeET.getText().toString().split("/");
                                    JSONObject temp = new JSONObject();
                                    temp.put("month", date[0]);
                                    temp.put("day", date[1]);
                                    temp.put("year", date[2]);
                                    customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt)
                                            .getOptionId(), temp);

                                } else if (isChecksEnabled && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1) {
                                    showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                            + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                            + " " + mContext.getString(R.string._is_not_complete_));
                                    isAllRequiredOptnFilled = false;
                                }
                            }else if(title.equals("Adults")||
                                    title.equals("Kids")||
                                    title.equals("Charged Per")){
                                LinearLayout customOptionDateView = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                                Log.d(ApplicationConstant.TAG, "onClickAddToCartBtn:1 " + customOptionDateView);

                                EditText dateOrTimeET;
                                dateOrTimeET = customOptionDateView.findViewById(R.id.integer);
                                if (!dateOrTimeET.getText().toString().isEmpty()) {
                                    customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionId(), dateOrTimeET.getText().toString());
                                }
                                if (isChecksEnabled && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1 && dateOrTimeET.getText().toString().isEmpty()) {
                                    showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                            + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                            + " " + mContext.getString(R.string._is_not_complete_));
                                    isAllRequiredOptnFilled = false;
                                }
                            }else if(title.equals("Booking Slot")){
                                //if fist element(none 0) is selected then -1 is send.
                                Spinner custonOptionDropDownSpinner = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                                int positionOfSpinnerItem = custonOptionDropDownSpinner.getSelectedItemPosition();

                                if (positionOfSpinnerItem != 0 && !custonOptionDropDownSpinner.getSelectedItem().equals("Closed")) {
                                    customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionId(),
                                            custonOptionDropDownSpinner.getSelectedItem());
                                } else {
                                    if (isChecksEnabled && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1) {
                                        showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                                + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                                + " " + mContext.getString(R.string._is_not_complete_));
                                        isAllRequiredOptnFilled = false;
                                    }
                                }

                            }
                            else {
                                EditText fieldTypeCustomOption = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                                customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionId(), fieldTypeCustomOption.getText().toString());

                                if (isChecksEnabled && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1 && fieldTypeCustomOption.getText().toString().isEmpty()) {
                                    showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                            + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                            + " " + mContext.getString(R.string._is_not_complete_));
                                    isAllRequiredOptnFilled = false;
                                }
                            }
                            break;
                        case "file":

/*TODOTODO :: update product page ::: CUSTOM OPTION FILE TYPE   */
//                                if (customOptnList.get(noOfViews).getIsRequired() == 1
//                                && ((TextView) mFragmentProductBinding.customOptionLl.findViewWithTag("file" + noOfViews).findViewById(R.id.fileSelectedTV)).getText().equals(getResources().getString(R.string.no_file_selected))
//                            /*&& ((TextView) findViewById(customOptnList.get(noOfViews).getAssociatedId())).getText().equals(getResources().getString(R.string.no_file_selected))*/) {
//                            isAllReqOptnFilled = false;
//                            if (canShowReqDialog) {
//                                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewProductActivity.this);
//                                dlgAlert.setMessage(getResources().getString(R.string.field_) + " " + customOptnList.get(noOfViews).getTitle() + getResources().getString(R.string._is_not_complete_));
//                                dlgAlert.setPositiveButton(getResources().getString(android.R.string.ok), null);
//                                dlgAlert.setCancelable(true);
//                                dlgAlert.create().show();
//                                canShowReqDialog = false;
//                            }
//                        }

                            break;

                        case "drop_down":
                            //if fist element(none 0) is selected then -1 is send.
                            Spinner custonOptionDropDownSpinner = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                            int positionOfSpinnerItem = custonOptionDropDownSpinner.getSelectedItemPosition();

                            if (positionOfSpinnerItem != 0) {
                                customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues()
                                                .get(positionOfSpinnerItem - 1).getOptionId(),
                                        mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionValues()
                                                .get(positionOfSpinnerItem - 1).getOptionTypeId());
                            } else {
                                if (isChecksEnabled && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1) {
                                    showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                            + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                            + " " + mContext.getString(R.string._is_not_complete_));
                                    isAllRequiredOptnFilled = false;
                                }
                            }
                            break;

                            /*TODOTODO:: put the check in case radio group has none option*/
                        case "radio":
                            RadioGroup customoptionRG = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                            RadioButton selectedRadioBtn = mContext.mFragmentProductBinding.getRoot().findViewById(customoptionRG.getCheckedRadioButtonId());
                            int indexOfSelectedOptionValue = customoptionRG.indexOfChild(selectedRadioBtn);
                            //return negative if nothing is selected
                            if (indexOfSelectedOptionValue != -1) {
                                customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getOptionId(),
                                        selectedRadioBtn.getTag().toString());
                            } else {
                                if (isChecksEnabled && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1) {
                                    showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                            + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                            + " " + mContext.getString(R.string._is_not_complete_));
                                    isAllRequiredOptnFilled = false;
                                }
                            }
                            break;

                        case "checkbox":
                        case "multiple":

                            int noOfCheckBoxChecked = 0;
                            JSONArray selectedCheckBoxCustomOptionJSON = new JSONArray();
                            LinearLayout checkBoxCustomOptionLayout = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                            for (int childPoistion = 0; childPoistion < checkBoxCustomOptionLayout.getChildCount(); childPoistion++) {
                                CheckBox customOptionValueCheck = (CheckBox) checkBoxCustomOptionLayout.getChildAt(childPoistion);
                                if (customOptionValueCheck.isChecked()) {
                                    noOfCheckBoxChecked++;
                                    break;
                                }

                            }

                            if (isChecksEnabled && noOfCheckBoxChecked == 0 && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1) {
                                showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                        + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                        + " " + mContext.getString(R.string._is_not_complete_));
                                isAllRequiredOptnFilled = false;

                            } else {
                                for (int childPoistion = 0; childPoistion < checkBoxCustomOptionLayout.getChildCount(); childPoistion++) {
                                    CheckBox customOptionValueCheck = (CheckBox) checkBoxCustomOptionLayout.getChildAt(childPoistion);
                                    if (customOptionValueCheck.isChecked()) {
                                        selectedCheckBoxCustomOptionJSON.put(customOptionValueCheck.getTag().toString());
                                    }
                                }
                                customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt)
                                        .getOptionId(), selectedCheckBoxCustomOptionJSON);
                            }
                            break;

                        case "date":
                        case "time":
                            LinearLayout customOptionDateView = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag(noOfCustomOpt);
                            Log.d(ApplicationConstant.TAG, "onClickAddToCartBtn:1 " + customOptionDateView);

                            EditText dateOrTimeET;
                            if (mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getType().equals("date")) {
                                dateOrTimeET = customOptionDateView.findViewById(R.id.dateET);
                            } else {
                                dateOrTimeET = customOptionDateView.findViewById(R.id.timeET);
                            }
                            if (!dateOrTimeET.getText().toString().isEmpty()) {
                                if (mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getType().equals("date")) {

                                    String date[] = dateOrTimeET.getText().toString().split("/");
                                    JSONObject temp = new JSONObject();
                                    temp.put("month", date[0]);
                                    temp.put("day", date[1]);
                                    temp.put("year", date[2]);
                                    customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt)
                                            .getOptionId(), temp);
                                } else {
                                    String time[] = dateOrTimeET.getText().toString().split(":");
                                    String minAmPm[] = time[1].split(" ");
                                    JSONObject temp = new JSONObject();
                                    temp.put("hour", time[0]);
                                    temp.put("minute", minAmPm[0]);
                                    temp.put("day_part", minAmPm[1]);
                                    customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt)
                                            .getOptionId(), temp);
                                }
                            } else if (isChecksEnabled && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1) {
                                showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                        + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                        + " " + mContext.getString(R.string._is_not_complete_));
                                isAllRequiredOptnFilled = false;
                            }
                            break;

                        case "date_time":
                            EditText dateEditText1 = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag("date" + noOfCustomOpt).findViewById(R.id.dateET);
                            EditText timeEditText1 = mContext.mFragmentProductBinding.productCustomOptionsContainer.findViewWithTag("time" + noOfCustomOpt).findViewById(R.id.timeET);

                            JSONObject temp = new JSONObject();
                            if (!dateEditText1.getText().toString().isEmpty() && !timeEditText1.getText().toString().isEmpty()) {
                                String date[] = dateEditText1.getText().toString().split("/");
                                temp.put("month", date[0]);
                                temp.put("day", date[1]);
                                temp.put("year", date[2]);
                                String time[] = timeEditText1.getText().toString().split(":");
                                String minAmPm[] = time[1].split(" ");
                                temp.put("hour", time[0]);
                                temp.put("minute", minAmPm[0]);
                                temp.put("day_part", minAmPm[1]);
                                customOptnObj.put(mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt)
                                        .getOptionId(), temp);
                            } else if (isChecksEnabled && mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getIsRequire() == 1
                                    && (dateEditText1.getText().toString().isEmpty() || timeEditText1.getText().toString().isEmpty())) {
                                showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.mCatalogSingleProductData.getName(), mContext.getString(R.string.field_) + " "
                                        + mContext.mCatalogSingleProductData.getCustomOptions().get(noOfCustomOpt).getTitle()
                                        + " " + mContext.getString(R.string._is_not_complete_));
                                isAllRequiredOptnFilled = false;
                            }
                            break;
                    }
                }
                mProductParamsJSON.put("options", customOptnObj);
            }

            mRelatedProductsJSONArray = new JSONArray();
            if (mContext.mCatalogSingleProductData.getRelatedProductListData() != null && mContext.mCatalogSingleProductData.getRelatedProductListData().size() > 0) {
                for (int noOfRelatedProducts = 0; noOfRelatedProducts < mContext.mCatalogSingleProductData.getRelatedProductListData().size(); noOfRelatedProducts++) {
                    if (mContext.mCatalogSingleProductData.getRelatedProductListData().get(noOfRelatedProducts).isAddToCart()) {
                        mRelatedProductsJSONArray.put(mContext.mCatalogSingleProductData.getRelatedProductListData().get(noOfRelatedProducts).getEntityId());
                    }
                }
            }
        } catch (Exception e) {
            isAllRequiredOptnFilled = false;
            showToast(mContext.getContext(), mContext.getString(R.string.something_went_wrong), Toast.LENGTH_LONG, 0);
            e.printStackTrace();
        }

        return isAllRequiredOptnFilled;
    }

    public void onClickViewSellerProfile(View view, int sellerId, String shopTitle) {
        Intent intent = new Intent(mContext.getContext(), ((MobikulApplication) view.getContext().getApplicationContext()).getSellerProfilePageClass());
        intent.putExtra(BUNDLE_KEY_SELLER_ID, sellerId);
        intent.putExtra(BUNDLE_KEY_SELLER_TITLE, shopTitle);
        mContext.startActivity(intent);
    }

    public void onClickContactUs(View view, int sellerId, int productId) {
        if (((MobikulApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass() != null) {
            FragmentManager manager = mContext.getFragmentManager();
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            fragmentTransaction.add(android.R.id.content, ((MobikulApplication) view.getContext().getApplicationContext()).getContactUsFragmentNewInstance(sellerId, productId), ((MobikulApplication) view.getContext().getApplicationContext()).getContactUsFragmentClass().getSimpleName());
            fragmentTransaction.addToBackStack(((MobikulApplication) view.getContext().getApplicationContext()).getContactUsFragmentClass().getSimpleName() + BACKSTACK_SUFFIX);
            fragmentTransaction.commit();
        }
    }

    private void showLoginAlertDialog(String message) {
        showAlertDialogWithClickListener(mContext.getContext(), SweetAlertDialog.NORMAL_TYPE, mContext.getResources().getString(R.string.login), message, mContext.getResources().getString(R.string.login)
                , mContext.getResources().getString(R.string.dialog_cancel), new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(mContext.getContext(), LoginAndSignUpActivity.class);
                        intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_IN_PAGE);
                        mContext.startActivityForResult(intent, RC_APP_SIGN_IN);
                        sweetAlertDialog.dismissWithAnimation();
                    }
                }, null);
    }
}