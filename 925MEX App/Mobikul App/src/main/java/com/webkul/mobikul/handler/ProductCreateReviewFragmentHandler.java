package com.webkul.mobikul.handler;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.adapter.ProductCreateReviewRatingDataRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.fragment.ProductCreateReviewFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.product.CreateReviewData;

import org.json.JSONObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;

/**
 * Created by vedesh.kumar on 14/1/17. @Webkul Software Pvt. Ltd
 */

public class ProductCreateReviewFragmentHandler {
    private Context mContext;

    public void onClickSubmitReview(View view, CreateReviewData data, int productId) {
        mContext = view.getContext();

        ProductCreateReviewFragment productCreateReviewFragment = (ProductCreateReviewFragment) ((NewProductActivity) mContext).getSupportFragmentManager()
                .findFragmentByTag(ProductCreateReviewFragment.class.getSimpleName());

        if (!((ProductCreateReviewRatingDataRvAdapter) productCreateReviewFragment.getBinding().ratingDataRv.getAdapter()).isRatingFilled()) {
            return;
        }

        if (data.isFormValidated(mContext)) {
            JSONObject mRatingObj = ((ProductCreateReviewRatingDataRvAdapter) productCreateReviewFragment.getBinding().ratingDataRv.getAdapter()).getRatingData();

            showDefaultAlertDialog(mContext);

            ApiConnection.saveReview(
                    AppSharedPref.getCustomerId(mContext)
                    , AppSharedPref.getStoreId(mContext)
                    , productId
                    , data.getSummary()
                    , data.getComment()
                    , data.getNickName()
                    , mRatingObj)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
                @Override
                public void onNext(BaseModel saveReviewResponse) {
                    super.onNext(saveReviewResponse);
                    SnackbarHelper.getSnackbar(((BaseActivity) mContext), saveReviewResponse.getMessage()).show();
                    if (saveReviewResponse.isSuccess()) {
                        ((AppCompatActivity) mContext).getSupportFragmentManager().popBackStack();
                    }
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });

        } else {
            Utils.hideKeyboard((NewProductActivity) mContext);
            SnackbarHelper.getSnackbar((NewProductActivity) mContext, mContext.getString(R.string.msg_fill_req_field)).show();
        }
    }
}