package com.webkul.mobikul.handler;

import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.fragment.ProductEmailFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.product.InviteeItem;

import org.json.JSONArray;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 12/7/17. @Webkul Software Private limited
 */

public class ProductEmailHandler {

    private ProductEmailFragment mContext;
    private JSONArray mInviteeNamesArray;
    private JSONArray mInviteeEmailsArray;

    public ProductEmailHandler(ProductEmailFragment productEmailFragment) {
        mContext = productEmailFragment;
    }

    public void onClickAddInvitee(@SuppressWarnings("UnusedParameters") View view) {
        mContext.productEmailData.getInviteeItemsList().add(new InviteeItem());
        mContext.mInviteeRvAdapter.notifyDataSetChanged();
    }

    public void onClickSendEmail(@SuppressWarnings("UnusedParameters") View view) {
        if (validateData()) {
            showDefaultAlertDialog(mContext.getContext());

            ApiConnection.productShare(AppSharedPref.getStoreId(mContext.getContext())
                    , mContext.mProductId
                    , mContext.productEmailData.getName()
                    , mContext.productEmailData.getEmail()
                    , mContext.productEmailData.getMessage()
                    , mInviteeNamesArray
                    , mInviteeEmailsArray)

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext.getContext()) {
                @Override
                public void onNext(BaseModel response) {
                    super.onNext(response);
                    showToast(mContext.getContext(), response.getMessage(), Toast.LENGTH_SHORT, 0);
                    mContext.getActivity().onBackPressed();
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        }
    }

    private boolean validateData() {
        boolean isFormValidated = true;

        if (mContext.productEmailData.getMessage().trim().isEmpty()) {
            isFormValidated = false;
            mContext.mBinding.msg.requestFocus();
            mContext.mBinding.msg.setError(mContext.getString(R.string.please_fill_message));
        }

        if (mContext.productEmailData.getEmail().trim().isEmpty()) {
            isFormValidated = false;
            mContext.mBinding.senderEmail.requestFocus();
            mContext.mBinding.senderEmail.setError(mContext.getString(R.string.please_fill_senders_details));
        }

        if (mContext.productEmailData.getName().trim().isEmpty()) {
            isFormValidated = false;
            mContext.mBinding.senderName.requestFocus();
            mContext.mBinding.senderName.setError(mContext.getString(R.string.please_fill_senders_details));
        }

        if (mContext.productEmailData.getInviteeItemsList().size() > 0) {
            mInviteeNamesArray = new JSONArray();
            mInviteeEmailsArray = new JSONArray();
            for (int noOfInvitee = 0; noOfInvitee < mContext.productEmailData.getInviteeItemsList().size(); noOfInvitee++) {
                if (mContext.productEmailData.getInviteeItemsList().get(noOfInvitee).getName().trim().isEmpty() || mContext.productEmailData.getInviteeItemsList().get(noOfInvitee).getEmail().trim().isEmpty() || !EMAIL_PATTERN.matcher(mContext.productEmailData.getInviteeItemsList().get(noOfInvitee).getEmail().trim()).matches()) {
                    isFormValidated = false;
                    if (!EMAIL_PATTERN.matcher(mContext.productEmailData.getInviteeItemsList().get(noOfInvitee).getEmail().trim()).matches()) {
                        showToast(mContext.getContext(), mContext.getString(R.string.enter_valid_email), Toast.LENGTH_SHORT, 0);
                    } else {
                        showToast(mContext.getContext(), mContext.getString(R.string.please_fill_invitee_details), Toast.LENGTH_SHORT, 0);
                    }
                } else {
                    mInviteeNamesArray.put(mContext.productEmailData.getInviteeItemsList().get(noOfInvitee).getName().trim());
                    mInviteeEmailsArray.put(mContext.productEmailData.getInviteeItemsList().get(noOfInvitee).getEmail().trim());
                }
            }
        }
        return isFormValidated;
    }
}