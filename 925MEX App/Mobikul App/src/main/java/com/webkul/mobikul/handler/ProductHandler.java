package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECTED_PRODUCT_NUMBER;

/**
 * Created by vedesh.kumar on 6/1/17. @Webkul Software Private limited
 */

public class ProductHandler {

    private ArrayList<ProductData> mProductsDatas;

    public ProductHandler(ArrayList<ProductData> productsDatas) {
        mProductsDatas = productsDatas;
    }

    public void onClickOpenProduct(View view, int selectedProduct) {
        Intent intent = new Intent(view.getContext(), NewProductActivity.class);
        intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, mProductsDatas);
        intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
        view.getContext().startActivity(intent);
    }
}