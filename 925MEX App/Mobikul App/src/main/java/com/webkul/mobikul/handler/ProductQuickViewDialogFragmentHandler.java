package com.webkul.mobikul.handler;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.CartActivity;
import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.ProductQuickViewDialogFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductAddToWishlistResponse;
import com.webkul.mobikul.model.product.ProductAddToCartResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECTED_PRODUCT_NUMBER;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;
import static com.webkul.mobikul.helper.ProductHelper.getProductShortData;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */

public class ProductQuickViewDialogFragmentHandler {

    private ProductQuickViewDialogFragment mContext;
    private int mProductId;
    private String mProductName;
    private JSONObject mProductParamsJSON;

    private Handler repeatUpdateHandler = new Handler();
    private int REFRESH_RATE = 200;
    private boolean isIncrement;
    private int productQty;
    private boolean mIsProcessing;

    private Runnable autoChangeQty = new Runnable() {
        @Override
        public void run() {
            if (isIncrement) {
                mContext.mProductData.setQty(String.valueOf(productQty++));
            } else {
                mContext.mProductData.setQty(String.valueOf(productQty--));
            }
            repeatUpdateHandler.postDelayed(this, REFRESH_RATE);
            if (REFRESH_RATE > 80)
                REFRESH_RATE -= 5;
        }
    };

    public ProductQuickViewDialogFragmentHandler(ProductQuickViewDialogFragment context) {
        this.mContext = context;
    }

    public void onClickAddToWishlist(final View wishlistBtnView, int productId, String productName, int productPosition, final int itemId) {
        if (!mIsProcessing) {
            if (AppSharedPref.isLoggedIn(mContext.getContext())) {
                mProductId = productId;
                mProductName = productName;

                if (mContext.mProductData.isInWishlist()) {
                    View layout = mContext.getActivity().getLayoutInflater().inflate(R.layout.popup_menu, null);
                    layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                    final PopupWindow mWishlistDropdown = new PopupWindow(layout, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, true);
                    mWishlistDropdown.setOutsideTouchable(true);
                    mWishlistDropdown.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    final TextView addAnother = layout.findViewById(R.id.add_another);
                    addAnother.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mWishlistDropdown.dismiss();
                            addToWishlist(wishlistBtnView);
                        }
                    });

                    final TextView removeFromWishlist = layout.findViewById(R.id.remove_from_wishlist);
                    removeFromWishlist.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mWishlistDropdown.dismiss();
                            showAlertDialogWithClickListener(mContext.getContext(), SweetAlertDialog.WARNING_TYPE, mContext.getString(R.string.warning_are_you_sure), mContext.getString(R.string.question_want_to_remove_this_product_from_whislist),
                                    mContext.getString(R.string.message_yes_remove_it), mContext.getString(R.string.no), new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismiss();
                                            mIsProcessing = true;
                                            ((LottieAnimationView) wishlistBtnView).reverseAnimation();

                                            mContext.mProductData.setInWishlist(false);
                                            updateActivityView();

                                            ApiConnection.removeItemFromWishlist(AppSharedPref.getCustomerId(mContext.getContext())
                                                    , AppSharedPref.getStoreId(mContext.getContext())
                                                    , itemId)

                                                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext.getContext()) {
                                                @Override
                                                public void onNext(BaseModel removeItemFromWishlistResponse) {
                                                    super.onNext(removeItemFromWishlistResponse);
                                                    mIsProcessing = false;
                                                    if (removeItemFromWishlistResponse.isSuccess()) {
                                                        mContext.mProductData.setWishlistItemId(0);
                                                    } else {
                                                        mContext.mProductData.setInWishlist(true);
                                                        ((LottieAnimationView) wishlistBtnView).cancelAnimation();
                                                        ((LottieAnimationView) wishlistBtnView).setProgress(1);
                                                        if (mContext.getContext() != null)
                                                            showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.something_went_wrong), removeItemFromWishlistResponse.getMessage());
                                                        updateActivityView();
                                                    }
                                                }

                                                @Override
                                                public void onError(Throwable t) {
                                                    super.onError(t);
                                                    mIsProcessing = false;
                                                    mContext.mProductData.setInWishlist(true);
                                                    ((LottieAnimationView) wishlistBtnView).cancelAnimation();
                                                    ((LottieAnimationView) wishlistBtnView).setProgress(1);
                                                    updateActivityView();
                                                }
                                            });
                                        }
                                    }, null);
                        }
                    });
                    mWishlistDropdown.showAsDropDown(wishlistBtnView, 5, 5);
                } else {
                    addToWishlist(wishlistBtnView);
                }
            } else {
                showToast(mContext.getContext(), mContext.getString(R.string.login_first), Toast.LENGTH_SHORT, 0);
            }
        }
    }

    private void addToWishlist(final View view) {
        mIsProcessing = true;
        ((LottieAnimationView) view).playAnimation();

        FirebaseAnalyticsImpl.logAddToWishlistEvent(mContext.getContext(), mProductId, mProductName);

        mProductParamsJSON = new JSONObject();

        /*adding configurable options*/
        try {
            if (mContext.mProductData.getTypeId().equals("configurable")) {
                JSONObject configurableOptnObj = new JSONObject();
                for (int attributePosition = 0; attributePosition < mContext.selectedConfigOptionsId.size(); attributePosition++) {
                    configurableOptnObj.put(mContext.mProductData.getConfigurableData().getAttributes().get(attributePosition).getId(), mContext.selectedConfigOptionsId.get(attributePosition));
                }
                mProductParamsJSON.put("super_attribute", configurableOptnObj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mContext.mProductData.setInWishlist(true);
        updateActivityView();

        ApiConnection.addToWishlist(AppSharedPref.getStoreId(mContext.getContext())
                , mProductId
                , AppSharedPref.getCustomerId(mContext.getContext())
                , mProductParamsJSON)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductAddToWishlistResponse>(mContext.getContext()) {
            @Override
            public void onNext(CatalogProductAddToWishlistResponse addToWishlistResponse) {
                super.onNext(addToWishlistResponse);
                mIsProcessing = false;
                if (addToWishlistResponse.isSuccess()) {
                    mContext.mProductData.setWishlistItemId(addToWishlistResponse.getItemId());
                } else {
                    mContext.mProductData.setInWishlist(false);
                    ((LottieAnimationView) view).cancelAnimation();
                    ((LottieAnimationView) view).setProgress(0);
                    if (mContext.getContext() != null)
                        showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mProductName, addToWishlistResponse.getMessage());
                    updateActivityView();
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                mIsProcessing = false;
                mContext.mProductData.setInWishlist(false);
                ((LottieAnimationView) view).cancelAnimation();
                ((LottieAnimationView) view).setProgress(0);
                updateActivityView();
            }
        });
    }

    private void updateActivityView() {
        if (mContext.getContext() != null) {
            switch (mContext.mCollectionType) {
                /*case "featuredProducts":
                    ((HomeActivity) mContext.getContext()).mBinding.featuredProductsRv.getAdapter().notifyDataSetChanged();
                    break;
                case "newProducts":
                    ((HomeActivity) mContext.getContext()).mBinding.newProductsRv.getAdapter().notifyDataSetChanged();
                    break;
                case "hotDeals":
                    ((HomeActivity) mContext.getContext()).mBinding.hotDealsRv.getAdapter().notifyDataSetChanged();
                    break;*/
                case "crossSell":
                    ((CartActivity) mContext.getContext()).mBinding.crossSellProductsRv.getAdapter().notifyDataSetChanged();
                    break;
                case "upSell":
                    break;
                default:
                    ((CatalogProductActivity) mContext.getContext()).mBinding.productCatalogRv.getAdapter().notifyDataSetChanged();
                    break;
            }
        }
    }

    public void onClickAddToCompare(View view, int productId, String productName, int productPosition) {
        showDefaultAlertDialog(mContext.getContext());

        mProductId = productId;
        mProductName = productName;

        ApiConnection.addToCompare(AppSharedPref.getStoreId(mContext.getContext())
                , AppSharedPref.getCustomerId(view.getContext())
                , mProductId)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext.getContext()) {
            @Override
            public void onNext(BaseModel addToCompareResponse) {
                super.onNext(addToCompareResponse);
                showToast(mContext.getContext(), addToCompareResponse.getMessage(), Toast.LENGTH_SHORT, 0);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    public void onClickItem(View view, int productId, String productName, boolean openProduct, int selectedProduct) {
        Intent intent = new Intent(view.getContext(), NewProductActivity.class);
        switch (mContext.mCollectionType) {
            case "featuredProducts":
                intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, getProductShortData(((HomeActivity) mContext.getContext()).mHomePageResponseDataObject.getFeaturedProductDatas()));
                break;
            case "newProducts":
                intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, getProductShortData(((HomeActivity) mContext.getContext()).mHomePageResponseDataObject.getNewProducts()));
                break;
            case "hotDeals":
                intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, getProductShortData(((HomeActivity) mContext.getContext()).mHomePageResponseDataObject.getHotDeals()));
                break;
            case "crossSell":
                intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, getProductShortData(((CartActivity) mContext.getContext()).mCartDetailsResponseData.getCrossSellList()));
                break;
            case "upSell":
                intent.putExtra(KEY_PRODUCT_ID, productId);
                intent.putExtra(KEY_PRODUCT_NAME, productName);
                break;
            case "sellerProducts":
                intent.putExtra(KEY_PRODUCT_ID, productId);
                intent.putExtra(KEY_PRODUCT_NAME, productName);
                break;
            default:
                intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, ((CatalogProductActivity) mContext.getContext()).mProductShortDatas);
                break;
        }
        if (openProduct) {
            intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
            view.getContext().startActivity(intent);
        } else {
            mProductName = productName;
            mProductId = productId;
            mProductParamsJSON = new JSONObject();

            /*adding configurable options*/
            try {
                if (mContext.mProductData.getTypeId().equals("configurable")) {
                    JSONObject configurableOptnObj = new JSONObject();

                    if (mContext.mProductData.getConfigurableData().getAttributes().size() == mContext.selectedConfigOptionsId.size()) {
                        for (int attributePosition = 0; attributePosition < mContext.selectedConfigOptionsId.size(); attributePosition++) {
                            configurableOptnObj.put(mContext.mProductData.getConfigurableData().getAttributes().get(attributePosition).getId(), mContext.selectedConfigOptionsId.get(attributePosition));
                        }
                    } else {
                        intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
                        view.getContext().startActivity(intent);
                        showToast(mContext.getContext(), mContext.getString(R.string.you_need_choose_options_for_your_item), Toast.LENGTH_LONG, 0);
                        return;
                    }

                    mProductParamsJSON.put("super_attribute", configurableOptnObj);
                } else if (mContext.mProductData.getTypeId().equals("grouped") || mContext.mProductData.getTypeId().equals("bundle")) {
                    intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
                    view.getContext().startActivity(intent);
                    showToast(mContext.getContext(), mContext.getString(R.string.you_need_choose_options_for_your_item), Toast.LENGTH_LONG, 0);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            FirebaseAnalyticsImpl.logAddToCartEvent(mContext.getContext(), mProductId, mProductName);
            if (NetworkHelper.isNetworkAvailable(mContext.getContext())) {

                showDefaultAlertDialog(mContext.getContext());

                ApiConnection.addToCart(AppSharedPref.getStoreId(mContext.getContext())
                        , mProductId
                        , Integer.parseInt(mContext.mProductData.getQty().isEmpty() ? "1" : mContext.mProductData.getQty())
                        , mProductParamsJSON
                        , new JSONArray()
                        , AppSharedPref.getQuoteId(view.getContext())
                        , AppSharedPref.getCustomerId(view.getContext()))

                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ProductAddToCartResponse>(mContext.getContext()) {
                    @Override
                    public void onNext(ProductAddToCartResponse productAddToCartResponse) {
                        super.onNext(productAddToCartResponse);
                        onResponseAddToCart(productAddToCartResponse);
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                    }
                });
            } else {
                try {
                    showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.SUCCESS_TYPE, mContext.getString(R.string.added_to_offline_bag), mContext.getString(R.string.offline_mode_add_to_cart_message));

                    ((BaseActivity) mContext.getContext()).mOfflineDataBaseHandler.addToCartOffline(
                            AppSharedPref.getStoreId(mContext.getContext())
                            , mProductId
                            , Integer.parseInt(mContext.mProductData.getQty().isEmpty() ? "1" : mContext.mProductData.getQty())
                            , mProductParamsJSON.toString()
                            , new JSONArray().toString()
                            , AppSharedPref.getQuoteId(view.getContext())
                            , AppSharedPref.getCustomerId(view.getContext())
                            , mProductName
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                    showToast(mContext.getContext(), mContext.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT, 0);
                }
            }
        }
    }

    private void onResponseAddToCart(ProductAddToCartResponse productAddToCartResponse) {
        if (mContext.getContext() != null) {
            if (productAddToCartResponse.getSuccess()) {
                showToast(mContext.getContext(), productAddToCartResponse.getMessage(), Toast.LENGTH_LONG, 0);
                mContext.dismiss();
                if (productAddToCartResponse.getQuoteId() != 0) {
                    AppSharedPref.setQuoteId(mContext.getContext(), productAddToCartResponse.getQuoteId());
                }
                ((BaseActivity) mContext.getContext()).updateCartBadge(productAddToCartResponse.getCartCount());
            } else {
                showSuccessOrErrorTypeAlertDialog(mContext.getContext(), SweetAlertDialog.ERROR_TYPE, mProductName, productAddToCartResponse.getMessage());
            }
        }
    }

    public void onClickIncrementQty(@SuppressWarnings("UnusedParameters") View view, String qty) {
        mContext.mProductData.setQty(String.valueOf(Integer.parseInt(qty) + 1));
    }

    public void onClickDecrementQty(@SuppressWarnings("UnusedParameters") View view, String qty) {
        mContext.mProductData.setQty(String.valueOf(Integer.parseInt(qty) - 1));
    }

    public boolean onLongClickDecrementQty(@SuppressWarnings("UnusedParameters") View view, String qty) {
        productQty = Integer.parseInt(qty);
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)) {
                    repeatUpdateHandler.removeCallbacks(autoChangeQty);
                }
                return false;
            }
        });
        repeatUpdateHandler.postDelayed(autoChangeQty, 0);
        return true;
    }

    public boolean onLongClickIncrementQty(@SuppressWarnings("UnusedParameters") View view, String qty) {
        productQty = Integer.parseInt(qty);
        isIncrement = true;
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)) {
                    repeatUpdateHandler.removeCallbacks(autoChangeQty);
                    isIncrement = false;
                }
                return false;
            }
        });
        repeatUpdateHandler.postDelayed(autoChangeQty, 0);
        return true;
    }
}