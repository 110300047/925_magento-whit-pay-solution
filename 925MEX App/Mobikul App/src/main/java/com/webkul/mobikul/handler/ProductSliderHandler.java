package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.FullScreenImageScrollActivity;
import com.webkul.mobikul.model.product.ImageGalleryData;

import java.util.ArrayList;

import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class ProductSliderHandler {

    private ArrayList<ImageGalleryData> mImageGalleryData;

    public ProductSliderHandler(ArrayList<ImageGalleryData> imageGalleryData) {
        mImageGalleryData = imageGalleryData;
    }

    public void onClickProductImage(View view, String productName, int currentPosition) {
        try {
            ArrayList<String> largeImageList = new ArrayList<>();
            ArrayList<String> smallImageList = new ArrayList<>();
            for (ImageGalleryData eachImageGalleryItem : mImageGalleryData) {
                largeImageList.add(eachImageGalleryItem.getLargeImage());
                smallImageList.add(eachImageGalleryItem.getSmallImage());
            }
            Intent intent = new Intent(view.getContext(), FullScreenImageScrollActivity.class);
            intent.putExtra(KEY_PRODUCT_NAME, productName);
            intent.putExtra(FullScreenImageScrollActivity.KEY_CURRENT_ITEM_POSITION, currentPosition);
            intent.putStringArrayListExtra(FullScreenImageScrollActivity.KEY_SMALL_IMAGE_GALLERY_DATA, smallImageList);
            intent.putStringArrayListExtra(FullScreenImageScrollActivity.KEY_LARGE_IMAGE_GALLERY_DATA, largeImageList);
            view.getContext().startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}