package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;

import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.model.extra.SuggestionProductData;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_NAME;


/**
 * Created by vedesh.kumar on 4/5/17.
 */

public class SearchSuggestionProductHandler {
    @SuppressWarnings("unused")
    private static final String TAG = "SearchSuggestionProduct";
    private final Context mContext;
    private final SuggestionProductData mData;

    public SearchSuggestionProductHandler(Context context, SuggestionProductData data) {
        mContext = context;
        mData = data;
    }

    public void onProductSelected() {
        Intent intent = new Intent(mContext, NewProductActivity.class);
        intent.putExtra(BUNDLE_KEY_PRODUCT_ID, Integer.parseInt(mData.getProductId()));
        intent.putExtra(BUNDLE_KEY_PRODUCT_NAME, android.text.Html.fromHtml(mData.getProductName()).toString());
        mContext.startActivity(intent);
    }
}