package com.webkul.mobikul.handler;

import android.content.Context;

import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.model.extra.SuggestionTagData;


/**
 * Created by vedesh.kumar on 4/5/17.
 */

public class SearchSuggestionTagHandler {

    @SuppressWarnings("unused")
    private static final String TAG = "SearchSuggestionTagHand";
    private final SuggestionTagData mData;
    private Context mContext;

    public SearchSuggestionTagHandler(Context context, SuggestionTagData data) {
        mContext = context;
        mData = data;
    }

    public void onTagSelected() {
        if (mContext instanceof HomeActivity) {
            ((HomeActivity) mContext).mBinding.searchView.setQuery(mData.getLabel(), false);
//            Fragment fragment = ((AppCompatActivity) mContext).getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());
//            if (fragment != null && fragment.isAdded()) {
//                ((HomeActivity) fragment).mBinding.searchView.setQuery(mData.getLabel(), false);
//            }
        } else if (mContext instanceof CatalogProductActivity) {
            ((CatalogProductActivity) mContext).mBinding.searchView.setQuery(mData.getLabel(), false);
        }
    }
}