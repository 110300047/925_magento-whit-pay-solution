package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.helper.MobikulApplication;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SEARCH_TERM_QUERY;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Private limited
 */

public class SearchTermsActivityHandler {

    private final Context mContext;

    public SearchTermsActivityHandler(Context context) {
        mContext = context;
    }

    public void onClickSearchItem(View v, String term) {
        Intent intent = new Intent(mContext, ((MobikulApplication) mContext.getApplicationContext()).getCatalogProductPageClass());
        intent.putExtra(BUNDLE_KEY_SEARCH_TERM_QUERY, term);
        mContext.startActivity(intent);
    }
}