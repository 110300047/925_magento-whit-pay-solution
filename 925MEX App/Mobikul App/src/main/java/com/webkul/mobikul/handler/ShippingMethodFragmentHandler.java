package com.webkul.mobikul.handler;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.fragment.PaymentMethodFragment;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.model.checkout.ShippingMethodInfoData;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class ShippingMethodFragmentHandler {

    public void onClickContinueBtn(View view, ShippingMethodInfoData shippingMethodInfoData) {
        if (shippingMethodInfoData.getSelectedShippingMethodCode().isEmpty()) {
            SnackbarHelper.getSnackbar((CheckoutActivity) view.getContext(), view.getContext().getString(R.string.message_no_shipping_method_chosen)).show();
        } else {
            FragmentManager fragmentManager = ((CheckoutActivity) view.getContext()).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.container, PaymentMethodFragment.newInstance(shippingMethodInfoData),
                    PaymentMethodFragment.class.getSimpleName());
            fragmentTransaction.addToBackStack(PaymentMethodFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
            fragmentTransaction.commit();
        }
    }
}