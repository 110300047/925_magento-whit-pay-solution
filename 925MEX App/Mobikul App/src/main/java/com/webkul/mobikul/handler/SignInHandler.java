package com.webkul.mobikul.handler;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.LoginAndSignUpActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.dialog.ForgotPasswordDialogFragment;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.SignInFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.signin.SignInResponseData;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_QUOTE_ID;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.FINGERPRINT_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CART_COUNT;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_BANNER_IMAGE;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_EMAIL;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_FINGER_PASSWORD;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_FINGER_USER_NAME;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_IS_LOGGED_IN;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_NAME;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_PROFILE_IMAGE;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_QUOTE_ID;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 10/1/17. @Webkul Software Private limited
 */

public class SignInHandler {

    private static final String KEY_NAME = "key";

    protected Context mContext;
    private Cipher mCipher;
    private KeyStore mKeyStore;
    private SignInFragment mSignInFragment;
    private boolean isPasswordVisible = false;
    private String email = "";
    private String mobile = "";

    public SignInHandler(Context context, SignInFragment signInFragment) {
        mContext = context;
        mSignInFragment = signInFragment;
    }

    public void onClickShowHidePassword(@SuppressWarnings("UnusedParameters") View view) {
        if (!isPasswordVisible) {
            mSignInFragment.mBinding.passwordEt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mSignInFragment.mBinding.passwordEt.setSelection(mSignInFragment.mBinding.passwordEt.length());
            isPasswordVisible = true;
            view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_strikethrough_diagonal));
        } else {
            mSignInFragment.mBinding.passwordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mSignInFragment.mBinding.passwordEt.setSelection(mSignInFragment.mBinding.passwordEt.length());
            isPasswordVisible = false;
            view.setBackground(null);
        }
    }

    public void onClickSignInBtn(boolean usernameValidated, boolean passwordValidated, String username, String password) {
        if (AppSharedPref.getMobileLoginEnabled(mContext) && android.util.Patterns.PHONE.matcher(username).matches())
            mobile = username;
        else
            email = username;

        if (usernameValidated && passwordValidated) {
            Utils.hideKeyboard((LoginAndSignUpActivity) mContext);

            if (AppSharedPref.getCustomerFingerUserName(mContext).equals("") && AppSharedPref.getCustomerFingerPassword(mContext).equals("")) {
                SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(mContext, FINGERPRINT_PREF);
                customerDataSharedPref.putString(KEY_CUSTOMER_FINGER_USER_NAME, email);
                customerDataSharedPref.putString(KEY_CUSTOMER_FINGER_PASSWORD, password);
                customerDataSharedPref.commit();
            }

            showDefaultAlertDialog(mContext);

            ApiConnection.getSignInData(email
                    , password
                    , Utils.getScreenWidth()
                    , AppSharedPref.getQuoteId(mContext)
                    , AppSharedPref.getStoreId(mContext)
                    , ApplicationConstant.DEFAULT_WEBSITE_ID
                    , mContext.getResources().getDisplayMetrics().density
                    , FirebaseInstanceId.getInstance().getToken()
                    , mobile
                    , "android")

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<SignInResponseData>(mContext) {
                @Override
                public void onNext(SignInResponseData signInResponseData) {
                    super.onNext(signInResponseData);
                    onResponseRecieved(signInResponseData);
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        } else {
            mSignInFragment.mSignInData.setCanDisplayError(true);
            mSignInFragment.mSignInData.setUsername(username);
            mSignInFragment.mSignInData.setPassword(password);
            mSignInFragment.mBinding.setData(mSignInFragment.mSignInData);
        }
    }

    private void onResponseRecieved(SignInResponseData signInResponseData) {
        if (signInResponseData.getSuccess()) {
            ((LoginAndSignUpActivity) mContext).updateCartBadge(signInResponseData.getCartCount());

            showToast(mContext, mContext.getString(R.string.login_successful_message), Toast.LENGTH_SHORT, 0);
            FirebaseAnalyticsImpl.logLoginEvent(mContext, signInResponseData.getCustomerId(), signInResponseData.getCustomerName());

            updateCustomerSharedPref(signInResponseData);

            Intent data = new Intent();
            ((LoginAndSignUpActivity) mContext).setResult(RESULT_OK, data);
            ((LoginAndSignUpActivity) mContext).finish();
        } else {
            SnackbarHelper.getSnackbar(((LoginAndSignUpActivity) mContext), signInResponseData.getMessage()).show();
        }
    }

    protected void updateCustomerSharedPref(SignInResponseData signInResponseData) {
        try {
            SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(mContext, CUSTOMER_PREF);
            customerDataSharedPref.putInt(KEY_QUOTE_ID, DEFAULT_QUOTE_ID);
            customerDataSharedPref.putBoolean(KEY_CUSTOMER_IS_LOGGED_IN, true);
            customerDataSharedPref.putInt(KEY_CART_COUNT, signInResponseData.getCartCount());
            customerDataSharedPref.putInt(KEY_CUSTOMER_ID, signInResponseData.getCustomerId());
            customerDataSharedPref.putString(KEY_CUSTOMER_NAME, signInResponseData.getCustomerName());
            customerDataSharedPref.putString(KEY_CUSTOMER_EMAIL, signInResponseData.getCustomerEmail());
            customerDataSharedPref.putString(KEY_CUSTOMER_PROFILE_IMAGE, signInResponseData.getCustomerProfileImage());
            customerDataSharedPref.putString(KEY_CUSTOMER_BANNER_IMAGE, signInResponseData.getCustomerBannerImage());
            customerDataSharedPref.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClickForgetPassword(String email) {
        FragmentManager fragmentManager = ((LoginAndSignUpActivity) mContext).getSupportFragmentManager();
        ForgotPasswordDialogFragment forgotPasswordDialogFragment = ForgotPasswordDialogFragment.newInstance(email);
        forgotPasswordDialogFragment.show(fragmentManager, ForgotPasswordDialogFragment.class.getSimpleName());
    }

    public void onClickFingerPrintBtn() {

        KeyguardManager keyguardManager = (KeyguardManager) mContext.getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) mContext.getSystemService(FINGERPRINT_SERVICE);

            if (!keyguardManager.isKeyguardSecure()) {
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getString(R.string.fingerprint_error))
                        .setMessage(mContext.getString(R.string.screen_lock_not_enabled) + "\n" +
                                mContext.getString(R.string.change_phone_security_settings) + "\n" +
                                mContext.getString(R.string.click_ok_to_redirect_to_password_login))
                        .setNeutralButton(mContext.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (mSignInFragment.mBinding.enableFingerprintLoginCv.isChecked()) {
                                    mSignInFragment.mBinding.enableFingerprintLoginCv.setChecked(false);
                                }
                            }
                        })
                        .show();
                return;
            }
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getString(R.string.fingerprint_error))
                        .setMessage(mContext.getString(R.string.fingerprint_authentication_permission_not_enabled) + "\n" +
                                mContext.getString(R.string.grant_device_the_permission_to_access_your_fingerprint) + "\n" +
                                mContext.getString(R.string.click_ok_to_redirect_to_password_login))
                        .setNeutralButton(mContext.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (mSignInFragment.mBinding.enableFingerprintLoginCv.isChecked()) {
                                    mSignInFragment.mBinding.enableFingerprintLoginCv.setChecked(false);
                                }
                            }
                        })
                        .show();
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (!fingerprintManager.hasEnrolledFingerprints())) {
                // This happens when no fingerprints are registered.
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getString(R.string.fingerprint_error))
                        .setMessage(mContext.getString(R.string.register_at_least_one_fingerprint_in_settings) + "\n"
                                + mContext.getString(R.string.click_ok_to_redirect_to_password_login))
                        .setNeutralButton(mContext.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (mSignInFragment.mBinding.enableFingerprintLoginCv.isChecked()) {
                                    mSignInFragment.mBinding.enableFingerprintLoginCv.setChecked(false);
                                }
                            }
                        })
                        .show();
                return;
            }

            if (mSignInFragment.mBinding.enableFingerprintLoginCv.isChecked()) {
                String touch_username = AppSharedPref.getCustomerFingerUserName(mContext);
                String touch_password = AppSharedPref.getCustomerFingerPassword(mContext);

                if ((!touch_username.equals("")) && !touch_password.equals("")) {
                    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    inflater.inflate(R.layout.fingerprint_login_layout, mSignInFragment.mBinding.fingerPrintLoginContainer);
                    mSignInFragment.mBinding.fingerPrintLoginContainer.setVisibility(View.VISIBLE);
                    mSignInFragment.mBinding.passwordLoginButton.setVisibility(View.VISIBLE);
                    mSignInFragment.mBinding.manualLoginContainer.setVisibility(View.GONE);

                    generateKey();
                    if (cipherInit()) {
                        FingerprintManager.CryptoObject cryptoObject;
                        FingerprintHandler helper;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            cryptoObject = new FingerprintManager.CryptoObject(mCipher);
                            helper = new FingerprintHandler(mContext);
                            helper.startAuth(fingerprintManager, cryptoObject);
                        }
                    }
                } else if (touch_username.equals("") && touch_password.equals("")) {
                    showToast(mContext, mContext.getString(R.string.login_once_tomap_fingerprint_with_account), Toast.LENGTH_LONG, 0);
                    mSignInFragment.mBinding.enableFingerprintLoginCv.setChecked(false);
                }
            } else {
                mSignInFragment.mBinding.fingerPrintLoginContainer.setVisibility(View.GONE);
                mSignInFragment.mBinding.passwordLoginButton.setVisibility(View.GONE);
                mSignInFragment.mBinding.manualLoginContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKey() {
        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            mKeyStore.load(null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator.init(new
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
            }
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean cipherInit() {
        try {
            mCipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(KEY_NAME, null);
            mCipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (InvalidKeyException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    public void onClickLoginWithPassword() {
        mSignInFragment.mBinding.fingerPrintLoginContainer.setVisibility(View.GONE);
        mSignInFragment.mBinding.passwordLoginButton.setVisibility(View.GONE);
        mSignInFragment.mBinding.manualLoginContainer.setVisibility(View.VISIBLE);
        mSignInFragment.mBinding.enableFingerprintLoginCv.setChecked(false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

        private Context mContext;

        FingerprintHandler(Context context) {
            mContext = context;
        }

        void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
            CancellationSignal cancellationSignal = new CancellationSignal();
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errMsgId,
                                          CharSequence errString) {
            showToast(mContext, "Authentication error\n" + errString, Toast.LENGTH_LONG, 0);
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            showToast(mContext, "Authentication help\n" + helpString, Toast.LENGTH_LONG, 0);
        }

        @Override
        public void onAuthenticationFailed() {
            showToast(mContext, "Authentication failed.", Toast.LENGTH_SHORT, 0);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            String touch_username = AppSharedPref.getCustomerFingerUserName(mContext);
            String touch_password = AppSharedPref.getCustomerFingerPassword(mContext);
            if ((!touch_username.equals("")) && (!touch_password.equals(""))) {
                showDefaultAlertDialog(mContext);

                ApiConnection.getSignInData(AppSharedPref.getCustomerFingerUserName(mContext)
                        , AppSharedPref.getCustomerFingerPassword(mContext)
                        , Utils.getScreenWidth()
                        , AppSharedPref.getQuoteId(mContext)
                        , AppSharedPref.getStoreId(mContext)
                        , ApplicationConstant.DEFAULT_WEBSITE_ID
                        , mContext.getResources().getDisplayMetrics().density
                        , FirebaseInstanceId.getInstance().getToken()
                        , mobile
                        , "android")

                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<SignInResponseData>(mContext) {
                    @Override
                    public void onNext(SignInResponseData signInResponseData) {
                        super.onNext(signInResponseData);
                        onResponseRecieved(signInResponseData);
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                    }
                });

                showToast(mContext, mContext.getString(R.string.auth_succeeded), Toast.LENGTH_SHORT, 0);
            }
        }
    }
}