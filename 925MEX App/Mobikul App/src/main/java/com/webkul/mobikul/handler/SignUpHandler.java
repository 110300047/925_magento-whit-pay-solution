package com.webkul.mobikul.handler;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.LoginAndSignUpActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.SignUpFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormData;
import com.webkul.mobikul.model.customer.signup.SignUpResponseData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_QUOTE_ID;
import static com.webkul.mobikul.helper.AlertDialogHelper.dismiss;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AppSharedPref.CUSTOMER_PREF;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_EMAIL;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_IS_LOGGED_IN;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CUSTOMER_NAME;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_QUOTE_ID;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 18/1/17. @Webkul Software Private limited
 */

public class SignUpHandler {

    private final Calendar dobCalendar = Calendar.getInstance();
    protected Context mContext;
    SignUpFragment mSignUpFragment;
    private CreateAccountFormData mData;
    private int mIsSocial = 0;
    private boolean isPasswordVisible = false;
    private boolean isConfirmPasswordVisible = false;

    public SignUpHandler(Context context, int isSocial, SignUpFragment signUpFragment, CreateAccountFormData createAccountFormData) {
        mContext = context;
        mIsSocial = isSocial;
        mSignUpFragment = signUpFragment;
        mData = createAccountFormData;

        if (mIsSocial == 1) {
            onClickSignUpBtn(mData);
        }
    }

    public void onClickShowHidePassword(@SuppressWarnings("UnusedParameters") View view, boolean isFromPassword) {
        if (isFromPassword) {
            if (!isPasswordVisible) {
                mSignUpFragment.mBinding.passwordEt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mSignUpFragment.mBinding.passwordEt.setSelection(mSignUpFragment.mBinding.passwordEt.length());
                isPasswordVisible = true;
                view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_strikethrough_diagonal));
            } else {
                mSignUpFragment.mBinding.passwordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                mSignUpFragment.mBinding.passwordEt.setSelection(mSignUpFragment.mBinding.passwordEt.length());
                isPasswordVisible = false;
                view.setBackground(null);
            }
        } else {
            if (!isConfirmPasswordVisible) {
                mSignUpFragment.mBinding.confirmpasswordEt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mSignUpFragment.mBinding.confirmpasswordEt.setSelection(mSignUpFragment.mBinding.confirmpasswordEt.length());
                isConfirmPasswordVisible = true;
                view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_strikethrough_diagonal));
            } else {
                mSignUpFragment.mBinding.confirmpasswordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                mSignUpFragment.mBinding.confirmpasswordEt.setSelection(mSignUpFragment.mBinding.confirmpasswordEt.length());
                isConfirmPasswordVisible = false;
                view.setBackground(null);
            }
        }
    }

    public void onClickOpenCalender(View view, final String dobFormat) {

        final DatePickerDialog.OnDateSetListener date_of_birth = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                dobCalendar.set(Calendar.YEAR, year);
                dobCalendar.set(Calendar.MONTH, month);
                dobCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dob = new SimpleDateFormat(dobFormat, Locale.US);

                mSignUpFragment.getBinding().dobEt.setText(dob.format(dobCalendar.getTime()));
            }
        };

        new DatePickerDialog(mSignUpFragment.getContext(), date_of_birth, dobCalendar.get(Calendar.YEAR), dobCalendar.get(Calendar.MONTH), dobCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void onClickSignUpBtn(CreateAccountFormData data) {
        mData = data;

        if (mIsSocial == 1 || mData.isFormValidated(mSignUpFragment)) {
            Utils.hideKeyboard((LoginAndSignUpActivity) mContext);
            showDefaultAlertDialog(mSignUpFragment.getContext());

            ApiConnection.getSignUpData(
                    FirebaseInstanceId.getInstance().getToken()
                    , AppSharedPref.getStoreId(mContext)
                    , ApplicationConstant.DEFAULT_WEBSITE_ID
                    , AppSharedPref.getQuoteId(mContext)
                    , mData.getPrefix()
                    , mData.getFirstName()
                    , mData.getMiddleName()
                    , mData.getLastName()
                    , mData.getSuffix()
                    , mData.getDob()
                    , mData.getTaxVat()
                    , mData.getGender()
                    , mData.getEmailAddr()
                    , mData.getPassword()
                    , mData.getPictureURL()
                    , mIsSocial
                    , mData.getMobile()
                    , mData.getShopURL()
                    , mData.isSignUpAsSeller()
                    , "android")

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<SignUpResponseData>(mContext) {
                @Override
                public void onNext(SignUpResponseData signUpResponseData) {
                    super.onNext(signUpResponseData);
                    onSignUpResponseRecieved(signUpResponseData);
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
            mIsSocial = 0;
        } else {
            SnackbarHelper.getSnackbar((LoginAndSignUpActivity) mSignUpFragment.getContext(), mContext.getString(R.string.msg_fill_req_field)).show();
        }
    }

    private void onSignUpResponseRecieved(SignUpResponseData signUpResponseData) {
        dismiss(mContext);
        if (signUpResponseData.isSuccess()) {
            FirebaseAnalyticsImpl.logSignUpEvent(mContext, signUpResponseData.getCustomerId(), signUpResponseData.getCustomerName());
            ((LoginAndSignUpActivity) mContext).updateCartBadge(signUpResponseData.getCartCount());
            updateCustomerSharedPref(signUpResponseData);
            showToast(mSignUpFragment.getContext(), signUpResponseData.getMessage(), Toast.LENGTH_SHORT, 0);
            ((LoginAndSignUpActivity) mContext).setResult(RESULT_OK, new Intent());
            ((LoginAndSignUpActivity) mContext).finish();
        } else {
            mSignUpFragment.mBinding.scrollView.setVisibility(View.VISIBLE);
            mData.isFormValidated(mSignUpFragment);
            Snackbar snackbar = SnackbarHelper.getSnackbar(((LoginAndSignUpActivity) mContext), signUpResponseData.getMessage());
            snackbar.show();
        }
    }

    protected void updateCustomerSharedPref(SignUpResponseData jsonObject) {
        try {
            SharedPreferences.Editor customerDataSharedPref = AppSharedPref.getSharedPreferenceEditor(mContext, CUSTOMER_PREF);
            customerDataSharedPref.putBoolean(KEY_CUSTOMER_IS_LOGGED_IN, true);
            customerDataSharedPref.putString(KEY_CUSTOMER_NAME, jsonObject.getCustomerName());
            customerDataSharedPref.putString(KEY_CUSTOMER_EMAIL, jsonObject.getCustomerEmail());
            customerDataSharedPref.putInt(KEY_CUSTOMER_ID, jsonObject.getCustomerId());
            customerDataSharedPref.putInt(KEY_QUOTE_ID, DEFAULT_QUOTE_ID);
            customerDataSharedPref.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}