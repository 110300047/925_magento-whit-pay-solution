package com.webkul.mobikul.handler;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.fragment.ProductFragment;
import com.webkul.mobikul.fragment.ProductQuickViewDialogFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.ProductData;
import com.webkul.mobikul.model.customer.wishlist.CatalogProductAddToWishlistResponse;
import com.webkul.mobikul.model.product.ProductAddToCartResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECTED_PRODUCT_NUMBER;
import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.ProductHelper.getProductShortData;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 6/1/17. @Webkul Software Private limited
 */

public class UpsellProductRvHandler {

    private ProductFragment mProductFragmentContext;
    private ArrayList<ProductData> mProductsDatas;
    private int mProductId;
    private int mQty = 1;
    private String mProductName;
    private int mProductPosition;
    private boolean mIsProcessing;

    public UpsellProductRvHandler(ProductFragment context, ArrayList<ProductData> productsDatas) {
        this.mProductFragmentContext = context;
        mProductsDatas = productsDatas;
    }

    public void onClickOpenProduct(View view, int selectedProduct) {
        Intent intent = new Intent(view.getContext(), NewProductActivity.class);
        intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, getProductShortData(mProductsDatas));
        intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
        view.getContext().startActivity(intent);
    }

    public void onClickAddToWishlist(final View view, int productId, String productName, int productPosition, final int itemId) {
        if (!mIsProcessing) {
            if (AppSharedPref.isLoggedIn(mProductFragmentContext.getContext())) {
                mProductId = productId;
                mProductName = productName;
                mProductPosition = productPosition;

                if (mProductFragmentContext.mCatalogSingleProductData.getUpsellProductListData().get(mProductPosition).isInWishlist()) {
                    showAlertDialogWithClickListener(mProductFragmentContext.getContext(), SweetAlertDialog.WARNING_TYPE, mProductFragmentContext.getContext().getString(R.string.warning_are_you_sure), mProductFragmentContext.getContext().getString(R.string.question_want_to_remove_this_product_from_whislist),
                            mProductFragmentContext.getContext().getString(R.string.message_yes_remove_it), mProductFragmentContext.getContext().getString(R.string.no), new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismiss();
                                    ApiConnection.removeItemFromWishlist(AppSharedPref.getCustomerId(mProductFragmentContext.getContext())
                                            , AppSharedPref.getStoreId(mProductFragmentContext.getContext())
                                            , itemId)

                                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mProductFragmentContext.getContext()) {
                                        @Override
                                        public void onNext(BaseModel removeItemFromWishlistResponse) {
                                            super.onNext(removeItemFromWishlistResponse);
                                            mIsProcessing = false;
                                            if (removeItemFromWishlistResponse.isSuccess()) {
                                                mProductFragmentContext.mCatalogSingleProductData.getUpsellProductListData().get(mProductPosition).setInWishlist(false);
                                                mProductFragmentContext.mCatalogSingleProductData.getUpsellProductListData().get(mProductPosition).setWishlistItemId(0);
                                            } else {
                                                ((LottieAnimationView) view).cancelAnimation();
                                                ((LottieAnimationView) view).setProgress(1);
                                                showSuccessOrErrorTypeAlertDialog(mProductFragmentContext.getContext(), SweetAlertDialog.ERROR_TYPE, mProductFragmentContext.getContext().getString(R.string.something_went_wrong), removeItemFromWishlistResponse.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable t) {
                                            super.onError(t);
                                            mIsProcessing = false;
                                            ((LottieAnimationView) view).cancelAnimation();
                                            ((LottieAnimationView) view).setProgress(1);
                                        }
                                    });
                                    mIsProcessing = true;
                                    ((LottieAnimationView) view).reverseAnimation();
                                }
                            }, null);
                } else {
                    mIsProcessing = true;
                    ((LottieAnimationView) view).playAnimation();

                    FirebaseAnalyticsImpl.logAddToWishlistEvent(mProductFragmentContext.getContext(), mProductId, mProductName);

                    ApiConnection.addToWishlist(AppSharedPref.getStoreId(mProductFragmentContext.getContext())
                            , mProductId
                            , AppSharedPref.getCustomerId(mProductFragmentContext.getContext())
                            , new JSONObject())

                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<CatalogProductAddToWishlistResponse>(mProductFragmentContext.getContext()) {
                        @Override
                        public void onNext(CatalogProductAddToWishlistResponse addToWishlistResponse) {
                            super.onNext(addToWishlistResponse);
                            mIsProcessing = false;
                            if (addToWishlistResponse.isSuccess()) {
                                mProductFragmentContext.mCatalogSingleProductData.getUpsellProductListData().get(mProductPosition).setInWishlist(true);
                                mProductFragmentContext.mCatalogSingleProductData.getUpsellProductListData().get(mProductPosition).setWishlistItemId(addToWishlistResponse.getItemId());
                            } else {
                                ((LottieAnimationView) view).cancelAnimation();
                                ((LottieAnimationView) view).setProgress(0);
                                showSuccessOrErrorTypeAlertDialog(mProductFragmentContext.getContext(), SweetAlertDialog.ERROR_TYPE, mProductFragmentContext.getContext().getString(R.string.something_went_wrong), addToWishlistResponse.getMessage());
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            mIsProcessing = false;
                            ((LottieAnimationView) view).cancelAnimation();
                            ((LottieAnimationView) view).setProgress(0);
                        }
                    });
                }
            } else {
                showToast(mProductFragmentContext.getContext(), mProductFragmentContext.getString(R.string.login_first), Toast.LENGTH_SHORT, 0);
            }
        }
    }

    public void onClickAddToCompare(View view, int productId, String productName, int productPosition) {
        showDefaultAlertDialog(mProductFragmentContext.getContext());

        mProductId = productId;
        mProductName = productName;

        ApiConnection.addToCompare(AppSharedPref.getStoreId(mProductFragmentContext.getContext())
                , AppSharedPref.getCustomerId(view.getContext())
                , mProductId)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mProductFragmentContext.getContext()) {
            @Override
            public void onNext(BaseModel addToCompareResponse) {
                super.onNext(addToCompareResponse);
                showToast(mProductFragmentContext.getContext(), addToCompareResponse.getMessage(), Toast.LENGTH_SHORT, 0);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    public void onClickItem(View view, int productId, String productName, boolean haveOptions, int selectedProduct) {
        if (haveOptions) {
            Intent intent = new Intent(view.getContext(), NewProductActivity.class);
            intent.putParcelableArrayListExtra(BUNDLE_KEY_PRODUCT_PAGE_DATA_LIST, mProductsDatas);
            intent.putExtra(BUNDLE_KEY_SELECTED_PRODUCT_NUMBER, selectedProduct);
            view.getContext().startActivity(intent);
        } else {
            mProductName = productName;
            mProductId = productId;

            FirebaseAnalyticsImpl.logAddToCartEvent(mProductFragmentContext.getContext(), mProductId, mProductName);

            showDefaultAlertDialog(mProductFragmentContext.getContext());

            ApiConnection.addToCart(AppSharedPref.getStoreId(mProductFragmentContext.getContext())
                    , mProductId
                    , mQty
                    , new JSONObject()
                    , new JSONArray()
                    , AppSharedPref.getQuoteId(view.getContext())
                    , AppSharedPref.getCustomerId(view.getContext()))

                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ProductAddToCartResponse>(mProductFragmentContext.getContext()) {
                @Override
                public void onNext(ProductAddToCartResponse productAddToCartResponse) {
                    super.onNext(productAddToCartResponse);
                    onResponseAddToCart(productAddToCartResponse);
                }

                @Override
                public void onError(Throwable t) {
                    super.onError(t);
                }
            });
        }
    }

    private void onResponseAddToCart(ProductAddToCartResponse productAddToCartResponse) {
        if (productAddToCartResponse.getSuccess()) {
            showToast(mProductFragmentContext.getContext(), productAddToCartResponse.getMessage(), Toast.LENGTH_LONG, 0);

            if (productAddToCartResponse.getQuoteId() != 0) {
                AppSharedPref.setQuoteId(mProductFragmentContext.getContext(), productAddToCartResponse.getQuoteId());
            }
            ((BaseActivity) mProductFragmentContext.getContext()).updateCartBadge(productAddToCartResponse.getCartCount());
        } else {
            showSuccessOrErrorTypeAlertDialog(mProductFragmentContext.getContext(), SweetAlertDialog.ERROR_TYPE, mProductName, productAddToCartResponse.getMessage());
        }
    }

    public void onClickConfigureProduct(View view, ProductData productData) {
        FragmentManager supportFragmentManager = mProductFragmentContext.getChildFragmentManager();
        ProductQuickViewDialogFragment productQuickViewDialogFragment = ProductQuickViewDialogFragment.newInstance("upSell", productData);
        productQuickViewDialogFragment.show(supportFragmentManager, ProductQuickViewDialogFragment.class.getSimpleName());
    }
}