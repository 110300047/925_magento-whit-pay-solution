package com.webkul.mobikul.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.helper.MobikulApplication;


/**
 * Created by vedesh.kumar on 29/12/16. @Webkul Software Pvt. Ltd
 */

public class WishListActivityHandler {

    public void onClickStartShopping(View view) {
        Intent intent = new Intent(view.getContext(), ((MobikulApplication) view.getContext().getApplicationContext()).getHomePageClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        view.getContext().startActivity(intent);
    }
}
