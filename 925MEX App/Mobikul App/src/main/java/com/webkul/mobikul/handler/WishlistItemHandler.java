package com.webkul.mobikul.handler;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.activity.WishlistActivty;
import com.webkul.mobikul.adapter.WishlistRvAdapter;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.customer.wishlist.AddToCartFromWishListResponse;
import com.webkul.mobikul.model.customer.wishlist.WishlistItemData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.webkul.mobikul.helper.AlertDialogHelper.showAlertDialogWithClickListener;
import static com.webkul.mobikul.helper.AlertDialogHelper.showDefaultAlertDialog;
import static com.webkul.mobikul.helper.AlertDialogHelper.showSuccessOrErrorTypeAlertDialog;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_PRODUCT_NAME;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class WishlistItemHandler {

    private final Context mContext;
    private final WishlistRvAdapter mWishlistRvAdapter;
    private int mPosition;
    private JSONArray mUpdateWishListJSONArray;
    private int mItemId;
    private int mProductId;
    private int mQty;
    private String mProductName;

    public WishlistItemHandler(Context context, WishlistRvAdapter wishlistRvAdapter) {
        mContext = context;
        mWishlistRvAdapter = wishlistRvAdapter;
    }

    public void onClickIncrementQty(View view) {
        int currentPostion = (int) view.getTag();
        mWishlistRvAdapter.getWishlistItem(currentPostion).setQty(String.valueOf(Integer.parseInt(mWishlistRvAdapter.getWishlistItem(currentPostion).getQty()) + 1));
        mWishlistRvAdapter.notifyDataSetChanged();
    }

    public void onClickDecrementQty(View view) {
        int currentPostion = (int) view.getTag();
        mWishlistRvAdapter.getWishlistItem(currentPostion).setQty(String.valueOf(Integer.parseInt(mWishlistRvAdapter.getWishlistItem(currentPostion).getQty()) - 1));
        mWishlistRvAdapter.notifyDataSetChanged();
    }

    public void onClickRemoveWishlistItem(View view, final int itemId, int position) {
        mItemId = itemId;
        mPosition = position;
        showAlertDialogWithClickListener(mContext, SweetAlertDialog.WARNING_TYPE, mContext.getString(R.string.warning_are_you_sure), mContext.getString(R.string.question_want_to_remove_this_product_from_whislist), mContext.getString(R.string.message_yes_remove_it),
                mContext.getString(R.string.no), new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        ApiConnection.removeItemFromWishlist(AppSharedPref.getCustomerId(mContext)
                                , AppSharedPref.getStoreId(mContext)
                                , itemId)

                                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
                            @Override
                            public void onNext(BaseModel removeItemFromWishlistResponse) {
                                super.onNext(removeItemFromWishlistResponse);
                                onResponseRemoveItemFromWishlist(removeItemFromWishlistResponse);
                            }

                            @Override
                            public void onError(Throwable t) {
                                super.onError(t);
                            }
                        });
                        showDefaultAlertDialog(mContext);
                    }
                }, null);
    }

    private void onResponseRemoveItemFromWishlist(BaseModel removeItemFromWishlistResponse) {
        if (removeItemFromWishlistResponse.isSuccess()) {
            showToast(mContext, removeItemFromWishlistResponse.getMessage(), Toast.LENGTH_SHORT, 0);

            ((WishlistRvAdapter) ((WishlistActivty) mContext).mBinding.wishlistRv.getAdapter()).remove(mPosition);
            ((WishlistActivty) mContext).mWishlistData.setTotalCount(((WishlistActivty) mContext).mWishlistData.getTotalCount() - 1);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((WishlistActivty) mContext).mBinding.wishlistRv.getAdapter().notifyDataSetChanged();
                    if (((WishlistActivty) mContext).mBinding.wishlistRv.getAdapter().getItemCount() == 0) {
                        ((WishlistActivty) mContext).mBinding.setData(((WishlistActivty) mContext).mWishlistData);
                        ((WishlistActivty) mContext).mBinding.executePendingBindings();
                    }
                }
            }, 1000);
        } else {
            showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.something_went_wrong), removeItemFromWishlistResponse.getMessage());
        }
    }

    public void onClickAddToCartFromWishlistItem(View view, int itemId, int productId, String qty, String productName) {
        mItemId = itemId;
        mProductId = productId;
        mQty = Integer.parseInt(qty);
        mProductName = productName;

        showDefaultAlertDialog(mContext);

        ApiConnection.addToCartFromWishlist(
                AppSharedPref.getCustomerId(mContext)
                , AppSharedPref.getStoreId(mContext)
                , mProductId
                , mItemId
                , mQty)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<AddToCartFromWishListResponse>(mContext) {
            @Override
            public void onNext(AddToCartFromWishListResponse addToCartFromWishListResponse) {
                super.onNext(addToCartFromWishListResponse);
                try {
                    if (addToCartFromWishListResponse.isSuccess()) {
                        showToast(mContext, addToCartFromWishListResponse.getMessage(), Toast.LENGTH_LONG, 0);
                        ((BaseActivity) mContext).updateCartBadge(addToCartFromWishListResponse.getCartCount());
                        Intent intent = new Intent(mContext, WishlistActivty.class);
                        mContext.startActivity(intent);
                    } else {
                        if (addToCartFromWishListResponse.getMessage().contains("You need to choose") || addToCartFromWishListResponse.getMessage().contains("Please specify")) {
                            Intent intent = new Intent(mContext, NewProductActivity.class);
                            intent.putExtra(KEY_PRODUCT_ID, mProductId);
                            intent.putExtra(KEY_PRODUCT_NAME, "");
                            mContext.startActivity(intent);
                        }

                        if (addToCartFromWishListResponse.getMessage().isEmpty()) {
                            showToast(mContext, mContext.getString(R.string.something_went_wrong), Toast.LENGTH_LONG, 0);
                        } else {
                            showToast(mContext, addToCartFromWishListResponse.getMessage(), Toast.LENGTH_LONG, 0);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    public void onClickWishlistUpdatetBtn(View view, String description) {
        mUpdateWishListJSONArray = new JSONArray();
        List<WishlistItemData> wishlistItemDataList = mWishlistRvAdapter.getWishlistDataList();
        for (int wishlistDataIndex = 0; wishlistDataIndex < wishlistItemDataList.size(); wishlistDataIndex++) {
            WishlistItemData eachWishListItemData = wishlistItemDataList.get(wishlistDataIndex);
            try {
                JSONObject temp = new JSONObject();
                temp.put("id", eachWishListItemData.getId());
                temp.put("description", eachWishListItemData.getDescription());
                temp.put("qty", eachWishListItemData.getQty());
                mUpdateWishListJSONArray.put(wishlistDataIndex, temp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        showDefaultAlertDialog(mContext);
        ApiConnection.updateWishlist(
                AppSharedPref.getCustomerId(view.getContext())
                , mUpdateWishListJSONArray)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<BaseModel>(mContext) {
            @Override
            public void onNext(BaseModel updateWishlistResponse) {
                super.onNext(updateWishlistResponse);
                if (updateWishlistResponse.isSuccess()) {
                    ToastHelper.showToast(mContext, updateWishlistResponse.getMessage(), Toast.LENGTH_LONG, 0);
                    ((WishlistActivty) mContext).handleIntent();
                } else {
                    showSuccessOrErrorTypeAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE, mContext.getString(R.string.something_went_wrong), updateWishlistResponse.getMessage());
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
            }
        });
    }

    public void onClickWishlistItem(View view, int itemId, String itemName) {
        Intent intent = new Intent(mContext, NewProductActivity.class);
        intent.putExtra(KEY_PRODUCT_ID, itemId);
        intent.putExtra(KEY_PRODUCT_NAME, itemName);
        mContext.startActivity(intent);
    }
}
