package com.webkul.mobikul.helper;


import com.webkul.mobikul.model.customer.address.CountryData;

import java.util.List;

/**
 * Created by vedesh.kumar on 19/1/17. @Webkul Software Pvt. Ltd
 */

public class AddressHelper {

    public static int getCountryPositionFromCountryCode(List<CountryData> countryDatas, String countryCode) {
        for (int countryPosition = 0; countryPosition < countryDatas.size(); countryPosition++) {
            if (countryDatas.get(countryPosition).getCountryId().equals(countryCode)) {
                return countryPosition;
            }
        }
        return 0;
    }

    public static int getCountryPositionFromCountryName(List<CountryData> countryDatas, String countryName) {
        for (int countryPosition = 0; countryPosition < countryDatas.size(); countryPosition++) {
            if (countryDatas.get(countryPosition).getName().equals(countryName)) {
                return countryPosition;
            }
        }
        return 0;
    }

    @SuppressWarnings("unused")
    public static String getCountryId(List<CountryData> countryDatas, String countryName) {
        for (int countryPosition = 0; countryPosition < countryDatas.size(); countryPosition++) {
            if (countryDatas.get(countryPosition).getName().equals(countryName)) {
                return countryDatas.get(countryPosition).getCountryId();
            }
        }
        return countryDatas.get(0).getCountryId();
    }

}
