package com.webkul.mobikul.helper;

import android.content.Context;
import android.widget.LinearLayout;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by vedesh.kumar on 9/2/17. @Webkul Software Pvt. Ltd
 */

public class AlertDialogHelper {

    public static void showDefaultAlertDialog(Context context) {
        ((BaseActivity) context).mSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        ((BaseActivity) context).mSweetAlertDialog.setTitleText(context.getString(R.string.please_wait));
        ((BaseActivity) context).mSweetAlertDialog.getProgressHelper().setBarColor(context.getResources().getColor(R.color.colorAccent));
        ((BaseActivity) context).mSweetAlertDialog.setCancelable(false);
        ((BaseActivity) context).mSweetAlertDialog.show();
    }

    public static void showSuccessOrErrorTypeAlertDialog(Context context, int type, String title, String content) {
        ((BaseActivity) context).mSweetAlertDialog = new SweetAlertDialog(context, type)
                .setTitleText(title)
                .setContentText(content)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });
        ((BaseActivity) context).mSweetAlertDialog.setCancelable(true);
        ((BaseActivity) context).mSweetAlertDialog.show();
        customizeDialogButtons(context);
    }

    public static void showAlertDialogWithClickListener(Context context, int type, String title, String content, String confirmText, String cancelText, SweetAlertDialog.OnSweetClickListener confirmClickListener, SweetAlertDialog.OnSweetClickListener cancelClickListener) {
        ((BaseActivity) context).mSweetAlertDialog = new SweetAlertDialog(context, type);
        ((BaseActivity) context).mSweetAlertDialog.setTitleText(title);
        ((BaseActivity) context).mSweetAlertDialog.setContentText(content);
        if (!confirmText.isEmpty())
            ((BaseActivity) context).mSweetAlertDialog.setConfirmText(confirmText);
        if (!cancelText.isEmpty())
            ((BaseActivity) context).mSweetAlertDialog.setCancelText(cancelText);
        if (confirmClickListener != null)
            ((BaseActivity) context).mSweetAlertDialog.setConfirmClickListener(confirmClickListener);
        if (cancelClickListener != null)
            ((BaseActivity) context).mSweetAlertDialog.setCancelClickListener(cancelClickListener);
        ((BaseActivity) context).mSweetAlertDialog.setCancelable(false);
        ((BaseActivity) context).mSweetAlertDialog.show();
        customizeDialogButtons(context);
    }

    private static void customizeDialogButtons(Context context) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, (int) context.getResources().getDimension(R.dimen.contextual_icon_dimens));
        params.setMargins(15, 5, 15, 5);
        ((BaseActivity) context).mSweetAlertDialog.findViewById(R.id.confirm_button).setLayoutParams(params);
        ((BaseActivity) context).mSweetAlertDialog.findViewById(R.id.confirm_button).setPadding(30, 0, 30, 0);
//        ((BaseActivity) context).mSweetAlertDialog.findViewById(R.id.confirm_button).setBackground(context.getResources().getDrawable(R.drawable.btn_custom_style_small_radius));
        ((BaseActivity) context).mSweetAlertDialog.findViewById(R.id.cancel_button).setLayoutParams(params);
        ((BaseActivity) context).mSweetAlertDialog.findViewById(R.id.cancel_button).setPadding(30, 0, 30, 0);
//        ((BaseActivity) context).mSweetAlertDialog.findViewById(R.id.cancel_button).setBackground(context.getResources().getDrawable(R.drawable.btn_custom_style_small_radius));
    }

    public static void dismiss(Context context) {
        try {
            if (((BaseActivity) context).mSweetAlertDialog != null) {
                ((BaseActivity) context).mSweetAlertDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}