package com.webkul.mobikul.helper;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_CURRENCY_CODE;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_CUSTOMER_ID;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_QUOTE_ID;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_STORE_CODE;
import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_STORE_ID;
import static com.webkul.mobikul.constants.ApplicationConstant.VIEW_TYPE_GRID;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */


//IN THIS FILE RETURN TYPE OF VARIOUS HAS BEEN TYPECASTED DUE TO CONFLICTS IN SOME DEVICES

public class AppSharedPref {

    public static final String CUSTOMER_PREF = "customerPreference";
    public static final String CONFIGURATION_PREF = "configurationPreference";
    public static final String FINGERPRINT_PREF = "fingerprintPreference";
    public static final String PRICE_FORMAT_PREF = "priceFormatPreference";

    public static final String KEY_PRICE_PATTERN = "pattern";
    public static final String KEY_PRICE_PRECISION = "precision";

    public static final String KEY_STORE_CODE = "storeCode";
    public static final String KEY_STORE_ID = "storeId";
    public static final String KEY_QUOTE_ID = "quoteId";
    public static final String KEY_CURRENCY_CODE = "currencyCode";
    public static final String KEY_VIEW_TYPE = "viewType";
    public static final String KEY_IS_MOBILE_LOGIN_ENABLED = "isMobileLoginEnabled";

    public static final String KEY_CUSTOMER_NAME = "customerName";
    public static final String KEY_CUSTOMER_EMAIL = "customerEmail";
    public static final String KEY_CUSTOMER_PROFILE_IMAGE = "customerProfileImage";
    public static final String KEY_CUSTOMER_BANNER_IMAGE = "customerBannerImage";
    public static final String KEY_CUSTOMER_ID = "customerId";
    public static final String KEY_CUSTOMER_IS_LOGGED_IN = "isLoggedIn";
    public static final String KEY_CUSTOMER_FINGER_USER_NAME = "fingerUsername";
    public static final String KEY_CUSTOMER_FINGER_PASSWORD = "fingerPassword";

    public static final String KEY_CART_COUNT = "cartCount";
    public static final String KEY_PRODUCT_ID = "productId";
    public static final String KEY_PRODUCT_NAME = "productName";
    public static final String KEY_CATEGORY_ID = "categoryId";
    public static final String KEY_CATEGORY_NAME = "categoryName";
    public static final String KEY_TITLE = "title";
    public static final boolean DEFAULT_MOILE_LOGIN_VALUE = false;

    public static SharedPreferences getSharedPreference(Context context, String preferenceFile) {
        return context.getSharedPreferences(preferenceFile, MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getSharedPreferenceEditor(Context context, String preferenceFile) {
        return context.getSharedPreferences(preferenceFile, MODE_PRIVATE).edit();
    }

    public static String getStoreCode(Context context) {
        return getSharedPreference(context, CONFIGURATION_PREF).getString(KEY_STORE_CODE, DEFAULT_STORE_CODE);
    }

    public static void setStoreCode(Context context, String languageCode) {
        getSharedPreferenceEditor(context, CONFIGURATION_PREF).putString(KEY_STORE_CODE, languageCode).commit();
    }

    public static int getStoreId(Context context) {
        try {
            return getSharedPreference(context, CONFIGURATION_PREF).getInt(KEY_STORE_ID, DEFAULT_STORE_ID);
        } catch (ClassCastException e) {
            return Integer.parseInt(getSharedPreference(context, CONFIGURATION_PREF).getString(KEY_STORE_ID, String.valueOf(DEFAULT_STORE_ID)));
        }
    }

    public static void setStoreId(Context context, int storeId) {
        getSharedPreferenceEditor(context, CONFIGURATION_PREF).putInt(KEY_STORE_ID, storeId).commit();
    }

    public static String getCurrencyCode(Context context) {
        return getSharedPreference(context, CONFIGURATION_PREF).getString(KEY_CURRENCY_CODE, DEFAULT_CURRENCY_CODE);
    }

    public static void setCurrencyCode(Context context, String currencyCode) {
        getSharedPreferenceEditor(context, CONFIGURATION_PREF).putString(KEY_CURRENCY_CODE, currencyCode).commit();
    }

    public static int getViewType(Context context) {
        return getSharedPreference(context, CONFIGURATION_PREF).getInt(KEY_VIEW_TYPE, VIEW_TYPE_GRID);
    }

    public static void setViewType(Context context, int viewType) {
        getSharedPreferenceEditor(context, CONFIGURATION_PREF).putInt(KEY_VIEW_TYPE, viewType).commit();
    }

    /*Price Format*/

    public static String getPattern(Context context) {
        return getSharedPreference(context, PRICE_FORMAT_PREF).getString(KEY_PRICE_PATTERN, "");
    }

    public static int getPrecision(Context context) {
        return getSharedPreference(context, PRICE_FORMAT_PREF).getInt(KEY_PRICE_PRECISION, 2);
    }

    /*Customer*/

    public static int getQuoteId(Context context) {
        try {
            return getSharedPreference(context, CUSTOMER_PREF).getInt(KEY_QUOTE_ID, DEFAULT_QUOTE_ID);
        } catch (ClassCastException e) {
            e.printStackTrace();
            if (!getSharedPreference(context, CUSTOMER_PREF).getString(KEY_QUOTE_ID, String.valueOf(DEFAULT_QUOTE_ID)).equals(""))
                return Integer.parseInt(getSharedPreference(context, CUSTOMER_PREF).getString(KEY_QUOTE_ID, String.valueOf(DEFAULT_QUOTE_ID)));
            else
                return DEFAULT_QUOTE_ID;
        }
    }

    public static void setQuoteId(Context context, int quoteId) {
        getSharedPreferenceEditor(context, CUSTOMER_PREF).putInt(KEY_QUOTE_ID, quoteId).commit();
    }

    public static boolean isLoggedIn(Context context) {
        try {
            return getSharedPreference(context, CUSTOMER_PREF).getBoolean(KEY_CUSTOMER_IS_LOGGED_IN, false);
        } catch (ClassCastException e) {
            return Boolean.parseBoolean(getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CUSTOMER_IS_LOGGED_IN, String.valueOf(false)));
        }
    }

    public static String getCustomerName(Context context) {
        return getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CUSTOMER_NAME, "");
    }

    public static String getCustomerEmail(Context context) {
        return getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CUSTOMER_EMAIL, "");
    }

    public static int getCustomerId(Context context) {
        try {

            return getSharedPreference(context, CUSTOMER_PREF).getInt(KEY_CUSTOMER_ID, DEFAULT_CUSTOMER_ID);
        } catch (ClassCastException e) {
            return Integer.parseInt(getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CUSTOMER_ID, String.valueOf(DEFAULT_CUSTOMER_ID)));
        }
    }

    public static String getCustomerProfileImage(Context context, String defaultValue) {
        return getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CUSTOMER_PROFILE_IMAGE, defaultValue);
    }

    public static String getCustomerBannerImage(Context context, String defaultValue) {
        return getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CUSTOMER_BANNER_IMAGE, defaultValue);
    }

    public static int getCartCount(Context context, int defaultValue) {
        try {
            return getSharedPreference(context, CUSTOMER_PREF).getInt(KEY_CART_COUNT, defaultValue);
        } catch (ClassCastException e) {
            return Integer.parseInt(getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CART_COUNT, String.valueOf(defaultValue)));
        }
    }

    public static void setMobileLoginEnabled(Context context, boolean isMobileLoginEnabled) {
        getSharedPreferenceEditor(context, CONFIGURATION_PREF).putBoolean(KEY_IS_MOBILE_LOGIN_ENABLED, isMobileLoginEnabled).commit();
    }

    public static boolean getMobileLoginEnabled(Context context) {
        try {

            return getSharedPreference(context, CONFIGURATION_PREF).getBoolean(KEY_IS_MOBILE_LOGIN_ENABLED, DEFAULT_MOILE_LOGIN_VALUE);
        } catch (ClassCastException e) {
            return Boolean.parseBoolean(getSharedPreference(context, CONFIGURATION_PREF).getString(KEY_IS_MOBILE_LOGIN_ENABLED, String.valueOf(DEFAULT_MOILE_LOGIN_VALUE)));
        }
    }

    /*Fingerprint*/

    public static String getCustomerFingerUserName(Context context) {
        return getSharedPreference(context, FINGERPRINT_PREF).getString(KEY_CUSTOMER_FINGER_USER_NAME, "");
    }

    public static String getCustomerFingerPassword(Context context) {
        return getSharedPreference(context, FINGERPRINT_PREF).getString(KEY_CUSTOMER_FINGER_PASSWORD, "");
    }

}