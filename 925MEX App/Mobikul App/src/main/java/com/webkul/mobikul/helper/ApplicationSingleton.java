package com.webkul.mobikul.helper;

/**
 * Created by vedesh.kumar on 12/10/16. @Webkul Software Pvt. Ltd
 */
public class ApplicationSingleton {
    private static String authKey = "";
    private static ApplicationSingleton mInstance;

    public static ApplicationSingleton getInstance() {
        if (mInstance == null) {
            mInstance = new ApplicationSingleton();
        }
        return mInstance;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        ApplicationSingleton.authKey = authKey;
    }
}