package com.webkul.mobikul.helper;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.webkul.mobikul.R;
import com.webkul.mobikul.model.catalog.SwatchData;

import java.util.Locale;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_RECTANGLE_RADIUS;
import static com.webkul.mobikul.constants.ApplicationConstant.HOST_NAME;
import static com.webkul.mobikul.constants.BundleKeyHelper.ORDER_STATUS_CANCELLED;
import static com.webkul.mobikul.constants.BundleKeyHelper.ORDER_STATUS_CLOSED;
import static com.webkul.mobikul.constants.BundleKeyHelper.ORDER_STATUS_COMPLETE;
import static com.webkul.mobikul.constants.BundleKeyHelper.ORDER_STATUS_HOLD;
import static com.webkul.mobikul.constants.BundleKeyHelper.ORDER_STATUS_NEW;
import static com.webkul.mobikul.constants.BundleKeyHelper.ORDER_STATUS_PENDING;
import static com.webkul.mobikul.constants.BundleKeyHelper.ORDER_STATUS_PROCESSING;
import static com.webkul.mobikul.helper.Utils.isTablet;

/**
 * Created by vedesh.kumar on 15/12/16. @Webkul Software Pvt. Ltd
 */

public class BindingAdapterUtils {

    @BindingAdapter(value = {"imageUrl", "placeholder"}, requireAll = false)
    public static void setImageUrl(ImageView view, String imageUrl, Drawable placeholder) {
        ImageHelper.load(view, imageUrl, placeholder);
    }

    @BindingAdapter("imageDrawableId")
    public static void setImageByDrawableId(ImageView view, int drawableId) {
        view.setImageDrawable(ContextCompat.getDrawable(view.getContext(), drawableId));
    }

    @BindingAdapter("textDrawable")
    public static void setTextDrawable(ImageView view, String language) {
        TextDrawable textDrawable = TextDrawable.builder()
                .beginConfig()
                .textColor(Color.BLACK)
                .bold()
                .toUpperCase()
                .withBorder(2) /* thickness in px */
                .endConfig()
                .buildRoundRect(String.valueOf(language.charAt(0)).toUpperCase(Locale.getDefault()), Color.WHITE, DEFAULT_RECTANGLE_RADIUS);
        view.setImageDrawable(textDrawable);
    }

    @BindingAdapter("error")
    public static void setError(EditText view, String errorMessage) {
        view.setError(errorMessage);
    }

    @BindingAdapter("srcCompat")
    public static void setImageDrawable(ImageView view, int drawableId) {
        view.setImageResource(drawableId);
    }

    @BindingAdapter("srcCompat")
    public static void setImageDrawable(ImageView view, Drawable drawableId) {
        view.setImageDrawable(drawableId);
    }

    @BindingAdapter("orderStatusBackground")
    public static void setOrderStatusBackground(TextView view, String status) {
        Drawable drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.btn_style_order_default);

        if (status != null) {
            switch (status.toLowerCase()) {
                case ORDER_STATUS_COMPLETE:
                    drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.btn_style_complete);
                    break;

                case ORDER_STATUS_PENDING:
                    drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.btn_style_pending);
                    break;

                case ORDER_STATUS_PROCESSING:
                    drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.btn_style_processing);
                    break;

                case ORDER_STATUS_HOLD:
                    drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.btn_style_hold);
                    break;

                case ORDER_STATUS_CANCELLED:
                    drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.btn_style_canceled);
                    break;

                case ORDER_STATUS_NEW:
                    drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.btn_style_new);
                    break;

                case ORDER_STATUS_CLOSED:
                    drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.btn_style_closed);
                    break;
            }
        }

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            //noinspection deprecation
            view.setBackgroundDrawable(drawable);
        } else {
            view.setBackground(drawable);
        }
    }

    @BindingAdapter("textColor")
    public static void setTextColor(TextView view, boolean isSelected) {
        if (isSelected) {
            view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorAccent));
            view.setTypeface(view.getTypeface(), Typeface.BOLD);
        } else {
            view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.text_color_primary));
            view.setTypeface(view.getTypeface(), Typeface.NORMAL);
        }
    }

    @BindingAdapter("priceTextColor")
    public static void setPriceTextColor(TextView view, boolean hasSpecialPrice) {
        if (hasSpecialPrice) {
            view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.text_color_primary));
        } else {
            view.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorAccent));
        }
    }

    @BindingAdapter(value = {"error", "displayError"}, requireAll = false)
    public static void setError(TextInputLayout view, String errorMessage, boolean displayError) {
        if (displayError) {
            view.setError(errorMessage);
            if (errorMessage.trim().isEmpty()) {
                view.setErrorEnabled(false);
            } else {
                view.setErrorEnabled(true);
            }
        } else {
            view.setErrorEnabled(false);
            view.setError(null);
        }
    }

    @BindingAdapter("underline")
    public static void setUnderline(TextView textView, boolean isUnderline) {
        if (isUnderline) {
            textView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        }
    }

    @BindingAdapter("backgroundColor")
    public static void setBackgroundColor(FrameLayout view, String color) {
        if (color != null && !color.isEmpty()) {
            view.setBackgroundColor(Color.parseColor(color));
        }
    }

    @BindingAdapter("loadData")
    public static void setLoadData(WebView webView, String content) {
        if (content == null) {
            return;
        }

        String htmlContent =
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" +
                        "<html lang=\"en\">" +
                        "<head>" +
                        "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" +
                        "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + HOST_NAME + "/pub/static/version1501785931/frontend/Magento/luma/en_US/css/styles-l.css\" media=\"all\">\n" +
                        "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + HOST_NAME + "/pub/static/version1501785931/frontend/Magento/luma/en_US/css/styles-m.css\" media=\"all\">" +
                        "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + HOST_NAME + "/pub/media/styles.css\" media=\"all\">" +
                        "</head>" +
                        "<body class=\"cms-index-index cms-page-view\" style=\"padding:5px;\">" +
                        "<div class=\"std\">" +
                        content +
                        "</div>" +
                        "</body>" +
                        "</html>";

        webView.loadData(htmlContent, "text/html", "UTF-8");
        WebSettings webSettings = webView.getSettings();
        if (isTablet(webView.getContext()))
            webSettings.setDefaultFontSize(20);
        else
            webSettings.setDefaultFontSize(14);

        LocaleUtils.setStoreLanguage(webView.getContext(), AppSharedPref.getStoreCode(webView.getContext()));
    }

    @BindingAdapter({"layout_width"})
    public static void setLayoutWidth(View view, int width) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = width;
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter({"layout_height"})
    public static void setLayoutHeight(View view, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("loadAddress")
    public static void setLoadAddress(TextView textView, String address) {
        if (address == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(address, Html.FROM_HTML_MODE_LEGACY));
        } else {
            //noinspection deprecation
            textView.setText(Html.fromHtml(address));
        }
    }

    @BindingAdapter("layout_marginBottom")
    public static void setLayoutMarginBottom(ViewGroup view, float marginBottom) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.bottomMargin = (int) marginBottom;
    }

    @BindingAdapter("loadHtmlText")
    public static void setLoadHtmlText(TextView textView, String htmlText) {
        if (htmlText == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY));
        } else {
            //noinspection deprecation
            textView.setText(Html.fromHtml(htmlText));
        }
    }

    @BindingAdapter(value = {"swatchData"})
    public static void loadSwatchData(ImageView imageView, SwatchData swatchData) {
        if (swatchData.getType().equals("1")) {
            imageView.setBackgroundColor(Color.parseColor(swatchData.getValue()));
        } else if (swatchData.getType().equals("2")) {
            ImageHelper.load(imageView, swatchData.getValue(), null);
        }
    }
}