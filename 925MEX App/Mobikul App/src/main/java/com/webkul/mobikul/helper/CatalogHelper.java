package com.webkul.mobikul.helper;

import android.support.annotation.NonNull;

import com.webkul.mobikul.model.catalog.CategoryList;
import com.webkul.mobikul.model.catalog.LayeredData;
import com.webkul.mobikul.model.catalog.LayeredDataOption;
import com.webkul.mobikul.model.catalog.SubCategory;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by vedesh.kumar on 30/1/17. @Webkul Software Pvt. Ltd
 */

public class CatalogHelper {

    private static final String TAG = CatalogHelper.class.getSimpleName();

    public static String getCategoryName(@NonNull SubCategory categoriesData, int selectedCategoryId) {
        if (categoriesData.getCategoryId() == selectedCategoryId) {
            return categoriesData.getName();
        } else if (categoriesData.getChildren().size() != 0) {
            for (int noOfChilder = 0; noOfChilder < categoriesData.getChildren().size(); noOfChilder++) {
                String categoryName = getCategoryName(categoriesData.getChildren().get(noOfChilder), selectedCategoryId);
                if (categoryName != null && !categoryName.isEmpty()) {
                    return categoryName;
                } else {
                    getCategoryName(categoriesData.getChildren().get(noOfChilder), selectedCategoryId);
                }
            }
        }
        return "";
    }

    public static String getLayeredDataAttributeLabel(@NonNull ArrayList<LayeredData> mLayeredDatas, Map.Entry pair) {
        for (LayeredData layeredData : mLayeredDatas) {
            if (layeredData.getCode().equals(pair.getKey().toString())) {
                return layeredData.getLabel();
            }
        }
        return "";
    }

    public static String getLayeredDataCategoryAttributeLabel(@NonNull ArrayList<CategoryList> mLayeredDatas, Map.Entry pair) {
        for (CategoryList layeredData : mLayeredDatas) {
            if (layeredData.getId().equals(pair.getValue().toString())) {
                return layeredData.getName();
            }
        }
        return "";
    }

    public static String getLayeredDataOptionLabel(@NonNull ArrayList<LayeredData> mLayeredDatas, Map.Entry pair) {
        for (LayeredData layeredData : mLayeredDatas) {
            if (layeredData.getCode().equals(pair.getKey())) {
                for (LayeredDataOption layeredDataOption : layeredData.getOptions()) {
                    if (layeredDataOption.getId().equals(pair.getValue())) {
                        return layeredDataOption.getLabel();
                    }
                }
            }
        }
        return "";
    }
}
