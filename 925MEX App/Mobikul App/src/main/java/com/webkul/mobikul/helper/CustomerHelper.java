package com.webkul.mobikul.helper;

import android.content.Context;
import android.util.Log;

import com.webkul.mobikul.constants.ApplicationConstant;


/**
 * Created by vedesh.kumar on 22/2/17. @Webkul Software Pvt. Ltd
 */

public class CustomerHelper {
    @SuppressWarnings("unused")
    private static final String TAG = "CustomerHelper";


    public static String getFirstName(Context context) {
        return getFirstName(AppSharedPref.getCustomerName(context));
    }

    public static String getFirstName(String fullName) {
        String firstName = "";
        try {
            if (fullName.split("\\w+").length > 1) {
                firstName = fullName.substring(0, fullName.lastIndexOf(' '));
            } else {
                firstName = fullName;
            }
        } catch (Exception e) {
            Log.e(ApplicationConstant.TAG, " getFirstName: " + e.getMessage());
        }
        return firstName;
    }

    public static String getLastName(Context context) {
        return getLastName(AppSharedPref.getCustomerName(context));
    }

    public static String getLastName(String fullName) {
        String lastName = "";
        try {
            if (fullName.split("\\w+").length > 1) {
                lastName = fullName.substring(fullName.lastIndexOf(" ") + 1);
            }
        } catch (Exception e) {
            Log.e(ApplicationConstant.TAG, " getLastName: " + e.getMessage());
        }
        return lastName;
    }


}
