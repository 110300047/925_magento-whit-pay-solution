package com.webkul.mobikul.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.webkul.mobikul.constants.ApplicationConstant;

import java.util.ArrayList;
import java.util.List;

import static com.webkul.mobikul.connection.ApiInterface.MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST;
import static com.webkul.mobikul.connection.ApiInterface.MOBIKUL_CATALOG_HOME_PAGE_DATA;
import static com.webkul.mobikul.connection.ApiInterface.MOBIKUL_CATALOG_PRODUCT_PAGE_DATA;

/**
 * Created by vedesh.kumar on 6/3/17. @Webkul Software Pvt. Ltd
 */
public class DataBaseHelper extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "notification_database";

    // Table Name
    private static final String TABLE_OFFLINE_DATA = "OFFLINE";
    private static final String TABLE_NOTIFICATION_DATA = "NOTIFICATION";
    private static final String TABLE_CART_DATA = "CART_OFFLINE";

    // Column Names
    private static final String TABLE_OFFLINE_DATA_COLUMN_ID = "id";
    private static final String TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME = "methodname";
    private static final String TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA = "responsedata";
    private static final String TABLE_OFFLINE_DATA_COLUMN_PRODUCT_ID = "productid";
    private static final String TABLE_OFFLINE_DATA_COLUMN_CATEGORY_ID = "categoryid";
    private static final String TABLE_OFFLINE_DATA_COLUMN_PROFILE_URL_OR_PAGE_IDENTIFIER = "profileurlorpageidentifier";
    private static final String TABLE_OFFLINE_DATA_COLUMN_ORDER_ID = "orderid";

    private static final String TABLE_NOTIFICATION_DATA_KEY_NOTIFICATION_ID = "id";

    private static final String TABLE_CART_DATA_COLUMN_ID = "id";
    private static final String TABLE_CART_DATA_COLUMN_STORE_ID = "storeId";
    private static final String TABLE_CART_DATA_COLUMN_PRODUCT_ID = "productId";
    private static final String TABLE_CART_DATA_COLUMN_QTY = "qty";
    private static final String TABLE_CART_DATA_COLUMN_PARAMS = "params";
    private static final String TABLE_CART_DATA_COLUMN_RELATED_PRODUCTS = "relatedProducts";
    private static final String TABLE_CART_DATA_COLUMN_QUOTE_ID = "quoteId";
    private static final String TABLE_CART_DATA_COLUMN_CUSTOMER_ID = "customerId";
    private static final String TABLE_CART_DATA_COLUMN_PRODUCT_NAME = "productName";


    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFLINE_DATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION_DATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART_DATA);
        // Create tables again
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_OFFLINE_DATA_TABLE = "CREATE TABLE " + TABLE_OFFLINE_DATA +
                "(" + TABLE_OFFLINE_DATA_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME + " VARCHAR, " +
                TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " VARCHAR, " +
                TABLE_OFFLINE_DATA_COLUMN_PRODUCT_ID + " VARCHAR, " +
                TABLE_OFFLINE_DATA_COLUMN_CATEGORY_ID + " VARCHAR, " +
                TABLE_OFFLINE_DATA_COLUMN_PROFILE_URL_OR_PAGE_IDENTIFIER + " VARCHAR , " +
                TABLE_OFFLINE_DATA_COLUMN_ORDER_ID + " VARCHAR)";
        db.execSQL(CREATE_OFFLINE_DATA_TABLE);

        String CREATE_NOTIFICATION_DATA_TABLE = "CREATE TABLE " + TABLE_NOTIFICATION_DATA + "(" + TABLE_NOTIFICATION_DATA_KEY_NOTIFICATION_ID + " INTEGER PRIMARY KEY" + ")";
        db.execSQL(CREATE_NOTIFICATION_DATA_TABLE);

        String CREATE_CART_TABLE = "CREATE TABLE " + TABLE_CART_DATA + "(" +
                TABLE_CART_DATA_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                TABLE_CART_DATA_COLUMN_STORE_ID + " INTEGER, " +
                TABLE_CART_DATA_COLUMN_PRODUCT_ID + " INTEGER, " +
                TABLE_CART_DATA_COLUMN_QTY + " INTEGER, " +
                TABLE_CART_DATA_COLUMN_PARAMS + " VARCHAR, " +
                TABLE_CART_DATA_COLUMN_RELATED_PRODUCTS + " VARCHAR, " +
                TABLE_CART_DATA_COLUMN_QUOTE_ID + " INTEGER, " +
                TABLE_CART_DATA_COLUMN_CUSTOMER_ID + " INTEGER, " +
                TABLE_CART_DATA_COLUMN_PRODUCT_NAME + " VARCHAR ) ";
        db.execSQL(CREATE_CART_TABLE);
    }

    //    Methods for Offline mode

    public void updateIntoOfflineDB(String methodName, String responseData, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        try {
            switch (methodName) {
                case MOBIKUL_CATALOG_HOME_PAGE_DATA:
                case "mobikulCheckoutGetcartDetails":
                case "mobikulExtrasGetnotificationList":
                case "mobikulCustomerGetwishlistData":
                case "mobikulmpMarketplaceGetlandingpageData":
                case "mobikulmpMarketplaceGetsellerdashboardData":
                case "mobikulmpMarketplaceGetmysoldordersData":
                case "mobikulCustomerGetdashboardData":
                case "mobikulCustomerGetallOrders":
                case "mobikulCustomerGetproReviews":
                case "mobikulCustomerGetaccountinfoData":
                case "mobikulCustomerGetaddrbookData":
                case "mobikulCustomerGetmyDownloads":
                    cursor = db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE " + TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME + " = '" + methodName + "'", null);
                    if (cursor.getCount() != 0) {
                        cursor.moveToFirst();
                        if (cursor.getString(0).equals(responseData)) {
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA IS SAME");
                        } else {
                            ContentValues values = new ContentValues();
                            values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);

                            String selection = TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME + " LIKE ?";
                            String[] selectionArgs = {methodName};

                            int count = db.update(TABLE_OFFLINE_DATA, values, selection, selectionArgs);
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA UPDATED : " + count);
                        }
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME, methodName);
                        values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);
                        long newRowId = db.insert(TABLE_OFFLINE_DATA, null, values);
                        Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA INSERTED AT ROW NUMBER : " + newRowId);
                    }
                    cursor.close();
                    break;

                case MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST:
                    Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: " + responseData);
                    cursor = db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE categoryid = '" + id + "'", null);
                    if (cursor.getCount() != 0) {
                        cursor.moveToFirst();
                        if (cursor.getString(0).equals(responseData)) {
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA IS SAME");
                        } else {
                            ContentValues values = new ContentValues();
                            values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);

                            String selection = "categoryid" + " LIKE ?";
                            String[] selectionArgs = {id};

                            int count = db.update(TABLE_OFFLINE_DATA, values, selection, selectionArgs);
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA UPDATED : " + count);
                        }
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME, methodName);
                        values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);
                        values.put("categoryid", id);
                        long newRowId = db.insert(TABLE_OFFLINE_DATA, null, values);
                        Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA INSERTED AT ROW NUMBER : " + newRowId);
                    }
                    cursor.close();
                    break;

                case MOBIKUL_CATALOG_PRODUCT_PAGE_DATA:
                    Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: " + responseData);
                    cursor = db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE " + TABLE_OFFLINE_DATA_COLUMN_PRODUCT_ID + " = '" + id + "'", null);
                    if (cursor.getCount() != 0) {
                        cursor.moveToFirst();
                        if (cursor.getString(0).equals(responseData)) {
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA IS SAME");
                        } else {
                            ContentValues values = new ContentValues();
                            values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);

                            String selection = TABLE_OFFLINE_DATA_COLUMN_PRODUCT_ID + " LIKE ?";
                            String[] selectionArgs = {id};

                            int count = db.update(TABLE_OFFLINE_DATA, values, selection, selectionArgs);
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA UPDATED : " + count);
                        }
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME, methodName);
                        values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);
                        values.put(TABLE_OFFLINE_DATA_COLUMN_PRODUCT_ID, id);
                        long newRowId = db.insert(TABLE_OFFLINE_DATA, null, values);
                        Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA INSERTED AT ROW NUMBER : " + newRowId);
                    }
                    cursor.close();
                    break;

                case "mobikulCustomerGetorderDetail":
                    Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: " + responseData);
                    cursor = db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE orderid = '" + id + "'", null);
                    if (cursor.getCount() != 0) {
                        cursor.moveToFirst();
                        if (cursor.getString(0).equals(responseData)) {
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA IS SAME");
                        } else {
                            ContentValues values = new ContentValues();
                            values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);

                            String selection = "orderid" + " LIKE ?";
                            String[] selectionArgs = {id};

                            int count = db.update(TABLE_OFFLINE_DATA, values, selection, selectionArgs);
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA UPDATED : " + count);
                        }
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME, methodName);
                        values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);
                        values.put("orderid", id);
                        long newRowId = db.insert(TABLE_OFFLINE_DATA, null, values);
                        Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA INSERTED AT ROW NUMBER : " + newRowId);
                    }
                    cursor.close();
                    break;

                case "mobikulmpMarketplaceGetsellerProfileData":
                case "mobikulGetCMSPagesData":
                    Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: " + responseData);
                    cursor = db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE profileurlorpageidentifier = '" + id + "'", null);
                    if (cursor.getCount() != 0) {
                        cursor.moveToFirst();
                        if (cursor.getString(0).equals(responseData)) {
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA IS SAME");
                        } else {
                            ContentValues values = new ContentValues();
                            values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);

                            String selection = "profileurlorpageidentifier" + " LIKE ?";
                            String[] selectionArgs = {id};

                            int count = db.update(TABLE_OFFLINE_DATA, values, selection, selectionArgs);
                            Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA UPDATED : " + count);
                        }
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME, methodName);
                        values.put(TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA, responseData);
                        values.put("profileurlorpageidentifier", id);
                        long newRowId = db.insert(TABLE_OFFLINE_DATA, null, values);
                        Log.d(ApplicationConstant.TAG, "updateIntoOfflineDB: DATA INSERTED AT ROW NUMBER : " + newRowId);
                    }
                    cursor.close();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: 11/12/16 checks should also contain custome id check for all the customer related queries...
    public Cursor selectFromOfflineDB(String methodName, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        switch (methodName) {
            case MOBIKUL_CATALOG_HOME_PAGE_DATA:

            case "mobikulExtrasGetnotificationList":
            case "mobikulCheckoutGetcartDetails":

            case "mobikulCustomerGetwishlistData":

            case "mobikulCustomerGetdashboardData":
            case "mobikulCustomerGetallOrders":
            case "mobikulCustomerGetproReviews":
            case "mobikulCustomerGetaccountinfoData":
            case "mobikulCustomerGetaddrbookData":
            case "mobikulCustomerGetmyDownloads":

            case "mobikulmpMarketplaceGetlandingpageData":
            case "mobikulmpMarketplaceGetsellerdashboardData":
            case "mobikulmpMarketplaceGetmysoldordersData":
                return db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE " + TABLE_OFFLINE_DATA_COLUMN_METHOD_NAME + " = '" + methodName + "'", null);

            case MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST:
                return db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE categoryid = '" + id + "'", null);

            case MOBIKUL_CATALOG_PRODUCT_PAGE_DATA:
                return db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE " + TABLE_OFFLINE_DATA_COLUMN_PRODUCT_ID + " = '" + id + "'", null);

            case "mobikulCustomerGetorderDetail":
                return db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE orderid = '" + id + "'", null);

            case "mobikulmpMarketplaceGetsellerProfileData":
            case "mobikulGetCMSPagesData":
                return db.rawQuery("SELECT " + TABLE_OFFLINE_DATA_COLUMN_RESPONSE_DATA + " FROM " + TABLE_OFFLINE_DATA + " WHERE profileurlorpageidentifier = '" + id + "'", null);

            default:
                return null;
        }
    }

    //    Methods for Notification

    public void addNotificationID(String notificatioId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TABLE_NOTIFICATION_DATA_KEY_NOTIFICATION_ID, notificatioId);

        db.insert(TABLE_NOTIFICATION_DATA, null, values);
        db.close();
    }

    public List<String> getAllNotification() {
        List<String> notificationList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION_DATA;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                notificationList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return notificationList;
    }

    public void deleteNotification(String notificationID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTIFICATION_DATA, TABLE_NOTIFICATION_DATA_KEY_NOTIFICATION_ID + " = ?", new String[]{notificationID});
        db.close();
    }

    /* Offline Cart Functiosn */

    public void addToCartOffline(int storeId, int productId, int qty, String params, String relatedProducts, int quoteId, int customerId, String productName) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        try {
            cursor = db.rawQuery("SELECT " + TABLE_CART_DATA_COLUMN_QTY + ", " + TABLE_CART_DATA_COLUMN_ID + " FROM " + TABLE_CART_DATA + " WHERE "
                            + TABLE_CART_DATA_COLUMN_STORE_ID + " = '" + storeId + "' AND "
                            + TABLE_CART_DATA_COLUMN_PRODUCT_ID + " = '" + productId + "' AND "
                            + TABLE_CART_DATA_COLUMN_PARAMS + " = '" + params + "' AND "
                            + TABLE_CART_DATA_COLUMN_RELATED_PRODUCTS + " = '" + relatedProducts + "' AND "
                            + TABLE_CART_DATA_COLUMN_QUOTE_ID + " = '" + quoteId + "' AND "
                            + TABLE_CART_DATA_COLUMN_CUSTOMER_ID + " = '" + customerId + "'"
                    , null);

            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                int previousQtyInDb = cursor.getInt(0);
                int id = cursor.getInt(1);

                ContentValues values = new ContentValues();
                values.put(TABLE_CART_DATA_COLUMN_QTY, previousQtyInDb + qty);

                String selection = TABLE_CART_DATA_COLUMN_ID + " LIKE ?";
                String[] selectionArgs = {String.valueOf(id)};

                int count = db.update(TABLE_CART_DATA, values, selection, selectionArgs);
                Log.d(ApplicationConstant.TAG, "addToCartOffline DATA UPDATED AT :" + count);
            } else {
                ContentValues values = new ContentValues();
                values.put(TABLE_CART_DATA_COLUMN_STORE_ID, storeId);
                values.put(TABLE_CART_DATA_COLUMN_PRODUCT_ID, productId);
                values.put(TABLE_CART_DATA_COLUMN_QTY, qty);
                values.put(TABLE_CART_DATA_COLUMN_PARAMS, params);
                values.put(TABLE_CART_DATA_COLUMN_RELATED_PRODUCTS, relatedProducts);
                values.put(TABLE_CART_DATA_COLUMN_QUOTE_ID, quoteId);
                values.put(TABLE_CART_DATA_COLUMN_CUSTOMER_ID, customerId);
                values.put(TABLE_CART_DATA_COLUMN_PRODUCT_NAME, productName);

                long newRowId = db.insert(TABLE_CART_DATA, null, values);
                Log.d(ApplicationConstant.TAG, "addToCartOffline DATA INSERTED AT : " + newRowId);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getCartTableRowCount() {
        SQLiteDatabase cartDb = this.getReadableDatabase();
        return DatabaseUtils.queryNumEntries(cartDb, TABLE_CART_DATA);
    }

    public Cursor getCartTableData() {
        SQLiteDatabase cartDb = this.getReadableDatabase();
        return cartDb.rawQuery("SELECT " + TABLE_CART_DATA_COLUMN_STORE_ID + ", "
                + TABLE_CART_DATA_COLUMN_PRODUCT_ID + ", "
                + TABLE_CART_DATA_COLUMN_QTY + ", "
                + TABLE_CART_DATA_COLUMN_PARAMS + ", "
                + TABLE_CART_DATA_COLUMN_RELATED_PRODUCTS + ", "
                + TABLE_CART_DATA_COLUMN_QUOTE_ID + ", "
                + TABLE_CART_DATA_COLUMN_CUSTOMER_ID + ", "
                + TABLE_CART_DATA_COLUMN_PRODUCT_NAME
                + " FROM " + TABLE_CART_DATA, null);
    }

    public void deleteCartEntry(int storeId, int productId, String params, String relatedProducts, int quoteId, int customerId) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        try {
            cursor = db.rawQuery("SELECT " + TABLE_CART_DATA_COLUMN_ID + " FROM " + TABLE_CART_DATA + " WHERE "
                            + TABLE_CART_DATA_COLUMN_STORE_ID + " = '" + storeId + "' AND "
                            + TABLE_CART_DATA_COLUMN_PRODUCT_ID + " = '" + productId + "' AND "
                            + TABLE_CART_DATA_COLUMN_PARAMS + " = '" + params + "' AND "
                            + TABLE_CART_DATA_COLUMN_RELATED_PRODUCTS + " = '" + relatedProducts + "' AND "
                            + TABLE_CART_DATA_COLUMN_QUOTE_ID + " = '" + quoteId + "' AND "
                            + TABLE_CART_DATA_COLUMN_CUSTOMER_ID + " = '" + customerId + "'"
                    , null);
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();
                int id = cursor.getInt(0);

                if (id > 0) {
                    int count = db.delete(TABLE_CART_DATA, TABLE_CART_DATA_COLUMN_ID + " LIKE ?", new String[]{String.valueOf(id)});
                    Log.d(ApplicationConstant.TAG, " deleteCartEntry DATA DELETED AT : " + count);
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearCartTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_CART_DATA);
    }
}