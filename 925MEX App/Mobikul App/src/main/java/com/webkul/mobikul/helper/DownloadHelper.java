package com.webkul.mobikul.helper;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.constants.ApplicationConstant;

import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 12/6/17.
 */

public class DownloadHelper {

    public static void downloadFile(Context context, String url, String fileName, String mimeType) {
        if (context == null || url == null || fileName == null || mimeType == null) {
            showToast(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT, 0);
            return;
        }
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(context.getResources().getString(R.string.download_started));
        request.setTitle(fileName);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        request.addRequestHeader("authKey", ApplicationSingleton.getInstance().getAuthKey());
        request.addRequestHeader("apiKey", ApplicationConstant.API_USER_NAME);
        request.addRequestHeader("apiPassword", ApplicationConstant.API_PASSWORD);
        request.setMimeType(mimeType);
        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }
}