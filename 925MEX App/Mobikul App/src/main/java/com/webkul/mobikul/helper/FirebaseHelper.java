package com.webkul.mobikul.helper;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by vedesh.kumar on 20/2/17. @Webkul Software Pvt. Ltd
 */

public class FirebaseHelper {

    public static void log(Exception e, String TAG) {
        FirebaseCrash.log(TAG + "\n"
                + "Message: " + e.getMessage() + "\n"
                + "Cause: " + e.getCause() + "\n"
        );
    }
}
