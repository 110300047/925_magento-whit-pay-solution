package com.webkul.mobikul.helper;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewParent;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.webkul.mobikul.R;

import java.io.File;

/**
 * Created by vedesh.kumar on 9/2/17. @Webkul Software Pvt. Ltd
 */

public class ImageHelper {

    public static void load(@NonNull ImageView view, String imageUrl, Drawable placeHolder) {
        if (imageUrl == null && placeHolder == null) {
            return;
        }

        if (imageUrl != null && imageUrl.isEmpty()) {
            imageUrl = null;
        }

        final ViewParent parentView = view.getParent();
        if (parentView instanceof ShimmerFrameLayout) {
            ((ShimmerFrameLayout) parentView).startShimmerAnimation();
        }

        /*For Picasso Library*/
//        Callback picassoCallBack = new Callback() {
//            @Override
//            public void onSuccess() {
//                if (parentView instanceof ShimmerFrameLayout) {
//                    ((ShimmerFrameLayout) parentView).stopShimmerAnimation();
//                }
//            }
//
//            @Override
//            public void onError() {
//                if (parentView instanceof ShimmerFrameLayout) {
//                    ((ShimmerFrameLayout) parentView).stopShimmerAnimation();
//                }
//            }
//        };
//
//        if (placeHolder == null) {
//            Picasso.with(view.getContext())
//                    .load(imageUrl)
//                    .placeholder(R.drawable.placeholder)
//                    .into(view, picassoCallBack); /*LOAD WITHOUT PLACE HOLDER*/
//        } else {
//            Picasso.with(view.getContext())
//                    .load(imageUrl)
//                    .placeholder(placeHolder)
//                    .into(view, picassoCallBack);  /*LOAD WITH PLACE HOLDER*/
//        }

        /*For Glide Library*/
        RequestListener<Drawable> glideCallBack = new RequestListener<Drawable>() {

            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                if (parentView instanceof ShimmerFrameLayout) {
                    ((ShimmerFrameLayout) parentView).stopShimmerAnimation();
                }
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if (parentView instanceof ShimmerFrameLayout) {
                    ((ShimmerFrameLayout) parentView).stopShimmerAnimation();
                }
                return false;
            }
        };

        if (placeHolder == null) {
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .listener(glideCallBack)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.placeholder)
                            .dontAnimate())
                    .into(view);    /*LOAD WITHOUT PLACE HOLDER*/
        } else {
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .listener(glideCallBack)
                    .apply(new RequestOptions()
                            .placeholder(placeHolder)
                            .dontAnimate())
                    .into(view);    /*LOAD WITH PLACE HOLDER*/
        }
    }

    public static void loadFromFile(@NonNull ImageView view, File image) {
        final ViewParent parentView = view.getParent();
        if (parentView instanceof ShimmerFrameLayout) {
            ((ShimmerFrameLayout) parentView).startShimmerAnimation();
        }

        /*For Glide Library*/
        RequestListener<Drawable> glideCallBack = new RequestListener<Drawable>() {

            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                if (parentView instanceof ShimmerFrameLayout) {
                    ((ShimmerFrameLayout) parentView).stopShimmerAnimation();
                }
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if (parentView instanceof ShimmerFrameLayout) {
                    ((ShimmerFrameLayout) parentView).stopShimmerAnimation();
                }
                return false;
            }
        };

        Glide.with(view.getContext())
                .load(image)
                .listener(glideCallBack)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.placeholder)
                        .dontAnimate())
                .into(view);
    }
}

/*License
BSD License

For Shimmer software

Copyright (c) 2015, Facebook, Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/