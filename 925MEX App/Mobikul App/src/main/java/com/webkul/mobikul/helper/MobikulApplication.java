package com.webkul.mobikul.helper;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;

import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.fragment.SignInFragment;
import com.webkul.mobikul.fragment.SignUpFragment;
import com.webkul.mobikul.handler.SignInHandler;
import com.webkul.mobikul.handler.SignUpHandler;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormData;

import java.net.CookieHandler;
import java.net.CookieManager;

import shortbread.Shortbread;

/**
 * Created by vedesh.kumar on 5/1/17. @Webkul Software Pvt. Ltd
 */

public class MobikulApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Shortbread.create(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        CookieHandler.setDefault(new CookieManager());
    }

    @Override
    protected void attachBaseContext(Context base) {
        MultiDex.install(base);
        super.attachBaseContext(base);
    }

    public Class getHomePageClass() {
        return HomeActivity.class;
    }

    public Class getMarketplaceLandingPageClass() {
        return null;
    }

    public Class getCatalogProductPageClass() {
        return CatalogProductActivity.class;
    }

    public SignUpHandler getSignUpHandler(Context context, int isSocial, SignUpFragment signUpFragment, CreateAccountFormData createAccountFormData) {
        return new SignUpHandler(context, isSocial, signUpFragment, createAccountFormData);
    }

    public SignInHandler getSignInHandlerClass(Context context, SignInFragment signInFragment) {
        return new SignInHandler(context, signInFragment);
    }

    public void loadAskQuestionToAdminDialogFragment(Context context) {
        //do nothing here
    }

    public void callSellerDashBoardActivity(Context context) {
        // do nothing here
    }

    public void callSellerProfileActivity(Context context) {
        // do nothing here
    }

    public void callSellerOrdersActivity(Context context) {
        // do nothing here
    }

    public void callSellerNewProductActivity(Context context) {
        // do nothing here
    }

    public void callSellerProductsListActivity(Context context) {
        // do nothing here
    }

    public void callSellerTransactionsListActivity(Context context) {
        // do nothing here
    }

    public void callManagePrintPdfHeaderActivity(Context context) {
        // do nothing here
    }

    public Class getContactUsFragmentClass() {
        return null;
    }

    public Fragment getContactUsFragmentNewInstance(int sellerId, int productId) {
        return null;
    }

    public Class getSellerProfilePageClass() {
        return null;
    }

    public void callSellerStatusActivity(Context mContext) {

    }

    public void callChatRelatedActivity(Context mContext) {

    }

    public Class getSellerOrderDetailsActivity() {
        return null;
    }

    public Class getSellerDashBoardActivity() {
        return null;
    }
}
