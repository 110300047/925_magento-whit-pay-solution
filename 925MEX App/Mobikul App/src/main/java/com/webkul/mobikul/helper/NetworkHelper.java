package com.webkul.mobikul.helper;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentTransaction;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.fragment.EmptyFragment;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Response;

/**
 * Created by vedesh.kumar on 2/2/17. @Webkul Software Pvt. Ltd
 */

public class NetworkHelper {

    /*NETWORK RESPONSE CODE*/
    public static final int NW_RESPONSE_CODE_INVALID_REQUEST_TYPE = 0;
    public static final int NW_RESPONSE_CODE_SUCCESS = 1;
    public static final int NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED = 2;
    public static final int NW_RESPONSE_CODE_AUTH_FAILURE = 3;

    @SuppressWarnings("unused")
    private static final String TAG = "NetworkHelper";

    public static void onFailure(Throwable t, Activity activity) {
        if (activity == null) {
            return;
        }
        /*dismiss dialog*/
        AlertDialogHelper.dismiss(activity);
        t.printStackTrace();

        /*display message*/
        if (t instanceof SocketTimeoutException) {
            SnackbarHelper.getSnackbar(activity, activity.getString(R.string.error_request_timeout)).show();
        } else if (t instanceof IOException) {
            /*NO NETWORK */
            if (isNetworkAvailable(activity)) {
                SnackbarHelper.getSnackbar(activity, activity.getString(R.string.error_request_timeout)).show();
            } else {
//                SnackbarHelper.getSnackbar(activity, activity.getString(R.string.error_no_network)).show();
                final FragmentTransaction ft = ((BaseActivity) activity).getSupportFragmentManager().beginTransaction();
                ft.replace(android.R.id.content, EmptyFragment.newInstance(R.drawable.ic_no_internet
                        , activity.getString(R.string.no_internet)
                        , ""
                        , true)
                        , EmptyFragment.class.getSimpleName()).commit();
            }
        } else {
            SnackbarHelper.getSnackbar(activity, activity.getString(R.string.error_request_failed)).show();
        }
    }

    public static boolean isNetworkAvailable(final Context context) {
        if (context != null) {
            final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
            return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
        } else
            return true;
    }

    /*checks whether the response is valid or not and display the message*/
    public static boolean isValidResponse(Activity activity, Response responseObj, boolean showError) {
        if (activity == null || activity.isFinishing())
            return false;

        if (responseObj == null || responseObj.body() == null) {
            AlertDialogHelper.dismiss(activity);
            if (showError) {
                SnackbarHelper.getSnackbar(activity, activity.getString(R.string.error_request_timeout)).show();
            }
            return false;
        }
        return true;
    }
}
