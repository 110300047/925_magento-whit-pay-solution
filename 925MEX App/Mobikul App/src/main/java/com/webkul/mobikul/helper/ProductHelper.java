package com.webkul.mobikul.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.webkul.mobikul.firebase.FirebaseAnalyticsImpl;
import com.webkul.mobikul.model.catalog.ProductData;
import com.webkul.mobikul.model.catalog.ProductShortData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 27/4/16. @Webkul Software Pvt. Ltd
 */
public class ProductHelper {

    public static void shareProduct(Context context, String url, int productId, String productName) {
        FirebaseAnalyticsImpl.logShareEvent(context, productId, productName);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            context.startActivity(Intent.createChooser(sendIntent, "Choose an Action:", null));
        } else {
            context.startActivity(sendIntent);
        }
    }

    public static ArrayList<ProductShortData> getProductShortData(ArrayList<ProductData> productDatas) {
        ArrayList<ProductShortData> productShortDatas = new ArrayList<>();
        for (int noOfProduct = 0; noOfProduct < productDatas.size(); noOfProduct++) {
            ProductShortData productShortData = new ProductShortData(productDatas.get(noOfProduct).getEntityId()
                    , productDatas.get(noOfProduct).getName()
                    , productDatas.get(noOfProduct).getThumbNail());
            productShortDatas.add(productShortData);
        }
        return productShortDatas;
    }
}