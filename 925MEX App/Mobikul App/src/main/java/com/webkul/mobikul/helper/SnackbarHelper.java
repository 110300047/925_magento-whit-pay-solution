package com.webkul.mobikul.helper;

import android.app.Activity;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.webkul.mobikul.R;

import org.jetbrains.annotations.NotNull;


/**
 * Created by vedesh.kumar on 21/2/17. @Webkul Software Pvt. Ltd
 */

public class SnackbarHelper {

    public static Snackbar getSnackbar(@NotNull Activity activity, int stringId) {
        return getSnackbar(activity, activity.getString(stringId), 0);
    }

    public static Snackbar getSnackbar(@NotNull Activity activity, String message) {
        return getSnackbar(activity, message, 0);
    }

    public static Snackbar getSnackbar(@NotNull Activity activity, String message, int colorId) {
        if (colorId == 0) {
            colorId = R.color.snackbar_background;
        }
        Snackbar snackbar;
        snackbar = Snackbar.make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ContextCompat.getColor(activity.getApplicationContext(), colorId));

        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
//        tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.error_center_x, 0, 0, 0);
//        tv.setCompoundDrawablePadding(5);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) group.getLayoutParams();
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        group.setLayoutParams(params);
        snackbar.setAction("Undo", null);
        snackbar.setActionTextColor(Color.RED);
        return snackbar;
    }
}