package com.webkul.mobikul.helper;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.model.product.ProductAddToCartResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vedesh.kumar on 28/10/17.
 */

public class SyncCartDbWithServer extends AsyncTask<Void, Void, Void> {

    private Context mContext;
    private DataBaseHelper mDataBaseHelper;

    public SyncCartDbWithServer(Context context) {
        this.mContext = context;
        mDataBaseHelper = new DataBaseHelper(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (NetworkHelper.isNetworkAvailable(mContext)) {
            try {
                Cursor cursor = mDataBaseHelper.getCartTableData();
                cursor.moveToFirst();
                do {
                    final int storeId = cursor.getInt(cursor.getColumnIndex("storeId"));
                    final int productId = cursor.getInt(cursor.getColumnIndex("productId"));
                    int qty = cursor.getInt(cursor.getColumnIndex("qty"));
                    final JSONObject paramsObject = new JSONObject(cursor.getString(cursor.getColumnIndex("params")));
                    final JSONArray relatedProductsArray = new JSONArray(cursor.getString(cursor.getColumnIndex("relatedProducts")));
                    final int quoteId = cursor.getInt(cursor.getColumnIndex("quoteId"));
                    final int customerId = cursor.getInt(cursor.getColumnIndex("customerId"));

                    ApiConnection.addToCart(storeId
                            , productId
                            , qty
                            , paramsObject
                            , relatedProductsArray
                            , quoteId
                            , customerId)

                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<ProductAddToCartResponse>(mContext) {
                        @Override
                        public void onNext(ProductAddToCartResponse productAddToCartResponse) {
                            super.onNext(productAddToCartResponse);
                            mDataBaseHelper.deleteCartEntry(storeId, productId, paramsObject.toString(), relatedProductsArray.toString(), quoteId, customerId);
                            if (productAddToCartResponse.isSuccess()) {
                                if (quoteId == 0 && customerId == 0 && AppSharedPref.getQuoteId(mContext) == 0 && AppSharedPref.getCustomerId(mContext) == 0) {
                                    AppSharedPref.setQuoteId(mContext, productAddToCartResponse.getQuoteId());
                                }

                                if (productAddToCartResponse.getCartCount() > 0) {
                                    ((BaseActivity) mContext).updateCartBadge(productAddToCartResponse.getCartCount());
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                        }
                    });
                } while (cursor.moveToNext());
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void callApi() {

    }
}
