package com.webkul.mobikul.helper;

import android.content.Context;
import android.graphics.Color;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;

/**
 * Created by vedesh.kumar on 3/9/17. @Webkul Software Private limited
 */

public class ToastHelper {

    public static void showToast(Context context, String message, int duration, int icon) {
        if (((BaseActivity) context).mStylableToast != null) {
            ((BaseActivity) context).mStylableToast.cancel();
        }
        StyleableToast.Builder toastBuilder = new StyleableToast.Builder(context);
        toastBuilder.text(message);
        toastBuilder.textColor(Color.WHITE);
        toastBuilder.backgroundColor(context.getResources().getColor(R.color.colorAccent));
        toastBuilder.duration(duration);
        if (icon != 0 && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT_WATCH) {
            toastBuilder.icon(icon);
            toastBuilder.spinIcon();
        }
        ((BaseActivity) context).mStylableToast = toastBuilder.build();
        ((BaseActivity) context).mStylableToast.show();
    }

    public static void dismiss(Context context) {
        if (((BaseActivity) context).mStylableToast != null) {
            ((BaseActivity) context).mStylableToast.cancel();
        }
    }
}