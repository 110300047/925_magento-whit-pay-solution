package com.webkul.mobikul.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.speech.RecognizerIntent;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Spinner;
import android.widget.TextView;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.BaseActivity;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.webkul.mobikul.fragment.DatePickerFragment.DEFAULT_DATE_FORMAT;


/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */
public class Utils {

    public static int getScreenWidth() {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return metrics.widthPixels;
    }

    public static void showKeyboard(View view) {
        view.requestFocus();
        if (!isHardKeyboardAvailable(view)) {
            InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, 0);
        }
    }

    private static boolean isHardKeyboardAvailable(View view) {
        return view.getContext().getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
    }

    public static void hideKeyboard(Context context) {
        if (context instanceof Activity) {
            hideKeyboard((Activity) context);
        }
    }

    public static boolean isVoiceAvailable(Context context) {
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        return activities.size() > 0;
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus().getWindowToken() != null)
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showShakeError(Context context, View viewToAnimate) {
        try {
            viewToAnimate.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake_error));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String generateRandomPassword() {
        Random random = new SecureRandom();
        String letters = "abcdefghjklmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ1234567890";
        String numbers = "1234567890";
        String specialChars = "!@#$%^&*_=+-/";
        String pw = "";
        for (int i = 0; i < 8; i++) {
            int index = (int) (random.nextDouble() * letters.length());
            pw += letters.substring(index, index + 1);
        }
        int indexA = (int) (random.nextDouble() * numbers.length());
        pw += numbers.substring(indexA, indexA + 1);
        int indexB = (int) (random.nextDouble() * specialChars.length());
        pw += specialChars.substring(indexB, indexB + 1);
        return pw;
    }

    public static void setAnimation(View viewToAnimate, int position, int animationId, Context context) {
        int lastPosition = -1;
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, animationId);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public static void setSpinnerError(Spinner spinner, String errorString) {
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setTextColor(Color.RED);
            selectedTextView.setText(errorString);
            spinner.performClick();
        }
    }

    public static void setNotificationBadge(Context context) {
        DataBaseHelper db = new DataBaseHelper(context);
        List<String> notificationIdList = db.getAllNotification();
        db.close();
        ShortcutBadger.applyCount(context, notificationIdList.size());
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, int cartCount) {
        BadgeDrawable badge;
        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_menu_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }
        badge.setCount(String.valueOf(cartCount));
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_menu_badge, badge);
    }

    public static String formatDate(String dateFormat, int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year, month, day);
        Date date = cal.getTime();
        if (dateFormat == null) {
            dateFormat = DEFAULT_DATE_FORMAT;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
        return sdf.format(date);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static void disableUserIntraction(Context context) {
        ((BaseActivity) context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public static void enableUserIntraction(Context context) {
        ((BaseActivity) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}