package com.webkul.mobikul.helper;

import android.os.AsyncTask;

import com.webkul.mobikul.constants.ApplicationConstant;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;

public class VersionChecker extends AsyncTask<String, String, String> {

    private String newVersion = "1.0";

    @Override
    protected String doInBackground(String... params) {

        try {
            Element element = Jsoup.connect(ApplicationConstant.APP_PLAYSTORE_URL)
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div[itemprop=softwareVersion]")
                    .first();
            if (element != null)
                newVersion = element.ownText();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newVersion;
    }
}