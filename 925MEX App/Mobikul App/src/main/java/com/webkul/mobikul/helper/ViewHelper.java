package com.webkul.mobikul.helper;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;

/**
 * Created by vedesh.kumar on 23/3/17.
 */

public class ViewHelper {

    public static void showTitleOnlyWhenCollapsed(AppBarLayout appBarLayout, final CollapsingToolbarLayout collapsingToolbarLayout, final String title) {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(title);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");// carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }
}
