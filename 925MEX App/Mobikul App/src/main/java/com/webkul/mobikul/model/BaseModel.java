package com.webkul.mobikul.model;

import android.databinding.BaseObservable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_SUCCESS;

/**
 * Created by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class BaseModel extends BaseObservable {

    @SerializedName("authKey")
    @Expose
    public String authKey;
    @SerializedName("responseCode")
    @Expose
    public int responseCode;
    @SerializedName("success")
    @Expose
    public boolean success;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("cartCount")
    @Expose
    public int cartCount;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }


    public boolean isRequestSuccessful() {
        return (getResponseCode() == NW_RESPONSE_CODE_SUCCESS || getResponseCode() == NW_RESPONSE_CODE_NEW_AUTH_KEY_GENERATED);
    }

    public int getCartCount() {
        return cartCount;
    }
}
