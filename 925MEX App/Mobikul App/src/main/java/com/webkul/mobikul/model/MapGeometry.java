package com.webkul.mobikul.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 13/6/17.
 */

public class MapGeometry {

    @SerializedName("location")
    @Expose
    private MapLocation location;

    public MapLocation getLocation() {
        return location;
    }

    public void setLocation(MapLocation location) {
        this.location = location;
    }
}
