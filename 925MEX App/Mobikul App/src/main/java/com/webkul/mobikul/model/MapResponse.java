package com.webkul.mobikul.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 13/6/17.
 */

public class MapResponse extends BaseModel {

    @SerializedName("results")
    @Expose
    private List<MapResult> results = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<MapResult> getResults() {
        return results;
    }

    public void setResults(List<MapResult> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
