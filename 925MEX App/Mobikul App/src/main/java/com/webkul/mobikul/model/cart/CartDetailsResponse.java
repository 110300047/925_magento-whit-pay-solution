package com.webkul.mobikul.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.ProductData;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 27/12/16. @Webkul Software Pvt. Ltd
 */

public class CartDetailsResponse extends BaseModel {
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("couponCode")
    @Expose
    private String couponCode;
    @SerializedName("showThreshold")
    @Expose
    private Boolean showThreshold;
    @SerializedName("isVirtual")
    @Expose
    private Boolean isVirtual;
    @SerializedName("isAllowedGuestCheckout")
    @Expose
    private Boolean isAllowedGuestCheckout;
    @SerializedName("subtotal")
    @Expose
    private Subtotal subtotal;
    @SerializedName("discount")
    @Expose
    private Discount discount;
    @SerializedName("shipping")
    @Expose
    private Shipping shipping;
    @SerializedName("tax")
    @Expose
    private Tax tax;
    @SerializedName("grandtotal")
    @Expose
    private GrandTotal mGrandTotal;
    @SerializedName("crossSellList")
    @Expose
    private ArrayList<ProductData> crossSellList = null;
    @SerializedName("canGuestCheckoutDownloadable")
    @Expose
    private Boolean canGuestCheckoutDownloadable;
    @SerializedName("allowMultipleShipping")
    @Expose
    private Boolean allowMultipleShipping;
    @SerializedName("minimumAmount")
    @Expose
    private Float minimumAmount;
    @SerializedName("minimumFormattedAmount")
    @Expose
    private String minimumFormattedAmount;

    private boolean canProcceedToCheckout = true;
    private boolean containsDownloadableProducts = false;
    private String error = "";

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Boolean getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(Boolean isVirtual) {
        this.isVirtual = isVirtual;
    }

    public Subtotal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Subtotal subtotal) {
        this.subtotal = subtotal;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public GrandTotal getGrandTotal() {
        return mGrandTotal;
    }

    public void setGrandTotal(GrandTotal grandTotal) {
        this.mGrandTotal = grandTotal;
    }

    public JSONArray getItemIds() {
        JSONArray itemIds = new JSONArray();
        for (Item eachItem : items) {
            itemIds.put(eachItem.getId());
        }
        return itemIds;
    }

    public JSONArray getQtys() {
        JSONArray qtyJsonArr = new JSONArray();
        for (Item eachItem : items) {
            qtyJsonArr.put(Integer.parseInt(eachItem.getQty()));
        }
        return qtyJsonArr;
    }

    public Boolean getAllowedGuestCheckout() {
        return isAllowedGuestCheckout;
    }

    public void setAllowedGuestCheckout(Boolean allowedGuestCheckout) {
        isAllowedGuestCheckout = allowedGuestCheckout;
    }

    public ArrayList<ProductData> getCrossSellList() {
        return crossSellList;
    }

    public void setCrossSellList(ArrayList<ProductData> crossSellList) {
        this.crossSellList = crossSellList;
    }

    public boolean isCanProcceedToCheckout() {
        return canProcceedToCheckout;
    }

    public void setCanProcceedToCheckout(boolean canProcceedToCheckout) {
        this.canProcceedToCheckout = canProcceedToCheckout;
    }

    public Boolean getShowThreshold() {
        return showThreshold;
    }

    public void setShowThreshold(Boolean showThreshold) {
        this.showThreshold = showThreshold;
    }

    public Boolean getCanGuestCheckoutDownloadable() {
        return canGuestCheckoutDownloadable;
    }

    public void setCanGuestCheckoutDownloadable(Boolean canGuestCheckoutDownloadable) {
        this.canGuestCheckoutDownloadable = canGuestCheckoutDownloadable;
    }

    public boolean isContainsDownloadableProducts() {
        return containsDownloadableProducts;
    }

    public void setContainsDownloadableProducts(boolean containsDownloadableProducts) {
        this.containsDownloadableProducts = containsDownloadableProducts;
    }

    public Boolean getAllowMultipleShipping() {
        return allowMultipleShipping;
    }

    public void setAllowMultipleShipping(Boolean allowMultipleShipping) {
        this.allowMultipleShipping = allowMultipleShipping;
    }

    public Float getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Float minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public String getMinimumFormattedAmount() {
        return minimumFormattedAmount;
    }

    public void setMinimumFormattedAmount(String minimumFormattedAmount) {
        this.minimumFormattedAmount = minimumFormattedAmount;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}