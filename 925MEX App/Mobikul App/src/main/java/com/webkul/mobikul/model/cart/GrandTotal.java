package com.webkul.mobikul.model.cart;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GrandTotal implements Parcelable {

    public static final Creator<GrandTotal> CREATOR = new Creator<GrandTotal>() {
        @Override
        public GrandTotal createFromParcel(Parcel in) {
            return new GrandTotal(in);
        }

        @Override
        public GrandTotal[] newArray(int size) {
            return new GrandTotal[size];
        }
    };
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("unformatedValue")
    @Expose
    private float unformatedValue;

    protected GrandTotal(Parcel in) {
        title = in.readString();
        value = in.readString();
        unformatedValue = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(value);
        dest.writeFloat(unformatedValue);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public float getUnformatedValue() {
        return unformatedValue;
    }

    public void setUnformatedValue(float unformatedValue) {
        this.unformatedValue = unformatedValue;
    }

}
