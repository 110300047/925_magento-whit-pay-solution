package com.webkul.mobikul.model.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_MAX_QTY;

public class Item {

    @SerializedName("thresholdQty")
    @Expose
    private int thresholdQty;
    @SerializedName("remainingQty")
    @Expose
    private double remainingQty;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("productId")
    @Expose
    private int productId;
    @SerializedName("typeId")
    @Expose
    private String typeId;
    @SerializedName("subTotal")
    @Expose
    private String subTotal;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("options")
    @Expose
    private List<CartOptionItem> optionItems = null;
    @SerializedName("messages")
    @Expose
    private Message messages = null;
    private boolean cartItemQtyChanged;

    public List<CartOptionItem> getOptionItems() {
        return optionItems;
    }

    public void setOptionItems(List<CartOptionItem> optionItems) {
        this.optionItems = optionItems;
    }

    public int getThresholdQty() {
        return thresholdQty;
    }

    public void setThresholdQty(int thresholdQty) {
        this.thresholdQty = thresholdQty;
    }

    public int getRemainingQty() {
        return (int) remainingQty;
    }

    public void setRemainingQty(double remainingQty) {
        this.remainingQty = remainingQty;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQtyByArrowBtn(String qty) {
        cartItemQtyChanged = !this.qty.equals(qty);
        if (qty.isEmpty()) {
            qty = "0";
        } else {
            if (Integer.parseInt(qty) < 0) {
                this.qty = "0";
                return;
            }
            if (Integer.parseInt(qty) > DEFAULT_MAX_QTY) {
                this.qty = String.valueOf(DEFAULT_MAX_QTY);
                return;
            }
        }
        this.qty = qty;
    }

    public void setQty(String qty) {
        if (qty.isEmpty()) {
            qty = "0";
        } else {
            if (Integer.parseInt(qty) < 0) {
                this.qty = "0";
                return;
            }
            if (Integer.parseInt(qty) > DEFAULT_MAX_QTY) {
                this.qty = String.valueOf(DEFAULT_MAX_QTY);
                return;
            }
        }
        this.qty = qty;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isCartItemQtyChanged() {
        return cartItemQtyChanged;
    }

    public boolean isShowThreshold() {
        return !(typeId.equals("configurable") || typeId.equals("bundle") || typeId.equals("grouped") || remainingQty > thresholdQty);
    }

    public Message getMessages() {
        return messages;
    }

    public void setMessages(Message messages) {
        this.messages = messages;
    }
}