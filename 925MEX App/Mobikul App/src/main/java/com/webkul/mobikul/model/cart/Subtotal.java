package com.webkul.mobikul.model.cart;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subtotal implements Parcelable {

    public static final Creator<Subtotal> CREATOR = new Creator<Subtotal>() {
        @Override
        public Subtotal createFromParcel(Parcel in) {
            return new Subtotal(in);
        }

        @Override
        public Subtotal[] newArray(int size) {
            return new Subtotal[size];
        }
    };
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("unformatedValue")
    @Expose
    private float unformatedValue;

    protected Subtotal(Parcel in) {
        title = in.readString();
        value = in.readString();
        unformatedValue = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(value);
        dest.writeFloat(unformatedValue);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public float getUnformatedValue() {
        return unformatedValue;
    }

    public void setUnformatedValue(float unformatedValue) {
        this.unformatedValue = unformatedValue;
    }

}
