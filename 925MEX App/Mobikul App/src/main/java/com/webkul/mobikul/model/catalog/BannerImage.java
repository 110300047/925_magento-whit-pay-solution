package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BannerImage implements Parcelable {

    public static final Creator<BannerImage> CREATOR = new Creator<BannerImage>() {
        @Override
        public BannerImage createFromParcel(Parcel in) {
            return new BannerImage(in);
        }

        @Override
        public BannerImage[] newArray(int size) {
            return new BannerImage[size];
        }
    };
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("bannerType")
    @Expose
    private String bannerType;
    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("productId")
    @Expose
    private int productId;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("categoryId")
    @Expose
    private int categoryId;

    protected BannerImage(Parcel in) {
        url = in.readString();
        bannerType = in.readString();
        error = in.readByte() != 0;
        productName = in.readString();
        productId = in.readInt();
        categoryName = in.readString();
        categoryId = in.readInt();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBannerType() {
        return bannerType;
    }

    public void setBannerType(String bannerType) {
        this.bannerType = bannerType;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(bannerType);
        dest.writeByte((byte) (error ? 1 : 0));
        dest.writeString(productName);
        dest.writeInt(productId);
        dest.writeString(categoryName);
        dest.writeInt(categoryId);
    }
}
