package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 28/2/17. @Webkul Software Private limited
 */

public class CMSPageData implements Parcelable {

    public static final Creator<CMSPageData> CREATOR = new Creator<CMSPageData>() {
        @Override
        public CMSPageData createFromParcel(Parcel in) {
            return new CMSPageData(in);
        }

        @Override
        public CMSPageData[] newArray(int size) {
            return new CMSPageData[size];
        }
    };
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;

    protected CMSPageData(Parcel in) {
        id = in.readInt();
        title = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
    }
}