package com.webkul.mobikul.model.catalog;

import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.BR;
import com.webkul.mobikul.model.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 24/12/16. @Webkul Software Pvt. Ltd
 */

public class CatalogProductData extends BaseModel {
    @SerializedName(value = "productCollection", alternate = {"productList"})
    @Expose
    private ArrayList<ProductData> mProductDatas = null;
    @SerializedName("sortingData")
    @Expose
    private List<SortingData> sortingData = null;
    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("layeredData")
    @Expose
    private List<LayeredData> layeredData = null;
    @SerializedName("bannerImage")
    @Expose
    private String bannerImage;
    @SerializedName(value = "criteriaData", alternate = {"critariaData"})
    @Expose
    private List<String> criteriaData = null;
    @SerializedName("showSwatchOnCollection")
    @Expose
    private boolean showSwatchOnCollection;
    @SerializedName("categoryList")
    @Expose
    private ArrayList<CategoryList> categoryList = null;

    private boolean lazyLoading;
    private boolean showBanner;
    private int requestType = 0;

    public ArrayList<ProductData> getCatalogProductCollection() {
        return mProductDatas;
    }

    public List<String> getCriteriaData() {
        return criteriaData;
    }

    public void setCriteriaData(List<String> criteriaData) {
        this.criteriaData = criteriaData;
    }


    public List<SortingData> getSortingData() {
        return sortingData;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public List<LayeredData> getLayeredData() {
        return layeredData;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    @Bindable
    public boolean isLazyLoading() {
        return lazyLoading;
    }

    public void setLazyLoading(boolean lazyLoading) {
        this.lazyLoading = lazyLoading;
        notifyPropertyChanged(BR.lazyLoading);
    }

    @Bindable
    public boolean isShowBanner() {
        return showBanner;
    }

    public void setShowBanner(boolean showBanner) {
        this.showBanner = showBanner;
        notifyPropertyChanged(BR.showBanner);
    }

    public boolean isShowSwatchOnCollection() {
        return showSwatchOnCollection;
    }

    public void setShowSwatchOnCollection(boolean showSwatchOnCollection) {
        this.showSwatchOnCollection = showSwatchOnCollection;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<CategoryList> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<CategoryList> categoryList) {
        this.categoryList = categoryList;
    }
}
