package com.webkul.mobikul.model.catalog;

import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.BR;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.product.AdditionalInformationData;
import com.webkul.mobikul.model.product.BundleOption;
import com.webkul.mobikul.model.product.ConfigurableData;
import com.webkul.mobikul.model.product.GroupedData;
import com.webkul.mobikul.model.product.ImageGalleryData;
import com.webkul.mobikul.model.product.Links;
import com.webkul.mobikul.model.product.PriceFormatData;
import com.webkul.mobikul.model.product.ProductCustomOption;
import com.webkul.mobikul.model.product.ProductRatingData;
import com.webkul.mobikul.model.product.ProductReviewData;
import com.webkul.mobikul.model.product.RatingFormData;
import com.webkul.mobikul.model.product.Samples;

import java.util.ArrayList;
import java.util.List;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_MAX_QTY;

/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */

@SuppressWarnings("unused")
public class CatalogSingleProductData extends BaseModel {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("productUrl")
    @Expose
    private String productUrl;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("formatedMinPrice")
    @Expose
    private String formatedMinPrice;
    @SerializedName("minPrice")
    @Expose
    private Double minPrice;
    @SerializedName("formatedMaxPrice")
    @Expose
    private String formatedMaxPrice;
    @SerializedName("maxPrice")
    @Expose
    private Double maxPrice;
    @SerializedName("formatedPrice")
    @Expose
    private String formatedPrice;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("formatedFinalPrice")
    @Expose
    private String formatedFinalPrice;
    @SerializedName("finalPrice")
    @Expose
    private Double finalPrice;
    @SerializedName("formatedSpecialPrice")
    @Expose
    private String formatedSpecialPrice;
    @SerializedName("specialPrice")
    @Expose
    private Double specialPrice;
    @SerializedName("groupedPrice")
    @Expose
    private String groupedPrice;
    @SerializedName("typeId")
    @Expose
    private String typeId;
    @SerializedName("msrpEnabled")
    @Expose
    private int msrpEnabled;
    @SerializedName("msrpDisplayActualPriceType")
    @Expose
    private int msrpDisplayActualPriceType;
    @SerializedName("msrp")
    @Expose
    private Double msrp;
    @SerializedName("formatedMsrp")
    @Expose
    private String formatedMsrp;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("isInRange")
    @Expose
    private boolean isInRange;
    @SerializedName("guestCanReview")
    @Expose
    private boolean guestCanReview;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("isAvailable")
    @Expose
    private boolean isAvailable;
    @SerializedName("priceFormat")
    @Expose
    private PriceFormatData priceFormat;
    @SerializedName("imageGallery")
    @Expose
    private ArrayList<ImageGalleryData> imageGallery = new ArrayList<>();
    @SerializedName("additionalInformation")
    @Expose
    private ArrayList<AdditionalInformationData> additionalInformation = new ArrayList<>();
    @SerializedName("ratingData")
    @Expose
    private ArrayList<ProductRatingData> ratingData = new ArrayList<>();
    @SerializedName("reviewList")
    @Expose
    private ArrayList<ProductReviewData> reviewList = null;
    @SerializedName("priceView")
    @Expose
    private String priceView;
    @SerializedName("tierPrices")
    @Expose
    private List<String> tierPrices = null;
    @SerializedName("relatedProductList")
    @Expose
    private ArrayList<ProductData> relatedProductListData = new ArrayList<>();
    @SerializedName("groupedData")
    @Expose
    private List<GroupedData> groupedData = null;
    @SerializedName("links")
    @Expose
    private Links links;
    @SerializedName("samples")
    @Expose
    private Samples samples;
    @SerializedName("bundleOptions")
    @Expose
    private List<BundleOption> bundleOptions = null;
    @SerializedName("configurableData")
    @Expose
    private ConfigurableData configurableData;
    @SerializedName("customOptions")
    @Expose
    private List<ProductCustomOption> customOptions = null;
    @SerializedName("ratingFormData")
    @Expose
    private ArrayList<RatingFormData> ratingFormData = null;
    @SerializedName("isInWishlist")
    @Expose
    private boolean isInWishlist;
    @SerializedName("wishlistItemId")
    @Expose
    private int wishlistItemId;
    @SerializedName("upsellProductList")
    @Expose
    private ArrayList<ProductData> upsellProductList = new ArrayList<>();
    @SerializedName("showPriceDropAlert")
    @Expose
    private boolean showPriceDropAlert;
    @SerializedName("showBackInStockAlert")
    @Expose
    private boolean showBackInStockAlert;
    @SerializedName("isAllowedGuestCheckout")
    @Expose
    private boolean isAllowedGuestCheckout;
    @SerializedName("displaySellerInfo")
    @Expose
    private boolean displaySellerInfo;
    @SerializedName("shoptitle")
    @Expose
    private String shoptitle;
    @SerializedName("sellerId")
    @Expose
    private int sellerId;
    @SerializedName("sellerRating")
    @Expose
    private List<ProductRatingData> sellerRating = new ArrayList<>();
    @SerializedName("reviewDescription")
    @Expose
    private String reviewDescription;
    @SerializedName("sellerAverageRating")
    @Expose
    private String sellerAverageRating;

    private String qty = "1";
    private boolean loading = true;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormatedMinPrice() {
        return formatedMinPrice;
    }

    public void setFormatedMinPrice(String formatedMinPrice) {
        this.formatedMinPrice = formatedMinPrice;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public String getFormatedMaxPrice() {
        return formatedMaxPrice;
    }

    public void setFormatedMaxPrice(String formatedMaxPrice) {
        this.formatedMaxPrice = formatedMaxPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getFormatedPrice() {
        return formatedPrice;
    }

    public void setFormatedPrice(String formatedPrice) {
        this.formatedPrice = formatedPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getFormatedFinalPrice() {
        return formatedFinalPrice;
    }

    public void setFormatedFinalPrice(String formatedFinalPrice) {
        this.formatedFinalPrice = formatedFinalPrice;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getFormatedSpecialPrice() {
        return formatedSpecialPrice;
    }

    public void setFormatedSpecialPrice(String formatedSpecialPrice) {
        this.formatedSpecialPrice = formatedSpecialPrice;
    }

    public Double getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(Double specialPrice) {
        this.specialPrice = specialPrice;
    }

    public boolean hasGroupedPrice() {
        return typeId.equals("grouped");
    }

    public String getGroupedPrice() {
        return groupedPrice;
    }

    public void setGroupedPrice(String groupedPrice) {
        this.groupedPrice = groupedPrice;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public int getMsrpEnabled() {
        return msrpEnabled;
    }

    public void setMsrpEnabled(int msrpEnabled) {
        this.msrpEnabled = msrpEnabled;
    }

    public int getMsrpDisplayActualPriceType() {
        return msrpDisplayActualPriceType;
    }

    public void setMsrpDisplayActualPriceType(int msrpDisplayActualPriceType) {
        this.msrpDisplayActualPriceType = msrpDisplayActualPriceType;
    }

    public Double getMsrp() {
        return msrp;
    }

    public void setMsrp(Double msrp) {
        this.msrp = msrp;
    }

    public String getFormatedMsrp() {
        return formatedMsrp;
    }

    public void setFormatedMsrp(String formatedMsrp) {
        this.formatedMsrp = formatedMsrp;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsInRange() {
        return isInRange;
    }

    public void setIsInRange(boolean isInRange) {
        this.isInRange = isInRange;
    }

    public boolean getGuestCanReview() {
        return guestCanReview;
    }

    public void setGuestCanReview(boolean guestCanReview) {
        this.guestCanReview = guestCanReview;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public PriceFormatData getPriceFormat() {
        return priceFormat;
    }

    public void setPriceFormat(PriceFormatData priceFormat) {
        this.priceFormat = priceFormat;
    }

    public ArrayList<ImageGalleryData> getImageGallery() {
        return imageGallery;
    }

    public void setImageGallery(ArrayList<ImageGalleryData> imageGallery) {
        this.imageGallery = imageGallery;
    }

    public ArrayList<AdditionalInformationData> getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(ArrayList<AdditionalInformationData> additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public ArrayList<ProductRatingData> getRatingData() {
        return ratingData;
    }

    public void setRatingData(ArrayList<ProductRatingData> ratingData) {
        this.ratingData = ratingData;
    }

    public ArrayList<ProductReviewData> getReviewList() {
        return reviewList;
    }

    public void setReviewList(ArrayList<ProductReviewData> reviewList) {
        this.reviewList = reviewList;
    }


    public String getPriceView() {
        return priceView;
    }

    public void setPriceView(String priceView) {
        this.priceView = priceView;
    }


    public List<String> getTierPrices() {
        return tierPrices;
    }

    public void setTierPrices(List<String> tierPrices) {
        this.tierPrices = tierPrices;
    }

    public ArrayList<ProductData> getRelatedProductListData() {
        return relatedProductListData;
    }

    public void setRelatedProductListData(ArrayList<ProductData> relatedProductListData) {
        this.relatedProductListData = relatedProductListData;
    }

    public ArrayList<ProductData> getUpsellProductListData() {
        return upsellProductList;
    }

    public void setUpsellProductListData(ArrayList<ProductData> upsellProductList) {
        this.upsellProductList = upsellProductList;
    }

    public float getRating() {
        double sumOfAllRatingData = 0d;
        for (int noOfRating = 0; noOfRating < ratingData.size(); noOfRating++) {
            sumOfAllRatingData += ratingData.get(noOfRating).getRatingValue();
        }
        return (float) (sumOfAllRatingData / ratingData.size());
    }

    @Bindable
    public String getQty() {
        if (qty == null) {
            setQty("1"); // DEFAULT QTY
        }
        return qty;
    }

    public void setQty(String qty) {
        if (qty.isEmpty()) {
            this.qty = "1";
            return;
        }
        int qtyInt = Integer.parseInt(qty);
        if (qtyInt < 1) {
            this.qty = "1";
        } else if (qtyInt > DEFAULT_MAX_QTY) {
            this.qty = String.valueOf(DEFAULT_MAX_QTY);
        } else {
            this.qty = qty;
        }
        notifyPropertyChanged(BR.qty);
    }

    public String getShoptitle() {
        return shoptitle;
    }

    public void setShoptitle(String shoptitle) {
        this.shoptitle = shoptitle;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public List<GroupedData> getGroupedData() {
        return groupedData;
    }

    public void setGroupedData(List<GroupedData> groupedData) {
        this.groupedData = groupedData;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public Samples getSamples() {
        return samples;
    }

    public void setSamples(Samples samples) {
        this.samples = samples;
    }

    public List<BundleOption> getBundleOptions() {
        return bundleOptions;
    }

    public void setBundleOptions(List<BundleOption> bundleOptions) {
        this.bundleOptions = bundleOptions;
    }

    public ConfigurableData getConfigurableData() {
        return configurableData;
    }

    public void setConfigurableData(ConfigurableData configurableData) {
        this.configurableData = configurableData;
    }

    public List<ProductCustomOption> getCustomOptions() {
        return customOptions;
    }

    public void setCustomOptions(List<ProductCustomOption> customOptions) {
        this.customOptions = customOptions;
    }

    public ArrayList<RatingFormData> getRatingFormData() {
        return ratingFormData;
    }

    public void setRatingFormData(ArrayList<RatingFormData> ratingFormData) {
        this.ratingFormData = ratingFormData;
    }

    public boolean hasAnyOptions() {
        if (getConfigurableData().getAttributes() == null && getBundleOptions().size() == 0 && getLinks().getLinkData() == null && getSamples().getLinkSampleData() == null && getGroupedData().size() == 0)
            return false;
        else
            return true;
    }

    public boolean hasSpecialPrice() {
        return specialPrice != null && specialPrice < price && isInRange;
    }

    public boolean hasPrice() {
        return !(finalPrice == null || finalPrice == 0.00);
    }

    public boolean hasMinPrice() {
        return minPrice != null;
    }

    @Bindable
    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
        notifyPropertyChanged(BR.loading);
    }

    public boolean getIsInWishlist() {
        return isInWishlist;
    }

    public void setIsInWishlist(boolean isInWishlist) {
        this.isInWishlist = isInWishlist;
    }

    public int getWishlistItemId() {
        return wishlistItemId;
    }

    public void setWishlistItemId(int wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    public boolean isShowPriceDropAlert() {
        return showPriceDropAlert;
    }

    public void setIsShowPriceDropAlert(boolean showPriceDropAlert) {
        this.showPriceDropAlert = showPriceDropAlert;
    }

    public boolean isShowBackInStockAlert() {
        return showPriceDropAlert;
    }

    public void setIsShowBackInStockAlert(boolean showBackInStockAlert) {
        this.showBackInStockAlert = showBackInStockAlert;
    }

    public boolean isAllowedGuestCheckout() {
        return isAllowedGuestCheckout;
    }

    public boolean isDisplaySellerInfo() {
        return displaySellerInfo;
    }

    public List<ProductRatingData> getSellerRating() {
        return sellerRating;
    }

    public String getReviewDescription() {
        return reviewDescription;
    }

    public String getSellerAverageRating() {
        return sellerAverageRating;
    }
}