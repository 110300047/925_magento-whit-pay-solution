package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoriesData implements Parcelable, Parent<SubCategory> {

    public static final Creator<CategoriesData> CREATOR = new Creator<CategoriesData>() {
        @Override
        public CategoriesData createFromParcel(Parcel in) {
            return new CategoriesData(in);
        }

        @Override
        public CategoriesData[] newArray(int size) {
            return new CategoriesData[size];
        }
    };
    @SerializedName("category_id")
    @Expose
    private int categoryId;
    @SerializedName("parent_id")
    @Expose
    private int parentId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("is_active")
    @Expose
    private int isActive;
    @SerializedName("position")
    @Expose
    private int position;
    @SerializedName("level")
    @Expose
    private int level;
    @SerializedName("children")
    @Expose
    private List<SubCategory> children = null;

    public CategoriesData() {

    }

    protected CategoriesData(Parcel in) {
        categoryId = in.readInt();
        parentId = in.readInt();
        name = in.readString();
        isActive = in.readInt();
        position = in.readInt();
        level = in.readInt();
        children = in.createTypedArrayList(SubCategory.CREATOR);
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<SubCategory> getChildren() {
        return children;
    }

    public void setChildren(List<SubCategory> children) {
        this.children = children;
    }

    @Override
    public List<SubCategory> getChildList() {
        return children;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoryId);
        dest.writeInt(parentId);
        dest.writeString(name);
        dest.writeInt(isActive);
        dest.writeInt(position);
        dest.writeInt(level);
        dest.writeTypedList(children);
    }
}
