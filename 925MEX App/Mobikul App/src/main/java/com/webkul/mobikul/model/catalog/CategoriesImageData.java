package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoriesImageData implements Parcelable {

    public static final Creator<CategoriesImageData> CREATOR = new Creator<CategoriesImageData>() {
        @Override
        public CategoriesImageData createFromParcel(Parcel in) {
            return new CategoriesImageData(in);
        }

        @Override
        public CategoriesImageData[] newArray(int size) {
            return new CategoriesImageData[size];
        }
    };
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("banner")
    @Expose
    private String banner;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    protected CategoriesImageData(Parcel in) {
        id = in.readInt();
        banner = in.readString();
        thumbnail = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(banner);
        dest.writeString(thumbnail);
    }
}
