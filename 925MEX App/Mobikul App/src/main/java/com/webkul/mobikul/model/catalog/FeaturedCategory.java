package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeaturedCategory implements Parcelable {

    public static final Creator<FeaturedCategory> CREATOR = new Creator<FeaturedCategory>() {
        @Override
        public FeaturedCategory createFromParcel(Parcel in) {
            return new FeaturedCategory(in);
        }

        @Override
        public FeaturedCategory[] newArray(int size) {
            return new FeaturedCategory[size];
        }
    };
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("categoryId")
    @Expose
    private int categoryId;

    protected FeaturedCategory(Parcel in) {
        url = in.readString();
        categoryName = in.readString();
        categoryId = in.readInt();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(categoryName);
        dest.writeInt(categoryId);
    }
}
