package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.product.PriceFormatData;

import java.util.ArrayList;
import java.util.List;

public class HomePageResponseData extends BaseModel implements Parcelable {

    public static final Creator<HomePageResponseData> CREATOR = new Creator<HomePageResponseData>() {
        @Override
        public HomePageResponseData createFromParcel(Parcel in) {
            return new HomePageResponseData(in);
        }

        @Override
        public HomePageResponseData[] newArray(int size) {
            return new HomePageResponseData[size];
        }
    };
    @SerializedName("otherError")
    @Expose
    private String otherError;
    @SerializedName("categories")
    @Expose
    private DefaultCategoryData categories;
    @SerializedName("bannerImages")
    @Expose
    private List<BannerImage> bannerImages = new ArrayList<>();
    @SerializedName("featuredCategories")
    @Expose
    private List<FeaturedCategory> featuredCategories = new ArrayList<>();
    @SerializedName("featuredProducts")
    @Expose
    private ArrayList<ProductData> featuredProductDatas = null;
    @SerializedName("newProducts")
    @Expose
    private ArrayList<ProductData> newProducts = null;
    @SerializedName("hotDeals")
    @Expose
    private ArrayList<ProductData> hotDeals = null;
    @SerializedName("categoryImages")
    @Expose
    private List<CategoriesImageData> categoryImages = null;
    @SerializedName("storeId")
    @Expose
    private int storeId;
    @SerializedName("versionCode")
    @Expose
    private int versionCode;
    @SerializedName("themeCode")
    @Expose
    private String themeCode;
    @SerializedName("cmsData")
    @Expose
    private List<CMSPageData> cmsData = null;
    @SerializedName("storeData")
    @Expose
    private List<StoreData> storeData = null;
    @SerializedName("allowedCurrencies")
    @Expose
    private List<String> allowedCurrencies = null;
    @SerializedName("productId")
    @Expose
    private int productId;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("categoryId")
    @Expose
    private int categoryId;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("minQueryLength")
    @Expose
    private int minQueryLength;
    @SerializedName("isMobileLoginEnable")
    @Expose
    private boolean isMobileLoginEnable;
    @SerializedName("defaultCurrency")
    @Expose
    private String defaultCurrency;
    @SerializedName("showSwatchOnCollection")
    @Expose
    private boolean showSwatchOnCollection;
    @SerializedName("priceFormat")
    @Expose
    private PriceFormatData priceFormat;

    protected HomePageResponseData(Parcel in) {
        authKey = in.readString();
        responseCode = in.readInt();
        success = in.readByte() != 0;
        message = in.readString();
        cartCount = in.readInt();
        otherError = in.readString();
        categories = in.readParcelable(DefaultCategoryData.class.getClassLoader());
        bannerImages = in.createTypedArrayList(BannerImage.CREATOR);
        featuredCategories = in.createTypedArrayList(FeaturedCategory.CREATOR);
        featuredProductDatas = in.createTypedArrayList(ProductData.CREATOR);
        newProducts = in.createTypedArrayList(ProductData.CREATOR);
        hotDeals = in.createTypedArrayList(ProductData.CREATOR);
        categoryImages = in.createTypedArrayList(CategoriesImageData.CREATOR);
        storeId = in.readInt();
        versionCode = in.readInt();
        themeCode = in.readString();
        cmsData = in.createTypedArrayList(CMSPageData.CREATOR);
        storeData = in.createTypedArrayList(StoreData.CREATOR);
        allowedCurrencies = in.createStringArrayList();
        productId = in.readInt();
        productName = in.readString();
        categoryId = in.readInt();
        categoryName = in.readString();
        minQueryLength = in.readInt();
        isMobileLoginEnable = in.readByte() != 0;
        defaultCurrency = in.readString();
        showSwatchOnCollection = in.readByte() != 0;
        priceFormat = in.readParcelable(PriceFormatData.class.getClassLoader());
    }

    public DefaultCategoryData getDefaultCategory() {
        return categories;
    }

    public List<BannerImage> getBannerImages() {
        return bannerImages;
    }

    public List<FeaturedCategory> getFeaturedCategories() {
        return featuredCategories;
    }

    public ArrayList<ProductData> getFeaturedProductDatas() {
        return featuredProductDatas;
    }

    public ArrayList<ProductData> getNewProducts() {
        return newProducts;
    }

    public ArrayList<ProductData> getHotDeals() {
        return hotDeals;
    }

    public List<CategoriesImageData> getCategoryImages() {
        return categoryImages;
    }

    public int getStoreId() {
        return storeId;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public String getThemeCode() {
        return themeCode;
    }

    public List<CMSPageData> getCmsData() {
        return cmsData;
    }

    public List<StoreData> getStoreData() {
        return storeData;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(authKey);
        dest.writeInt(responseCode);
        dest.writeByte((byte) (success ? 1 : 0));
        dest.writeString(message);
        dest.writeInt(cartCount);
        dest.writeString(otherError);
        dest.writeParcelable(categories, flags);
        dest.writeTypedList(bannerImages);
        dest.writeTypedList(featuredCategories);
        dest.writeTypedList(featuredProductDatas);
        dest.writeTypedList(newProducts);
        dest.writeTypedList(hotDeals);
        dest.writeTypedList(categoryImages);
        dest.writeInt(storeId);
        dest.writeInt(versionCode);
        dest.writeString(themeCode);
        dest.writeTypedList(cmsData);
        dest.writeTypedList(storeData);
        dest.writeStringList(allowedCurrencies);
        dest.writeInt(productId);
        dest.writeString(productName);
        dest.writeInt(categoryId);
        dest.writeString(categoryName);
        dest.writeInt(minQueryLength);
        dest.writeByte((byte) (isMobileLoginEnable ? 1 : 0));
        dest.writeString(defaultCurrency);
        dest.writeByte((byte) (showSwatchOnCollection ? 1 : 0));
        dest.writeParcelable(priceFormat, flags);
    }

    public boolean canDisplayStores() {
        return ApplicationConstant.CAN_DISPLAY_STORES;
    }

    public boolean canDisplayCurrency() {
        return ApplicationConstant.CAN_DISPLAY_CURRENCY;
    }

    public int getMinQueryLength() {
        return minQueryLength;
    }

    public void setMinQueryLength(int minQueryLength) {
        this.minQueryLength = minQueryLength;
    }

    public boolean isMobileLoginEnable() {
        return isMobileLoginEnable;
    }

    public void setMobileLoginEnable(boolean mobileLoginEnable) {
        isMobileLoginEnable = mobileLoginEnable;
    }

    public List<String> getAllowedCurrencies() {
        return allowedCurrencies;
    }

    public void setAllowedCurrencies(List<String> allowedCurrencies) {
        this.allowedCurrencies = allowedCurrencies;
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public PriceFormatData getPriceFormat() {
        return priceFormat;
    }

    public boolean isShowSwatchOnCollection() {
        return showSwatchOnCollection;
    }

    public void setShowSwatchOnCollection(boolean showSwatchOnCollection) {
        this.showSwatchOnCollection = showSwatchOnCollection;
    }

    public String getOtherError() {
        return otherError;
    }

    public void setOtherError(String otherError) {
        this.otherError = otherError;
    }
}