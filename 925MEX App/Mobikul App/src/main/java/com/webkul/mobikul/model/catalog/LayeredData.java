package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 24/12/16. @Webkul Software Pvt. Ltd
 */

public class LayeredData implements Parcelable {
    public static final Creator<LayeredData> CREATOR = new Creator<LayeredData>() {
        @Override
        public LayeredData createFromParcel(Parcel in) {
            return new LayeredData(in);
        }

        @Override
        public LayeredData[] newArray(int size) {
            return new LayeredData[size];
        }
    };
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("options")
    @Expose
    private List<LayeredDataOption> options = null;

    protected LayeredData(Parcel in) {
        label = in.readString();
        code = in.readString();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<LayeredDataOption> getOptions() {
        return options;
    }

    public void setOptions(List<LayeredDataOption> options) {
        this.options = options;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(code);
    }
}
