package com.webkul.mobikul.model.catalog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

/**
 * Created by vedesh.kumar on 13/10/16. @Webkul Software Pvt. Ltd
 */
public class NavDrawerCustomerItem {

    public static final int TYPE_DEFAULT = 100;
    public static final int TYPE_NOTIFICATION = 101;
    public static final int TYPE_DASHBOARD = 102;
    public static final int TYPE_MY_DOWNLOADABLE_PRODUCTS = 103;
    public static final int TYPE_ACCOUNT_INFO = 104;
    public static final int TYPE_SIGN_IN = 107;
    public static final int TYPE_WISHLIST = 109;
    public static final int TYPE_WALLET = 112;

    public static final int TYPE_CHAT = 203;
    public static final int TYPE_SELLER_DASHBOARD = 204;
    public static final int TYPE_SELLER_PROFILE = 205;
    public static final int TYPE_SELLER_ORDER = 206;
    public static final int TYPE_MANAGE_PRINT_PDF_HEADER = 208;
    public static final int TYPE_ASK_QUESTION_TO_ADMIN = 209;
    public static final int TYPE_SELLER_STATUS = 210;
    public static final int TYPE_SELLER_PRODUCTS_LIST = 211;
    public static final int TYPE_SELLER_TRANSACTIONS_LIST = 212;
    public static final int TYPE_SELLER_NEW_PRODUCT = 214;

    public static final int TYPE_HEADER = 999;

    private final String mTitle;
    private final int mItemType;
    private final int iconId;
    private Context mContext;

    public NavDrawerCustomerItem(Context context, String title, int itemType, int iconId) {
        mContext = context;
        mTitle = title;
        mItemType = itemType;
        this.iconId = iconId;
    }

    public NavDrawerCustomerItem(Context context, String title, int itemType) {
        mContext = context;
        mTitle = title;
        mItemType = itemType;
        iconId = 0;
    }


    public String getTitle() {
        return mTitle;
    }

    public int getItemType() {
        return mItemType;
    }

    public Drawable getIconId() {
        return ContextCompat.getDrawable(mContext, iconId);
    }
}