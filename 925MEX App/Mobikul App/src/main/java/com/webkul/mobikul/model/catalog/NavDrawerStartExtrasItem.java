package com.webkul.mobikul.model.catalog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

/**
 * Created by vedesh.kumar on 13/10/16. @Webkul Software Pvt. Ltd
 */
public class NavDrawerStartExtrasItem {

    public static final int TYPE_COMPARE_PRODUCT = 105;
    public static final int TYPE_CURRENCY = 106;
    public static final int TYPE_CONTACT_US = 110;
    public static final int TYPE_ORDERS_AND_RETURNS = 111;
    public static final int TYPE_MARKETPLACE = 200;

    private final String mTitle;
    private final int mItemType;
    private final int iconId;
    private Context mContext;

    public NavDrawerStartExtrasItem(Context context, String title, int itemType, int iconId) {
        mContext = context;
        mTitle = title;
        mItemType = itemType;
        this.iconId = iconId;
    }

    public NavDrawerStartExtrasItem(Context context, String title, int itemType) {
        mContext = context;
        mTitle = title;
        mItemType = itemType;
        iconId = 0;
    }


    public String getTitle() {
        return mTitle;
    }

    public int getItemType() {
        return mItemType;
    }

    public Drawable getIconId() {
        return ContextCompat.getDrawable(mContext, iconId);
    }
}