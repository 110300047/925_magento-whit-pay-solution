package com.webkul.mobikul.model.catalog;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.BR;
import com.webkul.mobikul.model.product.ConfigurableData;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_MAX_QTY;


public class ProductData extends BaseObservable implements Parcelable {

    @SerializedName("entityId")
    @Expose
    private int entityId;
    @SerializedName("hasTierPrice")
    @Expose
    private boolean hasTierPrice;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("isAvailable")
    @Expose
    private boolean isAvailable;
    @SerializedName("rating")
    @Expose
    private float rating;
    @SerializedName("formatedPrice")
    @Expose
    private String formatedPrice;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("formatedFinalPrice")
    @Expose
    private String formatedFinalPrice;
    @SerializedName("finalPrice")
    @Expose
    private float finalPrice;
    @SerializedName("formatedSpecialPrice")
    @Expose
    private String formatedSpecialPrice;
    @SerializedName("specialPrice")
    @Expose
    private String specialPrice;
    @SerializedName("typeId")
    @Expose
    private String typeId;
    @SerializedName("hasOptions")
    @Expose
    private int hasOptions;
    @SerializedName("requiredOptions")
    @Expose
    private int requiredOptions;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("isInRange")
    @Expose
    private boolean isInRange;
    @SerializedName("thumbNail")
    @Expose
    private String thumbNail;
    @SerializedName("groupedPrice")
    @Expose
    private String groupedPrice;
    @SerializedName("priceView")
    @Expose
    private String priceView;
    @SerializedName("formatedMinPrice")
    @Expose
    private String formatedMinPrice;
    @SerializedName("minPrice")
    @Expose
    private String minPrice;
    @SerializedName("formatedMaxPrice")
    @Expose
    private String formatedMaxPrice;
    @SerializedName("maxPrice")
    @Expose
    private String maxPrice;
    @SerializedName("linksPurchasedSeparately")
    @Expose
    private String linksPurchasedSeparately;
    @SerializedName("isInWishlist")
    @Expose
    private boolean isInWishlist;
    @SerializedName("wishlistItemId")
    @Expose
    private int wishlistItemId;
    @SerializedName("configurableData")
    @Expose
    private ConfigurableData configurableData;

    private int productPosition;
    private String qty = "1";

    private boolean addToCart = false; // For related products only

    protected ProductData(Parcel in) {
        entityId = in.readInt();
        hasTierPrice = in.readByte() != 0;
        shortDescription = in.readString();
        isAvailable = in.readByte() != 0;
        rating = in.readFloat();
        formatedPrice = in.readString();
        price = in.readFloat();
        formatedFinalPrice = in.readString();
        finalPrice = in.readFloat();
        formatedSpecialPrice = in.readString();
        specialPrice = in.readString();
        typeId = in.readString();
        hasOptions = in.readInt();
        requiredOptions = in.readInt();
        name = in.readString();
        isInRange = in.readByte() != 0;
        thumbNail = in.readString();
        groupedPrice = in.readString();
        priceView = in.readString();
        formatedMinPrice = in.readString();
        minPrice = in.readString();
        formatedMaxPrice = in.readString();
        maxPrice = in.readString();
        linksPurchasedSeparately = in.readString();
        isInWishlist = in.readByte() != 0;
        wishlistItemId = in.readInt();
        configurableData = in.readParcelable(ConfigurableData.class.getClassLoader());
        productPosition = in.readInt();
        qty = in.readString();
        addToCart = in.readByte() != 0;
    }

    public static final Creator<ProductData> CREATOR = new Creator<ProductData>() {
        @Override
        public ProductData createFromParcel(Parcel in) {
            return new ProductData(in);
        }

        @Override
        public ProductData[] newArray(int size) {
            return new ProductData[size];
        }
    };

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public boolean getHasTierPrice() {
        return hasTierPrice;
    }

    public void setHasTierPrice(boolean hasTierPrice) {
        this.hasTierPrice = hasTierPrice;
    }

    public String getShortDescription() {
        if (shortDescription != null) {
            shortDescription = shortDescription.replaceAll("<style([\\s\\S]+?)</style>", "");
        }
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getFormatedPrice() {
        return formatedPrice;
    }

    public void setFormatedPrice(String formatedPrice) {
        this.formatedPrice = formatedPrice;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getFormatedFinalPrice() {
        return formatedFinalPrice;
    }

    public void setFormatedFinalPrice(String formatedFinalPrice) {
        this.formatedFinalPrice = formatedFinalPrice;
    }

    public float getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(float finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getFormatedSpecialPrice() {
        return formatedSpecialPrice;
    }

    public void setFormatedSpecialPrice(String formatedSpecialPrice) {
        this.formatedSpecialPrice = formatedSpecialPrice;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public int getHasOptions() {
        if (/*typeId.equalsIgnoreCase("configurable") || */typeId.equalsIgnoreCase("grouped") /*||typeId.equalsIgnoreCase("bundle")*/) {
            return 1;
        } else {
            return hasOptions;
        }
    }

    public void setHasOptions(int hasOptions) {
        this.hasOptions = hasOptions;
    }

    public int getRequiredOptions() {
        if (typeId.equalsIgnoreCase("configurable") || typeId.equalsIgnoreCase("grouped") || typeId.equalsIgnoreCase("bundle")) {
            return 1;
        } else {
            return requiredOptions;
        }
    }

    public void setRequiredOptions(int requiredOptions) {
        this.requiredOptions = requiredOptions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIsInRange() {
        return isInRange;
    }

    public void setIsInRange(boolean isInRange) {
        this.isInRange = isInRange;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    public String getGroupedPrice() {
        return groupedPrice;
    }

    public void setGroupedPrice(String groupedPrice) {
        this.groupedPrice = groupedPrice;
    }

    public String getPriceView() {
        return priceView;
    }

    public void setPriceView(String priceView) {
        this.priceView = priceView;
    }

    public String getFormatedMinPrice() {
        return formatedMinPrice;
    }

    public void setFormatedMinPrice(String formatedMinPrice) {
        this.formatedMinPrice = formatedMinPrice;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getFormatedMaxPrice() {
        return formatedMaxPrice;
    }

    public void setFormatedMaxPrice(String formatedMaxPrice) {
        this.formatedMaxPrice = formatedMaxPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getLinksPurchasedSeparately() {
        return linksPurchasedSeparately;
    }

    public void setLinksPurchasedSeparately(String linksPurchasedSeparately) {
        this.linksPurchasedSeparately = linksPurchasedSeparately;
    }

    public boolean hasSpecialPrice() {
        return specialPrice != null && Double.parseDouble(specialPrice) < price && isInRange;
    }

    public boolean hasPrice() {
        return !(hasMinPrice() || hasGroupedPrice());
    }

    public boolean hasMinPrice() {
        return minPrice != null;
    }

    public boolean hasGroupedPrice() {
        return !(groupedPrice == null);
    }

    public int getProductPosition() {
        return productPosition;
    }

    public void setProductPosition(int productPosition) {
        this.productPosition = productPosition;
    }

    @Bindable
    public boolean isInWishlist() {
        return isInWishlist;
    }

    public void setInWishlist(boolean inWishlist) {
        isInWishlist = inWishlist;
        notifyPropertyChanged(BR.inWishlist);
    }

    public boolean isAddToCart() {
        return addToCart;
    }

    public void setAddToCart(boolean addToCart) {
        this.addToCart = addToCart;
    }

    public int getWishlistItemId() {
        return wishlistItemId;
    }

    public void setWishlistItemId(int wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    public ConfigurableData getConfigurableData() {
        return configurableData;
    }

    public void setConfigurableData(ConfigurableData configurableData) {
        this.configurableData = configurableData;
    }

    @Bindable
    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        if (qty.isEmpty()) {
            this.qty = "1";
            return;
        }
        int qtyInt = Integer.parseInt(qty);
        if (qtyInt < 1) {
            this.qty = "1";
        } else if (qtyInt > DEFAULT_MAX_QTY) {
            this.qty = String.valueOf(DEFAULT_MAX_QTY);
        } else {
            this.qty = qty;
        }
        notifyPropertyChanged(BR.qty);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(entityId);
        dest.writeByte((byte) (hasTierPrice ? 1 : 0));
        dest.writeString(shortDescription);
        dest.writeByte((byte) (isAvailable ? 1 : 0));
        dest.writeFloat(rating);
        dest.writeString(formatedPrice);
        dest.writeFloat(price);
        dest.writeString(formatedFinalPrice);
        dest.writeFloat(finalPrice);
        dest.writeString(formatedSpecialPrice);
        dest.writeString(specialPrice);
        dest.writeString(typeId);
        dest.writeInt(hasOptions);
        dest.writeInt(requiredOptions);
        dest.writeString(name);
        dest.writeByte((byte) (isInRange ? 1 : 0));
        dest.writeString(thumbNail);
        dest.writeString(groupedPrice);
        dest.writeString(priceView);
        dest.writeString(formatedMinPrice);
        dest.writeString(minPrice);
        dest.writeString(formatedMaxPrice);
        dest.writeString(maxPrice);
        dest.writeString(linksPurchasedSeparately);
        dest.writeByte((byte) (isInWishlist ? 1 : 0));
        dest.writeInt(wishlistItemId);
        dest.writeParcelable(configurableData, flags);
        dest.writeInt(productPosition);
        dest.writeString(qty);
        dest.writeByte((byte) (addToCart ? 1 : 0));
    }
}