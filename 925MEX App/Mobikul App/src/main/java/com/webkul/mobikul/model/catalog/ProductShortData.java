package com.webkul.mobikul.model.catalog;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ProductShortData extends BaseObservable implements Serializable, Parcelable {

    public static final Creator<ProductShortData> CREATOR = new Creator<ProductShortData>() {
        @Override
        public ProductShortData createFromParcel(Parcel in) {
            return new ProductShortData(in);
        }

        @Override
        public ProductShortData[] newArray(int size) {
            return new ProductShortData[size];
        }
    };
    @SerializedName("entityId")
    @Expose
    private int entityId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("thumbNail")
    @Expose
    private String thumbNail;

    protected ProductShortData(Parcel in) {
        entityId = in.readInt();
        name = in.readString();
        thumbNail = in.readString();
    }

    public ProductShortData(int entityId, String name, String thumbNail) {
        this.entityId = entityId;
        this.name = name;
        this.thumbNail = thumbNail;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(entityId);
        dest.writeString(name);
        dest.writeString(thumbNail);
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }
}