package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 21/12/16. @Webkul Software Pvt. Ltd
 */

public class SortingData implements Parcelable {
    public static final Creator<SortingData> CREATOR = new Creator<SortingData>() {
        @Override
        public SortingData createFromParcel(Parcel in) {
            return new SortingData(in);
        }

        @Override
        public SortingData[] newArray(int size) {
            return new SortingData[size];
        }
    };
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("label")
    @Expose
    private String label;

    protected SortingData(Parcel in) {
        code = in.readString();
        label = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(label);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
