package com.webkul.mobikul.model.catalog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategory implements Parcelable {

    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };
    @SerializedName("category_id")
    @Expose
    private int categoryId;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("children")
    @Expose
    private List<SubCategory> children = null;

    public SubCategory() {

    }

    protected SubCategory(Parcel in) {
        categoryId = in.readInt();
        parentId = in.readString();
        name = in.readString();
        isActive = in.readString();
        position = in.readString();
        level = in.readString();
        children = in.createTypedArrayList(SubCategory.CREATOR);
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<SubCategory> getChildren() {
        return children;
    }

    public void setChildren(List<SubCategory> children) {
        this.children = children;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoryId);
        dest.writeString(parentId);
        dest.writeString(name);
        dest.writeString(isActive);
        dest.writeString(position);
        dest.writeString(level);
        dest.writeTypedList(children);
    }
}
