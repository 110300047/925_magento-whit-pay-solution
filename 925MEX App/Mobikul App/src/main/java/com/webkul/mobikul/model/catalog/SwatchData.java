package com.webkul.mobikul.model.catalog;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.webkul.mobikul.BR;

/**
 * Created by vedesh.kumar on 25/7/17. @Webkul Software Private limited
 */

public class SwatchData extends BaseObservable {

    private String id;
    private String type;
    private String value;
    private int position;
    private boolean isSelected;
    private boolean isEnabled = true;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Bindable
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
        notifyPropertyChanged(BR.isSelected);
    }

    @Bindable
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        notifyPropertyChanged(BR.enabled);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}