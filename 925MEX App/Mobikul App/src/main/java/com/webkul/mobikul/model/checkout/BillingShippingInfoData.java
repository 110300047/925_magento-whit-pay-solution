package com.webkul.mobikul.model.checkout;

import android.content.Context;
import android.databinding.Bindable;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.BR;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.CheckoutActivity;
import com.webkul.mobikul.adapter.NewAddressStreetAddressRvAdapter;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.databinding.FragmentBillingShippingBinding;
import com.webkul.mobikul.fragment.BillingShippingFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.customer.address.BillingShippingAddress;
import com.webkul.mobikul.model.customer.address.CountryData;
import com.webkul.mobikul.model.customer.address.State;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static com.webkul.mobikul.constants.ApplicationConstant.METHOD_GUEST;
import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;


/**
 * Created with passion and love by vedesh.kumar on 11/1/17. @Webkul Software Pvt. Ltd
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class BillingShippingInfoData extends BaseModel {

    private Context mContext;

    private boolean displayError;
    @SerializedName("isVirtual")
    @Expose
    private boolean isVirtual;
    @SerializedName("address")
    @Expose
    private ArrayList<BillingShippingAddress> addressData = new ArrayList<>();
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("prefixValue")
    @Expose
    private String prefixValue;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("suffixValue")
    @Expose
    private String suffixValue;
    @SerializedName("countryData")
    @Expose
    private List<CountryData> countryData = null;
    @SerializedName("streetLineCount")
    @Expose
    private int streetLineCount;
    @SerializedName("isPrefixVisible")
    @Expose
    private boolean isPrefixVisible;
    @SerializedName("isPrefixRequired")
    @Expose
    private boolean isPrefixRequired;
    @SerializedName("prefixHasOptions")
    @Expose
    private boolean prefixHasOptions;
    @SerializedName("prefixOptions")
    @Expose
    private List<String> prefixOptions = null;
    @SerializedName("isMiddlenameVisible")
    @Expose
    private boolean isMiddlenameVisible;
    @SerializedName("isSuffixVisible")
    @Expose
    private boolean isSuffixVisible;
    @SerializedName("isSuffixRequired")
    @Expose
    private boolean isSuffixRequired;
    @SerializedName("suffixHasOptions")
    @Expose
    private boolean suffixHasOptions;
    @SerializedName("suffixOptions")
    @Expose
    private List<String> suffixOptions = null;
    @SerializedName("isDOBVisible")
    @Expose
    private boolean isDOBVisible;
    @SerializedName("isDOBRequired")
    @Expose
    private boolean isDOBRequired;
    @SerializedName("isTaxVisible")
    @Expose
    private boolean isTaxVisible;
    @SerializedName("isTaxRequired")
    @Expose
    private boolean isTaxRequired;
    @SerializedName("isGenderVisible")
    @Expose
    private boolean isGenderVisible;
    @SerializedName("isGenderRequired")
    @Expose
    private boolean isGenderRequired;
    @SerializedName("dateFormat")
    @Expose
    private String dateFormat;
    /*existing Address based fields*/
    private boolean shipToDifferentAddress;
    private boolean useBillingAddress;
    private int selectedBillingAddressPosition;
    private int selectedShippingAddressPosition;
    private boolean showNewAddressBillingForm;
    private boolean showNewAddressShippingForm;
    /*Billing New Address based fields*/
    private String billingEmail;
    private boolean billingEmailvalidated;
    private String billingPrefixValue = "";
    private int billingPrefixPosition;
    private boolean billingPrefixValidated;
    private String billingFirstName = "";
    private boolean billingFirstNameValidated;
    private String billingMiddleName = "";
    private String billingLastName = "";
    private boolean billingLastNameValidated;
    private String billingSuffixValue = "";
    private int billingSuffixPosition;
    private boolean billingSuffixValidated;
    private List<String> countryNameList;
    private int selectedBillingCountryPosition;
    private boolean hasBillingStateData;
    private int selectedBillingStatePosition;
    private String billingStateValue = "";
    private boolean billingStateValidated;
    private String billingCompany = "";
    private String billingTelephone = "";
    private boolean billingTelephoneValidated;
    private String billingFax = "";
    private String billingCity = "";
    private boolean billingCityValidated;
    private String billingPostCode = "";
    private boolean billingPostCodeValidated;
    private boolean billingStreetAddressValidated;
    private List<String> billingStreetAddressLines;
    private boolean saveBillingAddressInAddressBook;
    private String billingDobValue = "";
    private boolean billingDobValidated;
    private String billingTaxValue = "";
    private boolean billingTaxValidated;
    private int selectedBillingGenderPosition;
    /*Shipping New Address based fields*/
    private String shippingPrefixValue = "";
    private int shippingPrefixPosition;
    private boolean shippingPrefixValidated;
    private String shippingFirstName = "";
    private boolean shippingFirstNameValidated;
    private String shippingMiddleName = "";
    private String shippingLastName = "";
    private boolean shippingLastNameValidated;
    private String shippingSuffixValue = "";
    private boolean shippingSuffixValidated;
    private int shippingSuffixPosition;
    private int selectedShippingCountryPosition;
    private boolean hasShippingStateData;
    private int selectedShippingStatePosition;
    private String shippingStateValue = "";
    private boolean shippingStateValidated;
    private String shippingCompany = "";
    private String shippingEmail = "";
    private boolean shippingEmailValidated;
    private String shippingTelephone = "";
    private boolean shippingTelephoneValidated;
    private String shippingCity = "";
    private boolean shippingCityValidated;
    private String shippingFax = "";
    private String shippingPostCode = "";
    private boolean shippingPostCodeValidated;
    private boolean shippingStreetAddressValidated;
    private List<String> shippingStreetAddressLines;
    private boolean saveShippingAddressInAddressBook;
    private String shippingDobValue = "";
    private boolean shippingDobValidated;
    private int selectedShippingGenderPosition;
    private String shippingTaxValue = "";
    private boolean shippingTaxValidated;

    @Bindable
    public boolean isDisplayError() {
        return displayError;
    }

    public void setDisplayError(boolean displayError) {
        this.displayError = displayError;
        notifyPropertyChanged(BR.displayError);
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        mContext = context;
    }


    public boolean isIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(boolean isVirtual) {
        this.isVirtual = isVirtual;
    }

    public List<BillingShippingAddress> getAddressData() {
        return addressData;
    }

    public void setAddressData(ArrayList<BillingShippingAddress> addressData) {
        this.addressData = addressData;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPrefixValue() {
        return prefixValue;
    }

    public void setPrefixValue(String prefixValue) {
        this.prefixValue = prefixValue;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSuffixValue() {
        return suffixValue;
    }

    public void setSuffixValue(String suffixValue) {
        this.suffixValue = suffixValue;
    }

    public List<CountryData> getCountryData() {
        return countryData;
    }

    public void setCountryData(List<CountryData> countryData) {
        this.countryData = countryData;
    }

    public int getStreetLineCount() {
        return streetLineCount;
    }

    public void setStreetLineCount(int streetLineCount) {
        this.streetLineCount = streetLineCount;
    }

    public boolean isIsPrefixVisible() {
        return isPrefixVisible;
    }

    public void setIsPrefixVisible(boolean isPrefixVisible) {
        this.isPrefixVisible = isPrefixVisible;
    }

    public boolean isIsPrefixRequired() {
        return isPrefixRequired;
    }

    public void setIsPrefixRequired(boolean isPrefixRequired) {
        this.isPrefixRequired = isPrefixRequired;
    }

    public boolean isPrefixHasOptions() {
        return prefixHasOptions;
    }

    public void setPrefixHasOptions(boolean prefixHasOptions) {
        this.prefixHasOptions = prefixHasOptions;
    }

    public List<String> getPrefixOptions() {
        return prefixOptions;
    }

    public void setPrefixOptions(List<String> prefixOptions) {
        this.prefixOptions = prefixOptions;
    }

    public boolean isIsMiddlenameVisible() {
        return isMiddlenameVisible;
    }

    public void setIsMiddlenameVisible(boolean isMiddlenameVisible) {
        this.isMiddlenameVisible = isMiddlenameVisible;
    }

    public boolean isIsSuffixVisible() {
        return isSuffixVisible;
    }

    public void setIsSuffixVisible(boolean isSuffixVisible) {
        this.isSuffixVisible = isSuffixVisible;
    }

    public boolean isIsSuffixRequired() {
        return isSuffixRequired;
    }

    public void setIsSuffixRequired(boolean isSuffixRequired) {
        this.isSuffixRequired = isSuffixRequired;
    }

    public boolean isSuffixHasOptions() {
        return suffixHasOptions;
    }

    public void setSuffixHasOptions(boolean suffixHasOptions) {
        this.suffixHasOptions = suffixHasOptions;
    }

    public List<String> getSuffixOptions() {
        return suffixOptions;
    }

    public void setSuffixOptions(List<String> suffixOptions) {
        this.suffixOptions = suffixOptions;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public boolean isGenderRequired() {
        return isGenderRequired;
    }

    public void setGenderRequired(boolean genderRequired) {
        isGenderRequired = genderRequired;
    }

    public boolean isGenderVisible() {
        return isGenderVisible;
    }

    public void setGenderVisible(boolean genderVisible) {
        isGenderVisible = genderVisible;
    }

    public boolean isTaxRequired() {
        return isTaxRequired;
    }

    public void setTaxRequired(boolean taxRequired) {
        isTaxRequired = taxRequired;
    }

    public boolean isTaxVisible() {
        return isTaxVisible;
    }

    public void setTaxVisible(boolean taxVisible) {
        isTaxVisible = taxVisible;
    }

    public boolean isDOBRequired() {
        return isDOBRequired;
    }

    public void setDOBRequired(boolean DOBRequired) {
        isDOBRequired = DOBRequired;
    }

    public boolean isDOBVisible() {
        return isDOBVisible;
    }

    public void setDOBVisible(boolean DOBVisible) {
        isDOBVisible = DOBVisible;
    }
/*=================================================================================================================================================================*/


    public void initAddressList() {
        BillingShippingAddress billingShippingAddress = new BillingShippingAddress();
        billingShippingAddress.setValue(mContext.getString(R.string.checkout_billing_address_new_address_spinner));
        addressData.add(billingShippingAddress);
    }


//    private ArrayAdapter<String> adddressAdapter;

    public ArrayList<String> getAddressList() {
        ArrayList<String> addressArrayList = new ArrayList<>();
        for (BillingShippingAddress eachAddress : addressData) {
            addressArrayList.add(eachAddress.getValue());
        }
        return addressArrayList;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty("line.separator");

        result.append(this.getClass().getName());
        result.append(" Object {");
        result.append(newLine);

        //determine fields declared in this class only (no fields of superclass)
        Field[] fields = this.getClass().getDeclaredFields();

        //print field names paired with their values
        for (Field field : fields) {
            result.append("  ");
            try {
                result.append(field.getName());
                result.append(": ");
                //requires access to private field:
                result.append(field.get(this));
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
            result.append(newLine);
        }
        result.append("}");

        return result.toString();
    }

    @Bindable
    public boolean isShipToDifferentAddress() {
        return shipToDifferentAddress;
    }

    public void setShipToDifferentAddress(boolean shipToDifferentAddress) {
        this.shipToDifferentAddress = shipToDifferentAddress;
        notifyPropertyChanged(BR.shipToDifferentAddress);
    }

    public boolean isUseBillingAddress() {
        return useBillingAddress;
    }

    public void setUseBillingAddress(boolean useBillingAddress) {
        this.useBillingAddress = useBillingAddress;
        if (useBillingAddress) {
            setSelectedShippingAddressPosition(selectedBillingAddressPosition);
        }

        try {
/*in case new address is selected used for billing we need to update the shipping new address values*/
            if (getAddressData().get(getSelectedBillingAddressPosition()).getId() == 0) {
                setShippingPrefixValue(getBillingPrefixValue());

                BillingShippingFragment billingShippingFragment = (BillingShippingFragment) ((CheckoutActivity) mContext).getSupportFragmentManager().findFragmentByTag
                        (BillingShippingFragment.class.getSimpleName());

                billingShippingFragment.getBinding().shippingPrefixSpinner.setSelection(getBillingPrefixPosition());
                setShippingFirstName(getBillingFirstName());
                setShippingMiddleName(getBillingMiddleName());
                setShippingLastName(getBillingLastName());
                setShippingSuffixValue(getShippingSuffixValue());
                billingShippingFragment.getBinding().shippingSuffixSpinner.setSelection(getBillingSuffixPosition());
                updateBillingStreetAddressValidationStatus();
                billingShippingFragment.getBinding().shippingStreetAddressRv.setAdapter(new NewAddressStreetAddressRvAdapter(getContext()
                        , getBillingStreetAddressLines()
                        , getStreetLineCount()));
                updateShippingStreetAddressValidationStatus();
                setShippingCompany(getBillingCompany());
                setShippingTelephone(getBillingTelephone());
                setShippingFax(getBillingFax());
                setShippingCity(getBillingCity());
                setShippingStateValue(getBillingStateValue());
                billingShippingFragment.getBinding().shippingCountrySpinner.setSelection(getSelectedBillingCountryPosition());
                setShippingPostCode(getBillingPostCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Bindable
    public int getSelectedBillingAddressPosition() {
        return selectedBillingAddressPosition;
    }

    public void setSelectedBillingAddressPosition(int selectedBillingAddressPosition) {
        this.selectedBillingAddressPosition = selectedBillingAddressPosition;
        setShowNewAddressBillingForm(selectedBillingAddressPosition == addressData.size() - 1);
        notifyPropertyChanged(BR.selectedBillingAddressPosition);
    }

    @Bindable
    public int getSelectedShippingAddressPosition() {
        return selectedShippingAddressPosition;
    }

    public void setSelectedShippingAddressPosition(int selectedShippingAddressPosition) {
        this.selectedShippingAddressPosition = selectedShippingAddressPosition;
        setShowNewAddressShippingForm(selectedShippingAddressPosition == addressData.size() - 1);
        notifyPropertyChanged(BR.selectedShippingAddressPosition);
    }

    public JSONObject getBillingJsonData() {
        try {
            JSONObject billingDataJson = new JSONObject();
            billingDataJson.put("useForShipping", isShipToDifferentAddress() ? 0 : 1);
            billingDataJson.put("addressId", getAddressData().get(getSelectedBillingAddressPosition()).getId());


            JSONObject newAddressJson = new JSONObject();
            if (getAddressData().get(getSelectedBillingAddressPosition()).getId() == 0) {
                newAddressJson.put("prefix", getBillingPrefixValue());
                newAddressJson.put("firstName", getBillingFirstName());
                newAddressJson.put("middleName", getBillingMiddleName());
                newAddressJson.put("middleName", getBillingMiddleName());
                newAddressJson.put("lastName", getBillingLastName());
                newAddressJson.put("suffix", getBillingSuffixValue());
                newAddressJson.put("company", getBillingCompany());
                newAddressJson.put("email", getBillingEmail());
                newAddressJson.put("telephone", getBillingTelephone());
                newAddressJson.put("dob", getBillingDobValue().replace("/", "-"));
                newAddressJson.put("gender", getSelectedBillingGenderPosition() + 1);
                newAddressJson.put("fax", getBillingFax());
                newAddressJson.put("city", getBillingCity());
                newAddressJson.put("postcode", getBillingPostCode());

                newAddressJson.put("region_id", (isHasBillingStateData()) ? getCountryData().get(getSelectedBillingCountryPosition()).getStates().get(getSelectedBillingStatePosition()).getRegionId() : 0);
                newAddressJson.put("region", (isHasBillingStateData()) ? "" : getBillingStateValue());
                newAddressJson.put("country_id", getCountryData().get(getSelectedBillingCountryPosition()).getCountryId());
                JSONArray streetJsonArr = new JSONArray();
                for (int noOfSteet = 0; noOfSteet < getBillingStreetAddressLines().size(); noOfSteet++) {
                    streetJsonArr.put(getBillingStreetAddressLines().get(noOfSteet));
                }
                newAddressJson.put("street", streetJsonArr);
                newAddressJson.put("saveInAddressBook", isSaveBillingAddressInAddressBook() ? 1 : 0);
                newAddressJson.put("taxvat", getBillingTaxValue());
            }
            billingDataJson.put("newAddress", newAddressJson);

            return billingDataJson;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    private void updateBillingStreetAddressValidationStatus() {
        billingStreetAddressLines = new ArrayList<>();
        BillingShippingFragment billingShippingFragment = (BillingShippingFragment) ((CheckoutActivity) mContext).getSupportFragmentManager().findFragmentByTag(BillingShippingFragment.class.getSimpleName());

        NewAddressStreetAddressRvAdapter newBillingAddressStreetAddressRvAdapter = (NewAddressStreetAddressRvAdapter) billingShippingFragment.getBinding().billingStreetAddressRv.getAdapter();
        setBillingStreetAddressValidated(newBillingAddressStreetAddressRvAdapter.isStreetAddressValidated());

        for (int noOfSteet = 0; noOfSteet < getStreetLineCount(); noOfSteet++) {
            billingStreetAddressLines.add(newBillingAddressStreetAddressRvAdapter.getItem(noOfSteet));
        }
    }


    private void updateShippingStreetAddressValidationStatus() {
        shippingStreetAddressLines = new ArrayList<>();
        BillingShippingFragment billingShippingFragment = (BillingShippingFragment) ((CheckoutActivity) mContext).getSupportFragmentManager().findFragmentByTag
                (BillingShippingFragment.class.getSimpleName());

        NewAddressStreetAddressRvAdapter newShippingAddressStreetAddressRvAdapter = (NewAddressStreetAddressRvAdapter) billingShippingFragment.getBinding()
                .shippingStreetAddressRv.getAdapter();

        setShippingStreetAddressValidated(newShippingAddressStreetAddressRvAdapter.isStreetAddressValidated());

        for (int noOfSteet = 0; noOfSteet < getStreetLineCount(); noOfSteet++) {
            shippingStreetAddressLines.add(newShippingAddressStreetAddressRvAdapter.getItem(noOfSteet));
        }
    }

    public JSONObject getShippingJsonData() {
        try {
            JSONObject shippingDataJson = new JSONObject();
            shippingDataJson.put("sameAsBilling", isUseBillingAddress() ? 1 : 0);
            shippingDataJson.put("addressId", getAddressData().get(getSelectedShippingAddressPosition()).getId());

            JSONObject newAddressJson = new JSONObject();
            if (getAddressData().get(getSelectedShippingAddressPosition()).getId() == 0) {
                newAddressJson.put("prefix", getShippingPrefixValue());
                newAddressJson.put("firstName", getShippingFirstName());
                newAddressJson.put("middleName", getShippingMiddleName());
                newAddressJson.put("middleName", getShippingMiddleName());
                newAddressJson.put("lastName", getShippingLastName());
                newAddressJson.put("suffix", getShippingSuffixValue());
                newAddressJson.put("company", getShippingCompany());
                newAddressJson.put("email", getShippingEmail());
                newAddressJson.put("telephone", getShippingTelephone());
                newAddressJson.put("dob", getShippingDobValue().replace("/", "-"));
                newAddressJson.put("gender", getSelectedShippingGenderPosition() + 1);
                newAddressJson.put("fax", getShippingFax());
                newAddressJson.put("city", getShippingCity());
                newAddressJson.put("postcode", getShippingPostCode());

                newAddressJson.put("region_id", (isHasShippingStateData())
                        ? getCountryData().get(getSelectedShippingCountryPosition()).getStates().get(getSelectedShippingStatePosition()).getRegionId() : 0);
                newAddressJson.put("region", (isHasShippingStateData()) ? "" : getShippingStateValue());
                newAddressJson.put("country_id", getCountryData().get(getSelectedShippingCountryPosition()).getCountryId());
                JSONArray streetJsonArr = new JSONArray();
                for (int noOfSteet = 0; noOfSteet < getShippingStreetAddressLines().size(); noOfSteet++) {
                    streetJsonArr.put(getShippingStreetAddressLines().get(noOfSteet));
                }
                newAddressJson.put("street", streetJsonArr);
                newAddressJson.put("saveInAddressBook", isSaveShippingAddressInAddressBook() ? 1 : 0);
                newAddressJson.put("taxvat", getShippingTaxValue());
            }
            shippingDataJson.put("newAddress", newAddressJson);
            return shippingDataJson;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    public String getCheckoutMethodName(Context context) {
        if (AppSharedPref.getCustomerId(context) != 0) {
            return ApplicationConstant.METHOD_CUSTOMER;
        } else {
            return METHOD_GUEST;
        }
    }

    @Bindable
    public boolean isShowNewAddressShippingForm() {
        return showNewAddressShippingForm;
    }

    public void setShowNewAddressShippingForm(boolean showNewAddressShippingForm) {
        this.showNewAddressShippingForm = showNewAddressShippingForm;
        notifyPropertyChanged(BR.showNewAddressShippingForm);
    }

    @Bindable
    public boolean isShowNewAddressBillingForm() {
        return showNewAddressBillingForm;
    }

    public void setShowNewAddressBillingForm(boolean showNewAddressBillingForm) {
        this.showNewAddressBillingForm = showNewAddressBillingForm;
        notifyPropertyChanged(BR.showNewAddressBillingForm);
    }


    /*=================================================================================================================================================================*/


    public String getBillingEmail() {
        if ((billingEmail == null || billingEmail.isEmpty()) && AppSharedPref.isLoggedIn(mContext)) {
            return AppSharedPref.getCustomerEmail(mContext);
        }
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        setBillingEmailvalidated(billingEmail);
        this.billingEmail = billingEmail;
    }

    @Bindable
    public boolean isBillingEmailvalidated() {
        return billingEmailvalidated;
    }

    public void setBillingEmailvalidated(String billingEmail) {
        if (billingEmail.trim().isEmpty())
            this.billingEmailvalidated = false;
        else if (!EMAIL_PATTERN.matcher(billingEmail.trim()).matches())
            this.billingEmailvalidated = false;
        else
            this.billingEmailvalidated = true;
        notifyPropertyChanged(BR.billingEmailvalidated);
    }

    @Bindable
    public int getSelectedBillingCountryPosition() {
        return selectedBillingCountryPosition;
    }

    public void setSelectedBillingCountryPosition(int selectedBillingCountryPosition) {
        this.selectedBillingCountryPosition = selectedBillingCountryPosition;
        notifyPropertyChanged(BR.selectedBillingCountryPosition);
    }

    public int getSelectedBillingStatePosition() {
        return selectedBillingStatePosition;
    }

    public void setSelectedBillingStatePosition(int selectedBillingStatePosition) {
        this.selectedBillingStatePosition = selectedBillingStatePosition;
    }

    @Bindable
    public int getSelectedShippingCountryPosition() {
        return selectedShippingCountryPosition;
    }

    public void setSelectedShippingCountryPosition(int selectedShippingCountryPosition) {
        this.selectedShippingCountryPosition = selectedShippingCountryPosition;
        notifyPropertyChanged(BR.selectedShippingCountryPosition);
    }

    public int getSelectedShippingStatePosition() {
        return selectedShippingStatePosition;
    }

    public void setSelectedShippingStatePosition(int selectedShippingStatePosition) {
        this.selectedShippingStatePosition = selectedShippingStatePosition;
    }

    @Bindable
    public boolean isBillingFirstNameValidated() {
        return billingFirstNameValidated;
    }

    public void setBillingFirstNameValidated(boolean billingFirstNameValidated) {
        this.billingFirstNameValidated = billingFirstNameValidated;
        notifyPropertyChanged(BR.billingFirstNameValidated);
    }

    public String getBillingFirstName() {
        if (billingFirstName == null || billingFirstName.isEmpty()) {
            return firstName;
        }
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        setBillingFirstNameValidated(!billingFirstName.matches(""));
        this.billingFirstName = billingFirstName;
    }

    public String getBillingMiddleName() {
        if (billingMiddleName == null || billingMiddleName.isEmpty()) {
            return middleName;
        }
        return billingMiddleName;
    }

    public void setBillingMiddleName(String billingMiddleName) {
        this.billingMiddleName = billingMiddleName;
    }

    public String getBillingLastName() {
        if (billingLastName == null || billingLastName.isEmpty()) {
            return lastName;
        }
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        setBillingLastNameValidated(!billingLastName.matches(""));
        this.billingLastName = billingLastName;
    }

    @Bindable
    public boolean isBillingLastNameValidated() {
        return billingLastNameValidated;
    }

    public void setBillingLastNameValidated(boolean billingLastNameValidated) {
        this.billingLastNameValidated = billingLastNameValidated;
        notifyPropertyChanged(BR.billingLastNameValidated);
    }

    @Bindable
    public boolean isBillingPrefixValidated() {
        return billingPrefixValidated;
    }

    public void setBillingPrefixValidated(boolean billingPrefixValidated) {
        this.billingPrefixValidated = billingPrefixValidated;
        notifyPropertyChanged(BR.billingPrefixValidated);
    }

    public String getBillingPrefixValue() {
        if (billingPrefixValue == null) {
            return prefixValue;
        }
        return billingPrefixValue;
    }

    public void setBillingPrefixValue(String billingPrefixValue) {
        setBillingPrefixValidated(!billingPrefixValue.matches(""));
        this.billingPrefixValue = billingPrefixValue;
    }


    public int getBillingPrefixPosition() {
        return billingPrefixPosition;
    }

    public void setBillingPrefixPosition(int billingPrefixPosition) {
        this.billingPrefixPosition = billingPrefixPosition;
    }


    public String getBillingSuffixValue() {
        if (billingSuffixValue == null) {
            return suffixValue;
        }
        return billingSuffixValue;
    }

    public void setBillingSuffixValue(String billingSuffixValue) {
        setBillingSuffixValidated(!billingSuffixValue.matches(""));
        this.billingSuffixValue = billingSuffixValue;
    }


    public int getBillingSuffixPosition() {
        return billingSuffixPosition;
    }

    public void setBillingSuffixPosition(int billingSuffixPosition) {
        this.billingSuffixPosition = billingSuffixPosition;
    }

    @Bindable
    public boolean isBillingSuffixValidated() {
        return billingSuffixValidated;
    }

    public void setBillingSuffixValidated(boolean billingSuffixValidated) {
        this.billingSuffixValidated = billingSuffixValidated;
        notifyPropertyChanged(BR.billingSuffixValidated);
    }

    /**
     * creating country name list from the available country data
     */
    public void initCountryNameList() {
        List<String> countryNameList = new ArrayList<>();
        for (CountryData eachCountryData : getCountryData()) {
            countryNameList.add(eachCountryData.getName());
        }
        setCountryNameList(countryNameList);
    }


    public List<String> getCountryNameList() {
        return countryNameList;
    }

    public void setCountryNameList(List<String> countryNameList) {
        this.countryNameList = countryNameList;
    }

    @Bindable
    public boolean isHasBillingStateData() {
        return hasBillingStateData;
    }

    public void setHasBillingStateData(boolean hasBillingStateData) {
        this.hasBillingStateData = hasBillingStateData;
        notifyPropertyChanged(BR.hasBillingStateData);
    }

    public List<String> getStateNameList(int countryPosition) {
        List<String> stateNameList = new ArrayList<>();
        for (State eachStateData : countryData.get(countryPosition).getStates()) {
            stateNameList.add(eachStateData.getName());
        }
        return stateNameList;
    }

    public String getBillingCompany() {
        return billingCompany;
    }

    public void setBillingCompany(String billingCompany) {
        this.billingCompany = billingCompany;
    }

    public String getBillingTelephone() {
        return billingTelephone;
    }

    public void setBillingTelephone(String billingTelephone) {
        setBillingTelephoneValidated(!billingTelephone.trim().isEmpty());
        this.billingTelephone = billingTelephone;
    }

    public String getBillingFax() {
        return billingFax;
    }

    public void setBillingFax(String billingFax) {
        this.billingFax = billingFax;
    }

    @Bindable
    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        setBillingCityValidated(!billingCity.trim().isEmpty());
        this.billingCity = billingCity;
        notifyPropertyChanged(BR.billingCity);
    }

    @Bindable
    public boolean isBillingCityValidated() {
        return billingCityValidated;
    }

    public void setBillingCityValidated(boolean billingCityValidated) {
        this.billingCityValidated = billingCityValidated;
        notifyPropertyChanged(BR.billingCityValidated);
    }

    @Bindable
    public boolean isBillingTelephoneValidated() {
        return billingTelephoneValidated;
    }

    public void setBillingTelephoneValidated(boolean billingTelephoneValidated) {
        this.billingTelephoneValidated = billingTelephoneValidated;
        notifyPropertyChanged(BR.billingTelephoneValidated);
    }

    @Bindable
    public String getBillingPostCode() {
        return billingPostCode;
    }

    public void setBillingPostCode(String billingPostCode) {
        setBillingPostCodeValidated(!billingPostCode.trim().isEmpty());
        this.billingPostCode = billingPostCode;
        notifyPropertyChanged(BR.billingPostCode);
    }

    @Bindable
    public boolean isBillingPostCodeValidated() {
        return billingPostCodeValidated;
    }

    public void setBillingPostCodeValidated(boolean billingPostCodeValidated) {
        this.billingPostCodeValidated = billingPostCodeValidated;
        notifyPropertyChanged(BR.billingPostCodeValidated);
    }

    public boolean isSaveBillingAddressInAddressBook() {
        return saveBillingAddressInAddressBook;
    }

    public void setSaveBillingAddressInAddressBook(boolean saveBillingAddressInAddressBook) {
        this.saveBillingAddressInAddressBook = saveBillingAddressInAddressBook;
    }

    @Bindable
    public boolean isBillingDobValidated() {
        return billingDobValidated;
    }

    public void setBillingDobValidated(boolean billingDobValidated) {
        this.billingDobValidated = billingDobValidated;
        notifyPropertyChanged(BR.billingDobValidated);
    }

    @Bindable
    public String getBillingDobValue() {
        if (billingDobValue == null) return "";
        return billingDobValue;
    }

    public void setBillingDobValue(String billingDobValue) {
        setBillingDobValidated(!billingDobValue.trim().isEmpty());
        this.billingDobValue = billingDobValue;
        notifyPropertyChanged(BR.billingDobValue);
    }

    @Bindable
    public String getBillingTaxValue() {
        return billingTaxValue;
    }

    public void setBillingTaxValue(String billingTaxValue) {
        setBillingTaxValidated(!billingTaxValue.trim().isEmpty());
        this.billingTaxValue = billingTaxValue;
        notifyPropertyChanged(BR.billingTaxValue);
    }

    @Bindable
    public boolean isBillingTaxValidated() {
        return billingTaxValidated;
    }

    public void setBillingTaxValidated(boolean billingTaxValidated) {
        this.billingTaxValidated = billingTaxValidated;
        notifyPropertyChanged(BR.billingTaxValidated);
    }

    public int getSelectedBillingGenderPosition() {
        return selectedBillingGenderPosition;
    }

    public void setSelectedBillingGenderPosition(int selectedBillingGenderPosition) {
        this.selectedBillingGenderPosition = selectedBillingGenderPosition;
    }

    @Bindable
    public String getBillingStateValue() {
        return billingStateValue;
    }

    public void setBillingStateValue(String billingStateValue) {
        setBillingStateValidated(!billingStateValue.trim().isEmpty());
        this.billingStateValue = billingStateValue;
        notifyPropertyChanged(BR.billingStateValue);
    }

    public boolean isBillingStreetAddressValidated() {
        return billingStreetAddressValidated;
    }

    public void setBillingStreetAddressValidated(boolean billingStreetAddressValidated) {
        this.billingStreetAddressValidated = billingStreetAddressValidated;
    }

     /*=================================================================================================================================================================*/

    public List<String> getBillingStreetAddressLines() {
        return billingStreetAddressLines;
    }

    public void setBillingStreetAddressLines(List<String> billingStreetAddressLines) {
        this.billingStreetAddressLines = billingStreetAddressLines;
    }

    public String getShippingPrefixValue() {
        if (shippingPrefixValue == null) {
            return prefixValue;
        }
        return shippingPrefixValue;
    }

    public void setShippingPrefixValue(String shippingPrefixValue) {
        setShippingPrefixValidated(!shippingPrefixValue.matches(""));
        this.shippingPrefixValue = shippingPrefixValue;
    }

    public int getShippingPrefixPosition() {
        return shippingPrefixPosition;
    }

    public void setShippingPrefixPosition(int shippingPrefixPosition) {
        this.shippingPrefixPosition = shippingPrefixPosition;
    }


    @Bindable
    public boolean isShippingPrefixValidated() {
        return shippingPrefixValidated;
    }

    public void setShippingPrefixValidated(boolean shippingPrefixValidated) {
        this.shippingPrefixValidated = shippingPrefixValidated;
        notifyPropertyChanged(BR.shippingPrefixValidated);
    }

    @Bindable
    public boolean isShippingFirstNameValidated() {
        return shippingFirstNameValidated;
    }

    public void setShippingFirstNameValidated(boolean shippingFirstNameValidated) {
        this.shippingFirstNameValidated = shippingFirstNameValidated;
        notifyPropertyChanged(BR.shippingFirstNameValidated);
    }

    @Bindable
    public String getShippingFirstName() {
        if (shippingFirstName == null || shippingFirstName.isEmpty()) {
            return firstName;
        }
        return shippingFirstName;
    }

    public void setShippingFirstName(String shippingFirstName) {
        setShippingFirstNameValidated(!shippingFirstName.matches(""));
        this.shippingFirstName = shippingFirstName;
    }

    @Bindable
    public String getShippingMiddleName() {
        if (shippingMiddleName == null || shippingMiddleName.isEmpty()) {
            return middleName;
        }
        return shippingMiddleName;
    }

    public void setShippingMiddleName(String shippingMiddleName) {
        this.shippingMiddleName = shippingMiddleName;
        notifyPropertyChanged(BR.shippingMiddleName);
    }

    @Bindable
    public String getShippingLastName() {
        if (shippingLastName == null || shippingLastName.isEmpty()) {
            return lastName;
        }
        return shippingLastName;
    }

    public void setShippingLastName(String shippingLastName) {
        setShippingLastNameValidated(!shippingLastName.matches(""));
        this.shippingLastName = shippingLastName;
    }

    @Bindable
    public boolean isShippingLastNameValidated() {
        return shippingLastNameValidated;
    }

    public void setShippingLastNameValidated(boolean shippingLastNameValidated) {
        this.shippingLastNameValidated = shippingLastNameValidated;
        notifyPropertyChanged(BR.shippingLastNameValidated);
    }

    @Bindable
    public String getShippingSuffixValue() {
        if (shippingSuffixValue == null) {
            return suffixValue;
        }
        return shippingSuffixValue;
    }

    public void setShippingSuffixValue(String shippingSuffixValue) {
        setShippingSuffixValidated(!shippingSuffixValue.matches(""));
        this.shippingSuffixValue = shippingSuffixValue;
        notifyPropertyChanged(BR.shippingSuffixValue);
    }

    public int getShippingSuffixPosition() {
        return shippingSuffixPosition;
    }

    public void setShippingSuffixPosition(int shippingSuffixPosition) {
        this.shippingSuffixPosition = shippingSuffixPosition;
    }

    @Bindable
    public boolean isShippingSuffixValidated() {
        return shippingSuffixValidated;
    }

    public void setShippingSuffixValidated(boolean shippingSuffixValidated) {
        this.shippingSuffixValidated = shippingSuffixValidated;
        notifyPropertyChanged(BR.shippingSuffixValidated);
    }

    @Bindable
    public boolean isHasShippingStateData() {
        return hasShippingStateData;
    }

    public void setHasShippingStateData(boolean hasShippingStateData) {
        this.hasShippingStateData = hasShippingStateData;
        notifyPropertyChanged(BR.hasShippingStateData);
    }

    public boolean isSaveShippingAddressInAddressBook() {
        return saveShippingAddressInAddressBook;
    }

    public void setSaveShippingAddressInAddressBook(boolean saveShippingAddressInAddressBook) {
        this.saveShippingAddressInAddressBook = saveShippingAddressInAddressBook;
    }


    @Bindable
    public boolean isShippingDobValidated() {
        return shippingDobValidated;
    }

    public void setShippingDobValidated(boolean shippingDobValidated) {
        this.shippingDobValidated = shippingDobValidated;
        notifyPropertyChanged(BR.shippingDobValidated);
    }

    @Bindable
    public String getShippingDobValue() {
        if (shippingDobValue == null) {
            return "";
        }
        return shippingDobValue;
    }

    public void setShippingDobValue(String shippingDobValue) {
        setShippingDobValidated(!shippingDobValue.trim().isEmpty());
        this.shippingDobValue = shippingDobValue;
        notifyPropertyChanged(BR.shippingDobValue);
    }

    @Bindable
    public String getShippingTaxValue() {
        return shippingTaxValue;
    }

    public void setShippingTaxValue(String shippingTaxValue) {
        setShippingTaxValidated(!shippingTaxValue.trim().isEmpty());
        this.shippingTaxValue = shippingTaxValue;
        notifyPropertyChanged(BR.shippingTaxValue);
    }

    @Bindable
    public boolean isShippingTaxValidated() {
        return shippingTaxValidated;
    }

    public void setShippingTaxValidated(boolean shippingTaxValidated) {
        this.shippingTaxValidated = shippingTaxValidated;
        notifyPropertyChanged(BR.shippingTaxValidated);
    }


    public int getSelectedShippingGenderPosition() {
        return selectedShippingGenderPosition;
    }

    public void setSelectedShippingGenderPosition(int selectedShippingGenderPosition) {
        this.selectedShippingGenderPosition = selectedShippingGenderPosition;
    }

    /*=================================================================================================================================================================*/

    public boolean isFormValidated() {
/*in case of new billing address*/
        if (getAddressData().get(getSelectedBillingAddressPosition()).getId() == 0) {

            updateBillingStreetAddressValidationStatus();
            BillingShippingFragment billingShippingFragment = (BillingShippingFragment) ((CheckoutActivity) mContext).getSupportFragmentManager().findFragmentByTag(BillingShippingFragment.class.getSimpleName());
            FragmentBillingShippingBinding fragmentBillingShippingBinding = billingShippingFragment.getBinding();

            Log.d(ApplicationConstant.TAG, "BillingShippingInfoData isFormValidated isBillingStreetAddressValidated : " + !isBillingStreetAddressValidated());

            if (isIsPrefixRequired() && !isBillingPrefixValidated()) {
                fragmentBillingShippingBinding.billingPrefixTil.requestFocus();
                return false;
            } else if (!isBillingFirstNameValidated()) {
                fragmentBillingShippingBinding.billingFirstNameTil.requestFocus();
                return false;
            } else if (!isBillingLastNameValidated()) {
                fragmentBillingShippingBinding.billingLastNameTil.requestFocus();
                return false;
            } else if (isIsSuffixRequired() && !isBillingSuffixValidated()) {
                fragmentBillingShippingBinding.billingSuffixTil.requestFocus();
                return false;
            } else if (!isBillingEmailvalidated() && getCheckoutMethodName(mContext).equals(METHOD_GUEST)) {
                fragmentBillingShippingBinding.emailTil.requestFocus();
                return false;
            } else if (!isBillingTelephoneValidated()) {
                fragmentBillingShippingBinding.billingTelephoneTil.requestFocus();
                return false;
            } else if (isDOBRequired() && !isBillingDobValidated()) {
                fragmentBillingShippingBinding.billingDobTil.requestFocus();
                return false;
            } else if (!isBillingStreetAddressValidated()) {
                fragmentBillingShippingBinding.billingStreetAddressRv.requestFocus();
                return false;
            } else if (!isBillingCityValidated()) {
                fragmentBillingShippingBinding.billingCityTil.requestFocus();
                return false;
            } else if (!isHasBillingStateData() && !getBillingStateValidated()) {
                fragmentBillingShippingBinding.billingStateTil.requestFocus();
                return false;
            } else if (!isBillingPostCodeValidated()) {
                fragmentBillingShippingBinding.billingPostcodeTil.requestFocus();
                return false;
            } else if (isTaxRequired() && !isBillingTaxValidated()) {
                fragmentBillingShippingBinding.billingTaxTil.requestFocus();
                return false;
            }
        }

/*Validating Shipping New Address Form data*/
        if (isShipToDifferentAddress() && getAddressData().get(getSelectedShippingAddressPosition()).getId() == 0) {
            updateShippingStreetAddressValidationStatus();
            BillingShippingFragment billingShippingFragment = (BillingShippingFragment) ((CheckoutActivity) mContext).getSupportFragmentManager().findFragmentByTag
                    (BillingShippingFragment.class.getSimpleName());
            FragmentBillingShippingBinding fragmentBillingShippingBinding = billingShippingFragment.getBinding();
            if (isIsPrefixRequired() && !isShippingPrefixValidated()) {
                fragmentBillingShippingBinding.shippingPrefixTil.requestFocus();
                return false;
            } else if (!isShippingFirstNameValidated()) {
                fragmentBillingShippingBinding.shippingFirstNameTil.requestFocus();
                return false;
            } else if (!isShippingLastNameValidated()) {
                fragmentBillingShippingBinding.shippingLastNameTil.requestFocus();
                return false;
            } else if (isIsSuffixRequired() && !isShippingSuffixValidated()) {
                fragmentBillingShippingBinding.shippingSuffixTil.requestFocus();
                return false;
            } else if (!isShippingEmailValidated() && getCheckoutMethodName(mContext).equals(METHOD_GUEST)) {
                fragmentBillingShippingBinding.shippingEmailTil.requestFocus();
                return false;
            } else if (!isShippingTelephoneValidated()) {
                fragmentBillingShippingBinding.shippingTelephoneTil.requestFocus();
                return false;
            } else if (isDOBRequired() && !isShippingDobValidated()) {
                fragmentBillingShippingBinding.shippingDobTil.requestFocus();
                return false;
            } else if (!isShippingStreetAddressValidated()) {
                fragmentBillingShippingBinding.shippingStreetAddressRv.requestFocus();
                return false;
            } else if (!isShippingCityValidated()) {
                fragmentBillingShippingBinding.shippingCityTil.requestFocus();
                return false;
            } else if (!isHasShippingStateData() && !isShippingStateValidated()) {
                fragmentBillingShippingBinding.shippingStateTil.requestFocus();
                return false;
            } else if (!isShippingPostCodeValidated()) {
                fragmentBillingShippingBinding.shippingPostcodeTil.requestFocus();
                return false;
            } else if (isTaxRequired() && !isShippingTaxValidated()) {
                fragmentBillingShippingBinding.shippingTaxTil.requestFocus();
                return false;
            }
        }
        return true;
    }

    @Bindable
    public boolean getBillingStateValidated() {
        return billingStateValidated;
    }

    public void setBillingStateValidated(boolean billingStateValidated) {
        this.billingStateValidated = billingStateValidated;
        notifyPropertyChanged(BR.billingStateValidated);
    }

    public List<String> getShippingStreetAddressLines() {
        return shippingStreetAddressLines;
    }

    public void setShippingStreetAddressLines(List<String> shippingStreetAddressLines) {
        this.shippingStreetAddressLines = shippingStreetAddressLines;
    }

    @Bindable
    public boolean isShippingStreetAddressValidated() {
        return shippingStreetAddressValidated;
    }

    public void setShippingStreetAddressValidated(boolean shippingStreetAddressValidated) {
        this.shippingStreetAddressValidated = shippingStreetAddressValidated;
        notifyPropertyChanged(BR.shippingStreetAddressValidated);
    }

    public String getShippingEmail() {
        if (((shippingEmail == null) || shippingEmail.isEmpty()) && AppSharedPref.isLoggedIn(mContext)) {
            return AppSharedPref.getCustomerEmail(mContext);
        }
        return shippingEmail;
    }

    public void setShippingEmail(String shippingEmail) {
        setShippingEmailValidated(shippingEmail);
        this.shippingEmail = shippingEmail;
    }

    @Bindable
    public boolean isShippingEmailValidated() {
        return shippingEmailValidated;
    }

    public void setShippingEmailValidated(String shippingEmail) {
        if (shippingEmail.trim().isEmpty())
            this.shippingEmailValidated = false;
        else if (!EMAIL_PATTERN.matcher(shippingEmail.trim()).matches())
            this.shippingEmailValidated = false;
        else
            this.shippingEmailValidated = true;
        notifyPropertyChanged(BR.shippingEmailValidated);
    }

    @Bindable
    public boolean isShippingTelephoneValidated() {
        return shippingTelephoneValidated;
    }

    public void setShippingTelephoneValidated(boolean shippingTelephoneValidated) {
        this.shippingTelephoneValidated = shippingTelephoneValidated;
        notifyPropertyChanged(BR.shippingTelephoneValidated);
    }

    @Bindable
    public String getShippingTelephone() {
        return shippingTelephone;
    }

    public void setShippingTelephone(String shippingTelephone) {
        setShippingTelephoneValidated(!shippingTelephone.trim().isEmpty());
        this.shippingTelephone = shippingTelephone;
        notifyPropertyChanged(BR.shippingTelephone);
    }

    @Bindable
    public boolean isShippingCityValidated() {
        return shippingCityValidated;
    }

    public void setShippingCityValidated(boolean shippingCityValidated) {
        this.shippingCityValidated = shippingCityValidated;
        notifyPropertyChanged(BR.shippingCityValidated);
    }

    @Bindable
    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        setShippingCityValidated(!shippingCity.trim().isEmpty());
        this.shippingCity = shippingCity;
        notifyPropertyChanged(BR.shippingCity);
    }

    @Bindable
    public String getShippingStateValue() {
        return shippingStateValue;
    }

    public void setShippingStateValue(String shippingStateValue) {
        setShippingStateValidated(!shippingStateValue.trim().isEmpty());
        this.shippingStateValue = shippingStateValue;
        notifyPropertyChanged(BR.shippingStateValue);
    }

    @Bindable
    public boolean isShippingStateValidated() {
        return shippingStateValidated;
    }

    public void setShippingStateValidated(boolean shippingStateValidated) {
        this.shippingStateValidated = shippingStateValidated;
        notifyPropertyChanged(BR.shippingStateValidated);
    }

    @Bindable
    public String getShippingPostCode() {
        return shippingPostCode;
    }

    public void setShippingPostCode(String shippingPostCode) {
        Log.d(ApplicationConstant.TAG, "BillingShippingInfoData setShippingPostCode: " + shippingPostCode);
        setShippingPostCodeValidated(!shippingPostCode.trim().isEmpty());
        this.shippingPostCode = shippingPostCode;
        notifyPropertyChanged(BR.shippingPostCode);
    }

    @Bindable
    public boolean isShippingPostCodeValidated() {
        return shippingPostCodeValidated;
    }

    public void setShippingPostCodeValidated(boolean shippingPostCodeValidated) {
        this.shippingPostCodeValidated = shippingPostCodeValidated;
        notifyPropertyChanged(BR.shippingPostCodeValidated);
    }

    @Bindable
    public String getShippingCompany() {
        return shippingCompany;
    }

    public void setShippingCompany(String shippingCompany) {
        this.shippingCompany = shippingCompany;
        notifyPropertyChanged(BR.shippingCompany);
    }

    @Bindable
    public String getShippingFax() {
        return shippingFax;
    }

    public void setShippingFax(String shippingFax) {
        this.shippingFax = shippingFax;
        notifyPropertyChanged(BR.shippingFax);
    }

}
