package com.webkul.mobikul.model.checkout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 3/4/18. @Webkul Software Private limited
 */

public class OpenPayResponseData extends BaseModel {

    @SerializedName("openpay_token")
    @Expose
    public String openpayToken;
    @SerializedName("device_session_id")
    @Expose
    public String deviceSessionId;

    public String getOpenpayToken() {
        return openpayToken;
    }

    public String getDeviceSessionId() {
        return deviceSessionId;
    }

}