package com.webkul.mobikul.model.checkout;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.cart.Discount;
import com.webkul.mobikul.model.cart.GrandTotal;
import com.webkul.mobikul.model.cart.Shipping;
import com.webkul.mobikul.model.cart.Subtotal;
import com.webkul.mobikul.model.cart.Tax;

import java.util.List;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */
public class OrderReviewData implements Parcelable {

    public static final Creator<OrderReviewData> CREATOR = new Creator<OrderReviewData>() {
        @Override
        public OrderReviewData createFromParcel(Parcel in) {
            return new OrderReviewData(in);
        }

        @Override
        public OrderReviewData[] newArray(int size) {
            return new OrderReviewData[size];
        }
    };
    @SerializedName("items")
    @Expose
    private List<OrderReviewItemProduct> items = null;
    @SerializedName("subtotal")
    @Expose
    private Subtotal subtotal;
    @SerializedName("shipping")
    @Expose
    private Shipping shipping;
    @SerializedName("grandtotal")
    @Expose
    private GrandTotal grandTotal;
    @SerializedName("discount")
    @Expose
    private Discount discount;
    @SerializedName("tax")
    @Expose
    private Tax tax;
    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;

    protected OrderReviewData(Parcel in) {
        items = in.createTypedArrayList(OrderReviewItemProduct.CREATOR);
        subtotal = in.readParcelable(Subtotal.class.getClassLoader());
        shipping = in.readParcelable(Shipping.class.getClassLoader());
        grandTotal = in.readParcelable(GrandTotal.class.getClassLoader());
        discount = in.readParcelable(Discount.class.getClassLoader());
        tax = in.readParcelable(Tax.class.getClassLoader());
        currencyCode = in.readString();
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(items);
        dest.writeParcelable(subtotal, flags);
        dest.writeParcelable(shipping, flags);
        dest.writeParcelable(grandTotal, flags);
        dest.writeParcelable(discount, flags);
        dest.writeParcelable(tax, flags);
        dest.writeString(currencyCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<OrderReviewItemProduct> getItems() {
        return items;
    }

    public void setItems(List<OrderReviewItemProduct> items) {
        this.items = items;
    }

    public Subtotal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Subtotal subtotal) {
        this.subtotal = subtotal;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public GrandTotal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(GrandTotal grandtotal) {
        this.grandTotal = grandtotal;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

}
