package com.webkul.mobikul.model.checkout;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class OrderReviewItemProduct implements Parcelable {

    public static final Creator<OrderReviewItemProduct> CREATOR = new Creator<OrderReviewItemProduct>() {
        @Override
        public OrderReviewItemProduct createFromParcel(Parcel in) {
            return new OrderReviewItemProduct(in);
        }

        @Override
        public OrderReviewItemProduct[] newArray(int size) {
            return new OrderReviewItemProduct[size];
        }
    };
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("unformatedPrice")
    @Expose
    private float unformatedPrice;
    @SerializedName("qty")
    @Expose
    private int qty;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("subTotal")
    @Expose
    private String subTotal;
    @SerializedName("thumbNail")
    @Expose
    private String thumbNail;

    protected OrderReviewItemProduct(Parcel in) {
        productName = in.readString();
        price = in.readString();
        unformatedPrice = in.readFloat();
        qty = in.readInt();
        sku = in.readString();
        subTotal = in.readString();
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productName);
        dest.writeString(price);
        dest.writeFloat(unformatedPrice);
        dest.writeInt(qty);
        dest.writeString(sku);
        dest.writeString(subTotal);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public float getUnformatedPrice() {
        return unformatedPrice;
    }

    public void setUnformatedPrice(float unformatedPrice) {
        this.unformatedPrice = unformatedPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }
}
