package com.webkul.mobikul.model.checkout;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */
public class OrderReviewRequestData extends BaseModel implements Parcelable {
    public static final Creator<OrderReviewRequestData> CREATOR = new Creator<OrderReviewRequestData>() {
        @Override
        public OrderReviewRequestData createFromParcel(Parcel in) {
            return new OrderReviewRequestData(in);
        }

        @Override
        public OrderReviewRequestData[] newArray(int size) {
            return new OrderReviewRequestData[size];
        }
    };
    @SerializedName("billingAddress")
    @Expose
    private String billingAddress;
    @SerializedName("shippingAddress")
    @Expose
    private String shippingAddress;
    @SerializedName("shippingMethod")
    @Expose
    private String shippingMethod;
    @SerializedName("billingMethod")
    @Expose
    private String billingMethod;
    @SerializedName("orderReviewData")
    @Expose
    private OrderReviewData orderReviewData;

    protected OrderReviewRequestData(Parcel in) {
        billingAddress = in.readString();
        shippingAddress = in.readString();
        shippingMethod = in.readString();
        billingMethod = in.readString();
        orderReviewData = in.readParcelable(OrderReviewData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(billingAddress);
        dest.writeString(shippingAddress);
        dest.writeString(shippingMethod);
        dest.writeString(billingMethod);
        dest.writeParcelable(orderReviewData, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getBillingAddress() {
        return billingAddress.replaceAll("<br>", "\n");
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getShippingAddress() {
        return shippingAddress.replaceAll("<br>", "\n");
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getBillingMethod() {
        return billingMethod;
    }

    public void setBillingMethod(String billingMethod) {
        this.billingMethod = billingMethod;
    }

    public OrderReviewData getOrderReviewData() {
        return orderReviewData;
    }

    public void setOrderReviewData(OrderReviewData orderReviewData) {
        this.orderReviewData = orderReviewData;
    }

}
