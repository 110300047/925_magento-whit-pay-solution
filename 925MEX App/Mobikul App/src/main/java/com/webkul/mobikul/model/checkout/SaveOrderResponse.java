package com.webkul.mobikul.model.checkout;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created with passion and love by vedesh.kumar on 13/1/17. @Webkul Software Pvt. Ltd
 */

public class SaveOrderResponse extends BaseModel implements Parcelable {


    public static final Creator<SaveOrderResponse> CREATOR = new Creator<SaveOrderResponse>() {
        @Override
        public SaveOrderResponse createFromParcel(Parcel in) {
            return new SaveOrderResponse(in);
        }

        @Override
        public SaveOrderResponse[] newArray(int size) {
            return new SaveOrderResponse[size];
        }
    };
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("incrementId")
    @Expose
    private String incrementId;
    @SerializedName("canReorder")
    @Expose
    private boolean canReorder;
    @SerializedName("showCreateAccountLink")
    @Expose
    private boolean showCreateAccountLink;
    @SerializedName("email")
    @Expose
    private String email;

    protected SaveOrderResponse(Parcel in) {
        orderId = in.readString();
        incrementId = in.readString();
        canReorder = in.readByte() != 0;
        showCreateAccountLink = in.readByte() != 0;
        email = in.readString();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public boolean isCanReorder() {
        return canReorder;
    }

    public void setCanReorder(boolean canReorder) {
        this.canReorder = canReorder;
    }

    public boolean isShowCreateAccountLink() {
        return showCreateAccountLink;
    }

    public void setShowCreateAccountLink(boolean showCreateAccountLink) {
        this.showCreateAccountLink = showCreateAccountLink;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderId);
        dest.writeString(incrementId);
        dest.writeByte((byte) (canReorder ? 1 : 0));
        dest.writeByte((byte) (showCreateAccountLink ? 1 : 0));
        dest.writeString(email);
    }
}
