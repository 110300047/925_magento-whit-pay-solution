package com.webkul.mobikul.model.checkout;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */
public class ShippingMethod implements Parcelable {

    public static final Creator<ShippingMethod> CREATOR = new Creator<ShippingMethod>() {
        @Override
        public ShippingMethod createFromParcel(Parcel in) {
            return new ShippingMethod(in);
        }

        @Override
        public ShippingMethod[] newArray(int size) {
            return new ShippingMethod[size];
        }
    };
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("method")
    @Expose
    private List<ShippingMethodDetailData> mShippingMethodDetailDataList = null;

    private ShippingMethod(Parcel in) {
        title = in.readString();
        mShippingMethodDetailDataList = in.createTypedArrayList(ShippingMethodDetailData.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeTypedList(mShippingMethodDetailDataList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ShippingMethodDetailData> getMethod() {
        return mShippingMethodDetailDataList;
    }

    public void setMethod(List<ShippingMethodDetailData> method) {
        this.mShippingMethodDetailDataList = method;
    }

}
