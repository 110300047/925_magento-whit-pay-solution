package com.webkul.mobikul.model.checkout;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created with passion and love by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */
public class ShippingMethodDetailData implements Parcelable {

    public static final Creator<ShippingMethodDetailData> CREATOR = new Creator<ShippingMethodDetailData>() {
        @Override
        public ShippingMethodDetailData createFromParcel(Parcel in) {
            return new ShippingMethodDetailData(in);
        }

        @Override
        public ShippingMethodDetailData[] newArray(int size) {
            return new ShippingMethodDetailData[size];
        }
    };
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("price")
    @Expose

    private String price;

    private ShippingMethodDetailData(Parcel in) {
        error = in.readString();
        code = in.readString();
        label = in.readString();
        price = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(error);
        dest.writeString(code);
        dest.writeString(label);
        dest.writeString(price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getError() {
        return error;
    }
}