package com.webkul.mobikul.model.checkout;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with passion and love by vedesh.kumar on 11/1/17. @Webkul Software Pvt. Ltd
 */

public class ShippingMethodInfoData extends BaseModel implements Parcelable {
    @SerializedName("shippingMethods")
    @Expose
    private List<ShippingMethod> shippingMethods = null;
    @SerializedName("paymentMethods")
    @Expose
    private ArrayList<PaymentMethod> paymentMethods = null;
    @SerializedName("merchantId")
    @Expose
    public String merchantId;
    @SerializedName("publicKey")
    @Expose
    public String publicKey;
    @SerializedName("isSandbox")
    @Expose
    public int isSandbox;

    private String selectedShippingMethodCode = "";

    protected ShippingMethodInfoData(Parcel in) {
        shippingMethods = in.createTypedArrayList(ShippingMethod.CREATOR);
        paymentMethods = in.createTypedArrayList(PaymentMethod.CREATOR);
        merchantId = in.readString();
        publicKey = in.readString();
        isSandbox = in.readInt();
        selectedShippingMethodCode = in.readString();
    }

    public static final Creator<ShippingMethodInfoData> CREATOR = new Creator<ShippingMethodInfoData>() {
        @Override
        public ShippingMethodInfoData createFromParcel(Parcel in) {
            return new ShippingMethodInfoData(in);
        }

        @Override
        public ShippingMethodInfoData[] newArray(int size) {
            return new ShippingMethodInfoData[size];
        }
    };

    public String getSelectedShippingMethodCode() {
        if (selectedShippingMethodCode == null) {
            return "";
        }
        return selectedShippingMethodCode;
    }

    public void setSelectedShippingMethodCode(String selectedShippingMethodCode) {
        this.selectedShippingMethodCode = selectedShippingMethodCode;
    }

    public List<ShippingMethod> getShippingMethods() {
        return shippingMethods;
    }

    public void setShippingMethods(List<ShippingMethod> shippingMethods) {
        this.shippingMethods = shippingMethods;
    }

    public ArrayList<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(ArrayList<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public int getIsSandbox() {
        return isSandbox;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty("line.separator");

        result.append(this.getClass().getName());
        result.append(" Object {");
        result.append(newLine);

        //determine fields declared in this class only (no fields of superclass)
        Field[] fields = this.getClass().getDeclaredFields();

        //print field names paired with their values
        for (Field field : fields) {
            result.append("  ");
            try {
                result.append(field.getName());
                result.append(": ");
                //requires access to private field:
                result.append(field.get(this));
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
            result.append(newLine);
        }
        result.append("}");

        return result.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(shippingMethods);
        dest.writeTypedList(paymentMethods);
        dest.writeString(merchantId);
        dest.writeString(publicKey);
        dest.writeInt(isSandbox);
        dest.writeString(selectedShippingMethodCode);
    }
}
