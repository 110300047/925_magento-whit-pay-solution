package com.webkul.mobikul.model.compare;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AttributeValueList {

    @SerializedName("attributeName")
    @Expose
    private String attributeName;
    @SerializedName("value")
    @Expose
    private ArrayList<String> value = null;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public ArrayList<String> getValue() {
        return value;
    }

    public void setValue(ArrayList<String> value) {
        this.value = value;
    }

}
