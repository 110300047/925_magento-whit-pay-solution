package com.webkul.mobikul.model.compare;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

public class CompareListData extends BaseModel {

    @SerializedName("productList")
    @Expose
    private ArrayList<ProductData> productList = null;
    @SerializedName("attributeValueList")
    @Expose
    private ArrayList<AttributeValueList> attributeValueList = null;

    public ArrayList<ProductData> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<ProductData> productList) {
        this.productList = productList;
    }

    public ArrayList<AttributeValueList> getAttributeValueList() {
        return attributeValueList;
    }

    public void setAttributeValueList(ArrayList<AttributeValueList> attributeValueList) {
        this.attributeValueList = attributeValueList;
    }
}