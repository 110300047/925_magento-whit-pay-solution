package com.webkul.mobikul.model.customer;

/**
 * Created by vedesh.kumar on 23/1/17. @Webkul Software Private limited
 */

public class NavDrawerEndData {

    private String customerName;
    private String customerEmail;
    private String customerProfileImage;
    private String customerBannerImage;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerProfileImage() {
        return customerProfileImage;
    }

    public void setCustomerProfileImage(String customerProfileImage) {
        this.customerProfileImage = customerProfileImage;
    }

    public String getCustomerBannerImage() {
        return customerBannerImage;
    }

    public void setCustomerBannerImage(String customerBannerImage) {
        this.customerBannerImage = customerBannerImage;
    }
}
