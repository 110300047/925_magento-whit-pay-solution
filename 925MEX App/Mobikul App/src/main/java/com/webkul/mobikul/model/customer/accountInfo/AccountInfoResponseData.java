package com.webkul.mobikul.model.customer.accountInfo;

import android.databinding.Bindable;
import android.graphics.Color;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.BR;
import com.webkul.mobikul.activity.AccountInfoActivity;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;

/**
 * Created by vedesh.kumar on 21/1/17. @Webkul Software Private limited
 */

public class AccountInfoResponseData extends BaseModel {
    @SerializedName("isPrefixVisible")
    @Expose
    private boolean isPrefixVisible;
    @SerializedName("isPrefixRequired")
    @Expose
    private boolean isPrefixRequired;
    @SerializedName("prefixHasOptions")
    @Expose
    private boolean prefixHasOptions;
    @SerializedName("prefixOptions")
    @Expose
    private List<String> prefixOptions = null;
    @SerializedName("isMiddlenameVisible")
    @Expose
    private boolean isMiddlenameVisible;
    @SerializedName("isSuffixVisible")
    @Expose
    private boolean isSuffixVisible;
    @SerializedName("isSuffixRequired")
    @Expose
    private boolean isSuffixRequired;
    @SerializedName("suffixHasOptions")
    @Expose
    private boolean suffixHasOptions;
    @SerializedName("suffixOptions")
    @Expose
    private List<String> suffixOptions = null;
    @SerializedName("isDOBVisible")
    @Expose
    private boolean isDOBVisible;
    @SerializedName("isDOBRequired")
    @Expose
    private boolean isDOBRequired;
    @SerializedName("isTaxVisible")
    @Expose
    private boolean isTaxVisible;
    @SerializedName("isTaxRequired")
    @Expose
    private boolean isTaxRequired;
    @SerializedName("isGenderVisible")
    @Expose
    private boolean isGenderVisible;
    @SerializedName("isGenderRequired")
    @Expose
    private boolean isGenderRequired;
    @SerializedName("prefixValue")
    @Expose
    private String prefixValue;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("suffixValue")
    @Expose
    private String suffixValue;
    @SerializedName("DOBValue")
    @Expose
    private String dOBValue;
    @SerializedName("taxValue")
    @Expose
    private String taxValue;
    @SerializedName("genderValue")
    @Expose
    private int genderValue;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("dateFormat")
    @Expose
    private String dateFormat;

    @SerializedName("isMobileVisible")
    @Expose
    private boolean isMobileVisible;
    @SerializedName("isMobileRequired")
    @Expose
    private boolean isMobileRequired;
    @SerializedName("mobile")
    @Expose
    private String mobile;


    private boolean isFormValidated;
    private String currentPassword = "";
    private String newPassword = "";
    private String confirmNewPassword = "";
    private boolean isChangesEmailChecked = false;
    private boolean isChangesPasswordChecked = false;
    private int prefixSelectedItemPosition = 0;
    private int suffixSelectedItemPosition = 0;

    public boolean getIsPrefixVisible() {
        return isPrefixVisible;
    }

    public void setIsPrefixVisible(boolean isPrefixVisible) {
        this.isPrefixVisible = isPrefixVisible;
    }

    public boolean getIsPrefixRequired() {
        return isPrefixRequired;
    }

    public void setIsPrefixRequired(boolean isPrefixRequired) {
        this.isPrefixRequired = isPrefixRequired;
    }

    public boolean isPrefixHasOptions() {
        return prefixHasOptions;
    }

    public void setPrefixHasOptions(boolean prefixHasOptions) {
        this.prefixHasOptions = prefixHasOptions;
    }

    public List<String> getPrefixOptions() {
        return prefixOptions;
    }

    public void setPrefixOptions(List<String> prefixOptions) {
        this.prefixOptions = prefixOptions;
    }

    public boolean getIsMiddlenameVisible() {
        return isMiddlenameVisible;
    }

    public void setIsMiddlenameVisible(boolean isMiddlenameVisible) {
        this.isMiddlenameVisible = isMiddlenameVisible;
    }

    public boolean getIsSuffixVisible() {
        return isSuffixVisible;
    }

    public void setIsSuffixVisible(boolean isSuffixVisible) {
        this.isSuffixVisible = isSuffixVisible;
    }

    public boolean getIsSuffixRequired() {
        return isSuffixRequired;
    }

    public void setIsSuffixRequired(boolean isSuffixRequired) {
        this.isSuffixRequired = isSuffixRequired;
    }

    public boolean isSuffixHasOptions() {
        return suffixHasOptions;
    }

    public void setSuffixHasOptions(boolean suffixHasOptions) {
        this.suffixHasOptions = suffixHasOptions;
    }

    public List<String> getSuffixOptions() {
        return suffixOptions;
    }

    public void setSuffixOptions(List<String> suffixOptions) {
        this.suffixOptions = suffixOptions;
    }

    public boolean getIsDOBVisible() {
        return isDOBVisible;
    }

    public void setIsDOBVisible(boolean isDOBVisible) {
        this.isDOBVisible = isDOBVisible;
    }

    public boolean getIsDOBRequired() {
        return isDOBRequired;
    }

    public void setIsDOBRequired(boolean isDOBRequired) {
        this.isDOBRequired = isDOBRequired;
    }

    public boolean getIsTaxVisible() {
        return isTaxVisible;
    }

    public void setIsTaxVisible(boolean isTaxVisible) {
        this.isTaxVisible = isTaxVisible;
    }

    public boolean getIsTaxRequired() {
        return isTaxRequired;
    }

    public void setIsTaxRequired(boolean isTaxRequired) {
        this.isTaxRequired = isTaxRequired;
    }

    public boolean getIsGenderVisible() {
        return isGenderVisible;
    }

    public void setIsGenderVisible(boolean isGenderVisible) {
        this.isGenderVisible = isGenderVisible;
    }

    public boolean getIsGenderRequired() {
        return isGenderRequired;
    }

    public void setIsGenderRequired(boolean isGenderRequired) {
        this.isGenderRequired = isGenderRequired;
    }

    public String getPrefixValue() {
        return prefixValue;
    }

    public void setPrefixValue(String prefixValue) {
        this.prefixValue = prefixValue;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSuffixValue() {
        return suffixValue;
    }

    public void setSuffixValue(String suffixValue) {
        this.suffixValue = suffixValue;
    }

    public String getDOBValue() {
        return dOBValue;
    }

    public void setDOBValue(String dOBValue) {
        this.dOBValue = dOBValue;
    }

    public String getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(String taxValue) {
        this.taxValue = taxValue;
    }

    public int getGenderValue() {
        return genderValue;
    }

    public void setGenderValue(String genderValue) {
        if (genderValue.equals("Male"))
            this.genderValue = 1;
        else if (genderValue.equals("Female"))
            this.genderValue = 2;
        else
            this.genderValue = 0;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public boolean isFormValidated(AccountInfoActivity accountInfoActivity) {
        isFormValidated = true;

        if (isChangesPasswordChecked) {
            if (!newPassword.equals(confirmNewPassword)) {
                accountInfoActivity.mBinding.confirmpasswordEt.requestFocus();
                accountInfoActivity.mBinding.confirmpasswordEt.setError("Password and confirm password must match");
                isFormValidated = false;
            } else if (confirmNewPassword.matches("")) {
                accountInfoActivity.mBinding.confirmpasswordEt.requestFocus();
                accountInfoActivity.mBinding.confirmpasswordEt.setError("Please fill confirm password !!!");
                isFormValidated = false;
            } else
                accountInfoActivity.mBinding.confirmpasswordEt.setError(null);

            if (newPassword.matches("")) {
                accountInfoActivity.mBinding.newPasswordEt.requestFocus();
                accountInfoActivity.mBinding.newPasswordEt.setError("Please fill New password !!!");
                isFormValidated = false;
            } else
                accountInfoActivity.mBinding.newPasswordEt.setError(null);
        }

        if (isChangesPasswordChecked || isChangesEmailChecked) {
            if (currentPassword.matches("")) {
                accountInfoActivity.mBinding.currentPasswordEt.requestFocus();
                accountInfoActivity.mBinding.currentPasswordEt.setError("Please fill password !!!");
                isFormValidated = false;
            } else
                accountInfoActivity.mBinding.currentPasswordEt.setError(null);
        }

        if (isChangesEmailChecked) {
            if (email.matches("")) {
                accountInfoActivity.mBinding.emailEt.requestFocus();
                accountInfoActivity.mBinding.emailEt.setError("Please fill email address !!!");
                isFormValidated = false;
            } else if (!EMAIL_PATTERN.matcher(email).matches()) {
                accountInfoActivity.mBinding.emailEt.requestFocus();
                accountInfoActivity.mBinding.emailEt.setError("Enter a valid email address");
                isFormValidated = false;
            }
        }

//        if (isIsMobileVisible() && isIsMobileRequired()) {
//            if (mobile.matches("")) {
//                accountInfoActivity.mBinding.mobileEt.requestFocus();
//                accountInfoActivity.mBinding.mobileEt.setError("Please fill Mobile Number!!!");
//                isFormValidated = false;
//            } else if (mobile.length() != ApplicationConstant.NUMBER_OF_MOBILE_NUMBER_DIGIT) {
//                accountInfoActivity.mBinding.mobileEt.requestFocus();
//                accountInfoActivity.mBinding.mobileEt.setError(accountInfoActivity.getResources().getString(R.string.enter_valid_mobile));
//                isFormValidated = false;
//            } else {
//                accountInfoActivity.mBinding.mobileEt.setError(null);
//            }
//        }

        if (getIsGenderVisible() && getIsGenderRequired()) {
            if (genderValue == 0) {
                setSpinnerError(accountInfoActivity.mBinding.genderSpinner, "Please select gender");
                isFormValidated = false;
            }
        }

        if (getIsTaxVisible() && getIsTaxRequired()) {
            if (taxValue.matches("")) {
                accountInfoActivity.mBinding.taxvatEt.requestFocus();
                accountInfoActivity.mBinding.taxvatEt.setError("Please fill TAX/VAT !!!");
                isFormValidated = false;
            } else
                accountInfoActivity.mBinding.taxvatEt.setError(null);
        }

        if (getIsDOBVisible() && getIsDOBRequired()) {
            if (dOBValue.matches("")) {
                accountInfoActivity.mBinding.dobEt.requestFocus();
                accountInfoActivity.mBinding.dobEt.setError("Please fill date of birth !!!");
                isFormValidated = false;
            } else
                accountInfoActivity.mBinding.dobEt.setError(null);
        }

        if (getIsSuffixVisible() && getIsSuffixRequired()) {
            if (suffixHasOptions) {
                if (suffixValue.matches("Please choose ...")) {
                    setSpinnerError(accountInfoActivity.mBinding.suffixSpinner, "Please select suffix");
                    isFormValidated = false;
                }
            } else {
                if (suffixValue.matches("")) {
                    accountInfoActivity.mBinding.suffixEt.requestFocus();
                    accountInfoActivity.mBinding.suffixEt.setError("Please fill suffix !!!");
                    isFormValidated = false;
                } else
                    accountInfoActivity.mBinding.suffixEt.setError(null);
            }
        }

        if (lastName.matches("")) {
            accountInfoActivity.mBinding.lastnameEt.requestFocus();
            accountInfoActivity.mBinding.lastnameEt.setError("Please fill last Name !!!");
            isFormValidated = false;
        } else
            accountInfoActivity.mBinding.lastnameEt.setError(null);

        if (firstName.matches("")) {
            accountInfoActivity.mBinding.firstnameEt.requestFocus();
            accountInfoActivity.mBinding.firstnameEt.setError("Please fill first Name !!!");
            isFormValidated = false;
        } else
            accountInfoActivity.mBinding.firstnameEt.setError(null);

        if (getIsPrefixVisible() && getIsPrefixRequired()) {
            if (prefixHasOptions) {
                if (prefixValue.matches("Please choose ...")) {
                    setSpinnerError(accountInfoActivity.mBinding.prefixSpinner, "Please select prefix");
                    isFormValidated = false;
                }
            } else {
                if (prefixValue.matches("")) {
                    accountInfoActivity.mBinding.prefixEt.requestFocus();
                    accountInfoActivity.mBinding.prefixEt.setError("Please fill prefix !!!");
                    isFormValidated = false;
                } else
                    accountInfoActivity.mBinding.prefixEt.setError(null);
            }
        }
        return isFormValidated;
    }

    public void setFormValidated(boolean formValidated) {
        isFormValidated = formValidated;
    }

    private void setSpinnerError(Spinner spinner, String errorString) {
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setTextColor(Color.RED);
            selectedTextView.setText(errorString);
            spinner.performClick();
        }
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmPassword) {
        this.confirmNewPassword = confirmPassword;
    }

    public void initPrefixSelectedItemPosition() {
        for (int noOfPrefix = 0; noOfPrefix < getPrefixOptions().size(); noOfPrefix++) {
            if (getPrefixOptions().get(noOfPrefix).trim().equals(prefixValue)) {
                if (getIsPrefixRequired())
                    prefixSelectedItemPosition = noOfPrefix;
                else
                    prefixSelectedItemPosition = noOfPrefix + 1;
            }
        }
    }

    public int getPrefixSelectedItemPosition() {
        return prefixSelectedItemPosition;
    }

    public void initSuffixSelectedItemPosition() {
        for (int noOfSuffix = 0; noOfSuffix < getSuffixOptions().size(); noOfSuffix++) {
            if (getSuffixOptions().get(noOfSuffix).trim().equals(suffixValue)) {
                if (getIsSuffixRequired())
                    suffixSelectedItemPosition = noOfSuffix;
                else
                    suffixSelectedItemPosition = noOfSuffix + 1;
            }
        }
    }

    public int getSuffixSelectedItemPosition() {
        return suffixSelectedItemPosition;
    }

    public boolean isIsMobileVisible() {
        return isMobileVisible;
    }

    public void setMobileVisible(boolean mobileVisible) {
        isMobileVisible = mobileVisible;
    }

    public boolean isIsMobileRequired() {
        return isMobileRequired;
    }

    public void setMobileRequired(boolean mobileRequired) {
        isMobileRequired = mobileRequired;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Bindable
    public boolean isChangesPasswordChecked() {
        return isChangesPasswordChecked;
    }

    public void setChangesPasswordChecked(boolean changesPasswordChecked) {
        isChangesPasswordChecked = changesPasswordChecked;
        notifyPropertyChanged(BR.changesPasswordChecked);
    }

    @Bindable
    public boolean isChangesEmailChecked() {
        return isChangesEmailChecked;
    }

    public void setChangesEmailChecked(boolean changesEmailChecked) {
        isChangesEmailChecked = changesEmailChecked;
        notifyPropertyChanged(BR.changesEmailChecked);
    }
}
