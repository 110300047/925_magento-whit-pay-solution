package com.webkul.mobikul.model.customer.accountInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 21/1/17. @Webkul Software Private limited
 */

public class SaveAccountInfoResponseData extends BaseModel {

    @SerializedName("customerName")
    @Expose
    private String customerName;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
