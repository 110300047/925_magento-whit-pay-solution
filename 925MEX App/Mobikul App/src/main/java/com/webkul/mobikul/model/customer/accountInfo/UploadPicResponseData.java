package com.webkul.mobikul.model.customer.accountInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 12/1/17. @Webkul Software Pvt. Ltd
 */

public class UploadPicResponseData extends BaseModel {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("error")
    @Expose
    private int error;
    @SerializedName("size")
    @Expose
    private int size;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("url")
    @Expose
    private String url;

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getError() {
        return error;
    }

    public int getSize() {
        return size;
    }

    public String getFile() {
        return file;
    }
}
