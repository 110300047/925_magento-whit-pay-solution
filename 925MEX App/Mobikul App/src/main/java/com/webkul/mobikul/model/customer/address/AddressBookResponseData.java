package com.webkul.mobikul.model.customer.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */

public class AddressBookResponseData extends BaseModel {

    @SerializedName("billingAddress")
    @Expose
    private BillingShippingAddress billingAddress;
    @SerializedName("shippingAddress")
    @Expose
    private BillingShippingAddress shippingAddress;
    @SerializedName("additionalAddress")
    @Expose
    private List<BillingShippingAddress> additionalAddress = null;

    public BillingShippingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingShippingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public BillingShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(BillingShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public List<BillingShippingAddress> getAdditionalAddress() {
        return additionalAddress;
    }

    public void setAdditionalAddress(List<BillingShippingAddress> additionalAddress) {
        this.additionalAddress = additionalAddress;
    }
}
