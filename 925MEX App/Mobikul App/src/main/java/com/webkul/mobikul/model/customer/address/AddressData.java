package com.webkul.mobikul.model.customer.address;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.activity.NewAddressActivity;
import com.webkul.mobikul.adapter.NewAddressStreetAddressRvAdapter;

import java.util.ArrayList;
import java.util.List;

public class AddressData {

    @SerializedName("isDefaultBilling")
    @Expose
    private boolean isDefaultBilling;
    @SerializedName("isDefaultShipping")
    @Expose
    private boolean isDefaultShipping;
    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("entity_type_id")
    @Expose
    private String entityTypeId;
    @SerializedName("attribute_set_id")
    @Expose
    private String attributeSetId;
    @SerializedName("increment_id")
    @Expose
    private String incrementId;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("prefix")
    @Expose
    private String prefix = "";
    @SerializedName("firstname")
    @Expose
    private String firstname = "";
    @SerializedName("middlename")
    @Expose
    private String middlename = "";
    @SerializedName("lastname")
    @Expose
    private String lastname = "";
    @SerializedName("suffix")
    @Expose
    private String suffix = "";
    @SerializedName("company")
    @Expose
    private String company = "";
    @SerializedName("city")
    @Expose
    private String city = "";
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("region")
    @Expose
    private String region = "";
    @SerializedName("postcode")
    @Expose
    private String postcode = "";
    @SerializedName("telephone")
    @Expose
    private String telephone = "";
    @SerializedName("fax")
    @Expose
    private String fax = "";
    @SerializedName("region_id")
    @Expose
    private String regionId;
    @SerializedName("street")
    @Expose
    private List<String> street = new ArrayList<>();
    private boolean streetAddressValidated;

    public boolean isIsDefaultBilling() {
        return isDefaultBilling;
    }

    public void setIsDefaultBilling(boolean isDefaultBilling) {
        this.isDefaultBilling = isDefaultBilling;
    }

    public boolean isIsDefaultShipping() {
        return isDefaultShipping;
    }

    public void setIsDefaultShipping(boolean isDefaultShipping) {
        this.isDefaultShipping = isDefaultShipping;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityTypeId() {
        return entityTypeId;
    }

    public void setEntityTypeId(String entityTypeId) {
        this.entityTypeId = entityTypeId;
    }

    public String getAttributeSetId() {
        return attributeSetId;
    }

    public void setAttributeSetId(String attributeSetId) {
        this.attributeSetId = attributeSetId;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public List<String> getStreet() {
        return street;
    }

    public void setStreet(List<String> street) {
        this.street = street;
    }

    private void updateStreetAddressValidationStatus(Context context) {
        streetAddressValidated = ((NewAddressStreetAddressRvAdapter) ((NewAddressActivity) context).getBinding().streetAddressRv.getAdapter())
                .isStreetAddressValidated();
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }
}