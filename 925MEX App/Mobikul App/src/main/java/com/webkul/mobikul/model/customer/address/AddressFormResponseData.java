package com.webkul.mobikul.model.customer.address;

import android.content.Context;
import android.databinding.Bindable;
import android.view.View;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.BR;
import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.NewAddressActivity;
import com.webkul.mobikul.adapter.NewAddressStreetAddressRvAdapter;
import com.webkul.mobikul.model.BaseModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Pvt. Ltd
 */

public class AddressFormResponseData extends BaseModel {

    @SerializedName("streetLineCount")
    @Expose
    private int streetLineCount;
    @SerializedName("isPrefixVisible")
    @Expose
    private boolean isPrefixVisible;
    @SerializedName("isPrefixRequired")
    @Expose
    private boolean isPrefixRequired;
    @SerializedName("prefixHasOptions")
    @Expose
    private boolean prefixHasOptions;
    @SerializedName("prefixOptions")
    @Expose
    private List<String> prefixOptions = null;
    @SerializedName("isMiddlenameVisible")
    @Expose
    private boolean isMiddlenameVisible;
    @SerializedName("isSuffixVisible")
    @Expose
    private boolean isSuffixVisible;
    @SerializedName("isSuffixRequired")
    @Expose
    private boolean isSuffixRequired;
    @SerializedName("suffixHasOptions")
    @Expose
    private boolean suffixHasOptions;
    @SerializedName("suffixOptions")
    @Expose
    private List<String> suffixOptions = null;
    @SerializedName("prefixValue")
    @Expose
    private String prefixValue;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("suffixValue")
    @Expose
    private String suffixValue;
    @SerializedName("addressData")
    @Expose
    private AddressData addressData = new AddressData();
    @SerializedName("countryData")
    @Expose
    private List<CountryData> countryData = null;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;

    private boolean hasStateData;
    private List<String> countryNameList;
    private int selectedCountryPosition;
    private int selectedStatePosition;

    private int prefixSelectedItemPosition = 0;
    private int suffixSelectedItemPosition = 0;

    private boolean isFormValidated = false;

    public int getStreetLineCount() {
        return streetLineCount;
    }

    public void setStreetLineCount(int streetLineCount) {
        this.streetLineCount = streetLineCount;
    }

    public boolean getIsPrefixVisible() {
        return isPrefixVisible;
    }

    public void setIsPrefixVisible(boolean isPrefixVisible) {
        this.isPrefixVisible = isPrefixVisible;
    }

    public boolean getIsPrefixRequired() {
        return isPrefixRequired;
    }

    public void setIsPrefixRequired(boolean isPrefixRequired) {
        this.isPrefixRequired = isPrefixRequired;
    }

    public boolean isPrefixHasOptions() {
        return prefixHasOptions;
    }

    public void setPrefixHasOptions(boolean prefixHasOptions) {
        this.prefixHasOptions = prefixHasOptions;
    }

    public List<String> getPrefixOptions() {
        return prefixOptions;
    }

    public void setPrefixOptions(List<String> prefixOptions) {
        this.prefixOptions = prefixOptions;
    }

    public boolean getIsMiddlenameVisible() {
        return isMiddlenameVisible;
    }

    public void setIsMiddlenameVisible(boolean isMiddlenameVisible) {
        this.isMiddlenameVisible = isMiddlenameVisible;
    }

    public boolean getIsSuffixVisible() {
        return isSuffixVisible;
    }

    public void setIsSuffixVisible(boolean isSuffixVisible) {
        this.isSuffixVisible = isSuffixVisible;
    }

    public boolean getIsSuffixRequired() {
        return isSuffixRequired;
    }

    public void setIsSuffixRequired(boolean isSuffixRequired) {
        this.isSuffixRequired = isSuffixRequired;
    }

    public boolean isSuffixHasOptions() {
        return suffixHasOptions;
    }

    public void setSuffixHasOptions(boolean suffixHasOptions) {
        this.suffixHasOptions = suffixHasOptions;
    }

    public List<String> getSuffixOptions() {
        return suffixOptions;
    }

    public void setSuffixOptions(List<String> suffixOptions) {
        this.suffixOptions = suffixOptions;
    }

    public String getPrefixValue() {
        return prefixValue;
    }

    public void setPrefixValue(String prefixValue) {
        this.prefixValue = prefixValue;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSuffixValue() {
        return suffixValue;
    }

    public void setSuffixValue(String suffixValue) {
        this.suffixValue = suffixValue;
    }

    public AddressData getAddressData() {
        return addressData;
    }

    public void setAddressData(AddressData addressData) {
        this.addressData = addressData;
    }

    public List<CountryData> getCountryData() {
        return countryData;
    }

    public void setCountryData(List<CountryData> countryData) {
        this.countryData = countryData;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @Bindable
    public boolean isHasStateData() {
        return hasStateData;
    }

    public void setHasStateData(boolean hasStateData) {
        this.hasStateData = hasStateData;
        notifyPropertyChanged(BR.hasStateData);
    }

    public void initPrefixSelectedItemPosition() {
        for (int noOfPrefix = 0; noOfPrefix < getPrefixOptions().size(); noOfPrefix++) {
            if (getPrefixOptions().get(noOfPrefix).trim().equals(prefixValue)) {
                if (getIsPrefixRequired())
                    prefixSelectedItemPosition = noOfPrefix;
                else
                    prefixSelectedItemPosition = noOfPrefix + 1;
            }
        }
    }

    public int getPrefixSelectedItemPosition() {
        return prefixSelectedItemPosition;
    }

    public void initSuffixSelectedItemPosition() {
        for (int noOfSuffix = 0; noOfSuffix < getSuffixOptions().size(); noOfSuffix++) {
            if (getSuffixOptions().get(noOfSuffix).trim().equals(suffixValue)) {
                if (getIsSuffixRequired())
                    suffixSelectedItemPosition = noOfSuffix;
                else
                    suffixSelectedItemPosition = noOfSuffix + 1;
            }
        }
    }

    public int getSuffixSelectedItemPosition() {
        return suffixSelectedItemPosition;
    }

    public void initCountryNameList() {
        List<String> countryNameList = new ArrayList<>();
        for (CountryData eachCountryData : countryData) {
            countryNameList.add(eachCountryData.getName());
        }
        setCountryNameList(countryNameList);
    }

    @Bindable
    public int getSelectedCountryPosition() {
        return selectedCountryPosition;
    }

    public void setSelectedCountryPosition(int selectedCountryPosition) {
        this.selectedCountryPosition = selectedCountryPosition;
        notifyPropertyChanged(BR.selectedCountryPosition);
    }

    public int getSelectedCountryPositionFromAddressData() {
        for (int countryPosition = 0; countryPosition < countryData.size(); countryPosition++) {
            if (countryData.get(countryPosition).getCountryId().equals(addressData.getCountryId())) {
                return countryPosition;
            }
        }
        return 0;
    }

    public List<String> getCountryNameList() {
        return countryNameList;
    }

    public void setCountryNameList(List<String> countryNameList) {
        this.countryNameList = countryNameList;
    }

    public List<String> getStateNameList(int countryPosition) {
        List<String> stateNameList = new ArrayList<>();
        for (State eachStateData : countryData.get(countryPosition).getStates()) {
            stateNameList.add(eachStateData.getName());
        }
        return stateNameList;
    }

    public int getSelectedStatePositionFromAddressData() {

        for (int countryPosition = 0; countryPosition < countryData.size(); countryPosition++) {
            if (countryData.get(countryPosition).getCountryId().equals(addressData.getCountryId())) {
                if (countryData.get(countryPosition).getStates() != null && countryData.get(countryPosition).getStates().size() > 0) {
                    for (int statePosition = 0; statePosition < countryData.get(countryPosition).getStates().size(); statePosition++) {
                        if (countryData.get(countryPosition).getStates().get(statePosition).getRegionId() == Integer.parseInt(addressData.getRegionId())) {
                            return statePosition;
                        }
                    }
                } else {
                    return 0;
                }
            }
        }
        return 0;
    }

    public void setSelectedStatePosition(int selectedStatePosition) {
        this.selectedStatePosition = selectedStatePosition;
    }

    public JSONObject getNewAddressData(Context context) {
        try {
            JSONObject newAddressJsonObj = new JSONObject();
            newAddressJsonObj.put("firstname", addressData.getFirstname());
            newAddressJsonObj.put("lastname", addressData.getLastname());
            newAddressJsonObj.put("company", addressData.getCompany());
            newAddressJsonObj.put("telephone", addressData.getTelephone());
            newAddressJsonObj.put("fax", addressData.getFax());
            JSONArray streetJsonArr = new JSONArray();
            for (int noOfSteet = 0; noOfSteet < getStreetLineCount(); noOfSteet++) {
                streetJsonArr.put(((NewAddressStreetAddressRvAdapter) ((NewAddressActivity) context).getBinding().streetAddressRv.getAdapter())
                        .getItem(noOfSteet));
            }
            newAddressJsonObj.put("street", streetJsonArr);
            newAddressJsonObj.put("city", addressData.getCity());
            newAddressJsonObj.put("region_id", (hasStateData) ? getCountryData().get(getSelectedCountryPosition()).getStates().get(selectedStatePosition)
                    .getRegionId() : 0);
            newAddressJsonObj.put("region", (hasStateData) ? "" : addressData.getRegion());
            newAddressJsonObj.put("postcode", addressData.getPostcode());
            newAddressJsonObj.put("country_id", getCountryData().get(getSelectedCountryPosition()).getCountryId());
            newAddressJsonObj.put("default_billing", ((NewAddressActivity) context).getBinding().defaultBillingCb.isChecked() ? "1" : "0");
            newAddressJsonObj.put("default_shipping", ((NewAddressActivity) context).getBinding().defaultShippingCb.isChecked() ? "1" : "0");

            newAddressJsonObj.put("prefix", getPrefixValue());
            newAddressJsonObj.put("middlename", getMiddleName());
            newAddressJsonObj.put("suffix", getSuffixValue());

            return newAddressJsonObj;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isFormValidated(NewAddressActivity mContext) {
        isFormValidated = true;
        if (addressData.getPostcode().trim().matches("")) {
            mContext.getBinding().zipEt.requestFocus();
            mContext.getBinding().zipError.setVisibility(View.VISIBLE);
            mContext.getBinding().zipError.setText(mContext.getString(R.string.fill_postcode));
            isFormValidated = false;
        } else {
            mContext.getBinding().zipError.setVisibility(View.GONE);
            mContext.getBinding().zipError.setText(null);
        }

        if (mContext.getBinding().stateEt.getVisibility() == View.VISIBLE) {
            if (addressData.getRegion().trim().matches("")) {
                mContext.getBinding().stateEt.requestFocus();
                mContext.getBinding().stateError.setVisibility(View.VISIBLE);
                mContext.getBinding().stateError.setText(mContext.getString(R.string.fill_state));
                isFormValidated = false;
            } else {
                mContext.getBinding().stateError.setVisibility(View.GONE);
                mContext.getBinding().stateError.setText(null);
            }
        }

        if (addressData.getCity().trim().matches("")) {
            mContext.getBinding().cityEt.requestFocus();
            mContext.getBinding().cityError.setVisibility(View.VISIBLE);
            mContext.getBinding().cityError.setText(mContext.getString(R.string.fill_city));
            isFormValidated = false;
        } else {
            mContext.getBinding().cityError.setVisibility(View.GONE);
            mContext.getBinding().cityError.setText(null);
        }

        // Checking street address
        isFormValidated = ((NewAddressStreetAddressRvAdapter) mContext.getBinding().streetAddressRv.getAdapter()).isStreetAddressValidated();

        if (addressData.getTelephone().trim().matches("")) {
            mContext.getBinding().telephoneEt.requestFocus();
            mContext.getBinding().telephoneError.setVisibility(View.VISIBLE);
            mContext.getBinding().telephoneError.setText(mContext.getString(R.string.fill_telephone));
            isFormValidated = false;
        } else if (addressData.getTelephone().trim().length() != 10 && addressData.getTelephone().trim().length() != 12 && addressData.getTelephone().trim().length() == 13) {
            mContext.getBinding().telephoneEt.requestFocus();
            mContext.getBinding().telephoneError.setVisibility(View.VISIBLE);
            mContext.getBinding().telephoneError.setText(mContext.getString(R.string.fill_valid_phone));
        } else {
            mContext.getBinding().telephoneError.setVisibility(View.GONE);
            mContext.getBinding().telephoneError.setText(null);
        }

        if (getIsSuffixVisible() && getIsSuffixRequired()) {
            if (suffixHasOptions) {
                if (addressData.getSuffix().matches("")) {
                    // Nothing to do
                }
            } else {
                if (addressData.getSuffix().trim().matches("")) {
                    mContext.getBinding().suffixEt.requestFocus();
                    mContext.getBinding().suffixError.setVisibility(View.VISIBLE);
                    mContext.getBinding().suffixError.setText(mContext.getString(R.string.fill_suffix));
                    isFormValidated = false;
                } else {
                    mContext.getBinding().suffixError.setVisibility(View.GONE);
                    mContext.getBinding().suffixError.setText(null);
                }
            }
        }

        if (addressData.getLastname() == null || addressData.getLastname().trim().matches("")) {
            mContext.getBinding().lastnameEt.requestFocus();
            mContext.getBinding().lastnameError.setVisibility(View.VISIBLE);
            mContext.getBinding().lastnameError.setText(mContext.getString(R.string.fill_last_name));
            isFormValidated = false;
        } else {
            mContext.getBinding().lastnameError.setVisibility(View.GONE);
            mContext.getBinding().lastnameError.setText(null);
        }

        if (addressData.getFirstname() == null || addressData.getFirstname().trim().matches("")) {
            mContext.getBinding().firstnameEt.requestFocus();
            mContext.getBinding().firstnameError.setVisibility(View.VISIBLE);
            mContext.getBinding().firstnameError.setText(mContext.getString(R.string.fill_first_name));
            isFormValidated = false;
        } else {
            mContext.getBinding().firstnameError.setVisibility(View.GONE);
            mContext.getBinding().firstnameError.setText(null);
        }

        if (getIsPrefixVisible() && getIsPrefixRequired()) {
            if (prefixHasOptions) {
                // Nothing to do
            } else {
                if (addressData.getPrefix().trim().matches("")) {
                    mContext.getBinding().prefixEt.requestFocus();
                    mContext.getBinding().prefixError.setVisibility(View.VISIBLE);
                    mContext.getBinding().prefixError.setText(mContext.getString(R.string.fill_prefix));
                    isFormValidated = false;
                } else {
                    mContext.getBinding().prefixError.setVisibility(View.GONE);
                    mContext.getBinding().prefixError.setText(null);
                }
            }
        }
        return isFormValidated;
    }
}