package com.webkul.mobikul.model.customer.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */

public class BillingShippingAddress {

    @SerializedName("value")
    @Expose
    private String value = "";
    @SerializedName("id")
    @Expose
    private int id;

    public String getValue() {
        return value.replace("<br>", "\n");
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
