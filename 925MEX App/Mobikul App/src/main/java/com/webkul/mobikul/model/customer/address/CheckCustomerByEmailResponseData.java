package com.webkul.mobikul.model.customer.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 24/1/18. @Webkul Software Private limited
 */

public class CheckCustomerByEmailResponseData extends BaseModel {

    @SerializedName("isCustomerExist")
    @Expose
    public boolean isCustomerExist;

    public boolean isCustomerExist() {
        return isCustomerExist;
    }
}