package com.webkul.mobikul.model.customer.address;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.webkul.mobikul.BR;

/**
 * Created by vedesh.kumar on 6/1/17. @Webkul Software Pvt. Ltd
 */

public class StreetAddressData extends BaseObservable {

    private String streetValue;

    private boolean streetValidated = true;

    public StreetAddressData(String streetValue) {
        this.streetValue = streetValue;
    }

    public String getStreetValue() {
        return streetValue;
    }

    public void setStreetValue(String streetValue) {
        this.streetValue = streetValue;
    }

    @Bindable
    public boolean isStreetValidated() {
        return streetValidated;
    }

    public void setStreetValidated(boolean streetValidated) {
        this.streetValidated = streetValidated;
        notifyPropertyChanged(BR.streetValidated);
    }
}