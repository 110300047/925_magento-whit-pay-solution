package com.webkul.mobikul.model.customer.downloadable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 28/1/17. @Webkul Software Private limited
 */

public class DownloadProductResponse extends BaseModel {
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("fileName")
    @Expose
    private String fileName;
    @SerializedName("mimeType")
    @Expose
    private String mimeType;

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
