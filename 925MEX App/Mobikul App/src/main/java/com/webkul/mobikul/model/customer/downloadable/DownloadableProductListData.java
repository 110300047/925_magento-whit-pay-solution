package com.webkul.mobikul.model.customer.downloadable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

public class DownloadableProductListData extends BaseModel {

    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("downloadsList")
    @Expose
    private List<DownloadsList> downloadsList = null;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<DownloadsList> getDownloadsList() {
        return downloadsList;
    }

    public void setDownloadsList(List<DownloadsList> downloadsList) {
        this.downloadsList = downloadsList;
    }
}
