package com.webkul.mobikul.model.customer.downloadable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownloadsList {

    @SerializedName("incrementId")
    @Expose
    private String incrementId;
    @SerializedName("isOrderExist")
    @Expose
    private boolean isOrderExist;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("hash")
    @Expose
    private String hash;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("proName")
    @Expose
    private String proName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("remainingDownloads")
    @Expose
    private String remainingDownloads;
    @SerializedName("canReorder")
    @Expose
    private boolean canReorder;


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public boolean getIsOrderExist() {
        return isOrderExist;
    }

    public void setIsOrderExist(boolean isOrderExist) {
        this.isOrderExist = isOrderExist;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemainingDownloads() {
        return remainingDownloads;
    }

    public void setRemainingDownloads(String remainingDownloads) {
        this.remainingDownloads = remainingDownloads;
    }

    public boolean isCanReorder() {
        return canReorder;
    }

    public void setCanReorder(boolean canReorder) {
        this.canReorder = canReorder;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
