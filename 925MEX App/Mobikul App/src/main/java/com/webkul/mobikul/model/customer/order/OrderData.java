package com.webkul.mobikul.model.customer.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 19/7/17. @Webkul Software Private limited
 */

public class OrderData {

    @SerializedName("itemList")
    @Expose
    private List<OrderItem> items = null;
    @SerializedName("totals")
    @Expose
    private List<TotalItem> totals;

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public List<TotalItem> getTotals() {
        return totals;
    }

    public void setTotals(List<TotalItem> totals) {
        this.totals = totals;
    }
}