package com.webkul.mobikul.model.customer.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class OrderDetailResponseData extends BaseModel {

    @SerializedName("incrementId")
    @Expose
    private String incrementId;
    @SerializedName("statusLabel")
    @Expose
    private String statusLabel = "";
    @SerializedName("orderDate")
    @Expose
    private String orderDate = "";
    @SerializedName("shippingAddress")
    @Expose
    private String shippingAddress = "";
    @SerializedName("shippingMethod")
    @Expose
    private String shippingMethod = "";
    @SerializedName("billingAddress")
    @Expose
    private String billingAddress = "";
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("orderData")
    @Expose
    private OrderData orderData;
    @SerializedName("canReorder")
    private boolean canReorder;
    @SerializedName("state")
    private String state;

    public String getState() {
        return state;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getOrderDate() {
        if (orderDate == null || orderDate.equals("null"))
            return "";
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public boolean isCanReorder() {
        return canReorder;
    }

    public void setCanReorder(boolean canReorder) {
        this.canReorder = canReorder;
    }

    public OrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderData orderData) {
        this.orderData = orderData;
    }
}
