package com.webkul.mobikul.model.customer.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class OrderList {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("ship_to")
    @Expose
    private String shipTo;
    @SerializedName("order_total")
    @Expose
    private String orderTotal;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("canReorder")
    @Expose
    private Boolean canReorder;
    @SerializedName("state")
    @Expose
    private String state;

    public String getState() {
        return state;
    }

    public int getId() {
        return id;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getDate() {
        return date;
    }

    public String getShipTo() {
        return shipTo;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public String getStatus() {
        return status;
    }

    public Boolean getCanReorder() {
        return canReorder;
    }

}
