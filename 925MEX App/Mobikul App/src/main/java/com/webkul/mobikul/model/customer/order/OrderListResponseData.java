package com.webkul.mobikul.model.customer.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */


public class OrderListResponseData extends BaseModel {

    @SerializedName("orderList")
    @Expose
    private List<OrderList> orderList = null;

    @SerializedName("totalCount")
    @Expose
    private int totalCount;

    public List<OrderList> getOrderList() {
        return orderList;
    }

    public int getTotalCount() {
        return totalCount;
    }
}