package com.webkul.mobikul.model.customer.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.product.ProductRatingData;

import java.util.List;


/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class ReviewDetailsResponseData extends BaseModel {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("ratingData")
    @Expose
    private List<ProductRatingData> ratingData = null;
    @SerializedName("reviewDate")
    @Expose
    private String reviewDate;
    @SerializedName("reviewDetail")
    @Expose
    private String reviewDetail;
    @SerializedName("productId")
    @Expose
    private int productId;

    @SerializedName("rating")
    @Expose
    private float rating;

    public float getRating() {
//        if (ratingData != null && ratingData.size() > 0){

//            double sumOfAllRatingData = 0d;
//            for (int noOfRating = 0; noOfRating < ratingData.size(); noOfRating++) {
//                sumOfAllRatingData += ratingData.get(noOfRating).getRatingValue();
//            }
//
//        /*divide by 20 as the value are in multiple of 20*/
//            return (float) (sumOfAllRatingData / ratingData.size()) / 20;
//        }else {
        return rating;
//        }
    }

    public void setRating(float rating) {
        this.rating = rating;
//        String rating1 = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ProductRatingData> getRatingData() {
        return ratingData;
    }

    public void setRatingData(List<ProductRatingData> ratingData) {
        this.ratingData = ratingData;
    }

    public String getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewDetail() {
        return reviewDetail;
    }

    public void setReviewDetail(String reviewDetail) {
        this.reviewDetail = reviewDetail;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
