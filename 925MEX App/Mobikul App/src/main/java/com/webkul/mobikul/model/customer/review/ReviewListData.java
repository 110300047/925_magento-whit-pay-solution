package com.webkul.mobikul.model.customer.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.product.ProductRatingData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class ReviewListData {

    private static final String TAG = ReviewListData.class.getClass().getSimpleName();
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("thumbNail")
    @Expose
    private String thumbNail;
    @SerializedName("typeId")
    @Expose
    private String typeId;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("proName")
    @Expose
    private String proName;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("ratingData")
    @Expose
    private List<ProductRatingData> ratingData = new ArrayList<>();

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<ProductRatingData> getRatingData() {
        return ratingData;
    }


    public void setRatingData(List<ProductRatingData> ratingData) {
        this.ratingData = ratingData;
    }

    public float getRating() {
        double sumOfAllRatingData = 0d;
        for (int noOfRating = 0; noOfRating < ratingData.size(); noOfRating++) {
            sumOfAllRatingData += ratingData.get(noOfRating).getRatingValue();
        }

        float rating = (float) (sumOfAllRatingData / ratingData.size()) / 20;
        /*divide by 20 as the value are in multiple of 20*/
        return rating;
    }

}

