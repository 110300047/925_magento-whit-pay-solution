package com.webkul.mobikul.model.customer.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 2/1/17. @Webkul Software Pvt. Ltd
 */

public class ReviewListResponseData extends BaseModel {

    @SerializedName("reviewList")
    @Expose
    private List<ReviewListData> reviewList = null;
    @SerializedName("totalCount")
    @Expose
    private int totalCount;

    public List<ReviewListData> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<ReviewListData> reviewList) {
        this.reviewList = reviewList;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}