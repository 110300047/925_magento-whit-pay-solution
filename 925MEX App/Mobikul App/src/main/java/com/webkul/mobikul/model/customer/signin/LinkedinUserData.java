package com.webkul.mobikul.model.customer.signin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 17/1/17. @Webkul Software Private limited
 */

public class LinkedinUserData extends BaseModel implements Parcelable {
    public static final Creator<LinkedinUserData> CREATOR = new Creator<LinkedinUserData>() {
        @Override
        public LinkedinUserData createFromParcel(Parcel in) {
            return new LinkedinUserData(in);
        }

        @Override
        public LinkedinUserData[] newArray(int size) {
            return new LinkedinUserData[size];
        }
    };
    @SerializedName("emailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("pictureUrl")
    @Expose
    private String pictureUrl;

    protected LinkedinUserData(Parcel in) {
        emailAddress = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        pictureUrl = in.readString();
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(emailAddress);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(pictureUrl);
    }
}
