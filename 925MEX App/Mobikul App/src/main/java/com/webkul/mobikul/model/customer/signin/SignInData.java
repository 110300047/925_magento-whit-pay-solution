package com.webkul.mobikul.model.customer.signin;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;

import com.webkul.mobikul.BR;
import com.webkul.mobikul.R;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AppSharedPref;

import static android.content.Context.FINGERPRINT_SERVICE;
import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;


/**
 * Created by vedesh.kumar on 29/12/16. @Webkul Software Pvt. Ltd
 */

public class SignInData extends BaseObservable {
    private FingerprintManager fingerprintManager = null;
    private String username = "";
    private String password = "";
    private boolean usernameValidated = false;
    private boolean passwordValidated = false;

    private Context mContext;
    private String usernameError = "";
    private String passwordError = "";
    private boolean canDisplayError = false;

    public SignInData(Context context) {
        mContext = context;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) mContext.getSystemService(FINGERPRINT_SERVICE);
        }
    }

    public boolean isMobileLoginEnabled() {
        return AppSharedPref.getMobileLoginEnabled(mContext);
    }

    @Bindable
    public boolean isPasswordValidated() {
        return passwordValidated;
    }

    public void setPasswordValidated(boolean passwordValidated) {
        this.passwordValidated = passwordValidated;
        notifyPropertyChanged(BR.passwordValidated);
    }

    @Bindable
    public String getPasswordError() {
        return passwordError;
    }

    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
        notifyPropertyChanged(BR.passwordError);
    }

    @Bindable
    public String getUsernameError() {
        return usernameError;
    }

    public void setUsernameError(String usernameError) {
        this.usernameError = (usernameError);
        notifyPropertyChanged(BR.usernameError);
    }

    @Bindable
    public boolean isUsernameValidated() {
        return usernameValidated;
    }

    public void setUsernameValidated(boolean usernameValidated) {
        this.usernameValidated = usernameValidated;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password.matches("")) {
            if (canDisplayError)
                setPasswordError(mContext.getString(R.string.password) + " " + mContext.getResources().getString(R.string.is_require_text));
            passwordValidated = false;
        } else if (password.length() < 6) {
            if (canDisplayError)
                setPasswordError(mContext.getString(R.string.password) + " " + mContext.getResources().getString(R.string.alert_password_length));
            passwordValidated = false;
        } else {
            passwordValidated = true;
            setPasswordError(null);
        }
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        if (username.matches("")) {
            usernameValidated = false;
            if (canDisplayError)
                if (AppSharedPref.getMobileLoginEnabled(mContext)) {
                    setUsernameError(mContext.getString(R.string.email_or_mobile) + " " + mContext.getResources().getString(R.string.is_require_text));
                } else {
                    setUsernameError(mContext.getString(R.string.email_address) + " " + mContext.getResources().getString(R.string.is_require_text));
                }
        } else if (AppSharedPref.getMobileLoginEnabled(mContext) && android.util.Patterns.PHONE.matcher(username).matches() && username.length() != ApplicationConstant.NUMBER_OF_MOBILE_NUMBER_DIGIT) {
            usernameValidated = false;
            if (canDisplayError)
                setUsernameError(mContext.getResources().getString(R.string.enter_valid_mobile));
        } else if (!android.util.Patterns.PHONE.matcher(username).matches() && !EMAIL_PATTERN.matcher(username).matches()) {
            usernameValidated = false;
            if (canDisplayError)
                setUsernameError(mContext.getResources().getString(R.string.enter_valid_email));
        } else {
            usernameValidated = true;
            setUsernameError(null);
        }
        this.username = username;
    }

    public boolean isFingerPrintEnable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                if (fingerprintManager.isHardwareDetected()) {
                    return true;
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean isCanDisplayError() {
        return canDisplayError;
    }

    public void setCanDisplayError(boolean canDisplayError) {
        this.canDisplayError = canDisplayError;
    }
}