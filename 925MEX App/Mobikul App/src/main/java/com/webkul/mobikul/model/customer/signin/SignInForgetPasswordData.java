package com.webkul.mobikul.model.customer.signin;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */

public class SignInForgetPasswordData {

    private String email;

    public SignInForgetPasswordData(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
