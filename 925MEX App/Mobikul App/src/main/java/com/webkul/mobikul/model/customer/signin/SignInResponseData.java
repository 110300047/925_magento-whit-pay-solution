package com.webkul.mobikul.model.customer.signin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 13/12/16. @Webkul Software Pvt. Ltd
 */
public class SignInResponseData extends BaseModel {

    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("customerEmail")
    @Expose
    private String customerEmail;
    @SerializedName("customerId")
    @Expose
    private int customerId;
    @SerializedName("bannerImage")
    @Expose
    private String customerBannerImage;
    @SerializedName("profileImage")
    @Expose
    private String customerProfileImage;
    @SerializedName("isSeller")
    @Expose
    private boolean isSeller;
    @SerializedName("isAdmin")
    @Expose
    private boolean isAdmin;

    @SerializedName("isPending")
    @Expose
    private boolean isPending;

    public boolean isPending() {
        return isPending;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getCustomerProfileImage() {
        return customerProfileImage;
    }

    public boolean isSeller() {
        return isSeller;
    }

    public String getCustomerBannerImage() {
        return customerBannerImage;
    }

    public void setCustomerBannerImage(String customerBannerImage) {
        this.customerBannerImage = customerBannerImage;
    }

    public boolean isAdmin() {
        return isAdmin;
    }
}