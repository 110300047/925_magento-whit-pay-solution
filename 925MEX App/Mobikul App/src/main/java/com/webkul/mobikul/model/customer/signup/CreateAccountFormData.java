package com.webkul.mobikul.model.customer.signup;

import android.databinding.Bindable;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.BR;
import com.webkul.mobikul.R;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.fragment.SignUpFragment;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;
import static com.webkul.mobikul.helper.Utils.setSpinnerError;

/**
 * Created by vedesh.kumar on 18/1/17. @Webkul Software Private limited
 */

public class CreateAccountFormData extends BaseModel {

    @SerializedName("isPrefixVisible")
    @Expose
    private boolean isPrefixVisible;
    @SerializedName("isPrefixRequired")
    @Expose
    private boolean isPrefixRequired;
    @SerializedName("prefixHasOptions")
    @Expose
    private boolean prefixHasOptions;
    @SerializedName("prefixOptions")
    @Expose
    private List<String> prefixOptions;
    @SerializedName("isMiddlenameVisible")
    @Expose
    private boolean isMiddlenameVisible;
    @SerializedName("isSuffixVisible")
    @Expose
    private boolean isSuffixVisible;
    @SerializedName("isSuffixRequired")
    @Expose
    private boolean isSuffixRequired;
    @SerializedName("suffixHasOptions")
    @Expose
    private boolean suffixHasOptions;
    @SerializedName("suffixOptions")
    @Expose
    private List<String> suffixOptions;
    @SerializedName("isDOBVisible")
    @Expose
    private boolean isDOBVisible;
    @SerializedName("isDOBRequired")
    @Expose
    private boolean isDOBRequired;
    @SerializedName("isTaxVisible")
    @Expose
    private boolean isTaxVisible;
    @SerializedName("isTaxRequired")
    @Expose
    private boolean isTaxRequired;
    @SerializedName("isGenderVisible")
    @Expose
    private boolean isGenderVisible;
    @SerializedName("isGenderRequired")
    @Expose
    private boolean isGenderRequired;
    @SerializedName("dateFormat")
    @Expose
    private String dateFormat;
    @SerializedName("isMobileVisible")
    @Expose
    private boolean isMobileVisible;
    @SerializedName("isMobileRequired")
    @Expose
    private boolean isMobileRequired;
    @SerializedName("sellerRegistrationStatus")
    @Expose
    private boolean showSellerSignUp;


    private String prefix = "";
    private String firstName = "";
    private String middleName = "";
    private String lastName = "";
    private String suffix = "";
    private String dob = "";
    private String taxVat = "";
    private int gender = 0;
    private String emailAddr = "";
    private String password = "";
    private String confirmPassword = "";
    private String pictureURL = "";
    private String mobile = "";
    private int isSocial;
    private String shopURL = "";
    private boolean signUpAsSeller;
    private boolean isFormValidated;
    private String shopName = "";

    public String getShopURL() {
        return shopURL;
    }

    public void setShopURL(String shopURL) {
        this.shopURL = shopURL;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public boolean isShowSellerSignUp() {
        return ApplicationConstant.IS_MARKETPLACE_APP;
    }

    public void setShowSellerSignUp(boolean showSellerSignUp) {
        this.showSellerSignUp = showSellerSignUp;
    }

    @Bindable
    public boolean isSignUpAsSeller() {
        return signUpAsSeller;
    }

    public void setSignUpAsSeller(boolean signUpAsSeller) {
        this.signUpAsSeller = signUpAsSeller;
        notifyPropertyChanged(BR.signUpAsSeller);
        Log.d(ApplicationConstant.TAG, "SignUpFormData setSignUpAsSeller: " + signUpAsSeller);
    }


    public String getPrefix() {
        return prefix.trim();
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName.trim();
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName.trim();
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName.trim();
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSuffix() {
        return suffix.trim();
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTaxVat() {
        return taxVat.trim();
    }

    public void setTaxVat(String taxVat) {
        this.taxVat = taxVat;
    }


    public int getGender() {
        return gender;
    }

    public void setGender(String gender) {
        if (gender.equals("Male"))
            this.gender = 1;
        else if (gender.equals("Female"))
            this.gender = 2;
        else
            this.gender = 0;
    }

    public String getEmailAddr() {
        return emailAddr.trim();
    }

    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    public String getPassword() {
        return password.trim();
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword.trim();
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public int getIsSocial() {
        return isSocial;
    }

    public void setIsSocial(int isSocial) {
        this.isSocial = isSocial;
    }

    public boolean isFormValidated(SignUpFragment signUpFragment) {
        isFormValidated = true;
        if (!password.trim().equals(confirmPassword.trim())) {
            signUpFragment.getBinding().confirmpasswordEt.requestFocus();
            signUpFragment.getBinding().confirmpasswordEt.setError(signUpFragment.getString(R.string.password_match_issue));
            isFormValidated = false;
        } else if (confirmPassword.trim().matches("")) {
            signUpFragment.getBinding().confirmpasswordEt.requestFocus();
            signUpFragment.getBinding().confirmpasswordEt.setError(signUpFragment.getString(R.string.confirm_password_error));
            isFormValidated = false;
        } else
            signUpFragment.getBinding().confirmpasswordEt.setError(null);

        if (password.trim().matches("")) {
            signUpFragment.getBinding().passwordEt.requestFocus();
            signUpFragment.getBinding().passwordEt.setError(signUpFragment.getString(R.string.fill_password));
            isFormValidated = false;
        } else if (password.trim().length() < 6) {
            signUpFragment.getBinding().passwordEt.requestFocus();
            signUpFragment.getBinding().passwordEt.setError(signUpFragment.getResources().getString(R.string.alert_password_length));
            isFormValidated = false;
        } else
            signUpFragment.getBinding().passwordEt.setError(null);

        if (emailAddr.trim().matches("")) {
            signUpFragment.getBinding().emailEt.requestFocus();
            signUpFragment.getBinding().emailEt.setError(signUpFragment.getString(R.string.fill_email_error));
            isFormValidated = false;
        } else if (!EMAIL_PATTERN.matcher(emailAddr.trim()).matches()) {
            signUpFragment.getBinding().emailEt.requestFocus();
            signUpFragment.getBinding().emailEt.setError(signUpFragment.getString(R.string.fill_valid_email));
            isFormValidated = false;
        }

        if (isIsGenderVisible() && isIsGenderRequired()) {
            if (gender == 0) {
                setSpinnerError(signUpFragment.getBinding().genderSpinner, signUpFragment.getString(R.string.select_gender_error));
                isFormValidated = false;
            }
        }

        if (isIsTaxVisible() && isIsTaxRequired()) {
            if (taxVat.trim().matches("")) {
                signUpFragment.getBinding().taxvatEt.requestFocus();
                signUpFragment.getBinding().taxvatEt.setError(signUpFragment.getString(R.string.fill_tax_error));
                isFormValidated = false;
            } else
                signUpFragment.getBinding().taxvatEt.setError(null);
        }

        if (isIsDOBVisible() && isIsDOBRequired()) {
            if (dob.trim().matches("")) {
                signUpFragment.getBinding().dobEt.requestFocus();
                signUpFragment.getBinding().dobEt.setError(signUpFragment.getString(R.string.fill_dob_error));
                isFormValidated = false;
            } else
                signUpFragment.getBinding().dobEt.setError(null);
        }

        if (isIsSuffixVisible() && isIsSuffixRequired()) {
            if (suffixHasOptions) {
                if (suffix.matches("")) {
                    setSpinnerError(signUpFragment.getBinding().suffixSpinner, signUpFragment.getString(R.string.select_suffix_error));
                    isFormValidated = false;
                }
            } else {
                if (suffix.trim().matches("")) {
                    signUpFragment.getBinding().suffixEt.requestFocus();
                    signUpFragment.getBinding().suffixEt.setError(signUpFragment.getString(R.string.fill_suffix_error));
                    isFormValidated = false;
                } else
                    signUpFragment.getBinding().suffixEt.setError(null);
            }
        }

//        if (isIsMobileVisible() && isIsMobileRequired()){
//            if (mobile.matches("")){
//                signUpFragment.getBinding().mobileEt.requestFocus();
//                signUpFragment.getBinding().mobileEt.setError("Please fill Mobile Number!!!");
//                isFormValidated=false;
//            }else if (mobile.length() != ApplicationConstant.NUMBER_OF_MOBILE_NUMBER_DIGIT){
//                signUpFragment.getBinding().mobileEt.requestFocus();
//                signUpFragment.getBinding().mobileEt.setError(signUpFragment.getContext().getResources().getString(R.string.enter_valid_mobile));
//                isFormValidated = false;
//            }else {
//                signUpFragment.getBinding().mobileEt.setError(null);
//            }
//        }

        if (lastName.trim().matches("")) {
            signUpFragment.getBinding().lastnameEt.requestFocus();
            signUpFragment.getBinding().lastnameEt.setError(signUpFragment.getString(R.string.fill_last_name_error));
            isFormValidated = false;
        } else
            signUpFragment.getBinding().lastnameEt.setError(null);

        if (firstName.trim().matches("")) {
            signUpFragment.getBinding().firstnameEt.requestFocus();
            signUpFragment.getBinding().firstnameEt.setError(signUpFragment.getString(R.string.fill_first_name_error));
            isFormValidated = false;
        } else
            signUpFragment.getBinding().firstnameEt.setError(null);

        if (isIsPrefixVisible() && isIsPrefixRequired()) {
            if (prefixHasOptions) {
                if (prefix.matches("")) {
                    setSpinnerError(signUpFragment.getBinding().prefixSpinner, signUpFragment.getString(R.string.select_prefix_error));
                    isFormValidated = false;
                }
            } else {
                if (prefix.trim().matches("")) {
                    signUpFragment.getBinding().prefixEt.requestFocus();
                    signUpFragment.getBinding().prefixEt.setError(signUpFragment.getString(R.string.please_fill_prefix));
                    isFormValidated = false;
                } else
                    signUpFragment.getBinding().prefixEt.setError(null);
            }
        }
        if (ApplicationConstant.IS_MARKETPLACE_APP && isSignUpAsSeller()) {
            if (getShopURL().isEmpty()) {
                signUpFragment.getBinding().sellerStoreUrlEt.requestFocus();
                signUpFragment.getBinding().sellerStoreUrlEt.setError(signUpFragment.getContext().getResources().getString(R.string.shop_url) + " " + signUpFragment.getContext().getString(R.string.is_require_text));
                isFormValidated = false;
            } else {
                signUpFragment.getBinding().sellerStoreUrlEt.setError(null);
            }
        }

        return isFormValidated;
    }

    public void setFormValidated(boolean formValidated) {
        isFormValidated = formValidated;
    }


    public boolean isIsPrefixVisible() {
        return isPrefixVisible;
    }

    public void setIsPrefixVisible(boolean isPrefixVisible) {
        this.isPrefixVisible = isPrefixVisible;
    }

    public boolean isIsPrefixRequired() {
        return isPrefixRequired;
    }

    public void setIsPrefixRequired(boolean isPrefixRequired) {
        this.isPrefixRequired = isPrefixRequired;
    }

    public boolean isPrefixHasOptions() {
        return prefixHasOptions;
    }

    public void setPrefixHasOptions(boolean prefixHasOptions) {
        this.prefixHasOptions = prefixHasOptions;
    }

    public List<String> getPrefixOptions() {
        return prefixOptions;
    }

    public void setPrefixOptions(List<String> prefixOptions) {
        this.prefixOptions = prefixOptions;
    }

    public boolean isIsMiddlenameVisible() {
        return isMiddlenameVisible;
    }

    public void setIsMiddlenameVisible(boolean isMiddlenameVisible) {
        this.isMiddlenameVisible = isMiddlenameVisible;
    }

    public boolean isIsSuffixVisible() {
        return isSuffixVisible;
    }

    public void setIsSuffixVisible(boolean isSuffixVisible) {
        this.isSuffixVisible = isSuffixVisible;
    }

    public boolean isIsSuffixRequired() {
        return isSuffixRequired;
    }

    public void setIsSuffixRequired(boolean isSuffixRequired) {
        this.isSuffixRequired = isSuffixRequired;
    }

    public boolean isSuffixHasOptions() {
        return suffixHasOptions;
    }

    public void setSuffixHasOptions(boolean suffixHasOptions) {
        this.suffixHasOptions = suffixHasOptions;
    }

    public List<String> getSuffixOptions() {
        return suffixOptions;
    }

    public void setSuffixOptions(List<String> suffixOptions) {
        this.suffixOptions = suffixOptions;
    }

    public boolean isIsDOBVisible() {
        return isDOBVisible;
    }

    public void setIsDOBVisible(boolean isDOBVisible) {
        this.isDOBVisible = isDOBVisible;
    }

    public boolean isIsDOBRequired() {
        return isDOBRequired;
    }

    public void setIsDOBRequired(boolean isDOBRequired) {
        this.isDOBRequired = isDOBRequired;
    }

    public boolean isIsTaxVisible() {
        return isTaxVisible;
    }

    public void setIsTaxVisible(boolean isTaxVisible) {
        this.isTaxVisible = isTaxVisible;
    }

    public boolean isIsTaxRequired() {
        return isTaxRequired;
    }

    public void setIsTaxRequired(boolean isTaxRequired) {
        this.isTaxRequired = isTaxRequired;
    }

    public boolean isIsGenderVisible() {
        return isGenderVisible;
    }

    public void setIsGenderVisible(boolean isGenderVisible) {
        this.isGenderVisible = isGenderVisible;
    }

    public boolean isIsGenderRequired() {
        return isGenderRequired;
    }

    public void setIsGenderRequired(boolean isGenderRequired) {
        this.isGenderRequired = isGenderRequired;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public boolean isIsMobileVisible() {
        return isMobileVisible;
    }

    public void setMobileVisible(boolean mobileVisible) {
        isMobileVisible = mobileVisible;
    }

    public boolean isIsMobileRequired() {
        return isMobileRequired;
    }

    public void setMobileRequired(boolean mobileRequired) {
        isMobileRequired = mobileRequired;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
