package com.webkul.mobikul.model.customer.signup;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 18/1/17. @Webkul Software Private limited
 */

public class CreateAccountFormDataParcelable extends BaseModel implements Parcelable {

    public static final Creator<CreateAccountFormDataParcelable> CREATOR = new Creator<CreateAccountFormDataParcelable>() {
        @Override
        public CreateAccountFormDataParcelable createFromParcel(Parcel in) {
            return new CreateAccountFormDataParcelable(in);
        }

        @Override
        public CreateAccountFormDataParcelable[] newArray(int size) {
            return new CreateAccountFormDataParcelable[size];
        }
    };
    @SerializedName("isPrefixVisible")
    @Expose
    private boolean isPrefixVisible;
    @SerializedName("isPrefixRequired")
    @Expose
    private boolean isPrefixRequired;
    @SerializedName("prefixHasOptions")
    @Expose
    private boolean prefixHasOptions;
    @SerializedName("prefixOptions")
    @Expose
    private List<String> prefixOptions;
    @SerializedName("isMiddlenameVisible")
    @Expose
    private boolean isMiddlenameVisible;
    @SerializedName("isSuffixVisible")
    @Expose
    private boolean isSuffixVisible;
    @SerializedName("isSuffixRequired")
    @Expose
    private boolean isSuffixRequired;
    @SerializedName("suffixHasOptions")
    @Expose
    private boolean suffixHasOptions;
    @SerializedName("suffixOptions")
    @Expose
    private List<String> suffixOptions;
    @SerializedName("isDOBVisible")
    @Expose
    private boolean isDOBVisible;
    @SerializedName("isDOBRequired")
    @Expose
    private boolean isDOBRequired;
    @SerializedName("isTaxVisible")
    @Expose
    private boolean isTaxVisible;
    @SerializedName("isTaxRequired")
    @Expose
    private boolean isTaxRequired;
    @SerializedName("isGenderVisible")
    @Expose
    private boolean isGenderVisible;
    @SerializedName("isGenderRequired")
    @Expose
    private boolean isGenderRequired;
    @SerializedName("dateFormat")
    @Expose
    private String dateFormat;
    private String prefix = "";
    private String firstName = "";
    private String middleName = "";
    private String lastName = "";
    private String suffix = "";
    private String dob = "";
    private String taxVat = "";
    private String gender = "";
    private String emailAddr = "";
    private String password = "";
    private String confirmPassword = "";
    private String pictureURL = "";
    private int isSocial;
    private boolean isFormValidated;
    private String loginFrom = "";

    public CreateAccountFormDataParcelable() {
    }

    public CreateAccountFormDataParcelable(Parcel in) {
        isPrefixVisible = in.readByte() != 0;
        isPrefixRequired = in.readByte() != 0;
        prefixHasOptions = in.readByte() != 0;
        prefixOptions = in.createStringArrayList();
        isMiddlenameVisible = in.readByte() != 0;
        isSuffixVisible = in.readByte() != 0;
        isSuffixRequired = in.readByte() != 0;
        suffixHasOptions = in.readByte() != 0;
        suffixOptions = in.createStringArrayList();
        isDOBVisible = in.readByte() != 0;
        isDOBRequired = in.readByte() != 0;
        isTaxVisible = in.readByte() != 0;
        isTaxRequired = in.readByte() != 0;
        isGenderVisible = in.readByte() != 0;
        isGenderRequired = in.readByte() != 0;
        dateFormat = in.readString();
        prefix = in.readString();
        firstName = in.readString();
        middleName = in.readString();
        lastName = in.readString();
        suffix = in.readString();
        dob = in.readString();
        taxVat = in.readString();
        gender = in.readString();
        emailAddr = in.readString();
        password = in.readString();
        confirmPassword = in.readString();
        pictureURL = in.readString();
        isSocial = in.readInt();
        isFormValidated = in.readByte() != 0;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTaxVat() {
        return taxVat;
    }

    public void setTaxVat(String taxVat) {
        this.taxVat = taxVat;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailAddr() {
        return emailAddr;
    }

    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public int getIsSocial() {
        return isSocial;
    }

    public void setIsSocial(int isSocial) {
        this.isSocial = isSocial;
    }

//    public boolean isFormValidated(SignUpFragment signUpFragment) {
//        isFormValidated = true;
//        if (!password.trim().equals(confirmPassword.trim())) {
//            signUpFragment.getBinding().confirmpasswordEt.requestFocus();
//            signUpFragment.getBinding().confirmpasswordEt.setError("Password and confirm password must match");
//            isFormValidated = false;
//        } else if (confirmPassword.trim().matches("")) {
//            signUpFragment.getBinding().confirmpasswordEt.requestFocus();
//            signUpFragment.getBinding().confirmpasswordEt.setError("Please fill confirm password !!!");
//            isFormValidated = false;
//        } else
//            signUpFragment.getBinding().confirmpasswordEt.setError(null);
//
//        if (password.trim().matches("")) {
//            signUpFragment.getBinding().passwordEt.requestFocus();
//            signUpFragment.getBinding().passwordEt.setError("Please fill password !!!");
//            isFormValidated = false;
//        } else if (password.trim().length() < 6) {
//            signUpFragment.getBinding().passwordEt.requestFocus();
//            signUpFragment.getBinding().passwordEt.setError(signUpFragment.getResources().getString(R.string.alert_password_length));
//            isFormValidated = false;
//        } else
//            signUpFragment.getBinding().passwordEt.setError(null);
//
//        if (emailAddr.trim().matches("")) {
//            signUpFragment.getBinding().emailEt.requestFocus();
//            signUpFragment.getBinding().emailEt.setError("Please fill email address !!!");
//            isFormValidated = false;
//        } else if (!EMAIL_PATTERN.matcher(emailAddr.trim()).matches()) {
//            signUpFragment.getBinding().emailEt.requestFocus();
//            signUpFragment.getBinding().emailEt.setError("Enter a valid email address");
//            isFormValidated = false;
//        }
//
//        if (isIsGenderVisible() && isIsGenderRequired()) {
//            if (gender == 0) {
//                setSpinnerError(signUpFragment.getBinding().genderSpinner, "Please select gender");
//                isFormValidated = false;
//            }
//        }
//
//        if (isIsTaxVisible() && isIsTaxRequired()) {
//            if (taxVat.trim().matches("")) {
//                signUpFragment.getBinding().taxvatEt.requestFocus();
//                signUpFragment.getBinding().taxvatEt.setError("Please fill TAX/VAT !!!");
//                isFormValidated = false;
//            } else
//                signUpFragment.getBinding().taxvatEt.setError(null);
//        }
//
//        if (isIsDOBVisible() && isIsDOBRequired()) {
//            if (dob.trim().matches("")) {
//                signUpFragment.getBinding().dobEt.requestFocus();
//                signUpFragment.getBinding().dobEt.setError("Please fill date of birth !!!");
//                isFormValidated = false;
//            } else
//                signUpFragment.getBinding().dobEt.setError(null);
//        }
//
//        if (isIsSuffixVisible() && isIsSuffixRequired()) {
//            if (suffixHasOptions) {
//                if (suffix.matches("")) {
//                    setSpinnerError(signUpFragment.getBinding().suffixSpinner, "Please select suffix");
//                    isFormValidated = false;
//                }
//            } else {
//                if (suffix.trim().matches("")) {
//                    signUpFragment.getBinding().suffixEt.requestFocus();
//                    signUpFragment.getBinding().suffixEt.setError("Please fill suffix !!!");
//                    isFormValidated = false;
//                } else
//                    signUpFragment.getBinding().suffixEt.setError(null);
//            }
//        }
//
//        if (lastName.trim().matches("")) {
//            signUpFragment.getBinding().lastnameEt.requestFocus();
//            signUpFragment.getBinding().lastnameEt.setError("Please fill last Name !!!");
//            isFormValidated = false;
//        } else
//            signUpFragment.getBinding().lastnameEt.setError(null);
//
//        if (firstName.trim().matches("")) {
//            signUpFragment.getBinding().firstnameEt.requestFocus();
//            signUpFragment.getBinding().firstnameEt.setError("Please fill first Name !!!");
//            isFormValidated = false;
//        } else
//            signUpFragment.getBinding().firstnameEt.setError(null);
//
//        if (isIsPrefixVisible() && isIsPrefixRequired()) {
//            if (prefixHasOptions) {
//                if (prefix.matches("")) {
//                    setSpinnerError(signUpFragment.getBinding().prefixSpinner, "Please select prefix");
//                    isFormValidated = false;
//                }
//            } else {
//                if (prefix.trim().matches("")) {
//                    signUpFragment.getBinding().prefixEt.requestFocus();
//                    signUpFragment.getBinding().prefixEt.setError("Please fill prefix !!!");
//                    isFormValidated = false;
//                } else
//                    signUpFragment.getBinding().prefixEt.setError(null);
//            }
//        }
//        return isFormValidated;
//    }

    public void setFormValidated(boolean formValidated) {
        isFormValidated = formValidated;
    }


    public boolean isIsPrefixVisible() {
        return isPrefixVisible;
    }

    public void setIsPrefixVisible(boolean isPrefixVisible) {
        this.isPrefixVisible = isPrefixVisible;
    }

    public boolean isIsPrefixRequired() {
        return isPrefixRequired;
    }

    public void setIsPrefixRequired(boolean isPrefixRequired) {
        this.isPrefixRequired = isPrefixRequired;
    }

    public boolean isPrefixHasOptions() {
        return prefixHasOptions;
    }

    public void setPrefixHasOptions(boolean prefixHasOptions) {
        this.prefixHasOptions = prefixHasOptions;
    }

    public List<String> getPrefixOptions() {
        return prefixOptions;
    }

    public void setPrefixOptions(List<String> prefixOptions) {
        this.prefixOptions = prefixOptions;
    }

    public boolean isIsMiddlenameVisible() {
        return isMiddlenameVisible;
    }

    public void setIsMiddlenameVisible(boolean isMiddlenameVisible) {
        this.isMiddlenameVisible = isMiddlenameVisible;
    }

    public boolean isIsSuffixVisible() {
        return isSuffixVisible;
    }

    public void setIsSuffixVisible(boolean isSuffixVisible) {
        this.isSuffixVisible = isSuffixVisible;
    }

    public boolean isIsSuffixRequired() {
        return isSuffixRequired;
    }

    public void setIsSuffixRequired(boolean isSuffixRequired) {
        this.isSuffixRequired = isSuffixRequired;
    }

    public boolean isSuffixHasOptions() {
        return suffixHasOptions;
    }

    public void setSuffixHasOptions(boolean suffixHasOptions) {
        this.suffixHasOptions = suffixHasOptions;
    }

    public List<String> getSuffixOptions() {
        return suffixOptions;
    }

    public void setSuffixOptions(List<String> suffixOptions) {
        this.suffixOptions = suffixOptions;
    }

    public boolean isIsDOBVisible() {
        return isDOBVisible;
    }

    public void setIsDOBVisible(boolean isDOBVisible) {
        this.isDOBVisible = isDOBVisible;
    }

    public boolean isIsDOBRequired() {
        return isDOBRequired;
    }

    public void setIsDOBRequired(boolean isDOBRequired) {
        this.isDOBRequired = isDOBRequired;
    }

    public boolean isIsTaxVisible() {
        return isTaxVisible;
    }

    public void setIsTaxVisible(boolean isTaxVisible) {
        this.isTaxVisible = isTaxVisible;
    }

    public boolean isIsTaxRequired() {
        return isTaxRequired;
    }

    public void setIsTaxRequired(boolean isTaxRequired) {
        this.isTaxRequired = isTaxRequired;
    }

    public boolean isIsGenderVisible() {
        return isGenderVisible;
    }

    public void setIsGenderVisible(boolean isGenderVisible) {
        this.isGenderVisible = isGenderVisible;
    }

    public boolean isIsGenderRequired() {
        return isGenderRequired;
    }

    public void setIsGenderRequired(boolean isGenderRequired) {
        this.isGenderRequired = isGenderRequired;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isPrefixVisible ? 1 : 0));
        dest.writeByte((byte) (isPrefixRequired ? 1 : 0));
        dest.writeByte((byte) (prefixHasOptions ? 1 : 0));
        dest.writeStringList(prefixOptions);
        dest.writeByte((byte) (isMiddlenameVisible ? 1 : 0));
        dest.writeByte((byte) (isSuffixVisible ? 1 : 0));
        dest.writeByte((byte) (isSuffixRequired ? 1 : 0));
        dest.writeByte((byte) (suffixHasOptions ? 1 : 0));
        dest.writeStringList(suffixOptions);
        dest.writeByte((byte) (isDOBVisible ? 1 : 0));
        dest.writeByte((byte) (isDOBRequired ? 1 : 0));
        dest.writeByte((byte) (isTaxVisible ? 1 : 0));
        dest.writeByte((byte) (isTaxRequired ? 1 : 0));
        dest.writeByte((byte) (isGenderVisible ? 1 : 0));
        dest.writeByte((byte) (isGenderRequired ? 1 : 0));
        dest.writeString(dateFormat);
        dest.writeString(prefix);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(lastName);
        dest.writeString(suffix);
        dest.writeString(dob);
        dest.writeString(taxVat);
        dest.writeString(gender);
        dest.writeString(emailAddr);
        dest.writeString(password);
        dest.writeString(confirmPassword);
        dest.writeString(pictureURL);
        dest.writeInt(isSocial);
        dest.writeByte((byte) (isFormValidated ? 1 : 0));
    }

    public String getLoginFrom() {
        return loginFrom;
    }

    public void setLoginFrom(String loginFrom) {
        this.loginFrom = loginFrom;
    }
}
