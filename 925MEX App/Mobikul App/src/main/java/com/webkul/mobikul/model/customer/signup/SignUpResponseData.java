package com.webkul.mobikul.model.customer.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 18/1/17. @Webkul Software Private limited
 */

public class SignUpResponseData extends BaseModel {

    @SerializedName("customerId")
    @Expose
    private int customerId;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("customerEmail")
    @Expose
    private String customerEmail;
    @SerializedName("isSeller")
    @Expose
    private boolean isSeller;
    @SerializedName("isPending")
    @Expose
    private boolean isPending;


    public boolean isIsSeller() {
        return isSeller;
    }

    public void setSeller(boolean seller) {
        isSeller = seller;
    }

    public boolean isIsPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }
}
