package com.webkul.mobikul.model.customer.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class CatalogProductRemoveFromWishlistResponse extends BaseModel {

    @SerializedName("alreadyDeleted")
    @Expose
    private boolean alreadyDeleted;

    public void setAlreadyDeleted(boolean alreadyDeleted) {
        this.alreadyDeleted = alreadyDeleted;
    }

    public boolean getAlreadyDeleted() {
        return alreadyDeleted;
    }
}