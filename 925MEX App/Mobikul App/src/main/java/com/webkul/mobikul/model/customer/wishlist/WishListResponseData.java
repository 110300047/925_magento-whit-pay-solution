package com.webkul.mobikul.model.customer.wishlist;


/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

public class WishListResponseData extends BaseModel {

    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("wishList")
    @Expose
    private List<WishlistItemData> mWishlistItemData = null;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<WishlistItemData> getWishlistItemData() {
        return mWishlistItemData;
    }

    public void setWishlistItemData(List<WishlistItemData> wishlistItemData) {
        this.mWishlistItemData = wishlistItemData;
    }
}