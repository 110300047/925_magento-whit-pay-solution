package com.webkul.mobikul.model.customer.wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.cart.CartOptionItem;

import java.util.List;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_MAX_QTY;

/**
 * Created by vedesh.kumar on 28/12/16. @Webkul Software Pvt. Ltd
 */

public class WishlistItemData {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("productId")
    @Expose
    private int productId;
    @SerializedName("typeId")
    @Expose
    private String typeId;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("thumbNail")
    @Expose
    private String thumbNail;
    @SerializedName("rating")
    @Expose
    private float rating;
    @SerializedName("options")
    @Expose
    private List<CartOptionItem> itemOption = null;

    private int position;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        if (qty.isEmpty()) {
            qty = "0";
        } else {
            if (Integer.parseInt(qty) < 0) {
                this.qty = "0";
                return;
            }
            if (Integer.parseInt(qty) > DEFAULT_MAX_QTY) {
                this.qty = String.valueOf(DEFAULT_MAX_QTY);
                return;
            }
        }
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public List<CartOptionItem> getItemOption() {
        return itemOption;
    }

    public void setItemOption(List<CartOptionItem> itemOption) {
        this.itemOption = itemOption;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
