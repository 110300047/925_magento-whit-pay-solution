package com.webkul.mobikul.model.extra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 4/5/17.
 */

public class SearchSuggestionResponse extends BaseModel {

    @SerializedName("suggestProductArray")
    @Expose
    private SuggestProductArray suggestProductArray;

    public SuggestProductArray getSuggestProductArray() {
        if (suggestProductArray == null) {
            return new SuggestProductArray();
        }
        return suggestProductArray;
    }
}
