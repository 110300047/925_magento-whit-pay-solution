package com.webkul.mobikul.model.extra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 4/5/17.
 */

public class SuggestProductArray {
    @SuppressWarnings("unused")
    private static final String TAG = "SuggestProductArray";

    @SerializedName("tags")
    @Expose
    private List<SuggestionTagData> tags = null;
    @SerializedName("products")
    @Expose
    private List<SuggestionProductData> products = null;


    public List<SuggestionTagData> getTags() {
        if (tags == null) {
            return new ArrayList<>();
        }
        return tags;
    }

    public List<SuggestionProductData> getProducts() {
        if (products == null) {
            return new ArrayList<>();
        }
        return products;
    }
}
