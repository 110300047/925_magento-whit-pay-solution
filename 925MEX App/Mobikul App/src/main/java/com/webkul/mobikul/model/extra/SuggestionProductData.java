package com.webkul.mobikul.model.extra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 4/5/17.
 */

public class SuggestionProductData {
    @SuppressWarnings("unused")
    private static final String TAG = "SuggestionProductData";


    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("thumbNail")
    @Expose
    private String thumbNail;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("hasSpecialPrice")
    @Expose
    private boolean hasSpecialPrice;
    @SerializedName("specialPrice")
    @Expose
    private String specialPrice;

    private String highlightedProductName;

    public String getProductName() {
        if (productName == null) {
            return "";
        }
        return productName;
    }

    public String getProductId() {
        if (productId == null) {
            return "";

        }
        return productId;
    }

    public String getThumbNail() {
        if (thumbNail == null) {
            return "";
        }

        return thumbNail;
    }

    public String getPrice() {
        if (price == null) {
            return "";
        }

        return price;
    }

    public boolean isHasSpecialPrice() {
        return hasSpecialPrice;
    }

    public String getSpecialPrice() {
        if (specialPrice == null) {
            return "";
        }

        return specialPrice;
    }

    public String getHighlightedProductName() {
        if (highlightedProductName == null) {
            return "";
        }
        return highlightedProductName;
    }

    public void setHighlightedProductName(String highlightedProductName) {
        this.highlightedProductName = highlightedProductName;
    }
}