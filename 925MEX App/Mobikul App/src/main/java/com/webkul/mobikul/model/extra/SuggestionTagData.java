package com.webkul.mobikul.model.extra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 4/5/17.
 */

public class SuggestionTagData {
    @SuppressWarnings("unused")
    private static final String TAG = "SuggestionTagData";


    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("count")
    @Expose
    private String count;


    public String getLabel() {
        if (label == null) {
            return "";
        }

        return label;
    }

    public String getCount() {
        if (count == null) {
            return "";
        }

        return count;
    }
}

