package com.webkul.mobikul.model.notification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Private limited
 */

public class NotificationList implements Parcelable {

    public static final Creator<NotificationList> CREATOR = new Creator<NotificationList>() {
        @Override
        public NotificationList createFromParcel(Parcel in) {
            return new NotificationList(in);
        }

        @Override
        public NotificationList[] newArray(int size) {
            return new NotificationList[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("notificationType")
    @Expose
    private String notificationType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("categoryId")
    @Expose
    private int categoryId;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("productType")
    @Expose
    private String productType;
    @SerializedName("productId")
    @Expose
    private int productId;
    @SerializedName("banner")
    @Expose
    private String banner;

    protected NotificationList(Parcel in) {
        id = in.readString();
        content = in.readString();
        notificationType = in.readString();
        title = in.readString();
        categoryName = in.readString();
        categoryId = in.readInt();
        productName = in.readString();
        productType = in.readString();
        productId = in.readInt();
        banner = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(content);
        dest.writeString(notificationType);
        dest.writeString(title);
        dest.writeString(categoryName);
        dest.writeInt(categoryId);
        dest.writeString(productName);
        dest.writeString(productType);
        dest.writeInt(productId);
        dest.writeString(banner);
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}