package com.webkul.mobikul.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Private limited
 */

public class NotificationResponseData extends BaseModel {

    @SerializedName("notificationList")
    @Expose
    private List<NotificationList> notificationList = null;

    public List<NotificationList> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<NotificationList> notificationList) {
        this.notificationList = notificationList;
    }
}