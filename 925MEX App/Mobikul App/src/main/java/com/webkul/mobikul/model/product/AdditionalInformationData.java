package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdditionalInformationData implements Parcelable {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private String value;

    protected AdditionalInformationData(Parcel in) {
        label = in.readString();
        value = in.readString();
    }

    public static final Creator<AdditionalInformationData> CREATOR = new Creator<AdditionalInformationData>() {
        @Override
        public AdditionalInformationData createFromParcel(Parcel in) {
            return new AdditionalInformationData(in);
        }

        @Override
        public AdditionalInformationData[] newArray(int size) {
            return new AdditionalInformationData[size];
        }
    };

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(label);
        parcel.writeString(value);
    }
}