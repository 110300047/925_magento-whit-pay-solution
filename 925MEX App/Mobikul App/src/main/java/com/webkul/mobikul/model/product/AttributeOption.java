package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class AttributeOption implements Parcelable {
    public static final Creator<AttributeOption> CREATOR = new Creator<AttributeOption>() {
        @Override
        public AttributeOption createFromParcel(Parcel in) {
            return new AttributeOption(in);
        }

        @Override
        public AttributeOption[] newArray(int size) {
            return new AttributeOption[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("products")
    @Expose
    private List<String> products = null;

    protected AttributeOption(Parcel in) {
        id = in.readString();
        label = in.readString();
        products = in.createStringArrayList();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(label);
        dest.writeStringList(products);
    }
}