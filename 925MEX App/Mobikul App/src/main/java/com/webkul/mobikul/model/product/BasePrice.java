package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BasePrice implements Parcelable {

    public static final Creator<BasePrice> CREATOR = new Creator<BasePrice>() {
        @Override
        public BasePrice createFromParcel(Parcel in) {
            return new BasePrice(in);
        }

        @Override
        public BasePrice[] newArray(int size) {
            return new BasePrice[size];
        }
    };
    @SerializedName("amount")
    @Expose
    private String amount;

    protected BasePrice(Parcel in) {
        amount = in.readString();
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
    }
}