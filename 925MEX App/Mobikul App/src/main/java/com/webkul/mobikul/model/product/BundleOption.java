package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class BundleOption {
    @SerializedName("option_id")
    @Expose
    private String optionId;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("required")
    @Expose
    private int required;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("default_title")
    @Expose
    private String defaultTitle;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("optionValues")
    @Expose
    private List<OptionDetails> optionValues;

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefaultTitle() {
        return defaultTitle;
    }

    public void setDefaultTitle(String defaultTitle) {
        this.defaultTitle = defaultTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<OptionDetails> getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(List<OptionDetails> optionValues) {
        this.optionValues = optionValues;
    }
}
