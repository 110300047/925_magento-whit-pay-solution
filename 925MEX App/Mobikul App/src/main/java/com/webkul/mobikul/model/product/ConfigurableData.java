package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class ConfigurableData implements Parcelable {
    public static final Creator<ConfigurableData> CREATOR = new Creator<ConfigurableData>() {
        @Override
        public ConfigurableData createFromParcel(Parcel in) {
            return new ConfigurableData(in);
        }

        @Override
        public ConfigurableData[] newArray(int size) {
            return new ConfigurableData[size];
        }
    };
    @SerializedName("attributes")
    @Expose
    private ArrayList<OptionAttributes> attributes = null;
    @SerializedName("template")
    @Expose
    private String template;
    @SerializedName("optionPrices")
    @Expose
    private ArrayList<OptionPrice> optionPrices = null;
    @SerializedName("prices")
    @Expose
    private Prices prices;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("chooseText")
    @Expose
    private String chooseText;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("index")
    @Expose
    private String index;
    @SerializedName("swatchData")
    @Expose
    private String swatchData;

    protected ConfigurableData(Parcel in) {
        attributes = in.createTypedArrayList(OptionAttributes.CREATOR);
        template = in.readString();
        optionPrices = in.createTypedArrayList(OptionPrice.CREATOR);
        productId = in.readString();
        chooseText = in.readString();
        images = in.readString();
        index = in.readString();
        swatchData = in.readString();
    }

    public ArrayList<OptionAttributes> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<OptionAttributes> attributes) {
        this.attributes = attributes;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getChooseText() {
        return chooseText;
    }

    public void setChooseText(String chooseText) {
        this.chooseText = chooseText;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public ArrayList<OptionPrice> getOptionPrices() {
        return optionPrices;
    }

    public void setOptionPrices(ArrayList<OptionPrice> optionPrices) {
        this.optionPrices = optionPrices;
    }

    public Prices getPrices() {
        return prices;
    }

    public void setPrices(Prices prices) {
        this.prices = prices;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(attributes);
        dest.writeString(template);
        dest.writeTypedList(optionPrices);
        dest.writeString(productId);
        dest.writeString(chooseText);
        dest.writeString(images);
        dest.writeString(index);
        dest.writeString(swatchData);
    }

    public String getSwatchData() {
        return swatchData;
    }

    public void setSwatchData(String swatchData) {
        this.swatchData = swatchData;
    }
}
