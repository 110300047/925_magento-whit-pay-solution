package com.webkul.mobikul.model.product;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.webkul.mobikul.BR;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.fragment.ProductCreateReviewFragment;


/**
 * Created by vedesh.kumar on 3/1/17. @Webkul Software Pvt. Ltd
 */

@SuppressWarnings("WeakerAccess")
public class CreateReviewData extends BaseObservable {

    private String nickName;
    private String summary;
    private String comment;

    private boolean nickNameValidated;
    private boolean summaryValidated;
    private boolean commentValidated;


    @Bindable
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        setNickNameValidated(!nickName.isEmpty());
        this.nickName = nickName;
        notifyPropertyChanged(BR.nickName);
    }

    @Bindable
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        setSummaryValidated(!summary.isEmpty());
        this.summary = summary;
        notifyPropertyChanged(BR.summary);
    }

    @Bindable
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        setCommentValidated(!comment.isEmpty());
        this.comment = comment;
        notifyPropertyChanged(BR.comment);
    }

    public boolean isNickNameValidated() {
        return nickNameValidated;
    }

    public void setNickNameValidated(boolean nickNameValidated) {
        this.nickNameValidated = nickNameValidated;
    }

    public boolean isSummaryValidated() {
        return summaryValidated;
    }

    public void setSummaryValidated(boolean summaryValidated) {
        this.summaryValidated = summaryValidated;
    }

    public boolean isCommentValidated() {
        return commentValidated;
    }

    public void setCommentValidated(boolean commentValidated) {
        this.commentValidated = commentValidated;
    }

    public boolean isFormValidated(Context context) {

        ProductCreateReviewFragment productCreateReviewFragment = (ProductCreateReviewFragment) ((NewProductActivity) context).getSupportFragmentManager()
                .findFragmentByTag(ProductCreateReviewFragment.class.getSimpleName());
        if (!isNickNameValidated()) {
            productCreateReviewFragment.getBinding().nicknameTil.requestFocus();
            return false;
        } else if (!isSummaryValidated()) {
            productCreateReviewFragment.getBinding().summaryTil.requestFocus();
            return false;
        } else if (!isCommentValidated()) {
            productCreateReviewFragment.getBinding().commentTil.requestFocus();
            return false;
        }

        return true;
    }

}
