package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FinalPrice implements Parcelable {

    public static final Creator<FinalPrice> CREATOR = new Creator<FinalPrice>() {
        @Override
        public FinalPrice createFromParcel(Parcel in) {
            return new FinalPrice(in);
        }

        @Override
        public FinalPrice[] newArray(int size) {
            return new FinalPrice[size];
        }
    };
    @SerializedName("amount")
    @Expose
    private String amount;

    protected FinalPrice(Parcel in) {
    }

    public Double getAmount() {
        if (amount == null || amount.equals(""))
            return 0.000;
        else
            return Double.valueOf(amount);
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}