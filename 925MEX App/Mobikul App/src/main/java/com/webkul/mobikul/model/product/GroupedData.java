package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupedData {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isAvailable")
    @Expose
    private boolean isAvailable;
    @SerializedName("isInRange")
    @Expose
    private boolean isInRange;
    @SerializedName("specialPrice")
    @Expose
    private String specialPrice;
    @SerializedName("foramtedPrice")
    @Expose
    private String foramtedPrice;
    @SerializedName("thumbNail")
    @Expose
    private String thumbNail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public boolean getIsInRange() {
        return isInRange;
    }

    public void setIsInRange(boolean isInRange) {
        this.isInRange = isInRange;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getForamtedPrice() {
        return foramtedPrice;
    }

    public void setForamtedPrice(String foramtedPrice) {
        this.foramtedPrice = foramtedPrice;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

}
