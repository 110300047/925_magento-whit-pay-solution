package com.webkul.mobikul.model.product;

/**
 * Created by vedesh.kumar on 12/7/17. @Webkul Software Private limited
 */

public class InviteeItem {

    private String name;
    private String email;
    private int position;

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        if (email == null) {
            return "";
        }
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}