package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class LinkData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("linkTitle")
    @Expose
    private String linkTitle;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("formatedPrice")
    @Expose
    private String formatedPrice;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("fileName")
    @Expose
    private String fileName;
    @SerializedName("haveLinkSample")
    @Expose
    private int haveLinkSample;
    @SerializedName("linkSampleTitle")
    @Expose
    private String linkSampleTitle;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLinkTitle() {
        return linkTitle;
    }

    public void setLinkTitle(String linkTitle) {
        this.linkTitle = linkTitle;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getFormatedPrice() {
        return formatedPrice;
    }

    public void setFormatedPrice(String formatedPrice) {
        this.formatedPrice = formatedPrice;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getHaveLinkSample() {
        return haveLinkSample;
    }

    public void setHaveLinkSample(int haveLinkSample) {
        this.haveLinkSample = haveLinkSample;
    }

    public String getLinkSampleTitle() {
        return linkSampleTitle;
    }

    public void setLinkSampleTitle(String linkSampleTitle) {
        this.linkSampleTitle = linkSampleTitle;
    }
}
