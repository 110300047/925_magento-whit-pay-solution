package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class Links {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("linksPurchasedSeparately")
    @Expose
    private int linksPurchasedSeparately;
    @SerializedName("linkData")
    @Expose
    private List<LinkData> linkData = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLinksPurchasedSeparately() {
        return linksPurchasedSeparately;
    }

    public void setLinksPurchasedSeparately(int linksPurchasedSeparately) {
        this.linksPurchasedSeparately = linksPurchasedSeparately;
    }

    public List<LinkData> getLinkData() {
        return linkData;
    }

    public void setLinkData(List<LinkData> linkData) {
        this.linkData = linkData;
    }
}
