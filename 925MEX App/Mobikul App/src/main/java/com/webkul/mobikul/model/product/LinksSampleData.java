package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class LinksSampleData {
    @SerializedName("sampleTitle")
    @Expose
    private String sampleTitle;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("fileName")
    @Expose
    private String fileName;
    @SerializedName("mimeType")
    @Expose
    private String mimeType;

    public String getSampleTitle() {
        return sampleTitle;
    }

    public void setSampleTitle(String sampleTitle) {
        this.sampleTitle = sampleTitle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
