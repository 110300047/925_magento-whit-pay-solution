package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OldPrice implements Parcelable {

    public static final Creator<OldPrice> CREATOR = new Creator<OldPrice>() {
        @Override
        public OldPrice createFromParcel(Parcel in) {
            return new OldPrice(in);
        }

        @Override
        public OldPrice[] newArray(int size) {
            return new OldPrice[size];
        }
    };
    @SerializedName("amount")
    @Expose
    private String amount;

    protected OldPrice(Parcel in) {
        amount = in.readString();
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
    }
}