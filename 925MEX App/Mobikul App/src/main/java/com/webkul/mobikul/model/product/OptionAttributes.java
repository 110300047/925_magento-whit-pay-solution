package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class OptionAttributes implements Parcelable {
    public static final Creator<OptionAttributes> CREATOR = new Creator<OptionAttributes>() {
        @Override
        public OptionAttributes createFromParcel(Parcel in) {
            return new OptionAttributes(in);
        }

        @Override
        public OptionAttributes[] newArray(int size) {
            return new OptionAttributes[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("swatchType")
    @Expose
    private String swatchType;
    @SerializedName("updateProductPreviewImage")
    @Expose
    private boolean updateProductPreviewImage;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("options")
    @Expose
    private List<AttributeOption> options = null;
    @SerializedName("position")
    @Expose
    private String position;

    protected OptionAttributes(Parcel in) {
        id = in.readString();
        swatchType = in.readString();
        updateProductPreviewImage = in.readByte() != 0;
        code = in.readString();
        label = in.readString();
        options = in.createTypedArrayList(AttributeOption.CREATOR);
        position = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<AttributeOption> getOptions() {
        return options;
    }

    public void setOptions(List<AttributeOption> options) {
        this.options = options;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(swatchType);
        dest.writeByte((byte) (updateProductPreviewImage ? 1 : 0));
        dest.writeString(code);
        dest.writeString(label);
        dest.writeTypedList(options);
        dest.writeString(position);
    }

    public String getSwatchType() {
        return swatchType;
    }

    public void setSwatchType(String swatchType) {
        this.swatchType = swatchType;
    }

    public boolean getUpdateProductPreviewImage() {
        return updateProductPreviewImage;
    }

    public void setUpdateProductPreviewImage(boolean updateProductPreviewImage) {
        this.updateProductPreviewImage = updateProductPreviewImage;
    }
}
