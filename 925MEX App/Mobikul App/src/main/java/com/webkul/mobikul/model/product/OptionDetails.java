package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class OptionDetails {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("isQtyUserDefined")
    @Expose
    private int isQtyUserDefined;
    @SerializedName("isDefault")
    @Expose
    private String isDefault;
    @SerializedName("optionId")
    @Expose
    private String optionId;
    @SerializedName("optionValueId")
    @Expose
    private int optionValueId;
    @SerializedName("foramtedPrice")
    @Expose
    private String foramtedPrice;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("isSingle")
    @Expose
    private boolean isSingle;
    @SerializedName("defaultQty")
    @Expose
    private int defaultQty;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIsQtyUserDefined() {
        return isQtyUserDefined;
    }

    public void setIsQtyUserDefined(int isQtyUserDefined) {
        this.isQtyUserDefined = isQtyUserDefined;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public int getOptionValueId() {
        return optionValueId;
    }

    public void setOptionValueId(int optionValueId) {
        this.optionValueId = optionValueId;
    }

    public String getForamtedPrice() {
        return foramtedPrice;
    }

    public void setForamtedPrice(String foramtedPrice) {
        this.foramtedPrice = foramtedPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isIsSingle() {
        return isSingle;
    }

    public void setIsSingle(boolean isSingle) {
        this.isSingle = isSingle;
    }

    public int getDefaultQty() {
        return defaultQty;
    }

    public void setDefaultQty(int defaultQty) {
        this.defaultQty = defaultQty;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }
}
