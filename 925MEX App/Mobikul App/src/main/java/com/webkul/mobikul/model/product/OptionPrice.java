package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OptionPrice implements Parcelable {

    public static final Creator<OptionPrice> CREATOR = new Creator<OptionPrice>() {
        @Override
        public OptionPrice createFromParcel(Parcel in) {
            return new OptionPrice(in);
        }

        @Override
        public OptionPrice[] newArray(int size) {
            return new OptionPrice[size];
        }
    };
    @SerializedName("oldPrice")
    @Expose
    private OldPrice oldPrice;
    @SerializedName("basePrice")
    @Expose
    private BasePrice basePrice;
    @SerializedName("finalPrice")
    @Expose
    private FinalPrice finalPrice;
    @SerializedName("product")
    @Expose
    private int product;

    protected OptionPrice(Parcel in) {
        oldPrice = in.readParcelable(OldPrice.class.getClassLoader());
        basePrice = in.readParcelable(BasePrice.class.getClassLoader());
        finalPrice = in.readParcelable(FinalPrice.class.getClassLoader());
        product = in.readInt();
    }

    public OldPrice getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(OldPrice oldPrice) {
        this.oldPrice = oldPrice;
    }

    public BasePrice getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BasePrice basePrice) {
        this.basePrice = basePrice;
    }

    public FinalPrice getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(FinalPrice finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(oldPrice, flags);
        dest.writeParcelable(basePrice, flags);
        dest.writeParcelable(finalPrice, flags);
        dest.writeInt(product);
    }
}