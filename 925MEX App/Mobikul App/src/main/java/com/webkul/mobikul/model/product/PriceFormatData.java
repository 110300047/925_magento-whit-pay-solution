package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */
public class PriceFormatData implements Parcelable {
    public static final Creator<PriceFormatData> CREATOR = new Creator<PriceFormatData>() {
        @Override
        public PriceFormatData createFromParcel(Parcel in) {
            return new PriceFormatData(in);
        }

        @Override
        public PriceFormatData[] newArray(int size) {
            return new PriceFormatData[size];
        }
    };
    @SerializedName("pattern")
    @Expose
    private String pattern;
    @SerializedName("precision")
    @Expose
    private int precision;
    @SerializedName("requiredPrecision")
    @Expose
    private int requiredPrecision;
    @SerializedName("decimalSymbol")
    @Expose
    private String decimalSymbol;
    @SerializedName("groupSymbol")
    @Expose
    private String groupSymbol;
    @SerializedName("groupLength")
    @Expose
    private int groupLength;
    @SerializedName("integerRequired")
    @Expose
    private int integerRequired;

    protected PriceFormatData(Parcel in) {
        pattern = in.readString();
        precision = in.readInt();
        requiredPrecision = in.readInt();
        decimalSymbol = in.readString();
        groupSymbol = in.readString();
        groupLength = in.readInt();
        integerRequired = in.readInt();
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public int getRequiredPrecision() {
        return requiredPrecision;
    }

    public void setRequiredPrecision(int requiredPrecision) {
        this.requiredPrecision = requiredPrecision;
    }

    public String getDecimalSymbol() {
        return decimalSymbol;
    }

    public void setDecimalSymbol(String decimalSymbol) {
        this.decimalSymbol = decimalSymbol;
    }

    public String getGroupSymbol() {
        return groupSymbol;
    }

    public void setGroupSymbol(String groupSymbol) {
        this.groupSymbol = groupSymbol;
    }

    public int getGroupLength() {
        return groupLength;
    }

    public void setGroupLength(int groupLength) {
        this.groupLength = groupLength;
    }

    public int getIntegerRequired() {
        return integerRequired;
    }

    public void setIntegerRequired(int integerRequired) {
        this.integerRequired = integerRequired;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(pattern);
        parcel.writeInt(precision);
        parcel.writeInt(requiredPrecision);
        parcel.writeString(decimalSymbol);
        parcel.writeString(groupSymbol);
        parcel.writeInt(groupLength);
        parcel.writeInt(integerRequired);
    }
}
