package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Prices {

    @SerializedName("oldPrice")
    @Expose
    private OldPrice oldPrice;
    @SerializedName("basePrice")
    @Expose
    private BasePrice basePrice;
    @SerializedName("finalPrice")
    @Expose
    private FinalPrice finalPrice;

    public OldPrice getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(OldPrice oldPrice) {
        this.oldPrice = oldPrice;
    }

    public BasePrice getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BasePrice basePrice) {
        this.basePrice = basePrice;
    }

    public FinalPrice getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(FinalPrice finalPrice) {
        this.finalPrice = finalPrice;
    }

}