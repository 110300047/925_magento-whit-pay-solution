package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 29/12/16. @Webkul Software Pvt. Ltd
 */

@SuppressWarnings("unused")
public class ProductAddToCartResponse extends BaseModel {

    @SerializedName("quoteId")
    @Expose
    private int quoteId;

    public int getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(int quoteId) {
        this.quoteId = quoteId;
    }
}