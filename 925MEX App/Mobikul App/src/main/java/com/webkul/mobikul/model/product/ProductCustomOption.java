package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 17/1/17.
 */
public class ProductCustomOption {

    @SerializedName("optionValues")
    @Expose
    public List<ProductCustomOptionValues> optionValues = null;
    @SerializedName("option_id")
    @Expose
    private String optionId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("is_require")
    @Expose
    private int isRequire;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("max_characters")
    @Expose
    private String maxCharacters;
    @SerializedName("file_extension")
    @Expose
    private String fileExtension;
    @SerializedName("image_size_x")
    @Expose
    private String imageSizeX;
    @SerializedName("image_size_y")
    @Expose
    private String imageSizeY;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("default_title")
    @Expose
    private String defaultTitle;
    @SerializedName("store_title")
    @Expose
    private String storeTitle;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("default_price")
    @Expose
    private double defaultPrice;
    @SerializedName("default_price_type")
    @Expose
    private String defaultPriceType;
    @SerializedName("store_price")
    @Expose
    private double storePrice;
    @SerializedName("store_price_type")
    @Expose
    private String storePriceType;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("price_type")
    @Expose
    private String priceType;
    @SerializedName("decorated_is_first")
    @Expose
    private Boolean decoratedIsFirst;
    @SerializedName("decorated_is_odd")
    @Expose
    private Boolean decoratedIsOdd;
    @SerializedName("decorated_is_last")
    @Expose
    private Boolean decoratedIsLast;
    @SerializedName("unformated_default_price")
    @Expose
    private double unformatedDefaultPrice;
    @SerializedName("formated_default_price")
    @Expose
    private String formatedDefaultPrice;
    @SerializedName("unformated_price")
    @Expose
    private double unformatedPrice;
    @SerializedName("formated_price")
    @Expose
    private String formatedPrice;
    @SerializedName("decorated_is_even")
    @Expose
    private Boolean decoratedIsEven;
    @SerializedName("slot_duration")
    @Expose
    private String slotDuration;
    @SerializedName("break_time_bw_slot")
    @Expose
    private String breakTimeBwSlot;
    @SerializedName("prevent_scheduling_before")
    @Expose
    private String preventSchedulingBefore;
    @SerializedName("booking_slot_info")
    @Expose
    private List<List<String[]>> bookingSlotInfo=new ArrayList<>();

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIsRequire() {
        return isRequire;
    }

    public void setIsRequire(int isRequire) {
        this.isRequire = isRequire;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getMaxCharacters() {
        return maxCharacters;
    }

    public void setMaxCharacters(String maxCharacters) {
        this.maxCharacters = maxCharacters;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getImageSizeX() {
        return imageSizeX;
    }

    public void setImageSizeX(String imageSizeX) {
        this.imageSizeX = imageSizeX;
    }

    public String getImageSizeY() {
        return imageSizeY;
    }

    public void setImageSizeY(String imageSizeY) {
        this.imageSizeY = imageSizeY;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getDefaultTitle() {
        return defaultTitle;
    }

    public void setDefaultTitle(String defaultTitle) {
        this.defaultTitle = defaultTitle;
    }

    public String getStoreTitle() {
        return storeTitle;
    }

    public void setStoreTitle(String storeTitle) {
        this.storeTitle = storeTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public String getDefaultPriceType() {
        return defaultPriceType;
    }

    public void setDefaultPriceType(String defaultPriceType) {
        this.defaultPriceType = defaultPriceType;
    }

    public double getStorePrice() {
        return storePrice;
    }

    public void setStorePrice(double storePrice) {
        this.storePrice = storePrice;
    }

    public String getStorePriceType() {
        return storePriceType;
    }

    public void setStorePriceType(String storePriceType) {
        this.storePriceType = storePriceType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public Boolean getDecoratedIsFirst() {
        return decoratedIsFirst;
    }

    public void setDecoratedIsFirst(Boolean decoratedIsFirst) {
        this.decoratedIsFirst = decoratedIsFirst;
    }

    public Boolean getDecoratedIsOdd() {
        return decoratedIsOdd;
    }

    public void setDecoratedIsOdd(Boolean decoratedIsOdd) {
        this.decoratedIsOdd = decoratedIsOdd;
    }

    public Boolean getDecoratedIsLast() {
        return decoratedIsLast;
    }

    public void setDecoratedIsLast(Boolean decoratedIsLast) {
        this.decoratedIsLast = decoratedIsLast;
    }

    public double getUnformatedDefaultPrice() {
        return unformatedDefaultPrice;
    }

    public void setUnformatedDefaultPrice(double unformatedDefaultPrice) {
        this.unformatedDefaultPrice = unformatedDefaultPrice;
    }

    public String getFormatedDefaultPrice() {
        return formatedDefaultPrice;
    }

    public void setFormatedDefaultPrice(String formatedDefaultPrice) {
        this.formatedDefaultPrice = formatedDefaultPrice;
    }

    public double getUnformatedPrice() {
        return unformatedPrice;
    }

    public void setUnformatedPrice(double unformatedPrice) {
        this.unformatedPrice = unformatedPrice;
    }

    public String getFormatedPrice() {
        return formatedPrice;
    }

    public void setFormatedPrice(String formatedPrice) {
        this.formatedPrice = formatedPrice;
    }

    public List<ProductCustomOptionValues> getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(List<ProductCustomOptionValues> optionValues) {
        this.optionValues = optionValues;
    }

    public Boolean getDecoratedIsEven() {
        return decoratedIsEven;
    }

    public void setDecoratedIsEven(Boolean decoratedIsEven) {
        this.decoratedIsEven = decoratedIsEven;
    }

    public String getSlotDuration() {
        return slotDuration;
    }

    public void setSlotDuration(String slotDuration) {
        this.slotDuration = slotDuration;
    }

    public String getBreakTimeBwSlot() {
        return breakTimeBwSlot;
    }

    public void setBreakTimeBwSlot(String breakTimeBwSlot) {
        this.breakTimeBwSlot = breakTimeBwSlot;
    }

    public String getPreventSchedulingBefore() {
        return preventSchedulingBefore;
    }

    public void setPreventSchedulingBefore(String preventSchedulingBefore) {
        this.preventSchedulingBefore = preventSchedulingBefore;
    }

    public List<List<String[]>>  getBookingSlotInfo() {
        return bookingSlotInfo;
    }

    public void setBookingSlotInfo(List<List<String[]>> bookingSlotInfo) {
        this.bookingSlotInfo = bookingSlotInfo;
    }
}
