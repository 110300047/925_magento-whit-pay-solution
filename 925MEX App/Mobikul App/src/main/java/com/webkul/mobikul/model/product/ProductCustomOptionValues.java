package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 18/1/17.
 */
public class ProductCustomOptionValues {


    @SerializedName("option_type_id")
    @Expose
    private String optionTypeId;
    @SerializedName("option_id")
    @Expose
    private String optionId;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("default_title")
    @Expose
    private String defaultTitle;
    @SerializedName("store_title")
    @Expose
    private String storeTitle;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("default_price")
    @Expose
    private double defaultPrice;
    @SerializedName("default_price_type")
    @Expose
    private String defaultPriceType;
    @SerializedName("store_price")
    @Expose
    private double storePrice;
    @SerializedName("store_price_type")
    @Expose
    private String storePriceType;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("price_type")
    @Expose
    private String priceType;
    @SerializedName("formated_price")
    @Expose
    private String formatedPrice;
    @SerializedName("formated_default_price")
    @Expose
    private String formatedDefaultPrice;

    public String getOptionTypeId() {
        return optionTypeId;
    }

    public void setOptionTypeId(String optionTypeId) {
        this.optionTypeId = optionTypeId;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getDefaultTitle() {
        return defaultTitle;
    }

    public void setDefaultTitle(String defaultTitle) {
        this.defaultTitle = defaultTitle;
    }

    public String getStoreTitle() {
        return storeTitle;
    }

    public void setStoreTitle(String storeTitle) {
        this.storeTitle = storeTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getDefaultPrice() {
        return defaultPrice;
    }

    public void setDefaultPrice(double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public String getDefaultPriceType() {
        return defaultPriceType;
    }

    public void setDefaultPriceType(String defaultPriceType) {
        this.defaultPriceType = defaultPriceType;
    }

    public double getStorePrice() {
        return storePrice;
    }

    public void setStorePrice(double storePrice) {
        this.storePrice = storePrice;
    }

    public String getStorePriceType() {
        return storePriceType;
    }

    public void setStorePriceType(String storePriceType) {
        this.storePriceType = storePriceType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getFormatedPrice() {
        return formatedPrice;
    }

    public void setFormatedPrice(String formatedPrice) {
        this.formatedPrice = formatedPrice;
    }

    public String getFormatedDefaultPrice() {
        return formatedDefaultPrice;
    }

    public void setFormatedDefaultPrice(String formatedDefaultPrice) {
        this.formatedDefaultPrice = formatedDefaultPrice;
    }

}
