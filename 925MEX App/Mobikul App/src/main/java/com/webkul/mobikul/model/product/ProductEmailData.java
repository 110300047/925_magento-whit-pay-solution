package com.webkul.mobikul.model.product;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 12/7/17. @Webkul Software Private limited
 */

public class ProductEmailData {
    private String name;
    private String email;
    private String message;

    private ArrayList<InviteeItem> inviteeItemsList = new ArrayList<>();

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        if (email == null) {
            return "";
        }
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        if (message == null) {
            return "";
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<InviteeItem> getInviteeItemsList() {
        return inviteeItemsList;
    }

    public void setInviteeItemsList(ArrayList<InviteeItem> inviteeItemsList) {
        this.inviteeItemsList = inviteeItemsList;
    }
}
