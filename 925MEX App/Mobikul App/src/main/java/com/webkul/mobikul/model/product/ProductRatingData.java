package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */
public class ProductRatingData implements Parcelable {

    public static final Creator<ProductRatingData> CREATOR = new Creator<ProductRatingData>() {
        @Override
        public ProductRatingData createFromParcel(Parcel in) {
            return new ProductRatingData(in);
        }

        @Override
        public ProductRatingData[] newArray(int size) {
            return new ProductRatingData[size];
        }
    };
    @SerializedName(value = "ratingCode", alternate = {"label"})
    @Expose
    private String ratingCode;
    @SerializedName(value = "ratingValue", alternate = {"value"})
    @Expose
    private float ratingValue;

    protected ProductRatingData(Parcel in) {
        ratingCode = in.readString();
        ratingValue = in.readFloat();
    }

    public String getRatingCode() {
        return ratingCode;
    }

    public void setRatingCode(String ratingCode) {
        this.ratingCode = ratingCode;
    }

    public float getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(float ratingValue) {
        this.ratingValue = ratingValue;
    }

    public float getRating() {
        if (ratingValue > 5) {
            return ratingValue / 20;
        } else {
            return ratingValue;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ratingCode);
        dest.writeFloat(ratingValue);
    }
}
