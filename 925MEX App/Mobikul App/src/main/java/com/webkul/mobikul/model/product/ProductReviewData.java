package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 3/1/17. @Webkul Software Pvt. Ltd
 */
public class ProductReviewData implements Parcelable {
    public static final Creator<ProductReviewData> CREATOR = new Creator<ProductReviewData>() {
        @Override
        public ProductReviewData createFromParcel(Parcel in) {
            return new ProductReviewData(in);
        }

        @Override
        public ProductReviewData[] newArray(int size) {
            return new ProductReviewData[size];
        }
    };
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("ratings")
    @Expose
    private List<ProductRatingData> ratings = null;
    @SerializedName("reviewBy")
    @Expose
    private String reviewBy;
    @SerializedName("reviewOn")
    @Expose
    private String reviewOn;

    protected ProductReviewData(Parcel in) {
        title = in.readString();
        details = in.readString();
        ratings = in.createTypedArrayList(ProductRatingData.CREATOR);
        reviewBy = in.readString();
        reviewOn = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<ProductRatingData> getRatings() {
        return ratings;
    }

    public void setRatings(List<ProductRatingData> ratings) {
        this.ratings = ratings;
    }

    public String getReviewBy() {
        return reviewBy;
    }

    public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
    }

    public String getReviewOn() {
        return reviewOn;
    }

    public void setReviewOn(String reviewOn) {
        this.reviewOn = reviewOn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(details);
        dest.writeTypedList(ratings);
        dest.writeString(reviewBy);
        dest.writeString(reviewOn);
    }
}
