package com.webkul.mobikul.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 31/1/17. @Webkul Software Pvt. Ltd
 */
public class RatingFormData implements Parcelable {

    public static final Creator<RatingFormData> CREATOR = new Creator<RatingFormData>() {
        @Override
        public RatingFormData createFromParcel(Parcel in) {
            return new RatingFormData(in);
        }

        @Override
        public RatingFormData[] newArray(int size) {
            return new RatingFormData[size];
        }
    };
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("values")
    @Expose
    private List<String> values = null;
    private float ratingValue;

    protected RatingFormData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        values = in.createStringArrayList();
        ratingValue = in.readFloat();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public float getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(float ratingValue) {
        this.ratingValue = ratingValue;
    }

    public String getSelectedRatingValue() {
        return getValues().get((int) (getRatingValue() - 1));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeStringList(values);
        dest.writeFloat(ratingValue);
    }
}
