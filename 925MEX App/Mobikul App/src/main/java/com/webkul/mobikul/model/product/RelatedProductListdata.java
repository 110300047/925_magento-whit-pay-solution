package com.webkul.mobikul.model.product;

import android.media.Rating;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 26/12/16. @Webkul Software Pvt. Ltd
 */
public class RelatedProductListdata {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("ratings")
    @Expose
    private List<Rating> ratings = null;
    @SerializedName("reviewBy")
    @Expose
    private String reviewBy;
    @SerializedName("reviewOn")
    @Expose
    private String reviewOn;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public String getReviewBy() {
        return reviewBy;
    }

    public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
    }

    public String getReviewOn() {
        return reviewOn;
    }

    public void setReviewOn(String reviewOn) {
        this.reviewOn = reviewOn;
    }
}
