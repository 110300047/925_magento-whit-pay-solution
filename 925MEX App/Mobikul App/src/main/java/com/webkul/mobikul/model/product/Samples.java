package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class Samples {

    @SerializedName("hasSample")
    @Expose
    private boolean hasSample;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("linkSampleData")
    @Expose
    private List<LinksSampleData> linkSampleData = null;

    public boolean isHasSample() {
        return hasSample;
    }

    public void setHasSample(boolean hasSample) {
        this.hasSample = hasSample;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<LinksSampleData> getLinkSampleData() {
        return linkSampleData;
    }

    public void setLinkSampleData(List<LinksSampleData> linkSampleData) {
        this.linkSampleData = linkSampleData;
    }
}
