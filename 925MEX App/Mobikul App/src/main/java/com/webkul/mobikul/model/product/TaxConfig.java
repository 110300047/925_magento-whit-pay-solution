package com.webkul.mobikul.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 3/2/17. @Webkul Software Private limited
 */

public class TaxConfig {
    @SerializedName("includeTax")
    @Expose
    private boolean includeTax;
    @SerializedName("showIncludeTax")
    @Expose
    private boolean showIncludeTax;
    @SerializedName("showBothPrices")
    @Expose
    private boolean showBothPrices;
    @SerializedName("defaultTax")
    @Expose
    private Double defaultTax;
    @SerializedName("currentTax")
    @Expose
    private Double currentTax;
    @SerializedName("inclTaxTitle")
    @Expose
    private String inclTaxTitle;

    public boolean isIncludeTax() {
        return includeTax;
    }

    public void setIncludeTax(boolean includeTax) {
        this.includeTax = includeTax;
    }

    public boolean isShowIncludeTax() {
        return showIncludeTax;
    }

    public void setShowIncludeTax(boolean showIncludeTax) {
        this.showIncludeTax = showIncludeTax;
    }

    public boolean isShowBothPrices() {
        return showBothPrices;
    }

    public void setShowBothPrices(boolean showBothPrices) {
        this.showBothPrices = showBothPrices;
    }

    public Double getDefaultTax() {
        return defaultTax;
    }

    public void setDefaultTax(Double defaultTax) {
        this.defaultTax = defaultTax;
    }

    public Double getCurrentTax() {
        return currentTax;
    }

    public void setCurrentTax(Double currentTax) {
        this.currentTax = currentTax;
    }

    public String getInclTaxTitle() {
        return inclTaxTitle;
    }

    public void setInclTaxTitle(String inclTaxTitle) {
        this.inclTaxTitle = inclTaxTitle;
    }
}
