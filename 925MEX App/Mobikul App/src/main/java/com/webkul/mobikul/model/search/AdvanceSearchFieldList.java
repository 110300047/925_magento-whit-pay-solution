package com.webkul.mobikul.model.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdvanceSearchFieldList {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("inputType")
    @Expose
    private String inputType;
    @SerializedName("attributeCode")
    @Expose
    private String attributeCode;
    @SerializedName("maxQueryLength")
    @Expose
    private String maxQueryLength;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("options")
    @Expose
    private List<AdvanceSearchFieldOption> options = null;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }

    public String getMaxQueryLength() {
        return maxQueryLength;
    }

    public void setMaxQueryLength(String maxQueryLength) {
        this.maxQueryLength = maxQueryLength;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AdvanceSearchFieldOption> getOptions() {
        return options;
    }

    public void setOptions(List<AdvanceSearchFieldOption> options) {
        this.options = options;
    }

}
