package com.webkul.mobikul.model.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

public class AdvanceSearchFormData extends BaseModel {

    @SerializedName("fieldList")
    @Expose
    private List<AdvanceSearchFieldList> fieldList = null;

    public List<AdvanceSearchFieldList> getFieldList() {
        return fieldList;
    }

    public void setFieldList(List<AdvanceSearchFieldList> fieldList) {
        this.fieldList = fieldList;
    }

}
