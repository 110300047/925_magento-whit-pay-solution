package com.webkul.mobikul.model.search;

/**
 * Created by vedesh.kumar on 31/1/17. @Webkul Software Private limited
 */

public class NavDrawerStartSearchMenuData {

    public static final int SEARCH_TERMS = 0;
    public static final int ADVANCE_SEARCH = 1;

    private String name;
    private int drawableId;
    private int type;

    public NavDrawerStartSearchMenuData(String name, int drawableId, int type) {
        this.name = name;
        this.drawableId = drawableId;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public int getType() {
        return type;
    }

    public void setType(int mType) {
        this.type = mType;
    }
}
