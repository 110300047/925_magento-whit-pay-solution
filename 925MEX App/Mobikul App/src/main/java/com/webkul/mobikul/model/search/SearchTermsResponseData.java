package com.webkul.mobikul.model.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Private limited
 */

public class SearchTermsResponseData extends BaseModel {

    @SerializedName("termList")
    @Expose
    private List<TermList> termList = null;

    public List<TermList> getTermList() {
        return termList;
    }

    public void setTermList(List<TermList> termList) {
        this.termList = termList;
    }
}