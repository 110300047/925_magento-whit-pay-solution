package com.webkul.mobikul.model.search;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.constants.ApplicationConstant;

/**
 * Created by vedesh.kumar on 25/2/17. @Webkul Software Private limited
 */

public class TermList {

    @SerializedName("ratio")
    @Expose
    private float ratio;
    @SerializedName("term")
    @Expose
    private String term;

    public float getRatio() {
        return ratio;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public float getTextSize() {
        float size = ratio / 2.5f;
        Log.d(ApplicationConstant.TAG, "getTextSize: " + size);
        size = (size < 25) ? 25 : size;
        return size;
    }
}