package com.webkul.mobikul.widget;

import android.content.Intent;
import android.os.IBinder;
import android.widget.RemoteViewsService;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_WIDGET_DATA;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_WIDGET_TYPE;


/**
 * Created by vedesh.kumar on 16/08/17. @Webkul Software Private limited
 */

public class AppWidgetService extends RemoteViewsService {
    public AppWidgetService() {
        super();
    }

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        switch (intent.getStringExtra(BUNDLE_KEY_WIDGET_TYPE)) {
            case "offers":
                return new OffersWidgetRemoteViewFactory(this.getApplicationContext(), intent, intent.getStringArrayListExtra(BUNDLE_KEY_WIDGET_DATA));
        }
        return null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }
}