package com.webkul.mobikul.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.SplashScreenActivity;
import com.webkul.mobikul.connection.ApiConnection;
import com.webkul.mobikul.connection.CustomSubscriber;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.notification.NotificationList;
import com.webkul.mobikul.model.notification.NotificationResponseData;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.appwidget.AppWidgetManager.ACTION_APPWIDGET_UPDATE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_WIDGET_DATA;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_WIDGET_TYPE;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 14/08/17. @Webkul Software Private limited
 */

public class OffersWidget extends AppWidgetProvider {

    private ArrayList<String> mNotificationBannerUrls;

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equals(ACTION_APPWIDGET_UPDATE)) {
            callApi(context);
        }
    }

    private void callApi(final Context context) {
        ApiConnection.getNotificationsList(AppSharedPref.getStoreId(context)
                , Utils.getScreenWidth()
                , context.getResources().getDisplayMetrics().density)

                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<NotificationResponseData>(context) {
            @Override
            public void onNext(NotificationResponseData notificationResponseData) {
                super.onNext(notificationResponseData);
                if (notificationResponseData.getSuccess()) {
                    mNotificationBannerUrls = new ArrayList<>();
                    for (NotificationList notificationList : notificationResponseData.getNotificationList()) {
                        mNotificationBannerUrls.add(notificationList.getBanner());
                    }
                    updateWidget(context);
                }
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                showToast(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT, 0);
            }
        });
    }

    public void updateWidget(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, OffersWidget.class));
        if (appWidgetIds.length > 0) {
            onUpdate(context, appWidgetManager, appWidgetIds);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;
        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];
            // Get the layout for the App Widget and attach an on-click listener
            // to the button
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.offers_widget);

            Intent updateIntent = new Intent(context, getClass());
            updateIntent.setAction(ACTION_APPWIDGET_UPDATE);
            PendingIntent updatePendingIntent = PendingIntent.getBroadcast(context, 0, updateIntent, 0);

            remoteViews.setRemoteAdapter(R.id.refresh, updateIntent);
            remoteViews.setOnClickPendingIntent(R.id.refresh, updatePendingIntent);

            //Adding data to the RemoteViews
            if (mNotificationBannerUrls != null && mNotificationBannerUrls.size() > 0) {
                remoteViews.setViewVisibility(R.id.empty_view, View.GONE);

                Intent serviceIntent = new Intent(context, AppWidgetService.class);
                serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
                serviceIntent.putExtra(BUNDLE_KEY_WIDGET_DATA, mNotificationBannerUrls);
                serviceIntent.putExtra(BUNDLE_KEY_WIDGET_TYPE, "offers");
                remoteViews.setRemoteAdapter(R.id.offers_view_flipper, serviceIntent);
            } else {
                remoteViews.setViewVisibility(R.id.empty_view, View.VISIBLE);

                Intent splashScreenIntent = new Intent(context, SplashScreenActivity.class);
                PendingIntent splashScreenPendingIntent = PendingIntent.getActivity(context, 0, splashScreenIntent, 0);
                remoteViews.setRemoteAdapter(R.id.empty_view, splashScreenIntent);
                remoteViews.setOnClickPendingIntent(R.id.empty_view, splashScreenPendingIntent);
            }

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        }
    }
}