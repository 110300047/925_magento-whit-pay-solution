package com.webkul.mobikul.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.webkul.mobikul.R;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by vedesh.kumar on 16/08/17. @Webkul Software Private limited
 */

public class OffersWidgetRemoteViewFactory implements RemoteViewsService.RemoteViewsFactory {
    private Context mContext;
    private int mWidgetId;
    private ArrayList<String> mBannerUrl;

    public OffersWidgetRemoteViewFactory(Context context, Intent intent, ArrayList<String> bannerUrl) {
        mContext = context;
        mWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        mBannerUrl = bannerUrl;
    }

    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {
        mBannerUrl.clear();
    }

    @Override
    public int getCount() {
        return mBannerUrl.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(), R.layout.app_widget_item_notification);
        try {
            Bitmap bitmap = Glide.with(mContext)
                    .asBitmap()
                    .load(mBannerUrl.get(position))
                    .submit()
                    .get();
            remoteViews.setImageViewBitmap(R.id.notification_banner_iv, bitmap);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return remoteViews;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return mBannerUrl.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}