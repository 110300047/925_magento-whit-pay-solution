package com.webkul.mobikul.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.webkul.mobikul.R;
import com.webkul.mobikul.activity.HomeActivity;

import static android.content.Intent.ACTION_SEARCH;
import static com.webkul.mobikul.R.id.search;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ACTION;

public class SearchWidgetProvider extends AppWidgetProvider {

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;
        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];
            // Create an Intent to launch ExampleActivity
            Intent intent = new Intent(context, HomeActivity.class);
            intent.putExtra(BUNDLE_KEY_ACTION, ACTION_SEARCH);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            // Get the layout for the App Widget and attach an on-click listener
            // to the button
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.search_widget);
            views.setOnClickPendingIntent(search, pendingIntent);

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }
}