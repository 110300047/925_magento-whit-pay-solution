<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import
            alias="View"
            type="android.view.View" />

        <variable
            name="data"
            type="com.webkul.mobikul.model.catalog.ProductData" />

        <variable
            name="handler"
            type="com.webkul.mobikul.handler.CompareProductListRvHandler" />
    </data>

    <RelativeLayout
        android:layout_width="@dimen/product_grid_view_width"
        android:layout_height="match_parent"
        android:background="@drawable/shape_rectangular_white_bg_gray_border_half_dp"
        android:foreground="?attr/selectableItemBackground"
        android:onClick="@{(v) -> handler.onClickItem(v, data.entityId, data.name, true, data.productPosition)}">

        <RelativeLayout
            android:id="@+id/main_container"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:padding="10dp">

            <ImageView
                android:id="@+id/product_image"
                android:layout_width="@dimen/product_image_height"
                android:layout_height="@dimen/product_image_height"
                android:contentDescription="image"
                android:scaleType="fitXY"
                app:imageUrl="@{data.thumbNail}"
                tools:background="?attr/colorAccent"
                tools:layout_height="@dimen/product_image_height"
                tools:layout_width="@dimen/product_image_height"
                tools:src="@drawable/splash_screen" />

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_alignEnd="@id/product_image"
                android:layout_alignLeft="@id/product_image"
                android:layout_alignRight="@id/product_image"
                android:layout_alignStart="@id/product_image"
                android:layout_below="@id/product_image"
                android:gravity="center"
                android:orientation="vertical"
                android:padding="5dp">

                <TextView
                    android:id="@+id/product_name"
                    android:layout_width="wrap_content"
                    android:layout_height="@dimen/item_home_product_grid_name_height"
                    android:ellipsize="end"
                    android:gravity="center"
                    android:maxLength="30"
                    android:maxLines="2"
                    android:text="@{data.name}"
                    android:textSize="@dimen/text_size_small"
                    tools:text="productNameproductNameproductNameproductNameproductNameproductNameproductNameproductName" />

                <RatingBar
                    android:id="@+id/ratingBar"
                    style="?android:attr/ratingBarStyleSmall"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/spacing_tiny"
                    android:rating="@{data.rating}"
                    android:theme="@style/RatingBar"
                    tools:rating="2"
                    tools:visibility="visible" />

                <LinearLayout
                    android:id="@+id/price_ll"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:id="@+id/product_price_tv"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:background="@{(data.hasSpecialPrice()) ? @drawable/bg_strikethrough : null }"
                        android:gravity="center"
                        android:maxLines="1"
                        android:text="@{(data.hasSpecialPrice()) ? data.formatedPrice : data.formatedFinalPrice}"
                        android:textColor="@{data.hasSpecialPrice() ? @color/grey_400 : @color/text_color_primary}"
                        android:textSize="@dimen/text_size_small"
                        android:visibility="@{data.hasPrice() ? View.VISIBLE : View.GONE}"
                        tools:text="$310.00" />

                    <TextView
                        android:id="@+id/product_special_price_tv"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:gravity="center"
                        android:maxLines="1"
                        android:text="@{data.formatedSpecialPrice}"
                        android:textColor="@color/text_color_primary"
                        android:textSize="@dimen/text_size_small"
                        android:visibility="@{data.hasSpecialPrice() ? View.VISIBLE : View.GONE}"
                        tools:text="$210.00" />

                    <TextView
                        android:id="@+id/product_min_price_tv"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:gravity="center"
                        android:maxLines="1"
                        android:text="@{data.formatedMinPrice}"
                        android:textColor="@color/text_color_primary"
                        android:textSize="@dimen/text_size_small"
                        android:visibility="@{data.hasMinPrice() ? View.VISIBLE : View.GONE}"
                        tools:text="$410.00" />

                    <TextView
                        android:id="@+id/product_min_max_price_dash_tv"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_weight="0.2"
                        android:gravity="center"
                        android:text="-"
                        android:textColor="@color/text_color_primary"
                        android:textSize="@dimen/text_size_small"
                        android:visibility="@{data.hasMinPrice() ? View.VISIBLE : View.GONE}"
                        tools:text="-" />

                    <TextView
                        android:id="@+id/product_max_price_tv"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:gravity="center"
                        android:maxLines="1"
                        android:text="@{data.formatedMaxPrice}"
                        android:textColor="@color/text_color_primary"
                        android:textSize="@dimen/text_size_small"
                        android:visibility="@{data.hasMinPrice() ? View.VISIBLE : View.GONE}"
                        tools:text="$410.00" />

                    <TextView
                        android:id="@+id/product_grp_price_tv"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_weight="1"
                        android:gravity="center"
                        android:maxLines="1"
                        android:text="@{data.groupedPrice}"
                        android:textColor="@color/text_color_primary"
                        android:textSize="@dimen/text_size_small"
                        android:visibility="@{data.hasGroupedPrice() ? View.VISIBLE : View.GONE}"
                        tools:text="$410.00" />

                </LinearLayout>

                <Button
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center"
                    android:layout_marginTop="@dimen/spacing_generic"
                    android:background="@drawable/shape_rounded_corners_white_bg_accent_border_1dp"
                    android:onClick="@{(v) -> handler.onClickItem(v,  data.entityId, data.name, (data.hasOptions == 1), data.productPosition)}"
                    android:paddingEnd="@dimen/spacing_small"
                    android:paddingLeft="@dimen/spacing_small"
                    android:paddingRight="@dimen/spacing_small"
                    android:paddingStart="@dimen/spacing_small"
                    android:text="@{(data.hasOptions == 1) ? @string/view_product : @string/add_to_cart }"
                    android:textAppearance="?android:attr/textAppearanceSmall"
                    android:textColor="@color/button_background_color"
                    android:textSize="@dimen/text_size_small"
                    android:visibility="@{data.isAvailable ? View.VISIBLE : View.GONE}"
                    tools:text="Add to cart" />

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/spacing_small"
                    android:maxLines="2"
                    android:text="@string/out_of_stock"
                    android:textAllCaps="true"
                    android:textColor="@color/text_color_special"
                    android:textSize="@dimen/text_size_small"
                    android:visibility="@{data.isAvailable ? View.GONE : View.VISIBLE}"
                    tools:text="OUT OF STOCK" />

            </LinearLayout>

        </RelativeLayout>

        <com.airbnb.lottie.LottieAnimationView
            android:id="@+id/add_to_wishlist_iv"
            android:layout_width="@dimen/wishlist_icon_size"
            android:layout_height="@dimen/wishlist_icon_size"
            android:layout_margin="@dimen/spacing_tiny"
            android:foreground="?attr/selectableItemBackground"
            android:onClick="@{(v) -> handler.onClickAddToWishlist(v, data.entityId, data.name, data.productPosition, data.wishlistItemId)}"
            app:lottie_fileName="wishlist_heart.json"
            app:lottie_loop="false" />

        <ImageView
            android:layout_width="@dimen/wishlist_icon_size"
            android:layout_height="@dimen/wishlist_icon_size"
            android:layout_alignEnd="@id/main_container"
            android:layout_alignRight="@id/main_container"
            android:layout_margin="@dimen/spacing_tiny"
            android:layout_marginTop="2dp"
            android:foreground="?attr/selectableItemBackground"
            android:onClick="@{(v)->handler.onClickDeleteItemBtn(v, data.entityId, data.name)}"
            app:srcCompat="@drawable/ic_vector_close_bg" />

    </RelativeLayout>

</layout>