package com.webkul.mobikul.constants;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */

public interface ApplicationConstant {

    /* SERVER CONFIGURATION */
    String HOST_NAME = "http://puertomorelos.ml";

    String BASE_URL = HOST_NAME + "/index.php/";
    String API_USER_NAME = "mobikul";
    String API_PASSWORD = "mobikul123";

    /* APP STORE URL */
    String APP_PLAYSTORE_URL = "https://play.google.com/store/apps/details?id=com.925mex.android";

    String[] DEFAULT_FCM_TOPICS = {"925mex_android"};

    /* ALLOWED PAYMENT METHODS */
    String PAYMENT_CODE_COD = "cashondelivery";
    String PAYMENT_CODE_BANK_TRANSFER = "banktransfer";
    String PAYMENT_CODE_CHECK_MONEY_ORDER = "checkmo";
    String PAYMENT_CODE_OPENPAY_CARDS = "openpay_cards";
    String PAYMENT_CODE_OPENPAY_STORES = "openpay_stores";
    String PAYMENT_CODE_PAYPAL = "paypal_express";

    //String[] AVAILABLE_PAYMENT_METHOD = {PAYMENT_CODE_COD, PAYMENT_CODE_BANK_TRANSFER, PAYMENT_CODE_CHECK_MONEY_ORDER, PAYMENT_CODE_OPENPAY_CARDS, PAYMENT_CODE_PAYPAL};
    String[] AVAILABLE_PAYMENT_METHOD = {PAYMENT_CODE_PAYPAL};

    /* DEMO USERNAME AND PASSWORD */
    String DEMO_USERNAME = "";
    String DEMO_PASSWORD = "";

    /* FEATURES SETTINGS */
    boolean CAN_DISPLAY_STORES = true;
    boolean CAN_DISPLAY_CURRENCY = true;

    /* MARKET PLACE RELATED CONSTANTS */
    @SuppressWarnings("RedundantFieldInitialization")
    boolean IS_MARKETPLACE_APP = true;

    /* ENABLE API LOGGING WITH 1 */
    int LOG_PARAMS = 1;
    int LOG_RESPONSE = 0;

    /* UNIVERSAL TAG */
    String TAG = "DEBUG";

    /* GOOGLE MAPS API FOR ADDRESS FROM LAT LONG*/
    String GOOGLE_MAP_BASE_URL = "https://maps.googleapis.com/maps/api/geocode/";

    /* FONT PATH */
    String CALLIGRAPHY_FONT_PATH = "fonts/NotoKufiArabic-Regular.ttf";

    /*  DEFAULT CONSTANTS */
    String DEFAULT_CURRENCY_CODE = "";
    String DEFAULT_STORE_CODE = "en";
    int DEFAULT_CUSTOMER_ID = 0;
    int DEFAULT_STORE_ID = 0;
    int DEFAULT_WEBSITE_ID = 1;
    int DEFAULT_QUOTE_ID = 0;
    int DEFAULT_PAGE_COUNT = 1;
    int DEFAULT_RECTANGLE_RADIUS = 10;
    int NUMBER_OF_MOBILE_NUMBER_DIGIT = 14;
    int DEFAULT_BACK_PRESSED_TIME_TO_CLOSE = 2000;
    int DEFAULT_TIME_TO_SWITCH_BANNER_IN_MILIS = 5 * 1000;
    int DEFAULT_MAX_QTY = 99999;

    /* START ACTIVITY FOR RESULT CONSTANTS */
    int RC_APP_SIGN_IN = 100;
    int RC_APP_SIGN_UP = 101;
    int RC_GOOGLE_SIGN_IN = 102;
    int RC_LINKEDIN_SIGN_IN = 103;
    int RC_FACEBOOK_SIGN_IN = 104;
    int RC_PICK_IMAGE_FROM_GALLERY = 105;
    int RC_PICK_IMAGE_FROM_CAMERA = 106;

    /* CATALOG PRODUCT ACTIVITY */
    int REQUEST_TYPE_SEARCH_PRODUCT_COLLECTION = 1;
    int REQUEST_TYPE_CATEGORY_COLLECTION = 2;
    int REQUEST_TYPE_ADVANCE_SEARCH_COLLECTION = 3;
    int REQUEST_TYPE_CUSTOM_COLLECTION = 4;
    int REQUEST_TYPE_HOME_PAGE_COLLECTION = 5;
    int REQUEST_TYPE_SELLER_COLLECTION = 6;

    int VIEW_TYPE_LIST = 1;
    int VIEW_TYPE_GRID = 2;

    /*ORDER PLACED ACTIVITY*/
    int DEFAULT_INCREMENT_ID = 1234567890;

    int RC_LOCATION_FETCH_ADDRESS_PERMISSION = 1001;// RC for asking access permission for location
    int RC_WRITE_TO_EXTERNAL_STORAGE = 2001;// RC for access permission for writing to the device Storage
    int RC_CHECK_LOCATION_SETTINGS = 2002;// RC for access permission for enabling GPS
    int RC_PAYMENT = 2003;// RC for Payment method

    /* CHECKOUT METHOD TYPE*/
    String METHOD_CUSTOMER = "customer";
    String METHOD_GUEST = "guest";

    /*LOGIN REGISTRATION ACTIVITY*/
    /*Based on VIEW PAGE POISTION*/
    int SIGN_IN_PAGE = 0;
    int SIGN_UP_PAGE = 1;

    /* FRAGMENT CONSTANT */
    String BACKSTACK_SUFFIX = "backstack";
}