package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ActivityBecomeSellerBinding;
import com.webkul.mobikulmp.handler.BecomePartnerHandler;
import com.webkul.mobikulmp.model.BecomePartnerData;

public class BecomeSellerActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBecomeSellerBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_become_seller);
        binding.setData(new BecomePartnerData());
        binding.setHandler(new BecomePartnerHandler(this, binding));
    }
}