package com.webkul.mobikulmp.activity;

import android.database.Cursor;

import com.webkul.mobikul.activity.CatalogProductActivity;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.model.catalog.CatalogProductData;
import com.webkul.mobikulmp.connection.MpApiConnection;

import retrofit2.Call;
import retrofit2.Response;

import static com.webkul.mobikul.connection.ApiInterface.MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST;
import static com.webkul.mobikul.constants.ApplicationConstant.REQUEST_TYPE_SELLER_COLLECTION;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;
import static com.webkul.mobikul.helper.AppSharedPref.KEY_CATEGORY_NAME;

/**
 * Created by shubham.agarwal on 20/3/17 @Webkul Software Pvt. Ltd.
 */

public class CatalogProductExtendedActivity extends CatalogProductActivity {

    @Override
    protected void handleIntent() {
        super.handleIntent();
        if (getIntent().hasExtra(BUNDLE_KEY_SELLER_ID)) {
            mCatalogProductData.setRequestType(REQUEST_TYPE_SELLER_COLLECTION);
            setTitle(getIntent().getExtras().getString(KEY_CATEGORY_NAME));
        }
    }

    @Override
    protected void callApi() {
        super.callApi();
        if (getIntent().hasExtra(BUNDLE_KEY_SELLER_ID)) {
            mCatalogProductData.setRequestType(REQUEST_TYPE_SELLER_COLLECTION);
            if (NetworkHelper.isNetworkAvailable(this)) {
                MpApiConnection.getSellerCollectionData(this, getIntent().getExtras().getInt(BUNDLE_KEY_SELLER_ID), mPageNumber, mCategoryId, this);
            } else {
                final Cursor databaseCursor = mOfflineDataBaseHandler.selectFromOfflineDB(MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST, "sellerCollection" + getIntent().getExtras().getInt(BUNDLE_KEY_SELLER_ID) + "");
                if (databaseCursor != null && databaseCursor.getCount() != 0) {
                    databaseCursor.moveToFirst();
                    onResponseRecieved(gson.fromJson(databaseCursor.getString(0), CatalogProductData.class));
                } else {
                    SnackbarHelper.getSnackbar(this, getString(com.webkul.mobikul.R.string.error_no_network)).show();
                }
            }
        }
    }


    @Override
    public void onResponse(Call<CatalogProductData> call, Response<CatalogProductData> response) {
        super.onResponse(call, response);
        if (!NetworkHelper.isValidResponse(this, response, true)) {
            return;
        }

        if (isFirstCall) {
            if (getIntent().hasExtra(BUNDLE_KEY_SELLER_ID)) {
                String responseJSON = gson.toJson(response.body());
                if (NetworkHelper.isNetworkAvailable(CatalogProductExtendedActivity.this)) {
                    mOfflineDataBaseHandler.updateIntoOfflineDB(MOBIKUL_CATALOG_CATEGORY_PRODUCT_LIST, responseJSON, "sellerCollection" + getIntent().getExtras().getInt(BUNDLE_KEY_SELLER_ID) + "");
                }
            }
        }
    }
}
