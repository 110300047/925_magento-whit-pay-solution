package com.webkul.mobikulmp.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.ChatMessageAdapter;
import com.webkul.mobikulmp.databinding.ActivityChatBinding;
import com.webkul.mobikulmp.handler.ChatMessageHandler;
import com.webkul.mobikulmp.helper.MpAppSharedPref;
import com.webkul.mobikulmp.model.chat.ChatMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_FROM_NOTIFICATION;

public class ChatActivity extends BaseActivity {

    public ActivityChatBinding mBinding;
    public DatabaseReference mDatabaseReference;
    public String user_name = "", user_id = "", token = "";
    private Intent intent;
    public String name;
    private ChatMessageAdapter chatMessageAdapter;
    public String currentMessage = "";
    private List<ChatMessage> messageList = new ArrayList<>();
    private boolean isFromNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        intent = getIntent();
        user_name = intent.getStringExtra("user_name");
        user_id = intent.getStringExtra("user_id");
        if (intent.hasExtra("token")) {
            token = intent.getStringExtra("token");
        } else {
            token = "";
        }
        if (intent.hasExtra(BUNDLE_KEY_FROM_NOTIFICATION)) {
            isFromNotification = getIntent().getBooleanExtra(BUNDLE_KEY_FROM_NOTIFICATION, false);
        }
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child(user_id);
        mBinding.msgRv.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
        chatMessageAdapter = new ChatMessageAdapter(ChatActivity.this, messageList);
        mBinding.msgRv.setAdapter(chatMessageAdapter);

        mBinding.setCurrentMessage(currentMessage);
        mBinding.setIsLoading(false);
        mBinding.setHandler(new ChatMessageHandler(ChatActivity.this));


        if (MpAppSharedPref.isAdmin(ChatActivity.this)) {
            name = getString(R.string.admin);
            setTitle(user_name);
        } else {
            name = user_name;
            setTitle(getString(R.string.admin));
        }

        mDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                loadChat(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                loadChat(dataSnapshot);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void loadChat(DataSnapshot dataSnapshot) {

        Iterator i = dataSnapshot.getChildren().iterator();
        while (i.hasNext()) {
            mBinding.setIsLoading(true);

            String chat_msg = (String) ((DataSnapshot) i.next()).getValue();
            String chat_user_name = (String) ((DataSnapshot) i.next()).getValue();
//            String timeStamp = (String) ((DataSnapshot) i.next()).getValue();
            long timeStampValue = (long) ((DataSnapshot) i.next()).getValue();
            SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa", Locale.getDefault());
            SimpleDateFormat displayDayFormat = new SimpleDateFormat("EEEE", Locale.getDefault());

            Calendar calendar = Calendar.getInstance();
            String currentDate = displayDateFormat.format(calendar.getTime());
            currentDate = currentDate.split(" ")[0];
            calendar.setTimeInMillis(timeStampValue);

            String displayDay = displayDayFormat.format(calendar.getTime());
            String dispalyDateTimeValue = displayDateFormat.format(calendar.getTime());

            String[] dispalyDateTimeValueArray = dispalyDateTimeValue.split(" ");
            String displayDate = dispalyDateTimeValueArray[0];
            String displayTime = dispalyDateTimeValueArray[1] + " " + dispalyDateTimeValueArray[2];

            if (currentDate.matches(displayDate)) {
                displayDay = "";
            }

            ChatMessage chatMessage = new ChatMessage(chat_msg, chat_user_name, displayDay, displayTime, displayDate);

            messageList.add(chatMessage);

        }
        mBinding.setIsLoading(false);
        chatMessageAdapter.notifyDataSetChanged();

        mBinding.msgRv.scrollToPosition(chatMessageAdapter.getItemCount() - 1);

    }

    @Override
    public void onBackPressed() {
        if (isFromNotification) {
            Intent intent = new Intent(this, ((MobikulApplication) getApplication()).getHomePageClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }
}
