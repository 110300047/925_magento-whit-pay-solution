package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.widget.Filter;
import android.widget.SearchView;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.ChatSellerAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivityChatWithSellersBinding;
import com.webkul.mobikulmp.helper.MpAppSharedPref;
import com.webkul.mobikulmp.model.chat.ChatSellerData;
import com.webkul.mobikulmp.model.chat.ChatSellerListResponseData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatWithSellersActivity extends BaseActivity {
    private ActivityChatWithSellersBinding mBinding;
    private ChatSellerAdapter sellerAdapter;
    private ChatSellerListResponseData data;
    private List<ChatSellerData> sellerDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat_with_sellers);
        mBinding.sellerRv.setLayoutManager(new LinearLayoutManager(ChatWithSellersActivity.this));
        sellerAdapter = new ChatSellerAdapter(ChatWithSellersActivity.this, sellerDataList);
        mBinding.sellerRv.setAdapter(sellerAdapter);
        mBinding.sellerSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchSeller(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchSeller(newText);
                return true;
            }
        });
        MpApiConnection.getChatSellerList(this, chatSellerListResponseDataCallback);
        AlertDialogHelper.showDefaultAlertDialog(this);
    }

    private Callback<ChatSellerListResponseData> chatSellerListResponseDataCallback = new Callback<ChatSellerListResponseData>() {
        @Override
        public void onResponse(Call<ChatSellerListResponseData> call, Response<ChatSellerListResponseData> response) {
            AlertDialogHelper.dismiss(ChatWithSellersActivity.this);
            if (NetworkHelper.isValidResponse(ChatWithSellersActivity.this, response, true)) {
                if (response.body().getResponseCode() == NetworkHelper.NW_RESPONSE_CODE_AUTH_FAILURE) {
                    MpApiConnection.getChatSellerList(ChatWithSellersActivity.this, chatSellerListResponseDataCallback);
                } else {
                    ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
                    mBinding.setData(response.body());
                    data = response.body();
                    MpAppSharedPref.setApiKey(ChatWithSellersActivity.this, response.body().getApiKey());
                    onResponseRecieved(response.body());
                }
            }

        }

        @Override
        public void onFailure(Call<ChatSellerListResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, ChatWithSellersActivity.this);
        }
    };

    private void onResponseRecieved(ChatSellerListResponseData data) {

        if (data.getApiKey() != null && !data.getApiKey().isEmpty()) {
            MpAppSharedPref.setApiKey(ChatWithSellersActivity.this, data.getApiKey());
        }
        if (data.getSellerList() != null && data.getSellerList().size() > 0) {
            sellerDataList.addAll(data.getSellerList());
            sellerAdapter.notifyDataSetChanged();
            mBinding.setShowNoresultTv(false);
        }
    }


    private void searchSeller(String sellerName) {
        Log.d(ApplicationConstant.TAG, "ChatWithSellersActivity searchSeller  yahaan aaya : " + sellerName);
        if (sellerName == null) {
            return;
        }
        if (sellerName.isEmpty() || sellerName.length() < 3) {
            sellerDataList.clear();
            sellerDataList.addAll(data.getSellerList());
            sellerAdapter.notifyDataSetChanged();
            mBinding.setShowNoresultTv(false);
            return;
        }

        sellerAdapter.getFilter().filter(sellerName, new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {
                Log.d(ApplicationConstant.TAG, "ChatWithSellersActivity onFilterComplete : " + count);
                if (count > 0) {
                    mBinding.setShowNoresultTv(false);
                } else if (count == 0) {
                    mBinding.setShowNoresultTv(true);
                }
            }
        });

    }
}
