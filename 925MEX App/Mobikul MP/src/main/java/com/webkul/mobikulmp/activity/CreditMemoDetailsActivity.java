package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerOrderItemAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivityCreditMemoDetailsBinding;
import com.webkul.mobikulmp.handler.CreditMemoDetailsHandler;
import com.webkul.mobikulmp.model.CreditMemoDetailsResponseData;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CREDIT_MEMO_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_AUTH_FAILURE;

public class CreditMemoDetailsActivity extends BaseActivity {

    private ActivityCreditMemoDetailsBinding mBinding;
    private String incrementId;
    private int creditMemoId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_credit_memo_details);
        incrementId = getIntent().getStringExtra(BUNDLE_KEY_INCREMENT_ID);
        creditMemoId = getIntent().getIntExtra(BUNDLE_KEY_CREDIT_MEMO_ID, 0);

        mSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mSweetAlertDialog.setTitleText(getString(com.webkul.mobikul.R.string.please_wait));
        mSweetAlertDialog.setCancelable(false);
        mSweetAlertDialog.show();

        MpApiConnection.getCreditMemoDetails(CreditMemoDetailsActivity.this, incrementId, creditMemoId, creditMemoDetailsResponseDataCallback);

    }

    private Callback<CreditMemoDetailsResponseData> creditMemoDetailsResponseDataCallback = new Callback<CreditMemoDetailsResponseData>() {
        @Override
        public void onResponse(Call<CreditMemoDetailsResponseData> call, Response<CreditMemoDetailsResponseData> response) {

            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE) {
                MpApiConnection.getCreditMemoDetails(CreditMemoDetailsActivity.this, incrementId, creditMemoId, creditMemoDetailsResponseDataCallback);
                return;
            }

            onResponseRecieved(response.body());

        }

        @Override
        public void onFailure(Call<CreditMemoDetailsResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, CreditMemoDetailsActivity.this);
        }
    };

    private void onResponseRecieved(CreditMemoDetailsResponseData data) {
        ApplicationSingleton.getInstance().setAuthKey(data.getAuthKey());
        mBinding.setData(data);
        mBinding.setHandler(new CreditMemoDetailsHandler(CreditMemoDetailsActivity.this, incrementId, creditMemoId));
        mSweetAlertDialog.dismiss();

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle(data.getMainHeading());
        }
        mBinding.creditMemoDetailsItemsRv.setLayoutManager(new LinearLayoutManager(CreditMemoDetailsActivity.this));
//        mBinding.creditMemoDetailsItemsRv.setAdapter(new SellerOrderItemAdapter(CreditMemoDetailsActivity.this, data.getItems()));

    }
}
