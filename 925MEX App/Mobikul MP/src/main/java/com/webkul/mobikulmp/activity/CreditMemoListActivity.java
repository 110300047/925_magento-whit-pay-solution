package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.CreditMemoListRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivityCreditMemoListBinding;
import com.webkul.mobikulmp.model.CreditMemoListResponseData;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_AUTH_FAILURE;

public class CreditMemoListActivity extends BaseActivity {
    private ActivityCreditMemoListBinding mBinding;
    private String incrementId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_credit_memo_list);
        incrementId = getIntent().getStringExtra(BUNDLE_KEY_INCREMENT_ID);
        mSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mSweetAlertDialog.setTitleText(getString(com.webkul.mobikul.R.string.please_wait));
        mSweetAlertDialog.setCancelable(false);
        mSweetAlertDialog.show();
        MpApiConnection.getCreditMemoList(CreditMemoListActivity.this, incrementId, creditMemoListResponseDataCallback);
    }


    private Callback<CreditMemoListResponseData> creditMemoListResponseDataCallback = new Callback<CreditMemoListResponseData>() {
        @Override
        public void onResponse(Call<CreditMemoListResponseData> call, Response<CreditMemoListResponseData> response) {
            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE) {
                MpApiConnection.getCreditMemoList(CreditMemoListActivity.this, incrementId, creditMemoListResponseDataCallback);
                return;
            }
            onResponseRecieved(response.body());
        }

        @Override
        public void onFailure(Call<CreditMemoListResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, CreditMemoListActivity.this);
        }
    };

    private void onResponseRecieved(CreditMemoListResponseData data) {
        ApplicationSingleton.getInstance().setAuthKey(data.getAuthKey());
        mBinding.setData(data);
        mSweetAlertDialog.dismiss();
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle(data.getMainHeading());
        }
        mBinding.creditMemoListRv.setLayoutManager(new LinearLayoutManager(CreditMemoListActivity.this));
        mBinding.creditMemoListRv.setAdapter(new CreditMemoListRvAdapter(CreditMemoListActivity.this, data.getCreditMemoList(), data.getLabels()));
    }
}
