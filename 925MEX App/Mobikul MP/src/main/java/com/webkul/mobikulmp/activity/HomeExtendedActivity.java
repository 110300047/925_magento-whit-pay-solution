package com.webkul.mobikulmp.activity;

import com.webkul.mobikul.activity.HomeActivity;
import com.webkul.mobikul.adapter.NavDrawerEndRvAdapter;
import com.webkul.mobikul.model.catalog.NavDrawerCustomerItem;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.helper.MpAppSharedPref;

import java.util.List;

public class HomeExtendedActivity extends HomeActivity {

    @Override
    public void loadNavigationViewEndRecyclerViewAdapter() {
        super.loadNavigationViewEndRecyclerViewAdapter();
        List<NavDrawerCustomerItem> navDrawerCustmerItems = ((NavDrawerEndRvAdapter) mBinding.navDrawerEndRecyclerView.getAdapter()).getNavDrawerCustomerItems();
        /*if (MpAppSharedPref.isSeller(this)) {
            navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_seller_label), NavDrawerCustomerItem.TYPE_HEADER));
            if (MpAppSharedPref.isSellerPending(this)) {
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_seller_pending_status), NavDrawerCustomerItem.TYPE_SELLER_STATUS, R.drawable.ic_want_to_become_seller_wrapper));
            } else {
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_seller_dashboard), NavDrawerCustomerItem.TYPE_SELLER_DASHBOARD, R.drawable.ic_vector_seller_dashboard_wrapper));
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_seller_profile), NavDrawerCustomerItem.TYPE_SELLER_PROFILE, R.drawable.ic_vector_seller_profile_wrapper));
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_seller_order), NavDrawerCustomerItem.TYPE_SELLER_ORDER, R.drawable.ic_vector_seller_orders_wrapper));
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_seller_new_product), NavDrawerCustomerItem.TYPE_SELLER_NEW_PRODUCT, R.drawable.ic_vector_seller_add_product_wrapper));
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_seller_product_list), NavDrawerCustomerItem.TYPE_SELLER_PRODUCTS_LIST, R.drawable.ic_vector_seller_product_list_wrapper));
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_seller_transaction_list), NavDrawerCustomerItem.TYPE_SELLER_TRANSACTIONS_LIST, R.drawable.ic_vector_seller_transaction_list_wrapper));
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_manage_print_pdf_header), NavDrawerCustomerItem.TYPE_MANAGE_PRINT_PDF_HEADER, R.drawable.ic_vector_print_pdf_wrapper));
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, MpAppSharedPref.isAdmin(HomeExtendedActivity.this) ? getString(R.string.chat_with_sellers) : getString(R.string.chat_with_admin), NavDrawerCustomerItem.TYPE_CHAT, R.drawable.ic_chat_wrapper));
            }
            if (!MpAppSharedPref.isAdmin(HomeExtendedActivity.this)) {
                navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_menu_ask_question_to_admin), NavDrawerCustomerItem.TYPE_ASK_QUESTION_TO_ADMIN, R.drawable.ic_vector_ask_question_to_admin_wrapper));
            }
        } else {
            navDrawerCustmerItems.add(new NavDrawerCustomerItem(this, getString(R.string.nav_become_seller), NavDrawerCustomerItem.TYPE_SELLER_STATUS, R.drawable.ic_want_to_become_seller_wrapper));
        }*/
    }
}