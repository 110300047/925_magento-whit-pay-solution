package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerOrderItemAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivityInvoiceDetailsBinding;
import com.webkul.mobikulmp.handler.InvoiceDetailHandler;
import com.webkul.mobikulmp.model.InvoiceDetailResponseData;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INVOICE_ID;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_AUTH_FAILURE;

public class InvoiceDetailsActivity extends BaseActivity {
    private ActivityInvoiceDetailsBinding mBinding;
    private String incrementId;
    private int invoiceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_invoice_details);
        incrementId = getIntent().getStringExtra(BUNDLE_KEY_INCREMENT_ID);
        invoiceId = getIntent().getIntExtra(BUNDLE_KEY_INVOICE_ID, 0);

        mSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mSweetAlertDialog.setTitleText(getString(com.webkul.mobikul.R.string.please_wait));
        mSweetAlertDialog.setCancelable(false);
        mSweetAlertDialog.show();

        MpApiConnection.getInvoiceDetails(this, incrementId, invoiceId, invoiceDetailResponseDataCallback);

    }

    private Callback<InvoiceDetailResponseData> invoiceDetailResponseDataCallback = new Callback<InvoiceDetailResponseData>() {
        @Override
        public void onResponse(Call<InvoiceDetailResponseData> call, Response<InvoiceDetailResponseData> response) {
            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE) {
                MpApiConnection.getInvoiceDetails(InvoiceDetailsActivity.this, incrementId, invoiceId, invoiceDetailResponseDataCallback);
                return;
            }

            onResponseRecieved(response.body());

        }

        @Override
        public void onFailure(Call<InvoiceDetailResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, InvoiceDetailsActivity.this);
        }
    };

    public void onResponseRecieved(InvoiceDetailResponseData data) {
        ApplicationSingleton.getInstance().setAuthKey(data.getAuthKey());
        mBinding.setData(data);
        mBinding.setHandler(new InvoiceDetailHandler(InvoiceDetailsActivity.this, incrementId, invoiceId));

        mSweetAlertDialog.dismiss();

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle(data.getMainHeading());
        }

        mBinding.invoicedItemsRv.setLayoutManager(new LinearLayoutManager(InvoiceDetailsActivity.this));
//        mBinding.invoicedItemsRv.setAdapter(new SellerOrderItemAdapter(InvoiceDetailsActivity.this, data.getItems()));


    }

}
