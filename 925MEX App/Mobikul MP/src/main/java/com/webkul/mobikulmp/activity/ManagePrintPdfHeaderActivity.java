package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivityManagePrintPdfHeaderBinding;
import com.webkul.mobikulmp.model.pdfheader.PdfHeaderInfoDataResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManagePrintPdfHeaderActivity extends BaseActivity {

    private ActivityManagePrintPdfHeaderBinding mBinding;
    private Callback<PdfHeaderInfoDataResponse> mCallback = new Callback<PdfHeaderInfoDataResponse>() {
        @Override
        public void onResponse(Call<PdfHeaderInfoDataResponse> call, Response<PdfHeaderInfoDataResponse> response) {
            AlertDialogHelper.dismiss(ManagePrintPdfHeaderActivity.this);
            if (response.body().getSuccess() && response.body().getHeaderInfo() != null) {
                mBinding.setData(response.body().getHeaderInfo());
            } else {
                ToastHelper.showToast(ManagePrintPdfHeaderActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT, 0);
            }
        }

        @Override
        public void onFailure(Call<PdfHeaderInfoDataResponse> call, Throwable t) {
            NetworkHelper.onFailure(t, ManagePrintPdfHeaderActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_manage_print_pdf_header);
        initialize();
    }

    private void initialize() {
        setTitle(getString(R.string.activity_title_manage_print_pdf_header));
        AlertDialogHelper.showDefaultAlertDialog(this);

        MpApiConnection.getPdfHeaderFormData(this, mCallback);
    }

    public void onClickSaveInfoBtn(View view) {
        if (mBinding.getData() == null || mBinding.getData().isEmpty()) {
            ToastHelper.showToast(this, getString(R.string.please_fill_the_information), Toast.LENGTH_SHORT, 0);
        } else {
            AlertDialogHelper.showDefaultAlertDialog(this);
            MpApiConnection.savePdfHeader(this, mBinding.getData(), mSweetAlertDialog, mCallback);
        }
    }
}