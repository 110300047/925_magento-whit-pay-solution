package com.webkul.mobikulmp.activity;

import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.MarketplaceLandingPageBestSellerListRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivityMarketplaceLandingBinding;
import com.webkul.mobikulmp.handler.MarketplaceLandingActivityHandler;
import com.webkul.mobikulmp.model.landingpage.MarketplaceLandingPageData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarketplaceLandingActivity extends BaseActivity {

    private ActivityMarketplaceLandingBinding mBinding;

    private Callback<MarketplaceLandingPageData> mMarketplaceLandingPageDataCallback = new Callback<MarketplaceLandingPageData>() {
        @Override
        public void onResponse(Call<MarketplaceLandingPageData> call, Response<MarketplaceLandingPageData> response) {
            if (!NetworkHelper.isValidResponse(MarketplaceLandingActivity.this, response, true)) {
                return;
            }

            ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
            initViews(response.body());
            String responseJSON = gson.toJson(response.body());
            mOfflineDataBaseHandler.updateIntoOfflineDB("mobikulmphttp/marketplace/landingPageData/", responseJSON, null);
        }

        @Override
        public void onFailure(Call<MarketplaceLandingPageData> call, Throwable t) {
            NetworkHelper.onFailure(t, MarketplaceLandingActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_marketplace_landing);
        if (NetworkHelper.isNetworkAvailable(MarketplaceLandingActivity.this)) {
            MpApiConnection.getLandingPageData(this, ApplicationConstant.API_USER_NAME, ApplicationConstant.API_PASSWORD, mMarketplaceLandingPageDataCallback);
        } else {
            final Cursor databaseCursor = mOfflineDataBaseHandler.selectFromOfflineDB("mobikulmphttp/marketplace/landingPageData/", null);
            if (databaseCursor != null && databaseCursor.getCount() != 0) {
                databaseCursor.moveToFirst();
                MarketplaceLandingPageData data = gson.fromJson(databaseCursor.getString(0), MarketplaceLandingPageData.class);
                initViews(data);
            } else {
                SnackbarHelper.getSnackbar(this, getString(com.webkul.mobikul.R.string.error_no_network)).show();
            }
        }
    }

    private void initViews(MarketplaceLandingPageData landingPageData) {
        mBinding.setData(landingPageData);
        mBinding.setHandler(new MarketplaceLandingActivityHandler(MarketplaceLandingActivity.this));
        mBinding.bestSellerListRv.setLayoutManager(new LinearLayoutManager(MarketplaceLandingActivity.this));
        mBinding.bestSellerListRv.setAdapter(new MarketplaceLandingPageBestSellerListRvAdapter(MarketplaceLandingActivity.this
                , landingPageData.getLayout1().getSellersData()));
    }
}
