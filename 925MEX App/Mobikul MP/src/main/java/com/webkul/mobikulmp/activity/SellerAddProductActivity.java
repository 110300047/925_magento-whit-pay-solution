package com.webkul.mobikulmp.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.ImageHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.model.catalog.CategoriesData;
import com.webkul.mobikul.model.catalog.SubCategory;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.AddProductCategoryRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerAddProductBinding;
import com.webkul.mobikulmp.handler.SellerAddProductActivityHandler;
import com.webkul.mobikulmp.model.seller.CheckSkuResponseData;
import com.webkul.mobikulmp.model.seller.CustomCategoryData;
import com.webkul.mobikulmp.model.seller.MedialGallery;
import com.webkul.mobikulmp.model.seller.SellerAddProductResponseData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.ApplicationConstant.RC_PICK_IMAGE_FROM_CAMERA;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_PICK_IMAGE_FROM_GALLERY;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;

public class SellerAddProductActivity extends BaseActivity {

    public String mProductId;
    public ActivitySellerAddProductBinding mBinding;
    public ArrayList<CustomCategoryData> mCategoryList;
    public SellerAddProductResponseData mSellerAddProductResponseData;
    private int mChildLevel;
    private ArrayList<CustomCategoryData> mCategoryChildList;
    private Callback<CheckSkuResponseData> mCheckSkuResponseCallback = new Callback<CheckSkuResponseData>() {
        @Override
        public void onResponse(Call<CheckSkuResponseData> call, Response<CheckSkuResponseData> response) {
            mBinding.setLoading(false);
            if (response.body().isSuccess()) {
                ToastHelper.showToast(SellerAddProductActivity.this, response.body().getMessage(), Toast.LENGTH_LONG, 0);
                if (!response.body().isAvailable()) {
                    mBinding.skuEt.setText("");
                }
            }
        }

        @Override
        public void onFailure(Call<CheckSkuResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerAddProductActivity.this);
            mBinding.setLoading(false);
        }
    };
    private Callback<SellerAddProductResponseData> mSellerAddProductResponse = new Callback<SellerAddProductResponseData>() {
        @Override
        public void onResponse(Call<SellerAddProductResponseData> call, Response<SellerAddProductResponseData> response) {
            mBinding.setLoading(false);
            try {
                if (response.body().isSuccess()) {
                    mSellerAddProductResponseData = response.body();
                    mBinding.setData(mSellerAddProductResponseData);
                    mBinding.setHandler(new SellerAddProductActivityHandler(SellerAddProductActivity.this));
                    startInitialization();
                } else {
                    if (response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        ToastHelper.showToast(SellerAddProductActivity.this, response.body().getMessage(), Toast.LENGTH_LONG, 0);
                    } else {
                        ToastHelper.showToast(SellerAddProductActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_LONG, 0);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<SellerAddProductResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerAddProductActivity.this);
            mBinding.setLoading(false);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_add_product);
        if (getIntent().hasExtra(BUNDLE_KEY_PRODUCT_ID)) {
            mProductId = getIntent().getStringExtra(BUNDLE_KEY_PRODUCT_ID);
            setActionbarTitle(getString(R.string.activity_title_seller_edit_product));
        } else {
            setActionbarTitle(getString(R.string.activity_title_seller_add_product));
        }
        callApi();
    }

    public void callApi() {
        mBinding.setLoading(true);
        MpApiConnection.getProductFormData(this, mProductId, mSellerAddProductResponse);
    }

    private void startInitialization() {
        setupAttributeSetSpinner();
        setupProductTypeSpinner();
        setupProductCategories();
        setupSkuChangeListener();
        setupStockAvailabilitySpinner();
        setupVisibilitySpinner();
        setupTaxClassSpinner();
        setupProductImages();
        if (mProductId == null) {
            mSellerAddProductResponseData.getProductData().setProductHasWeight(true);
        }
    }

    private void setupAttributeSetSpinner() {
        ArrayList<String> attributeNames = new ArrayList<>();
        for (int noOfCountries = 0; noOfCountries < mSellerAddProductResponseData.getAllowedAttributes().size(); noOfCountries++) {
            attributeNames.add(mSellerAddProductResponseData.getAllowedAttributes().get(noOfCountries).getLabel());
        }
        mBinding.attributeSetSp.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                , attributeNames));
        mBinding.attributeSetSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSellerAddProductResponseData.getProductData().setAttributeSetId(mSellerAddProductResponseData.getAllowedAttributes().get(position).getValue());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mBinding.attributeSetSp.setSelection(mSellerAddProductResponseData.getSelectedAttributeSet());
    }

    private void setupProductTypeSpinner() {
        ArrayList<String> productTypeNames = new ArrayList<>();
        for (int noOfType = 0; noOfType < mSellerAddProductResponseData.getAllowedTypes().size(); noOfType++) {
            productTypeNames.add(mSellerAddProductResponseData.getAllowedTypes().get(noOfType).getLabel());
        }
        mBinding.productTypeSp.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                , productTypeNames));
        mBinding.productTypeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSellerAddProductResponseData.getProductData().setType(mSellerAddProductResponseData.getAllowedTypes().get(position).getValue());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mBinding.productTypeSp.setSelection(mSellerAddProductResponseData.getSelectedType());
    }

    private void setupProductCategories() {
        checkCategoryAvailability();
        createCategoryViewData();

        mBinding.categoryRv.setAdapter(new AddProductCategoryRvAdapter(this, mCategoryList));
        mBinding.categoryRv.setNestedScrollingEnabled(false);
    }

    private void checkCategoryAvailability() {
        // This method checks whether the categories are available or not because when particular categories are assigned to seller then assignedCategories data is used.
        if (mSellerAddProductResponseData.getCategories().getCategoriesData() == null &&
                mSellerAddProductResponseData.getAssignedCategories().size() > 0) {
            mSellerAddProductResponseData.getCategories().setCategoriesData(new ArrayList<CategoriesData>());
            for (int mainCategory = 0; mainCategory < mSellerAddProductResponseData.getAssignedCategories().size(); mainCategory++) {
                CategoriesData customCategories = new CategoriesData();
                customCategories.setCategoryId(Integer.parseInt(mSellerAddProductResponseData.getAssignedCategories().get(mainCategory).getValue()));
                customCategories.setName(mSellerAddProductResponseData.getAssignedCategories().get(mainCategory).getLabel());
                customCategories.setChildren(new ArrayList<SubCategory>());
                mSellerAddProductResponseData.getCategories().getCategoriesData().add(customCategories);
            }
        }
    }

    private void createCategoryViewData() {
        mCategoryList = new ArrayList<>();
        if (mSellerAddProductResponseData.getCategories().getCategoriesData() != null) {
            for (int mainCategory = 0; mainCategory < mSellerAddProductResponseData.getCategories().getCategoriesData().size(); mainCategory++) {
                mCategoryChildList = new ArrayList<>();
                CustomCategoryData customCategories = new CustomCategoryData();
                customCategories.setId(mSellerAddProductResponseData.getCategories().getCategoriesData().get(mainCategory).getCategoryId());
                customCategories.setName(mSellerAddProductResponseData.getCategories().getCategoriesData().get(mainCategory).getName());
                if (mSellerAddProductResponseData.getProductData().getCategoryIds() != null && mSellerAddProductResponseData.getProductData().getCategoryIds().contains(String.valueOf(customCategories.getId())))
                    customCategories.setChecked(true);

                if (mSellerAddProductResponseData.getCategories().getCategoriesData().get(mainCategory).getChildren().size() != 0) {
                    mChildLevel = 1;
                    addChildrenData(mSellerAddProductResponseData.getCategories().getCategoriesData().get(mainCategory).getChildren());
                }
                customCategories.setChildrenData(mCategoryChildList);
                mCategoryList.add(customCategories);
            }
        }
    }

    private void addChildrenData(List<SubCategory> childrenData) {
        for (int childCategory = 0; childCategory < childrenData.size(); childCategory++) {
            CustomCategoryData customChildren = new CustomCategoryData();
            customChildren.setId(childrenData.get(childCategory).getCategoryId());
            customChildren.setName(prepareNameWithSpace(childrenData.get(childCategory).getName()));
            if (mSellerAddProductResponseData.getProductData().getCategoryIds() != null && mSellerAddProductResponseData.getProductData().getCategoryIds().contains(String.valueOf(customChildren.getId())))
                customChildren.setChecked(true);

            mCategoryChildList.add(customChildren);
            if (childrenData.get(childCategory).getChildren().size() != 0) {
                mChildLevel++;
                addChildrenData(childrenData.get(childCategory).getChildren());
            }
        }
        mChildLevel--;
    }

    private String prepareNameWithSpace(String name) {
        String finalName = "";
        for (int noOfSpaces = 0; noOfSpaces < mChildLevel * 5; noOfSpaces++)
            finalName = finalName.concat(" ");
        finalName = finalName.concat(name);
        return finalName;
    }

    private void setupSkuChangeListener() {
        mBinding.skuEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!((AppCompatEditText) v).getText().toString().trim().isEmpty()) {
                        mBinding.setLoading(true);
                        MpApiConnection.checkSku(SellerAddProductActivity.this, ((AppCompatEditText) v).getText().toString(), mCheckSkuResponseCallback);
                    }
                }
            }
        });
    }

    private void setupStockAvailabilitySpinner() {
        ArrayList<String> stockAvailabilityNames = new ArrayList<>();
        for (int noOfStockItems = 0; noOfStockItems < mSellerAddProductResponseData.getInventoryAvailabilityOptions().size(); noOfStockItems++) {
            stockAvailabilityNames.add(mSellerAddProductResponseData.getInventoryAvailabilityOptions().get(noOfStockItems).getLabel());
        }
        mBinding.stockAvailabilitySp.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                , stockAvailabilityNames));
        mBinding.stockAvailabilitySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSellerAddProductResponseData.getProductData().setIsInStock(mSellerAddProductResponseData.getInventoryAvailabilityOptions().get(position).getValue().equals("1"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mBinding.stockAvailabilitySp.setSelection(mProductId == null || mSellerAddProductResponseData.getProductData().isIsInStock() ? 0 : 1);
    }

    private void setupVisibilitySpinner() {
        ArrayList<String> visibilityOptions = new ArrayList<>();
        visibilityOptions.add(getString(R.string.please_choose));
        for (int noOfVisibilityItems = 0; noOfVisibilityItems < mSellerAddProductResponseData.getVisibilityOptions().size(); noOfVisibilityItems++) {
            visibilityOptions.add(mSellerAddProductResponseData.getVisibilityOptions().get(noOfVisibilityItems).getLabel());
        }
        mBinding.visibilitySp.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                , visibilityOptions));
        mBinding.visibilitySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mSellerAddProductResponseData.getProductData().setVisibility("");
                } else {
                    mSellerAddProductResponseData.getProductData().setVisibility(mSellerAddProductResponseData.getVisibilityOptions().get(position - 1).getValue());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mBinding.visibilitySp.setSelection(mSellerAddProductResponseData.getSelectedVisibility());
    }

    private void setupTaxClassSpinner() {
        ArrayList<String> taxClassNames = new ArrayList<>();
        taxClassNames.add(getString(R.string.none));
        for (int noOfTaxClasses = 0; noOfTaxClasses < mSellerAddProductResponseData.getTaxOptions().size(); noOfTaxClasses++) {
            taxClassNames.add(mSellerAddProductResponseData.getTaxOptions().get(noOfTaxClasses).getLabel());
        }
        mBinding.taxClassSp.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item
                , taxClassNames));
        mBinding.taxClassSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mSellerAddProductResponseData.getProductData().setTaxClassId("0");
                } else {
                    mSellerAddProductResponseData.getProductData().setTaxClassId(mSellerAddProductResponseData.getTaxOptions().get(position - 1).getValue());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mBinding.taxClassSp.setSelection(mSellerAddProductResponseData.getSelectedTaxClass());
    }

    private void setupProductImages() {
        if (mSellerAddProductResponseData.getProductData().getMedialGallery() != null &&
                mSellerAddProductResponseData.getProductData().getMedialGallery().size() > 0) {
            for (int noOfImages = 0; noOfImages < mSellerAddProductResponseData.getProductData().getMedialGallery().size(); noOfImages++) {
                switch (noOfImages) {
                    case 0:
                        ImageHelper.load(mBinding.productImage1, mSellerAddProductResponseData.getProductData().getMedialGallery().get(noOfImages).getUrl(), getResources().getDrawable(R.drawable.placeholder));
                        mBinding.productImage2.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        ImageHelper.load(mBinding.productImage2, mSellerAddProductResponseData.getProductData().getMedialGallery().get(noOfImages).getUrl(), getResources().getDrawable(R.drawable.placeholder));
                        mBinding.productImage3.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        ImageHelper.load(mBinding.productImage3, mSellerAddProductResponseData.getProductData().getMedialGallery().get(noOfImages).getUrl(), getResources().getDrawable(R.drawable.placeholder));
                        mBinding.productImage4.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        ImageHelper.load(mBinding.productImage4, mSellerAddProductResponseData.getProductData().getMedialGallery().get(noOfImages).getUrl(), getResources().getDrawable(R.drawable.placeholder));
                        break;
                }
            }
        } else {
            mSellerAddProductResponseData.getProductData().setMedialGallery(new ArrayList<MedialGallery>());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == RC_PICK_IMAGE_FROM_GALLERY) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_from_library)), RC_PICK_IMAGE_FROM_GALLERY);
            } else if (requestCode == RC_PICK_IMAGE_FROM_CAMERA) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mBinding.getHandler().getImageUri());
                startActivityForResult(intent, RC_PICK_IMAGE_FROM_CAMERA);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mBinding.getHandler().onActivityResult(requestCode, resultCode, data);
    }
}