package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SalesPagerAdapter;
import com.webkul.mobikulmp.adapter.TopSellingProductsRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerDashboardBinding;
import com.webkul.mobikulmp.handler.SellerDashboardActivityHandler;
import com.webkul.mobikulmp.model.seller.SellerDashboardData;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vedesh.kumar on 4/1/18. @Webkul Software Private limited
 */

public class SellerDashboardActivity extends BaseActivity {

    private ActivitySellerDashboardBinding mBinding;
    private Callback<SellerDashboardData> mDashboardDataCallback = new Callback<SellerDashboardData>() {
        @Override
        public void onResponse(@NonNull Call<SellerDashboardData> call, @NonNull Response<SellerDashboardData> response) {
            AlertDialogHelper.dismiss(SellerDashboardActivity.this);
            if (NetworkHelper.isValidResponse(SellerDashboardActivity.this, response, true)) {
                ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());

                mBinding.setData(response.body());

                /*Init Hot Deals RV and Adapter*/
                if (response.body().getTopSellingProducts() != null && response.body().getTopSellingProducts().size() > 0) {
                    mBinding.topProductsRv.setAdapter(new TopSellingProductsRvAdapter(SellerDashboardActivity.this, response.body().getTopSellingProducts()));
                    mBinding.topProductsRv.setNestedScrollingEnabled(false);
                }

                /*Init Sales View Pager*/

                ArrayList<String> listOfLocationReportImages = new ArrayList<>();
                listOfLocationReportImages.add(response.body().getYearlySalesLocationReport());
                listOfLocationReportImages.add(response.body().getMonthlySalesLocationReport());
                listOfLocationReportImages.add(response.body().getWeeklySalesLocationReport());
                listOfLocationReportImages.add(response.body().getDailySalesLocationReport());

                ArrayList<String> listOfStatsImages = new ArrayList<>();
                listOfStatsImages.add(response.body().getYearlySalesStats());
                listOfStatsImages.add(response.body().getMonthlySalesStats());
                listOfStatsImages.add(response.body().getWeeklySalesStats());
                listOfStatsImages.add(response.body().getDailySalesStats());

                mBinding.statsViewPager.setAdapter(new SalesPagerAdapter(SellerDashboardActivity.this, listOfLocationReportImages, listOfStatsImages, response.body().getCategoryChart()));
                mBinding.statsViewPager.setOffscreenPageLimit(2);
                mBinding.statsTabs.setupWithViewPager(mBinding.statsViewPager);

                mBinding.setHandler(new SellerDashboardActivityHandler(SellerDashboardActivity.this, mBinding.getData()));
            }
        }

        @Override
        public void onFailure(@NonNull Call<SellerDashboardData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerDashboardActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_dashboard);
        showBackButton();
        MpApiConnection.getsellerdashboardData(this, ApplicationConstant.API_USER_NAME, ApplicationConstant.API_PASSWORD, mDashboardDataCallback);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true; // Displaying no menu...
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setActionbarTitle(getString(R.string.activity_title_seller_dashboard));
    }
}
