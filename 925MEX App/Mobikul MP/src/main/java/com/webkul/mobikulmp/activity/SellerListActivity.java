package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerListActivityRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerListBinding;
import com.webkul.mobikulmp.handler.SellerListActivityHandler;
import com.webkul.mobikulmp.model.SellerListResponseData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerListActivity extends BaseActivity {

    public ActivitySellerListBinding mBinding;
    public String mStoreName = "";
    private Callback<SellerListResponseData> mSellerListResponseDataCallback = new Callback<SellerListResponseData>() {

        @Override
        public void onResponse(Call<SellerListResponseData> call, Response<SellerListResponseData> response) {
            if (!NetworkHelper.isValidResponse(SellerListActivity.this, response, true)) {
                return;
            }

            ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
            AlertDialogHelper.dismiss(SellerListActivity.this);
            if (response.body().isSuccess()) {
                mBinding.setData(response.body());
                mBinding.setHandler(new SellerListActivityHandler(SellerListActivity.this));

                if (response.body().getSellerList().size() == 0) {
                    SnackbarHelper.getSnackbar(SellerListActivity.this, getString(R.string.no_seller_found)).show();
                }

                mBinding.sellerListRv.setAdapter(new SellerListActivityRvAdapter(SellerListActivity.this, response.body().getSellerList()));
            }
        }

        @Override
        public void onFailure(Call<SellerListResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerListActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_list);
        setSupportActionBar(mBinding.toolbar);
        showBackButton();
        callApi();
    }

    public void callApi() {
        MpApiConnection.getSellerList(this, mStoreName, mSellerListResponseDataCallback);
    }

}
