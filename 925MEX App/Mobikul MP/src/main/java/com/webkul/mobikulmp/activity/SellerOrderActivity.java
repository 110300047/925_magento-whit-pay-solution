package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerOrdersListRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerOrderBinding;
import com.webkul.mobikulmp.handler.SellerOrderActivityHandler;
import com.webkul.mobikulmp.handler.SellerOrderFilterFragHandler;
import com.webkul.mobikulmp.model.SellerOrderFilterFragData;
import com.webkul.mobikulmp.model.seller.SellerOrderListData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_PAGE_COUNT;

/**
 * Created by vedesh.kumar on 23/3/17. @Webkul Software Private limited
 */

public class SellerOrderActivity extends BaseActivity implements SellerOrderFilterFragHandler.OnOrderFilterAppliedListener {

    public SellerOrderFilterFragData mSellerOrderFilterFragData;
    private ActivitySellerOrderBinding mBinding;
    private int mPageNumber = DEFAULT_PAGE_COUNT;
    private boolean isFirstCall = true;
    private Callback<SellerOrderListData> mSellerOrderListCallback = new Callback<SellerOrderListData>() {
        @Override
        public void onResponse(Call<SellerOrderListData> call, Response<SellerOrderListData> response) {
            if (NetworkHelper.isValidResponse(SellerOrderActivity.this, response, true)) {
                onResponseRecieved(response.body());
            }
        }

        @Override
        public void onFailure(Call<SellerOrderListData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerOrderActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_order);
        mSellerOrderFilterFragData = new SellerOrderFilterFragData(this);
        AlertDialogHelper.showDefaultAlertDialog(this);
        callApi();
    }

    public void callApi() {
        MpApiConnection.getSellerOrderListData(this, mSellerOrderFilterFragData, mPageNumber, mSellerOrderListCallback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true; // no menu to dislay
    }

    private void onResponseRecieved(SellerOrderListData orderData) {
        AlertDialogHelper.dismiss(this);
        ApplicationSingleton.getInstance().setAuthKey(orderData.getAuthKey());
        mBinding.setLazyLoading(false);
        mPageNumber++;
        if (isFirstCall) {
            isFirstCall = false;
            mBinding.setData(orderData);
            mBinding.setHandler(new SellerOrderActivityHandler(this));
            mBinding.executePendingBindings();

            mBinding.orderRv.setLayoutManager(new LinearLayoutManager(this));
            mBinding.orderRv.setAdapter(new SellerOrdersListRvAdapter(this, orderData.getOrderList()));
            mBinding.orderRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    int lastCompletelyVisibleItemPosition;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    callLazyConnection(lastCompletelyVisibleItemPosition);
                }
            });
        } else {
            mBinding.getData().getOrderList().addAll(orderData.getOrderList());
            mBinding.orderRv.getAdapter().notifyDataSetChanged();
        }
    }

    private void callLazyConnection(int lastCompletelyVisibleItemPosition) {
        if (!mBinding.getLazyLoading() && mBinding.getData().getOrderList().size() != mBinding.getData().getTotalCount()
                && lastCompletelyVisibleItemPosition == (mBinding.getData().getOrderList().size() - 1)) {
            callApi();
            mBinding.setLazyLoading(true);
        }
    }

    @Override
    public void onOrderFilterApplied(SellerOrderFilterFragData sellerOrderFilterFragData) {
        mSellerOrderFilterFragData = sellerOrderFilterFragData;/*updating data model*/
        mPageNumber = DEFAULT_PAGE_COUNT;
        isFirstCall = true;
        callApi();
    }
}

