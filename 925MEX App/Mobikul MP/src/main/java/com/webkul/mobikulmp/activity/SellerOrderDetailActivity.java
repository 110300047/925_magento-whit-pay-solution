package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerOrderItemAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerOrderDetailBinding;
import com.webkul.mobikulmp.handler.SellerOrderDetailHandler;
import com.webkul.mobikulmp.model.seller.SellerOrderDetailResponseData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;

public class SellerOrderDetailActivity extends BaseActivity {
    private ActivitySellerOrderDetailBinding mBinding;
    private String incrementId;
    private Callback<SellerOrderDetailResponseData> mSellerOrderDetailResponseDataCallback = new Callback<SellerOrderDetailResponseData>() {
        @Override
        public void onResponse(Call<SellerOrderDetailResponseData> call, Response<SellerOrderDetailResponseData> response) {
            onResponseRecieved(response.body());
        }

        @Override
        public void onFailure(Call<SellerOrderDetailResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerOrderDetailActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_order_detail);
        mBinding.setLazyLoading(true);
        incrementId = getIntent().getStringExtra(BUNDLE_KEY_INCREMENT_ID);

        MpApiConnection.getSellerOrderDetailData(this, incrementId, mSellerOrderDetailResponseDataCallback);
    }

    public void onResponseRecieved(SellerOrderDetailResponseData data) {
        ApplicationSingleton.getInstance().setAuthKey(data.getAuthKey());
        if (data.isSuccess()) {
            mBinding.setData(data);
            mBinding.setHandler(new SellerOrderDetailHandler(SellerOrderDetailActivity.this, incrementId));
            mBinding.orderedItemsRv.setAdapter(new SellerOrderItemAdapter(SellerOrderDetailActivity.this, data.getItemList()));
            mBinding.orderedItemsRv.setNestedScrollingEnabled(false);
        }
        mBinding.setLazyLoading(false);
    }
}