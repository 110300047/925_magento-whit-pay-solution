package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerProductsListRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerProductsBinding;
import com.webkul.mobikulmp.handler.SellerOrderFilterFragHandler;
import com.webkul.mobikulmp.handler.SellerProductsListActivityHandler;
import com.webkul.mobikulmp.model.SellerOrderFilterFragData;
import com.webkul.mobikulmp.model.seller.SellerProductListResponseData;

import org.json.JSONArray;

import java.util.List;

import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_PAGE_COUNT;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 23/3/17. @Webkul Software Private limited
 */

public class SellerProductsListActivity extends BaseActivity implements SellerOrderFilterFragHandler.OnOrderFilterAppliedListener, ActionMode.Callback, View.OnClickListener, RecyclerView.OnItemTouchListener {

    public int mPageNumber = DEFAULT_PAGE_COUNT;
    public SellerOrderFilterFragData mSellerOrderFilterFragData;
    public SellerProductListResponseData mSellerProductListResponseData;
    public ActivitySellerProductsBinding mBinding;
    public String mProductName = "";
    public String mDateFrom = "";
    public String mDateTo = "";
    public String mProductStatus = "";
    public boolean isFirstCall = true;
    public SellerProductsListRvAdapter mSellerProductListRvAdapter;
    public List<Integer> mSelectedItemPositions;
    private ActionMode actionMode;
    private GestureDetectorCompat gestureDetector;
    private Callback<SellerProductListResponseData> mSellerProductListCallback = new Callback<SellerProductListResponseData>() {
        @Override
        public void onResponse(Call<SellerProductListResponseData> call, Response<SellerProductListResponseData> response) {
            if (NetworkHelper.isValidResponse(SellerProductsListActivity.this, response, true)) {
                onResponseRecieved(response.body());
            }
        }

        @Override
        public void onFailure(Call<SellerProductListResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerProductsListActivity.this);
        }
    };
    private Callback<BaseModel> mMassDeleteCallBack = new Callback<BaseModel>() {
        @Override
        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
            AlertDialogHelper.dismiss(SellerProductsListActivity.this);
            ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
            ToastHelper.showToast(SellerProductsListActivity.this, response.body().getMessage(), Toast.LENGTH_LONG, 0);
            if (response.body().isSuccess()) {
                mSellerProductListResponseData.setTotalCount((mBinding.getData().getTotalCount() - mSelectedItemPositions.size()));
                for (int noOfItem = mSelectedItemPositions.size() - 1; noOfItem >= 0; noOfItem--) {
                    mSellerProductListRvAdapter.remove(mSelectedItemPositions.get(noOfItem));
                }
            }
        }

        @Override
        public void onFailure(Call<BaseModel> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerProductsListActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_products);
        mSellerOrderFilterFragData = new SellerOrderFilterFragData(this);
        AlertDialogHelper.showDefaultAlertDialog(this);
        callApi();
    }

    public void callApi() {
        MpApiConnection.getSellerProductsListData(this, mPageNumber, mDateTo, mDateFrom, mProductName, mProductStatus, mSellerProductListCallback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true; // no menu to dislay
    }

    private void onResponseRecieved(SellerProductListResponseData productsData) {
        AlertDialogHelper.dismiss(this);
        ApplicationSingleton.getInstance().setAuthKey(productsData.getAuthKey());
        mBinding.setLazyLoading(false);
        mPageNumber++;
        if (isFirstCall) {
            isFirstCall = false;
            mSellerProductListResponseData = productsData;
            mBinding.setData(mSellerProductListResponseData);
            mBinding.setHandler(new SellerProductsListActivityHandler(this));
            mBinding.executePendingBindings();
            if (mSellerProductListResponseData.getProductList().size() == 0 && mProductName.isEmpty() && mDateFrom.isEmpty() && mDateTo.isEmpty() && mProductStatus.isEmpty()) {
                mBinding.filterBtn.animate().alpha(1.0f).translationY(200).setInterpolator(new AccelerateInterpolator(1.4f));
            }

            mSellerProductListRvAdapter = new SellerProductsListRvAdapter(this, mSellerProductListResponseData.getProductList());
            mBinding.productsRv.setAdapter(mSellerProductListRvAdapter);
            mBinding.productsRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    int lastCompletelyVisibleItemPosition;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    String toastString = lastCompletelyVisibleItemPosition + 1 + " " + getString(com.webkul.mobikul.R.string.of_toast_for_no_of_item) + " " + mSellerProductListResponseData.getTotalCount();
                    showToast(SellerProductsListActivity.this, toastString, Toast.LENGTH_LONG, 0);
                    callLazyConnection(lastCompletelyVisibleItemPosition);
                }
            });
            mBinding.productsRv.setItemAnimator(new FadeInLeftAnimator());
            mBinding.productsRv.getItemAnimator().setRemoveDuration(500);

            mBinding.productsRv.addOnItemTouchListener(this);
            gestureDetector = new GestureDetectorCompat(this, new RecyclerViewOnGestureListener());
        } else {
            mBinding.getData().getProductList().addAll(mSellerProductListResponseData.getProductList());
            mBinding.productsRv.getAdapter().notifyDataSetChanged();
        }
    }

    private void callLazyConnection(int lastCompletelyVisibleItemPosition) {
        if (!mBinding.getLazyLoading() && mBinding.getData().getProductList().size() != mBinding.getData().getTotalCount()
                && lastCompletelyVisibleItemPosition == (mBinding.getData().getProductList().size() - 1)) {
            callApi();
            mBinding.setLazyLoading(true);
        }
    }

    @Override
    public void onOrderFilterApplied(SellerOrderFilterFragData sellerOrderFilterFragData) {
        mSellerOrderFilterFragData = sellerOrderFilterFragData;/*updating data model*/
        mPageNumber = DEFAULT_PAGE_COUNT;
        isFirstCall = true;
        callApi();
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.seller_product_delete_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                mSelectedItemPositions = mSellerProductListRvAdapter.getSelectedItems();
                JSONArray selectedProductsIds = new JSONArray();
                for (int noOfItem = mSelectedItemPositions.size() - 1; noOfItem >= 0; noOfItem--) {
                    selectedProductsIds.put(mSellerProductListRvAdapter.getItem(noOfItem).getProductId());
                }
                actionMode.finish();
                MpApiConnection.massDeleteProduct(SellerProductsListActivity.this, selectedProductsIds, mMassDeleteCallBack);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        this.actionMode = null;
        mSellerProductListRvAdapter.clearSelections();
        mBinding.filterBtn.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
    }

    @Override
    public void onClick(View view) {
        if (view != null) {
            if (view.getId() == R.id.main_container) {
                int idx = mBinding.productsRv.getChildPosition(view);
                if (actionMode != null) {
                    myToggleSelection(idx);
                }
            }
        }
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    private void myToggleSelection(int idx) {
        mSellerProductListRvAdapter.toggleSelection(idx);
        String title = getString(R.string.x_selected_count, mSellerProductListRvAdapter.getSelectedItemCount());
        actionMode.setTitle(title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPageNumber = DEFAULT_PAGE_COUNT;
        isFirstCall = true;
        callApi();
    }

    private class RecyclerViewOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = mBinding.productsRv.findChildViewUnder(e.getX(), e.getY());
            onClick(view);
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            View view = mBinding.productsRv.findChildViewUnder(e.getX(), e.getY());
            if (actionMode != null) {
                return;
            }
            // Start the CAB using the ActionMode.Callback defined above
            int idx = mBinding.productsRv.getChildPosition(view);
            if (idx > -1) {
                actionMode = startActionMode(SellerProductsListActivity.this);
                myToggleSelection(idx);
                for (int i = 0; i < mSellerProductListResponseData.getProductList().size(); i++) {
                    mSellerProductListResponseData.getProductList().get(i).setSelectionModeOn(true);
                }
                mBinding.filterBtn.animate().alpha(1.0f).translationY(200).setInterpolator(new AccelerateInterpolator(1.4f));
            }
            super.onLongPress(e);
        }
    }
}