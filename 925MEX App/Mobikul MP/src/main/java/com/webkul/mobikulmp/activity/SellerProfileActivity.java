package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerFeedbackRvAdapter;
import com.webkul.mobikulmp.adapter.SellerRecentProductsRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerProfileBinding;
import com.webkul.mobikulmp.fragment.ContactSellerFragment;
import com.webkul.mobikulmp.fragment.SellerMakeReviewFragment;
import com.webkul.mobikulmp.fragment.SellerReviewFragment;
import com.webkul.mobikulmp.handler.SellerProfileActivityHandler;
import com.webkul.mobikulmp.helper.LocationHelper;
import com.webkul.mobikulmp.model.SellerProfileData;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_TITLE;

public class SellerProfileActivity extends BaseActivity implements OnMapReadyCallback, FragmentManager.OnBackStackChangedListener {
    public static final int DEFAULT_MAX_LINES_WITHOUT_EXPANDING = 5;
    public static final int DEFAULT_MAP_ZOOM_VALUE = 10;

    /*SELLER STORE PROFILE*/
    @SuppressWarnings("unused")
    private ActivitySellerProfileBinding mBinding;
    private int mSellerId;
    private SellerProfileData mSellerProfileData;
    private Callback<SellerProfileData> mSellerProfileDataCallback = new Callback<SellerProfileData>() {

        @Override
        public void onResponse(Call<SellerProfileData> call, Response<SellerProfileData> response) {
            if (response.body().isSuccess()) {
                mSellerProfileData = response.body();

                ApplicationSingleton.getInstance().setAuthKey(mSellerProfileData.getAuthKey());
                mBinding.setData(mSellerProfileData);
                mBinding.setHandler(new SellerProfileActivityHandler(SellerProfileActivity.this, mSellerId));

                setTitle(mSellerProfileData.getShopTitle());

                mBinding.sellerRecentCollectionRv.setAdapter(new SellerRecentProductsRvAdapter(SellerProfileActivity.this, mSellerProfileData.getRecentProductList()));

                if (isRTL(Locale.getDefault())) {
                    ((LinearLayoutManager) mBinding.sellerRecentCollectionRv.getLayoutManager()).setReverseLayout(true);
                }

                try {
                    SupportMapFragment supportMapFragment = SupportMapFragment.newInstance();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.add(R.id.map, supportMapFragment);
                    fragmentTransaction.commit();
                    supportMapFragment.getMapAsync(SellerProfileActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            /*display recent seller feedback*/
                mBinding.recentSellerFeedbackRv.setLayoutManager(new LinearLayoutManager(SellerProfileActivity.this));
                mBinding.recentSellerFeedbackRv.setAdapter(new SellerFeedbackRvAdapter(SellerProfileActivity.this, mSellerProfileData.getReviewList()));
            }
        }

        @Override
        public void onFailure(Call<SellerProfileData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerProfileActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_profile);
        mSellerId = getIntent().getIntExtra(BUNDLE_KEY_SELLER_ID, 0);
        setTitle(getIntent().getStringExtra(BUNDLE_KEY_SELLER_TITLE));
        showBackButton();
        MpApiConnection.getSellerProfileData(SellerProfileActivity.this, mSellerId, mSellerProfileDataCallback);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBinding.notifyChange();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mSellerProfileData.getLocation() != null) {
            LatLng latLng = LocationHelper.getLatLang(this, mSellerProfileData.getLocation().trim());
            if (latLng != null) {
                googleMap.addMarker(new MarkerOptions().position(latLng));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_MAP_ZOOM_VALUE);
                googleMap.animateCamera(cameraUpdate);
            }
        }
    }

    @Override
    public void onBackStackChanged() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            setTitle(mSellerProfileData.getShopTitle());
            return;
        }
        String visibleFragmentTag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        if (visibleFragmentTag.equals(SellerMakeReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX)) {
            setTitle(getString(R.string.write_your_review));
        } else if (visibleFragmentTag.equals(SellerReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX)) {
            setTitle(getString(R.string.seller_reviews));
        } else if (visibleFragmentTag.equals(ContactSellerFragment.class.getSimpleName() + BACKSTACK_SUFFIX)) {
            setTitle(getString(R.string.contact));
        }
    }

    public static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }
}
