package com.webkul.mobikulmp.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.theartofdev.edmodo.cropper.CropImage;
import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerProfileEditBinding;
import com.webkul.mobikulmp.handler.SellerProfileEditActivityHandler;
import com.webkul.mobikulmp.model.seller.SellerProfileFormResponseData;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.ApplicationConstant.RC_PICK_IMAGE_FROM_CAMERA;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_PICK_IMAGE_FROM_GALLERY;

public class SellerProfileEditActivity extends BaseActivity {

    public ActivitySellerProfileEditBinding mBinding;
    private SellerProfileFormResponseData mSellerEditProfileResponseData;
    private Callback<SellerProfileFormResponseData> mCallBack = new Callback<SellerProfileFormResponseData>() {
        @Override
        public void onResponse(Call<SellerProfileFormResponseData> call, Response<SellerProfileFormResponseData> response) {
            AlertDialogHelper.dismiss(SellerProfileEditActivity.this);
            if (response.body().isSuccess()) {
                mSellerEditProfileResponseData = response.body();
                startInitialization();
            }
        }

        @Override
        public void onFailure(Call<SellerProfileFormResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerProfileEditActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_profile_edit);

        callApi();
    }

    private void callApi() {
        MpApiConnection.getsellerProfileFormData(this, mCallBack);
    }

    private void startInitialization() {
        mBinding.setData(mSellerEditProfileResponseData);
        mBinding.setHandler(new SellerProfileEditActivityHandler(this));
        setupCountrySpinner();
    }

    private void setupCountrySpinner() {
        ArrayList<String> countryNames = new ArrayList<>();
        for (int noOfCountries = 0; noOfCountries < mSellerEditProfileResponseData.getCountryList().size(); noOfCountries++) {
            countryNames.add(mSellerEditProfileResponseData.getCountryList().get(noOfCountries).getLabel());
        }
        mBinding.countrySpinner.setAdapter(new ArrayAdapter<>(SellerProfileEditActivity.this, android.R.layout.simple_spinner_dropdown_item
                , countryNames));
        mBinding.countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSellerEditProfileResponseData.setCountry(mSellerEditProfileResponseData.getCountryList().get(position).getValue());
                mBinding.setFlagImageUrl(mSellerEditProfileResponseData.getFlagImageUrl() + "/" + mSellerEditProfileResponseData.getCountry() + ".png");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mBinding.countrySpinner.setSelection(mSellerEditProfileResponseData.getSelectedCountryPositionFromAddressData());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent intent;
            switch (requestCode) {
                case RC_PICK_IMAGE_FROM_CAMERA:
                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mBinding.getHandler().getImageUri());
                    startActivityForResult(intent, RC_PICK_IMAGE_FROM_CAMERA);
                    break;
                case RC_PICK_IMAGE_FROM_GALLERY:
                    intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, getString(com.webkul.mobikul.R.string.choose_from_library)), RC_PICK_IMAGE_FROM_GALLERY);
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_PICK_IMAGE_FROM_CAMERA || requestCode == RC_PICK_IMAGE_FROM_GALLERY) {
                mBinding.getHandler().cropImage(data);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                mBinding.getHandler().updateData(data);
            }
        }
    }
}