package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerTransactionsListRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivitySellerTransactionsBinding;
import com.webkul.mobikulmp.handler.SellerOrderFilterFragHandler;
import com.webkul.mobikulmp.handler.SellerTransactionsListActivityHandler;
import com.webkul.mobikulmp.model.SellerOrderFilterFragData;
import com.webkul.mobikulmp.model.seller.SellerTransactionListResponseData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_PAGE_COUNT;

/**
 * Created by vedesh.kumar on 23/3/17. @Webkul Software Private limited
 */

public class SellerTransactionsListActivity extends BaseActivity implements SellerOrderFilterFragHandler.OnOrderFilterAppliedListener {

    public SellerOrderFilterFragData mSellerOrderFilterFragData;
    private ActivitySellerTransactionsBinding mBinding;
    private int mPageNumber = DEFAULT_PAGE_COUNT;
    public String mDateTo = "";
    public String mDateFrom = "";
    public String mTransactionId = "";
    public boolean isFirstCall = true;
    private Callback<SellerTransactionListResponseData> mSellerTransactionListCallback = new Callback<SellerTransactionListResponseData>() {
        @Override
        public void onResponse(Call<SellerTransactionListResponseData> call, Response<SellerTransactionListResponseData> response) {
            if (NetworkHelper.isValidResponse(SellerTransactionsListActivity.this, response, true)) {
                onResponseRecieved(response.body());
            }
        }

        @Override
        public void onFailure(Call<SellerTransactionListResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, SellerTransactionsListActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_seller_transactions);
        mSellerOrderFilterFragData = new SellerOrderFilterFragData(this);
        AlertDialogHelper.showDefaultAlertDialog(this);
        callApi();
    }

    public void callApi() {
        MpApiConnection.getSellerTransactionListData(this, mPageNumber, mDateTo, mDateFrom, mTransactionId, mSellerTransactionListCallback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true; // no menu to dislay
    }

    private void onResponseRecieved(SellerTransactionListResponseData transactionData) {
        AlertDialogHelper.dismiss(this);
        ApplicationSingleton.getInstance().setAuthKey(transactionData.getAuthKey());
        mBinding.setLazyLoading(false);
        mPageNumber++;
        if (isFirstCall) {
            isFirstCall = false;
            mBinding.setData(transactionData);
            mBinding.setHandler(new SellerTransactionsListActivityHandler());
            mBinding.executePendingBindings();

            mBinding.transactionsRv.setAdapter(new SellerTransactionsListRvAdapter(this, transactionData.getSellerTransactionList()));
            mBinding.transactionsRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    int lastCompletelyVisibleItemPosition;
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    callLazyConnection(lastCompletelyVisibleItemPosition);
                }
            });
        } else {
            mBinding.getData().getSellerTransactionList().addAll(transactionData.getSellerTransactionList());
            mBinding.transactionsRv.getAdapter().notifyDataSetChanged();
        }
    }

    private void callLazyConnection(int lastCompletelyVisibleItemPosition) {
        if (!mBinding.getLazyLoading() && mBinding.getData().getSellerTransactionList().size() != mBinding.getData().getTotalCount()
                && lastCompletelyVisibleItemPosition == (mBinding.getData().getSellerTransactionList().size() - 1)) {
            callApi();
            mBinding.setLazyLoading(true);
        }
    }

    @Override
    public void onOrderFilterApplied(SellerOrderFilterFragData sellerOrderFilterFragData) {
        mSellerOrderFilterFragData = sellerOrderFilterFragData;/*updating data model*/
        mPageNumber = DEFAULT_PAGE_COUNT;
        isFirstCall = true;
        callApi();
    }
}

