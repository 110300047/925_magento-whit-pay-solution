package com.webkul.mobikulmp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerOrderItemAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivityShipmentDetailsBinding;
import com.webkul.mobikulmp.model.ShipmentDetailsResponse;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SHIPMENT_ID;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_AUTH_FAILURE;

public class ShipmentDetailsActivity extends BaseActivity {

    private ActivityShipmentDetailsBinding mBinding;
    private String incrementId;
    private int shipmentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_shipment_details);
        incrementId = getIntent().getStringExtra(BUNDLE_KEY_INCREMENT_ID);
        shipmentId = getIntent().getIntExtra(BUNDLE_KEY_SHIPMENT_ID, 0);

        mSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mSweetAlertDialog.setTitleText(getString(com.webkul.mobikul.R.string.please_wait));
        mSweetAlertDialog.setCancelable(false);
        mSweetAlertDialog.show();
        MpApiConnection.getShipmentDetails(ShipmentDetailsActivity.this, incrementId, shipmentId, shipmentDetailsResponseCallback);

    }

    private Callback<ShipmentDetailsResponse> shipmentDetailsResponseCallback = new Callback<ShipmentDetailsResponse>() {
        @Override
        public void onResponse(Call<ShipmentDetailsResponse> call, Response<ShipmentDetailsResponse> response) {
            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE) {
                MpApiConnection.getShipmentDetails(ShipmentDetailsActivity.this, incrementId, shipmentId, shipmentDetailsResponseCallback);
                return;
            }
            onResponseRecieved(response.body());
        }

        @Override
        public void onFailure(Call<ShipmentDetailsResponse> call, Throwable t) {
            NetworkHelper.onFailure(t, ShipmentDetailsActivity.this);
        }
    };

    public void onResponseRecieved(ShipmentDetailsResponse data) {
        ApplicationSingleton.getInstance().setAuthKey(data.getAuthKey());
        mBinding.setData(data);
        mSweetAlertDialog.dismiss();

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setTitle(data.getMainHeading());
        }

        mBinding.shipmentItemsRv.setLayoutManager(new LinearLayoutManager(ShipmentDetailsActivity.this));
//        mBinding.shipmentItemsRv.setAdapter(new SellerOrderItemAdapter(ShipmentDetailsActivity.this, data.getItems()));
    }

}