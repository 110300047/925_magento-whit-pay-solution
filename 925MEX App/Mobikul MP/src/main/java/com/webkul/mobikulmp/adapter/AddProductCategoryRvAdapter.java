package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemNewProductCategoryBinding;
import com.webkul.mobikulmp.databinding.ItemNewProductCategoryChildBinding;
import com.webkul.mobikulmp.model.seller.CustomCategoryData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 13/10/16. @Webkul Software Pvt. Ltd
 */
public class AddProductCategoryRvAdapter extends ExpandableRecyclerAdapter<CustomCategoryData, CustomCategoryData, AddProductCategoryRvAdapter.CategoryParentViewHolder, AddProductCategoryRvAdapter.CategoryChildViewHolder> {
    private Context mContext;
    private List<CustomCategoryData> mCategoriesData;

    public AddProductCategoryRvAdapter(Context context, ArrayList<CustomCategoryData> categoriesData) {
        super(categoriesData);
        mContext = context;
        mCategoriesData = categoriesData;
    }

    @NonNull
    @Override
    public CategoryParentViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View parentView = inflater.inflate(R.layout.item_new_product_category, parentViewGroup, false);
        return new AddProductCategoryRvAdapter.CategoryParentViewHolder(parentView);
    }

    @NonNull
    @Override
    public CategoryChildViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View childView = inflater.inflate(R.layout.item_new_product_category_child, childViewGroup, false);
        return new AddProductCategoryRvAdapter.CategoryChildViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(@NonNull CategoryParentViewHolder parentViewHolder, int parentPosition, @NonNull CustomCategoryData parent) {
        final CustomCategoryData categoriesData = mCategoriesData.get(parentViewHolder.getAdapterPosition());
        parentViewHolder.mBinding.setData(categoriesData);
        if (categoriesData.getChildrenData() == null || categoriesData.getChildrenData().size() == 0) {
            parentViewHolder.mBinding.arrowIv.setImageDrawable(null);
        }
        parentViewHolder.mBinding.executePendingBindings();
    }

    @Override
    public void onBindChildViewHolder(@NonNull CategoryChildViewHolder childViewHolder, int parentPosition, int childPosition, @NonNull CustomCategoryData subCategoryData) {
        childViewHolder.mBinding.setData(subCategoryData);
        childViewHolder.mBinding.executePendingBindings();
    }

    static class CategoryParentViewHolder extends ParentViewHolder {
        private ItemNewProductCategoryBinding mBinding;

        private CategoryParentViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }

    static class CategoryChildViewHolder extends ChildViewHolder {
        private final ItemNewProductCategoryChildBinding mBinding;

        private CategoryChildViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}