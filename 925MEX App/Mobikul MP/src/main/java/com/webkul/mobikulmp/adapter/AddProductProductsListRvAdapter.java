package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerAddProductBinding;
import com.webkul.mobikulmp.model.seller.ProductCollectionData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class AddProductProductsListRvAdapter extends RecyclerView.Adapter<AddProductProductsListRvAdapter.ViewHolder> {
    private final Context mContext;
    private final ArrayList<ProductCollectionData> mProductsList;

    public AddProductProductsListRvAdapter(Context context, ArrayList<ProductCollectionData> productList) {
        mContext = context;
        mProductsList = productList;
    }

    @Override
    public AddProductProductsListRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_seller_add_product, parent, false));
    }

    @Override
    public void onBindViewHolder(AddProductProductsListRvAdapter.ViewHolder holder, int position) {
        final ProductCollectionData eachProductData = mProductsList.get(position);
        holder.mBinding.setData(eachProductData);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mProductsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemSellerAddProductBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
