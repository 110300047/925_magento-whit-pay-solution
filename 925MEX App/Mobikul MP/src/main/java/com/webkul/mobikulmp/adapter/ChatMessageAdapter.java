package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.helper.MpAppSharedPref;
import com.webkul.mobikulmp.model.chat.ChatMessage;
import com.webkul.mobikulmp.model.chat.ChatMessageViewHolder;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/7/17.
 */

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageViewHolder> {
    private List<ChatMessage> messageList;
    private Context mContext;

    public ChatMessageAdapter(Context mContext, List<ChatMessage> messageList) {
        this.messageList = messageList;
        this.mContext = mContext;
    }

    @Override
    public ChatMessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View messageView = inflater.inflate(R.layout.chat_message, parent, false);
        return new ChatMessageViewHolder(messageView);
    }

    @Override
    public void onBindViewHolder(ChatMessageViewHolder holder, int position) {
        ChatMessage chatMessage = messageList.get(position);
        if (MpAppSharedPref.isAdmin(mContext)) {
            if (chatMessage.getSender().equalsIgnoreCase(mContext.getString(R.string.admin))) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    holder.mBinding.mainContainer.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                }
                holder.mBinding.message.setBackgroundResource(R.drawable.bubble_right);
//                holder.mBinding.msgContainer.setBackgroundResource(android.R.color.white);
                holder.mBinding.chatMessageIv.setVisibility(View.GONE);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    holder.mBinding.mainContainer.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                }
                holder.mBinding.message.setBackgroundResource(R.drawable.bubble_left);
//                holder.mBinding.msgContainer.setBackgroundResource(R.color.grey_300);
                holder.mBinding.chatMessageIv.setVisibility(View.VISIBLE);
            }
        } else {
            if (chatMessage.getSender().equalsIgnoreCase(mContext.getString(R.string.admin))) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    holder.mBinding.mainContainer.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                }
                holder.mBinding.message.setBackgroundResource(R.drawable.bubble_left);
//                holder.mBinding.msgContainer.setBackgroundResource(R.color.grey_300);
                holder.mBinding.chatMessageIv.setVisibility(View.VISIBLE);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    holder.mBinding.mainContainer.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                }
                holder.mBinding.message.setBackgroundResource(R.drawable.bubble_right);
//                holder.mBinding.msgContainer.setBackgroundResource(android.R.color.white);
                holder.mBinding.chatMessageIv.setVisibility(View.GONE);
            }

        }
        holder.mBinding.setData(chatMessage);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }
}
