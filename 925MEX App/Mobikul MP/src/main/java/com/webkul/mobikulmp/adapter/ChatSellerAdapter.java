package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.handler.ChatSellerHandler;
import com.webkul.mobikulmp.model.chat.ChatSellerData;
import com.webkul.mobikulmp.model.chat.ChatSellerViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 30/7/17.
 */

public class ChatSellerAdapter extends RecyclerView.Adapter <ChatSellerViewHolder> implements Filterable{

    private final Context mContext;
    private List<ChatSellerData> sellerList;
    private List<ChatSellerData> filteredSellerList;
    public SellerFilter sellerFilter;


    public ChatSellerAdapter(Context mContext, List<ChatSellerData> mItems) {
        this.mContext = mContext;
        this.sellerList = mItems;
        this.filteredSellerList = new ArrayList<>();
    }

    @Override
    public ChatSellerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View sellerView = inflater.inflate(R.layout.chat_seller_details,parent,false);

        return new ChatSellerViewHolder(sellerView);
    }

    @Override
    public void onBindViewHolder(ChatSellerViewHolder holder, int position) {
        final ChatSellerData sellerData = sellerList.get(position);
        holder.mBinding.setSeller(sellerData);
        holder.mBinding.setHandler(new ChatSellerHandler(mContext));
        holder.mBinding.executePendingBindings();

    }

    @Override
    public int getItemCount() {
        return sellerList.size();
    }

    @Override
    public Filter getFilter() {
        if (sellerFilter == null) {

            filteredSellerList.clear();
            filteredSellerList.addAll(this.sellerList);
            sellerFilter = new SellerFilter(this,filteredSellerList );
        }

        return sellerFilter;
    }


    private static class  SellerFilter extends Filter{

        private ChatSellerAdapter sellerAdapter;
        private List<ChatSellerData> originalList;
        private List<ChatSellerData> filteredList;

        public SellerFilter(ChatSellerAdapter sellerAdapter, List<ChatSellerData> originalList) {
            this.sellerAdapter = sellerAdapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<ChatSellerData>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final ChatSellerData seller : originalList) {

                    if (seller.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(seller);
                    }

                }
            }

            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            sellerAdapter.sellerList.clear();
            sellerAdapter.sellerList.addAll((ArrayList<ChatSellerData>)results.values);
            sellerAdapter.notifyDataSetChanged();

        }
    }
}
