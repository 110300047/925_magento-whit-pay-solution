package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemCreditMemoBinding;
import com.webkul.mobikulmp.handler.CreditMemoListHandler;
import com.webkul.mobikulmp.model.CreditMemoLabels;
import com.webkul.mobikulmp.model.ItemCreditMemo;

import java.util.List;

/**
 * Created by vedesh.kumar on 1/6/17.
 */

public class CreditMemoListRvAdapter extends RecyclerView.Adapter<CreditMemoListRvAdapter.ViewHolder> {

    private final Context mContext;
    private final List<ItemCreditMemo> creditMemoList;
    private final CreditMemoLabels memoLabels;

    public CreditMemoListRvAdapter(Context mContext, List<ItemCreditMemo> creditMemoList, CreditMemoLabels memoLabels) {
        this.mContext = mContext;
        this.creditMemoList = creditMemoList;
        this.memoLabels = memoLabels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View creditMemoItemView = inflater.inflate(R.layout.item_credit_memo,parent,false);
        return new ViewHolder(creditMemoItemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ItemCreditMemo itemCreditMemo = creditMemoList.get(position);
        holder.mBinding.setData(itemCreditMemo);
        holder.mBinding.setLabeldata(memoLabels);
        holder.mBinding.setHandler(new CreditMemoListHandler(mContext));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return creditMemoList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        private final ItemCreditMemoBinding mBinding;
        public ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
