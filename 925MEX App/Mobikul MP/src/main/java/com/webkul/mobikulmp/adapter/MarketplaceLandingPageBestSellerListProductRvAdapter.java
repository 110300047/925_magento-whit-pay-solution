package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.model.catalog.ProductData;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemMarketplaceLandingPageSellerProductBinding;
import com.webkul.mobikulmp.handler.MarketplaceLandingPageProductHandler;

import java.util.List;

/**
 * Created by shubham.agarwal on 15/2/17. @Webkul Software Pvt. Ltd
 */

class MarketplaceLandingPageBestSellerListProductRvAdapter extends RecyclerView.Adapter<MarketplaceLandingPageBestSellerListProductRvAdapter.ViewHolder> {

    private final Context mContext;
    private final List<ProductData> mProductList;

    MarketplaceLandingPageBestSellerListProductRvAdapter(Context context, List<ProductData> productList) {
        mContext = context;
        mProductList = productList;
    }

    @Override
    public MarketplaceLandingPageBestSellerListProductRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_marketplace_landing_page_seller_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MarketplaceLandingPageBestSellerListProductRvAdapter.ViewHolder holder, int position) {
        final ProductData marketplaceLandingPageProductData = mProductList.get(position);
        holder.mBinding.setData(marketplaceLandingPageProductData);
        holder.mBinding.setHandler(new MarketplaceLandingPageProductHandler(mContext));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemMarketplaceLandingPageSellerProductBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
