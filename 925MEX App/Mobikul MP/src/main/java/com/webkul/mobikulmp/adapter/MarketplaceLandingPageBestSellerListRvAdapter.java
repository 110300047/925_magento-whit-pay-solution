package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemMarketplaceLandingPageSellerBinding;
import com.webkul.mobikulmp.handler.MarketplaceLandingPageSellerHandler;
import com.webkul.mobikulmp.model.landingpage.SellersData;

import java.util.List;

/**
 * Created by shubham.agarwal on 15/2/17. @Webkul Software Pvt. Ltd
 */

public class MarketplaceLandingPageBestSellerListRvAdapter extends RecyclerView.Adapter<MarketplaceLandingPageBestSellerListRvAdapter.ViewHolder> {

    private final Context mContext;
    private final List<SellersData> mSellerList;

    public MarketplaceLandingPageBestSellerListRvAdapter(Context context, List<SellersData> sellerList) {
        mContext = context;
        mSellerList = sellerList;
    }

    @Override
    public MarketplaceLandingPageBestSellerListRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_marketplace_landing_page_seller, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MarketplaceLandingPageBestSellerListRvAdapter.ViewHolder holder, int position) {
        final SellersData marketplaceLandingPageSellerData = mSellerList.get(position);
        holder.mBinding.setData(marketplaceLandingPageSellerData);
        holder.mBinding.setHandler(new MarketplaceLandingPageSellerHandler(mContext));
        holder.mBinding.sellerProductRv.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        holder.mBinding.sellerProductRv.setAdapter(new MarketplaceLandingPageBestSellerListProductRvAdapter(mContext, marketplaceLandingPageSellerData.getProducts()));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mSellerList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemMarketplaceLandingPageSellerBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
