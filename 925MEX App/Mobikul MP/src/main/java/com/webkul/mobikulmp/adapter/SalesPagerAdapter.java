package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerSalesPagerBinding;
import com.webkul.mobikulmp.handler.SellerSalesPagerItemHandler;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class SalesPagerAdapter extends PagerAdapter {

    private final String[] tabTitles;
    private final Context mContext;
    private final ArrayList<String> mListOfLocationReportImages;
    private final ArrayList<String> mListOfStatsImages;
    private final String mCategoryChartImage;

    public SalesPagerAdapter(Context context, ArrayList<String> listOfLocationReportImages, ArrayList<String> listOfStatsImages, String categoryChartImage) {
        mContext = context;
        this.mListOfLocationReportImages = listOfLocationReportImages;
        this.mListOfStatsImages = listOfStatsImages;
        this.mCategoryChartImage = categoryChartImage;
        tabTitles = new String[]{
                mContext.getString(R.string.sales_by_locations)
                , mContext.getString(R.string.sales_stats)
                , mContext.getString(R.string.top_selling_category)
        };
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ItemSellerSalesPagerBinding itemSellerSalesPagerBinding = DataBindingUtil.bind(LayoutInflater.from(mContext).inflate(R.layout.item_seller_sales_pager, container, false));
        itemSellerSalesPagerBinding.setPosition(position);
        switch (position) {
            case 0:
                itemSellerSalesPagerBinding.setImageUrl(mListOfLocationReportImages.get(0));
                itemSellerSalesPagerBinding.setHandler(new SellerSalesPagerItemHandler(itemSellerSalesPagerBinding, mListOfLocationReportImages));
                break;
            case 1:
                itemSellerSalesPagerBinding.setImageUrl(mListOfStatsImages.get(0));
                itemSellerSalesPagerBinding.setHandler(new SellerSalesPagerItemHandler(itemSellerSalesPagerBinding, mListOfStatsImages));
                break;
            case 2:
                itemSellerSalesPagerBinding.setImageUrl(mCategoryChartImage);
                break;
        }
        itemSellerSalesPagerBinding.executePendingBindings();
        container.addView(itemSellerSalesPagerBinding.getRoot());
        return (itemSellerSalesPagerBinding.getRoot());
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
