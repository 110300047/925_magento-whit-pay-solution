package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerFeedbackBinding;
import com.webkul.mobikulmp.model.seller.SellerReviewList;

import java.util.List;

/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */
public class SellerFeedbackRvAdapter extends RecyclerView.Adapter<SellerFeedbackRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<SellerReviewList> mSellerReviewDatas;

    public SellerFeedbackRvAdapter(Context context, List<SellerReviewList> sellerReviewDatas) {
        mContext = context;
        mSellerReviewDatas = sellerReviewDatas;
    }

    @Override
    public SellerFeedbackRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_seller_feedback, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SellerFeedbackRvAdapter.ViewHolder holder, int position) {
        final SellerReviewList sellerReviewData = mSellerReviewDatas.get(position);
        holder.mBinding.setData(sellerReviewData);
    }

    @Override
    public int getItemCount() {
        return mSellerReviewDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemSellerFeedbackBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
