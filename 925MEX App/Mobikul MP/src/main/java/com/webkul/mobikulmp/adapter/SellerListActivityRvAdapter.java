package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerListBinding;
import com.webkul.mobikulmp.handler.SellerListItemHandler;
import com.webkul.mobikulmp.model.SellerListData;

import java.util.List;

/**
 * Created by shubham.agarwal on 14/2/17. @Webkul Software Pvt. Ltd
 */
public class SellerListActivityRvAdapter extends RecyclerView.Adapter<SellerListActivityRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<SellerListData> mSellerListDatas;

    public SellerListActivityRvAdapter(Context context, List<SellerListData> sellerListDatas) {
        mContext = context;
        mSellerListDatas = sellerListDatas;
    }


    @Override
    public SellerListActivityRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_seller_list, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(SellerListActivityRvAdapter.ViewHolder holder, int position) {
        final SellerListData sellerListData = mSellerListDatas.get(position);
        holder.mBinding.setData(sellerListData);
        holder.mBinding.setHandler(new SellerListItemHandler(mContext));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mSellerListDatas.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemSellerListBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
