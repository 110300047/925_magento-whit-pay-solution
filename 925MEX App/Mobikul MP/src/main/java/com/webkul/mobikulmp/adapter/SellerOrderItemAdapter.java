package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerOrderedItemBinding;
import com.webkul.mobikulmp.model.seller.CustomOption;
import com.webkul.mobikulmp.model.seller.SellerOrderItemList;
import com.webkul.mobikulmp.model.seller.SellerOrderItemQty;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 26/5/17. @Webkul Software Private limited
 */

public class SellerOrderItemAdapter extends RecyclerView.Adapter<SellerOrderItemAdapter.ViewHolder> {
    private final Context mContext;
    private final ArrayList<SellerOrderItemList> orderItemDataList;

    public SellerOrderItemAdapter(Context mContext, ArrayList<SellerOrderItemList> orderItemDataList) {
        this.mContext = mContext;
        this.orderItemDataList = orderItemDataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View orderedItemView = inflater.inflate(R.layout.item_seller_ordered_item, parent, false);
        return new ViewHolder(orderedItemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final SellerOrderItemList itemData = orderItemDataList.get(position);
        holder.mBinding.setData(itemData);
        holder.mBinding.optionsLayout.removeAllViews();
        for (int optionIterator = 0; optionIterator < itemData.getCustomOption().size(); optionIterator++) {
            CustomOption optionItem = itemData.getCustomOption().get(optionIterator);

            TableRow tableRow = new TableRow(mContext);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
            tableRow.setBackgroundResource(com.webkul.mobikul.R.drawable.shape_rectangular_white_bg_gray_border_1dp);
            tableRow.setGravity(Gravity.CENTER_VERTICAL);

            TextView labelTv = new TextView(mContext);
            labelTv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            labelTv.setMaxWidth((int) (Utils.getScreenWidth() / 2.5));
            labelTv.setPadding(10, 5, 10, 5);
            labelTv.setTextSize(10);
            labelTv.setBackgroundResource(com.webkul.mobikul.R.color.grey_200);
            labelTv.setText(optionItem.getLabel());
            tableRow.addView(labelTv);

            TextView val = new TextView(mContext);
            val.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            val.setPadding(10, 5, 10, 5);
            val.setTextSize(10);
            val.setText(optionItem.getValue());
            tableRow.addView(val);
            holder.mBinding.optionsLayout.addView(tableRow);

        }
        holder.mBinding.orderedItemsQty.removeAllViews();
        for (int optionIterator = 0; optionIterator < itemData.getQty().size(); optionIterator++) {

            SellerOrderItemQty optionItemQty = itemData.getQty().get(optionIterator);

            TableRow tableRow = new TableRow(mContext);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            params.weight = 1;

            TextView labelTv = new TextView(mContext);
            labelTv.setLayoutParams(params);
            labelTv.setPadding(14, 0, 0, 0);
            labelTv.setTextSize(14);
            labelTv.setText(optionItemQty.getLabel());
            tableRow.addView(labelTv);

            TextView colonTv = new TextView(mContext);
            colonTv.setTextSize(14);
            colonTv.setText(":");
            tableRow.addView(colonTv);

            TextView val = new TextView(mContext);
            val.setLayoutParams(params);
            val.setPadding(0, 0, 14, 0);
            val.setGravity(Gravity.END);
            val.setTextSize(14);
            val.setText(optionItemQty.getValue());
            tableRow.addView(val);
            holder.mBinding.orderedItemsQty.addView(tableRow);

        }
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return orderItemDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemSellerOrderedItemBinding mBinding;

        public ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}