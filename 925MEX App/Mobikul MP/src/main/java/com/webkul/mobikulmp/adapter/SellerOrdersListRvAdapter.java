package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerOrderBinding;
import com.webkul.mobikulmp.databinding.ItemSellerOrderProductBinding;
import com.webkul.mobikulmp.handler.SellerOrderItemRecyclerHandler;
import com.webkul.mobikulmp.handler.SellerOrderProductHandler;
import com.webkul.mobikulmp.model.seller.SellerOrderData;
import com.webkul.mobikulmp.model.seller.SellerOrderProductList;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class SellerOrdersListRvAdapter extends RecyclerView.Adapter<SellerOrdersListRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<SellerOrderData> mOrderLists;

    public SellerOrdersListRvAdapter(Context context, List<SellerOrderData> orderList) {
        mContext = context;
        mOrderLists = orderList;
    }

    @Override
    public SellerOrdersListRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_seller_order, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(SellerOrdersListRvAdapter.ViewHolder holder, int position) {
        final SellerOrderData eachOrderData = mOrderLists.get(position);
        if (holder.mBinding.productListContainer.getChildCount() > 0) {
            holder.mBinding.productListContainer.removeAllViews();
        }

        if (eachOrderData.getSellerOrderProductList() != null && eachOrderData.getSellerOrderProductList().size() != 0) {
            for (SellerOrderProductList eachSellerOrderProduct : eachOrderData.getSellerOrderProductList()) {
                ItemSellerOrderProductBinding itemSellerOrderProductBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), com.webkul.mobikulmp.R.layout.item_seller_order_product, holder.mBinding.productListContainer, true);
                itemSellerOrderProductBinding.setData(eachSellerOrderProduct);
                itemSellerOrderProductBinding.setHandler(new SellerOrderProductHandler(mContext, eachSellerOrderProduct));
            }
        }
        holder.mBinding.setData(eachOrderData);
        holder.mBinding.setHandler(new SellerOrderItemRecyclerHandler());
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mOrderLists.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemSellerOrderBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
