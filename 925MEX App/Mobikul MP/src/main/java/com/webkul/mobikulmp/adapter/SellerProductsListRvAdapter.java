package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerProductBinding;
import com.webkul.mobikulmp.handler.ItemSellerProductListHandler;
import com.webkul.mobikulmp.model.seller.SellerProductData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class SellerProductsListRvAdapter extends RecyclerView.Adapter<SellerProductsListRvAdapter.ViewHolder> {
    private final Context mContext;
    private final ArrayList<SellerProductData> mProductsList;

    private SparseBooleanArray selectedItems;

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            mProductsList.get(pos).setSelected(false);
        } else {
            selectedItems.put(pos, true);
            mProductsList.get(pos).setSelected(true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        for (int noOfProduct = 0; noOfProduct < mProductsList.size(); noOfProduct++) {
            mProductsList.get(noOfProduct).setSelected(false);
            mProductsList.get(noOfProduct).setSelectionModeOn(false);
        }
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public SellerProductData getItem(int position) {
        return mProductsList.get(position);
    }


    public SellerProductsListRvAdapter(Context context, ArrayList<SellerProductData> transactionList) {
        mContext = context;
        mProductsList = transactionList;
        selectedItems = new SparseBooleanArray();
    }

    @Override
    public SellerProductsListRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_seller_product, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(SellerProductsListRvAdapter.ViewHolder holder, int position) {
        final SellerProductData eachProductData = mProductsList.get(position);
        eachProductData.setPosition(position);
        holder.mBinding.setData(eachProductData);
        holder.mBinding.setHandler(new ItemSellerProductListHandler(mContext));
        holder.itemView.setActivated(selectedItems.get(position, false));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mProductsList.size();
    }

    public void remove(int position) {
        mProductsList.remove(position);
        notifyItemRemoved(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemSellerProductBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
