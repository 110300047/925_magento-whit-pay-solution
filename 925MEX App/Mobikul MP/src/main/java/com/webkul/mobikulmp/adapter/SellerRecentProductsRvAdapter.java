package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.R;
import com.webkul.mobikul.databinding.ItemProductGridViewBinding;
import com.webkul.mobikul.handler.HomePageProductHandler;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class SellerRecentProductsRvAdapter extends RecyclerView.Adapter<SellerRecentProductsRvAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<ProductData> mSellerRecentProductsDatas;

    public SellerRecentProductsRvAdapter(Context context, ArrayList<ProductData> sellerRecentProductsDatas) {
        mContext = context;
        mSellerRecentProductsDatas = sellerRecentProductsDatas;
    }

    @Override
    public SellerRecentProductsRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_product_grid_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SellerRecentProductsRvAdapter.ViewHolder holder, int position) {
        holder.mBinding.mainLayout.setVisibility(View.VISIBLE);
        holder.mBinding.viewAllBtn.setVisibility(View.GONE);
        holder.mBinding.productContainer.setVisibility(View.VISIBLE);
        holder.mBinding.addToCompareIv.setVisibility(View.VISIBLE);
        holder.mBinding.wishlistAnimationView.setVisibility(View.VISIBLE);
        final ProductData featuredProductData = mSellerRecentProductsDatas.get(position);
        featuredProductData.setProductPosition(position);
        holder.mBinding.setData(featuredProductData);
        if (featuredProductData.isInWishlist())
            holder.mBinding.wishlistAnimationView.setProgress(1);
        else
            holder.mBinding.wishlistAnimationView.setProgress(0);
        holder.mBinding.executePendingBindings();
        holder.mBinding.setHandler(new HomePageProductHandler(mContext, mContext.getString(R.string.category_seller_product_identifier), mSellerRecentProductsDatas));
    }

    @Override
    public int getItemCount() {
        return mSellerRecentProductsDatas.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemProductGridViewBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}