package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerTransactionBinding;
import com.webkul.mobikulmp.handler.SellerTransactionItemRecyclerHandler;
import com.webkul.mobikulmp.model.seller.SellerTransactionList;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class SellerTransactionsListRvAdapter extends RecyclerView.Adapter<SellerTransactionsListRvAdapter.ViewHolder> {
    private final Context mContext;
    private final List<SellerTransactionList> mTransactionList;

    public SellerTransactionsListRvAdapter(Context context, List<SellerTransactionList> transactionList) {
        mContext = context;
        mTransactionList = transactionList;
    }

    @Override
    public SellerTransactionsListRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View contactView = inflater.inflate(R.layout.item_seller_transaction, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(SellerTransactionsListRvAdapter.ViewHolder holder, int position) {
        final SellerTransactionList eachTransactionData = mTransactionList.get(position);
        holder.mBinding.setData(eachTransactionData);
        holder.mBinding.setHandler(new SellerTransactionItemRecyclerHandler());
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mTransactionList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemSellerTransactionBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
