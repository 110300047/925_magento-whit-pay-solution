package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemTopSellingProductBinding;
import com.webkul.mobikulmp.handler.TopSellingProductsHandler;
import com.webkul.mobikulmp.model.seller.TopSellingProduct;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/1/17. @Webkul Software Pvt. Ltd
 */

public class TopSellingProductsRvAdapter extends RecyclerView.Adapter<TopSellingProductsRvAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<TopSellingProduct> mTopSellingProductsData;

    public TopSellingProductsRvAdapter(Context context, ArrayList<TopSellingProduct> topSellingProductsDatas) {
        mContext = context;
        mTopSellingProductsData = topSellingProductsDatas;
    }

    @Override
    public TopSellingProductsRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_top_selling_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TopSellingProductsRvAdapter.ViewHolder holder, int position) {
        final TopSellingProduct topSellingProduct = mTopSellingProductsData.get(position);
        holder.mBinding.setData(topSellingProduct);
        holder.mBinding.setHandler(new TopSellingProductsHandler());
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mTopSellingProductsData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemTopSellingProductBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
