package com.webkul.mobikulmp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerTransactionOrderBinding;
import com.webkul.mobikulmp.model.seller.TransactionOrderList;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 23/12/16. @Webkul Software Pvt. Ltd
 */

public class TransactionOrderListRvAdapter extends RecyclerView.Adapter<TransactionOrderListRvAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<TransactionOrderList> mTransactionOrderList;

    public TransactionOrderListRvAdapter(Context context, ArrayList<TransactionOrderList> transactionOrderList) {
        mContext = context;
        mTransactionOrderList = transactionOrderList;
    }

    @Override
    public TransactionOrderListRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_seller_transaction_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TransactionOrderListRvAdapter.ViewHolder holder, int position) {
        final TransactionOrderList eachTransactionOrderData = mTransactionOrderList.get(position);
        holder.mBinding.setData(eachTransactionOrderData);
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mTransactionOrderList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemSellerTransactionOrderBinding mBinding;

        private ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}