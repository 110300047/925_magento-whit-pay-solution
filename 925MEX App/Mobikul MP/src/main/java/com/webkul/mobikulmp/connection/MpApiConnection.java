package com.webkul.mobikulmp.connection;

import android.content.Context;

import com.webkul.mobikul.connection.RetrofitClient;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.CatalogProductData;
import com.webkul.mobikulmp.model.AskQuesToAdminRequestData;
import com.webkul.mobikulmp.model.BecomePartnerResponseData;
import com.webkul.mobikulmp.model.CancelOrderResponseData;
import com.webkul.mobikulmp.model.ContactSellerRequestData;
import com.webkul.mobikulmp.model.CreateInvoiceResponseData;
import com.webkul.mobikulmp.model.CreditMemoDetailsResponseData;
import com.webkul.mobikulmp.model.CreditMemoListResponseData;
import com.webkul.mobikulmp.model.InvoiceDetailResponseData;
import com.webkul.mobikulmp.model.SellerListResponseData;
import com.webkul.mobikulmp.model.SellerOrderFilterFragData;
import com.webkul.mobikulmp.model.SellerProfileData;
import com.webkul.mobikulmp.model.SellerReviewListData;
import com.webkul.mobikulmp.model.SendCreditMemoMailResponseData;
import com.webkul.mobikulmp.model.SendInvoiceOrderMailResponse;
import com.webkul.mobikulmp.model.SendOrderMailResponse;
import com.webkul.mobikulmp.model.ShipmentDetailsResponse;
import com.webkul.mobikulmp.model.chat.ChatSellerListResponseData;
import com.webkul.mobikulmp.model.landingpage.MarketplaceLandingPageData;
import com.webkul.mobikulmp.model.pdfheader.PdfHeaderInfoDataResponse;
import com.webkul.mobikulmp.model.seller.AddProductData;
import com.webkul.mobikulmp.model.seller.AddProductFieldProductCollectionData;
import com.webkul.mobikulmp.model.seller.CheckSkuResponseData;
import com.webkul.mobikulmp.model.seller.SaveProductResponseData;
import com.webkul.mobikulmp.model.seller.SellerAddProductResponseData;
import com.webkul.mobikulmp.model.seller.SellerDashboardData;
import com.webkul.mobikulmp.model.seller.SellerOrderDetailResponseData;
import com.webkul.mobikulmp.model.seller.SellerOrderListData;
import com.webkul.mobikulmp.model.seller.SellerProductListResponseData;
import com.webkul.mobikulmp.model.seller.SellerProfileFormResponseData;
import com.webkul.mobikulmp.model.seller.SellerTransactionListResponseData;
import com.webkul.mobikulmp.model.seller.ViewTransactionResponseData;

import org.json.JSONArray;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Callback;

/**
 * Created by shubham.agarwal on 16/3/17. @Webkul Software Pvt. Ltd
 */

public class MpApiConnection {

/* Marketplace Apis*/

    public static void getLandingPageData(Context context, String apiUserName, String apiPassword, Callback<MarketplaceLandingPageData>
            callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getLandingPageData(
                AppSharedPref.getStoreId(context)
                , Utils.getScreenWidth()
                , context.getResources().getDisplayMetrics().density
        ).enqueue(callback);
    }


    public static void getSellerList(Context context, String storeName, Callback<SellerListResponseData> sellerListResponseDataCallback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSellerList(
                storeName
                , Utils.getScreenWidth()
                , AppSharedPref.getStoreId(context)
                , context.getResources().getDisplayMetrics().density
        ).enqueue(sellerListResponseDataCallback);
    }

    public static void getSellerProfileData(Context context, int sellerId, Callback<SellerProfileData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSellerProfileData(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , sellerId
                , Utils.getScreenWidth()
        ).enqueue(callback);
    }

    public static void getSellerCollectionData(Context context, int sellerId, int pageNumber, int categoryId, Callback<CatalogProductData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSellerCollectionData(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , sellerId
                , Utils.getScreenWidth()
                , pageNumber
                , categoryId
        ).enqueue(callback);
    }

    public static void getSellerReviews(Context context, int sellerId, int pageNumber, Callback<SellerReviewListData>
            callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSellerReviews(
                AppSharedPref.getStoreId(context)
                , sellerId
                , Utils.getScreenWidth()
                , pageNumber
        ).enqueue(callback);
    }

    public static void saveReview(Context context, int sellerId, String nickName, String summary, String details
            , int price, int value, int quantity, String shopUrl, Callback<BaseModel> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).saveReview(
                sellerId
                , AppSharedPref.getCustomerId(context)
                , AppSharedPref.getCustomerEmail(context)
                , nickName
                , summary
                , details
                , price
                , value
                , quantity
                , shopUrl
        ).enqueue(callback);
    }

    public static void contactSeller(Context context, ContactSellerRequestData data, int sellerId, int productId, Callback<BaseModel> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).contactSeller(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , sellerId
                , productId
                , data.getNickname()
                , data.getEmail()
                , data.getSubject()
                , data.getQuery()
        ).enqueue(callback);
    }


    public static void askQuestionToAdmin(Context context, AskQuesToAdminRequestData data, Callback<BaseModel> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).askQuestionToAdmin(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , data.getSubject()
                , data.getQuery()
        ).enqueue(callback);
    }

    public static void getsellerdashboardData(Context context, String apiUserName, String apiPassword, Callback<SellerDashboardData>
            callback) {
        AlertDialogHelper.showDefaultAlertDialog(context);
        RetrofitClient.getClient().create(MpApiInterface.class).getsellerdashboardData(
                AppSharedPref.getCustomerId(context)
                , AppSharedPref.getStoreId(context)
                , 700
                , 1
        ).enqueue(callback);
    }

    public static void getsellerProfileFormData(Context context, Callback<SellerProfileFormResponseData> callback) {
        AlertDialogHelper.showDefaultAlertDialog(context);
        RetrofitClient.getClient().create(MpApiInterface.class).getsellerProfileFormData(
                AppSharedPref.getCustomerId(context)
                , AppSharedPref.getStoreId(context)
        ).enqueue(callback);
    }

    public static void getDeleteSellerImage(Context context, String entity, Callback<BaseModel> callback) {
        AlertDialogHelper.showDefaultAlertDialog(context);
        RetrofitClient.getClient().create(MpApiInterface.class).getDeleteSellerImage(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , entity
        ).enqueue(callback);
    }

    public static void saveSellerProfileData(Context context, SellerProfileFormResponseData profileData, RequestBody companyBanner, RequestBody companyLogo, Callback<BaseModel> callback) {
        AlertDialogHelper.showDefaultAlertDialog(context);
        try {
            RetrofitClient.getClient().create(MpApiInterface.class).saveSellerProfileData(
                    AppSharedPref.getStoreId(context)
                    , AppSharedPref.getCustomerId(context)
                    , profileData.isIsTwitterActive() ? 1 : 0
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getTwitterId() != null ? profileData.getTwitterId() : "")
                    , profileData.isIsFacebookActive() ? 1 : 0
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getFacebookId() != null ? profileData.getFacebookId() : "")
                    , profileData.isIsInstagramActive() ? 1 : 0
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getInstagramId() != null ? profileData.getInstagramId() : "")
                    , profileData.isIsgoogleplusActive() ? 1 : 0
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getGoogleplusId() != null ? profileData.getGoogleplusId() : "")
                    , profileData.isIsYoutubeActive() ? 1 : 0
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getYoutubeId() != null ? profileData.getYoutubeId() : "")
                    , profileData.isIsVimeoActive() ? 1 : 0
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getVimeoId() != null ? profileData.getVimeoId() : "")
                    , profileData.isIsPinterestActive() ? 1 : 0
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getPinterestId() != null ? profileData.getPinterestId() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getContactNumber() != null ? profileData.getContactNumber() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getTaxvat() != null ? profileData.getTaxvat() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getBackgroundColor() != null ? profileData.getBackgroundColor() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getShopTitle() != null ? profileData.getShopTitle() : "")
                    , companyBanner
                    , companyLogo
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getCompanyLocality() != null ? profileData.getCompanyLocality() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getCompanyDescription() != null ? profileData.getCompanyDescription() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getReturnPolicy() != null ? profileData.getReturnPolicy() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getShippingPolicy() != null ? profileData.getShippingPolicy() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getCountry() != null ? profileData.getCountry() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getMetaKeyword() != null ? profileData.getMetaKeyword() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getMetaDescription() != null ? profileData.getMetaDescription() : "")
                    , RequestBody.create(MediaType.parse("text/plain"), profileData.getPaymentDetails() != null ? profileData.getPaymentDetails() : "")
            ).enqueue(callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getSellerOrderListData(Context context, SellerOrderFilterFragData data, int pageNumber, Callback<SellerOrderListData>
            callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSellerOrderListData(
                AppSharedPref.getCustomerId(context)
                , AppSharedPref.getStoreId(context)
                , data.getOrderStatus()
                , data.getToDate()
                , data.getFromDate()
                , data.getOrderId()
                , pageNumber
        ).enqueue(callback);
    }

    public static void getSellerOrderDetailData(Context context, String incrementId, Callback<SellerOrderDetailResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSellerOrderDetailData(
                AppSharedPref.getCustomerId(context)
                , AppSharedPref.getStoreId(context)
                , incrementId

        ).enqueue(callback);
    }

    public static void getSellerProductsListData(Context context, int pageNumber, String dateTo, String dateFrom, String productName, String productStatus, Callback<SellerProductListResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSellerProductsListData(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , pageNumber
                , dateTo
                , dateFrom
                , productName
                , productStatus
        ).enqueue(callback);
    }

    public static void deleteProduct(Context context, int productId, Callback<BaseModel> callback) {
        AlertDialogHelper.showDefaultAlertDialog(context);
        RetrofitClient.getClient().create(MpApiInterface.class).deleteProduct(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , productId
        ).enqueue(callback);
    }

    public static void massDeleteProduct(Context context, JSONArray productIds, Callback<BaseModel> callback) {
        AlertDialogHelper.showDefaultAlertDialog(context);
        RetrofitClient.getClient().create(MpApiInterface.class).massDeleteProduct(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , productIds
        ).enqueue(callback);
    }

    public static void getSellerTransactionListData(Context context, int pageNumber, String dateTo, String dateFrom, String transactionId, Callback<SellerTransactionListResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSellerTransactionListData(
                AppSharedPref.getCustomerId(context)
                , AppSharedPref.getStoreId(context)
                , pageNumber
                , dateTo
                , dateFrom
                , transactionId

        ).enqueue(callback);
    }

    public static void viewSellerTransaction(Context context, String transactionId, Callback<ViewTransactionResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).viewSellerTransaction(
                AppSharedPref.getCustomerId(context)
                , AppSharedPref.getStoreId(context)
                , transactionId

        ).enqueue(callback);
    }


    public static void getSendOrderMailData(@SuppressWarnings("unused") Context context, String incrementId, Callback<SendOrderMailResponse> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getSendOrderMailData(
                incrementId
        ).enqueue(callback);
    }

    public static void getCreateInvoiceResponseData(@SuppressWarnings("unused") Context context, String incrementId, Callback<CreateInvoiceResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getCreateInvoiceResponseData(
                incrementId
                , AppSharedPref.getCustomerId(context)
        ).enqueue(callback);
    }

    public static void getCancelOrderResponseData(@SuppressWarnings("unused") Context context, String incrementId, Callback<CancelOrderResponseData> callback) {

        RetrofitClient.getClient().create(MpApiInterface.class).getCancelOrderResponseData(
                incrementId
                , AppSharedPref.getCustomerId(context)
        ).enqueue(callback);
    }

    public static void getInvoiceDetails(Context context, String incrementId, int invoiceId, Callback<InvoiceDetailResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getInvoiceDetails(
                AppSharedPref.getCustomerId(context)
                , incrementId
                , invoiceId
                , AppSharedPref.getStoreId(context)
        ).enqueue(callback);
    }

    public static void sendInvoiceMAil(Context context, String incrementId, int invoiceId, Callback<SendInvoiceOrderMailResponse> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).sendInvoiceMail(
                AppSharedPref.getCustomerId(context)
                , incrementId
                , invoiceId
                , AppSharedPref.getStoreId(context)
        ).enqueue(callback);
    }


    public static void getShipmentDetails(Context context, String incrementId, int shipmentId, Callback<ShipmentDetailsResponse> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getShipmentDetails(
                AppSharedPref.getCustomerId(context)
                , incrementId
                , shipmentId
                , AppSharedPref.getStoreId(context)
        ).enqueue(callback);
    }

    public static void getCreditMemoList(Context context, String incrementId, Callback<CreditMemoListResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getCreditMemoListResponseData(
                AppSharedPref.getCustomerId(context)
                , incrementId
                , AppSharedPref.getStoreId(context)
        ).enqueue(callback);
    }

    public static void getCreditMemoDetails(Context context, String incrementId, int creditMemoId, Callback<CreditMemoDetailsResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getCreditMemoDetailsResponseData(
                AppSharedPref.getCustomerId(context)
                , creditMemoId
                , incrementId
                , AppSharedPref.getStoreId(context)
        ).enqueue(callback);
    }

    public static void sendCreditMemoMail(Context context, String incrementId, int creditMemoId, Callback<SendCreditMemoMailResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).sendCreditMemoMail(
                AppSharedPref.getCustomerId(context)
                , creditMemoId
                , incrementId
                , AppSharedPref.getStoreId(context)
        ).enqueue(callback);
    }

    public static void makeSeller(Context context, String storeUrl, Callback<BecomePartnerResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).sendBecomePartnerRequest(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , storeUrl
        ).enqueue(callback);
    }

    public static void getPdfHeaderFormData(Context context, Callback<PdfHeaderInfoDataResponse> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getPdfHeaderFormData(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
        ).enqueue(callback);
    }

    public static void savePdfHeader(Context context, String data, SweetAlertDialog sweetAlertDialog, Callback<PdfHeaderInfoDataResponse> callback) {
        sweetAlertDialog.show();

        RetrofitClient.getClient().create(MpApiInterface.class).savePdfHeader(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , data
        ).enqueue(callback);
    }

    public static void getChatSellerList(Context context, Callback<ChatSellerListResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getChatSellerList(
                AppSharedPref.getCustomerEmail(context)
                , ApplicationConstant.DEFAULT_WEBSITE_ID
                , AppSharedPref.getStoreId(context)
                , context.getResources().getDisplayMetrics().density
        ).enqueue(callback);
    }

    public static void chatNotifyAdmin(String message, String sellerId, String sellerName, Callback<BaseModel> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).chatNotifyAdmin(
                message
                , sellerId
                , sellerName
                , ApplicationConstant.DEFAULT_WEBSITE_ID
        ).enqueue(callback);
    }

    public static void getProductFormData(Context context, String productId, Callback<SellerAddProductResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getProductFormData(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , productId
        ).enqueue(callback);
    }

    public static void checkSku(Context context, String sku, Callback<CheckSkuResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).checkSku(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , sku
        ).enqueue(callback);
    }

    public static void saveProduct(Context context, String productId, AddProductData addProductData, Callback<SaveProductResponseData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).saveProduct(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , productId
                , addProductData.getType()
                , "1"
                , addProductData.getAttributeSetId()
                , addProductData.getWebsiteJSONArray()
                , addProductData.getCategoryIdsJSONArray()
                , addProductData.getName()
                , addProductData.getDescription()
                , addProductData.getShortDescription()
                , addProductData.getSku()
                , addProductData.getPrice()
                , addProductData.getSpecialPrice()
                , addProductData.getSpecialFromDateForRequest()
                , addProductData.getSpecialToDateForRequest()
                , addProductData.getQty()
                , addProductData.isIsInStock() ? 1 : 0
                , addProductData.getVisibility()
                , addProductData.getTaxClassId()
                , addProductData.getProductHasWeight() ? 1 : 0
                , addProductData.getWeight()
                , addProductData.getMetaTitle()
                , addProductData.getMetaKeyword()
                , addProductData.getMetaDescription()
                , addProductData.getMpProductCartLimit()
                , addProductData.getMedialGallery().size() > 0 ? addProductData.getMedialGallery().get(0).getFile() : ""
                , addProductData.getMedialGallery().size() > 0 ? addProductData.getMedialGallery().get(0).getFile() : ""
                , addProductData.getMedialGallery().size() > 0 ? addProductData.getMedialGallery().get(0).getFile() : ""
                , addProductData.getMedialGallery().size() > 0 ? addProductData.getMedialGallery().get(0).getFile() : ""
                , addProductData.getMedialGalleryJSONArray()
                , addProductData.getCrossSellJSONArray()
                , addProductData.getUpsellJSONArray()
                , addProductData.getRelatedJSONArray()
        ).enqueue(callback);
    }

    public static void getRelatedProductData(Context context, String productId, int pageNumber, JSONArray sortData, JSONArray filterData, Callback<AddProductFieldProductCollectionData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getRelatedProductData(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , productId
                , pageNumber
                , sortData
                , filterData
        ).enqueue(callback);
    }

    public static void getUpSellProductData(Context context, String productId, int pageNumber, JSONArray sortData, JSONArray filterData, Callback<AddProductFieldProductCollectionData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getUpSellProductData(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , productId
                , pageNumber
                , sortData
                , filterData
        ).enqueue(callback);
    }

    public static void getCrossSellProductData(Context context, String productId, int pageNumber, JSONArray sortData, JSONArray filterData, Callback<AddProductFieldProductCollectionData> callback) {
        RetrofitClient.getClient().create(MpApiInterface.class).getCrossSellProductData(
                AppSharedPref.getStoreId(context)
                , AppSharedPref.getCustomerId(context)
                , productId
                , pageNumber
                , sortData
                , filterData
        ).enqueue(callback);
    }
}
