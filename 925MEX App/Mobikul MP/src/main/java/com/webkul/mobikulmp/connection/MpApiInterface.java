package com.webkul.mobikulmp.connection;


import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.CatalogProductData;
import com.webkul.mobikulmp.model.BecomePartnerResponseData;
import com.webkul.mobikulmp.model.CancelOrderResponseData;
import com.webkul.mobikulmp.model.CreateInvoiceResponseData;
import com.webkul.mobikulmp.model.CreditMemoDetailsResponseData;
import com.webkul.mobikulmp.model.CreditMemoListResponseData;
import com.webkul.mobikulmp.model.InvoiceDetailResponseData;
import com.webkul.mobikulmp.model.SellerListResponseData;
import com.webkul.mobikulmp.model.SellerProfileData;
import com.webkul.mobikulmp.model.SellerReviewListData;
import com.webkul.mobikulmp.model.SendCreditMemoMailResponseData;
import com.webkul.mobikulmp.model.SendInvoiceOrderMailResponse;
import com.webkul.mobikulmp.model.SendOrderMailResponse;
import com.webkul.mobikulmp.model.ShipmentDetailsResponse;
import com.webkul.mobikulmp.model.chat.ChatSellerListResponseData;
import com.webkul.mobikulmp.model.landingpage.MarketplaceLandingPageData;
import com.webkul.mobikulmp.model.pdfheader.PdfHeaderInfoDataResponse;
import com.webkul.mobikulmp.model.seller.AddProductFieldProductCollectionData;
import com.webkul.mobikulmp.model.seller.CheckSkuResponseData;
import com.webkul.mobikulmp.model.seller.SaveProductResponseData;
import com.webkul.mobikulmp.model.seller.SellerAddProductResponseData;
import com.webkul.mobikulmp.model.seller.SellerDashboardData;
import com.webkul.mobikulmp.model.seller.SellerOrderDetailResponseData;
import com.webkul.mobikulmp.model.seller.SellerOrderListData;
import com.webkul.mobikulmp.model.seller.SellerProductListResponseData;
import com.webkul.mobikulmp.model.seller.SellerProfileFormResponseData;
import com.webkul.mobikulmp.model.seller.SellerTransactionListResponseData;
import com.webkul.mobikulmp.model.seller.ViewTransactionResponseData;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

/**
 * Created by shubham.agarwal on 17/3/17. @Webkul Software Pvt. Ltd
 */
public interface MpApiInterface {

    String MOBIKUL_MARKETPLACE_LANDING_PAGE_DATA = "mobikulmphttp/marketplace/landingPageData/";
    String MOBIKUL_SELLER_LIST = "mobikulmphttp/marketplace/sellerList/";
    String MOBIKUL_SELLER_PROFILE_DATA = "mobikulmphttp/marketplace/SellerProfile/";
    String MOBIKUL_SELLER_COLLECTION = "mobikulmphttp/marketplace/SellerCollection/";
    String MOBIKUL_SELLER_SAVE_REVIEW = "mobikulmphttp/marketplace/saveReview/";
    String MOBIKUL_SELLER_REVIEW_LIST = "mobikulmphttp/marketplace/SellerReviews/";
    String MOBIKUL_CONTACT_SELLER = "mobikulmphttp/marketplace/contactSeller/";
    String MOBIKUL_CONTACT_ADMIN = "mobikulmphttp/marketplace/askQuestionToAdmin/";
    String MOBIKUL_SELLER_DASHBOARD_DATA = "mobikulmphttp/marketplace/Dashboard/";
    String MOBIKUL_SELLER_PROFILE_FORM_DATA = "mobikulmphttp/marketplace/profileFormData/";
    String MOBIKUL_SELLER_PROFILE_DELETE_IMAGE = "mobikulmphttp/marketplace/deleteSellerImage/";
    String MOBIKUL_SELLER_SAVE_PROFILE = "mobikulmphttp/marketplace/SaveProfile";
    String MOBIKUL_SELLER_ORDER_LIST_DATA = "mobikulmphttp/marketplace/orderList/";
    String MOBIKUL_SELLER_ORDER_DETAILS_DATA = "mobikulmphttp/marketplace/viewOrder";
    String MOBIKUL_SELLER_PRODUCTS_LIST = "mobikulmphttp/marketplace/productlist";
    String MOBIKUL_SELLER_DELETE_PRODUCT = "mobikulmphttp/marketplace/productDelete";
    String MOBIKUL_SELLER_MASS_DELETE_PRODUCT = "mobikulmphttp/marketplace/productMassDelete";
    String MOBIKUL_SELLER_TRANSACTIONS_LIST = "mobikulmphttp/marketplace/transactionList";
    String MOBIKUL_SELLER_VIEW_TRANSACTION = "mobikulmphttp/marketplace/viewTransaction";
    String MOBIKUL_DOWNLOAD_TRANSACTION_LIST = "mobikulmphttp/marketplace/downloadTransactionList";
    String MOBIKUL_CANEL_ORDER = "mobikulmphttp/marketplace/cancelOrder";
    String MOBIKUL_SEND_ORDER_MAIL = "mobikulmphttp/marketplace/sendOrderMail";
    String MOBIKUL_CREATE_INVOICE = "mobikulmphttp/marketplace/createInvoice";
    String MOBIKUL_INVOICE_DETAILS = "mobikulhttp/marketplace/invoiceDetails";
    String MOBIKUL_SEND_INVOICE_MAIL = "mobikulhttp/marketplace/sendInvoiceMail";
    String MOBIKUL_SHIPMENT_DETAILS = "mobikulhttp/marketplace/shipmentDetails";
    String MOBIKUL_CREDIT_MEMO_LIST = "mobikulhttp/marketplace/creditmemoList";
    String MOBIKUL_CREDIT_MEMO_DETAILS = "mobikulhttp/marketplace/creditMemoDetails";
    String MOBIKUL_SEND_CREDIT_MEMO_MAIL = "mobikulhttp/marketplace/sendCreditMemoMail";
    String MOBIKUL_BECOME_PARTNER = "mobikulmphttp/marketplace/BecomeSeller";
    String MOBIKUL_PDF_HEADER_FORM_DATA = "mobikulmphttp/marketplace/pdfHeaderFormData";
    String MOBIKUL_SAVE_PDF_HEADER = "mobikulmphttp/marketplace/savePdfHeader";
    String MOBIKUL_DOWNLOAD_ALL_INVOICE = "mobikulmphttp/marketplace/downloadAllInvoice/";
    String MOBIKUL_DOWNLOAD_ALL_SHIPPING = "mobikulmphttp/marketplace/downloadAllShipping";
    String MOBIKUL_CHAT_SELLER_LIST = "mobikulmphttp/chat/sellerList";
    String MOBIKUL_CHAT_NOTIFY_ADMIN = "mobikulmphttp/chat/notifyAdmin";


    String MOBIKUL_PRODUCT_NEW_FORM_DATA = "mobikulmphttp/product/newformdata";
    String MOBIKUL_PRODUCT_CHECK_SKU = "mobikulmphttp/product/checkSku";
    String MOBIKUL_PRODUCT_SAVE_PRODUCT = "mobikulmphttp/product/SaveProduct";
    String MOBIKUL_PRODUCT_RELATED_PRODUCT = "mobikulmphttp/product/RelatedProductData";
    String MOBIKUL_PRODUCT_UPSELL_PRODUCT = "mobikulmphttp/product/UpsellProductData";
    String MOBIKUL_PRODUCT_CROSS_SELL_PRODUCT = "mobikulmphttp/product/CrosssellProductData";

    @FormUrlEncoded
    @POST(MOBIKUL_MARKETPLACE_LANDING_PAGE_DATA)
    Call<MarketplaceLandingPageData> getLandingPageData(
            @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("mFactor") Float density
    );


    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_LIST)
    Call<SellerListResponseData> getSellerList(
            @Field("searchQuery") String searchQuery
            , @Field("width") int width
            , @Field("storeId") int storeId
            , @Field("mFactor") Float density
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_PROFILE_DATA)
    Call<SellerProfileData> getSellerProfileData(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("sellerId") int sellerId
            , @Field("width") int width
    );


    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_COLLECTION)
    Call<CatalogProductData> getSellerCollectionData(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("sellerId") int sellerId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
            , @Field("categoryId") int categoryId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_REVIEW_LIST)
    Call<SellerReviewListData> getSellerReviews(
            @Field("storeId") int storeId
            , @Field("sellerId") int sellerId
            , @Field("width") int width
            , @Field("pageNumber") int pageNumber
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_SAVE_REVIEW)
    Call<BaseModel> saveReview(
            @Field("sellerId") int sellerId
            , @Field("customerId") int customerId
            , @Field("customerEmail") String customerEmail
            , @Field("nickName") String nickName
            , @Field("summary") String summary
            , @Field("description") String details
            , @Field("priceRating") int price
            , @Field("valueRating") int value
            , @Field("qualityRating") int quality
            , @Field("shopUrl") String shopUrl
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CONTACT_SELLER)
    Call<BaseModel> contactSeller(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("sellerId") int sellerId
            , @Field("productId") int productId
            , @Field("name") String customerName
            , @Field("email") String customerEmail
            , @Field("subject") String subject
            , @Field("query") String query
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CONTACT_ADMIN)
    Call<BaseModel> askQuestionToAdmin(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("subject") String subject
            , @Field("query") String query
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_DASHBOARD_DATA)
    Call<SellerDashboardData> getsellerdashboardData(
            @Field("customerId") int customerId
            , @Field("storeId") int storeId
            , @Field("width") int width
            , @Field("mFactor") int mFactor
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_PROFILE_FORM_DATA)
    Call<SellerProfileFormResponseData> getsellerProfileFormData(
            @Field("customerId") int customerId
            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_PROFILE_DELETE_IMAGE)
    Call<BaseModel> getDeleteSellerImage(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("entity") String entity
    );

    @Multipart
    @POST(MOBIKUL_SELLER_SAVE_PROFILE)
    Call<BaseModel> saveSellerProfileData(
            @Part("storeId") int storeId
            , @Part("customerId") int customerId
            , @Part("twActive") int twActive
            , @Part("twitterId") RequestBody twitterId
            , @Part("fbActive") int fbActive
            , @Part("facebookId") RequestBody facebookId
            , @Part("instagramActive") int instagramActive
            , @Part("instagramId") RequestBody instagramId
            , @Part("gplusActive") int gplusActive
            , @Part("gplusId") RequestBody gplusId
            , @Part("youtubeActive") int youtubeActive
            , @Part("youtubeId") RequestBody youtubeId
            , @Part("vimeoActive") int vimeoActive
            , @Part("vimeoId") RequestBody vimeoId
            , @Part("pinterestActive") int pinterestActive
            , @Part("pinterestId") RequestBody pinterestId
            , @Part("contactNumber") RequestBody contactNumber
            , @Part("taxvat") RequestBody taxvat
            , @Part("backgroundColor") RequestBody backgroundColor
            , @Part("shopTitle") RequestBody shopTitle
            , @Part("banner\"; filename=\"companyBanner.png\" ") RequestBody companyBanner
            , @Part("logo\"; filename=\"companylogo.png\" ") RequestBody logo
            , @Part("companyLocality") RequestBody companyLocality
            , @Part("companyDescription") RequestBody companyDescription
            , @Part("returnPolicy") RequestBody returnPolicy
            , @Part("shippingPolicy") RequestBody shippingPolicy
            , @Part("country") RequestBody country
            , @Part("metaKeyword") RequestBody metaKeyword
            , @Part("metaDescription") RequestBody metaDescription
            , @Part("paymentDetails") RequestBody paymentDetails
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_ORDER_LIST_DATA)
    Call<SellerOrderListData> getSellerOrderListData(
            @Field("customerId") int customerId
            , @Field("storeId") int storeId
            , @Field("status") String status
            , @Field("dateTo") String dateTo
            , @Field("dateFrom") String dateFrom
            , @Field("incrementId") String incrementId
            , @Field("pageNumber") int pageNumber
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_ORDER_DETAILS_DATA)
    Call<SellerOrderDetailResponseData> getSellerOrderDetailData(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("orderId") String orderId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_PRODUCTS_LIST)
    Call<SellerProductListResponseData> getSellerProductsListData(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("pageNumber") int pageNumber
            , @Field("dateTo") String dateTo
            , @Field("dateFrom") String dateFrom
            , @Field("productName") String productName
            , @Field("productStatus") String productStatus
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_DELETE_PRODUCT)
    Call<BaseModel> deleteProduct(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("productId") int productId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_MASS_DELETE_PRODUCT)
    Call<BaseModel> massDeleteProduct(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("productIds") JSONArray productIds
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_TRANSACTIONS_LIST)
    Call<SellerTransactionListResponseData> getSellerTransactionListData(
            @Field("customerId") int customerId
            , @Field("storeId") int storeId
            , @Field("pageNumber") int pageNumber
            , @Field("dateTo") String dateTo
            , @Field("dateFrom") String dateFrom
            , @Field("transactionId") String transactionId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SELLER_VIEW_TRANSACTION)
    Call<ViewTransactionResponseData> viewSellerTransaction(
            @Field("customerId") int customerId
            , @Field("storeId") int storeId
            , @Field("transactionId") String transactionId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SEND_ORDER_MAIL)
    Call<SendOrderMailResponse> getSendOrderMailData(
            @Field("incrementId") String incrementId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CREATE_INVOICE)
    Call<CreateInvoiceResponseData> getCreateInvoiceResponseData(
            @Field("incrementId") String incrementId
            , @Field("sellerId") int sellerId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CANEL_ORDER)
    Call<CancelOrderResponseData> getCancelOrderResponseData(
            @Field("incrementId") String incrementId
            , @Field("sellerId") int sellerId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_INVOICE_DETAILS)
    Call<InvoiceDetailResponseData> getInvoiceDetails(
            @Field("sellerId") int sellerId
            , @Field("incrementId") String incrementId
            , @Field("invoiceId") int invoiceId
            , @Field("storeId") int storeId
    );


    @FormUrlEncoded
    @POST(MOBIKUL_SEND_INVOICE_MAIL)
    Call<SendInvoiceOrderMailResponse> sendInvoiceMail(
            @Field("sellerId") int sellerId
            , @Field("incrementId") String incrementId
            , @Field("invoiceId") int invoiceId
            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SHIPMENT_DETAILS)
    Call<ShipmentDetailsResponse> getShipmentDetails(
            @Field("sellerId") int sellerId
            , @Field("incrementId") String incrementId
            , @Field("shipmentId") int shipmentId
            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CREDIT_MEMO_LIST)
    Call<CreditMemoListResponseData> getCreditMemoListResponseData(
            @Field("sellerId") int sellerId
            , @Field("incrementId") String incrementId
            , @Field("storeId") int storeId

    );

    @FormUrlEncoded
    @POST(MOBIKUL_CREDIT_MEMO_DETAILS)
    Call<CreditMemoDetailsResponseData> getCreditMemoDetailsResponseData(
            @Field("sellerId") int sellerId
            , @Field("creditMemoId") int creditMemoId
            , @Field("incrementId") String incrementId
            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SEND_CREDIT_MEMO_MAIL)
    Call<SendCreditMemoMailResponseData> sendCreditMemoMail(
            @Field("sellerId") int sellerId
            , @Field("creditMemoId") int creditMemoId
            , @Field("incrementId") String incrementId
            , @Field("storeId") int storeId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_BECOME_PARTNER)
    Call<BecomePartnerResponseData> sendBecomePartnerRequest(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("shopUrl") String shopUrl
    );

    @FormUrlEncoded
    @POST(MOBIKUL_PDF_HEADER_FORM_DATA)
    Call<PdfHeaderInfoDataResponse> getPdfHeaderFormData(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_SAVE_PDF_HEADER)
    Call<PdfHeaderInfoDataResponse> savePdfHeader(
            @Field("storeId") int storeId
            , @Field("customerId") int customerId
            , @Field("pdfHeader") String pdfHeader
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHAT_SELLER_LIST)
    Call<ChatSellerListResponseData> getChatSellerList(
            @Field("adminEmail") String adminEmail
            , @Field("websiteId") int websiteId
            , @Field("storeId") int storeId
            , @Field("mFactor") Float density
    );

    @FormUrlEncoded
    @POST(MOBIKUL_CHAT_NOTIFY_ADMIN)
    Call<BaseModel> chatNotifyAdmin(
            @Field("message") String message
            , @Field("sellerId") String sellerId
            , @Field("sellerName") String sellerName
            , @Field("websiteId") int websiteId
    );

    @FormUrlEncoded
    @POST
    Call<BaseModel> chatNotifyUser(
            @Url String url

            , @Header("Content-Type") String contentType
            , @Header("Authorization") String authorizationKey


            , @Field("data") JSONObject data
            , @Field("notification") JSONObject notification
            , @Field("to") String token
            , @Field("time_to_live") int timeTolive
            , @Field("delay_while_idle") boolean delayWhileIdle

    );

    @FormUrlEncoded
    @POST(MOBIKUL_PRODUCT_NEW_FORM_DATA)
    Call<SellerAddProductResponseData> getProductFormData(
            @Field("storeId") int storeId
            , @Field("sellerId") int sellerId
            , @Field("productId") String productId
    );

    @FormUrlEncoded
    @POST(MOBIKUL_PRODUCT_CHECK_SKU)
    Call<CheckSkuResponseData> checkSku(
            @Field("storeId") int storeId
            , @Field("sellerId") int sellerId
            , @Field("sku") String sku
    );

    @FormUrlEncoded
    @POST(MOBIKUL_PRODUCT_SAVE_PRODUCT)
    Call<SaveProductResponseData> saveProduct(
            @Field("storeId") int storeId
            , @Field("sellerId") int sellerId
            , @Field("productId") String productId
            , @Field("type") String type
            , @Field("status") String status
            , @Field("attributeSetId") String attributeSetId
            , @Field("websiteIds") JSONArray websiteIds
            , @Field("categoryIds") JSONArray categoryIds
            , @Field("name") String name
            , @Field("description") String description
            , @Field("shortDescription") String shortDescription
            , @Field("sku") String sku
            , @Field("price") String price
            , @Field("specialPrice") String specialPrice
            , @Field("specialFromDate") String specialFromDate
            , @Field("specialToDate") String specialToDate
            , @Field("qty") String qty
            , @Field("isInStock") int isInStock
            , @Field("visibility") String visibility
            , @Field("taxClassId") String taxClassId
            , @Field("productHasWeight") int productHasWeight
            , @Field("weight") String weight
            , @Field("metaTitle") String metaTitle
            , @Field("metaKeyword") String metaKeyword
            , @Field("metaDescription") String metaDescription
            , @Field("mpProductCartLimit") String mpProductCartLimit
            , @Field("baseImage") String baseImage
            , @Field("smallImage") String smallImage
            , @Field("swatchImage") String swatchImage
            , @Field("thumbnail") String thumbnail
            , @Field("medialGallery") JSONArray medialGallery
            , @Field("crossSell") JSONArray crossSell
            , @Field("upsell") JSONArray upsell
            , @Field("related") JSONArray related
    );

    @FormUrlEncoded
    @POST(MOBIKUL_PRODUCT_RELATED_PRODUCT)
    Call<AddProductFieldProductCollectionData> getRelatedProductData(
            @Field("storeId") int storeId
            , @Field("sellerId") int sellerId
            , @Field("productId") String productId
            , @Field("pageNumber") int pageNumber
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
    );

    @FormUrlEncoded
    @POST(MOBIKUL_PRODUCT_UPSELL_PRODUCT)
    Call<AddProductFieldProductCollectionData> getUpSellProductData(
            @Field("storeId") int storeId
            , @Field("sellerId") int sellerId
            , @Field("productId") String productId
            , @Field("pageNumber") int pageNumber
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
    );

    @FormUrlEncoded
    @POST(MOBIKUL_PRODUCT_CROSS_SELL_PRODUCT)
    Call<AddProductFieldProductCollectionData> getCrossSellProductData(
            @Field("storeId") int storeId
            , @Field("sellerId") int sellerId
            , @Field("productId") String productId
            , @Field("pageNumber") int pageNumber
            , @Field("sortData") JSONArray sortData
            , @Field("filterData") JSONArray filterData
    );
}
