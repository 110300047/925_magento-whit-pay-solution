package com.webkul.mobikulmp.custom;

import android.content.Context;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.webkul.mobikulmp.R;

/**
 * Created by philipp on 02/06/16.
 */
public class DayAxisValueFormatter implements IAxisValueFormatter {

    @SuppressWarnings("unused")
    private static final String TAG = "DayAxisValueFormatter";
    private Context mContext;


    public DayAxisValueFormatter(Context context) {
        this.mContext = context;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mContext.getResources().getStringArray(R.array.months)[(int) value];
    }

}
