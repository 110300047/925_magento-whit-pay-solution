package com.webkul.mobikulmp.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.DialogFragmentHintBinding;

/**
 * Created by vedesh.kumar on 11/1/18. @Webkul Software Private limited
 */

public class HintDialogFragment extends DialogFragment {

    public DialogFragmentHintBinding mBinding;

    public static HintDialogFragment newInstance(String hint) {
        HintDialogFragment hintDialogFragment = new HintDialogFragment();
        hintDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        Bundle bundle = new Bundle();
        bundle.putString("hint", hint);
        hintDialogFragment.setArguments(bundle);
        return hintDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_hint, container, false);
        mBinding.setHint(getArguments().getString("hint"));
        return mBinding.getRoot();
    }
}