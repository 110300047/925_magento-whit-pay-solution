package com.webkul.mobikulmp.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerAddProductActivity;
import com.webkul.mobikulmp.databinding.DialogFragmentSellerAddProductCollectionFilterBinding;
import com.webkul.mobikulmp.databinding.ItemFilterOptionSelectBinding;
import com.webkul.mobikulmp.databinding.ItemFilterOptionTextBinding;
import com.webkul.mobikulmp.databinding.ItemFilterOptionTextRangeBinding;
import com.webkul.mobikulmp.fragment.SellerSelectProductsFragment;
import com.webkul.mobikulmp.handler.SellerAddProductsFilterFragHandler;
import com.webkul.mobikulmp.model.seller.FilterOption;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 24/3/17. @Webkul Software Private limited
 */

public class SellerAddProductCollectionFilterDialogFragment extends DialogFragment {

    public DialogFragmentSellerAddProductCollectionFilterBinding mBinding;
    public ArrayList<FilterOption> mFilterOption;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_seller_add_product_collection_filter, container, false);
        initializeFilters();
        return mBinding.getRoot();
    }

    private void initializeFilters() {
        mBinding.setHandler(new SellerAddProductsFilterFragHandler(this));
        mFilterOption = ((SellerSelectProductsFragment) ((SellerAddProductActivity) getContext())
                .getSupportFragmentManager().findFragmentByTag(SellerSelectProductsFragment.class.getSimpleName())).mCollectionData.getFilterOption();
        for (int noOfFilters = 0; noOfFilters < mFilterOption.size(); noOfFilters++) {
            switch (mFilterOption.get(noOfFilters).getType()) {
                case "textRange":
                    ItemFilterOptionTextRangeBinding itemFilterOptionTextRangeBinding = ItemFilterOptionTextRangeBinding.inflate(getActivity().getLayoutInflater(), mBinding.filterOptionsContainer, true);
                    itemFilterOptionTextRangeBinding.setData(mFilterOption.get(noOfFilters));
                    break;
                case "text":
                    ItemFilterOptionTextBinding itemFilterOptionTextBinding = ItemFilterOptionTextBinding.inflate(getActivity().getLayoutInflater(), mBinding.filterOptionsContainer, true);
                    itemFilterOptionTextBinding.setData(mFilterOption.get(noOfFilters));
                    break;
                case "select":
                    ItemFilterOptionSelectBinding itemFilterOptionSelectBinding = ItemFilterOptionSelectBinding.inflate(getActivity().getLayoutInflater(), mBinding.filterOptionsContainer, true);
                    itemFilterOptionSelectBinding.setData(mFilterOption.get(noOfFilters));
                    ArrayList<String> optionLabels = new ArrayList<>();
                    optionLabels.add("");
                    for (int noOfCountries = 0; noOfCountries < mFilterOption.get(noOfFilters).getOptions().size(); noOfCountries++) {
                        optionLabels.add(mFilterOption.get(noOfFilters).getOptions().get(noOfCountries).getLabel());
                    }
                    itemFilterOptionSelectBinding.optionSpinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item
                            , optionLabels));
                    final int finalNoOfFilters = noOfFilters;
                    itemFilterOptionSelectBinding.optionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0) {
                                mFilterOption.get(finalNoOfFilters).setValueFrom("");
                            } else {
                                mFilterOption.get(finalNoOfFilters).setValueFrom(mFilterOption.get(finalNoOfFilters).getOptions().get(position - 1).getValue());
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    itemFilterOptionSelectBinding.optionSpinner.setSelection(mFilterOption.get(noOfFilters).getSelectedOptionData());
                    break;
            }
        }
    }
}