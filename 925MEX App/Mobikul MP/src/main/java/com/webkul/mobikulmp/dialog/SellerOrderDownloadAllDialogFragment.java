package com.webkul.mobikulmp.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.DialogFragmentSellerOrderDownloadAllBinding;
import com.webkul.mobikulmp.handler.SellerOrderDownloadAllFragHandler;
import com.webkul.mobikulmp.model.seller.SellerDownloadAllFragmentData;

/**
 * Created by shubham.agarwal on 24/3/17. @Webkul Software Private limited
 */

public class SellerOrderDownloadAllDialogFragment extends DialogFragment {

    public DialogFragmentSellerOrderDownloadAllBinding mBinding;

    public static SellerOrderDownloadAllDialogFragment newInstance(int type) {
        SellerOrderDownloadAllDialogFragment sellerOrderFilterDialogFragment = new SellerOrderDownloadAllDialogFragment();
        sellerOrderFilterDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        sellerOrderFilterDialogFragment.setArguments(bundle);
        return sellerOrderFilterDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_seller_order_download_all, container, false);
        mBinding.setType(getArguments().getInt("type"));
        mBinding.setData(new SellerDownloadAllFragmentData());
        mBinding.setHandler(new SellerOrderDownloadAllFragHandler(this));
        return mBinding.getRoot();
    }
}