package com.webkul.mobikulmp.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.DialogFragmentSellerOrderFilterBinding;
import com.webkul.mobikulmp.handler.SellerOrderFilterFragHandler;
import com.webkul.mobikulmp.model.SellerOrderFilterFragData;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ORDER_FILTER_DATA;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerOrderFilterDialogFragment extends DialogFragment {

    private DialogFragmentSellerOrderFilterBinding mBinding;

    public static SellerOrderFilterDialogFragment newInstance(SellerOrderFilterFragData sellerOrderFilterFragData) {
        SellerOrderFilterDialogFragment sellerOrderFilterDialogFragment = new SellerOrderFilterDialogFragment();
        sellerOrderFilterDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_KEY_SELLER_ORDER_FILTER_DATA, sellerOrderFilterFragData);
        sellerOrderFilterDialogFragment.setArguments(bundle);
        return sellerOrderFilterDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_seller_order_filter, container, false);
        mBinding.setData((SellerOrderFilterFragData) getArguments().getParcelable(BUNDLE_KEY_SELLER_ORDER_FILTER_DATA));
        mBinding.setHandler(new SellerOrderFilterFragHandler(getContext(), this, mBinding.getData()));
        return mBinding.getRoot();
    }

}
