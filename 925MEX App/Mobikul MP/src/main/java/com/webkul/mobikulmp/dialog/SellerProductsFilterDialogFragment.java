package com.webkul.mobikulmp.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerProductsListActivity;
import com.webkul.mobikulmp.databinding.DialogFragmentSellerProductFilterBinding;
import com.webkul.mobikulmp.handler.SellerProductsFilterFragHandler;
import com.webkul.mobikulmp.model.SellerOrderFilterFragData;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ORDER_FILTER_DATA;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerProductsFilterDialogFragment extends DialogFragment {

    public DialogFragmentSellerProductFilterBinding mBinding;

    public static SellerProductsFilterDialogFragment newInstance(SellerOrderFilterFragData sellerOrderFilterFragData) {
        SellerProductsFilterDialogFragment sellerOrderFilterDialogFragment = new SellerProductsFilterDialogFragment();
        sellerOrderFilterDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUNDLE_KEY_SELLER_ORDER_FILTER_DATA, sellerOrderFilterFragData);
        sellerOrderFilterDialogFragment.setArguments(bundle);
        return sellerOrderFilterDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_seller_product_filter, container, false);
        mBinding.setProductName("");
        mBinding.setFromDate("");
        mBinding.setToDate("");
        mBinding.setHandler(new SellerProductsFilterFragHandler(this));
        return mBinding.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((SellerProductsListActivity) getContext()).mBinding.filterBtn.animate().alpha(1.0f).translationY(0).setInterpolator(new DecelerateInterpolator(1.4f));
    }
}