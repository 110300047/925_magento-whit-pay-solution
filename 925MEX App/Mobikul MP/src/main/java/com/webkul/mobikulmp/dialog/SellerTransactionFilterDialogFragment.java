package com.webkul.mobikulmp.dialog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.DialogFragmentSellerTransactionFilterBinding;
import com.webkul.mobikulmp.handler.SellerTransactionFilterFragHandler;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerTransactionFilterDialogFragment extends DialogFragment {

    public DialogFragmentSellerTransactionFilterBinding mBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_seller_transaction_filter, container, false);
        mBinding.setTransactionId("");
        mBinding.setFromDate("");
        mBinding.setToDate("");
        mBinding.setHandler(new SellerTransactionFilterFragHandler(this));
        return mBinding.getRoot();
    }
}