package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.FragmentAskQuestionToAdminBinding;
import com.webkul.mobikulmp.handler.AskQuestionToAdminFragmentHandler;
import com.webkul.mobikulmp.model.AskQuesToAdminRequestData;

/**
 * Created by shubham.agarwal on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class AskQuestionToAdminFragment extends DialogFragment {
    public FragmentAskQuestionToAdminBinding mBinding;

    public static AskQuestionToAdminFragment newInstance() {
        AskQuestionToAdminFragment fragment = new AskQuestionToAdminFragment();
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ask_question_to_admin, container, false);
        mBinding.setData(new AskQuesToAdminRequestData(getContext()));
        mBinding.setHandler(new AskQuestionToAdminFragmentHandler(getContext(), mBinding.getData(), this));
        return mBinding.getRoot();
    }
}