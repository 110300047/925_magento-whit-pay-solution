package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.FragmentContactSellerBinding;
import com.webkul.mobikulmp.handler.ContactSellerFragmentHandler;
import com.webkul.mobikulmp.model.ContactSellerRequestData;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;

/**
 * Created by shubham.agarwal on 30/12/16. @Webkul Software Pvt. Ltd
 */
public class ContactSellerFragment extends Fragment {
    public FragmentContactSellerBinding mBinding;

    public static ContactSellerFragment newInstance(int sellerId, int productId) {
        ContactSellerFragment fragment = new ContactSellerFragment();

        Bundle args = new Bundle();
        args.putInt(BUNDLE_KEY_SELLER_ID, sellerId);
        args.putInt(BUNDLE_KEY_PRODUCT_ID, productId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_seller, container, false);
        ContactSellerRequestData contactSellerRequestData = new ContactSellerRequestData(getContext());
        contactSellerRequestData.setNickname(AppSharedPref.getCustomerName(getContext()));
        contactSellerRequestData.setEmail(AppSharedPref.getCustomerEmail(getContext()));
        mBinding.setData(contactSellerRequestData);
        mBinding.setHandler(new ContactSellerFragmentHandler(getContext(), mBinding.getData(), getArguments().getInt(BUNDLE_KEY_SELLER_ID), getArguments().getInt(BUNDLE_KEY_PRODUCT_ID)));
        return mBinding.getRoot();
    }
}