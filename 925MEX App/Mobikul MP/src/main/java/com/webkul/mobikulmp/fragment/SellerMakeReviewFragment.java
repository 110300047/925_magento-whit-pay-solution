package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.FragmentSellerMakeReviewBinding;
import com.webkul.mobikulmp.handler.SellerMakeReviewHandler;
import com.webkul.mobikulmp.model.SellerMakeReviewData;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SHOP_URL;

/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */
public class SellerMakeReviewFragment extends Fragment {

    public FragmentSellerMakeReviewBinding mBinding;

    public static SellerMakeReviewFragment newInstance(int sellerId, String shopUrl) {
        Bundle args = new Bundle();
        args.putInt(BUNDLE_KEY_SELLER_ID, sellerId);
        args.putString(BUNDLE_KEY_SHOP_URL, shopUrl);
        SellerMakeReviewFragment fragment = new SellerMakeReviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_seller_make_review, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(getString(R.string.write_your_review));
        mBinding.setData(new SellerMakeReviewData());
        mBinding.setHandler(new SellerMakeReviewHandler(getContext(), getArguments().getInt(BUNDLE_KEY_SELLER_ID), getArguments().getString(BUNDLE_KEY_SHOP_URL)));
    }


}
