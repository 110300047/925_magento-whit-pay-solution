package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerOrdersListRvAdapter;
import com.webkul.mobikulmp.databinding.FragmentSellerRecentOrdersListBinding;
import com.webkul.mobikulmp.model.seller.SellerOrderData;

import java.util.ArrayList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ORDER_DATA;

/**
 * Created by vedesh.kumar on 3/1/17. @Webkul Software Pvt. Ltd
 */

public class SellerRecentOrdersListFragment extends Fragment {

    private FragmentSellerRecentOrdersListBinding mBinding;
    private ArrayList<SellerOrderData> mRecentOrdersList;

    public static SellerRecentOrdersListFragment newInstance(ArrayList<SellerOrderData> recentOrderList) {
        SellerRecentOrdersListFragment sellerRecentOrdersListFragment = new SellerRecentOrdersListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(BUNDLE_KEY_SELLER_ORDER_DATA, recentOrderList);
        sellerRecentOrdersListFragment.setArguments(args);
        return sellerRecentOrdersListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_seller_recent_orders_list, container, false);
        mRecentOrdersList = getArguments().getParcelableArrayList(BUNDLE_KEY_SELLER_ORDER_DATA);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(R.string.activity_title_seller_order);
        mBinding.setDisplayOrder(false);
        if (mRecentOrdersList.size() > 0) {
            mBinding.setDisplayOrder(true);
            mBinding.sellerOrdersRv.setAdapter(new SellerOrdersListRvAdapter(getContext(), mRecentOrdersList));
        }
    }
}