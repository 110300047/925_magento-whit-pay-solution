package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerFeedbackRvAdapter;
import com.webkul.mobikulmp.databinding.FragmentSellerRecentReviewsListBinding;
import com.webkul.mobikulmp.model.seller.SellerReviewList;

import java.util.ArrayList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_REVIEW_DATA;

/**
 * Created by vedesh.kumar on 3/1/17. @Webkul Software Pvt. Ltd
 */

public class SellerRecentReviewListFragment extends Fragment {

    private FragmentSellerRecentReviewsListBinding mBinding;
    private ArrayList<SellerReviewList> mReviewList;

    public static SellerRecentReviewListFragment newInstance(ArrayList<SellerReviewList> reviewList) {
        SellerRecentReviewListFragment productReviewListFragment = new SellerRecentReviewListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(BUNDLE_KEY_SELLER_REVIEW_DATA, reviewList);
        productReviewListFragment.setArguments(args);
        return productReviewListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_seller_recent_reviews_list, container, false);

        mReviewList = getArguments().getParcelableArrayList(BUNDLE_KEY_SELLER_REVIEW_DATA);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(R.string.product_reviews);
        if (mReviewList.size() > 0) {
            mBinding.setDisplayReview(true);
            mBinding.sellerReviewsRv.setAdapter(new SellerFeedbackRvAdapter(getContext(), mReviewList));
        } else {
            mBinding.setDisplayReview(false);
        }
    }
}