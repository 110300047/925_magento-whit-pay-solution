package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.SellerFeedbackRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.FragmentSellerReviewBinding;
import com.webkul.mobikulmp.handler.SellerReviewListHandler;
import com.webkul.mobikulmp.model.SellerReviewListData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SHOP_URL;

/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */
public class SellerReviewFragment extends Fragment {

    private FragmentSellerReviewBinding mBinding;
    private int mPageNumber = 1;
    private int mSellerId = 0;
    private boolean isFirstCall = true;

    private Callback<SellerReviewListData> mSellerReviewListDataCallback = new Callback<SellerReviewListData>() {
        @Override
        public void onResponse(Call<SellerReviewListData> call, Response<SellerReviewListData> response) {
            mBinding.setLazyLoading(false);
            if (response.body().isSuccess()) {
                ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
                if (isFirstCall) {
                    isFirstCall = false;
                    mBinding.setData(response.body());
                    mBinding.sellerFeedbackRv.setLayoutManager(new LinearLayoutManager(getContext()));
                    mBinding.sellerFeedbackRv.setAdapter(new SellerFeedbackRvAdapter(getActivity(), response.body().getReviewList()));
                    mBinding.sellerFeedbackRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            int lastCompletelyVisibleItemPosition;
                            lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                            callLazyConnection(lastCompletelyVisibleItemPosition);
                        }
                    });
                } else {
                    mBinding.getData().getReviewList().addAll(response.body().getReviewList());
                    mBinding.sellerFeedbackRv.getAdapter().notifyDataSetChanged();
                }
            } else {
                ToastHelper.showToast(getContext(), response.body().getMessage(), Toast.LENGTH_LONG, 0);
            }
        }

        @Override
        public void onFailure(Call<SellerReviewListData> call, Throwable t) {
            NetworkHelper.onFailure(t, getActivity());
            mBinding.setLazyLoading(false);
        }
    };

    public static SellerReviewFragment newInstance(int sellerId, String shopUrl) {
        Bundle args = new Bundle();
        args.putInt(BUNDLE_KEY_SELLER_ID, sellerId);
        args.putString(BUNDLE_KEY_SHOP_URL, shopUrl);
        SellerReviewFragment fragment = new SellerReviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_seller_review, container, false);
        mSellerId = getArguments().getInt(BUNDLE_KEY_SELLER_ID);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(getString(R.string.seller_reviews));
        mBinding.setHandler(new SellerReviewListHandler(getContext(), mSellerId, getArguments().getString(BUNDLE_KEY_SHOP_URL)));
        callApi();
    }

    public void callApi() {
        mBinding.setLazyLoading(true);
        MpApiConnection.getSellerReviews(getContext(), mSellerId, mPageNumber++, mSellerReviewListDataCallback);
    }

    private void callLazyConnection(int lastCompletelyVisibleItemPosition) {
        if (!mBinding.getLazyLoading() && mBinding.getData().getReviewList().size() != mBinding.getData().getTotalCount()
                && lastCompletelyVisibleItemPosition == (mBinding.getData().getReviewList().size() - 1)) {
            callApi();
        }
    }
}
