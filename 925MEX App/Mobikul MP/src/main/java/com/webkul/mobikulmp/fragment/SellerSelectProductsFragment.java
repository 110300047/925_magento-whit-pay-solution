package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerAddProductActivity;
import com.webkul.mobikulmp.adapter.AddProductProductsListRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.FragmentSellerSelectProductsBinding;
import com.webkul.mobikulmp.dialog.SellerAddProductCollectionFilterDialogFragment;
import com.webkul.mobikulmp.model.seller.AddProductFieldProductCollectionData;
import com.webkul.mobikulmp.model.seller.ProductCollectionData;

import org.json.JSONArray;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_COLLECTION_TYPE;
import static com.webkul.mobikul.helper.ToastHelper.showToast;

/**
 * Created by vedesh.kumar on 21/3/17. @Webkul Software Pvt. Ltd
 */
public class SellerSelectProductsFragment extends Fragment implements View.OnClickListener {

    public FragmentSellerSelectProductsBinding mBinding;
    public int mPageNumber = 0;
    public boolean isFirstCall = true;
    public JSONArray mSortData;
    public JSONArray mFilterData;
    public AddProductFieldProductCollectionData mCollectionData;
    private String mCollectionType = "";
    private Callback<AddProductFieldProductCollectionData> mCallBack = new Callback<AddProductFieldProductCollectionData>() {
        @Override
        public void onResponse(Call<AddProductFieldProductCollectionData> call, Response<AddProductFieldProductCollectionData> response) {
            mBinding.loader.setVisibility(View.GONE);
            if (response.body().isSuccess()) {
                updateSelectedData(response.body().getProductCollectionData());
                if (isFirstCall) {
                    isFirstCall = false;
                    mCollectionData = response.body();
                    mBinding.setData(mCollectionData);
                    initProductRv();
                } else {
                    mCollectionData.getProductCollectionData().addAll(response.body().getProductCollectionData());
                    mBinding.productsRv.getAdapter().notifyDataSetChanged();
                }
            } else {
                ToastHelper.showToast(getContext(), response.body().getMessage(), Toast.LENGTH_LONG, 0);
            }
        }

        @Override
        public void onFailure(Call<AddProductFieldProductCollectionData> call, Throwable t) {
            mBinding.loader.setVisibility(View.GONE);
            NetworkHelper.onFailure(t, getActivity());
        }
    };

    public static SellerSelectProductsFragment newInstance(String collectionType) {
        SellerSelectProductsFragment sellerSelectProductsDialogFragment = new SellerSelectProductsFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_COLLECTION_TYPE, collectionType);
        sellerSelectProductsDialogFragment.setArguments(args);
        return sellerSelectProductsDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_seller_select_products, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCollectionType = getArguments().getString(BUNDLE_KEY_COLLECTION_TYPE);
        mBinding.submitBtn.setOnClickListener(this);
        mBinding.filterBtn.setOnClickListener(this);
        setFragmentTitle();
        callApi();
    }

    private void setFragmentTitle() {
        switch (mCollectionType) {
            case "related":
                mBinding.setHeading(getString(R.string.related_product_title));
                break;
            case "upsell":
                mBinding.setHeading(getString(R.string.upsell_product));
                break;
            case "crosssell":
                mBinding.setHeading(getString(R.string.cross_sell_product));
                break;
        }
    }

    public void callApi() {
        mBinding.loader.setVisibility(View.VISIBLE);
        switch (mCollectionType) {
            case "related":
                MpApiConnection.getRelatedProductData(getContext(), ((SellerAddProductActivity) getContext()).mProductId, mPageNumber++, mSortData, mFilterData, mCallBack);
                break;
            case "upsell":
                MpApiConnection.getUpSellProductData(getContext(), ((SellerAddProductActivity) getContext()).mProductId, mPageNumber++, mSortData, mFilterData, mCallBack);
                break;
            case "crosssell":
                MpApiConnection.getCrossSellProductData(getContext(), ((SellerAddProductActivity) getContext()).mProductId, mPageNumber++, mSortData, mFilterData, mCallBack);
                break;
        }
    }

    private void updateSelectedData(ArrayList<ProductCollectionData> productCollectionData) {
        ArrayList<String> selectProductIds = null;
        switch (mCollectionType) {
            case "related":
                selectProductIds = ((SellerAddProductActivity) getContext()).mSellerAddProductResponseData.getProductData().getRelated();
                break;
            case "upsell":
                selectProductIds = ((SellerAddProductActivity) getContext()).mSellerAddProductResponseData.getProductData().getUpsell();
                break;
            case "crosssell":
                selectProductIds = ((SellerAddProductActivity) getContext()).mSellerAddProductResponseData.getProductData().getCrossSell();
                break;
        }
        if (selectProductIds != null && selectProductIds.size() > 0) {
            for (int noOfProducts = 0; noOfProducts < productCollectionData.size(); noOfProducts++) {
                if (selectProductIds.contains(productCollectionData.get(noOfProducts).getEntityId()))
                    productCollectionData.get(noOfProducts).setSelected(true);
                else
                    productCollectionData.get(noOfProducts).setSelected(false);
            }
        }
    }

    private void initProductRv() {
        if (mCollectionData.getProductCollectionData() != null && mCollectionData.getProductCollectionData().size() > 0) {
            mBinding.productsRv.setAdapter(new AddProductProductsListRvAdapter(getContext(), mCollectionData.getProductCollectionData()));
            mBinding.productsRv.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                    int lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

                    if (NetworkHelper.isNetworkAvailable(getContext())) {
                        if (mBinding.loader.getVisibility() == View.GONE && mCollectionData.getProductCollectionData().size() != mCollectionData.getTotalCount()
                                && lastCompletelyVisibleItemPosition == (mCollectionData.getProductCollectionData().size() - 1) && lastCompletelyVisibleItemPosition < mCollectionData.getTotalCount()) {
                            callApi();
                            showPositionToast(lastCompletelyVisibleItemPosition);
                        }
                    }
                }
            });
        }
    }

    private void showPositionToast(int lastCompletelyVisibleItemPosition) {
        String toastString = lastCompletelyVisibleItemPosition + 1 + " " + getString(com.webkul.mobikul.R.string.of_toast_for_no_of_item) + " " + mCollectionData.getTotalCount();
        showToast(getContext(), toastString, Toast.LENGTH_LONG, 0);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit_btn) {
            ArrayList<String> selectProductIds = new ArrayList<>();
            for (int noOfProducts = 0; noOfProducts < mCollectionData.getProductCollectionData().size(); noOfProducts++) {
                if (mCollectionData.getProductCollectionData().get(noOfProducts).isSelected())
                    selectProductIds.add(mCollectionData.getProductCollectionData().get(noOfProducts).getEntityId());
            }
            switch (mCollectionType) {
                case "related":
                    ((SellerAddProductActivity) getContext()).mSellerAddProductResponseData.getProductData().setRelated(selectProductIds);
                    break;
                case "upsell":
                    ((SellerAddProductActivity) getContext()).mSellerAddProductResponseData.getProductData().setUpsell(selectProductIds);
                    break;
                case "crosssell":
                    ((SellerAddProductActivity) getContext()).mSellerAddProductResponseData.getProductData().setCrossSell(selectProductIds);
                    break;
            }
            getActivity().getSupportFragmentManager().popBackStack();
        } else if (v.getId() == R.id.filter_btn) {
            if (mCollectionData.getFilterOption() != null && mCollectionData.getFilterOption().size() > 0) {
                FragmentManager supportFragmentManager = ((SellerAddProductActivity) v.getContext()).getSupportFragmentManager();
                SellerAddProductCollectionFilterDialogFragment sellerAddProductCollectionFilterDialogFragment = new SellerAddProductCollectionFilterDialogFragment();
                FragmentTransaction transaction = supportFragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.add(R.id.main_container, sellerAddProductCollectionFilterDialogFragment, SellerAddProductCollectionFilterDialogFragment.class.getSimpleName());
                transaction.addToBackStack(SellerAddProductCollectionFilterDialogFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
                transaction.commit();
            } else {
                showToast(getContext(), getContext().getString(com.webkul.mobikul.R.string.msg_no_filter_option_available), Toast.LENGTH_SHORT, 0);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}