package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.adapter.TransactionOrderListRvAdapter;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.FragmentViewSellerTransactionBinding;
import com.webkul.mobikulmp.model.seller.ViewTransactionResponseData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_TRANSACTION_ID;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerViewTransactionFragment extends DialogFragment {

    public FragmentViewSellerTransactionBinding mBinding;
    private String mTransactionId;
    private Callback<ViewTransactionResponseData> mCallback = new Callback<ViewTransactionResponseData>() {
        @Override
        public void onResponse(Call<ViewTransactionResponseData> call, Response<ViewTransactionResponseData> response) {
            AlertDialogHelper.dismiss(getContext());
            if (response.body().getSuccess()) {
                mBinding.setData(response.body());
                if (response.body().getTransactionOrderList() != null && response.body().getTransactionOrderList().size() > 0) {
                    mBinding.transactionOrderListRv.setAdapter(new TransactionOrderListRvAdapter(getContext(), response.body().getTransactionOrderList()));
                }
            }
        }

        @Override
        public void onFailure(Call<ViewTransactionResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, getActivity());
        }
    };

    public static SellerViewTransactionFragment newinstance(String id) {
        SellerViewTransactionFragment sellerViewTransactionFragment = new SellerViewTransactionFragment();
        sellerViewTransactionFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_TRANSACTION_ID, id);
        sellerViewTransactionFragment.setArguments(bundle);
        return sellerViewTransactionFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_view_seller_transaction, container, false);
        mTransactionId = getArguments().getString(BUNDLE_KEY_TRANSACTION_ID);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callApi();
    }

    private void callApi() {
        AlertDialogHelper.showDefaultAlertDialog(getContext());
        MpApiConnection.viewSellerTransaction(getContext(), mTransactionId, mCallback);
    }
}