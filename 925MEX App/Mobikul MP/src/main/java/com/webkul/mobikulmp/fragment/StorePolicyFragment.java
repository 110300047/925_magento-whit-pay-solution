package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikul.adapter.StorePolicyViewPagerAdapter;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.FragmentStorePolicyBinding;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_RETURN_POLICY;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SHIPPING_POLICY;

/**
 * Created by shubham.agarwal on 6/3/17. @Webkul Software Pvt. Ltd
 */

public class StorePolicyFragment extends Fragment {

    @SuppressWarnings("unused")
    private static final String TAG = "StorePolicyFragment";
    private FragmentStorePolicyBinding mBinding;

    public static StorePolicyFragment newInstance(String returnPolicy, String shippingPolicy) {
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_RETURN_POLICY, returnPolicy);
        args.putString(BUNDLE_KEY_SHIPPING_POLICY, shippingPolicy);
        StorePolicyFragment fragment = new StorePolicyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_store_policy, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(getString(R.string.title_fragment_policies));
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        StorePolicyViewPagerAdapter storePolicyViewPagerAdapter = new StorePolicyViewPagerAdapter(getChildFragmentManager());
        storePolicyViewPagerAdapter.addFragment(WebviewFragment.newInstance(getArguments().getString(BUNDLE_KEY_RETURN_POLICY), null)
                , getString(R.string.return_policy));
        storePolicyViewPagerAdapter.addFragment(WebviewFragment.newInstance(getArguments().getString(BUNDLE_KEY_SHIPPING_POLICY), null)
                , getString(R.string.shipping_policy));
        mBinding.viewpager.setAdapter(storePolicyViewPagerAdapter);
    }

}
