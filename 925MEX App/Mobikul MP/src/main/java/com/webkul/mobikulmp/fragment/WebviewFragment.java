package com.webkul.mobikulmp.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.FragmentWebViewBinding;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_ACTIVITY_TITLE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_HTML_CONTENT;

/**
 * Created by shubham.agarwal on 3/1/17. @Webkul Software Pvt. Ltd
 */

public class WebviewFragment extends Fragment {

    private FragmentWebViewBinding mBinding;

    public static WebviewFragment newInstance(String description, String title) {
        WebviewFragment webviewFragment = new WebviewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_HTML_CONTENT, description);
        bundle.putString(BUNDLE_KEY_ACTIVITY_TITLE, title);
        webviewFragment.setArguments(bundle);
        return webviewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_web_view, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments().getString(BUNDLE_KEY_ACTIVITY_TITLE) != null) {
            getActivity().setTitle(getArguments().getString(BUNDLE_KEY_ACTIVITY_TITLE));
        }
        mBinding.setData(getArguments().getString(BUNDLE_KEY_HTML_CONTENT));
//        mBinding.webview.loadDataWithBaseURL(null, getArguments().getString(BUNDLE_KEY_HTML_CONTENT), "text/html", Xml.Encoding.UTF_8.name(), null);
    }
}
