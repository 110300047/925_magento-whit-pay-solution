package com.webkul.mobikulmp.handler;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.fragment.AskQuestionToAdminFragment;
import com.webkul.mobikulmp.model.AskQuesToAdminRequestData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shubham.agarwal on 22/3/17.
 */

public class AskQuestionToAdminFragmentHandler {
    private final Context mContext;
    private final AskQuesToAdminRequestData mData;
    private final AskQuestionToAdminFragment askQuestionToAdminFragment;
    private Callback<BaseModel> mAskQuestionToAdminCallback = new Callback<BaseModel>() {
        @Override
        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
            if (!NetworkHelper.isValidResponse((Activity) mContext, response, true)) {
                return;
            }
            AlertDialogHelper.dismiss(mContext);
            ToastHelper.showToast(mContext, mContext.getResources().getString(R.string.query_submitted), Toast.LENGTH_LONG, 0);
            mData.resetFormData();
            ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
        }

        @Override
        public void onFailure(Call<BaseModel> call, Throwable t) {
            NetworkHelper.onFailure(t, (Activity) mContext);
        }
    };

    public AskQuestionToAdminFragmentHandler(Context context, AskQuesToAdminRequestData data, AskQuestionToAdminFragment fragment) {
        this.mContext = context;
        this.mData = data;
        this.askQuestionToAdminFragment = fragment;
    }

    public void submitQuery() {
        if (mData.isFormValidated()) {
            askQuestionToAdminFragment.dismiss();
            AlertDialogHelper.showDefaultAlertDialog(mContext);
            MpApiConnection.askQuestionToAdmin(mContext, mData, mAskQuestionToAdminCallback);

        } else {
            Toast.makeText(mContext, R.string.msg_fill_req_field, Toast.LENGTH_SHORT).show();
        }
    }
}
