package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.widget.Toast;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.BecomeSellerActivity;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.databinding.ActivityBecomeSellerBinding;
import com.webkul.mobikulmp.helper.MpAppSharedPref;
import com.webkul.mobikulmp.model.BecomePartnerData;
import com.webkul.mobikulmp.model.BecomePartnerResponseData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vedesh.kumar on 29/6/17. @Webkul Software Private limited
 */

public class BecomePartnerHandler {
    private Context context;
    private ActivityBecomeSellerBinding mBinding;
    private Callback<BecomePartnerResponseData> mCallback = new Callback<BecomePartnerResponseData>() {
        @Override
        public void onResponse(Call<BecomePartnerResponseData> call, Response<BecomePartnerResponseData> response) {
            AlertDialogHelper.dismiss(context);
            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
            if (response.body().isSuccess()) {
                MpAppSharedPref.setIsSeller(context, true);
                MpAppSharedPref.setIsSellerPending(context, response.body().isPending());
                ((BecomeSellerActivity) context).finish();
            }
        }

        @Override
        public void onFailure(Call<BecomePartnerResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, (BaseActivity) context);
        }
    };

    public BecomePartnerHandler(Context context, ActivityBecomeSellerBinding mBinding) {
        this.context = context;
        this.mBinding = mBinding;
    }

    public boolean validateFormData(BecomePartnerData data) {
        boolean isDataValid = true;
        if (data.getStoreUrl().isEmpty()) {
            mBinding.storeUrlEt.setError(context.getString(R.string.shop_url) + context.getString(R.string.is_require_text));
            mBinding.storeUrlEt.requestFocus();
            isDataValid = false;
        } else {
            mBinding.storeUrlEt.setError(null);
        }
        return isDataValid;
    }

    public void onClickMakeSellerBtn(BecomePartnerData data) {
        AlertDialogHelper.showDefaultAlertDialog(context);
        if (validateFormData(data)) {
            MpApiConnection.makeSeller(context, data.getStoreUrl(), mCallback);
        }
    }
}
