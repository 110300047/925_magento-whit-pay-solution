package com.webkul.mobikulmp.handler;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.ChatActivity;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.helper.MpAppSharedPref;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vedesh.kumar on 30/7/17.
 */

public class ChatMessageHandler {
    private ChatActivity chatActivity;
    private String token;
    private String[] tokenArray;

    public ChatMessageHandler(ChatActivity chatActivity) {
        this.chatActivity = chatActivity;
//        root = this.chatActivity.mDatabaseReference;
        token = this.chatActivity.token;
        tokenArray = token.split(",");
    }

    public void onClickSendButton(View view, String currentMsg) {
        if (currentMsg.isEmpty()) {
            return;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        String temp_key = chatActivity.mDatabaseReference.push().getKey();
        chatActivity.mDatabaseReference.updateChildren(map);

        DatabaseReference message_root = chatActivity.mDatabaseReference.child(temp_key);
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("name", chatActivity.name);
        map2.put("msg", currentMsg);
        map2.put("timestamp", ServerValue.TIMESTAMP);

        message_root.updateChildren(map2);

        if (MpAppSharedPref.isAdmin(chatActivity)) {
            sendNotificationToUser(currentMsg);
        } else {
            sendNotificationToServer(currentMsg);
        }
        chatActivity.mBinding.textMsgEt.setText("");

    }

    private void sendNotificationToUser(String currentMsg) {
        for (String temp_token : tokenArray) {
            try {

                String url = "https://fcm.googleapis.com/fcm/send";

                JSONObject data = new JSONObject();
                data.put("title", chatActivity.getString(R.string.new_message_from_admin));
                data.put("body", currentMsg);
                data.put("message", currentMsg);
                data.put("id", chatActivity.user_id);
                data.put("name", chatActivity.user_name);
                data.put("notificationType", "chatNotification");
                data.put("sound", "default");
                JSONObject notification_data = new JSONObject();
                notification_data.put("data", data);
                notification_data.put("notification", data);
                notification_data.put("to", temp_token);
                notification_data.put("priority", "high");
                notification_data.put("content_available", true);
                notification_data.put("time_to_live", 30);
                notification_data.put("delay_while_idle", true);


                new notifyUserTask().execute(url, notification_data.toString(), MpAppSharedPref.getApiKey(chatActivity));


            } catch (Exception e) {

                e.printStackTrace();
            }

        }
    }

    private void sendNotificationToServer(String currentMsg) {

        String name = chatActivity.user_name;

        MpApiConnection.chatNotifyAdmin(currentMsg, chatActivity.user_id, name, notifyAdminCallback);

    }


    private Callback<BaseModel> notifyAdminCallback = new Callback<BaseModel>() {
        @Override
        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
            Log.d(ApplicationConstant.TAG, "ChatMessageHandler onResponse : " + response);
        }

        @Override
        public void onFailure(Call<BaseModel> call, Throwable t) {
            t.printStackTrace();

        }
    };


    private class notifyUserTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            String data = params[1];
            String apiKey = params[2];

            try {
                URL new_URl = new URL(url);
                HttpsURLConnection httpURLConnection = (HttpsURLConnection) new_URl.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestProperty("Authorization", "Key=" + apiKey);
                try {
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setChunkedStreamingMode(0);

                    OutputStream outputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                    OutputStreamWriter streamWriter = new OutputStreamWriter(outputStream);
                    streamWriter.write(data);
                    streamWriter.flush();
                    streamWriter.close();

                    Log.d(ApplicationConstant.TAG, "notifyUserTask doInBackground : " + httpURLConnection.getResponseCode());
                    Log.d(ApplicationConstant.TAG, "notifyUserTask doInBackground : " + httpURLConnection.getResponseMessage());


                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    httpURLConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
