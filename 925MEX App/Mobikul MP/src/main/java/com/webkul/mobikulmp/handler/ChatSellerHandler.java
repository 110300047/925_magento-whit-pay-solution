package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikulmp.activity.ChatActivity;

/**
 * Created by vedesh.kumar on 30/7/17.
 */

public class ChatSellerHandler {
    private Context mContext;

    public ChatSellerHandler(Context mContext) {
        this.mContext = mContext;
    }

    public void onClickSeller(View view, String customerName, String customerId, String token, String profileImage){

        Log.d(ApplicationConstant.TAG, "customerName: "+customerName +"\ncustomerId :" +customerId + "\nprofileImage :" +profileImage);

        Intent intent = new Intent(mContext, ChatActivity.class);
        intent.putExtra("user_name",customerName);
        intent.putExtra("user_id",customerId);
        intent.putExtra("token",token);
        mContext.startActivity(intent);

    }
}
