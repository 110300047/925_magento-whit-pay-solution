package com.webkul.mobikulmp.handler;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.model.ContactSellerRequestData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by shubham.agarwal on 23/1/17. @Webkul Software Pvt. Ltd
 */

public class ContactSellerFragmentHandler {

    private final ContactSellerRequestData mData;
    private final Context mContext;
    private int mSellerId;
    private int mProductId;
    private Callback<BaseModel> mContactSellerCallback = new Callback<BaseModel>() {
        @Override
        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
            if (!NetworkHelper.isValidResponse((Activity) mContext, response, true)) {
                return;
            }
            AlertDialogHelper.dismiss(mContext);
            mData.resetFormData();
            ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
            SnackbarHelper.getSnackbar((Activity) mContext, response.body().getMessage()).show();
        }

        @Override
        public void onFailure(Call<BaseModel> call, Throwable t) {
            NetworkHelper.onFailure(t, (Activity) mContext);
        }
    };

    public ContactSellerFragmentHandler(Context context, ContactSellerRequestData data, int sellerId, int productId) {
        this.mContext = context;
        this.mData = data;
        mSellerId = sellerId;
        mProductId = productId;
    }

    public void submitQuery() {
        if (mData.isFormValidated()) {
            AlertDialogHelper.showDefaultAlertDialog(mContext);
            MpApiConnection.contactSeller(mContext, mData, mSellerId, mProductId, mContactSellerCallback);
        } else {
            Toast.makeText(mContext, R.string.msg_fill_req_field, Toast.LENGTH_SHORT).show();
        }
    }
}
