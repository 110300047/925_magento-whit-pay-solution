package com.webkul.mobikulmp.handler;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.model.SendCreditMemoMailResponseData;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_AUTH_FAILURE;

/**
 * Created by vedesh.kumar on 1/6/17.
 */

public class CreditMemoDetailsHandler {
    private Context mContext;
    private String incrementId;
    private int creditMemoId;
    private SweetAlertDialog mSweetAlertDialog;

    public CreditMemoDetailsHandler(Context mContext, String incrementId, int creditMemoId) {
        this.mContext = mContext;
        this.incrementId = incrementId;
        this.creditMemoId = creditMemoId;
    }

    public void sendCreditMemoMail(String warningMessage){
        try{

            final AlertDialog.Builder creditMemoMailDialog = new AlertDialog.Builder(mContext);
            creditMemoMailDialog.setMessage(warningMessage).setCancelable(false)
                    .setNegativeButton(mContext.getResources().getString(android.R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton(mContext.getResources().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mSweetAlertDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
                            mSweetAlertDialog.setTitleText(mContext.getString(com.webkul.mobikul.R.string.please_wait));
                            mSweetAlertDialog.setCancelable(false);
                            mSweetAlertDialog.show();
                            MpApiConnection.sendCreditMemoMail(mContext,incrementId,creditMemoId,sendCreditMemoMailResponseDataCallback);
                            dialog.dismiss();
                        }
                    }).show();


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private Callback<SendCreditMemoMailResponseData> sendCreditMemoMailResponseDataCallback = new Callback<SendCreditMemoMailResponseData>() {
        @Override
        public void onResponse(Call<SendCreditMemoMailResponseData> call, Response<SendCreditMemoMailResponseData> response) {
            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE){
                MpApiConnection.sendCreditMemoMail(mContext,incrementId,creditMemoId,sendCreditMemoMailResponseDataCallback);
            }else {
                if (mSweetAlertDialog != null){
                    mSweetAlertDialog.dismiss();
                }
                Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_LONG).show();
                ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
            }

        }

        @Override
        public void onFailure(Call<SendCreditMemoMailResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, (Activity) mContext);
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
            }
        }
    };
}
