package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.content.Intent;

import com.webkul.mobikulmp.activity.CreditMemoDetailsActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CREDIT_MEMO_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;

/**
 * Created by vedesh.kumar on 1/6/17.
 */

public class CreditMemoListHandler {
    private Context mContext;

    public CreditMemoListHandler(Context mContext) {
        this.mContext = mContext;
    }

    public void showCreditMemoDetails(String incrementId, int creditMemoId) {
        Intent intent = new Intent(mContext, CreditMemoDetailsActivity.class);
        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, incrementId);
        intent.putExtra(BUNDLE_KEY_CREDIT_MEMO_ID, creditMemoId);
        mContext.startActivity(intent);

    }
}