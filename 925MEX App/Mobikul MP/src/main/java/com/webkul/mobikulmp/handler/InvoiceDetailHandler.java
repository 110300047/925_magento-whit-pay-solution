package com.webkul.mobikulmp.handler;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.model.SendInvoiceOrderMailResponse;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_AUTH_FAILURE;

/**
 * Created by vedesh.kumar on 30/5/17.
 */

public class InvoiceDetailHandler {
    private Context mContext;
    private String incrementId;
    private int invoiceId;
    private SweetAlertDialog mSweetAlertDialog;

    public InvoiceDetailHandler(Context mContext, String incrementId, int invoiceId) {
        this.mContext = mContext;
        this.incrementId = incrementId;
        this.invoiceId = invoiceId;
    }

    public void sendInvoiceMail(String mailWarningMessage){
        try{
            final AlertDialog.Builder invoiceMailDialog = new AlertDialog.Builder(mContext);
            invoiceMailDialog.setMessage(mailWarningMessage).setCancelable(false)
                    .setNegativeButton(mContext.getResources().getString(android.R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton(mContext.getResources().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mSweetAlertDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
                            mSweetAlertDialog.setTitleText(mContext.getString(com.webkul.mobikul.R.string.please_wait));
                            mSweetAlertDialog.setCancelable(false);
                            mSweetAlertDialog.show();
                            MpApiConnection.sendInvoiceMAil(mContext,incrementId,invoiceId,sendInvoiceOrderMailResponseCallback);
                            dialog.dismiss();
                        }
                    }).show();

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private Callback<SendInvoiceOrderMailResponse> sendInvoiceOrderMailResponseCallback = new Callback<SendInvoiceOrderMailResponse>() {
        @Override
        public void onResponse(Call<SendInvoiceOrderMailResponse> call, Response<SendInvoiceOrderMailResponse> response) {
            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE){
                MpApiConnection.sendInvoiceMAil(mContext,incrementId,invoiceId,sendInvoiceOrderMailResponseCallback);
            }else {
                if (mSweetAlertDialog != null){
                    mSweetAlertDialog.dismiss();
                }
                Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_LONG).show();
                ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());

            }


        }

        @Override
        public void onFailure(Call<SendInvoiceOrderMailResponse> call, Throwable t) {
            NetworkHelper.onFailure(t, (Activity) mContext);
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
            }
        }
    };
}
