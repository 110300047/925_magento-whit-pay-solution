package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerAddProductActivity;
import com.webkul.mobikulmp.activity.SellerProductsListActivity;
import com.webkul.mobikulmp.connection.MpApiConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_IMAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_NAME;

/**
 * Created by vedesh.kumar on 10/5/17. @Webkul Software Private limited
 */

public class ItemSellerProductListHandler {

    private Context mContext;
    private int mPosition;
    private Callback<BaseModel> mSellerDeleteProductCallback = new Callback<BaseModel>() {
        @Override
        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
            AlertDialogHelper.dismiss(mContext);
            ToastHelper.showToast(mContext, response.body().getMessage(), Toast.LENGTH_LONG, 0);
            if (response.body().getSuccess()) {
                ((SellerProductsListActivity) mContext).mSellerProductListRvAdapter.remove(mPosition);
                ((SellerProductsListActivity) mContext).mSellerProductListResponseData.setTotalCount((((SellerProductsListActivity) mContext).mBinding.getData().getTotalCount() - 1));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((SellerProductsListActivity) mContext).mBinding.productsRv.getAdapter().notifyDataSetChanged();
                        if (((SellerProductsListActivity) mContext).mSellerProductListRvAdapter.getItemCount() == 0) {
                            ((SellerProductsListActivity) mContext).mBinding.setData(((SellerProductsListActivity) mContext).mSellerProductListResponseData);
                            ((SellerProductsListActivity) mContext).mBinding.executePendingBindings();
                        }
                    }
                }, 1000);
            }
        }

        @Override
        public void onFailure(Call<BaseModel> call, Throwable t) {
            NetworkHelper.onFailure(t, (BaseActivity) mContext);
        }
    };

    public ItemSellerProductListHandler(Context context) {
        mContext = context;
    }

    public void onClickProductListItem(int id, String name, boolean isSelectionModeOn) {
        if (!isSelectionModeOn) {
            Intent intent = new Intent(mContext, NewProductActivity.class);
            intent.putExtra(BUNDLE_KEY_PRODUCT_ID, id);
            intent.putExtra(BUNDLE_KEY_PRODUCT_NAME, name);
            intent.putExtra(BUNDLE_KEY_PRODUCT_IMAGE, "");
            mContext.startActivity(intent);
        }
    }

    public void onClickDeleteItem(final int id, int position) {
        mPosition = position;
        AlertDialogHelper.showAlertDialogWithClickListener(
                mContext
                , SweetAlertDialog.WARNING_TYPE
                , mContext.getString(R.string.warning_are_you_sure)
                , mContext.getString(R.string.delete_product)
                , mContext.getString(R.string.message_deleted)
                , mContext.getString(R.string.dialog_cancel)
                , new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        MpApiConnection.deleteProduct(mContext, id, mSellerDeleteProductCallback);
                        sweetAlertDialog.dismissWithAnimation();
                    }
                }
                , null);
    }

    public void onClickProductEditBtn(View view, int id) {
        Intent intent = new Intent(mContext, SellerAddProductActivity.class);
        intent.putExtra(BUNDLE_KEY_PRODUCT_ID, String.valueOf(id));
        mContext.startActivity(intent);
    }
}