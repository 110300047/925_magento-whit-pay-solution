package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.content.Intent;

import com.webkul.mobikul.activity.NewProductActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_NAME;

/**
 * Created by shubham.agarwal on 15/2/17. @Webkul Software Pvt. Ltd
 */

public class MarketplaceLandingPageProductHandler {
    private Context mContext;

    public MarketplaceLandingPageProductHandler(Context context) {
        mContext = context;
    }


    public void onClickViewProduct(int productId, String productName) {
        Intent intent = new Intent(mContext, NewProductActivity.class);
        intent.putExtra(BUNDLE_KEY_PRODUCT_ID, productId);
        intent.putExtra(BUNDLE_KEY_PRODUCT_NAME, productName);
        mContext.startActivity(intent);
    }
}
