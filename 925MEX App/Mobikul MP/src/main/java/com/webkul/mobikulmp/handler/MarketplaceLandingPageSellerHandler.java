package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.content.Intent;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerProfileActivity;
import com.webkul.mobikulmp.helper.MarketplaceApplication;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CATEGORY_NAME;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_TITLE;

/**
 * Created by shubham.agarwal on 15/2/17. @Webkul Software Pvt. Ltd
 */

public class MarketplaceLandingPageSellerHandler {

    private Context mContext;

    public MarketplaceLandingPageSellerHandler(Context context) {
        mContext = context;
    }

    public void onClickViewSellerProfile(int sellerId, String sellerTitle) {
        Intent intent = new Intent(mContext, SellerProfileActivity.class);
        intent.putExtra(BUNDLE_KEY_SELLER_ID, sellerId);
        intent.putExtra(BUNDLE_KEY_SELLER_TITLE, sellerTitle);
        mContext.startActivity(intent);
    }

    public void onClickViewSellerCollection(int sellerId, String storeName) {
        Intent intent = new Intent(mContext, ((MarketplaceApplication) mContext.getApplicationContext()).getCatalogProductPageClass());
        intent.putExtra(BUNDLE_KEY_SELLER_ID, sellerId);
        intent.putExtra(BUNDLE_KEY_CATEGORY_NAME, String.format(mContext.getString(R.string.X_collection), storeName));
        mContext.startActivity(intent);
    }
}
