package com.webkul.mobikulmp.handler;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.webkul.mobikul.connection.ApiInterface;
import com.webkul.mobikul.connection.RetrofitClient;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ImageHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.customer.accountInfo.UploadPicResponseData;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerAddProductActivity;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.dialog.HintDialogFragment;
import com.webkul.mobikulmp.fragment.SellerSelectProductsFragment;
import com.webkul.mobikulmp.model.seller.AddProductData;
import com.webkul.mobikulmp.model.seller.MedialGallery;
import com.webkul.mobikulmp.model.seller.SaveProductResponseData;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_PICK_IMAGE_FROM_CAMERA;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_PICK_IMAGE_FROM_GALLERY;

/**
 * Created by vedesh.kumar on 21/2/18. @Webkul Software Private limited
 */

public class SellerAddProductActivityHandler {

    private String UPLOAD_PIC_CONTROLLER = "mobikulmphttp/product/UploadProductImage/sellerId/";
    private SellerAddProductActivity mContext;
    private Uri mFileUri;
    private int mSelectedImageBlockNo;
    private Callback<UploadPicResponseData> mUploadPicCallback = new Callback<UploadPicResponseData>() {
        @Override
        public void onResponse(Call<UploadPicResponseData> call, Response<UploadPicResponseData> response) {
            AlertDialogHelper.dismiss(mContext);
            ToastHelper.showToast(mContext, response.body().getMessage(), Toast.LENGTH_LONG, 0);
            if (response.body().isSuccess()) {
                MedialGallery medialGallery = new MedialGallery();
                medialGallery.setLabel(response.body().getName());
                medialGallery.setMediaType(response.body().getType());
                medialGallery.setFile(response.body().getFile());
                medialGallery.setUrl(response.body().getUrl());
                mContext.mSellerAddProductResponseData.getProductData().getMedialGallery().add(medialGallery);
            }
        }

        @Override
        public void onFailure(Call<UploadPicResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, mContext);
        }
    };
    private Callback<SaveProductResponseData> mSaveProductCallBack = new Callback<SaveProductResponseData>() {
        @Override
        public void onResponse(Call<SaveProductResponseData> call, Response<SaveProductResponseData> response) {
            AlertDialogHelper.dismiss(mContext);
            ToastHelper.showToast(mContext, response.body().getMessage(), Toast.LENGTH_LONG, 0);
            if (response.body().isSuccess()) {
                mContext.mProductId = response.body().getProductId();
                mContext.mBinding.setHandler(null);
                mContext.setActionbarTitle(mContext.getString(R.string.activity_title_seller_edit_product));
                mContext.callApi();
            }
        }

        @Override
        public void onFailure(Call<SaveProductResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, mContext);
        }
    };

    public SellerAddProductActivityHandler(SellerAddProductActivity context) {
        mContext = context;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public void onClickHintBtn(View view, boolean showHint, String hint) {
        if (showHint && (hint != null && !hint.isEmpty())) {
            Utils.hideKeyboard(mContext);
            FragmentManager supportFragmentManager = mContext.getSupportFragmentManager();
            HintDialogFragment hintDialogFragment = HintDialogFragment.newInstance(hint);
            FragmentTransaction transaction = supportFragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            transaction.add(R.id.main_container, hintDialogFragment, HintDialogFragment.class.getSimpleName());
            transaction.addToBackStack(HintDialogFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
            transaction.commit();
        }
    }

    public void onClickOpenCalender(final View v) {
        final Calendar dobCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date_of_birth = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                dobCalendar.set(Calendar.YEAR, year);
                dobCalendar.set(Calendar.MONTH, month);
                dobCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat dob = new SimpleDateFormat("dd MMM, yyyy", Locale.US);
                ((EditText) v).setText(dob.format(dobCalendar.getTime()));
            }
        };

        new DatePickerDialog(mContext, com.webkul.mobikul.R.style.AlertDialogTheme, date_of_birth, dobCalendar.get(Calendar.YEAR), dobCalendar.get(Calendar.MONTH), dobCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void onClickProductImage(View v, int viewNumber) {
        mSelectedImageBlockNo = viewNumber;
        intentToPickImage();
    }

    private void intentToPickImage() {
        final CharSequence[] items = {mContext.getString(R.string.choose_from_library), mContext.getString(R.string.camera_pick), mContext.getString(R.string.no)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.please_choose));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getString(R.string.choose_from_library))) {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        mContext.startActivityForResult(Intent.createChooser(intent, mContext.getString(R.string.choose_from_library)), RC_PICK_IMAGE_FROM_GALLERY);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                            mContext.requestPermissions(permissions, RC_PICK_IMAGE_FROM_GALLERY);
                        }
                    }
                } else if (items[item].equals(mContext.getString(R.string.camera_pick))) {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
                        mContext.startActivityForResult(intent, RC_PICK_IMAGE_FROM_CAMERA);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                            mContext.requestPermissions(permissions, RC_PICK_IMAGE_FROM_CAMERA);
                        }
                    }
                } else if (items[item].equals(mContext.getString(R.string.no))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public Uri getImageUri() {
        // Store image in dcim
        File imagesFolder = new File(Environment.getExternalStorageDirectory() + "/DCIM");
        if (imagesFolder.isDirectory() || imagesFolder.mkdir()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mFileUri = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".my.package.name.provider", new File(imagesFolder, "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            } else {
                mFileUri = Uri.fromFile(new File(imagesFolder, "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            }
        }
        return mFileUri;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_PICK_IMAGE_FROM_CAMERA || requestCode == RC_PICK_IMAGE_FROM_GALLERY) {
                CropImage.activity(data == null ? mFileUri : data.getData())
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(mContext);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                final Uri uri_data = CropImage.getActivityResult(data).getUri();
                final String path = getPath(mContext, uri_data);
                if (path != null) {
                    File f = new File(path);
                    mFileUri = Uri.fromFile(f);
                    switch (mSelectedImageBlockNo) {
                        case 0:
                            ImageHelper.loadFromFile(mContext.mBinding.productImage1, new File(mFileUri.getPath()));
                            mContext.mBinding.productImage2.setVisibility(View.VISIBLE);
                            break;
                        case 1:
                            ImageHelper.loadFromFile(mContext.mBinding.productImage2, new File(mFileUri.getPath()));
                            mContext.mBinding.productImage3.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            ImageHelper.loadFromFile(mContext.mBinding.productImage3, new File(mFileUri.getPath()));
                            mContext.mBinding.productImage4.setVisibility(View.VISIBLE);
                            break;
                        case 3:
                            ImageHelper.loadFromFile(mContext.mBinding.productImage4, new File(mFileUri.getPath()));
                            break;
                    }
                    uploadPic(CropImage.getActivityResult(data).getUri());
                }
            }
        }
    }

    public String getPath(Context context, Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public void uploadPic(Uri data) {
        try {
            AlertDialogHelper.showDefaultAlertDialog(mContext);

            Bitmap bitmap = BitmapFactory.decodeFile(data.getPath());

            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), bos.toByteArray());
            MultipartBody.Part multipartFileBody = MultipartBody.Part.createFormData("image", new File(data.getPath()).getName(), fileBody);

            RequestBody width = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Utils.getScreenWidth()));
            RequestBody mFactor = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mContext.getResources().getDisplayMetrics().density));

            Call<UploadPicResponseData> call = RetrofitClient.getClient().create(ApiInterface.class).uploadImage(
                    ApplicationConstant.BASE_URL + UPLOAD_PIC_CONTROLLER + AppSharedPref.getCustomerId(mContext)

                    , multipartFileBody
                    , width
                    , mFactor);
            call.enqueue(mUploadPicCallback);
        } catch (Exception e) {
            AlertDialogHelper.dismiss(mContext);
            ToastHelper.showToast(mContext, mContext.getString(R.string.error_please_try_again), Toast.LENGTH_SHORT, 0);
            e.printStackTrace();
        }
    }

    public void onClickSelectFromProductCollection(String collectionType) {
        Utils.hideKeyboard(mContext);
        FragmentManager supportFragmentManager = mContext.getSupportFragmentManager();
        SellerSelectProductsFragment sellerSelectProductsDialogFragment = SellerSelectProductsFragment.newInstance(collectionType);
        FragmentTransaction transaction = supportFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(R.id.main_container, sellerSelectProductsDialogFragment, SellerSelectProductsFragment.class.getSimpleName());
        transaction.addToBackStack(SellerSelectProductsFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        transaction.commit();
    }

    public void onClickSaveBtn(AddProductData addProductData) {
        if (addProductData.isFormValidated(mContext)) {
            AlertDialogHelper.showDefaultAlertDialog(mContext);
            MpApiConnection.saveProduct(mContext, mContext.mProductId, addProductData, mSaveProductCallBack);
        }
    }
}