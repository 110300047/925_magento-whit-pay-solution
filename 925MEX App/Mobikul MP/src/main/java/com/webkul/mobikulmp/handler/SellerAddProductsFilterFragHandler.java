package com.webkul.mobikulmp.handler;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikulmp.activity.SellerAddProductActivity;
import com.webkul.mobikulmp.dialog.SellerAddProductCollectionFilterDialogFragment;
import com.webkul.mobikulmp.fragment.SellerSelectProductsFragment;

import org.json.JSONArray;

/**
 * Created by vedesh.kumar on 24/3/17.
 */

public class SellerAddProductsFilterFragHandler {

    private SellerAddProductCollectionFilterDialogFragment mFragmentContext;

    public SellerAddProductsFilterFragHandler(SellerAddProductCollectionFilterDialogFragment sellerAddProductCollectionFilterDialogFragment) {
        mFragmentContext = sellerAddProductCollectionFilterDialogFragment;
    }

    public void applyFilters() {
        JSONArray filterJsonArray = new JSONArray();
        JSONArray filterValuesJsonArray = new JSONArray();
        JSONArray filterIdsJsonArray = new JSONArray();
        for (int noOfFilter = 0; noOfFilter < mFragmentContext.mFilterOption.size(); noOfFilter++) {
            if (mFragmentContext.mFilterOption.get(noOfFilter).getValueFrom() != null && !mFragmentContext.mFilterOption.get(noOfFilter).getValueFrom().isEmpty()) {
                if (mFragmentContext.mFilterOption.get(noOfFilter).getType().equals("textRange")) {
                    filterValuesJsonArray.put(mFragmentContext.mFilterOption.get(noOfFilter).getValueFrom() + "-" + mFragmentContext.mFilterOption.get(noOfFilter).getValueTo());
                } else {
                    filterValuesJsonArray.put(mFragmentContext.mFilterOption.get(noOfFilter).getValueFrom());
                }
                filterIdsJsonArray.put(mFragmentContext.mFilterOption.get(noOfFilter).getId());
            }
        }
        filterJsonArray.put(filterValuesJsonArray);
        filterJsonArray.put(filterIdsJsonArray);
        ((SellerSelectProductsFragment) ((SellerAddProductActivity) mFragmentContext.getContext())
                .getSupportFragmentManager().findFragmentByTag(SellerSelectProductsFragment.class.getSimpleName())).mFilterData = filterJsonArray;
        ((SellerSelectProductsFragment) ((SellerAddProductActivity) mFragmentContext.getContext())
                .getSupportFragmentManager().findFragmentByTag(SellerSelectProductsFragment.class.getSimpleName())).isFirstCall = true;
        ((SellerSelectProductsFragment) ((SellerAddProductActivity) mFragmentContext.getContext())
                .getSupportFragmentManager().findFragmentByTag(SellerSelectProductsFragment.class.getSimpleName())).mPageNumber = 0;
        ((SellerSelectProductsFragment) ((SellerAddProductActivity) mFragmentContext.getContext())
                .getSupportFragmentManager().findFragmentByTag(SellerSelectProductsFragment.class.getSimpleName())).callApi();
        ((BaseActivity) mFragmentContext.getContext()).onBackPressed();
    }

    public void onClickOutsideFilter() {
        ((BaseActivity) mFragmentContext.getContext()).onBackPressed();
    }
}
