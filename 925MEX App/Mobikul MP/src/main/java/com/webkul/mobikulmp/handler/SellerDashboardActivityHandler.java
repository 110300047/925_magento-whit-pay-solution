package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerDashboardActivity;
import com.webkul.mobikulmp.fragment.SellerRecentOrdersListFragment;
import com.webkul.mobikulmp.fragment.SellerRecentReviewListFragment;
import com.webkul.mobikulmp.model.seller.SellerDashboardData;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;

/**
 * Created by shubham.agarwal on 23/3/17. @Webkul Software Private limited
 */

public class SellerDashboardActivityHandler {

    private final Context mContext;
    private final SellerDashboardData mData;

    public SellerDashboardActivityHandler(Context context, SellerDashboardData data) {
        mContext = context;
        mData = data;
    }

    public void viewSellerOrders() {
        FragmentManager fragmentManager = ((SellerDashboardActivity) mContext).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.main_container, SellerRecentOrdersListFragment.newInstance(mData.getRecentOrderList()));
        fragmentTransaction.addToBackStack(SellerRecentOrdersListFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        fragmentTransaction.commit();
    }

    public void viewSellerReviews() {
        FragmentManager fragmentManager = ((SellerDashboardActivity) mContext).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.add(R.id.main_container, SellerRecentReviewListFragment.newInstance(mData.getSellerReviewList()));
        fragmentTransaction.addToBackStack(SellerRecentReviewListFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        fragmentTransaction.commit();
    }
}