package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.webkul.mobikul.activity.LoginAndSignUpActivity;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.BecomeSellerActivity;
import com.webkul.mobikulmp.activity.SellerListActivity;
import com.webkul.mobikulmp.helper.MpAppSharedPref;

import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_UP_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;

/**
 * Created by shubham.agarwal on 15/2/17. @Webkul Software Pvt. Ltd
 */

public class SellerListActivityHandler {
    private Context mContext;

    public SellerListActivityHandler(Context context) {
        mContext = context;
    }

    public void onClickSearchSeller() {
        Utils.hideKeyboard(mContext);
        ((SellerListActivity) mContext).mStoreName = ((SellerListActivity) mContext).mBinding.searchSellerEt.getText().toString();
        AlertDialogHelper.showDefaultAlertDialog(mContext);
        ((SellerListActivity) mContext).callApi();
    }

    public void onClickOpenYourShop() {
        if (AppSharedPref.isLoggedIn(mContext)) {
            if (MpAppSharedPref.isSellerPending(mContext)) {
                ToastHelper.showToast(mContext, mContext.getString(R.string.nav_seller_pending_status), Toast.LENGTH_LONG, 0);
            } else if (MpAppSharedPref.isSeller(mContext)) {
                ToastHelper.showToast(mContext, mContext.getString(R.string.you_are_already_selling_with_us), Toast.LENGTH_LONG, 0);
            } else {
                mContext.startActivity(new Intent(mContext, BecomeSellerActivity.class));
            }
        } else {
            Intent intent = new Intent(mContext, LoginAndSignUpActivity.class);
            intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_UP_PAGE);
            mContext.startActivity(intent);
        }
    }
}
