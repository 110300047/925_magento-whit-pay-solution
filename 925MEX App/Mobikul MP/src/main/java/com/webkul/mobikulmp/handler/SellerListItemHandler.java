package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.content.Intent;

import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerProfileActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CATEGORY_NAME;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;

/**
 * Created by shubham.agarwal on 15/2/17. @Webkul Software Pvt. Ltd
 */

public class SellerListItemHandler {
    private Context mContext;

    public SellerListItemHandler(Context context) {
        mContext = context;
    }

    public void onClickViewSellerProfile(int sellerId) {
        Intent intent = new Intent(mContext, SellerProfileActivity.class);
        intent.putExtra(BUNDLE_KEY_SELLER_ID, sellerId);
        mContext.startActivity(intent);
    }

    public void onClickViewSellerCollection(int sellerId, String storeName) {
        Intent intent = new Intent(mContext, ((MobikulApplication) mContext.getApplicationContext()).getCatalogProductPageClass());
        intent.putExtra(BUNDLE_KEY_SELLER_ID, sellerId);
        intent.putExtra(BUNDLE_KEY_CATEGORY_NAME, String.format(mContext.getString(R.string.X_collection), storeName));
        mContext.startActivity(intent);
    }
}
