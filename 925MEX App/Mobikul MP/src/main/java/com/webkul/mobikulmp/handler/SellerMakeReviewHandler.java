package com.webkul.mobikulmp.handler;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.model.SellerMakeReviewData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */

public class SellerMakeReviewHandler {

    private int mSellerId;
    private String mShopUrl;
    private Context mContext;

    private Callback<BaseModel> mSaveReviewCallback = new Callback<BaseModel>() {
        @Override
        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
            AlertDialogHelper.dismiss(mContext);
            ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
            SnackbarHelper.getSnackbar((Activity) mContext, response.body().getMessage()).show();
            if (response.body().isSuccess()) {
                ((AppCompatActivity) mContext).getSupportFragmentManager().popBackStack();
            }
        }

        @Override
        public void onFailure(Call<BaseModel> call, Throwable t) {
            NetworkHelper.onFailure(t, (Activity) mContext);
        }
    };

    public SellerMakeReviewHandler(Context context, int sellerId, String shopUrl) {
        mContext = context;
        mSellerId = sellerId;
        mShopUrl = shopUrl;
    }

    public void onClickSaveReview(SellerMakeReviewData data) {
        data.setDisplayError(true);
        if (data.isFormValidated(mContext)) {
            AlertDialogHelper.showDefaultAlertDialog(mContext);
            MpApiConnection.saveReview(mContext, mSellerId, data.getNickName(), data.getSummary(), data.getComment(), data.getPriceRating() * 20
                    , data.getValueRating() * 20, data.getQuantityRating() * 20, mShopUrl, mSaveReviewCallback);
        } else {
            Utils.hideKeyboard((Activity) mContext);
            Toast.makeText(mContext, R.string.msg_fill_req_field, Toast.LENGTH_SHORT).show();
        }
    }
}
