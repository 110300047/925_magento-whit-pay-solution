package com.webkul.mobikulmp.handler;

import android.app.DownloadManager;
import android.content.Context;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerOrderActivity;
import com.webkul.mobikulmp.dialog.SellerOrderDownloadAllDialogFragment;
import com.webkul.mobikulmp.dialog.SellerOrderFilterDialogFragment;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;

/**
 * Created by vedesh.kumar on 24/3/17. @Webkul Software Private limited
 */

public class SellerOrderActivityHandler {
    private Context mContext;

    public SellerOrderActivityHandler(Context context) {
        mContext = context;
    }

    public void viewOrderFilterDialog() {
        FragmentManager supportFragmentManager = ((SellerOrderActivity) mContext).getSupportFragmentManager();
        SellerOrderFilterDialogFragment sellerOrderFilterDialogFragment = SellerOrderFilterDialogFragment.newInstance(((SellerOrderActivity) mContext).mSellerOrderFilterFragData);
        FragmentTransaction transaction = supportFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(R.id.main_container, sellerOrderFilterDialogFragment, SellerOrderFilterDialogFragment.class.getSimpleName());
        transaction.addToBackStack(SellerOrderFilterDialogFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        transaction.commit();
    }

    public void onClickDownloadAllSlip(int type) {
        FragmentManager supportFragmentManager = ((SellerOrderActivity) mContext).getSupportFragmentManager();
        SellerOrderDownloadAllDialogFragment sellerOrderDownloadAllDialogFragment = SellerOrderDownloadAllDialogFragment.newInstance(type);
        FragmentTransaction transaction = supportFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        sellerOrderDownloadAllDialogFragment.show(transaction, SellerOrderDownloadAllDialogFragment.class.getSimpleName());
    }
}