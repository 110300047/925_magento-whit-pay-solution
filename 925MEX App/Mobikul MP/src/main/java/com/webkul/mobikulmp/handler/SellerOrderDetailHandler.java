package com.webkul.mobikulmp.handler;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikulmp.activity.CreditMemoListActivity;
import com.webkul.mobikulmp.activity.InvoiceDetailsActivity;
import com.webkul.mobikulmp.activity.SellerOrderDetailActivity;
import com.webkul.mobikulmp.activity.ShipmentDetailsActivity;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.model.CancelOrderResponseData;
import com.webkul.mobikulmp.model.CreateInvoiceResponseData;
import com.webkul.mobikulmp.model.SendOrderMailResponse;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INVOICE_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SHIPMENT_ID;
import static com.webkul.mobikul.helper.NetworkHelper.NW_RESPONSE_CODE_AUTH_FAILURE;

/**
 * Created by vedesh.kumar on 26/5/17.
 */

public class SellerOrderDetailHandler {
    private Context mContext;
    private String incrementId;
    private SweetAlertDialog mSweetAlertDialog;

    public SellerOrderDetailHandler(Context mContext, String incrementId) {
        this.mContext = mContext;
        this.incrementId = incrementId;
    }

    public void showInvoiceDetails(int invoiceId) {
        Intent intent = new Intent(mContext, InvoiceDetailsActivity.class);
        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, incrementId);
        intent.putExtra(BUNDLE_KEY_INVOICE_ID, invoiceId);
        mContext.startActivity(intent);
    }

    public void showShipmentDetails(int shipmentId) {
        Intent intent = new Intent(mContext, ShipmentDetailsActivity.class);
        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, incrementId);
        intent.putExtra(BUNDLE_KEY_SHIPMENT_ID, shipmentId);
        mContext.startActivity(intent);
    }

    public void showCreditMemoList() {
        Intent intent = new Intent(mContext, CreditMemoListActivity.class);
        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, incrementId);
        mContext.startActivity(intent);
    }


    public void sendCancelOrderRequest(String cancelOrderWarningMesasge) {
        try {
            final AlertDialog.Builder cancelOrderDialog = new AlertDialog.Builder(mContext);
            cancelOrderDialog.setMessage(cancelOrderWarningMesasge).setCancelable(false)
                    .setNegativeButton(mContext.getResources().getString(android.R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setPositiveButton(mContext.getResources().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mSweetAlertDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
                    mSweetAlertDialog.setTitleText(mContext.getString(com.webkul.mobikul.R.string.please_wait));
                    mSweetAlertDialog.setCancelable(false);
                    mSweetAlertDialog.show();
                    MpApiConnection.getCancelOrderResponseData(mContext, incrementId, mCancelOrderResponseDataCallback);
                    dialog.dismiss();
                }
            }).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendCreateInvoiceRequest(String createInvoiceWarningMesasge) {
        try {
            final AlertDialog.Builder createInvoiceDialog = new AlertDialog.Builder(mContext);
            createInvoiceDialog.setMessage(createInvoiceWarningMesasge).setCancelable(false)
                    .setNegativeButton(mContext.getResources().getString(android.R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setPositiveButton(mContext.getResources().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mSweetAlertDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
                    mSweetAlertDialog.setTitleText(mContext.getString(com.webkul.mobikul.R.string.please_wait));
                    mSweetAlertDialog.setCancelable(false);
                    mSweetAlertDialog.show();
                    MpApiConnection.getCreateInvoiceResponseData(mContext, incrementId, mCreateInvoiceResponseDataCallback);
                    dialog.dismiss();
                }
            }).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendOrderMail(String sendEmailWarningMessage) {
        try {

            final AlertDialog.Builder sendEmailDialog = new AlertDialog.Builder(mContext);
            sendEmailDialog.setMessage(sendEmailWarningMessage).setCancelable(false)
                    .setPositiveButton(mContext.getResources().getString(android.R.string.yes), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mSweetAlertDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
                            mSweetAlertDialog.setTitleText(mContext.getString(com.webkul.mobikul.R.string.please_wait));
                            mSweetAlertDialog.setCancelable(false);
                            mSweetAlertDialog.show();
                            MpApiConnection.getSendOrderMailData(mContext, incrementId, mSendOrderMailResponseCallBack);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(mContext.getResources().getString(android.R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private Callback<SendOrderMailResponse> mSendOrderMailResponseCallBack = new Callback<SendOrderMailResponse>() {
        @Override
        public void onResponse(Call<SendOrderMailResponse> call, Response<SendOrderMailResponse> response) {

            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE) {
                MpApiConnection.getSendOrderMailData(mContext, incrementId, mSendOrderMailResponseCallBack);
            } else {
                if (mSweetAlertDialog != null) {
                    mSweetAlertDialog.dismiss();
                }
                ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
                Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(Call<SendOrderMailResponse> call, Throwable t) {
            NetworkHelper.onFailure(t, (Activity) mContext);
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
            }
        }
    };

    private Callback<CreateInvoiceResponseData> mCreateInvoiceResponseDataCallback = new Callback<CreateInvoiceResponseData>() {
        @Override
        public void onResponse(Call<CreateInvoiceResponseData> call, Response<CreateInvoiceResponseData> response) {
            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE) {
                MpApiConnection.getCreateInvoiceResponseData(mContext, incrementId, mCreateInvoiceResponseDataCallback);
            } else {
                if (mSweetAlertDialog != null) {
                    mSweetAlertDialog.dismiss();
                }
                ((SellerOrderDetailActivity) mContext).recreate();
                ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
                Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_LONG).show();

            }
        }

        @Override
        public void onFailure(Call<CreateInvoiceResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, (Activity) mContext);
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
            }
        }
    };

    private Callback<CancelOrderResponseData> mCancelOrderResponseDataCallback = new Callback<CancelOrderResponseData>() {
        @Override
        public void onResponse(Call<CancelOrderResponseData> call, Response<CancelOrderResponseData> response) {
            if (response.body().getResponseCode() == NW_RESPONSE_CODE_AUTH_FAILURE) {
                MpApiConnection.getCancelOrderResponseData(mContext, incrementId, mCancelOrderResponseDataCallback);
            } else {
                if (mSweetAlertDialog != null) {
                    mSweetAlertDialog.dismiss();
                }
                ApplicationSingleton.getInstance().setAuthKey(response.body().getAuthKey());
                Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(Call<CancelOrderResponseData> call, Throwable t) {
            NetworkHelper.onFailure(t, (Activity) mContext);
            if (mSweetAlertDialog != null) {
                mSweetAlertDialog.dismiss();
            }
        }
    };
}
