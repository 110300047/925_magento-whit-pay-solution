package com.webkul.mobikulmp.handler;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.fragment.DatePickerFragment;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.dialog.SellerOrderDownloadAllDialogFragment;

import static com.webkul.mobikul.constants.ApplicationConstant.BASE_URL;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_WRITE_TO_EXTERNAL_STORAGE;
import static com.webkul.mobikul.fragment.DatePickerFragment.FROM_DATE_DOB;
import static com.webkul.mobikul.fragment.DatePickerFragment.TO_DATE_DOB;
import static com.webkul.mobikulmp.connection.MpApiInterface.MOBIKUL_DOWNLOAD_ALL_INVOICE;
import static com.webkul.mobikulmp.connection.MpApiInterface.MOBIKUL_DOWNLOAD_ALL_SHIPPING;

/**
 * Created by shubham.agarwal on 24/3/17. @Webkul Software Private limited
 */

public class SellerOrderDownloadAllFragHandler implements DatePickerFragment.OnDateSelected {
    @SuppressWarnings("unused")
    private SellerOrderDownloadAllDialogFragment mSellerOrderDownloadAllDialogFragment;
    private long mDownloadId;
    private BroadcastReceiver downloadCompleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(mDownloadId);
            Cursor cursor = downloadManager.query(query);

            // it shouldn't be empty, but just in case
            if (!cursor.moveToFirst()) {
                int statusIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if (DownloadManager.STATUS_SUCCESSFUL != cursor.getInt(statusIndex)) {
                    Log.w(ApplicationConstant.TAG, "There are no printable documents related to selected date range.");
                    ToastHelper.showToast(mSellerOrderDownloadAllDialogFragment.getContext(), "There are no printable documents related to selected date range.", Toast.LENGTH_SHORT, 0);
                }
            }
        }
    };

    public SellerOrderDownloadAllFragHandler(SellerOrderDownloadAllDialogFragment sellerOrderDownloadAllDialogFragment) {
        mSellerOrderDownloadAllDialogFragment = sellerOrderDownloadAllDialogFragment;
    }

    public void reset() {
        mSellerOrderDownloadAllDialogFragment.mBinding.getData().resetData();
    }

    public void pickFromDate() {
        DialogFragment newFragment = DatePickerFragment.newInstance(this, "", FROM_DATE_DOB);
        newFragment.show(((AppCompatActivity) mSellerOrderDownloadAllDialogFragment.getContext()).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void pickToDate() {
        DialogFragment newFragment = DatePickerFragment.newInstance(this, "", TO_DATE_DOB);
        newFragment.show(((AppCompatActivity) mSellerOrderDownloadAllDialogFragment.getContext()).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    @Override
    public void onDateSet(int year, int month, int day, int type) {
        switch (type) {
            case FROM_DATE_DOB:
                mSellerOrderDownloadAllDialogFragment.mBinding.getData().setFromDate(Utils.formatDate(null, year, month, day));
                break;
            case TO_DATE_DOB:
                mSellerOrderDownloadAllDialogFragment.mBinding.getData().setToDate(Utils.formatDate(null, year, month, day));
                break;
        }
    }

    public void submit() {
        if (mSellerOrderDownloadAllDialogFragment.mBinding.getData().getFromDate().isEmpty() || mSellerOrderDownloadAllDialogFragment.mBinding.getData().getToDate().isEmpty()) {
            ToastHelper.showToast(mSellerOrderDownloadAllDialogFragment.getContext(), mSellerOrderDownloadAllDialogFragment.getContext().getResources().getString(R.string.fill_all_req_option), Toast.LENGTH_SHORT, 0);
        } else {
            if (ActivityCompat.checkSelfPermission(mSellerOrderDownloadAllDialogFragment.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    ((Activity) mSellerOrderDownloadAllDialogFragment.getContext()).requestPermissions(permissions, RC_WRITE_TO_EXTERNAL_STORAGE);
                }
            } else {
                String url = BASE_URL;
                String name;
                if (mSellerOrderDownloadAllDialogFragment.mBinding.getType() == 0) {
                    url = url + MOBIKUL_DOWNLOAD_ALL_INVOICE;
                    name = "invoice_slips_";
                } else {
                    url = url + MOBIKUL_DOWNLOAD_ALL_SHIPPING;
                    name = "shipping_slips_";
                }
                url = url + "?storeId=" + AppSharedPref.getStoreId(mSellerOrderDownloadAllDialogFragment.getContext())
                        + "&dateFrom=" + mSellerOrderDownloadAllDialogFragment.mBinding.getData().getFromDate()
                        + "&dateTo=" + mSellerOrderDownloadAllDialogFragment.mBinding.getData().getToDate();
                name = name + mSellerOrderDownloadAllDialogFragment.mBinding.getData().getFromDate() + "_to_" + mSellerOrderDownloadAllDialogFragment.mBinding.getData().getToDate() + ".pdf";
                downloadSlips(url, name.replace("/", "-"));
                mSellerOrderDownloadAllDialogFragment.dismiss();
            }
        }
    }

    private void downloadSlips(String url, String fileName) {
        Log.d(ApplicationConstant.TAG, "downloadSlips: url: " + url);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(mSellerOrderDownloadAllDialogFragment.getContext().getResources().getString(R.string.download_started));
        request.setTitle(fileName);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        request.addRequestHeader("authKey", ApplicationSingleton.getInstance().getAuthKey());
        request.addRequestHeader("apiKey", ApplicationConstant.API_USER_NAME);
        request.addRequestHeader("apiPassword", ApplicationConstant.API_PASSWORD);
        request.addRequestHeader("customerId", String.valueOf(AppSharedPref.getCustomerId(mSellerOrderDownloadAllDialogFragment.getContext())));
        request.setMimeType("application/pdf");
        DownloadManager manager = (DownloadManager) mSellerOrderDownloadAllDialogFragment.getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        mDownloadId = manager.enqueue(request);

        mSellerOrderDownloadAllDialogFragment.getContext().registerReceiver(downloadCompleteReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }
}