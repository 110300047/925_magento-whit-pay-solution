package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.fragment.DatePickerFragment;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikulmp.activity.SellerOrderActivity;
import com.webkul.mobikulmp.dialog.SellerOrderFilterDialogFragment;
import com.webkul.mobikulmp.model.SellerOrderFilterFragData;

import static com.webkul.mobikul.fragment.DatePickerFragment.FROM_DATE_DOB;
import static com.webkul.mobikul.fragment.DatePickerFragment.TO_DATE_DOB;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerOrderFilterFragHandler implements DatePickerFragment.OnDateSelected {
    @SuppressWarnings("unused")
    private static final String TAG = "SellerOrderFilterFragHa";
    private Context mContext;
    private SellerOrderFilterDialogFragment mSellerOrderFilterDialogFragment;
    private SellerOrderFilterFragData mData;

    public SellerOrderFilterFragHandler(Context context, SellerOrderFilterDialogFragment sellerOrderFilterDialogFragment, SellerOrderFilterFragData data) {
        mContext = context;
        mSellerOrderFilterDialogFragment = sellerOrderFilterDialogFragment;
        mData = data;
    }

    public void applyFilters() {
        ((SellerOrderActivity) mContext).onOrderFilterApplied(mData);
        mSellerOrderFilterDialogFragment.dismiss();
    }

    public void resetFilters() {
        mData.resetFilters();
    }

    public void pickFromDate() {
        DialogFragment newFragment = DatePickerFragment.newInstance(this, "", FROM_DATE_DOB);
        newFragment.show(((AppCompatActivity) mContext).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void pickToDate() {
        DialogFragment newFragment = DatePickerFragment.newInstance(this, "", TO_DATE_DOB);
        newFragment.show(((AppCompatActivity) mContext).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void onClickOutsideFilter() {
        ((BaseActivity) mContext).onBackPressed();
    }

    @Override
    public void onDateSet(int year, int month, int day, int type) {
        switch (type) {
            case FROM_DATE_DOB:
                mData.setFromDate(Utils.formatDate(null, year, month, day));
                break;
            case TO_DATE_DOB:
                mData.setToDate(Utils.formatDate(null, year, month, day));
                break;
        }
    }


    @SuppressWarnings("unused")
    public interface OnOrderFilterAppliedListener {
        void onOrderFilterApplied(SellerOrderFilterFragData sellerOrderFilterFragData);
    }
}
