package com.webkul.mobikulmp.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikulmp.activity.SellerOrderDetailActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_INCREMENT_ID;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerOrderItemRecyclerHandler {

    public void viewOrderDetails(View view, String incrementId) {
        Intent intent = new Intent(view.getContext(), SellerOrderDetailActivity.class);
        intent.putExtra(BUNDLE_KEY_INCREMENT_ID, incrementId);
        view.getContext().startActivity(intent);
    }
}