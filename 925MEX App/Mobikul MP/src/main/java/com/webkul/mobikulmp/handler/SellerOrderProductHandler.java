package com.webkul.mobikulmp.handler;

/**
 * Created by shubham.agarwal on 24/3/17.
 */


import android.content.Context;
import android.content.Intent;

import com.webkul.mobikul.activity.NewProductActivity;
import com.webkul.mobikulmp.model.seller.SellerOrderProductList;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_NAME;

/**
 * Handler class used to handler click events coming from seller order item from seller order list especially
 * click actions from product name
 */
public class SellerOrderProductHandler {

    private final Context mContext;
    private final SellerOrderProductList mData;

    public SellerOrderProductHandler(Context context, SellerOrderProductList eachOrderData) {
        this.mContext = context;
        this.mData = eachOrderData;
    }

    public void viewProduct() {
        if (!mData.getProductId().equals("0")) {
            Intent intent = new Intent(mContext, NewProductActivity.class);
            intent.putExtra(BUNDLE_KEY_PRODUCT_ID, Integer.parseInt(mData.getProductId()));
            intent.putExtra(BUNDLE_KEY_PRODUCT_NAME, mData.getName());
            mContext.startActivity(intent);
        }
    }
}