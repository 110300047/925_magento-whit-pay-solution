package com.webkul.mobikulmp.handler;

import android.support.v4.app.DialogFragment;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.fragment.DatePickerFragment;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikulmp.activity.SellerProductsListActivity;
import com.webkul.mobikulmp.dialog.SellerProductsFilterDialogFragment;
import com.webkul.mobikulmp.model.SellerOrderFilterFragData;

import static com.webkul.mobikul.fragment.DatePickerFragment.FROM_DATE_DOB;
import static com.webkul.mobikul.fragment.DatePickerFragment.TO_DATE_DOB;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerProductsFilterFragHandler implements DatePickerFragment.OnDateSelected {
    @SuppressWarnings("unused")
    private static final String TAG = "SellerOrderFilterFragHa";
    private SellerProductsFilterDialogFragment mSellerProductsFilterDialogFragment;

    public SellerProductsFilterFragHandler(SellerProductsFilterDialogFragment sellerProductsFilterDialogFragment) {
        mSellerProductsFilterDialogFragment = sellerProductsFilterDialogFragment;
    }

    public void applyFilters() {
        ((SellerProductsListActivity) mSellerProductsFilterDialogFragment.getContext()).isFirstCall = true;
        ((SellerProductsListActivity) mSellerProductsFilterDialogFragment.getContext()).mPageNumber = 1;
        ((SellerProductsListActivity) mSellerProductsFilterDialogFragment.getContext()).mProductName = mSellerProductsFilterDialogFragment.mBinding.getProductName();
        ((SellerProductsListActivity) mSellerProductsFilterDialogFragment.getContext()).mDateFrom = mSellerProductsFilterDialogFragment.mBinding.getFromDate();
        ((SellerProductsListActivity) mSellerProductsFilterDialogFragment.getContext()).mDateTo = mSellerProductsFilterDialogFragment.mBinding.getToDate();
        ((SellerProductsListActivity) mSellerProductsFilterDialogFragment.getContext()).mProductStatus =
                String.valueOf(mSellerProductsFilterDialogFragment.mBinding.productStatusSpinner.getSelectedItemPosition());
        ((SellerProductsListActivity) mSellerProductsFilterDialogFragment.getContext()).callApi();
        ((SellerProductsListActivity) mSellerProductsFilterDialogFragment.getContext()).getSupportFragmentManager().popBackStack();
    }

    public void resetFilters() {
        mSellerProductsFilterDialogFragment.mBinding.setProductName("");
        mSellerProductsFilterDialogFragment.mBinding.setFromDate("");
        mSellerProductsFilterDialogFragment.mBinding.setToDate("");
        mSellerProductsFilterDialogFragment.mBinding.productStatusSpinner.setSelection(0);
    }

    public void pickFromDate() {
        DialogFragment newFragment = DatePickerFragment.newInstance(this, "", FROM_DATE_DOB);
        newFragment.show(((BaseActivity) mSellerProductsFilterDialogFragment.getContext()).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void pickToDate() {
        DialogFragment newFragment = DatePickerFragment.newInstance(this, "", TO_DATE_DOB);
        newFragment.show(((BaseActivity) mSellerProductsFilterDialogFragment.getContext()).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void onClickOutsideFilter() {
        ((BaseActivity) mSellerProductsFilterDialogFragment.getContext()).onBackPressed();
    }

    @Override
    public void onDateSet(int year, int month, int day, int type) {
        switch (type) {
            case FROM_DATE_DOB:
                mSellerProductsFilterDialogFragment.mBinding.setFromDate(Utils.formatDate(null, year, month, day));
                break;
            case TO_DATE_DOB:
                mSellerProductsFilterDialogFragment.mBinding.setToDate(Utils.formatDate(null, year, month, day));
                break;
        }
    }


    @SuppressWarnings("unused")
    public interface OnOrderFilterAppliedListener {
        void onOrderFilterApplied(SellerOrderFilterFragData sellerOrderFilterFragData);
    }
}
