package com.webkul.mobikulmp.handler;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerProductsListActivity;
import com.webkul.mobikulmp.dialog.SellerProductsFilterDialogFragment;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;

/**
 * Created by vedesh.kumar on 24/3/17. @Webkul Software Private limited
 */

public class SellerProductsListActivityHandler {

    private SellerProductsListActivity mSellerProductsListActivity;

    public SellerProductsListActivityHandler(SellerProductsListActivity sellerProductsListActivity) {

        mSellerProductsListActivity = sellerProductsListActivity;
    }

    public void viewProductsFilterDialog(View v) {
        mSellerProductsListActivity.mBinding.filterBtn.animate().alpha(1.0f).translationY(200).setInterpolator(new AccelerateInterpolator(1.4f));
        FragmentManager supportFragmentManager = ((SellerProductsListActivity) v.getContext()).getSupportFragmentManager();
        SellerProductsFilterDialogFragment sellerProductsFilterDialogFragment = new SellerProductsFilterDialogFragment();
        FragmentTransaction transaction = supportFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(R.id.main_container, sellerProductsFilterDialogFragment, SellerProductsFilterDialogFragment.class.getSimpleName());
        transaction.addToBackStack(SellerProductsFilterDialogFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        transaction.commit();
    }
}