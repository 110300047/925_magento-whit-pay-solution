package com.webkul.mobikulmp.handler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;
import com.webkul.mobikul.activity.LoginAndSignUpActivity;
import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerProfileActivity;
import com.webkul.mobikulmp.fragment.ContactSellerFragment;
import com.webkul.mobikulmp.fragment.SellerMakeReviewFragment;
import com.webkul.mobikulmp.fragment.SellerReviewFragment;
import com.webkul.mobikulmp.fragment.StorePolicyFragment;
import com.webkul.mobikulmp.helper.MarketplaceApplication;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.ApplicationConstant.SIGN_IN_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_CATEGORY_NAME;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELECT_PAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_SELLER_ID;

/**
 * Created by shubham.agarwal on 20/1/17. @Webkul Software Pvt. Ltd
 */

public class SellerProfileActivityHandler {
    @SuppressWarnings("unused")
    private static final String TAG = "SellerProfileActivityHa";
    private Context mContext;
    private int mSellerId;

    public SellerProfileActivityHandler(Context context, int sellerId) {
        mContext = context;
        mSellerId = sellerId;
    }

    public void onClickContactSellerBtn() {
        FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(android.R.id.content, ContactSellerFragment.newInstance(mSellerId, 0), ContactSellerFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(ContactSellerFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        fragmentTransaction.commit();
    }

    public void onClickViewLargeMap() {
        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction = ((SellerProfileActivity) mContext).getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(android.R.id.content, supportMapFragment, SupportMapFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(SupportMapFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        fragmentTransaction.commit();
        supportMapFragment.getMapAsync((SellerProfileActivity) mContext);
    }


    public void onClickAboutStoreBtn(View view) {
        WebView descriptionWebView = (WebView) view.findViewById(R.id.store_description_wv);
        TextView readMoreTv = (TextView) view.findViewById(R.id.read_more_tv);
        ViewGroup.LayoutParams lp = descriptionWebView.getLayoutParams();
        if (lp.height == ViewGroup.LayoutParams.WRAP_CONTENT) {
            lp.height = (int) Utils.convertDpToPixel(100, view.getContext());
            readMoreTv.setText(R.string.read_more);
        } else {
            lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            readMoreTv.setText(R.string.read_less);
        }
        descriptionWebView.setLayoutParams(lp);
        descriptionWebView.requestFocus();
        view.requestFocus();
    }

    public void onClickViewSellerCollection(View view, String storeName, int sellerId) {
        Intent intent = new Intent(view.getContext(), ((MarketplaceApplication) view.getContext().getApplicationContext()).getCatalogProductPageClass());
        intent.putExtra(BUNDLE_KEY_SELLER_ID, sellerId);
        intent.putExtra(BUNDLE_KEY_CATEGORY_NAME, String.format(mContext.getString(R.string.X_collection), storeName));
        view.getContext().startActivity(intent);
    }

    public void onClickViewSellerPolicies(String returnPolicy, String shippingPolicy) {
        if ((returnPolicy == null || returnPolicy.isEmpty()) && (shippingPolicy == null || shippingPolicy.isEmpty())) {
            SnackbarHelper.getSnackbar(((Activity) mContext), mContext.getString(R.string.no_policies_added_by_seller)).show();
        } else {
            FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(android.R.id.content, StorePolicyFragment.newInstance(returnPolicy, shippingPolicy), StorePolicyFragment.class.getSimpleName());
            fragmentTransaction.addToBackStack(StorePolicyFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
            fragmentTransaction.commit();
        }
    }

    public void onClickMakeSellerReview(int sellerId, String shopUrl) {
        FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(android.R.id.content, SellerMakeReviewFragment.newInstance(sellerId, shopUrl), SellerMakeReviewFragment.class.getSimpleName());
        fragmentTransaction.addToBackStack(SellerMakeReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        fragmentTransaction.commit();
    }

    public void onClickViewSellerReview(int sellerId, int recentSellerReviewCount, String shopUrl) {
        if (recentSellerReviewCount == 0) {
            SnackbarHelper.getSnackbar((Activity) mContext, mContext.getString(R.string.no_review_found_be_the_first_one)).show();
        } else {
            FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(android.R.id.content, SellerReviewFragment.newInstance(sellerId, shopUrl), SellerReviewFragment.class.getSimpleName());
            fragmentTransaction.addToBackStack(SellerReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
            fragmentTransaction.commit();
        }
    }

    public void onClickLogin(View view) {
        Intent intent = new Intent(view.getContext(), LoginAndSignUpActivity.class);
        intent.putExtra(BUNDLE_KEY_SELECT_PAGE, SIGN_IN_PAGE);
        ((Activity) mContext).startActivityForResult(intent, ApplicationConstant.RC_APP_SIGN_IN);
    }
}
