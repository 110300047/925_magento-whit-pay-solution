package com.webkul.mobikulmp.handler;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.webkul.mobikul.helper.AlertDialogHelper;
import com.webkul.mobikul.helper.ImageHelper;
import com.webkul.mobikul.helper.NetworkHelper;
import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerProfileEditActivity;
import com.webkul.mobikulmp.connection.MpApiConnection;
import com.webkul.mobikulmp.dialog.HintDialogFragment;

import java.io.File;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_PICK_IMAGE_FROM_CAMERA;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_PICK_IMAGE_FROM_GALLERY;

/**
 * Created by vedesh.kumar on 11/1/18. @Webkul Software Private limited
 */

public class SellerProfileEditActivityHandler {

    private SellerProfileEditActivity mContext;
    private String mImageType;
    private Uri mSelectedFileURI;
    private File mCompanyBannerFile;
    private File mCompanyLogoFile;
    private RequestBody mCompanyBannerRequestBody;
    private RequestBody mCompanyLogoRequestBody;
    private Callback<BaseModel> mSaveProfileCallBack = new Callback<BaseModel>() {
        @Override
        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
            AlertDialogHelper.dismiss(mContext);
            ToastHelper.showToast(mContext, response.body().getMessage(), Toast.LENGTH_LONG, 0);
            mCompanyBannerFile = null;
            mCompanyLogoFile = null;
        }

        @Override
        public void onFailure(Call<BaseModel> call, Throwable t) {
            NetworkHelper.onFailure(t, mContext);
        }
    };
    private Callback<BaseModel> mDeletImageCallBack = new Callback<BaseModel>() {
        @Override
        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
            AlertDialogHelper.dismiss(mContext);
            if (response.body().getSuccess()) {
                switch (mImageType) {
                    case "banner":
                        mContext.mBinding.getData().setBannerImage("");
                        break;
                    case "logo":
                        mContext.mBinding.getData().setProfileImage("");
                        break;
                }
            }
        }

        @Override
        public void onFailure(Call<BaseModel> call, Throwable t) {
            NetworkHelper.onFailure(t, mContext);
        }
    };

    public SellerProfileEditActivityHandler(SellerProfileEditActivity sellerProfileEditActivity) {
        mContext = sellerProfileEditActivity;
    }

    public void onClickSaveProfileBtn() {
        Utils.hideKeyboard(mContext);
        if (mCompanyBannerFile != null)
            mCompanyBannerRequestBody = RequestBody.create(MediaType.parse("image/*"), mCompanyBannerFile);
        if (mCompanyLogoFile != null)
            mCompanyLogoRequestBody = RequestBody.create(MediaType.parse("image/*"), mCompanyLogoFile);

        MpApiConnection.saveSellerProfileData(mContext, mContext.mBinding.getData(), mCompanyBannerRequestBody, mCompanyLogoRequestBody, mSaveProfileCallBack);
    }

    public void onClickDeleteBtn(String imageType) {
        mImageType = imageType;
        AlertDialogHelper.showAlertDialogWithClickListener(
                mContext
                , SweetAlertDialog.WARNING_TYPE
                , mContext.getString(R.string.warning_are_you_sure)
                , mContext.getString(R.string.deleted_image)
                , mContext.getString(R.string.message_deleted)
                , mContext.getString(R.string.dialog_cancel)
                , new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        MpApiConnection.getDeleteSellerImage(mContext, mImageType, mDeletImageCallBack);
                        sweetAlertDialog.dismissWithAnimation();
                    }
                }
                , null);
    }

    public void onClickPickColorBtn(String color) {
        if (color == null) {
            color = "#FFFFFF";
        }
        Utils.hideKeyboard(mContext);
        ColorPickerDialogBuilder
                .with(mContext)
                .setTitle(mContext.getString(R.string.choose_color))
                .initialColor(Color.parseColor(color))
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton(mContext.getString(R.string.dialog_ok), new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        String color = Integer.toHexString(selectedColor);
                        color = color.substring(2, color.length());
                        mContext.mBinding.getData().setBackgroundColor("#" + color);
                    }
                })
                .build()
                .show();
    }

    public void onClickHintBtn(View view, String hint) {
        if (hint != null && !hint.isEmpty()) {
            Utils.hideKeyboard(mContext);
            FragmentManager supportFragmentManager = mContext.getSupportFragmentManager();
            HintDialogFragment hintDialogFragment = HintDialogFragment.newInstance(hint);
            FragmentTransaction transaction = supportFragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            transaction.add(R.id.main_container, hintDialogFragment, HintDialogFragment.class.getSimpleName());
            transaction.addToBackStack(HintDialogFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
            transaction.commit();
        }
    }

    public void onClickBrowseBtn(String imageType) {
        Utils.hideKeyboard(mContext);
        mImageType = imageType;
        selectImage();
    }

    private void selectImage() {
        final CharSequence[] items = {mContext.getString(com.webkul.mobikul.R.string.choose_from_library), mContext.getString(com.webkul.mobikul.R.string.camera_pick), mContext.getString(android.R.string.cancel)};
        AlertDialog.Builder builder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            builder = new AlertDialog.Builder(mContext, R.style.AlertDialogTheme);
        } else {
            builder = new AlertDialog.Builder(mContext);
        }
        builder.setTitle(mContext.getString(R.string.please_choose));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getString(com.webkul.mobikul.R.string.choose_from_library))) {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        mContext.startActivityForResult(Intent.createChooser(intent, mContext.getString(com.webkul.mobikul.R.string.choose_from_library)), RC_PICK_IMAGE_FROM_GALLERY);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                            mContext.requestPermissions(permissions, RC_PICK_IMAGE_FROM_GALLERY);
                        }
                    }
                } else if (items[item].equals(mContext.getString(com.webkul.mobikul.R.string.camera_pick))) {
                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
                        mContext.startActivityForResult(intent, RC_PICK_IMAGE_FROM_CAMERA);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                            mContext.requestPermissions(permissions, RC_PICK_IMAGE_FROM_CAMERA);
                        }
                    }
                } else if (items[item].equals(mContext.getString(android.R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public Uri getImageUri() {
        File imagesFolder = new File(Environment.getExternalStorageDirectory() + "/DCIM");
        if (imagesFolder.isDirectory() || imagesFolder.mkdir()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mSelectedFileURI = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".my.package.name.provider", new File(imagesFolder, "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            } else {
                mSelectedFileURI = Uri.fromFile(new File(imagesFolder, "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            }
        }
        return mSelectedFileURI;
    }

    public void cropImage(Intent data) {
        CropImage.activity(data == null ? mSelectedFileURI : data.getData())
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(mImageType.equals("banner") ? 4 : 1, mImageType.equals("banner") ? 1 : 1)
                .start(mContext);
    }

    public void updateData(Intent data) {
        mSelectedFileURI = CropImage.getActivityResult(data).getUri();
        switch (mImageType) {
            case "banner":
                mCompanyBannerFile = new File(mSelectedFileURI.getPath());
                ImageHelper.loadFromFile(mContext.mBinding.bannerIv, mCompanyBannerFile);
                break;
            case "logo":
                mCompanyLogoFile = new File(mSelectedFileURI.getPath());
                ImageHelper.loadFromFile(mContext.mBinding.logoIv, mCompanyLogoFile);
                break;
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
}