package com.webkul.mobikulmp.handler;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.fragment.SellerMakeReviewFragment;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;

/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */

public class SellerReviewListHandler {
    private int mSellerId;
    private String mShopUrl;
    private Context mContext;

    public SellerReviewListHandler(Context context, int sellerId, String shopUrl) {
        mContext = context;
        mSellerId = sellerId;
        mShopUrl = shopUrl;
    }

    public void onClickMakeSellerReview() {
        if (!AppSharedPref.isLoggedIn(mContext)) {
            Toast.makeText(mContext, R.string.please_login_to_write_reviews, Toast.LENGTH_SHORT).show();
        } else {
            FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(android.R.id.content, SellerMakeReviewFragment.newInstance(mSellerId, mShopUrl), SellerMakeReviewFragment.class.getSimpleName());
            fragmentTransaction.addToBackStack(SellerMakeReviewFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
            fragmentTransaction.commit();
        }
    }
}
