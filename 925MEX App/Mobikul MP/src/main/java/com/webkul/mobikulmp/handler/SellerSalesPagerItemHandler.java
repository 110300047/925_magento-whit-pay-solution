package com.webkul.mobikulmp.handler;

import android.view.View;
import android.widget.Toast;

import com.webkul.mobikul.helper.ToastHelper;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.databinding.ItemSellerSalesPagerBinding;

import java.util.ArrayList;

/**
 * Created by vedesh.kumar on 6/1/18.
 */

public class SellerSalesPagerItemHandler {

    private ItemSellerSalesPagerBinding mItemSellerSalesPagerBinding;
    private ArrayList<String> mListOfImages;

    public SellerSalesPagerItemHandler(ItemSellerSalesPagerBinding itemSellerSalesPagerBinding, ArrayList<String> listOfImages) {
        this.mItemSellerSalesPagerBinding = itemSellerSalesPagerBinding;
        this.mListOfImages = listOfImages;
    }

    public void onClickYearBtn(View view) {
        if (mListOfImages.get(0) != null)
            mItemSellerSalesPagerBinding.setImageUrl(mListOfImages.get(0));
        else
            ToastHelper.showToast(view.getContext(), view.getContext().getString(R.string.stats_not_found), Toast.LENGTH_SHORT, 0);
    }

    public void onClickMonthBtn(View view) {
        if (mListOfImages.get(1) != null)
            mItemSellerSalesPagerBinding.setImageUrl(mListOfImages.get(1));
        else
            ToastHelper.showToast(view.getContext(), view.getContext().getString(R.string.stats_not_found), Toast.LENGTH_SHORT, 0);
    }

    public void onClickWeekBtn(View view) {
        if (mListOfImages.get(2) != null)
            mItemSellerSalesPagerBinding.setImageUrl(mListOfImages.get(2));
        else
            ToastHelper.showToast(view.getContext(), view.getContext().getString(R.string.stats_not_found), Toast.LENGTH_SHORT, 0);
    }

    public void onClickDayBtn(View view) {
        if (mListOfImages.get(3) != null)
            mItemSellerSalesPagerBinding.setImageUrl(mListOfImages.get(3));
        else
            ToastHelper.showToast(view.getContext(), view.getContext().getString(R.string.stats_not_found), Toast.LENGTH_SHORT, 0);
    }
}