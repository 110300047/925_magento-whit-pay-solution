package com.webkul.mobikulmp.handler;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;

import com.webkul.mobikul.activity.BaseActivity;
import com.webkul.mobikul.fragment.DatePickerFragment;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikulmp.activity.SellerTransactionsListActivity;
import com.webkul.mobikulmp.dialog.SellerTransactionFilterDialogFragment;

import static com.webkul.mobikul.fragment.DatePickerFragment.FROM_DATE_DOB;
import static com.webkul.mobikul.fragment.DatePickerFragment.TO_DATE_DOB;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerTransactionFilterFragHandler implements DatePickerFragment.OnDateSelected {

    private SellerTransactionFilterDialogFragment mSellerTransactionFilterDialogFragment;

    public SellerTransactionFilterFragHandler(SellerTransactionFilterDialogFragment sellerTransactionFilterDialogFragment) {
        mSellerTransactionFilterDialogFragment = sellerTransactionFilterDialogFragment;
    }

    public void applyFilters() {
        ((SellerTransactionsListActivity) mSellerTransactionFilterDialogFragment.getContext()).isFirstCall = true;
        ((SellerTransactionsListActivity) mSellerTransactionFilterDialogFragment.getContext()).mTransactionId = mSellerTransactionFilterDialogFragment.mBinding.getTransactionId();
        ((SellerTransactionsListActivity) mSellerTransactionFilterDialogFragment.getContext()).mDateFrom = mSellerTransactionFilterDialogFragment.mBinding.getFromDate();
        ((SellerTransactionsListActivity) mSellerTransactionFilterDialogFragment.getContext()).mDateTo = mSellerTransactionFilterDialogFragment.mBinding.getToDate();
        ((SellerTransactionsListActivity) mSellerTransactionFilterDialogFragment.getContext()).callApi();
        mSellerTransactionFilterDialogFragment.dismiss();
    }

    public void resetFilters() {
        mSellerTransactionFilterDialogFragment.mBinding.setTransactionId("");
        mSellerTransactionFilterDialogFragment.mBinding.setFromDate("");
        mSellerTransactionFilterDialogFragment.mBinding.setToDate("");
    }

    public void pickFromDate() {
        DialogFragment newFragment = DatePickerFragment.newInstance(this, "", FROM_DATE_DOB);
        newFragment.show(((AppCompatActivity) mSellerTransactionFilterDialogFragment.getContext()).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void pickToDate() {
        DialogFragment newFragment = DatePickerFragment.newInstance(this, "", TO_DATE_DOB);
        newFragment.show(((AppCompatActivity) mSellerTransactionFilterDialogFragment.getContext()).getSupportFragmentManager(), DatePickerFragment.class.getSimpleName());
    }

    public void onClickOutsideFilter() {
        ((BaseActivity) mSellerTransactionFilterDialogFragment.getContext()).onBackPressed();
    }

    @Override
    public void onDateSet(int year, int month, int day, int type) {
        switch (type) {
            case FROM_DATE_DOB:
                mSellerTransactionFilterDialogFragment.mBinding.setFromDate(Utils.formatDate(null, year, month, day));
                break;
            case TO_DATE_DOB:
                mSellerTransactionFilterDialogFragment.mBinding.setToDate(Utils.formatDate(null, year, month, day));
                break;
        }
    }
}
