package com.webkul.mobikulmp.handler;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerTransactionsListActivity;
import com.webkul.mobikulmp.fragment.SellerViewTransactionFragment;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerTransactionItemRecyclerHandler {

    public void viewOrderDetails(View view, String Id) {
        FragmentManager supportFragmentManager = ((SellerTransactionsListActivity) view.getContext()).getSupportFragmentManager();
        SellerViewTransactionFragment sellerViewTransactionFragment = SellerViewTransactionFragment.newinstance(Id);
        FragmentTransaction transaction = supportFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(R.id.main_container, sellerViewTransactionFragment, SellerViewTransactionFragment.class.getSimpleName());
        transaction.addToBackStack(SellerViewTransactionFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        transaction.commit();
    }
}