package com.webkul.mobikulmp.handler;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.webkul.mobikul.constants.ApplicationConstant;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.ApplicationSingleton;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerTransactionsListActivity;
import com.webkul.mobikulmp.dialog.SellerTransactionFilterDialogFragment;

import static com.webkul.mobikul.constants.ApplicationConstant.BACKSTACK_SUFFIX;
import static com.webkul.mobikul.constants.ApplicationConstant.BASE_URL;
import static com.webkul.mobikul.constants.ApplicationConstant.RC_WRITE_TO_EXTERNAL_STORAGE;
import static com.webkul.mobikulmp.connection.MpApiInterface.MOBIKUL_DOWNLOAD_TRANSACTION_LIST;

/**
 * Created by vedesh.kumar on 24/3/17. @Webkul Software Private limited
 */

public class SellerTransactionsListActivityHandler {

    public void viewTransactionFilterDialog(View v) {
        FragmentManager supportFragmentManager = ((SellerTransactionsListActivity) v.getContext()).getSupportFragmentManager();
        SellerTransactionFilterDialogFragment sellerTransactionFilterDialogFragment = new SellerTransactionFilterDialogFragment();
        FragmentTransaction transaction = supportFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(R.id.main_container, sellerTransactionFilterDialogFragment, SellerTransactionFilterDialogFragment.class.getSimpleName());
        transaction.addToBackStack(SellerTransactionFilterDialogFragment.class.getSimpleName() + BACKSTACK_SUFFIX);
        transaction.commit();
    }

    public void onClickDownloadCsv(View view) {
        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                ((Activity) view.getContext()).requestPermissions(permissions, RC_WRITE_TO_EXTERNAL_STORAGE);
            }
        } else {
            String url = BASE_URL + MOBIKUL_DOWNLOAD_TRANSACTION_LIST +
                    "/?storeId=" + AppSharedPref.getStoreId(view.getContext()) +
                    "&transactionId=" + ((SellerTransactionsListActivity) view.getContext()).mTransactionId +
                    "&dateFrom=" + ((SellerTransactionsListActivity) view.getContext()).mDateFrom +
                    "&dateTo=" + ((SellerTransactionsListActivity) view.getContext()).mDateTo;
            String fileName = "transaction_list_" + System.currentTimeMillis() + ".csv";
            downloadTransactionList(view, url, fileName);
        }
    }

    private void downloadTransactionList(View view, String url, String fileName) {
        Log.d(ApplicationConstant.TAG, "downloadSlips: url: " + url);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(view.getContext().getResources().getString(R.string.download_started));
        request.setTitle(fileName);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        request.addRequestHeader("authKey", ApplicationSingleton.getInstance().getAuthKey());
        request.addRequestHeader("apiKey", ApplicationConstant.API_USER_NAME);
        request.addRequestHeader("apiPassword", ApplicationConstant.API_PASSWORD);
        request.addRequestHeader("customerId", String.valueOf(AppSharedPref.getCustomerId(view.getContext())));
        request.setMimeType("text/csv");
        DownloadManager manager = (DownloadManager) view.getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }
}