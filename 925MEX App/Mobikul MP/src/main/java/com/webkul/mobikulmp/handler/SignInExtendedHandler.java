package com.webkul.mobikulmp.handler;

import android.content.Context;

import com.webkul.mobikul.fragment.SignInFragment;
import com.webkul.mobikul.handler.SignInHandler;
import com.webkul.mobikul.model.customer.signin.SignInResponseData;
import com.webkul.mobikulmp.helper.MpAppSharedPref;

/**
 * Created by shubham.agarwal on 22/3/17.
 */

public class SignInExtendedHandler extends SignInHandler {

    public SignInExtendedHandler(Context context, SignInFragment signInFragment) {
        super(context, signInFragment);
    }

    @Override
    protected void updateCustomerSharedPref(SignInResponseData signInResponseData) {
        super.updateCustomerSharedPref(signInResponseData);
        try {
            MpAppSharedPref.setIsSeller(mContext, signInResponseData.isSeller());
            MpAppSharedPref.setIsSellerPending(mContext, signInResponseData.isPending());
            MpAppSharedPref.setIsAdmin(mContext, signInResponseData.isAdmin());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}