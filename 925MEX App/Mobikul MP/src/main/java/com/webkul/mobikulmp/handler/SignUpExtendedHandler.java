package com.webkul.mobikulmp.handler;

import android.content.Context;

import com.webkul.mobikul.fragment.SignUpFragment;
import com.webkul.mobikul.handler.SignUpHandler;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormData;
import com.webkul.mobikul.model.customer.signup.SignUpResponseData;
import com.webkul.mobikulmp.helper.MpAppSharedPref;

/**
 * Created by vedesh.kumar on 29/6/17.
 */

public class SignUpExtendedHandler extends SignUpHandler {


    public SignUpExtendedHandler(Context context, int isSocial, SignUpFragment signUpFragment, CreateAccountFormData createAccountFormData) {
        super(context, isSocial, signUpFragment, createAccountFormData);
    }

    @Override
    protected void updateCustomerSharedPref(SignUpResponseData jsonObject) {
        super.updateCustomerSharedPref(jsonObject);
        try {
            MpAppSharedPref.setIsSeller(mContext, jsonObject.isIsSeller());
            MpAppSharedPref.setIsSellerPending(mContext, jsonObject.isIsPending());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
