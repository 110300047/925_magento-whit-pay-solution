package com.webkul.mobikulmp.handler;

import android.content.Intent;
import android.view.View;

import com.webkul.mobikul.activity.NewProductActivity;

import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_ID;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_IMAGE;
import static com.webkul.mobikul.constants.BundleKeyHelper.BUNDLE_KEY_PRODUCT_NAME;

/**
 * Created by vedesh.kumar on 6/1/18. @Webkul Software Private limited
 */

public class TopSellingProductsHandler {

    public void onClickItem(View view, int productId, String productName) {
        Intent intent = new Intent(view.getContext(), NewProductActivity.class);
        intent.putExtra(BUNDLE_KEY_PRODUCT_ID, productId);
        intent.putExtra(BUNDLE_KEY_PRODUCT_NAME, productName);
        intent.putExtra(BUNDLE_KEY_PRODUCT_IMAGE, "");
        view.getContext().startActivity(intent);
    }
}
