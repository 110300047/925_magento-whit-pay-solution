package com.webkul.mobikulmp.helper;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;
import com.webkul.mobikul.connection.ApiInterface;
import com.webkul.mobikul.connection.RetrofitClient;
import com.webkul.mobikul.model.MapResponse;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shubham.agarwal on 21/12/16. @Webkul Software Pvt. Ltd
 */

public class LocationHelper {

    /*Lcoation Contant */
    private static final String TAG = "LocationHelper";
    private static LatLng latLng = null;

    public static LatLng getLatLang(Context context, @NotNull String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if (address.size() == 0) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
        } catch (final Exception ex) {
            ex.printStackTrace();
            if (ex instanceof IOException) {
                String url = "json?address=" + strAddress;
                Call<MapResponse> call = RetrofitClient.getCLientForMap().create(ApiInterface.class).getGoogleMapResponse(url);
                call.enqueue(mapResponseCallback);
            }
        }
        return latLng;
    }

    private static Callback<MapResponse> mapResponseCallback = new Callback<MapResponse>() {
        @Override
        public void onResponse(Call<MapResponse> call, Response<MapResponse> response) {
            if (response.body().getStatus().equalsIgnoreCase("ok")) {
                latLng = new LatLng(response.body().getResults().get(0).getGeometry().getLocation().getLat(), response.body().getResults().get(0).getGeometry().getLocation().getLng());
            }
        }

        @Override
        public void onFailure(Call<MapResponse> call, Throwable t) {
            t.printStackTrace();
        }
    };
}
