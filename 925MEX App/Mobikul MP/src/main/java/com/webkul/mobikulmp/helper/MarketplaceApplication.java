package com.webkul.mobikulmp.helper;

import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.webkul.mobikul.fragment.SignInFragment;
import com.webkul.mobikul.fragment.SignUpFragment;
import com.webkul.mobikul.handler.SignInHandler;
import com.webkul.mobikul.handler.SignUpHandler;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.MobikulApplication;
import com.webkul.mobikul.model.customer.signup.CreateAccountFormData;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.BecomeSellerActivity;
import com.webkul.mobikulmp.activity.CatalogProductExtendedActivity;
import com.webkul.mobikulmp.activity.ChatActivity;
import com.webkul.mobikulmp.activity.ChatWithSellersActivity;
import com.webkul.mobikulmp.activity.HomeExtendedActivity;
import com.webkul.mobikulmp.activity.ManagePrintPdfHeaderActivity;
import com.webkul.mobikulmp.activity.MarketplaceLandingActivity;
import com.webkul.mobikulmp.activity.SellerAddProductActivity;
import com.webkul.mobikulmp.activity.SellerDashboardActivity;
import com.webkul.mobikulmp.activity.SellerOrderActivity;
import com.webkul.mobikulmp.activity.SellerOrderDetailActivity;
import com.webkul.mobikulmp.activity.SellerProductsListActivity;
import com.webkul.mobikulmp.activity.SellerProfileActivity;
import com.webkul.mobikulmp.activity.SellerProfileEditActivity;
import com.webkul.mobikulmp.activity.SellerTransactionsListActivity;
import com.webkul.mobikulmp.fragment.AskQuestionToAdminFragment;
import com.webkul.mobikulmp.fragment.ContactSellerFragment;
import com.webkul.mobikulmp.handler.SignInExtendedHandler;
import com.webkul.mobikulmp.handler.SignUpExtendedHandler;

/**
 * Created by shubham.agarwal on 16/3/17. @Webkul Software Pvt. Ltd
 */

public class MarketplaceApplication extends MobikulApplication {

    @Override
    public Class getHomePageClass() {
        return HomeExtendedActivity.class;
    }

    @Override
    public Class getMarketplaceLandingPageClass() {
        return MarketplaceLandingActivity.class;
    }

    @Override
    public Class getCatalogProductPageClass() {
        return CatalogProductExtendedActivity.class;
    }

    @Override
    public SignInHandler getSignInHandlerClass(Context context, SignInFragment signInFragment) {
        return new SignInExtendedHandler(context, signInFragment);
    }

    @Override
    public SignUpHandler getSignUpHandler(Context context, int isSocial, SignUpFragment signUpFragment, CreateAccountFormData createAccountFormData) {
        return new SignUpExtendedHandler(context, isSocial, signUpFragment, createAccountFormData);
    }

    @Override
    public void loadAskQuestionToAdminDialogFragment(Context context) {
        FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        AskQuestionToAdminFragment askQuestionToAdminFragment = AskQuestionToAdminFragment.newInstance();
        askQuestionToAdminFragment.show(fragmentManager, AskQuestionToAdminFragment.class.getSimpleName());
    }

    @Override
    public void callSellerDashBoardActivity(Context context) {
        context.startActivity(new Intent(context, SellerDashboardActivity.class));
    }

    @Override
    public void callSellerProfileActivity(Context context) {
        context.startActivity(new Intent(context, SellerProfileEditActivity.class));
    }

    @Override
    public void callSellerOrdersActivity(Context context) {
        context.startActivity(new Intent(context, SellerOrderActivity.class));
    }

    @Override
    public void callSellerNewProductActivity(Context context) {
        context.startActivity(new Intent(context, SellerAddProductActivity.class));
    }

    @Override
    public void callSellerProductsListActivity(Context context) {
        context.startActivity(new Intent(context, SellerProductsListActivity.class));
    }

    @Override
    public void callSellerTransactionsListActivity(Context context) {
        context.startActivity(new Intent(context, SellerTransactionsListActivity.class));
    }

    @Override
    public void callManagePrintPdfHeaderActivity(Context context) {
        context.startActivity(new Intent(context, ManagePrintPdfHeaderActivity.class));
    }

    @Override
    public void callSellerStatusActivity(Context context) {
        if (!MpAppSharedPref.isSeller(context) && !MpAppSharedPref.isSellerPending(context)) {
            context.startActivity(new Intent(context, BecomeSellerActivity.class));
        } else {
            if (MpAppSharedPref.isSeller(context) && MpAppSharedPref.isSellerPending(context)) {
                Toast.makeText(context, R.string.seller_status_pending_message, Toast.LENGTH_LONG).show();
            } else if (MpAppSharedPref.isSeller(context) && !MpAppSharedPref.isSellerPending(context)) {
                //do nothing
            }
        }
    }

    @Override
    public Class getSellerProfilePageClass() {
        return SellerProfileActivity.class;
    }

    public Class getContactUsFragmentClass() {
        return ContactSellerFragment.class;
    }

    public Fragment getContactUsFragmentNewInstance(int sellerId, int productId) {
        return ContactSellerFragment.newInstance(sellerId, productId);
    }

    @Override
    public void callChatRelatedActivity(Context mContext) {
        Intent intent = null;
        if (MpAppSharedPref.isAdmin(mContext)) {
            intent = new Intent(mContext, ChatWithSellersActivity.class);
        } else {
            intent = new Intent(mContext, ChatActivity.class);
            intent.putExtra("user_name", AppSharedPref.getCustomerName(mContext));
            intent.putExtra("user_id", String.valueOf(AppSharedPref.getCustomerId(mContext)));
        }

        if (intent != null) {
            mContext.startActivity(intent);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public Class getSellerOrderDetailsActivity() {
        return SellerOrderDetailActivity.class;
    }

    @Override
    public Class getSellerDashBoardActivity() {
        return SellerDashboardActivity.class;
    }
}
