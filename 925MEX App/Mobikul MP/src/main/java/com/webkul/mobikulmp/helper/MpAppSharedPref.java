package com.webkul.mobikulmp.helper;

import android.content.Context;

import com.webkul.mobikul.helper.AppSharedPref;

/**
 * Created by vedesh.kumar on 4/1/17. @Webkul Software Private limited
 */


//IN THIS FILE RETURN TYPE OF VARIOUS HAS BEEN TYPECASTED DUE TO CONFLICTS IN SOME DEVICES

public class MpAppSharedPref extends AppSharedPref {

    public static final String KEY_CUSTOMER_IS_SELLER = "isSeller";
    public static final String KEY_IS_SELLER_PENDING = "isPendingSeller";
    public static final String KEY_CUSTOMER_IS_ADMIN = "isAdmin";
    public static final String KEY_API_KEY = "apiKey";

    /*Seller*/

    public static boolean isSeller(Context context) {
        try {
            return getSharedPreference(context, CUSTOMER_PREF).getBoolean(KEY_CUSTOMER_IS_SELLER, false);
        } catch (ClassCastException e) {
            return Boolean.parseBoolean(getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CUSTOMER_IS_SELLER, String.valueOf(false)));
        }
    }

    public static void setIsSeller(Context context, boolean isSeller) {
        getSharedPreferenceEditor(context, CUSTOMER_PREF).putBoolean(KEY_CUSTOMER_IS_SELLER, isSeller).commit();
    }

    public static boolean isSellerPending(Context context) {
        try {

            return getSharedPreference(context, CUSTOMER_PREF).getBoolean(KEY_IS_SELLER_PENDING, false);
        } catch (ClassCastException e) {
            return Boolean.parseBoolean(getSharedPreference(context, CUSTOMER_PREF).getString(KEY_IS_SELLER_PENDING, String.valueOf(false)));
        }
    }

    public static void setIsSellerPending(Context context, boolean isPending) {
        getSharedPreferenceEditor(context, CUSTOMER_PREF).putBoolean(KEY_IS_SELLER_PENDING, isPending).commit();
    }

    public static boolean isAdmin(Context context) {

        try {
            return getSharedPreference(context, CUSTOMER_PREF).getBoolean(KEY_CUSTOMER_IS_ADMIN, false);
        } catch (ClassCastException e) {
            return Boolean.parseBoolean(getSharedPreference(context, CUSTOMER_PREF).getString(KEY_CUSTOMER_IS_ADMIN, String.valueOf(false)));
        }
    }

    public static void setIsAdmin(Context context, boolean isAdmin) {
        getSharedPreferenceEditor(context, CUSTOMER_PREF).putBoolean(KEY_CUSTOMER_IS_ADMIN, isAdmin).commit();
    }

    public static void setApiKey(Context context, String apiKey) {
        getSharedPreferenceEditor(context, CUSTOMER_PREF).putString(KEY_API_KEY, apiKey).commit();
    }

    public static String getApiKey(Context context) {
        return getSharedPreference(context, CUSTOMER_PREF).getString(KEY_API_KEY, "");
    }
}