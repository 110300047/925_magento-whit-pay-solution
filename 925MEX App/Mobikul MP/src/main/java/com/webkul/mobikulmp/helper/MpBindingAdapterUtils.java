package com.webkul.mobikulmp.helper;

import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.webkul.mobikul.helper.BindingAdapterUtils;
import com.webkul.mobikulmp.R;

import java.util.Locale;

/**
 * Created by vedesh.kumar on 10/8/17. @Webkul Software Private limited
 */

public class MpBindingAdapterUtils extends BindingAdapterUtils {

    @BindingAdapter("circleTextDrawable")
    public static void setCircleTextDrawable(ImageView view, String text) {
        TextDrawable textDrawable = TextDrawable.builder()
                .beginConfig()
                .textColor(Color.WHITE)
                .toUpperCase()
                .endConfig()
                .buildRoundRect(String.valueOf(text.charAt(0)).toUpperCase(Locale.getDefault()), view.getResources().getColor(com.webkul.mobikul.R.color.blue_a200), (int) view.getResources().getDimension(com.webkul.mobikul.R.dimen.material_drawer_item_profile_icon));
        view.setImageDrawable(textDrawable);
    }

    @BindingAdapter("drawableTint")
    public static void setDrawableTint(TextView textView, String ratingValue) {
        if (ratingValue != null && !ratingValue.isEmpty()) {
            Drawable drawable = textView.getContext().getResources().getDrawable(R.drawable.ic_star_wrapper);
            Double rating = Double.parseDouble(ratingValue);
            if (rating > 3) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), R.color.colorGood), PorterDuff.Mode.MULTIPLY));
            } else if (rating > 2) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), R.color.colorAvg), PorterDuff.Mode.MULTIPLY));
            } else {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), R.color.colorBad), PorterDuff.Mode.MULTIPLY));
            }

            if (MpAppSharedPref.getStoreCode(textView.getContext()).equals("ar")) {
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
            } else {
                textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
            }
        }
    }
}