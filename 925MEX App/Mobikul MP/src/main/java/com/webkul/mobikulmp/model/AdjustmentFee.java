package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 1/6/17.
 */

public class AdjustmentFee {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("unformattedValue")
    @Expose
    private Integer unformattedValue;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getUnformattedValue() {
        return unformattedValue;
    }

    public void setUnformattedValue(Integer unformattedValue) {
        this.unformattedValue = unformattedValue;
    }
}
