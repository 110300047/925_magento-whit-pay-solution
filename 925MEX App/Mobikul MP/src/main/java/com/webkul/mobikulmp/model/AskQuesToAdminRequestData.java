package com.webkul.mobikulmp.model;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.android.databinding.library.baseAdapters.BR;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.fragment.AskQuestionToAdminFragment;

/**
 * Created by shubham.agarwal on 21/3/17.
 */

public class AskQuesToAdminRequestData extends BaseObservable {
    @SuppressWarnings("unused")
    private static final String TAG = "ContactSellerRequestDat";

    private String subject;
    private String query;
    private Context mContext;
    private boolean displayError;

    public AskQuesToAdminRequestData(Context context) {
        mContext = context;
        subject = "";
        query = "";
    }

    @Bindable
    public boolean isDisplayError() {
        return displayError;
    }

    private void setDisplayError(boolean displayError) {
        this.displayError = displayError;
        notifyPropertyChanged(BR.displayError);
    }

    @Bindable({"subject"})
    public String getSubjectError() {
        if (getSubject().isEmpty()) {
            return mContext.getString(R.string.msg_this_is_a_required_field);
        }
        return "";
    }

    @Bindable
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
        notifyPropertyChanged(BR.subject);
    }

    @Bindable({"query"})
    public String getQueryError() {
        if (getQuery().isEmpty()) {
            return mContext.getString(R.string.msg_this_is_a_required_field);
        }
        return "";
    }

    @Bindable
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
        notifyPropertyChanged(BR.query);
    }

    public boolean isFormValidated() {
        setDisplayError(true); /*settting true if validation performed*/
        Fragment frag = ((AppCompatActivity) mContext).getSupportFragmentManager().findFragmentByTag(AskQuestionToAdminFragment.class.getSimpleName());
        if (frag != null && frag.isAdded()) {
            AskQuestionToAdminFragment askQuestionToAdminFragment = (AskQuestionToAdminFragment) frag;
            if (getSubject().isEmpty()) {
                askQuestionToAdminFragment.mBinding.subjectEt.requestFocus();
                return false;
            } else if (getQuery().isEmpty()) {
                askQuestionToAdminFragment.mBinding.queryEt.requestFocus();
                return false;
            }
            return true;
        }
        return false;
    }

    public void resetFormData() {
        setSubject("");
        setQuery("");
        setDisplayError(false);
    }


}
