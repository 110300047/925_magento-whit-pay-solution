package com.webkul.mobikulmp.model;

/**
 * Created by vedesh.kumar on 29/6/17.
 */

public class BecomePartnerData {

    private String storeUrl = "";

    private String storeName = "";

    public String getStoreUrl() {
        return storeUrl;
    }

    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
}
