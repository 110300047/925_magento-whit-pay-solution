package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 29/6/17.
 */

public class BecomePartnerResponseData extends BaseModel {

    @SerializedName("isPending")
    @Expose
    private boolean isPending;

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }
}
