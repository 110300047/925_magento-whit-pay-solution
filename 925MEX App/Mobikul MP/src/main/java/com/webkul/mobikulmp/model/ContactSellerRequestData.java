package com.webkul.mobikulmp.model;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.android.databinding.library.baseAdapters.BR;
import com.webkul.mobikul.helper.AppSharedPref;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.fragment.ContactSellerFragment;

import static com.webkul.mobikul.constants.MobikulConstant.EMAIL_PATTERN;

/**
 * Created by shubham.agarwal on 21/3/17.
 */

public class ContactSellerRequestData extends BaseObservable {
    @SuppressWarnings("unused")
    private static final String TAG = "ContactSellerRequestDat";

    private String nickname;
    private String email;
    private String subject;
    private String query;
    private Context mContext;
    private boolean displayError;

    public ContactSellerRequestData(Context context) {
        mContext = context;
        nickname = AppSharedPref.getCustomerName(context);
        email = AppSharedPref.getCustomerEmail(context);
        subject = "";
        query = "";
    }

    @Bindable
    public boolean isDisplayError() {
        return displayError;
    }

    private void setDisplayError(boolean displayError) {
        this.displayError = displayError;
        notifyPropertyChanged(BR.displayError);
    }

    @Bindable({"nickname"})
    public String getNicknameError() {
        if (getNickname().isEmpty()) {
            return mContext.getString(R.string.msg_this_is_a_required_field);
        }
        return "";
    }

    @Bindable
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
        notifyPropertyChanged(BR.nickname);
    }

    @Bindable({"email"})
    public String getEmailError() {
        if (getEmail().isEmpty()) {
            return mContext.getString(R.string.msg_this_is_a_required_field);
        }

        if (!EMAIL_PATTERN.matcher(getEmail()).matches()){
            return mContext.getResources().getString(R.string.enter_valid_email);
        }
        return "";
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable({"subject"})
    public String getSubjectError() {
        if (getSubject().isEmpty()) {
            return mContext.getString(R.string.msg_this_is_a_required_field);
        }
        return "";
    }

    @Bindable
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
        notifyPropertyChanged(BR.subject);
    }

    @Bindable({"query"})
    public String getQueryError() {
        if (getQuery().isEmpty()) {
            return mContext.getString(R.string.msg_this_is_a_required_field);
        }
        return "";
    }

    @Bindable
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
        notifyPropertyChanged(BR.query);
    }

    public boolean isFormValidated() {
        setDisplayError(true); /*settting true if validation performed*/
        Fragment frag = ((AppCompatActivity) mContext).getSupportFragmentManager().findFragmentByTag(ContactSellerFragment.class.getSimpleName());
        if (frag != null && frag.isAdded()) {
            ContactSellerFragment contactSellerFragment = (ContactSellerFragment) frag;
            if (getNickname().isEmpty()) {
                contactSellerFragment.mBinding.nicknameEt.requestFocus();
                return false;
            } else if (getEmail().isEmpty() || !(EMAIL_PATTERN.matcher(getEmail()).matches())) {
                contactSellerFragment.mBinding.emailEt.requestFocus();
                return false;
            } else if (getSubject().isEmpty()) {
                contactSellerFragment.mBinding.subjectEt.requestFocus();
                return false;
            } else if (getQuery().isEmpty()) {
                contactSellerFragment.mBinding.queryEt.requestFocus();
                return false;
            }
            return true;
        }
        return false;
    }

    public void resetFormData() {
        setNickname("");
        setEmail("");
        setSubject("");
        setQuery("");
        setDisplayError(false);
    }


}
