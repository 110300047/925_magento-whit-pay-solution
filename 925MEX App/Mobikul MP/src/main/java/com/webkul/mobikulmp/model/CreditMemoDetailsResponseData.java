package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/5/17.
 */

public class CreditMemoDetailsResponseData extends BaseModel{
    @SerializedName("mainHeading")
    @Expose
    private String mainHeading;
    @SerializedName("sendmailAction")
    @Expose
    private String sendmailAction;
    @SerializedName("sendmailWarning")
    @Expose
    private String sendmailWarning;
    @SerializedName("subHeading")
    @Expose
    private String subHeading;
    @SerializedName("orderData")
    @Expose
    private OrderData orderData;
    @SerializedName("buyerData")
    @Expose
    private InvoiceBuyerData buyerData;
    @SerializedName("shippingAddressData")
    @Expose
    private ShippingAddressData shippingAddressData;
    @SerializedName("shippingMethodData")
    @Expose
    private ShippingMethodData shippingMethodData;
    @SerializedName("billingAddressData")
    @Expose
    private BillingAddressData billingAddressData;
    @SerializedName("paymentMethodData")
    @Expose
    private PaymentMethodData paymentMethodData;
    @SerializedName("items")
    @Expose
    private List<OrderItemData> items = null;
    @SerializedName("subtotal")
    @Expose
    private InvoiceSubtotal subtotal;
    @SerializedName("shipping")
    @Expose
    private InvoiceShipping shipping;
    @SerializedName("tax")
    @Expose
    private InvoiceTax tax;
//    @SerializedName("cod")
//    @Expose
//    private InvoiceCod cod;
    @SerializedName("adjustmentRefund")
    @Expose
    private AdjustmentRefund adjustmentRefund;
    @SerializedName("adjustmentFee")
    @Expose
    private AdjustmentFee adjustmentFee;
    @SerializedName("grandTotal")
    @Expose
    private CreditMemoGrandTotal creditMemoGrandTotal;

    @SerializedName("canManageOrder")
    @Expose
    private boolean canManageOrder;

    public boolean isCanManageOrder() {
        return canManageOrder;
    }

    public void setCanManageOrder(boolean canManageOrder) {
        this.canManageOrder = canManageOrder;
    }

    public String getMainHeading() {
        return mainHeading;
    }

    public void setMainHeading(String mainHeading) {
        this.mainHeading = mainHeading;
    }

    public String getSendmailAction() {
        return sendmailAction;
    }

    public void setSendmailAction(String sendmailAction) {
        this.sendmailAction = sendmailAction;
    }

    public String getSendmailWarning() {
        return sendmailWarning;
    }

    public void setSendmailWarning(String sendmailWarning) {
        this.sendmailWarning = sendmailWarning;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }

    public OrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderData orderData) {
        this.orderData = orderData;
    }

    public InvoiceBuyerData getBuyerData() {
        return buyerData;
    }

    public void setBuyerData(InvoiceBuyerData buyerData) {
        this.buyerData = buyerData;
    }

    public ShippingAddressData getShippingAddressData() {
        return shippingAddressData;
    }

    public void setShippingAddressData(ShippingAddressData shippingAddressData) {
        this.shippingAddressData = shippingAddressData;
    }

    public ShippingMethodData getShippingMethodData() {
        return shippingMethodData;
    }

    public void setShippingMethodData(ShippingMethodData shippingMethodData) {
        this.shippingMethodData = shippingMethodData;
    }

    public BillingAddressData getBillingAddressData() {
        return billingAddressData;
    }

    public void setBillingAddressData(BillingAddressData billingAddressData) {
        this.billingAddressData = billingAddressData;
    }

    public PaymentMethodData getPaymentMethodData() {
        return paymentMethodData;
    }

    public void setPaymentMethodData(PaymentMethodData paymentMethodData) {
        this.paymentMethodData = paymentMethodData;
    }

    public List<OrderItemData> getItems() {
        return items;
    }

    public void setItems(List<OrderItemData> items) {
        this.items = items;
    }

    public InvoiceSubtotal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(InvoiceSubtotal subtotal) {
        this.subtotal = subtotal;
    }

    public InvoiceShipping getShipping() {
        return shipping;
    }

    public void setShipping(InvoiceShipping shipping) {
        this.shipping = shipping;
    }

    public InvoiceTax getTax() {
        return tax;
    }

    public void setTax(InvoiceTax tax) {
        this.tax = tax;
    }

    public AdjustmentRefund getAdjustmentRefund() {
        return adjustmentRefund;
    }

    public void setAdjustmentRefund(AdjustmentRefund adjustmentRefund) {
        this.adjustmentRefund = adjustmentRefund;
    }

    public AdjustmentFee getAdjustmentFee() {
        return adjustmentFee;
    }

    public void setAdjustmentFee(AdjustmentFee adjustmentFee) {
        this.adjustmentFee = adjustmentFee;
    }

    public CreditMemoGrandTotal getCreditMemoGrandTotal() {
        return creditMemoGrandTotal;
    }

    public void setCreditMemoGrandTotal(CreditMemoGrandTotal creditMemoGrandTotal) {
        this.creditMemoGrandTotal = creditMemoGrandTotal;
    }
}
