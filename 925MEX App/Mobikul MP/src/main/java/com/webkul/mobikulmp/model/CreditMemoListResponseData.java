package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/5/17.
 */

public class CreditMemoListResponseData extends BaseModel{

    @SerializedName("mainHeading")
    @Expose
    private String mainHeading;
    @SerializedName("labels")
    @Expose
    private CreditMemoLabels labels;
    @SerializedName("creditMemoList")
    @Expose
    private List<ItemCreditMemo> creditMemoList;

    public String getMainHeading() {
        return mainHeading;
    }

    public void setMainHeading(String mainHeading) {
        this.mainHeading = mainHeading;
    }

    public CreditMemoLabels getLabels() {
        return labels;
    }

    public void setLabels(CreditMemoLabels labels) {
        this.labels = labels;
    }

    public List<ItemCreditMemo> getCreditMemoList() {
        return creditMemoList;
    }

    public void setCreditMemoList(List<ItemCreditMemo> creditMemoList) {
        this.creditMemoList = creditMemoList;
    }
}
