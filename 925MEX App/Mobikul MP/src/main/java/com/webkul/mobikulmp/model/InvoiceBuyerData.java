package com.webkul.mobikulmp.model;

/**
 * Created by vedesh.kumar on 29/5/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceBuyerData {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("nameLabel")
    @Expose
    private String nameLabel;
    @SerializedName("nameValue")
    @Expose
    private String nameValue;
    @SerializedName("emailLabel")
    @Expose
    private String emailLabel;
    @SerializedName("emailValue")
    @Expose
    private String emailValue;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNameLabel() {
        return nameLabel;
    }

    public void setNameLabel(String nameLabel) {
        this.nameLabel = nameLabel;
    }

    public String getNameValue() {
        return nameValue;
    }

    public void setNameValue(String nameValue) {
        this.nameValue = nameValue;
    }

    public String getEmailLabel() {
        return emailLabel;
    }

    public void setEmailLabel(String emailLabel) {
        this.emailLabel = emailLabel;
    }

    public String getEmailValue() {
        return emailValue;
    }

    public void setEmailValue(String emailValue) {
        this.emailValue = emailValue;
    }

}
