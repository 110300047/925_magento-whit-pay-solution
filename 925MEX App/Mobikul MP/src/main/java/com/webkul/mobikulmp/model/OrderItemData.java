package com.webkul.mobikulmp.model;

/**
 * Created by vedesh.kumar on 25/5/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.customer.order.Qty;

public class OrderItemData {

    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("qty")
    @Expose
    private Qty qty;
    @SerializedName("subTotal")
    @Expose
    private String subTotal;
    @SerializedName("adminComission")
    @Expose
    private String adminComission;
    @SerializedName("vendorTotal")
    @Expose
    private String vendorTotal;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Qty getQty() {
        return qty;
    }

    public void setQty(Qty qty) {
        this.qty = qty;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getAdminComission() {
        return adminComission;
    }

    public void setAdminComission(String adminComission) {
        this.adminComission = adminComission;
    }

    public String getVendorTotal() {
        return vendorTotal;
    }

    public void setVendorTotal(String vendorTotal) {
        this.vendorTotal = vendorTotal;
    }

}
