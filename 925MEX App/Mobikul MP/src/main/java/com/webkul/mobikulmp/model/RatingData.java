package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */
public class RatingData {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private float value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
