package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shubham.agarwal on 14/2/17. @Webkul Software Pvt. Ltd
 */
public class SellerListData {
    @SerializedName("logo")
    @Expose
    private String sellerIcon;
    @SerializedName("shoptitle")
    @Expose
    private String shopTitle;
    @SerializedName("productCount")
    @Expose
    private String sellerProductCount;
    @SerializedName("sellerId")
    @Expose
    private int sellerId;

    public String getSellerIcon() {
        return sellerIcon;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public String getSellerProductCount() {
        return sellerProductCount;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }
}
