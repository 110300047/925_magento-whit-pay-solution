package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham.agarwal on 14/2/17. @Webkul Software Pvt. Ltd
 */
public class SellerListResponseData extends BaseModel {
    @SerializedName("displayBanner")
    @Expose
    private boolean showBanner = false;
    @SerializedName("bannerImage")
    @Expose
    private String banner;
    @SerializedName("bannerContent")
    @Expose
    private String bannerContent = "";
    @SerializedName("buttonNHeadingLabel")
    @Expose
    private String buttonLabel = "";
    @SerializedName("topLabel")
    @Expose
    private String topLabel = "";
    @SerializedName("bottomLabel")
    @Expose
    private String bottomLabel = "";
    @SerializedName("sellersData")
    @Expose
    private List<SellerListData> sellerList = new ArrayList<>();

    public boolean isShowBanner() {
        return showBanner;
    }

    public void setShowBanner(boolean showBanner) {
        this.showBanner = showBanner;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getBannerContent() {
        return bannerContent;
    }

    public String getButtonLabel() {
        return buttonLabel;
    }

    public String getTopLabel() {
        return topLabel;
    }

    public String getBottomLabel() {
        return bottomLabel;
    }

    public List<SellerListData> getSellerList() {
        return sellerList;
    }
}
