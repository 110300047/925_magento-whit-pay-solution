package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 13/6/17.
 */

public class SellerLocationData {
    @SerializedName("countryName")
    @Expose
    public String countryName = "";

    @SerializedName("location")
    @Expose
    public String location ="";

    public String getCountryName() {
        return countryName;
    }

    public String getLocation() {
        if (location.equalsIgnoreCase("false")){
            location = "";
        }
        return location;
    }
}
