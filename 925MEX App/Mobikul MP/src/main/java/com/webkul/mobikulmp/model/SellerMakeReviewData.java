package com.webkul.mobikulmp.model;

import android.app.Activity;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;

import com.webkul.mobikul.helper.SnackbarHelper;
import com.webkul.mobikulmp.BR;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.fragment.SellerMakeReviewFragment;


/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */

public class SellerMakeReviewData extends BaseObservable {
    private boolean displayError;

    private String nickName = "";
    private String summary = "";
    private String comment = "";

    private int priceRating = 1;
    private int valueRating = 1;
    private int quantityRating = 1;

    private boolean nickNameValidated;
    private boolean summaryValidated;
    private boolean commentValidated;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        setNickNameValidated(!nickName.isEmpty());
        this.nickName = nickName;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        setSummaryValidated(!summary.isEmpty());
        this.summary = summary;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        setCommentValidated(!comment.isEmpty());
        this.comment = comment;
    }

    @Bindable
    public boolean isDisplayError() {
        return displayError;
    }

    public void setDisplayError(boolean displayError) {
        this.displayError = displayError;
        notifyPropertyChanged(BR.displayError);
    }

    public boolean isFormValidated(Context context) {
        SellerMakeReviewFragment sellerMakeReviewFragment = (SellerMakeReviewFragment) ((AppCompatActivity) context).getSupportFragmentManager()
                .findFragmentByTag(SellerMakeReviewFragment.class.getSimpleName());
        if (getPriceRating() == 0 || getValueRating() == 0 || getQuantityRating() == 0) {
            SnackbarHelper.getSnackbar((Activity) context, R.string.alert_please_select_one_of_each_rating).show();
            return false;
        } else if (!isNickNameValidated()) {
            sellerMakeReviewFragment.mBinding.nicknameTil.requestFocus();
            return false;
        } else if (!isSummaryValidated()) {
            sellerMakeReviewFragment.mBinding.summaryTil.requestFocus();
            return false;
        } else if (!isCommentValidated()) {
            sellerMakeReviewFragment.mBinding.commentTil.requestFocus();
            return false;
        }
        return true;
    }

    public int getPriceRating() {
        return priceRating;
    }

    public void setPriceRating(int priceRating) {
        this.priceRating = priceRating;
    }

    public int getValueRating() {
        return valueRating;
    }

    public void setValueRating(int valueRating) {
        this.valueRating = valueRating;
    }

    public int getQuantityRating() {
        return quantityRating;
    }

    @Bindable
    public boolean isNickNameValidated() {
        return nickNameValidated;
    }

    private void setNickNameValidated(boolean nickNameValidated) {
        this.nickNameValidated = nickNameValidated;
        notifyPropertyChanged(com.webkul.mobikulmp.BR.nickNameValidated);
    }

    @Bindable
    public boolean isSummaryValidated() {
        return summaryValidated;
    }

    private void setSummaryValidated(boolean summaryValidated) {
        this.summaryValidated = summaryValidated;
        notifyPropertyChanged(com.webkul.mobikulmp.BR.summaryValidated);
    }

    @Bindable
    public boolean isCommentValidated() {
        return commentValidated;
    }

    private void setCommentValidated(boolean commentValidated) {
        this.commentValidated = commentValidated;
        notifyPropertyChanged(BR.commentValidated);
    }

    public void setQuantityRating(int quantityRating) {
        this.quantityRating = quantityRating;
    }
}
