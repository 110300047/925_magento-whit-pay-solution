package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shubham.agarwal on 23/3/17.
 */

public class SellerOrderData {
    @SerializedName("label")
    @Expose
    public String label;
    @SerializedName("itemList")
    @Expose
    public List<SellerOrderProduct> itemList = null;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("summary")
    @Expose
    public String summary;
    @SerializedName("orderTotal")
    @Expose
    public String orderTotal;
    @SerializedName("incrementId")
    @Expose
    public String incrementId;
    @SerializedName("canReorder")
    @Expose
    public boolean canReorder;
    @SerializedName("state")
    @Expose
    public String state;

}