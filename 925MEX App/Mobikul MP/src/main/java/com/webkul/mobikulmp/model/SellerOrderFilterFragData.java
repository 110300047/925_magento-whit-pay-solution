package com.webkul.mobikulmp.model;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.webkul.mobikulmp.BR;
import com.webkul.mobikulmp.R;

/**
 * Created by shubham.agarwal on 24/3/17.
 */

public class SellerOrderFilterFragData extends BaseObservable implements Parcelable {
    public static final Creator<SellerOrderFilterFragData> CREATOR = new Creator<SellerOrderFilterFragData>() {
        @Override
        public SellerOrderFilterFragData createFromParcel(Parcel in) {
            return new SellerOrderFilterFragData(in);
        }

        @Override
        public SellerOrderFilterFragData[] newArray(int size) {
            return new SellerOrderFilterFragData[size];
        }
    };
    @SuppressWarnings("unused")
    private static final String TAG = "SellerOrderFilterFragDa";
    private Context mContext;
    private String orderId = "";
    private String fromDate = "";
    private String toDate = "";
    private int selectedOrderStatusPos;


    public SellerOrderFilterFragData(Context context) {
        mContext = context;
    }

    private SellerOrderFilterFragData(Parcel in) {
        orderId = in.readString();
        fromDate = in.readString();
        toDate = in.readString();
        selectedOrderStatusPos = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderId);
        dest.writeString(fromDate);
        dest.writeString(toDate);
        dest.writeInt(selectedOrderStatusPos);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Bindable
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
        notifyPropertyChanged(BR.orderId);
    }

    @Bindable
    public String getFromDate() {
        return fromDate;
    }


    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
        notifyPropertyChanged(BR.fromDate);
    }

    @Bindable
    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
        notifyPropertyChanged(BR.toDate);
    }

    @Bindable({"selectedOrderStatusPos"})
    public String getOrderStatus() {
        return mContext.getResources().getStringArray(R.array.order_status_code)[getSelectedOrderStatusPos()];
    }

    @Bindable
    public int getSelectedOrderStatusPos() {
        return selectedOrderStatusPos;
    }

    public void setSelectedOrderStatusPos(int selectedOrderStatusPos) {
        this.selectedOrderStatusPos = selectedOrderStatusPos;
        notifyPropertyChanged(BR.selectedOrderStatusPos);
    }

    public void resetFilters() {
        setOrderId("");
        setFromDate("");
        setToDate("");
        setSelectedOrderStatusPos(0);
    }
}
