package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vedesh.kumar on 25/5/17.
 */

public class SellerOrderLinks {


    @SerializedName("creditmemo")
    @Expose
    private OrderCreditMemo creditmemo;
    @SerializedName("shipment")
    @Expose
    private OrderShipment shipment;
    @SerializedName("invoice")
    @Expose
    private OrderInvoice invoice;

    public OrderInvoice getInvoice() {
        return invoice;
    }

    public void setInvoice(OrderInvoice invoice) {
        this.invoice = invoice;
    }


    public OrderCreditMemo getCreditmemo() {
        return creditmemo;
    }

    public void setCreditmemo(OrderCreditMemo creditmemo) {
        this.creditmemo = creditmemo;
    }

    public OrderShipment getShipment() {
        return shipment;
    }

    public void setShipment(OrderShipment shipment) {
        this.shipment = shipment;
    }
}
