package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by shubham.agarwal on 23/3/17.
 */


public class SellerOrderListResponseData extends BaseModel {

    @SerializedName("orderList")
    @Expose
    public List<SellerOrderData> orderList = null;
    @SerializedName("totalCount")
    @Expose
    public int totalCount;

    public boolean lazyLoading;

    public void setLazyLoading(boolean lazyLoading) {
        this.lazyLoading = lazyLoading;
    }
}