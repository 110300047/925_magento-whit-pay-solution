package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shubham.agarwal on 23/3/17.
 */

public class SellerOrderProduct {
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("qty")
    @Expose
    public String qty;
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("type")
    @Expose
    public String type;
}
