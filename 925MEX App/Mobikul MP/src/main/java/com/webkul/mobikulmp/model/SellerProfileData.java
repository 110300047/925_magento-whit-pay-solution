package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.ProductData;
import com.webkul.mobikulmp.model.seller.SellerReviewList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham.agarwal on 20/1/17. @Webkul Software Pvt. Ltd
 */

public class SellerProfileData extends BaseModel {

    @SerializedName("shopUrl")
    @Expose
    private String shopUrl;
    @SerializedName("vimeoId")
    @Expose
    private String vimeoId;
    @SerializedName("sellerId")
    @Expose
    private int sellerId;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("shopTitle")
    @Expose
    private String shopTitle;
    @SerializedName("twitterId")
    @Expose
    private String twitterId;
    @SerializedName("youtubeId")
    @Expose
    private String youtubeId;
    @SerializedName("facebookId")
    @Expose
    private String facebookId;
    @SerializedName("orderCount")
    @Expose
    private int orderCount;
    @SerializedName("price5Star")
    @Expose
    private int price5Star;
    @SerializedName("price4Star")
    @Expose
    private int price4Star;
    @SerializedName("price3Star")
    @Expose
    private int price3Star;
    @SerializedName("price2Star")
    @Expose
    private int price2Star;
    @SerializedName("price1Star")
    @Expose
    private int price1Star;
    @SerializedName("value5Star")
    @Expose
    private int value5Star;
    @SerializedName("value4Star")
    @Expose
    private int value4Star;
    @SerializedName("value3Star")
    @Expose
    private int value3Star;
    @SerializedName("value2Star")
    @Expose
    private int value2Star;
    @SerializedName("value1Star")
    @Expose
    private int value1Star;
    @SerializedName("reviewList")
    @Expose
    private List<SellerReviewList> reviewList = null;
    @SerializedName("pinterestId")
    @Expose
    private String pinterestId;
    @SerializedName("bannerImage")
    @Expose
    private String bannerImage;
    @SerializedName("instagramId")
    @Expose
    private String instagramId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("productCount")
    @Expose
    private int productCount;
    @SerializedName("googleplusId")
    @Expose
    private String googleplusId;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("returnPolicy")
    @Expose
    private String returnPolicy;
    @SerializedName("quality5Star")
    @Expose
    private int quality5Star;
    @SerializedName("quality4Star")
    @Expose
    private int quality4Star;
    @SerializedName("quality3Star")
    @Expose
    private int quality3Star;
    @SerializedName("quality2Star")
    @Expose
    private int quality2Star;
    @SerializedName("quality1Star")
    @Expose
    private int quality1Star;
    @SerializedName("isVimeoActive")
    @Expose
    private boolean isVimeoActive;
    @SerializedName("averageRating")
    @Expose
    private double averageRating;
    @SerializedName("feedbackCount")
    @Expose
    private int feedbackCount;
    @SerializedName("shippingPolicy")
    @Expose
    private String shippingPolicy;
    @SerializedName("isYoutubeActive")
    @Expose
    private boolean isYoutubeActive;
    @SerializedName("backgroundColor")
    @Expose
    private String backgroundColor;
    @SerializedName("isTwitterActive")
    @Expose
    private boolean isTwitterActive;
    @SerializedName("isFacebookActive")
    @Expose
    private boolean isFacebookActive;
    @SerializedName("isInstagramActive")
    @Expose
    private boolean isInstagramActive;
    @SerializedName("isPinterestActive")
    @Expose
    private boolean isPinterestActive;
    @SerializedName("averagePriceRating")
    @Expose
    private double averagePriceRating;
    @SerializedName("isgoogleplusActive")
    @Expose
    private boolean isgoogleplusActive;
    @SerializedName("averageValueRating")
    @Expose
    private double averageValueRating;
    @SerializedName("averageQualityRating")
    @Expose
    private double averageQualityRating;
    @SerializedName("recentProductList")
    @Expose
    private ArrayList<ProductData> recentProductList = null;

    public String getVimeoId() {
        return vimeoId;
    }

    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeId = youtubeId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public int getPrice5Star() {
        return price5Star;
    }

    public int getPrice5StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(price5Star)) / feedbackCount) * 100);
        }
    }

    public void setPrice5Star(int price5Star) {
        this.price5Star = price5Star;
    }

    public int getPrice4Star() {
        return price4Star;
    }

    public int getPrice4StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(price4Star)) / feedbackCount) * 100);
        }
    }

    public void setPrice4Star(int price4Star) {
        this.price4Star = price4Star;
    }

    public int getPrice3Star() {
        return price3Star;
    }

    public int getPrice3StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(price3Star)) / feedbackCount) * 100);
        }
    }

    public void setPrice3Star(int price3Star) {
        this.price3Star = price3Star;
    }

    public int getPrice2Star() {
        return price2Star;
    }

    public int getPrice2StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(price2Star)) / feedbackCount) * 100);
        }
    }

    public void setPrice2Star(int price2Star) {
        this.price2Star = price2Star;
    }

    public int getPrice1Star() {
        return price1Star;
    }

    public int getPrice1StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(price1Star)) / feedbackCount) * 100);
        }
    }

    public void setPrice1Star(int price1Star) {
        this.price1Star = price1Star;
    }

    public int getValue5Star() {
        return value5Star;
    }

    public int getValue5StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(value5Star)) / feedbackCount) * 100);
        }
    }

    public void setValue5Star(int value5Star) {
        this.value5Star = value5Star;
    }

    public int getValue4Star() {
        return value4Star;
    }

    public int getValue4StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(value4Star)) / feedbackCount) * 100);
        }
    }

    public void setValue4Star(int value4Star) {
        this.value4Star = value4Star;
    }

    public int getValue3Star() {
        return value3Star;
    }

    public int getValue3StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(value3Star)) / feedbackCount) * 100);
        }
    }

    public void setValue3Star(int value3Star) {
        this.value3Star = value3Star;
    }

    public int getValue2Star() {
        return value2Star;
    }

    public int getValue2StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(value2Star)) / feedbackCount) * 100);
        }
    }

    public void setValue2Star(int value2Star) {
        this.value2Star = value2Star;
    }

    public int getValue1Star() {
        return value1Star;
    }

    public int getValue1StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(value1Star)) / feedbackCount) * 100);
        }
    }

    public void setValue1Star(int value1Star) {
        this.value1Star = value1Star;
    }

    public List<SellerReviewList> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<SellerReviewList> reviewList) {
        this.reviewList = reviewList;
    }

    public String getPinterestId() {
        return pinterestId;
    }

    public void setPinterestId(String pinterestId) {
        this.pinterestId = pinterestId;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getInstagramId() {
        return instagramId;
    }

    public void setInstagramId(String instagramId) {
        this.instagramId = instagramId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProductCount() {
        return productCount;
    }

    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

    public String getGoogleplusId() {
        return googleplusId;
    }

    public void setGoogleplusId(String googleplusId) {
        this.googleplusId = googleplusId;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getReturnPolicy() {
        return returnPolicy;
    }

    public void setReturnPolicy(String returnPolicy) {
        this.returnPolicy = returnPolicy;
    }

    public int getQuality5Star() {
        return quality5Star;
    }

    public int getQuality5StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(quality5Star)) / feedbackCount) * 100);
        }
    }

    public void setQuality5Star(int quality5Star) {
        this.quality5Star = quality5Star;
    }

    public int getQuality4Star() {
        return quality4Star;
    }

    public int getQuality4StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(quality4Star)) / feedbackCount) * 100);
        }
    }

    public void setQuality4Star(int quality4Star) {
        this.quality4Star = quality4Star;
    }

    public int getQuality3Star() {
        return quality3Star;
    }

    public int getQuality3StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(quality3Star)) / feedbackCount) * 100);
        }
    }

    public void setQuality3Star(int quality3Star) {
        this.quality3Star = quality3Star;
    }

    public int getQuality2Star() {
        return quality2Star;
    }

    public int getQuality2StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(quality2Star)) / feedbackCount) * 100);
        }
    }

    public void setQuality2Star(int quality2Star) {
        this.quality2Star = quality2Star;
    }

    public int getQuality1Star() {
        return quality1Star;
    }

    public int getQuality1StarPercent() {
        if (feedbackCount == 0) {
            return 0;
        } else {
            return (int) ((Double.parseDouble(String.valueOf(quality1Star)) / feedbackCount) * 100);
        }
    }

    public void setQuality1Star(int quality1Star) {
        this.quality1Star = quality1Star;
    }

    public boolean isIsVimeoActive() {
        return isVimeoActive;
    }

    public void setIsVimeoActive(boolean isVimeoActive) {
        this.isVimeoActive = isVimeoActive;
    }

    public String getAverageRating() {
        return String.valueOf(averageRating);
    }

    public double getAverageRatingInt() {
        return averageRating;
    }

    public String getAverageRatingPercent() {
        return String.valueOf((int) (averageRating / (0.05)));
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public String getFeedbackCount() {
        return String.valueOf(feedbackCount);
    }

    public void setFeedbackCount(int feedbackCount) {
        this.feedbackCount = feedbackCount;
    }

    public String getShippingPolicy() {
        return shippingPolicy;
    }

    public void setShippingPolicy(String shippingPolicy) {
        this.shippingPolicy = shippingPolicy;
    }

    public boolean isIsYoutubeActive() {
        return isYoutubeActive;
    }

    public void setIsYoutubeActive(boolean isYoutubeActive) {
        this.isYoutubeActive = isYoutubeActive;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public boolean isIsTwitterActive() {
        return isTwitterActive;
    }

    public void setIsTwitterActive(boolean isTwitterActive) {
        this.isTwitterActive = isTwitterActive;
    }

    public boolean isIsFacebookActive() {
        return isFacebookActive;
    }

    public void setIsFacebookActive(boolean isFacebookActive) {
        this.isFacebookActive = isFacebookActive;
    }

    public boolean isIsInstagramActive() {
        return isInstagramActive;
    }

    public void setIsInstagramActive(boolean isInstagramActive) {
        this.isInstagramActive = isInstagramActive;
    }

    public boolean isIsPinterestActive() {
        return isPinterestActive;
    }

    public void setIsPinterestActive(boolean isPinterestActive) {
        this.isPinterestActive = isPinterestActive;
    }

    public String getAveragePriceRating() {
        return String.valueOf(averagePriceRating);
    }

    public void setAveragePriceRating(double averagePriceRating) {
        this.averagePriceRating = averagePriceRating;
    }

    public boolean isIsgoogleplusActive() {
        return isgoogleplusActive;
    }

    public void setIsgoogleplusActive(boolean isgoogleplusActive) {
        this.isgoogleplusActive = isgoogleplusActive;
    }

    public String getAverageValueRating() {
        return String.valueOf(averageValueRating);
    }

    public void setAverageValueRating(double averageValueRating) {
        this.averageValueRating = averageValueRating;
    }

    public String getAverageQualityRating() {
        return String.valueOf(averageQualityRating);
    }

    public void setAverageQualityRating(double averageQualityRating) {
        this.averageQualityRating = averageQualityRating;
    }

    public ArrayList<ProductData> getRecentProductList() {
        return recentProductList;
    }

    public void setRecentProductList(ArrayList<ProductData> recentProductList) {
        this.recentProductList = recentProductList;
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl;
    }
}