package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */
public class SellerReviewData {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("rating")
    @Expose
    private List<RatingData> rating = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<RatingData> getRating() {
        return rating;
    }

    public void setRating(List<RatingData> rating) {
        this.rating = rating;
    }

}
