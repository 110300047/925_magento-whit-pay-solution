package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.model.seller.SellerReviewList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham.agarwal on 16/2/17. @Webkul Software Pvt. Ltd
 */
public class SellerReviewListData extends BaseModel {

    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("reviewList")
    @Expose
    private List<SellerReviewList> reviewList = null;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<SellerReviewList> getReviewList() {
        if (reviewList == null) {
            setReviewList(new ArrayList<SellerReviewList>());
        }
        return reviewList;
    }

    public void setReviewList(List<SellerReviewList> reviewList) {
        this.reviewList = reviewList;
    }
}
