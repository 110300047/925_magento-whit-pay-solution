package com.webkul.mobikulmp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/5/17.
 */

public class ShipmentDetailsResponse extends BaseModel {

    @SerializedName("mainHeading")
    @Expose
    private String mainHeading;
    @SerializedName("subHeading")
    @Expose
    private String subHeading;
    @SerializedName("orderData")
    @Expose
    private OrderData orderData;
    @SerializedName("buyerData")
    @Expose
    private InvoiceBuyerData buyerData;
    @SerializedName("shippingAddressData")
    @Expose
    private ShippingAddressData shippingAddressData;
    @SerializedName("shippingMethodData")
    @Expose
    private ShippingMethodData shippingMethodData;
    @SerializedName("billingAddressData")
    @Expose
    private BillingAddressData billingAddressData;
    @SerializedName("paymentMethodData")
    @Expose
    private PaymentMethodData paymentMethodData;
    @SerializedName("items")
    @Expose
    private List<OrderItemData> items = null;
    @SerializedName("subtotal")
    @Expose
    private InvoiceSubtotal subtotal;
    @SerializedName("shipping")
    @Expose
    private InvoiceShipping shipping;
    @SerializedName("tax")
    @Expose
    private InvoiceTax tax;
    @SerializedName("cod")
    @Expose
    private InvoiceCod cod;
    @SerializedName("totalOrderedAmount")
    @Expose
    private InvoiceTotalOrderedAmount totalOrderedAmount;
    @SerializedName("totalVendorAmount")
    @Expose
    private InvoiceTotalVendorAmount totalVendorAmount;
    @SerializedName("totalAdminComission")
    @Expose
    private InvoiceTotalAdminComission totalAdminComission;

    public String getMainHeading() {
        return mainHeading;
    }

    public void setMainHeading(String mainHeading) {
        this.mainHeading = mainHeading;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }

    public OrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderData orderData) {
        this.orderData = orderData;
    }

    public InvoiceBuyerData getBuyerData() {
        return buyerData;
    }

    public void setBuyerData(InvoiceBuyerData buyerData) {
        this.buyerData = buyerData;
    }

    public ShippingAddressData getShippingAddressData() {
        return shippingAddressData;
    }

    public void setShippingAddressData(ShippingAddressData shippingAddressData) {
        this.shippingAddressData = shippingAddressData;
    }

    public ShippingMethodData getShippingMethodData() {
        return shippingMethodData;
    }

    public void setShippingMethodData(ShippingMethodData shippingMethodData) {
        this.shippingMethodData = shippingMethodData;
    }

    public BillingAddressData getBillingAddressData() {
        return billingAddressData;
    }

    public void setBillingAddressData(BillingAddressData billingAddressData) {
        this.billingAddressData = billingAddressData;
    }

    public PaymentMethodData getPaymentMethodData() {
        return paymentMethodData;
    }

    public void setPaymentMethodData(PaymentMethodData paymentMethodData) {
        this.paymentMethodData = paymentMethodData;
    }

    public List<OrderItemData> getItems() {
        return items;
    }

    public void setItems(List<OrderItemData> items) {
        this.items = items;
    }

    public InvoiceSubtotal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(InvoiceSubtotal subtotal) {
        this.subtotal = subtotal;
    }

    public InvoiceShipping getShipping() {
        return shipping;
    }

    public void setShipping(InvoiceShipping shipping) {
        this.shipping = shipping;
    }

    public InvoiceTax getTax() {
        return tax;
    }

    public void setTax(InvoiceTax tax) {
        this.tax = tax;
    }

    public InvoiceCod getCod() {
        return cod;
    }

    public void setCod(InvoiceCod cod) {
        this.cod = cod;
    }

    public InvoiceTotalOrderedAmount getTotalOrderedAmount() {
        return totalOrderedAmount;
    }

    public void setTotalOrderedAmount(InvoiceTotalOrderedAmount totalOrderedAmount) {
        this.totalOrderedAmount = totalOrderedAmount;
    }

    public InvoiceTotalVendorAmount getTotalVendorAmount() {
        return totalVendorAmount;
    }

    public void setTotalVendorAmount(InvoiceTotalVendorAmount totalVendorAmount) {
        this.totalVendorAmount = totalVendorAmount;
    }

    public InvoiceTotalAdminComission getTotalAdminComission() {
        return totalAdminComission;
    }

    public void setTotalAdminComission(InvoiceTotalAdminComission totalAdminComission) {
        this.totalAdminComission = totalAdminComission;
    }
}
