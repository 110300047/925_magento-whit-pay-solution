package com.webkul.mobikulmp.model;

/**
 * Created by vedesh.kumar on 25/5/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingMethodData {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("method")
    @Expose
    private String method;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

}
