package com.webkul.mobikulmp.model.chat;

/**
 * Created by vedesh.kumar on 30/7/17.
 */

public class ChatMessage {

    private String msg;
    private String sender;
    private String displayDay;
    private String displayTime;
    private String displayDate;


//    public ChatMessage(String msg, String sender) {
//        this.msg = msg;
//        this.sender = sender;
//    }

    public ChatMessage(String msg, String sender, String displayDay, String displayTime, String displayDate) {
        this.msg = msg;
        this.sender = sender;
        this.displayDay = displayDay;
        this.displayTime = displayTime;
        this.displayDate = displayDate;
    }

    public String getDisplayDay() {
        return displayDay;
    }

    public void setDisplayDay(String displayDay) {
        this.displayDay = displayDay;
    }

    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
