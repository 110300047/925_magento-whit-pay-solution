package com.webkul.mobikulmp.model.chat;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.webkul.mobikulmp.databinding.ChatMessageBinding;

/**
 * Created by vedesh.kumar on 30/7/17.
 */

public class ChatMessageViewHolder extends RecyclerView.ViewHolder {
    public ChatMessageBinding mBinding;

    public ChatMessageViewHolder(View itemView) {
        super(itemView);
        mBinding = DataBindingUtil.bind(itemView);
    }
}
