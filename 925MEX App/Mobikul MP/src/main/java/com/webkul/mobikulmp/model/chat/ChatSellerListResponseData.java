package com.webkul.mobikulmp.model.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

/**
 * Created by vedesh.kumar on 30/7/17.
 */

public class ChatSellerListResponseData extends BaseModel {
    @SerializedName("sellerList")
    @Expose
    private List<ChatSellerData> sellerList = null;
    @SerializedName("apiKey")
    @Expose
    private String apiKey;

    public List<ChatSellerData> getSellerList() {
        return sellerList;
    }

    public void setSellerList(List<ChatSellerData> sellerList) {
        this.sellerList = sellerList;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
