package com.webkul.mobikulmp.model.chat;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.webkul.mobikulmp.databinding.ChatSellerDetailsBinding;

/**
 * Created by vedesh.kumar on 30/7/17.
 */

public class ChatSellerViewHolder extends RecyclerView.ViewHolder {
    public ChatSellerDetailsBinding mBinding;

    public ChatSellerViewHolder(View itemView) {
        super(itemView);
        mBinding = DataBindingUtil.bind(itemView);
    }
}
