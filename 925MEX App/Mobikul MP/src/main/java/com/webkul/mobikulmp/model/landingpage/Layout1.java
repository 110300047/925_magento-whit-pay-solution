
package com.webkul.mobikulmp.model.landingpage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Layout1 {

    @SerializedName("iconOne")
    @Expose
    private String iconOne;
    @SerializedName("iconTwo")
    @Expose
    private String iconTwo;
    @SerializedName("labelOne")
    @Expose
    private String labelOne;
    @SerializedName("labelTwo")
    @Expose
    private String labelTwo;
    @SerializedName("iconFour")
    @Expose
    private String iconFour;
    @SerializedName("iconThree")
    @Expose
    private String iconThree;
    @SerializedName("labelFour")
    @Expose
    private String labelFour;
    @SerializedName("firstLabel")
    @Expose
    private String firstLabel;
    @SerializedName("labelThree")
    @Expose
    private String labelThree;
    @SerializedName("thirdLabel")
    @Expose
    private String thirdLabel;
    @SerializedName("fourthLabel")
    @Expose
    private String fourthLabel;
    @SerializedName("bannerImage")
    @Expose
    private String bannerImage;
    @SerializedName("displayIcon")
    @Expose
    private boolean displayIcon;
    @SerializedName("showSellers")
    @Expose
    private boolean showSellers;
    @SerializedName("secondLabel")
    @Expose
    private String secondLabel;
    @SerializedName("sellersData")
    @Expose
    private List<SellersData> sellersData = null;
    @SerializedName("aboutContent")
    @Expose
    private String aboutContent;
    @SerializedName("displayBanner")
    @Expose
    private boolean displayBanner;
    @SerializedName("bannerContent")
    @Expose
    private String bannerContent;
    @SerializedName("buttonNHeadingLabel")
    @Expose
    private String buttonNHeadingLabel;

    public String getIconOne() {
        return iconOne;
    }

    public void setIconOne(String iconOne) {
        this.iconOne = iconOne;
    }

    public String getIconTwo() {
        return iconTwo;
    }

    public void setIconTwo(String iconTwo) {
        this.iconTwo = iconTwo;
    }

    public String getLabelOne() {
        return labelOne;
    }

    public void setLabelOne(String labelOne) {
        this.labelOne = labelOne;
    }

    public String getLabelTwo() {
        return labelTwo;
    }

    public void setLabelTwo(String labelTwo) {
        this.labelTwo = labelTwo;
    }

    public String getIconFour() {
        return iconFour;
    }

    public void setIconFour(String iconFour) {
        this.iconFour = iconFour;
    }

    public String getIconThree() {
        return iconThree;
    }

    public void setIconThree(String iconThree) {
        this.iconThree = iconThree;
    }

    public String getLabelFour() {
        return labelFour;
    }

    public void setLabelFour(String labelFour) {
        this.labelFour = labelFour;
    }

    public String getFirstLabel() {
        return firstLabel;
    }

    public void setFirstLabel(String firstLabel) {
        this.firstLabel = firstLabel;
    }

    public String getLabelThree() {
        return labelThree;
    }

    public void setLabelThree(String labelThree) {
        this.labelThree = labelThree;
    }

    public String getThirdLabel() {
        return thirdLabel;
    }

    public void setThirdLabel(String thirdLabel) {
        this.thirdLabel = thirdLabel;
    }

    public String getFourthLabel() {
        return fourthLabel;
    }

    public void setFourthLabel(String fourthLabel) {
        this.fourthLabel = fourthLabel;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public boolean isDisplayIcon() {
        return displayIcon;
    }

    public void setDisplayIcon(boolean displayIcon) {
        this.displayIcon = displayIcon;
    }

    public boolean isShowSellers() {
        return showSellers;
    }

    public void setShowSellers(boolean showSellers) {
        this.showSellers = showSellers;
    }

    public String getSecondLabel() {
        return secondLabel;
    }

    public void setSecondLabel(String secondLabel) {
        this.secondLabel = secondLabel;
    }

    public List<SellersData> getSellersData() {
        return sellersData;
    }

    public void setSellersData(List<SellersData> sellersData) {
        this.sellersData = sellersData;
    }

    public String getAboutContent() {
        return aboutContent;
    }

    public void setAboutContent(String aboutContent) {
        this.aboutContent = aboutContent;
    }

    public boolean isDisplayBanner() {
        return displayBanner;
    }

    public void setDisplayBanner(boolean displayBanner) {
        this.displayBanner = displayBanner;
    }

    public String getBannerContent() {
        return bannerContent;
    }

    public void setBannerContent(String bannerContent) {
        this.bannerContent = bannerContent;
    }

    public String getButtonNHeadingLabel() {
        return buttonNHeadingLabel;
    }

    public void setButtonNHeadingLabel(String buttonNHeadingLabel) {
        this.buttonNHeadingLabel = buttonNHeadingLabel;
    }

}
