
package com.webkul.mobikulmp.model.landingpage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Layout2 {

    @SerializedName("buttonLabel")
    @Expose
    private String buttonLabel;
    @SerializedName("bannerImage")
    @Expose
    private String bannerImage;
    @SerializedName("displayBanner")
    @Expose
    private boolean displayBanner;
    @SerializedName("bannerContent")
    @Expose
    private String bannerContent;

    public String getButtonLabel() {
        return buttonLabel;
    }

    public void setButtonLabel(String buttonLabel) {
        this.buttonLabel = buttonLabel;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public boolean isDisplayBanner() {
        return displayBanner;
    }

    public void setDisplayBanner(boolean displayBanner) {
        this.displayBanner = displayBanner;
    }

    public String getBannerContent() {
        return bannerContent;
    }

    public void setBannerContent(String bannerContent) {
        this.bannerContent = bannerContent;
    }

}
