
package com.webkul.mobikulmp.model.landingpage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Layout3 {

    @SerializedName("headingOne")
    @Expose
    private String headingOne;
    @SerializedName("headingTwo")
    @Expose
    private String headingTwo;
    @SerializedName("headingThree")
    @Expose
    private String headingThree;
    @SerializedName("iconOne")
    @Expose
    private String iconOne;
    @SerializedName("iconTwo")
    @Expose
    private String iconTwo;
    @SerializedName("labelOne")
    @Expose
    private String labelOne;
    @SerializedName("labelTwo")
    @Expose
    private String labelTwo;
    @SerializedName("iconFour")
    @Expose
    private String iconFour;
    @SerializedName("iconFive")
    @Expose
    private String iconFive;
    @SerializedName("iconThree")
    @Expose
    private String iconThree;
    @SerializedName("labelFour")
    @Expose
    private String labelFour;
    @SerializedName("labelFive")
    @Expose
    private String labelFive;
    @SerializedName("labelThree")
    @Expose
    private String labelThree;
    @SerializedName("bannerImage")
    @Expose
    private String bannerImage;
    @SerializedName("displayIcon")
    @Expose
    private boolean displayIcon;
    @SerializedName("displayBanner")
    @Expose
    private boolean displayBanner;
    @SerializedName("bannerContent")
    @Expose
    private String bannerContent;

    public String getIconOne() {
        return iconOne;
    }

    public void setIconOne(String iconOne) {
        this.iconOne = iconOne;
    }

    public String getIconTwo() {
        return iconTwo;
    }

    public void setIconTwo(String iconTwo) {
        this.iconTwo = iconTwo;
    }

    public String getLabelOne() {
        return labelOne;
    }

    public void setLabelOne(String labelOne) {
        this.labelOne = labelOne;
    }

    public String getLabelTwo() {
        return labelTwo;
    }

    public void setLabelTwo(String labelTwo) {
        this.labelTwo = labelTwo;
    }

    public String getIconFour() {
        return iconFour;
    }

    public void setIconFour(String iconFour) {
        this.iconFour = iconFour;
    }

    public String getIconFive() {
        return iconFive;
    }

    public void setIconFive(String iconFive) {
        this.iconFive = iconFive;
    }

    public String getIconThree() {
        return iconThree;
    }

    public void setIconThree(String iconThree) {
        this.iconThree = iconThree;
    }

    public String getLabelFour() {
        return labelFour;
    }

    public void setLabelFour(String labelFour) {
        this.labelFour = labelFour;
    }

    public String getLabelFive() {
        return labelFive;
    }

    public void setLabelFive(String labelFive) {
        this.labelFive = labelFive;
    }

    public String getLabelThree() {
        return labelThree;
    }

    public void setLabelThree(String labelThree) {
        this.labelThree = labelThree;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public boolean isDisplayIcon() {
        return displayIcon;
    }

    public void setDisplayIcon(boolean displayIcon) {
        this.displayIcon = displayIcon;
    }

    public boolean isDisplayBanner() {
        return displayBanner;
    }

    public void setDisplayBanner(boolean displayBanner) {
        this.displayBanner = displayBanner;
    }

    public String getBannerContent() {
        return bannerContent;
    }

    public void setBannerContent(String bannerContent) {
        this.bannerContent = bannerContent;
    }

    public String getHeadingOne() {
        return headingOne;
    }

    public void setHeadingOne(String headingOne) {
        this.headingOne = headingOne;
    }

    public String getHeadingTwo() {
        return headingTwo;
    }

    public void setHeadingTwo(String headingTwo) {
        this.headingTwo = headingTwo;
    }

    public String getHeadingThree() {
        return headingThree;
    }

    public void setHeadingThree(String headingThree) {
        this.headingThree = headingThree;
    }
}
