
package com.webkul.mobikulmp.model.landingpage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MarketplaceLandingPageData {

    @SerializedName("authKey")
    @Expose
    private String authKey;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("pageLayout")
    @Expose
    private String pageLayout;
    @SerializedName("layout1")
    @Expose
    private Layout1 layout1;
    @SerializedName("layout2")
    @Expose
    private Layout2 layout2;
    @SerializedName("layout3")
    @Expose
    private Layout3 layout3;
    @SerializedName("responseCode")
    @Expose
    private int responseCode;

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPageLayout() {
        return Integer.parseInt(pageLayout);
    }

    public void setPageLayout(String pageLayout) {
        this.pageLayout = pageLayout;
    }

    public Layout1 getLayout1() {
        return layout1;
    }

    public void setLayout1(Layout1 layout1) {
        this.layout1 = layout1;
    }

    public Layout2 getLayout2() {
        return layout2;
    }

    public void setLayout2(Layout2 layout2) {
        this.layout2 = layout2;
    }

    public Layout3 getLayout3() {
        return layout3;
    }

    public void setLayout3(Layout3 layout3) {
        this.layout3 = layout3;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

}
