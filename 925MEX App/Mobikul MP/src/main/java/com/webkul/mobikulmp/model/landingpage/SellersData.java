
package com.webkul.mobikulmp.model.landingpage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.catalog.ProductData;

import java.util.List;

public class SellersData {

    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("products")
    @Expose
    private List<ProductData> products = null;
    @SerializedName("sellerId")
    @Expose
    private int sellerId;
    @SerializedName("shoptitle")
    @Expose
    private String shopTitle;
    @SerializedName("productCount")
    @Expose
    private String productCount;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<ProductData> getProducts() {
        return products;
    }

    public void setProducts(List<ProductData> products) {
        this.products = products;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shoptitle) {
        this.shopTitle = shoptitle;
    }

    public String getProductCount() {
        return productCount;
    }

    public void setProductCount(String productCount) {
        this.productCount = productCount;
    }

}
