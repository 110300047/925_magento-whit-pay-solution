package com.webkul.mobikulmp.model.pdfheader;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 8/1/18. @Webkul Software Private limited
 */

public class PdfHeaderInfoDataResponse extends BaseModel {

    @SerializedName("headerInfo")
    @Expose
    public String headerInfo;

    public String getHeaderInfo() {
        return headerInfo;
    }

    public void setHeaderInfo(String headerInfo) {
        this.headerInfo = headerInfo;
    }
}
