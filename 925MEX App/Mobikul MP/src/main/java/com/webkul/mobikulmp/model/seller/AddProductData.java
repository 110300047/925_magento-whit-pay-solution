package com.webkul.mobikulmp.model.seller;

import android.databinding.Bindable;
import android.view.View;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.helper.Utils;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.BR;
import com.webkul.mobikulmp.R;
import com.webkul.mobikulmp.activity.SellerAddProductActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.webkul.mobikul.constants.ApplicationConstant.DEFAULT_WEBSITE_ID;

/**
 * Created by vedesh.kumar on 20/2/18. @Webkul Software Private limited
 */

public class AddProductData extends BaseModel {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("websiteIds")
    @Expose
    private List<String> websiteIds = null;
    @SerializedName("attributeSetId")
    @Expose
    private String attributeSetId;
    @SerializedName("categoryIds")
    @Expose
    private ArrayList<String> categoryIds = null;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("specialPrice")
    @Expose
    private String specialPrice;
    @SerializedName("specialFromDate")
    @Expose
    private String specialFromDate;
    @SerializedName("specialToDate")
    @Expose
    private String specialToDate;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("isInStock")
    @Expose
    private boolean isInStock;
    @SerializedName("visibility")
    @Expose
    private String visibility;
    @SerializedName("taxClassId")
    @Expose
    private String taxClassId;
    @SerializedName("productHasWeight")
    @Expose
    private int productHasWeight;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("metaTitle")
    @Expose
    private String metaTitle;
    @SerializedName("metaKeyword")
    @Expose
    private String metaKeyword;
    @SerializedName("metaDescription")
    @Expose
    private String metaDescription;
    @SerializedName("mpProductCartLimit")
    @Expose
    private String mpProductCartLimit;
    @SerializedName("baseImage")
    @Expose
    private String baseImage;
    @SerializedName("smallImage")
    @Expose
    private String smallImage;
    @SerializedName("swatchImage")
    @Expose
    private String swatchImage;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("medialGallery")
    @Expose
    private List<MedialGallery> medialGallery = null;
    @SerializedName("related")
    @Expose
    private ArrayList<String> related = null;
    @SerializedName("upsell")
    @Expose
    private ArrayList<String> upsell = null;
    @SerializedName("crossSell")
    @Expose
    private ArrayList<String> crossSell = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getWebsiteIds() {
        return websiteIds;
    }

    public void setWebsiteIds(List<String> websiteIds) {
        this.websiteIds = websiteIds;
    }

    public JSONArray getWebsiteJSONArray() {
        JSONArray websiteIdsJSONArray = new JSONArray();
        if (websiteIds != null) {
            for (int noOfWebsites = 0; noOfWebsites < websiteIds.size(); noOfWebsites++) {
                websiteIdsJSONArray.put(websiteIds.get(noOfWebsites));
            }
        }
//        if (websiteIdsJSONArray.length() == 0) {
//            websiteIdsJSONArray.put(String.valueOf(DEFAULT_WEBSITE_ID));
//        }
        return websiteIdsJSONArray;
    }

    public String getAttributeSetId() {
        return attributeSetId;
    }

    public void setAttributeSetId(String attributeSetId) {
        this.attributeSetId = attributeSetId;
    }

    public ArrayList<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(ArrayList<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public JSONArray getCategoryIdsJSONArray() {
        JSONArray categoryIdsJSONArray = new JSONArray();
        if (categoryIds != null) {
            for (int noOfCategories = 0; noOfCategories < categoryIds.size(); noOfCategories++) {
                categoryIdsJSONArray.put(categoryIds.get(noOfCategories));
            }
        }
        return categoryIdsJSONArray;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getSpecialFromDate() {
        return specialFromDate;
    }

    public void setSpecialFromDate(String specialFromDate) {
        this.specialFromDate = specialFromDate;
    }

    public String getSpecialFromDateForRequest() {
        try {
            Date date = new SimpleDateFormat("dd MMM, yyyy", Locale.US).parse(specialFromDate);
            return new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return specialFromDate;
    }

    public String getSpecialToDate() {
        return specialToDate;
    }

    public void setSpecialToDate(String specialToDate) {
        this.specialToDate = specialToDate;
    }

    public String getSpecialToDateForRequest() {
        try {
            Date date = new SimpleDateFormat("dd MMM, yyyy", Locale.US).parse(specialToDate);
            return new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return specialToDate;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public boolean isIsInStock() {
        return isInStock;
    }

    public void setIsInStock(boolean isInStock) {
        this.isInStock = isInStock;
    }

    public String getTaxClassId() {
        return taxClassId;
    }

    public void setTaxClassId(String taxClassId) {
        this.taxClassId = taxClassId;
    }

    @Bindable
    public boolean getProductHasWeight() {
        return productHasWeight == 1;
    }

    public void setProductHasWeight(boolean productHasWeight) {
        this.productHasWeight = productHasWeight ? 1 : 0;
        notifyPropertyChanged(BR.productHasWeight);
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaKeyword() {
        return metaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        this.metaKeyword = metaKeyword;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMpProductCartLimit() {
        return mpProductCartLimit;
    }

    public void setMpProductCartLimit(String mpProductCartLimit) {
        this.mpProductCartLimit = mpProductCartLimit;
    }

    public String getBaseImage() {
        return baseImage;
    }

    public void setBaseImage(String baseImage) {
        this.baseImage = baseImage;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getSwatchImage() {
        return swatchImage;
    }

    public void setSwatchImage(String swatchImage) {
        this.swatchImage = swatchImage;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<MedialGallery> getMedialGallery() {
        return medialGallery;
    }

    public void setMedialGallery(List<MedialGallery> medialGallery) {
        this.medialGallery = medialGallery;
    }

    public JSONArray getMedialGalleryJSONArray() {
        JSONArray mediaGallery = new JSONArray();
        try {
            for (int noOfImages = 0; noOfImages < medialGallery.size(); noOfImages++) {
                JSONObject imagesDataObject = new JSONObject();
                imagesDataObject.put("position", noOfImages + 1);
                imagesDataObject.put("file", medialGallery.get(noOfImages).getFile());
                imagesDataObject.put("value_id", medialGallery.get(noOfImages).getValueId());
                imagesDataObject.put("disabled", "0");
                mediaGallery.put(imagesDataObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mediaGallery;
    }

    public ArrayList<String> getRelated() {
        return related;
    }

    public void setRelated(ArrayList<String> related) {
        this.related = related;
    }

    public JSONArray getRelatedJSONArray() {
        JSONArray relatedJSONArray = new JSONArray();
        if (related != null) {
            for (int noOfProducts = 0; noOfProducts < related.size(); noOfProducts++) {
                relatedJSONArray.put(related.get(noOfProducts));
            }
        }
        return relatedJSONArray;
    }

    public ArrayList<String> getUpsell() {
        return upsell;
    }

    public void setUpsell(ArrayList<String> upsell) {
        this.upsell = upsell;
    }

    public JSONArray getUpsellJSONArray() {
        JSONArray upsellJSONArray = new JSONArray();
        if (upsell != null) {
            for (int noOfProducts = 0; noOfProducts < upsell.size(); noOfProducts++) {
                upsellJSONArray.put(upsell.get(noOfProducts));
            }
        }
        return upsellJSONArray;
    }

    public ArrayList<String> getCrossSell() {
        return crossSell;
    }

    public void setCrossSell(ArrayList<String> crossSell) {
        this.crossSell = crossSell;
    }

    public JSONArray getCrossSellJSONArray() {
        JSONArray crossJSONArray = new JSONArray();
        if (crossSell != null) {
            for (int noOfProducts = 0; noOfProducts < crossSell.size(); noOfProducts++) {
                crossJSONArray.put(crossSell.get(noOfProducts));
            }
        }
        return crossJSONArray;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public boolean isFormValidated(SellerAddProductActivity context) {
        boolean isFormValidated = true;

        if (productHasWeight == 1 && (weight == null || weight.trim().isEmpty())) {
            context.mBinding.weightTv.setTextColor(context.getResources().getColor(R.color.red_600));
            Utils.showShakeError(context, context.mBinding.weightTv);
            context.mBinding.weightEt.requestFocus();
            isFormValidated = false;
        } else {
            context.mBinding.weightTv.setTextColor(context.getResources().getColor(android.R.color.black));
        }

        if (visibility == null || visibility.trim().isEmpty()) {
            context.mBinding.visibilityTv.setTextColor(context.getResources().getColor(R.color.red_600));
            Utils.showShakeError(context, context.mBinding.visibilityTv);
            context.mBinding.visibilityTv.requestFocus();
            isFormValidated = false;
        } else {
            context.mBinding.visibilityTv.setTextColor(context.getResources().getColor(android.R.color.black));
        }

        if (qty == null || qty.trim().isEmpty()) {
            context.mBinding.stockTv.setTextColor(context.getResources().getColor(R.color.red_600));
            Utils.showShakeError(context, context.mBinding.stockTv);
            context.mBinding.stockEt.requestFocus();
            isFormValidated = false;
        } else {
            context.mBinding.stockTv.setTextColor(context.getResources().getColor(android.R.color.black));
        }

        if (price == null || price.trim().isEmpty()) {
            context.mBinding.priceTv.setTextColor(context.getResources().getColor(R.color.red_600));
            Utils.showShakeError(context, context.mBinding.priceTv);
            context.mBinding.priceEt.requestFocus();
            isFormValidated = false;
        } else {
            context.mBinding.priceTv.setTextColor(context.getResources().getColor(android.R.color.black));
        }

        if (context.mBinding.skuTv.getVisibility() == View.VISIBLE && (sku == null || sku.trim().isEmpty())) {
            context.mBinding.skuTv.setTextColor(context.getResources().getColor(R.color.red_600));
            Utils.showShakeError(context, context.mBinding.skuTv);
            context.mBinding.skuEt.requestFocus();
            isFormValidated = false;
        } else {
            context.mBinding.skuTv.setTextColor(context.getResources().getColor(android.R.color.black));
        }

        if (description == null || description.trim().isEmpty()) {
            context.mBinding.descTv.setTextColor(context.getResources().getColor(R.color.red_600));
            Utils.showShakeError(context, context.mBinding.descTv);
            context.mBinding.descEt.requestFocus();
            isFormValidated = false;
        } else {
            context.mBinding.descTv.setTextColor(context.getResources().getColor(android.R.color.black));
        }

        if (name == null || name.trim().isEmpty()) {
            context.mBinding.productNameTv.setTextColor(context.getResources().getColor(R.color.red_600));
            Utils.showShakeError(context, context.mBinding.productNameTv);
            context.mBinding.productNameEt.requestFocus();
            isFormValidated = false;
        } else {
            context.mBinding.productNameTv.setTextColor(context.getResources().getColor(android.R.color.black));
        }

        setSelectedCategoryIds(context);
        return isFormValidated;
    }

    private void setSelectedCategoryIds(SellerAddProductActivity context) {
        ArrayList<String> selectedCategoryIds = new ArrayList<>();
        for (int noOfCategories = 0; noOfCategories < context.mCategoryList.size(); noOfCategories++) {
            CustomCategoryData customCategories = context.mCategoryList.get(noOfCategories);
            if (customCategories.isChecked()) {
                selectedCategoryIds.add(String.valueOf(customCategories.getId()));
            }
            if (customCategories.getChildrenData().size() != 0) {
                for (int noOfSubcategory = 0; noOfSubcategory < customCategories.getChildrenData().size(); noOfSubcategory++) {
                    if (customCategories.getChildrenData().get(noOfSubcategory).isChecked()) {
                        selectedCategoryIds.add(String.valueOf(customCategories.getChildrenData().get(noOfSubcategory).getId()));
                    }
                }
            }
        }
        setCategoryIds(selectedCategoryIds);
    }
}