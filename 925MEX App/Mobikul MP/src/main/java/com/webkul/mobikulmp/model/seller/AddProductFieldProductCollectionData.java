package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.ArrayList;

public class AddProductFieldProductCollectionData extends BaseModel {

    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("filterOption")
    @Expose
    private ArrayList<FilterOption> filterOption = null;
    @SerializedName("productCollectionData")
    @Expose
    private ArrayList<ProductCollectionData> productCollectionData = null;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<FilterOption> getFilterOption() {
        return filterOption;
    }

    public void setFilterOption(ArrayList<FilterOption> filterOption) {
        this.filterOption = filterOption;
    }

    public ArrayList<ProductCollectionData> getProductCollectionData() {
        return productCollectionData;
    }

    public void setProductCollectionData(ArrayList<ProductCollectionData> productCollectionData) {
        this.productCollectionData = productCollectionData;
    }
}