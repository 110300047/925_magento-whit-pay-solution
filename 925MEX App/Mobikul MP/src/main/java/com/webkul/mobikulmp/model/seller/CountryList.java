package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryList {

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("is_region_visible")
    @Expose
    private boolean isRegionVisible;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isIsRegionVisible() {
        return isRegionVisible;
    }

    public void setIsRegionVisible(boolean isRegionVisible) {
        this.isRegionVisible = isRegionVisible;
    }

}