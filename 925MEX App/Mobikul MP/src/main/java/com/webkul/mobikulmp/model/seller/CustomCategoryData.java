package com.webkul.mobikulmp.model.seller;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 19/4/17. @Webkul Software Private limited
 */

public class CustomCategoryData implements Parent<CustomCategoryData> {

    private int id;
    private String name;
    private ArrayList<CustomCategoryData> childrenData = null;

    private boolean isChecked = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CustomCategoryData> getChildrenData() {
        return childrenData;
    }

    public void setChildrenData(ArrayList<CustomCategoryData> childrenData) {
        this.childrenData = childrenData;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public List<CustomCategoryData> getChildList() {
        return childrenData;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
