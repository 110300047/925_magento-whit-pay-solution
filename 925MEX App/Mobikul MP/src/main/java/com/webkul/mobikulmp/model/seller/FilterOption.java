package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilterOption {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("options")
    @Expose
    private List<OptionsData> options = null;

    private String valueFrom;
    private String valueTo;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public List<OptionsData> getOptions() {
        return options;
    }

    public String getValueFrom() {
        return valueFrom;
    }

    public void setValueFrom(String valueFrom) {
        this.valueFrom = valueFrom;
    }

    public String getValueTo() {
        return valueTo;
    }

    public void setValueTo(String valueTo) {
        this.valueTo = valueTo;
    }

    public int getSelectedOptionData() {
        if (valueFrom != null && !valueFrom.isEmpty()) {
            for (int noOfOptions = 0; noOfOptions < options.size(); noOfOptions++) {
                if (options.get(noOfOptions).getValue().equals(valueFrom))
                    return noOfOptions + 1;
            }
        }
        return 0;
    }
}