package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedialGallery {

    @SerializedName("value_id")
    @Expose
    private String valueId;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("media_type")
    @Expose
    private String mediaType;
    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("disabled")
    @Expose
    private String disabled;
    @SerializedName("label_default")
    @Expose
    private String labelDefault;
    @SerializedName("position_default")
    @Expose
    private String positionDefault;
    @SerializedName("disabled_default")
    @Expose
    private String disabledDefault;
    @SerializedName("video_provider")
    @Expose
    private String videoProvider;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("video_title")
    @Expose
    private String videoTitle;
    @SerializedName("video_description")
    @Expose
    private String videoDescription;
    @SerializedName("video_metadata")
    @Expose
    private String videoMetadata;
    @SerializedName("video_provider_default")
    @Expose
    private String videoProviderDefault;
    @SerializedName("video_url_default")
    @Expose
    private String videoUrlDefault;
    @SerializedName("video_title_default")
    @Expose
    private String videoTitleDefault;
    @SerializedName("video_description_default")
    @Expose
    private String videoDescriptionDefault;
    @SerializedName("video_metadata_default")
    @Expose
    private String videoMetadataDefault;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("path")
    @Expose
    private String path;

    public String getValueId() {
        return valueId;
    }

    public void setValueId(String valueId) {
        this.valueId = valueId;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public String getLabelDefault() {
        return labelDefault;
    }

    public void setLabelDefault(String labelDefault) {
        this.labelDefault = labelDefault;
    }

    public String getPositionDefault() {
        return positionDefault;
    }

    public void setPositionDefault(String positionDefault) {
        this.positionDefault = positionDefault;
    }

    public String getDisabledDefault() {
        return disabledDefault;
    }

    public void setDisabledDefault(String disabledDefault) {
        this.disabledDefault = disabledDefault;
    }

    public String getVideoProvider() {
        return videoProvider;
    }

    public void setVideoProvider(String videoProvider) {
        this.videoProvider = videoProvider;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getVideoMetadata() {
        return videoMetadata;
    }

    public void setVideoMetadata(String videoMetadata) {
        this.videoMetadata = videoMetadata;
    }

    public String getVideoProviderDefault() {
        return videoProviderDefault;
    }

    public void setVideoProviderDefault(String videoProviderDefault) {
        this.videoProviderDefault = videoProviderDefault;
    }

    public String getVideoUrlDefault() {
        return videoUrlDefault;
    }

    public void setVideoUrlDefault(String videoUrlDefault) {
        this.videoUrlDefault = videoUrlDefault;
    }

    public String getVideoTitleDefault() {
        return videoTitleDefault;
    }

    public void setVideoTitleDefault(String videoTitleDefault) {
        this.videoTitleDefault = videoTitleDefault;
    }

    public String getVideoDescriptionDefault() {
        return videoDescriptionDefault;
    }

    public void setVideoDescriptionDefault(String videoDescriptionDefault) {
        this.videoDescriptionDefault = videoDescriptionDefault;
    }

    public String getVideoMetadataDefault() {
        return videoMetadataDefault;
    }

    public void setVideoMetadataDefault(String videoMetadataDefault) {
        this.videoMetadataDefault = videoMetadataDefault;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}