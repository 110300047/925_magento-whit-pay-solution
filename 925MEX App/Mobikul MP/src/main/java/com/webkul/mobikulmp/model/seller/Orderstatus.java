package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Orderstatus {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("label")
    @Expose
    private String label;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}