package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductCollectionData {

    @SerializedName("selected")
    @Expose
    private boolean selected;
    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("attrinuteSet")
    @Expose
    private String attrinuteSet;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("price")
    @Expose
    private String price;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttrinuteSet() {
        return attrinuteSet;
    }

    public void setAttrinuteSet(String attrinuteSet) {
        this.attrinuteSet = attrinuteSet;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}