package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

/**
 * Created by vedesh.kumar on 22/2/18. @Webkul Software Private limited
 */

public class SaveProductResponseData extends BaseModel {
    @SerializedName("productId")
    @Expose
    private String productId;

    public String getProductId() {
        return productId;
    }
}
