package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikul.model.catalog.DefaultCategoryData;

import java.util.List;

public class SellerAddProductResponseData extends BaseModel {

    @SerializedName("skuType")
    @Expose
    private String skuType;
    @SerializedName("skuhint")
    @Expose
    private String skuhint;
    @SerializedName("taxHint")
    @Expose
    private String taxHint;
    @SerializedName("showHint")
    @Expose
    private boolean showHint;
    @SerializedName("skuPrefix")
    @Expose
    private String skuPrefix;
    @SerializedName("priceHint")
    @Expose
    private String priceHint;
    @SerializedName("imageHint")
    @Expose
    private String imageHint;
    @SerializedName("taxOptions")
    @Expose
    private List<OptionsData> taxOptions = null;
    @SerializedName("categories")
    @Expose
    private DefaultCategoryData categories;
    @SerializedName("assignedCategories")
    @Expose
    private List<OptionsData> assignedCategories = null;
    @SerializedName("weightHint")
    @Expose
    private String weightHint;
    @SerializedName("productHint")
    @Expose
    private String productHint;
    @SerializedName("categoryHint")
    @Expose
    private String categoryHint;
    @SerializedName("allowedTypes")
    @Expose
    private List<OptionsData> allowedTypes = null;
    @SerializedName("inventoryHint")
    @Expose
    private String inventoryHint;
    @SerializedName("currencySymbol")
    @Expose
    private String currencySymbol;
    @SerializedName("descriptionHint")
    @Expose
    private String descriptionHint;
    @SerializedName("specialPriceHint")
    @Expose
    private String specialPriceHint;
    @SerializedName("visibilityOptions")
    @Expose
    private List<OptionsData> visibilityOptions = null;
    @SerializedName("allowedAttributes")
    @Expose
    private List<AllowedAttribute> allowedAttributes = null;
    @SerializedName("specialEndDateHint")
    @Expose
    private String specialEndDateHint;
    @SerializedName("shortdescriptionHint")
    @Expose
    private String shortdescriptionHint;
    @SerializedName("specialStartDateHint")
    @Expose
    private String specialStartDateHint;
    @SerializedName("addProductLimitStatus")
    @Expose
    private boolean addProductLimitStatus;
    @SerializedName("isCategoryTreeAllowed")
    @Expose
    private boolean isCategoryTreeAllowed;
    @SerializedName("addUpsellProductStatus")
    @Expose
    private boolean addUpsellProductStatus;
    @SerializedName("addRelatedProductStatus")
    @Expose
    private boolean addRelatedProductStatus;
    @SerializedName("addCrosssellProductStatus")
    @Expose
    private boolean addCrosssellProductStatus;
    @SerializedName("inventoryAvailabilityHint")
    @Expose
    private String inventoryAvailabilityHint;
    @SerializedName("inventoryAvailabilityOptions")
    @Expose
    private List<OptionsData> inventoryAvailabilityOptions = null;
    @SerializedName("productData")
    @Expose
    private AddProductData productData;

    public String getSkuType() {
        return skuType;
    }

    public void setSkuType(String skuType) {
        this.skuType = skuType;
    }

    public String getSkuhint() {
        return skuhint;
    }

    public String getTaxHint() {
        return taxHint;
    }

    public boolean isShowHint() {
        return showHint;
    }

    public String getSkuPrefix() {
        return skuPrefix;
    }

    public String getPriceHint() {
        return priceHint;
    }

    public List<OptionsData> getTaxOptions() {
        return taxOptions;
    }

    public void setTaxOptions(List<OptionsData> taxOptions) {
        this.taxOptions = taxOptions;
    }

    public DefaultCategoryData getCategories() {
        return categories;
    }

    public void setCategories(DefaultCategoryData categories) {
        this.categories = categories;
    }

    public String getWeightHint() {
        return weightHint;
    }

    public String getProductHint() {
        return productHint;
    }

    public String getCategoryHint() {
        return categoryHint;
    }

    public List<OptionsData> getAllowedTypes() {
        return allowedTypes;
    }

    public void setAllowedTypes(List<OptionsData> allowedTypes) {
        this.allowedTypes = allowedTypes;
    }

    public String getInventoryHint() {
        return inventoryHint;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public String getDescriptionHint() {
        return descriptionHint;
    }

    public String getSpecialPriceHint() {
        return specialPriceHint;
    }

    public List<OptionsData> getVisibilityOptions() {
        return visibilityOptions;
    }

    public List<AllowedAttribute> getAllowedAttributes() {
        return allowedAttributes;
    }

    public String getSpecialEndDateHint() {
        return specialEndDateHint;
    }

    public String getShortdescriptionHint() {
        return shortdescriptionHint;
    }

    public String getSpecialStartDateHint() {
        return specialStartDateHint;
    }

    public boolean isIsCategoryTreeAllowed() {
        return isCategoryTreeAllowed;
    }

    public void setIsCategoryTreeAllowed(boolean isCategoryTreeAllowed) {
        this.isCategoryTreeAllowed = isCategoryTreeAllowed;
    }

    public String getInventoryAvailabilityHint() {
        return inventoryAvailabilityHint;
    }

    public List<OptionsData> getInventoryAvailabilityOptions() {
        return inventoryAvailabilityOptions;
    }

    public AddProductData getProductData() {
        return productData;
    }

    public void setProductData(AddProductData productData) {
        this.productData = productData;
    }

    public List<OptionsData> getAssignedCategories() {
        return assignedCategories;
    }

    public String getImageHint() {
        return imageHint;
    }

    public int getSelectedAttributeSet() {
        if (productData != null) {
            for (int noOfAttribute = 0; noOfAttribute < allowedAttributes.size(); noOfAttribute++) {
                if (allowedAttributes.get(noOfAttribute).getValue().equals(productData.getAttributeSetId())) {
                    return noOfAttribute;
                }
            }
        }
        return 0;
    }

    public int getSelectedType() {
        if (productData != null)
            for (int noOfTypes = 0; noOfTypes < allowedTypes.size(); noOfTypes++) {
                if (allowedTypes.get(noOfTypes).getValue().equals(productData.getType())) {
                    return noOfTypes;
                }
            }
        return 0;
    }

    public int getSelectedVisibility() {
        if (productData != null)
            for (int noOfVisibilityOptions = 0; noOfVisibilityOptions < visibilityOptions.size(); noOfVisibilityOptions++) {
                if (visibilityOptions.get(noOfVisibilityOptions).getValue().equals(productData.getVisibility())) {
                    return noOfVisibilityOptions + 1;
                }
            }
        return 0;
    }

    public int getSelectedTaxClass() {
        if (productData != null)
            for (int noOfTaxClass = 0; noOfTaxClass < taxOptions.size(); noOfTaxClass++) {
                if (taxOptions.get(noOfTaxClass).getValue().equals(productData.getTaxClassId())) {
                    return noOfTaxClass;
                }
            }
        return 0;
    }

    public boolean isAddProductLimitStatus() {
        return addProductLimitStatus;
    }

    public boolean isAddUpsellProductStatus() {
        return addUpsellProductStatus;
    }

    public boolean isAddRelatedProductStatus() {
        return addRelatedProductStatus;
    }

    public boolean isAddCrosssellProductStatus() {
        return addCrosssellProductStatus;
    }
}