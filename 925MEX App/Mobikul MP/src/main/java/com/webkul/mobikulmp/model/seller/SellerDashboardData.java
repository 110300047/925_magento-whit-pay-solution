package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.ArrayList;

public class SellerDashboardData extends BaseModel {

    @SerializedName("reviewList")
    @Expose
    private ArrayList<SellerReviewList> sellerReviewList = null;
    @SerializedName("totalPayout")
    @Expose
    private String totalPayout;
    @SerializedName("lifetimeSale")
    @Expose
    private String lifetimeSale;
    @SerializedName("categoryChart")
    @Expose
    private String categoryChart;
    @SerializedName("remainingAmount")
    @Expose
    private String remainingAmount;
    @SerializedName("recentOrderList")
    @Expose
    private ArrayList<SellerOrderData> recentOrderList = null;
    @SerializedName("dailySalesStats")
    @Expose
    private String dailySalesStats;
    @SerializedName("yearlySalesStats")
    @Expose
    private String yearlySalesStats;
    @SerializedName("weeklySalesStats")
    @Expose
    private String weeklySalesStats;
    @SerializedName("monthlySalesStats")
    @Expose
    private String monthlySalesStats;
    @SerializedName("topSellingProducts")
    @Expose
    private ArrayList<TopSellingProduct> topSellingProducts = null;
    @SerializedName("dailySalesLocationReport")
    @Expose
    private String dailySalesLocationReport;
    @SerializedName("yearlySalesLocationReport")
    @Expose
    private String yearlySalesLocationReport;
    @SerializedName("weeklySalesLocationReport")
    @Expose
    private String weeklySalesLocationReport;
    @SerializedName("monthlySalesLocationReport")
    @Expose
    private String monthlySalesLocationReport;


    public ArrayList<SellerReviewList> getSellerReviewList() {
        return sellerReviewList;
    }

    public void setSellerReviewList(ArrayList<SellerReviewList> reviewList) {
        this.sellerReviewList = reviewList;
    }

    public String getTotalPayout() {
        return totalPayout;
    }

    public void setTotalPayout(String totalPayout) {
        this.totalPayout = totalPayout;
    }

    public String getLifetimeSale() {
        return lifetimeSale;
    }

    public void setLifetimeSale(String lifetimeSale) {
        this.lifetimeSale = lifetimeSale;
    }

    public String getCategoryChart() {
        return categoryChart;
    }

    public void setCategoryChart(String categoryChart) {
        this.categoryChart = categoryChart;
    }

    public String getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(String remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public ArrayList<SellerOrderData> getRecentOrderList() {
        return recentOrderList;
    }

    public void setRecentOrderList(ArrayList<SellerOrderData> recentOrderList) {
        this.recentOrderList = recentOrderList;
    }

    public String getDailySalesStats() {
        return dailySalesStats;
    }

    public void setDailySalesStats(String dailySalesStats) {
        this.dailySalesStats = dailySalesStats;
    }

    public String getYearlySalesStats() {
        return yearlySalesStats;
    }

    public void setYearlySalesStats(String yearlySalesStats) {
        this.yearlySalesStats = yearlySalesStats;
    }

    public String getWeeklySalesStats() {
        return weeklySalesStats;
    }

    public void setWeeklySalesStats(String weeklySalesStats) {
        this.weeklySalesStats = weeklySalesStats;
    }

    public String getMonthlySalesStats() {
        return monthlySalesStats;
    }

    public void setMonthlySalesStats(String monthlySalesStats) {
        this.monthlySalesStats = monthlySalesStats;
    }

    public ArrayList<TopSellingProduct> getTopSellingProducts() {
        return topSellingProducts;
    }

    public void setTopSellingProducts(ArrayList<TopSellingProduct> topSellingProducts) {
        this.topSellingProducts = topSellingProducts;
    }

    public String getDailySalesLocationReport() {
        return dailySalesLocationReport;
    }

    public void setDailySalesLocationReport(String dailySalesLocationReport) {
        this.dailySalesLocationReport = dailySalesLocationReport;
    }

    public String getYearlySalesLocationReport() {
        return yearlySalesLocationReport;
    }

    public void setYearlySalesLocationReport(String yearlySalesLocationReport) {
        this.yearlySalesLocationReport = yearlySalesLocationReport;
    }

    public String getWeeklySalesLocationReport() {
        return weeklySalesLocationReport;
    }

    public void setWeeklySalesLocationReport(String weeklySalesLocationReport) {
        this.weeklySalesLocationReport = weeklySalesLocationReport;
    }

    public String getMonthlySalesLocationReport() {
        return monthlySalesLocationReport;
    }

    public void setMonthlySalesLocationReport(String monthlySalesLocationReport) {
        this.monthlySalesLocationReport = monthlySalesLocationReport;
    }
}