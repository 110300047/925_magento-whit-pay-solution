package com.webkul.mobikulmp.model.seller;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.webkul.mobikulmp.BR;

/**
 * Created by vedesh.kumar on 9/1/18. @Webkul Software Private limited
 */

public class SellerDownloadAllFragmentData extends BaseObservable {

    private String fromDate = "";
    private String toDate = "";

    @Bindable
    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
        notifyPropertyChanged(BR.fromDate);
    }

    @Bindable
    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
        notifyPropertyChanged(BR.toDate);
    }

    public void resetData() {
        setFromDate("");
        setToDate("");
    }
}