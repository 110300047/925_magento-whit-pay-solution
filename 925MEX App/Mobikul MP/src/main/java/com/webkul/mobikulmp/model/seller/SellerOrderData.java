
package com.webkul.mobikulmp.model.seller;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SellerOrderData implements Parcelable {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("incrementId")
    @Expose
    private String incrementId;
    @SerializedName("productNames")
    @Expose
    private List<SellerOrderProductList> sellerOrderProductList = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("customerDetails")
    @Expose
    private CustomerDetails customerDetails;

    protected SellerOrderData(Parcel in) {
        orderId = in.readString();
        incrementId = in.readString();
        sellerOrderProductList = in.createTypedArrayList(SellerOrderProductList.CREATOR);
        status = in.readString();
        customerDetails = in.readParcelable(CustomerDetails.class.getClassLoader());
    }

    public static final Creator<SellerOrderData> CREATOR = new Creator<SellerOrderData>() {
        @Override
        public SellerOrderData createFromParcel(Parcel in) {
            return new SellerOrderData(in);
        }

        @Override
        public SellerOrderData[] newArray(int size) {
            return new SellerOrderData[size];
        }
    };

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public List<SellerOrderProductList> getSellerOrderProductList() {
        return sellerOrderProductList;
    }

    public void setSellerOrderProductList(List<SellerOrderProductList> sellerOrderProductList) {
        this.sellerOrderProductList = sellerOrderProductList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderId);
        dest.writeString(incrementId);
        dest.writeTypedList(sellerOrderProductList);
        dest.writeString(status);
        dest.writeParcelable(customerDetails, flags);
    }
}
