package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vedesh.kumar on 25/5/17. @Webkul Software Private limited
 */

public class SellerOrderDetailResponseData extends BaseModel {

    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("canShip")
    @Expose
    private boolean canShip;
    @SerializedName("itemList")
    @Expose
    private ArrayList<SellerOrderItemList> itemList = null;
    @SerializedName("subTotal")
    @Expose
    private String subTotal;
    @SerializedName("shipping")
    @Expose
    private String shipping;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("buyerName")
    @Expose
    private String buyerName;
    @SerializedName("orderTotal")
    @Expose
    private String orderTotal;
    @SerializedName("buyerEmail")
    @Expose
    private String buyerEmail;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("incrementId")
    @Expose
    private String incrementId;
    @SerializedName("mpcodcharge")
    @Expose
    private String mpcodcharge;
    @SerializedName("vendorTotal")
    @Expose
    private String vendorTotal;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("shippingMethod")
    @Expose
    private String shippingMethod;
    @SerializedName("billingAddress")
    @Expose
    private String billingAddress;
    @SerializedName("mpCODAvailable")
    @Expose
    private boolean mpCODAvailable;
    @SerializedName("orderBaseTotal")
    @Expose
    private String orderBaseTotal;
    @SerializedName("vendorBaseTotal")
    @Expose
    private String vendorBaseTotal;
    @SerializedName("adminCommission")
    @Expose
    private String adminCommission;
    @SerializedName("shippingAddress")
    @Expose
    private String shippingAddress;
    @SerializedName("adminBaseCommission")
    @Expose
    private String adminBaseCommission;
    @SerializedName("showBuyerInformation")
    @Expose
    private boolean showBuyerInformation;
    @SerializedName("showAddressInformation")
    @Expose
    private boolean showAddressInformation;

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isCanShip() {
        return canShip;
    }

    public void setCanShip(boolean canShip) {
        this.canShip = canShip;
    }

    public ArrayList<SellerOrderItemList> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<SellerOrderItemList> itemList) {
        this.itemList = itemList;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public String getMpcodcharge() {
        return mpcodcharge;
    }

    public void setMpcodcharge(String mpcodcharge) {
        this.mpcodcharge = mpcodcharge;
    }

    public String getVendorTotal() {
        return vendorTotal;
    }

    public void setVendorTotal(String vendorTotal) {
        this.vendorTotal = vendorTotal;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public boolean isMpCODAvailable() {
        return mpCODAvailable;
    }

    public void setMpCODAvailable(boolean mpCODAvailable) {
        this.mpCODAvailable = mpCODAvailable;
    }

    public String getOrderBaseTotal() {
        return orderBaseTotal;
    }

    public void setOrderBaseTotal(String orderBaseTotal) {
        this.orderBaseTotal = orderBaseTotal;
    }

    public String getVendorBaseTotal() {
        return vendorBaseTotal;
    }

    public void setVendorBaseTotal(String vendorBaseTotal) {
        this.vendorBaseTotal = vendorBaseTotal;
    }

    public String getAdminCommission() {
        return adminCommission;
    }

    public void setAdminCommission(String adminCommission) {
        this.adminCommission = adminCommission;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getAdminBaseCommission() {
        return adminBaseCommission;
    }

    public void setAdminBaseCommission(String adminBaseCommission) {
        this.adminBaseCommission = adminBaseCommission;
    }

    public boolean isShowBuyerInformation() {
        return showBuyerInformation;
    }

    public void setShowBuyerInformation(boolean showBuyerInformation) {
        this.showBuyerInformation = showBuyerInformation;
    }

    public boolean isShowAddressInformation() {
        return showAddressInformation;
    }

    public void setShowAddressInformation(boolean showAddressInformation) {
        this.showAddressInformation = showAddressInformation;
    }

}
