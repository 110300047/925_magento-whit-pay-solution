package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SellerOrderItemList {

    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("customOption")
    @Expose
    private List<CustomOption> customOption = null;
    @SerializedName("downloadableOptionLable")
    @Expose
    private String downloadableOptionLable;
    @SerializedName("downloadableOptionValue")
    @Expose
    private List<Object> downloadableOptionValue = null;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("qty")
    @Expose
    private ArrayList<SellerOrderItemQty> qty = null;
    @SerializedName("totalPrice")
    @Expose
    private String totalPrice;
    @SerializedName("mpcodprice")
    @Expose
    private String mpcodprice;
    @SerializedName("adminCommission")
    @Expose
    private String adminCommission;
    @SerializedName("vendorTotal")
    @Expose
    private String vendorTotal;
    @SerializedName("subTotal")
    @Expose
    private String subTotal;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public List<CustomOption> getCustomOption() {
        return customOption;
    }

    public void setCustomOption(List<CustomOption> customOption) {
        this.customOption = customOption;
    }

    public String getDownloadableOptionLable() {
        return downloadableOptionLable;
    }

    public void setDownloadableOptionLable(String downloadableOptionLable) {
        this.downloadableOptionLable = downloadableOptionLable;
    }

    public List<Object> getDownloadableOptionValue() {
        return downloadableOptionValue;
    }

    public void setDownloadableOptionValue(List<Object> downloadableOptionValue) {
        this.downloadableOptionValue = downloadableOptionValue;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public ArrayList<SellerOrderItemQty> getQty() {
        return qty;
    }

    public void setQty(ArrayList<SellerOrderItemQty> qty) {
        this.qty = qty;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getMpcodprice() {
        return mpcodprice;
    }

    public void setMpcodprice(String mpcodprice) {
        this.mpcodprice = mpcodprice;
    }

    public String getAdminCommission() {
        return adminCommission;
    }

    public void setAdminCommission(String adminCommission) {
        this.adminCommission = adminCommission;
    }

    public String getVendorTotal() {
        return vendorTotal;
    }

    public void setVendorTotal(String vendorTotal) {
        this.vendorTotal = vendorTotal;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

}