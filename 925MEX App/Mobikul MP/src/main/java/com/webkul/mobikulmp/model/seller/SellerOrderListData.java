package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SellerOrderListData {

    @SerializedName("authKey")
    @Expose
    private String authKey;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("orderList")
    @Expose
    private List<SellerOrderData> orderList = null;
    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("orderStatus")
    @Expose
    private List<Orderstatus> orderStatus = null;
    @SerializedName("responseCode")
    @Expose
    private int responseCode;

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SellerOrderData> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<SellerOrderData> orderList) {
        this.orderList = orderList;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Orderstatus> getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(List<Orderstatus> orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

}