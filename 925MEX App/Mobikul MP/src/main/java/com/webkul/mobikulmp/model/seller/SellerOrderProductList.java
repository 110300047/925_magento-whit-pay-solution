
package com.webkul.mobikulmp.model.seller;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerOrderProductList implements Parcelable {

    @SerializedName("qty")
    @Expose
    private int qty;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("productId")
    @Expose
    private String productId;

    protected SellerOrderProductList(Parcel in) {
        qty = in.readInt();
        name = in.readString();
        productId = in.readString();
    }

    public static final Creator<SellerOrderProductList> CREATOR = new Creator<SellerOrderProductList>() {
        @Override
        public SellerOrderProductList createFromParcel(Parcel in) {
            return new SellerOrderProductList(in);
        }

        @Override
        public SellerOrderProductList[] newArray(int size) {
            return new SellerOrderProductList[size];
        }
    };

    public String getQty() {
        return String.valueOf(qty);
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(qty);
        dest.writeString(name);
        dest.writeString(productId);
    }
}
