package com.webkul.mobikulmp.model.seller;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikulmp.BR;

public class SellerProductData extends BaseObservable {

    @SerializedName("productId")
    @Expose
    private int productId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("openable")
    @Expose
    private boolean openable;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("productPrice")
    @Expose
    private String productPrice;
    @SerializedName("productType")
    @Expose
    private String productType;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("qtySold")
    @Expose
    private int qtySold;
    @SerializedName("qtyPending")
    @Expose
    private int qtyPending;
    @SerializedName("qtyConfirmed")
    @Expose
    private int qtyConfirmed;
    @SerializedName("earnedAmount")
    @Expose
    private String earnedAmount;

    private int position = 0;
    private boolean selectionModeOn = false;
    private boolean selected = false;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isOpenable() {
        return openable;
    }

    public void setOpenable(boolean openable) {
        this.openable = openable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQtySold() {
        return qtySold;
    }

    public void setQtySold(int qtySold) {
        this.qtySold = qtySold;
    }

    public int getQtyPending() {
        return qtyPending;
    }

    public void setQtyPending(int qtyPending) {
        this.qtyPending = qtyPending;
    }

    public int getQtyConfirmed() {
        return qtyConfirmed;
    }

    public void setQtyConfirmed(int qtyConfirmed) {
        this.qtyConfirmed = qtyConfirmed;
    }

    public String getEarnedAmount() {
        return earnedAmount;
    }

    public void setEarnedAmount(String earnedAmount) {
        this.earnedAmount = earnedAmount;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Bindable
    public boolean isSelectionModeOn() {
        return selectionModeOn;
    }

    public void setSelectionModeOn(boolean selectionModeOn) {
        this.selectionModeOn = selectionModeOn;
        notifyPropertyChanged(BR.selectionModeOn);
    }

    @Bindable
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        notifyPropertyChanged(BR.selected);
    }
}