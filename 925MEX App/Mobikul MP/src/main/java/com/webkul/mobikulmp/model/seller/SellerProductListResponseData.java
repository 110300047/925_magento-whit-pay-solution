package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.ArrayList;

public class SellerProductListResponseData extends BaseModel {

    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("productList")
    @Expose
    private ArrayList<SellerProductData> productList = null;
    @SerializedName("enabledStatusText")
    @Expose
    private String enabledStatusText;
    @SerializedName("disabledStatusText")
    @Expose
    private String disabledStatusText;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<SellerProductData> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<SellerProductData> productList) {
        this.productList = productList;
    }

    public String getEnabledStatusText() {
        return enabledStatusText;
    }

    public void setEnabledStatusText(String enabledStatusText) {
        this.enabledStatusText = enabledStatusText;
    }

    public String getDisabledStatusText() {
        return disabledStatusText;
    }

    public void setDisabledStatusText(String disabledStatusText) {
        this.disabledStatusText = disabledStatusText;
    }
}