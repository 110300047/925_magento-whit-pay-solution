package com.webkul.mobikulmp.model.seller;

import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;
import com.webkul.mobikulmp.BR;

import java.util.ArrayList;

public class SellerProfileFormResponseData extends BaseModel {

    @SerializedName("taxvat")
    @Expose
    private String taxvat;
    @SerializedName("vimeoId")
    @Expose
    private String vimeoId;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("twitterId")
    @Expose
    private String twitterId;
    @SerializedName("youtubeId")
    @Expose
    private String youtubeId;
    @SerializedName("shopTitle")
    @Expose
    private String shopTitle;
    @SerializedName("facebookId")
    @Expose
    private String facebookId;
    @SerializedName("taxvatHint")
    @Expose
    private String taxvatHint;
    @SerializedName("bannerHint")
    @Expose
    private String bannerHint;
    @SerializedName("instagramId")
    @Expose
    private String instagramId;
    @SerializedName("countryList")
    @Expose
    private ArrayList<CountryList> countryList = null;
    @SerializedName("pinterestId")
    @Expose
    private String pinterestId;
    @SerializedName("twitterHint")
    @Expose
    private String twitterHint;
    @SerializedName("metaKeyword")
    @Expose
    private String metaKeyword;
    @SerializedName("bannerImage")
    @Expose
    private String bannerImage;
    @SerializedName("countryHint")
    @Expose
    private String countryHint;
    @SerializedName("flagImageUrl")
    @Expose
    private String flagImageUrl;
    @SerializedName("facebookHint")
    @Expose
    private String facebookHint;
    @SerializedName("googleplusId")
    @Expose
    private String googleplusId;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("returnPolicy")
    @Expose
    private String returnPolicy;
    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;
    @SerializedName("shopTitleHint")
    @Expose
    private String shopTitleHint;
    @SerializedName("isVimeoActive")
    @Expose
    private boolean isVimeoActive;
    @SerializedName("paymentDetails")
    @Expose
    private String paymentDetails;
    @SerializedName("shippingPolicy")
    @Expose
    private String shippingPolicy;
    @SerializedName("companyLocality")
    @Expose
    private String companyLocality;
    @SerializedName("showProfileHint")
    @Expose
    private boolean showProfileHint;
    @SerializedName("isYoutubeActive")
    @Expose
    private boolean isYoutubeActive;
    @SerializedName("isTwitterActive")
    @Expose
    private boolean isTwitterActive;
    @SerializedName("backgroundColor")
    @Expose
    private String backgroundColor;
    @SerializedName("metaDescription")
    @Expose
    private String metaDescription;
    @SerializedName("metaKeywordHint")
    @Expose
    private String metaKeywordHint;
    @SerializedName("returnPolicyHint")
    @Expose
    private String returnPolicyHint;
    @SerializedName("isFacebookActive")
    @Expose
    private boolean isFacebookActive;
    @SerializedName("profileImageHint")
    @Expose
    private String profileImageHint;
    @SerializedName("isInstagramActive")
    @Expose
    private boolean isInstagramActive;
    @SerializedName("isPinterestActive")
    @Expose
    private boolean isPinterestActive;
    @SerializedName("contactNumberHint")
    @Expose
    private String contactNumberHint;
    @SerializedName("isgoogleplusActive")
    @Expose
    private boolean isgoogleplusActive;
    @SerializedName("companyDescription")
    @Expose
    private String companyDescription;
    @SerializedName("shippingPolicyHint")
    @Expose
    private String shippingPolicyHint;
    @SerializedName("paymentDetailsHint")
    @Expose
    private String paymentDetailsHint;
    @SerializedName("backgroundColorHint")
    @Expose
    private String backgroundColorHint;
    @SerializedName("metaDescriptionHint")
    @Expose
    private String metaDescriptionHint;
    @SerializedName("companyLocalityHint")
    @Expose
    private String companyLocalityHint;
    @SerializedName("companyDescriptionHint")
    @Expose
    private String companyDescriptionHint;

    public String getTaxvat() {
        return taxvat;
    }

    public void setTaxvat(String taxvat) {
        this.taxvat = taxvat;
    }

    public String getVimeoId() {
        return vimeoId;
    }

    public void setVimeoId(String vimeoId) {
        this.vimeoId = vimeoId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeId = youtubeId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getTaxvatHint() {
        return taxvatHint;
    }

    public void setTaxvatHint(String taxvatHint) {
        this.taxvatHint = taxvatHint;
    }

    public String getBannerHint() {
        return bannerHint;
    }

    public void setBannerHint(String bannerHint) {
        this.bannerHint = bannerHint;
    }

    public String getInstagramId() {
        return instagramId;
    }

    public void setInstagramId(String instagramId) {
        this.instagramId = instagramId;
    }

    public ArrayList<CountryList> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<CountryList> countryList) {
        this.countryList = countryList;
    }

    public int getSelectedCountryPositionFromAddressData() {
        for (int countryPosition = 0; countryPosition < countryList.size(); countryPosition++) {
            if (countryList.get(countryPosition).getValue().equals(country)) {
                return countryPosition;
            }
        }
        return 0;
    }

    public String getPinterestId() {
        return pinterestId;
    }

    public void setPinterestId(String pinterestId) {
        this.pinterestId = pinterestId;
    }

    public String getTwitterHint() {
        return twitterHint;
    }

    public void setTwitterHint(String twitterHint) {
        this.twitterHint = twitterHint;
    }

    public String getMetaKeyword() {
        return metaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        this.metaKeyword = metaKeyword;
    }

    @Bindable
    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
        notifyPropertyChanged(BR.bannerImage);
    }

    public String getCountryHint() {
        return countryHint;
    }

    public void setCountryHint(String countryHint) {
        this.countryHint = countryHint;
    }

    public String getFlagImageUrl() {
        return flagImageUrl;
    }

    public void setFlagImageUrl(String flagImageUrl) {
        this.flagImageUrl = flagImageUrl;
    }

    public String getFacebookHint() {
        return facebookHint;
    }

    public void setFacebookHint(String facebookHint) {
        this.facebookHint = facebookHint;
    }

    public String getGoogleplusId() {
        return googleplusId;
    }

    public void setGoogleplusId(String googleplusId) {
        this.googleplusId = googleplusId;
    }

    @Bindable
    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
        notifyPropertyChanged(BR.profileImage);
    }

    public String getReturnPolicy() {
        return returnPolicy;
    }

    public void setReturnPolicy(String returnPolicy) {
        this.returnPolicy = returnPolicy;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getShopTitleHint() {
        return shopTitleHint;
    }

    public void setShopTitleHint(String shopTitleHint) {
        this.shopTitleHint = shopTitleHint;
    }

    public boolean isIsVimeoActive() {
        return isVimeoActive;
    }

    public void setIsVimeoActive(boolean isVimeoActive) {
        this.isVimeoActive = isVimeoActive;
    }

    public String getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(String paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public String getShippingPolicy() {
        return shippingPolicy;
    }

    public void setShippingPolicy(String shippingPolicy) {
        this.shippingPolicy = shippingPolicy;
    }

    public String getCompanyLocality() {
        return companyLocality;
    }

    public void setCompanyLocality(String companyLocality) {
        this.companyLocality = companyLocality;
    }

    public boolean isShowProfileHint() {
        return showProfileHint;
    }

    public void setShowProfileHint(boolean showProfileHint) {
        this.showProfileHint = showProfileHint;
    }

    public boolean isIsYoutubeActive() {
        return isYoutubeActive;
    }

    public void setIsYoutubeActive(boolean isYoutubeActive) {
        this.isYoutubeActive = isYoutubeActive;
    }

    public boolean isIsTwitterActive() {
        return isTwitterActive;
    }

    public void setIsTwitterActive(boolean isTwitterActive) {
        this.isTwitterActive = isTwitterActive;
    }

    @Bindable
    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        notifyPropertyChanged(BR.backgroundColor);
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMetaKeywordHint() {
        return metaKeywordHint;
    }

    public void setMetaKeywordHint(String metaKeywordHint) {
        this.metaKeywordHint = metaKeywordHint;
    }

    public String getReturnPolicyHint() {
        return returnPolicyHint;
    }

    public void setReturnPolicyHint(String returnPolicyHint) {
        this.returnPolicyHint = returnPolicyHint;
    }

    public boolean isIsFacebookActive() {
        return isFacebookActive;
    }

    public void setIsFacebookActive(boolean isFacebookActive) {
        this.isFacebookActive = isFacebookActive;
    }

    public String getProfileImageHint() {
        return profileImageHint;
    }

    public void setProfileImageHint(String profileImageHint) {
        this.profileImageHint = profileImageHint;
    }

    public boolean isIsInstagramActive() {
        return isInstagramActive;
    }

    public void setIsInstagramActive(boolean isInstagramActive) {
        this.isInstagramActive = isInstagramActive;
    }

    public boolean isIsPinterestActive() {
        return isPinterestActive;
    }

    public void setIsPinterestActive(boolean isPinterestActive) {
        this.isPinterestActive = isPinterestActive;
    }

    public String getContactNumberHint() {
        return contactNumberHint;
    }

    public void setContactNumberHint(String contactNumberHint) {
        this.contactNumberHint = contactNumberHint;
    }

    public boolean isIsgoogleplusActive() {
        return isgoogleplusActive;
    }

    public void setIsgoogleplusActive(boolean isgoogleplusActive) {
        this.isgoogleplusActive = isgoogleplusActive;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public String getShippingPolicyHint() {
        return shippingPolicyHint;
    }

    public void setShippingPolicyHint(String shippingPolicyHint) {
        this.shippingPolicyHint = shippingPolicyHint;
    }

    public String getPaymentDetailsHint() {
        return paymentDetailsHint;
    }

    public void setPaymentDetailsHint(String paymentDetailsHint) {
        this.paymentDetailsHint = paymentDetailsHint;
    }

    public String getBackgroundColorHint() {
        return backgroundColorHint;
    }

    public void setBackgroundColorHint(String backgroundColorHint) {
        this.backgroundColorHint = backgroundColorHint;
    }

    public String getMetaDescriptionHint() {
        return metaDescriptionHint;
    }

    public void setMetaDescriptionHint(String metaDescriptionHint) {
        this.metaDescriptionHint = metaDescriptionHint;
    }

    public String getCompanyLocalityHint() {
        return companyLocalityHint;
    }

    public void setCompanyLocalityHint(String companyLocalityHint) {
        this.companyLocalityHint = companyLocalityHint;
    }

    public String getCompanyDescriptionHint() {
        return companyDescriptionHint;
    }

    public void setCompanyDescriptionHint(String companyDescriptionHint) {
        this.companyDescriptionHint = companyDescriptionHint;
    }

}