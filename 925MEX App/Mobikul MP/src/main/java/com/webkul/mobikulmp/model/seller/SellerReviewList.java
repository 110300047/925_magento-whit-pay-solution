
package com.webkul.mobikulmp.model.seller;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerReviewList implements Parcelable {

    @SerializedName(value = "name", alternate = {"userName"})
    @Expose
    private String name;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName(value = "comment", alternate = {"description"})
    @Expose
    private String comment;
    @SerializedName(value = "priceRating", alternate = {"feedPrice"})
    @Expose
    private int priceRating;
    @SerializedName(value = "valueRating", alternate = {"feedValue"})
    @Expose
    private int valueRating;
    @SerializedName(value = "qualityRating", alternate = {"feedQuality"})
    @Expose
    private int qualityRating;

    protected SellerReviewList(Parcel in) {
        name = in.readString();
        summary = in.readString();
        date = in.readString();
        comment = in.readString();
        priceRating = in.readInt();
        valueRating = in.readInt();
        qualityRating = in.readInt();
    }

    public static final Creator<SellerReviewList> CREATOR = new Creator<SellerReviewList>() {
        @Override
        public SellerReviewList createFromParcel(Parcel in) {
            return new SellerReviewList(in);
        }

        @Override
        public SellerReviewList[] newArray(int size) {
            return new SellerReviewList[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getPriceRating() {
        return priceRating;
    }

    public String getPriceRatingString() {
        return String.valueOf(priceRating * 0.05);
    }

    public void setPriceRating(int priceRating) {
        this.priceRating = priceRating;
    }

    public int getValueRating() {
        return valueRating;
    }

    public String getValueRatingString() {
        return String.valueOf(valueRating * 0.05);
    }

    public void setValueRating(int valueRating) {
        this.valueRating = valueRating;
    }

    public int getQualityRating() {
        return qualityRating;
    }

    public String getQualityRatingString() {
        return String.valueOf(qualityRating * 0.05);
    }

    public void setQualityRating(int qualityRating) {
        this.qualityRating = qualityRating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(summary);
        dest.writeString(date);
        dest.writeString(comment);
        dest.writeInt(priceRating);
        dest.writeInt(valueRating);
        dest.writeInt(qualityRating);
    }
}
