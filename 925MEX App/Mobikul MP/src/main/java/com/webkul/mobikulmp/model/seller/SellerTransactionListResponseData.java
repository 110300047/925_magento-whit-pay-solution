package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.List;

public class SellerTransactionListResponseData extends BaseModel {

    @SerializedName("totalCount")
    @Expose
    private int totalCount;
    @SerializedName("transactionList")
    @Expose
    private List<SellerTransactionList> sellerTransactionList = null;
    @SerializedName("remainingTransactionAmount")
    @Expose
    private String remainingTransactionAmount;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<SellerTransactionList> getSellerTransactionList() {
        return sellerTransactionList;
    }

    public void setSellerTransactionList(List<SellerTransactionList> sellerTransactionList) {
        this.sellerTransactionList = sellerTransactionList;
    }

    public String getRemainingTransactionAmount() {
        return remainingTransactionAmount;
    }

    public void setRemainingTransactionAmount(String remainingTransactionAmount) {
        this.remainingTransactionAmount = remainingTransactionAmount;
    }
}