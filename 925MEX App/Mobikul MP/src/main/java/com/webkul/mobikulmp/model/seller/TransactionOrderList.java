package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionOrderList {

@SerializedName("qty")
@Expose
private String qty;
@SerializedName("price")
@Expose
private String price;
@SerializedName("totalTax")
@Expose
private String totalTax;
@SerializedName("shipping")
@Expose
private String shipping;
@SerializedName("totalPrice")
@Expose
private String totalPrice;
@SerializedName("commission")
@Expose
private String commission;
@SerializedName("incrementId")
@Expose
private String incrementId;
@SerializedName("productName")
@Expose
private String productName;
@SerializedName("subTotal")
@Expose
private String subTotal;

public String getQty() {
return qty;
}

public void setQty(String qty) {
this.qty = qty;
}

public String getPrice() {
return price;
}

public void setPrice(String price) {
this.price = price;
}

public String getTotalTax() {
return totalTax;
}

public void setTotalTax(String totalTax) {
this.totalTax = totalTax;
}

public String getShipping() {
return shipping;
}

public void setShipping(String shipping) {
this.shipping = shipping;
}

public String getTotalPrice() {
return totalPrice;
}

public void setTotalPrice(String totalPrice) {
this.totalPrice = totalPrice;
}

public String getCommission() {
return commission;
}

public void setCommission(String commission) {
this.commission = commission;
}

public String getIncrementId() {
return incrementId;
}

public void setIncrementId(String incrementId) {
this.incrementId = incrementId;
}

public String getProductName() {
return productName;
}

public void setProductName(String productName) {
this.productName = productName;
}

public String getSubTotal() {
return subTotal;
}

public void setSubTotal(String subTotal) {
this.subTotal = subTotal;
}

}