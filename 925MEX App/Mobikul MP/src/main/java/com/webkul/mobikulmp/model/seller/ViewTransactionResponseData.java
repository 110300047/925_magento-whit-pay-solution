package com.webkul.mobikulmp.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webkul.mobikul.model.BaseModel;

import java.util.ArrayList;

public class ViewTransactionResponseData extends BaseModel {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("orderList")
    @Expose
    private ArrayList<TransactionOrderList> transactionOrderList = null;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<TransactionOrderList> getTransactionOrderList() {
        return transactionOrderList;
    }

    public void setTransactionOrderList(ArrayList<TransactionOrderList> transactionOrderList) {
        this.transactionOrderList = transactionOrderList;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

}